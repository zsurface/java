
import java.sql.*;

import static classfile.Print.*;

import java.util.*;

import classfile.StopWatch;

// Notice, do not import com.mysql.jdbc.*
// or you will have problems!
// java mysql, mysql java, mysql conneciton, mysql table

/**
<pre>
{@literal
    -------------------------------------------------------------------------------- 
    Wed Apr 17 09:55:30 2019 
    -------------------------------------------------------------------------------- 
    database: test1
    table: person 
    user: user1 
    password: {empty}
    no ssh: autoReconnect=true&useSSL=false

    Sql file:

    1. connect to local MacOS mysql, connection is working so far
    2. selectInnerJoin data, insert data

    Thu Aug  8 11:02:03 2019     
    -------------------------------------------------------------------------------- 
    There is problem with JDBC driver in Intellij 2019.1 version.
    But it works under console Vim.
    Add JDBC driver to library in Intellij.
    see http://xfido.com/html/indexWhatIdidtoday.html#org234dad3
}
{@code
    Connection connect = DBManager.connect();
}
</pre>
*/ 

// TODO: write a better class
class DBManager{
    final static String user = "aron";
    final static String pass = "1234";
    final static String db = "test1";
    final static String timezone = "&serverTimezone=UTC";

    final static String SELECT_QUERY = "SELECT * from test1.snippet";
    final static String INSERT_QUERY = "INSERT INTO  test1.snippet (title, snippet) VALUES (?, ?)";
    final static String OUTER_JOIN_QUERY = "SELECT person.id, person.name, test1.contact.name from test1.person OUTER JOIN test1.contact ON test1.person.name = test1.contact.name";
    final static String INNER_JOIN_QUERY = "SELECT person.id, person.name, test1.contact.name from test1.person INNER JOIN test1.contact ON test1.person.name = test1.contact.name";


    /**
     * Create connection with Mysql on MacOS. with JDBC driver under $b/javalib/jar/mysql-connector-java
     * https://mvnrepository.com/artifact/mysql/mysql-connector-java
     * @return
     */
    public static Connection connect(){
        Connection connect = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/" + db + "?useSSL=false" + timezone + "&requireSSL=false", user, pass
                );
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            p("Clean up");
        }
        return connect;
    }
}

class SnippetObj{
    Integer id;
    String title;
    String snippet;
    public  SnippetObj(Integer id, String title, String snippet){
        this.id = id;
        this.title = title;
        this.snippet = snippet;
    }
    public  SnippetObj(String title, String snippet){
        this.title = title;
        this.snippet = snippet;
    }

}


public class Main {
    public static void main(String[] args) {
        Connection connect = null;
        Statement statement = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connect = DBManager.connect();

            List<SnippetObj> ls = Arrays.asList(
                                                new SnippetObj("title 1", "nice snippet 1"),
                                                new SnippetObj("title 2", "nice nsippet 2")
                );
            insertSnippet(ls, DBManager.INSERT_QUERY, connect);

        }catch (Exception ex) {
            // handle the error
            ex.printStackTrace(); 
        }finally{
            try{
            if(connect != null)
                connect.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * @param connect is db connection.
     * @return a list of Snippet Objects.
     */
    public static List<SnippetObj> selectSnippet(Connection connect){
        List<SnippetObj> ls = new ArrayList<>();
        try{
            Statement statement = null;
            ResultSet resultSet = null;
            statement = connect.createStatement();
            resultSet = statement.executeQuery(DBManager.SELECT_QUERY);
            while(resultSet.next()){
                ls.add(new SnippetObj(resultSet.getInt(1),  resultSet.getString(2), resultSet.getString(3)));
            }

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            p("selectInnerJoin(), finally clean up");
        }
        return ls;
    }
    public static void selectOuterJoin(Connection connect){
        try{

            // person and contact table.
            Statement statement = null;
            ResultSet resultSet = null;

            statement = connect.createStatement();

            StopWatch sw = new StopWatch();
            sw.start();
            // resultSet = statement.executeQuery("selectInnerJoin * from test1.person");

            resultSet = statement.executeQuery(DBManager.OUTER_JOIN_QUERY);

            Set<String> set = new HashSet<>();
            while(resultSet.next()) {
                // get the second column
                String id = resultSet.getString(1);
                String name = resultSet.getString(2);
                String email = resultSet.getString(3);
                p("id=" + id + " name=" + name + " email=" + email);
                set.add(name);
            }
            p("set.size()=" + set.size());
            sw.printTime();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            p("selectInnerJoin(), finally clean up");
        }

    }

    public static void selectInnerJoin(Connection connect){
        try{
            Statement statement = null;
            ResultSet resultSet = null;

            statement = connect.createStatement();

            StopWatch sw = new StopWatch();
            sw.start();
            // resultSet = statement.executeQuery("selectInnerJoin * from test1.person");
            resultSet = statement.executeQuery(DBManager.INNER_JOIN_QUERY);

            Set<String> set = new HashSet<>();
            if(resultSet.next()) {
                // get the second column
                String id = resultSet.getString(1);
                String name = resultSet.getString(2);
                String email = resultSet.getString(3);
                p("id=" + id + " name=" + name + " email=" + email);
                set.add(name);
            }
            p("set.size()=" + set.size());
            sw.printTime(); 
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            p("selectInnerJoin(), finally clean up");
        }

    }
    public static void insert(Connection connect){
        try{
            PreparedStatement preparedStatement = null;

            // insert data to table: tabledb.item(name, phone, email, description)
            preparedStatement = connect.prepareStatement("INSERT INTO  test1.contact (name, email) VALUES (?, ?)");
            // "myuser, webpage, datum, summary, COMMENTS from feedback.comments");
            // Parameters start with 1
            // preparedStatement.setString(1, "3");
            for(int i=0; i<100; i++){
                pl("insert " + i);
                preparedStatement.setString(1, "Michael");
                preparedStatement.setString(2, "dogdog@gmail.com");
                preparedStatement.executeUpdate();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            p("insert(): finally clean up");
        }
    }

    /**
     * @param ls is list of String.
     * @param sqlStr is sql string
     * @param connect is db connection.
     */
    public static void insertSnippet(List<SnippetObj> ls, String sqlStr, Connection connect){
        try{
            StopWatch sw = new StopWatch();

            PreparedStatement preparedStatement = null;
            preparedStatement = connect.prepareStatement(sqlStr);
            sw.start();

            for(SnippetObj s : ls){
                preparedStatement.setString(1, s.title);
                preparedStatement.setString(2, s.snippet);
                preparedStatement.executeUpdate();
            }
            sw.getElapsedTimeSecs();

            }catch(Exception e){
                e.printStackTrace();
            }finally{
                p("insert(): finally clean up");
            }
    }
}
