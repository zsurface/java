import static classfile.Print.*;
import static classfile.Aron.*;
import static classfile.Test.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

// regex example, regular expression example
// match, case insensitive, case-insensitive, case-sensitive, group, find
// *jregex* *j_regex_example_many*
public class Main {
    public static void main(String[] args) {
//        test0();
//        test1();
//        test2();
//        test3();
//        test4();
//        test5();
//        test6();
//        test7();
//        test8();
//        test9();
//        test10();
//        test11();
//        test12();
//        test13();

//          test14();
//          test_filter();
          test111();
//          test_non_greedy(); 
          test_non_greedy_1();

//        test_split0();
//        test_split1();
//        test_split2();

//        test0_fileType();
        test_fileExt();
    }
    static void test0() {
        beg();
        String pattern = "([a-z]+).([a-zA-Z-]+)(/)";
        String[] strArr = {
                "www.google.com/search?q=goog/nice",
                "www.google.com/search?q=goog",
                "http://www.google.msn.ca/search?q=goog",
                "http://www.google.msn.ca/a/b/c/d"
        };

        Pattern r = Pattern.compile(pattern);
        for(int i=0; i<strArr.length; i++) {
            Matcher mat = r.matcher(strArr[i]);
            if(mat.find()) {
                p("found=" + mat.group(0));
            }
        }

        end();
    }
    static void test1() {
        beg();
        Pattern r = Pattern.compile("captheorem", Pattern.CASE_INSENSITIVE);
        Matcher mat = r.matcher("CAPTHEOREM");
        if(mat.find()) {
            p("found=" + mat.group(0));
        }
        end();
    }
    static void test2() {
        beg();
        //Pattern pattern = Pattern.compile("[0-9]{3}[-]{0,1}[0-9]{3}[-]{0,1}[0-9]{4}");
        Pattern pattern = Pattern.compile("[0-9]{3}[-]?[0-9]{3}[-]?[0-9]{4}");
        Matcher matcher = pattern.matcher("334-4467777 yes... 334666-4456 415-333-9674 whatever 4264491569");
        while(matcher.find()) {
            System.out.println(matcher.group());
        }
        end();
    }
    static void test3() {
        beg();

        // match number
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher("334-4467777 yes... 334666-4456 415-333-9674 whatever 4264491569");
        while(matcher.find()) {
            System.out.println(matcher.group());
        }
        end();
    }
    static void test4() {
        beg();

        // match number and group
        Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher("334-4467777 yes... 334666-4456 415-333-9674 whatever 4264491569");
        while(matcher.find()) {
            System.out.println(matcher.group());
        }
        end();
    }
    static void test5() {
        beg();

        // match number
        Pattern pattern = Pattern.compile("[A-Z]+");
        Matcher matcher = pattern.matcher("This is case insensitive PREPOSITION");
        while(matcher.find()) {
            pbl(matcher.group());
        }
        end();
    }
    static void test6() {
        beg();

        // match number
        Pattern pattern = Pattern.compile("[a-zA-Z0-9_]+");
        Matcher matcher = pattern.matcher("This is case_INSENSITIVE PREPOSITION");
        while(matcher.find()) {
            pbl(matcher.group());
        }
        end();
    }
    static void test7() {
        beg();

        // excluding a,n, and space
        Pattern pattern = Pattern.compile("[^an ]+");
        Matcher matcher = pattern.matcher("This is banana");
        while(matcher.find()) {
            pbl(matcher.group());
        }
        end();
    }
    static void test8() {
        beg();

        // excluding all vowels
        Pattern pattern = Pattern.compile("[a-z&&[^youiea]]+");
        Matcher matcher = pattern.matcher("show all the words which do not contains vowel");
        while(matcher.find()) {
            pbl(matcher.group());
        }
        end();
    }
    static void test9() {
        beg();

        // POSIX character classes \p{Lower}+ => [a-z]+
        Pattern pattern = Pattern.compile("\\p{Lower}+");
        Matcher matcher = pattern.matcher("show all the words which do not contains vowel");
        while(matcher.find()) {
            pbl(matcher.group());
        }
        end();
    }

    // grouping
    static void test10() {
        beg();
        //import java.util.regex.Matcher;
        //import java.util.regex.Pattern;
        String str = "\"dog cat\" 3241.10 ";
        Pattern pattern = Pattern.compile("(\"[^\"]*\")|([0-9]*\\.?[0-9]+)");
        Matcher matcher = pattern.matcher(str);
        while(matcher.find()) {
            for(int i=1; i<= matcher.groupCount(); i++) {
                if(matcher.group(i) != null && matcher.group(i).length() > 0) {
                    String ss = matcher.group(i);
                    pbl(ss);
                }
            }
        }


        end();
    }

    static void test0_fileType(){
        beg();
        String fName = "file.png";
        t(fileType(fName).equals("IMG"));

        String fName1 = "file.jpeg";
        t(fileType(fName1).equals("IMG"));

        String fName2 = "file.jpg";
        t(fileType(fName2).equals("IMG"));

        String fName3 = "file.jpgk";
        f(fileType(fName3).equals("IMG"));

        String fName4 = "file.jpg ";
        f(fileType(fName4).equals("IMG"));

        String fName5 = "file.pdf";
        t(fileType(fName5).equals("PDF"));

        end();
    }

    /**
     * detect file types from file extensions: image(.png, .jpeg, .jpg) and PDF(.pdf)
     *
     * @param fName is name of file.
     * @return image file: "IMG" or pdf file: "PDF", empty otherwise
     *
     */
     static String fileType(String fName){
        String type = "";
        Pattern pdfPattern = Pattern.compile("\\.pdf$", Pattern.CASE_INSENSITIVE);
        Matcher pdfMatcher = pdfPattern.matcher(fName);

        Pattern pattern = Pattern.compile("\\.png|\\.jpeg|\\.jpg$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(fName);

        if(matcher.find()){
            pbl("fName=" + fName);
            type = "IMG";
        }else if(pdfMatcher.find()){
            pbl("fName=" + fName);
            type = "PDF";
        }
        return type;
    }

    public static void test_fileExt() {
        beg();

        String f = "dog.txt";
        String ext = fileExt(f);
        t(ext.equals("txt"));

        end();
    }

    public static void test11() {
        beg();

        // KEY: match boundary word, match word, match words, match word only
        // boundary word, boundary words only, boundary only, word boundary
        String[] arr = {
                "dog",
                "cat",
                "key-value"
        };
        Pattern pattern = Pattern.compile("(?<=^|\\s)[a-z]+(?=\\s|$)");
        printArray(arr);
        for(String s : arr) {
            Matcher matcher = pattern.matcher(s);
            if(matcher.find()) {
                pbl("match word=" + matcher.group());
            }
        }
        pbl("Does not match key-value");

        end();
    }

    public static void test111() {
        beg();
        {
            // match boundary word, match word, match words, match word only
            // boundary word, boundary words only, boundary only, word boundary
            String[] arr = {
                    " -- dog",
                    "cat",
                    "key-value"
            };
            Pattern pattern = Pattern.compile("\\s*--");
            printArray(arr);
            for (String s : arr) {
                Matcher matcher = pattern.matcher(s);
                if (matcher.find() == false) {
                    pbl("match word=" + s);
                }
            }
            pbl("Does not match key-value");
        }

        {
            // Match slash in Java, need two slash for one slash literal in Java, e.g. match Latex commands
            String[] arr = {
                    "\\begin",
                    "\\end"
            };
            Pattern pattern = Pattern.compile("\\\\[a-zA-Z0-9]+");
            printArray(arr);
            for (String s : arr) {
                Matcher matcher = pattern.matcher(s);
                if (matcher.find() == false) {
                    pbl("\\begin, \\end: match word=" + s);
                }
            }
            pbl("Does not match key-value");
        }

        end();
    }
    // split string with different delimiters
    static void test12() {
        beg();
        String str = "dog:cat, pig  cow";
        List<String> listToken = split(str, "\\s*:\\s*|\\s*,\\s*|\\s+");
        p(listToken);

        end();
    }
    static void test13() {
        beg();
        String str = "dog:cat, pig  cow";
        List<String> listToken = split(str, "\\s*:\\s*|\\s*,\\s*");
        p(listToken);

        end();
    }
    static void test14() {
        beg();
        String str = "pig  cow pig ";
        List<String> listToken = split(str, "\\s+");
        p(listToken);
        for(String token : listToken){
            pb(token);
        }

        end();
    }
    /**
     * split string with special char, e.g. "{" -> escape "\\{"
     * or use Character Group: [{]
     */
    public static void test_split0() {
        beg();

        String str = "The fun()  { dog it now }; ";
        List<String> list = split(str, "\\{");
        p(list);
        // [The fun()  ][ dog it now }; ]

        end();
    }


    /**
     * split string to different tokens with multiple delimiters
     * Character Group: [{};{}]
     */
    public static void test_split1() {
        beg();

        String str = "The fun()  { dog it now }; ";
        List<String> list = split(str, "[{};()]+");
        p(list);
        // [The fun][  ][ dog it now ][ ]

        end();
    }

    
    /**
    <pre>
    {@literal
        split string with multiple delimiters such as [ .,?!]
        ------------------------------------------------------------------ 
    }
    {@code
        split(str, delims);
    }
    </pre>
    */ 
    public static void test_split2() {
        beg();
        String str = "This is a sentence.  This is a question, right?  Yes!  It is.";
        String delims = "[ .,?!]+";
        List<String> list = split(str, delims);
        p(list);
        // [This][is][a][sentence][This][is][a][question][right][Yes][It][is]
        end();
    }

    public static void test_filter() {
        beg();
        
        String pattern = "^\\s*--";
        List<String> list = Arrays.asList("cat1", "dog1", " -- cow1");
        List<String> flist = filter(pattern, list);
        p(flist);

        end();
    }

    // KEY:non greedy, java non-greedy, java regex greedy
    public static void test_non_greedy() {
        beg();
        
        String[] arr = {
                "dog: *.txt, *.java: cat",
                "cat",
                "key-value"
        };
        Pattern pattern = Pattern.compile(":[^:]+:");
        printArray(arr);
        p();
        for(String s : arr) {
            Matcher matcher = pattern.matcher(s);
            if(matcher.find()) {
                pbl("match word=" + matcher.group());
                pbl(s);
            }
        }

        end();
    }
    
    // KEY:non greedy, java non-greedy, java regex greedy
    public static void test_non_greedy_1() {
        beg();

        {
            // KEY: exact n number of char
            String reg = "\\w{3}";
            String str = "dog cow pig";
            String out = str.replaceAll(reg, "X"); // cow cows 
            p(str + " [" + reg + "] " + " => " + out);
            // dog cow pig [\w{3}]  => X X X
        }
        {
            // KEY: greedy
            String reg = "\\w+";
            String str = "dog cow pig";
            String out = str.replaceAll(reg, "X"); // => X
            p(str + " [" + reg + "] " + " => " + out);
            // dog cow pig [\w+]  => X X X
        }
        {
            // KEY: non greedy
            String reg = "\\w+?";
            String str = "dog cow pig";
            String out = str.replaceAll(reg, "X"); // => XXX
            p(str + " [" + reg + "] " + " => " + out);
            // dog cow pig [\w+?]  => XXX XXX XXX
        }
        {
            // KEY: m to n char 
            String reg = "\\w{1,3}";
            String str = "dog cow pig";
            String out = str.replaceAll(reg, "X"); // => X X X 
            p(str + " [" + reg + "] " + " => " + out);
            // dog cow pig [\w{1,3}]  => X X X
        }
        {
            // KEY: non greedy quantifier
            String reg = "\\w{1,3}?";
            String str = "dog cow pig";
            String out = str.replaceAll(reg, "X"); // => XXX XXX XXX 
            p(str + " [" + reg + "] " + " => " + out);
            // dog cow pig [\w{1,3}?]  => XXX XXX XXX
        }
        {
            // KEY: possessive quantifier
            String reg = "\\w{1,3}+";
            String str = "dog cow pig";
            String out = str.replaceAll(reg, "X"); // => XXX XXX XXX 
            p(str + " [" + reg + "] " + " => " + out);
            // dog cow pig [\w{1,3}+]  => X X X
        }
        {
            // KEY: word boundary 
            String reg = "\\bdog\\b"; // \b word \b 
            String str = "dog cow pig mydog";
            String out = str.replaceAll(reg, "X"); // => XXX XXX XXX 
            p(str + " [" + reg + "] " + " => " + out);
            // dog cow pig [\w{1,3}+]  => X X X
        }
        {
            fl();
            String data1 = "Java is object oriented language, and myjava is language";
            String regex = "\\bjava\\b";
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(data1);
            p(data1);
            while (matcher.find()) {
                p("(" + matcher.start() + "," + matcher.end() + ")");
                p(matcher.group());
            }
        }

        {
            String mystr = "dog [cat] aaa <cat> pig {cat} catrat";
            String s = replaceStr(mystr, "\\bcat\\b", "KK");
            t(s, "dog [KK] aaa <KK> pig {KK} catrat");
        }
        {
            String mystr = "cat";
            String s = replaceStr(mystr, "\\bcat\\b", "KK");
            t(s, "KK");
        }
        {
            String mystr = "cats";
            String s = replaceStr(mystr, "\\bcat\\b", "KK");
            t(s, "cats");
        }
        {
            String mystr = "";
            String s = replaceStr(mystr, "\\bcat\\b", "KK");
            t(s, "");
        }
        {
            String mystr = "catcat";
            String s = replaceStr(mystr, "\\bcat\\b", "KK");
            t(s, "catcat");
        }
        {
            String mystr = "catcat";
            String s = replaceStr(mystr, "cat", "KK");
            t(s, "KKKK");
        }

        end();
    }
}
