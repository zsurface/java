import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_aaa{
    public static void main(String[] args) {
        // test0();
        test1();
    }
     
    public static void test0(){
        beg();
        {
            String s1 = "dog";
            String s2 = "dogs";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "";
            String s2 = "";
            t(diffOneChar(s1, s2), false);
        }
        {
            String s1 = "";
            String s2 = "a";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "abc";
            String s2 = "abec";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "abc";
            String s2 = "aebc";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "abc";
            String s2 = "aabc";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "aaa";
            String s2 = "aaaa";
            t(diffOneChar(s1, s2), true);
        }

        end();
    }

    public static Set<String> copySet(Set<String> set){
        Set<String> retSet = new HashSet<>();
        for(String s : set){
            retSet.add(s);
        }
        return retSet;
    }

    List<String> list = new ArrayList<String>();
    List<String> list = new ArrayList<String>();
    

    /**
    <pre>
    {@literal
        longest chain of words from a dictionary

        * The problem is same as maximum or minimum numbers of coins to sum to \[ $S$ \] 

        * http://localhost/html/indexCoinChangeDynamicProgramming.html
    }
    {@code
    }
    {@link #concat(List<T> list1, List<T> list2)}

    </pre>
    */ 
    public static void check(Set<String> set, String s1, List<String> ls, List<List<String>> lss){
        String foundStr = null;
        for(String s2 : set){
            // s2 > s1
            boolean b = diffOneChar(s1, s2);
            if(b){
                Set<String> newSet = copySet(set);
                // pb(s2);
                List<String> mylist = append(ls, s2); 
                if(lss.size() > 0){
                    if(lss.get(0).size() < mylist.size()){
                        lss.clear();
                        lss.add(mylist);
                    }
                }else{
                    lss.add(mylist);
                }
                newSet.remove(s2);
                check(newSet, s2, mylist, lss);
            }
        }
    }
    public static void test1(){
        beg();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<String>(Arrays.asList("ab"));
            check(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<String>(Arrays.asList("ab", "ac"));
            check(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<String>(Arrays.asList("ab", "ac", "acc", "kab"));
            check(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<String>(Arrays.asList("ab", "ac", "acc", "kab", "kkk", "kkb", "akcc"));
            check(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<String>(Arrays.asList("ab", "ac", "acc", "kab", "kkk", "kkb", "akcc", "bb", "cc", "eakcc", "akcfc"));
            check(set1, s1, ls, lss);
            pp(lss);
        }
        end();
    }
} 

