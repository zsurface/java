import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_triplenum{
    public static void main(String[] args) {
//        test1();
        test0();
    }
    public static void test0(){
        Integer[][] arr = {
                        {1, 1, 1, 9},
                        {0, 1, 0, 1},
                        {0, 1, 1, 1}
                    };
        Integer height = arr.length;
        Integer width = arr[0].length;
        Integer h = 0, w = 0;
        Integer n = countx(height, h, width, w, arr);
        p(n);
    }
    public static void test1(){
        List<Integer> ls = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 6, 7, 5);
        Integer sum = 18;
        Set<List<Integer>> set = triple(sum, ls);
        p(set);
    }

    // sum = a + b + c
    // sum = a + (b + c)
    // sum - a = (b + c)
    public static Set<List<Integer>> triple(Integer num, List<Integer> ls){
        Set<List<Integer>> tri = new HashSet<>();
        if(ls != null){
            Integer[] arr = new Integer[ls.size()];    
            int k=0;
            for(Integer n : ls){
                arr[k] = n;
                k++;
            }
            Set<List<Integer>> set = new HashSet<>();
            for(int i=0; i<ls.size(); i++){
                for(int j=0; j<ls.size(); j++){
                    for(int e=0; e<ls.size(); e++){
                        // 1 2 2, 1 2 1, 1 2 3 
                        if((i != j && i != e) && (i != j && j != e)){
                            int sum = ls.get(i) + ls.get(j) + ls.get(e);
                            List<Integer> list = new ArrayList<>();
                            if(sum == num){
                                list.add(ls.get(i));
                                list.add(ls.get(j));
                                list.add(ls.get(e));
                                Collections.sort(list);
                                tri.add(list);
                            }
                        }
                    }
                }
            }
        }
        return tri;
    }

    public static Integer countx(Integer height, Integer h, Integer width, Integer w, Integer[][] arr){
        if(arr != null){
            if(arr[h][w] == 9){    
                return 0;
            }else if(arr[h][w] == 1){
                arr[h][w] = 0;
                Integer up = height * width, 
                        down = height * width,
                        left = height * width,
                        right = height * width;
                if(h + 1 < height)
                    up = countx(height, h + 1, width, w, arr);
                if(h - 1 >= 0)
                    down = countx(height, h - 1, width, w, arr);
                if(w - 1 >= 0)
                    left = countx(height, h, width, w - 1, arr);
                if(w + 1 < width)
                    right = countx(height, h, width, w + 1, arr);

                return Math.min(Math.min(up, down), Math.min(left, right)) + 1;
            }
        }
        return height + width;
    }
} 

