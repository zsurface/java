import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;
public class try_nextServer{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        end();
    }
    public static void test1(){
        beg();
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);
            queue.add(3);

            PriorityQueue<Integer> pq = insert(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);
            queue.add(2);

            PriorityQueue<Integer> pq = insert(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);
            queue.add(2);
            queue.add(5);

            PriorityQueue<Integer> pq = insert(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);

            PriorityQueue<Integer> pq = insert(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(2);

            PriorityQueue<Integer> pq = insert(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }

        end();
    }

    
} 

