import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.util.PriorityQueue;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


class Person{
    String firstName;
    String lastName;
    Integer age;
    public Person(String firstName, String lastName, Integer age){
	this.firstName = firstName;
	this.lastName = lastName;
	this.age = age;
    }
}

public class try_PriorityQueue2{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

	PriorityQueue<Person> queue = new PriorityQueue<>((x, y) -> x.firstName.compareTo(y.firstName));
	queue.add(new Person("David", "lee", 20));
	queue.add(new Person("Tommy", "kee", 30));
	while(!queue.isEmpty()){
	    Person p = queue.remove();
	    System.out.println("first Name:" + p.firstName + " last Name:" + p.lastName + " age:" + p.age);
	}
		  


        sw.printTime();
        end();
    }
} 

