import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class JavaSingleLinkedList{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();
        {
            {
                fl("insertNode 1");
                Node h = new Node(1);
                Node n1 = new Node(2);
                Node head = insertNode(h, n1);
                printNode(head);
            }
            {
                fl("insertNode 2");
                Node h = null;
                Node n1 = new Node(1);
                Node head = insertNode(h, n1);
                printNode(head);
            }
            {
                fl("insertFront 1");
                Node h = null;
                Node n1 = new Node(1);
                Node head = insertFront(h, n1);
                printNode(head);
            }
            {
                fl("insertFront 2");
                Node h = new Node(1);
                Node n1 = new Node(2);
                Node head = insertFront(h, n1);
                printNode(head);
            }
            {
                fl("insertFront 3");
                Node h = new Node(1);
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node head = insertFront(h, n1);
                head = insertFront(head, n2);
                printNode(head);
            }
            {
                fl("insertLast 1");
                Node h = null; 
                Node n1 = new Node(1);
                Node head = insertLast(h, n1);
                printNode(head);
            }
            {
                fl("insertLast 2");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                Node head = insertLast(h, n1);
                printNode(head);
            }
            {
                fl("insertLast 2");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node head = insertLast(h, n1);
                head = insertLast(head, n2);
                printNode(head);
            }
            {
                fl("removeFront 1");
                Node h = null; 
                Node head = removeFront(h);
                printNode(head);
            }
            {
                fl("removeFront 2");
                Node h = new Node(1); 
                Node head = removeFront(h);
                printNode(head);
            }
            {
                fl("removeFront 3");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                insertNode(h, n1);
                Node head = removeFront(h);
                printNode(head);
            }
            {
                fl("removeFront 4");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node head = insertNode(h, n1);
                head = insertNode(head, n2);
                Node newH = removeFront(head);
                printNode(newH);
            }
            {
                fl("removeLast 1");
                Node head = null; 
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 2");
                Node head = new Node(1); 
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 3");
                Node head = new Node(1); 
                Node n1 = new Node(2);
                insertNode(head, n1);
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 4");
                Node head = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                head = insertNode(head, n1);
                head = insertNode(head, n2);
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 5");
                Node head = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node n3 = new Node(4);
                head = insertNode(head, n1);
                head = insertNode(head, n2);
                head = insertNode(head, n3);
                head = removeLast(head);
                printNode(head);
            }
        }

        end();
    }

    public static void printNode(Node head){
        Node curr = head;
        while(curr != null){
            pl(curr.toString());
            curr = curr.next;
        }
    }
    public static Node insertNode(Node head, Node node){
       if(head == null){
           head = node;
       }else{
            Node curr = head;
            Node prev = null;
            while(curr.next != null){
               curr = curr.next;
            }
            curr.next = node;
       }
       return head;
    }

    public static Node insertFront(Node head, Node node){
        if(head == null){
            head = node;
        }else{
            node.next = head;
            head = node;
        }
        return head;
    }

    public static Node insertLast(Node head, Node node){
        if(head == null){
            head = node;
        }else{
            Node curr = head;
            while(curr.next != null){
                curr = curr.next;
            }
            curr.next = node;
        }
        return head;
    }

    public static Node removeFront(Node head){
        if(head == null){
            // Nothing to remove
        }else{
            Node h2 = head.next;
            head = h2;
        }
        return head;
    }
    public static Node removeLast(Node head){
        if(head == null){
            // Nothing to remove
        }else{
            Node curr = head;
            Node prev = null;
            while(curr.next != null){
                prev = curr;
                curr = curr.next;
            }
            if(prev == null){  // Only one node
                head = null;
            }else{
               prev.next = null; 
            }
        }
        return head;
    }
} 

