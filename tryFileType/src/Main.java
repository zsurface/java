import static classfile.Aron.*;
import classfile.FileType;

import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static classfile.Print.fl;
import static classfile.Print.p;
import static classfile.Print.*;

import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.io.*;
import java.lang.String;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.Pair;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.io.FilenameUtils;
import com.opencsv.CSVReader;
import static classfile.Print.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.charset.StandardCharsets;

import java.text.SimpleDateFormat;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.IntStream;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

class Table<T extends Comparable<T>>{
    List<List<T>> table = of();
    public Table(List<List<T>> lss){
        table = lss;
    }
    public int rowCount(){
        return table.size();
    }
    public int colCount(){
        if(table.size() > 0){
            return table.get(0).size();
        }else{
            return -1;
        }
    }
    public Table(){
        table = of();
    }
    public List<T> getRow(int index){
        return table.get(index);
    }
    public List<T> getCol(int index){
        return table.stream().map(x -> x.get(index)).collect(Collectors.toList());
    }
    public Table sortCol(BiFunction<List<T>, List<T>, Integer> f){
        Collections.sort(table, f::apply);
        return new Table(table);
    }
    public Table getNRow(int m, int n){
        return null;
    }
    public Table getNCol(int m, int n){
        return null;
    }
    public List<Table> partitionNRow(int nRow){
        return null;
    }
    public List<Table> partitionNCol(int nCol){
        return null;
    }
    public Table filter(int nCol, String pattern){
        return null;
    }
    public void print(){
        p(table);
    }
}

public class Main {
    public static void main(String[] args) {
        p("Hello World");
        test0();

        String fpath = "/dog/cat/file.html/f1.html/f2.html/f3.html";
        boolean bo  = FileType.isHtml(fpath);
        p("bo=" + bo);

        test1();
        test2();
    }
    static void test0(){
        beg();
        String fname = "/tmp/ls.x";
        List<String> ls = readFile(fname);
        List<List<String>> lss = ls.stream().map(x -> split(x, "\\s+")).collect(Collectors.toList());
        List<List<String>> lns =  lss.stream().filter(x -> FileType.isJava(x.get(8))).collect(Collectors.toList());
        List<List<String>> lfs = lss.stream().filter(x -> isMatched(x.get(5), "Apr")).collect(Collectors.toList());


        p(lss);
        fl();
        p(lns);
        fl();
        p(lfs);

        end();
    }
    static void test1(){
        beg();

        String fname = "/tmp/ls.x";
        List<String> ls = readFile(fname);
        List<List<String>> lss = ls.stream().map(x -> split(x, "\\s+")).collect(Collectors.toList());
        Table table = new Table(lss);
        int rowCount = table.rowCount();
        p("rowCount=" + rowCount);
        int colCount = table.colCount();
        p("colCount=" + colCount);
        List<String> row = table.getRow(1);
        pr(row);
//        List<String> first = head(table.table);
//        pr(first);

        // array to list
        // String[] arr = {"dog"};
        // List<String> lss = Arrays.asList(array);
        //
        // list to array
        // String[] arr =  


        end();
    }
    static void test2(){
        beg();
        List<List<Integer>> lss = of();
        List<Integer> ls1 = of(0, 2, 4); 
        List<Integer> ls2 = of(9, 0, 0); 
        List<Integer> ls3 = of(3, 1, 6); 
        lss.add(ls1);
        lss.add(ls2);
        lss.add(ls2);
        Table table = new Table(lss);
        BiFunction<List<Integer>, List<Integer>, Integer> f = (x, y) -> x.get(2) - y.get(2);
        Table t = table.sortCol(f);
        fl();
        t.print();

        end();
    }
}
