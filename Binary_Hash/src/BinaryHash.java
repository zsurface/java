import classfile.Print;

/**
 * Created by cat on 2/12/19.
 */
class BinaryHash{
    String str;
    int data;
    BinaryHash left;
    BinaryHash right;
    public BinaryHash(String str){
        this.str = str;
        data = str.hashCode();
    }
}

class HashTree{
    BinaryHash root;
    public void insert(String s){
        BinaryHash r = root;
        if(root == null){
            r = root = new BinaryHash(s);
        }else{
            while(r != null) {
                if (r.data < s.hashCode()) {
                    if (r.left == null) {
                        r.left = new BinaryHash(s);
                        break;
                    } else {
                        r = r.left;
                    }
                } else {
                    if (r.right == null) {
                        r.right = new BinaryHash(s);
                    } else {
                        r = r.right;
                        break;
                    }
                }
            }
        }
    }
    public void inorder(BinaryHash r){
        if(r != null){
            inorder(r.left);
            Print.p(r.str + "  h=" + r.data);
            inorder(r.right);
        }
    }
}