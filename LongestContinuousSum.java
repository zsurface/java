import static classfile.Aron.*;
import static classfile.Print.*;

public class LongestContinuousSum {
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
        test8();
    }
    public static int Sum(int[] array) {
        int max = 0;
        if(array != null) {
            int len = array.length;
            int[] result = new int[len];
            if(len > 0) {
                result[0] = array[0];
                for(int i=1; i<len; i++) {
                    result[i] = Math.max(result[i-1] + array[i], array[i]);
                }
                for(int i=0; i<len; i++) {
                    if(max < result[i])
                        max = result[i];
                }
            }
        }
        return max;
    }

    public static void test0() {
        int[] array = {-2, 8, -5, 20, 3, -2};
        int max = Sum(array);
        p("max=" + max);
    }

    public static void test1() {
        int[] array = {1, -5, 8, -2};
        p(array);
        maxSumWithIndex(array);
        p();
    }

    public static void test2() {
        int[] array = {1, -5, 8, 4, -2, 3, -1};
        p(array);
        maxSumWithIndex(array);
        p();
    }
    public static void test3() {
        int[] array = {-2, 1, -5, 8, 4, -2, 3, -1};
        p(array);
        maxSumWithIndex(array);
        p();
    }
    public static void test4() {
        int[] array = {-2, 4, 2};
        p(array);
        maxSumWithIndex(array);
        p();
    }
    public static void test5() {
        int[] array = {-2, 4, 2, -8};
        p(array);
        maxSumWithIndex(array);
        p();
    }
    public static void test6() {
        int[] array = {-2};
        p(array);
        maxSumWithIndex(array);
        p();
    }
    public static void test7() {
        int[] array = {8};
        p(array);
        maxSumWithIndex(array);
        p();
    }
    public static void test8() {
        int[] array = {8, -1};
        p(array);
        maxSumWithIndex(array);
        p();
    }
    public static void maxSumWithIndex(int[] array) {
        if(array != null && array.length > 0) {
            int len = array.length;
            int[] maxArr = new int[len];
            maxArr[0] = array[0];

            int first = 0;
            int second = 0;
            int max = array[0];
            for(int i=1; i < len; i++) {
                maxArr[i] = Math.max(maxArr[i-1] + array[i], array[i]);
                if(maxArr[i] > max) {
                    if(i-1 >=0 && maxArr[i-1] < 0)
                        first = i;
                    second = i;
                    max = maxArr[i];
                }
            }

            p("max=" + max);
            p("first=" + first);
            p("second=" + second);
        }
    }
}
