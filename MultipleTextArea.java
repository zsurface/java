import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MultipleTextArea extends Application {
  public static void main(String[] args) { launch(args); }
  @Override public void start(Stage stage) throws Exception {
    System.out.println("JavaFX Version: " + System.getProperties().get("javafx.runtime.version"));
    VBox layout = new VBox(10);
    for (int i = 0; i < 3; i++) layout.getChildren().add(new TextArea("Area " + i));
    layout.setStyle("-fx-background-color: cornsilk; -fx-padding: 10;");
    stage.setScene(new Scene(layout));
    stage.show();
  }
}
