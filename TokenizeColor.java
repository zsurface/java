import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import static classfile.AnsiColor.*;
import static classfile.Token.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/*
enum Token{
        Unknown,
        BracketLeftSq,
        BracketRightSq,
        BracketLeft,
        BracketRight,
        BracketLeftAngle,
        BracketRightAngle,
        Equal,
        Period,
        Comma,
        Colon,
        Semicolon,
        DoubleQuote,
        Letter,
        Digit,
        Space,
        ForwardSlash,
        BackwardSlash,
        Asterisk,
        Plus,
        Minus,
        Question,
        NumSign,
        SingleQuote,
        Exclamation,
        Dollar,
        Verbar,
        Tilde,
        Lowbar,
        BracketCurlyLeft,
        BracketCurlyRight
}


class NameType{
    String s;
    Token type;
    public NameType(String s, Token type){
        this.s = s;
        this.type = type;
    }
    NameType mk(String s, Token type){
        return new NameType(s, type);
    }
    @Override
    public boolean equals(Object that){
       if(this == that) return true; 

       if(!(that instanceof NameType)) return false;

       NameType nameType = (NameType)that;
       return this.s.equals(nameType.s) && this.type == nameType.type;
    }

    @Override
    public String toString(){
        return "(s=" + s + " " + "type=" + type + ")";
    }
}
*/

public class TokenizeColor{
    public static void main(String[] args) {
	    List<String> ls = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            String line = null;
            while((line = br.readLine()) != null){
                ls.add(line);
                // System.out.println(line);
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        Set<String> set = new HashSet(list("int", "float", "String", "string"));
        Set<String> setLang = new HashSet(list("let", "new", "import", "for", "where"));

        for(String s : ls){
            if(matchHeader(s) != null){
                String color = GRAY_BACKGROUND + s + RESET;
                System.out.println(color);
            }else{
                List<NameType> lr = tokenize(s);
                List<NameType> lt = new ArrayList<>();
                for(NameType t : lr){
                    if(t.type == Token.Letter){
                        // Color Keyword
                        if(set.contains(t.s)){
                            String color = COLOR_X4 + t.s + RESET;
                            lt.add(mk(color, t.type));
                        }else if(setLang.contains(t.s)){
                            String color = YELLOW + t.s + RESET;
                            lt.add(mk(color, t.type));
                        }else{
                            lt.add(mk(t.s, t.type));
                        }
                    }else if(t.type == Token.Digit){
                        String color = WHITE + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else if(
                        t.type == Token.BracketLeftSq || 
                        t.type == Token.BracketRightSq ||
                        t.type == Token.BracketLeftAngle ||
                        t.type == Token.BracketRightAngle 
                        ){
                        // SEE: $b/javalib/AnsiColor.java
                        String color = RED + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else if(
                        t.type == Token.BracketLeft || 
                        t.type == Token.BracketRight
                        ){
                        String color = COLOR_X2 + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else if(
                        // :: -> color 
                        t.type == Token.Colon &&
                        len(t.s) == 2
                        ){
                        String color = COLOR_X3 + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else if(
                        t.type == Token.Equal 
                        ){
                        String color = YELLOW + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else if(
                        t.type == Token.Verbar 
                        ){
                        String color = PURPLE + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else if(
                        t.type == Token.BracketCurlyLeft ||
                        t.type == Token.BracketCurlyRight
                        ){
                        String color = GREEN + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else if(
                        t.type == Token.ForwardSlash
                        ){
                        String color = GREEN + t.s + RESET;
                        lt.add(mk(color, t.type));
                    }else{
                        lt.add(mk(t.s, t.type));
                    }
                }
                List<String> ss = map (x -> x.s, lt);
                String cstr = concat("", ss);
                System.out.println(cstr);
            }
            // pl(lt);
        }

        // test0();
    }
    public static void test0(){
        beg();
        {
            {
                fl("tokenize 1");
                String s = "1";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("1", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            {
                fl("tokenize 2");
                String s = "12";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("12", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }

            {
                fl("tokenize 3");
                String s = "123";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("123", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 4");
                String s = "a";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("a", Token.Letter));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 5");
                String s = "ab";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("ab", Token.Letter));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 6");
                String s = "abc";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("abc", Token.Letter));
                t(ls, expectedls);
                pl(ls);
            }

            {
                fl("tokenize 7");
                String s = "a1";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("a", Token.Letter), mk("1", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 8");
                String s = "a12";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("a", Token.Letter), mk("12", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 9");
                String s = "ab12";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("ab", Token.Letter), mk("12", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 10");
                String s = "abc12";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("abc", Token.Letter), mk("12", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 11");
                String s = "a 1";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("a", Token.Letter), mk(" ", Token.Space), mk("1", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 12");
                String s = "ab 1";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("ab", Token.Letter), mk(" ", Token.Space), mk("1", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 13");
                String s = "ab 12";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("ab", Token.Letter), mk(" ", Token.Space), mk("12", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 14");
                String s = "ab  12";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("ab", Token.Letter), mk("  ", Token.Space), mk("12", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 15");
                String s = "abc  123";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("abc", Token.Letter), mk("  ", Token.Space), mk("123", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 16");
                String s = "abc  123 4";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("abc", Token.Letter), mk("  ", Token.Space), mk("123",Token.Digit), mk(" ",Token.Space), mk("4", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 17");
                String s = "abc  [123 4";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("abc", Token.Letter), mk("  ", Token.Space), mk("[", Token.BracketLeftSq), mk("123", Token.Digit), mk(" ", Token.Space), mk("4", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 18");
                String s = "[abc  [123 4";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("[", Token.BracketLeftSq), mk("abc", Token.Letter), mk("  ", Token.Space), mk("[", Token.BracketLeftSq), mk("123", Token.Digit), mk(" ",Token.Space), mk("4", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 19");
                String s = "[abc  [123 4[";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("[", Token.BracketLeftSq), mk("abc", Token.Letter), mk("  ", Token.Space), mk("[", Token.BracketLeftSq), mk("123", Token.Digit), mk(" ", Token.Space), mk("4", Token.Digit), mk("[", Token.BracketLeftSq));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 20");
                String s = "[abc]  [123 4[";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("[", Token.BracketLeftSq), mk("abc", Token.Letter), mk("]", Token.BracketRightSq), mk("  ", Token.Space), mk("[", Token.BracketLeftSq), mk("123", Token.Digit), mk(" ", Token.Space), mk("4", Token.Digit), mk("[", Token.BracketLeftSq));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 21");
                String s = "[abc]  [123] 4[";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("[", Token.BracketLeftSq), mk("abc", Token.Letter), mk("]", Token.BracketRightSq), mk("  ", Token.Space), mk("[", Token.BracketLeftSq), mk("123", Token.Digit), mk("]", Token.BracketRightSq), mk(" ", Token.Space), mk("4", Token.Digit), mk("[", Token.BracketLeftSq));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 22");
                String s = "[abc]  [123] 4][";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("[", Token.BracketLeftSq), mk("abc", Token.Letter), mk("]", Token.BracketRightSq), mk("  ", Token.Space), mk("[", Token.BracketLeftSq), mk("123", Token.Digit), mk("]", Token.BracketRightSq), mk(" ", Token.Space), mk("4", Token.Digit), mk("]", Token.BracketRightSq),mk("[", Token.BracketLeftSq));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 23");
                String s = "[]";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("[", Token.BracketLeftSq), mk("]", Token.BracketRightSq));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 24");
                String s = "(";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 25");
                String s = ")";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            {
                fl("tokenize 26");
                String s = "()";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 27");
                String s = ")(";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk(")", Token.BracketRight), mk("(", Token.BracketLeft));
                t(ls, expectedls);
                pl(ls);
            }
            {
                fl("tokenize 28");
                String s = "(abc) (123)";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk("abc", Token.Letter), mk(")", Token.BracketRight), mk(" ", Token.Space), mk("(", Token.BracketLeft), mk("123", Token.Digit), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 29");
                String s = "(abc) efg (123)";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk("abc", Token.Letter), mk(")", Token.BracketRight), mk(" ", Token.Space), mk("efg", Token.Letter), mk(" ", Token.Space), mk("(", Token.BracketLeft), mk("123", Token.Digit), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 30");
                String s = "<>()[]";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("<", Token.BracketLeftAngle), mk(">", Token.BracketRightAngle), mk("(", Token.BracketLeft), mk(")", Token.BracketRight), mk("[", Token.BracketLeftSq), mk("]", Token.BracketRightSq));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 31");
                String s = "<abc> efg <123>";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("<", Token.BracketLeftAngle), mk("abc", Token.Letter), mk(">", Token.BracketRightAngle), mk(" ", Token.Space), mk("efg", Token.Letter), mk(" ", Token.Space), mk("<", Token.BracketLeftAngle), mk("123", Token.Digit), mk(">", Token.BracketRightAngle));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 32");
                String s = "=";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("=", Token.Equal));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 33");
                String s = "==";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("==", Token.Equal));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 34");
                String s = "x = 3";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("x", Token.Letter), mk(" ", Token.Space), mk("=", Token.Equal), mk(" ", Token.Space), mk("3", Token.Digit));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 35");
                String s = "x.y";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("x", Token.Letter), mk(".", Token.Period), mk("y", Token.Letter));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 36");
                String s = "(x.y)";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk("x", Token.Letter), mk(".", Token.Period), mk("y", Token.Letter), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 37");
                String s = "(x .= y)";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk("x", Token.Letter), mk(" ", Token.Space), mk(".", Token.Period), mk("=", Token.Equal), mk(" ", Token.Space), mk("y", Token.Letter), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 38");
                String s = "x,y";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("x", Token.Letter), mk(",", Token.Comma), mk("y", Token.Letter));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 39");
                String s = "x , y";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("x", Token.Letter), mk(" ", Token.Space), mk(",", Token.Comma), mk(" ", Token.Space), mk("y", Token.Letter));
                t(ls, expectedls);
                pl(ls);
            }
           
            {
                fl("tokenize 40");
                String s = "x := y";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("x", Token.Letter), mk(" ", Token.Space), mk(":", Token.Colon), mk("=", Token.Equal), mk(" ", Token.Space), mk("y", Token.Letter));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 41");
                String s = "x = y:";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("x", Token.Letter), mk(" ", Token.Space), mk("=", Token.Equal), mk(" ", Token.Space), mk("y", Token.Letter), mk(":", Token.Colon));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 42");
                String s = "<[(:)]>";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("<", Token.BracketLeftAngle), mk("[", Token.BracketLeftSq), mk("(", Token.BracketLeft), mk(":", Token.Colon), mk(")", Token.BracketRight), mk("]", Token.BracketRightSq), mk(">", Token.BracketRightAngle));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 43");
                String s = "\"";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("\"", Token.DoubleQuote));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 44");
                String s = "\"abc\"";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("\"", Token.DoubleQuote), mk("abc", Token.Letter), mk("\"", Token.DoubleQuote));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 45");
                String s = "(\"abc\")";
                List<NameType> ls = tokenize(s);
            List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk("\"", Token.DoubleQuote), mk("abc", Token.Letter), mk("\"", Token.DoubleQuote), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 46");
                String s = "(\" abc \")";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk("\"", Token.DoubleQuote), mk(" ", Token.Space), mk("abc", Token.Letter), mk(" ", Token.Space), mk("\"", Token.DoubleQuote), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }
            
            {
                fl("tokenize 47");
                String s = "(\" abc 123 \")";
                List<NameType> ls = tokenize(s);
                List<NameType> expectedls = list(mk("(", Token.BracketLeft), mk("\"", Token.DoubleQuote), mk(" ", Token.Space), mk("abc", Token.Letter), mk(" ", Token.Space), mk("123", Token.Digit), mk(" ", Token.Space), mk("\"", Token.DoubleQuote), mk(")", Token.BracketRight));
                t(ls, expectedls);
                pl(ls);
            }

        }
        {
            // NOTE: test only
            fl("tokenize string 1");
            String s = "abc [123]";
            List<NameType> ls = tokenize(s);
            List<NameType> lt = new ArrayList<>();
            for(NameType t : ls){
                if(t.type == Token.Letter){
                    String color = RED + t.s + RESET;
                    lt.add(mk(color, t.type));
                }else if(t.type == Token.Digit){
                    String color = WHITE + t.s + RESET;
                    lt.add(mk(color, t.type));
                }else if(t.type == Token.BracketLeftSq || t.type == Token.BracketRightSq ){
                    String color = GREEN + t.s + RESET;
                    lt.add(mk(color, t.type));
                }else if(t.type == Token.Verbar){
                    String color = YELLOW + t.s + RESET;
                    lt.add(mk(color, t.type));
                }else if(t.type == Token.BracketCurlyLeft || t.type == Token.BracketCurlyRight){
                    String color = COLOR_X1 + t.s + RESET;
                    lt.add(mk(color, t.type));
                }else{
                    // String color = WHITE + t.s + RESET;
                    lt.add(mk(t.s, t.type));
                }
            }
            pl(lt);
        }
        {
            {
                String s = "ab:*:123";
                matchHeader(s);
            }
        }
        end();
    }

    /**
        KEY: Match header

        abc:.hs,*.java:java, file
    */
    public static String matchHeader(String s){
        String retStr = null;
        String regex = "^[\\s]*[a-zA-Z0-9_-]+[\\s]*:[^:]+:[ a-zA-Z0-9_,-]+$";
        if(matchStr(regex, s)){
            retStr = s;
        }
        return retStr;
    }

    public static NameType mk(String s, Token type){
        return new NameType(s, type);
    }

    public static List<NameType> tokenize(String line){
        char BracketLeftSq = '[';
        char BracketRightSq = ']';
        char BracketLeft= '(';
        char BracketRight = ')';
        char BracketLeftAngle= '<';
        char BracketRightAngle = '>';
        char Equal = '=';
        char Period = '.';
        char Comma = ',';
        char Colon = ':';
        char Semicolon = ';';
        char DoubleQuote = '"';
        char Space = ' ';
        char ForwardSlash = '/';
        char BackwardSlash = '\\';
        char Asterisk = '*';  
        char Plus = '+';  
        char Minus = '-';  
        char Question = '?';  
        char NumSign = '#';  
        char SingleQuote = '\'';
        char Exclamation = '!';
        char Dollar = '$';
        char Verbar = '|';
        char Tilde = '~';
        char Lowbar = '_';
        char BracketCurlyLeft = '{';
        char BracketCurlyRight = '}';
        char Percent = '%';
        char AtSign= '@';


        List<NameType> ls = new ArrayList<>();
        int len = line.length();
        char curr = '\0';
        char next = '\0';
        String token = "";
        String dqString = "";
        int dqCount = 0;

        boolean isLetter = false;
        boolean isDigit = false;

        for(int i = 0; i < len; i++){
            curr = line.charAt(i);

            if(i + 1 < len){
                next = line.charAt(i + 1);
            }else{
                next = '\0';
            }

            if(isLetter(curr)){
                token += charToStr(curr); 
                if(isLetter(next) == false){
                    ls.add(new NameType(token, Token.Letter));
                    token = "";
                }
            }else if(isDigit(curr)){
                token += charToStr(curr);
                if(isDigit(next) == false){
                    ls.add(new NameType(token, Token.Digit));
                    token = "";
                }
            }else if(curr == Space){
                token += charToStr(curr); 
                if(next != Space){
                    ls.add(new NameType(token, Token.Space));
                    token = "";
                }
            }else if(curr == BracketLeftSq){
                token += charToStr(curr);
                if(next != BracketLeftSq){
                    ls.add(new NameType(token, Token.BracketLeftSq));
                    token = "";
                }
            }else if(curr == BracketRightSq){
                token += charToStr(curr);
                if(next != BracketRightSq){
                    ls.add(new NameType(token, Token.BracketRightSq));
                    token = "";
                }
            }else if(curr == BracketRight){
                token += charToStr(curr);
                if(next != BracketRight){
                    ls.add(new NameType(token, Token.BracketRight));
                    token = "";
                }
            }else if(curr == BracketLeft){
                token += charToStr(curr);
                if(next != BracketLeft){
                    ls.add(new NameType(token, Token.BracketLeft));
                    token = "";
                }
            }else if(curr == BracketLeftAngle){
                token += charToStr(curr);
                if(next != BracketLeftAngle){
                    ls.add(new NameType(token, Token.BracketLeftAngle));
                    token = "";
                }
            }else if(curr == BracketRightAngle){
                token += charToStr(curr);
                if(next != BracketRightAngle){
                    ls.add(new NameType(token, Token.BracketRightAngle));
                    token = "";
                }
            }else if(curr == Equal){
                token += charToStr(curr);
                if(next != Equal ){
                    ls.add(new NameType(token, Token.Equal));
                    token = "";
                }
            }else if(curr == Period){
                token += charToStr(curr);
                if(next != Period){
                    ls.add(new NameType(token, Token.Period));
                    token = "";
                }
            }else if(curr == Comma){
                token += charToStr(curr);
                if(next != Comma){
                    ls.add(new NameType(token, Token.Comma));
                    token = "";
                }
            }else if(curr == Colon){
                token += charToStr(curr);
                if(next != Colon){
                    ls.add(new NameType(token, Token.Colon));
                    token = "";
                }
            }else if(curr == Semicolon){
                token += charToStr(curr);
                if(next != Semicolon){
                    ls.add(new NameType(token, Token.Semicolon));
                    token = "";
                }
            }else if(curr == DoubleQuote){
                token += charToStr(curr);
                if(next != DoubleQuote){
                    ls.add(new NameType(token, Token.DoubleQuote));
                    token = "";
                }
            }else if(curr == ForwardSlash){
                token += charToStr(curr);
                if(next != ForwardSlash){
                    ls.add(new NameType(token, Token.ForwardSlash));
                    token = "";
                }
            }else if(curr == Asterisk){
                token += charToStr(curr);
                if(next != Asterisk){
                    ls.add(new NameType(token, Token.Asterisk));
                    token = "";
                }
            }else if(curr == Plus){
                token += charToStr(curr);
                if(next != Plus){
                    ls.add(new NameType(token, Token.Plus));
                    token = "";
                }
            }else if(curr == Minus){
                token += charToStr(curr);
                if(next != Minus){
                    ls.add(new NameType(token, Token.Minus));
                    token = "";
                }
            }else if(curr == Question){
                token += charToStr(curr);
                if(next != Question){
                    ls.add(new NameType(token, Token.Question));
                    token = "";
                }
            }else if(curr == NumSign){
                token += charToStr(curr);
                if(next != NumSign){
                    ls.add(new NameType(token, Token.NumSign));
                    token = "";
                }
            }else if(curr == SingleQuote){
                token += charToStr(curr);
                if(next != SingleQuote){
                    ls.add(new NameType(token, Token.SingleQuote));
                    token = "";
                }
            }else if(curr == Exclamation){
                token += charToStr(curr);
                if(next != Exclamation){
                    ls.add(new NameType(token, Token.Exclamation));
                    token = "";
                }
            }else if(curr == Dollar){
                token += charToStr(curr);
                if(next != Dollar){
                    ls.add(new NameType(token, Token.Dollar));
                    token = "";
                }
            }else if(curr == Verbar){
                token += charToStr(curr);
                if(next != Verbar){
                    ls.add(new NameType(token, Token.Verbar));
                    token = "";
                }
            }else if(curr == Tilde){
                token += charToStr(curr);
                if(next != Tilde){
                    ls.add(new NameType(token, Token.Tilde));
                    token = "";
                }
            }else if(curr == Lowbar){
                token += charToStr(curr);
                if(next != Lowbar){
                    ls.add(new NameType(token, Token.Lowbar));
                    token = "";
                }
            }else if(curr == BracketCurlyLeft){
                token += charToStr(curr);
                if(next != BracketCurlyLeft){
                    ls.add(new NameType(token, Token.BracketCurlyLeft));
                    token = "";
                }
            }else if(curr == BracketCurlyRight){
                token += charToStr(curr);
                if(next != BracketCurlyRight){
                    ls.add(new NameType(token, Token.BracketCurlyRight));
                    token = "";
                }
            }else if(curr == Percent){
                token += charToStr(curr);
                if(next != Percent){
                    ls.add(new NameType(token, Token.Percent));
                    token = "";
                }
            }else if(curr == AtSign){
                token += charToStr(curr);
                if(next != AtSign){
                    ls.add(new NameType(token, Token.AtSign));
                    token = "";
                }
            }else{
                token += charToStr(curr);
                if(next != '\0'){
                    ls.add(new NameType(token, Token.Unknown));
                    token = "";
                }
            }
        }
        if(isLetter(next)){
            token += charToStr(next); 
            ls.add(new NameType(token, Token.Letter));
            token = "";
        }else if(isDigit(next)){
            token += charToStr(next); 
            ls.add(new NameType(token, Token.Digit));
            token = "";
        }else if(next == Space){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Space));
            token = "";
        }else if(next == BracketLeftSq){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketLeftSq));
            token = "";
        }else if(next == BracketRightSq){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketRightSq));
            token = "";
        }else if(next == BracketRight){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketRight));
            token = "";
        }else if(next == BracketLeft){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketLeft));
            token = "";
        }else if(next == BracketLeftAngle){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketLeftAngle));
            token = "";
        }else if(next == BracketRightAngle){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketRightAngle));
            token = "";
        }else if(next == Equal){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Equal));
            token = "";
        }else if(next == Period){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Period));
            token = "";
        }else if(next == Comma){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Comma));
            token = "";
        }else if(next == Colon){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Colon));
            token = "";
        }else if(next == Semicolon){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Semicolon));
            token = "";
        }else if(next == DoubleQuote){
            token += charToStr(next);
            ls.add(new NameType(token, Token.DoubleQuote));
            token = "";
        }else if(next == ForwardSlash){
            token += charToStr(next);
            ls.add(new NameType(token, Token.ForwardSlash));
            token = "";
        }else if(next == Asterisk){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Asterisk));
            token = "";
        }else if(next == Plus){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Plus));
            token = "";
        }else if(next == Minus){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Minus));
            token = "";
        }else if(next == Question){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Question));
            token = "";
        }else if(next == NumSign){
            token += charToStr(next);
            ls.add(new NameType(token, Token.NumSign));
            token = "";
        }else if(next == SingleQuote){
            token += charToStr(next);
            ls.add(new NameType(token, Token.SingleQuote));
            token = "";
        }else if(next == Exclamation){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Exclamation));
            token = "";
        }else if(next == Dollar){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Dollar));
            token = "";
        }else if(next == Verbar){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Verbar));
            token = "";
        }else if(next == Tilde){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Tilde));
            token = "";
        }else if(next == Lowbar){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Lowbar));
            token = "";
        }else if(next == BracketCurlyLeft){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketCurlyLeft));
            token = "";
        }else if(next == BracketCurlyRight){
            token += charToStr(next);
            ls.add(new NameType(token, Token.BracketCurlyRight));
            token = "";
        }else if(next == Percent){
            token += charToStr(next);
            ls.add(new NameType(token, Token.Percent));
            token = "";
        }else if(next == AtSign){
            token += charToStr(next);
            ls.add(new NameType(token, Token.AtSign));
            token = "";
        }
        return ls;
    }
}
