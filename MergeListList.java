import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;

public class MergeListList{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        Map<String, List<List<String>>> map1 = new HashMap<>();
        Map<String, List<List<String>>> map2 = new HashMap<>();
        // mutable list
        List<String> list1 = new ArrayList<>(Arrays.asList("cat", "dog", "cow"));
        List<String> list2 = new ArrayList<>(Arrays.asList("cat1", "dog1", "cow1"));
        List<String> list3 = new ArrayList<>(Arrays.asList("pig", "fox", "rat"));
        List<List<String>> listList1 = new ArrayList<>();
        listList1.add(list1);
        List<List<String>> listList2 = new ArrayList<>();
        listList2.add(list2);

        List<List<String>> listList3 = new ArrayList<>();
        listList3.add(list3);
        //
        map1.put("key1", listList1);
        map2.put("key2", listList2);
        map2.put("key1", listList3);
        Map<String, List<List<String>>> map = Aron.mergeMapListList(map1, map2);
        //
        for(Map.Entry<String, List<List<String>>> entry : map.entrySet()){
            //System.out.println("[" + entry.getKey());
            for(List<String> list : entry.getValue()){
                for(String s : list)
                    System.out.println("[" + entry.getKey() + "]->[" + s + "]");
            }
        }

        Aron.end();
    }
    public static Map<String, List<List<String>>>  mergeMapListList(
        Map<String, List<List<String>>> map1, 
        Map<String, List<List<String>>> map2){

        for(Map.Entry<String, List<List<String>>> entry : map1.entrySet()){
            System.out.println("[" + entry.getKey());
            
            // merge map1 => map2
            List<List<String>> list2 = map2.get(entry.getKey());
            if (list2 != null){
                for(List<String> ls: entry.getValue()){
                    list2.add(ls);
                }
            }else{
                map2.put(entry.getKey(), entry.getValue()); 
            }
        }
        return map2;
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
} 

