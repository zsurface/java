import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class removeDuplicate{
    public static void main(String[] args) {
        test0();
        //test1();
    }
    public static void test0(){
        beg();

        String s = "a";
        
        // immutable list
        {
            List<Integer> list1 = Arrays.asList(1, 2, 3, 4);
            List<Integer> list2 = Arrays.asList();
            List<Pair<Integer, Integer>> pl = zip(list1, list2);
            for(Pair<Integer, Integer> p : pl){
                p("k=" + p.getKey() + " v=" + p.getValue());
            }
        }
        {
            List<Integer> list1 = Arrays.asList(1, 2, 3, 3, 4);
            int n = monotonicList(list1);
            p("increasing=" + (n == -1));
        }
        {
            List<Integer> list1 = Arrays.asList(1, 2, 0, 4);
            int n = monotonicList(list1);
            p("increasing=" + (n == 0));
        }
        {
            List<Integer> list1 = Arrays.asList(3, 2, 1);
            int n = monotonicList(list1);
            p("decreasing=" + (n == 1));
        }
        {
            fl();
            /**
                1 2 3
                (0, 0) (1, 1) (2, 2) => len = 1
                (0, 1) (1, 2)        => len = 2
                (0, 2)               => len = 3

                (0, 0) -> (0, 1) -> (0, 2)
                (1, 1) -> (1, 2)
                (2, 2)

            */
            List<Integer> list1 = Arrays.asList(1, 2, 2, 1, 4, 5);
            Integer sum = 5;
            List<List<Integer>> lss = continuousSumA(list1, sum);
            p(lss);
        }
        {
            fl();
            List<Integer> list1 = Arrays.asList(1);
            Integer sum = 1;
            List<List<Integer>> lss = continuousSumA(list1, sum);
            p(lss);
        }
        {
            fl();
            String s1 = "a";
            List<String> list = substringAll(s1);
            p(list);
        }
        {
            fl();
            List<Integer> ls = Arrays.asList(1, 2, 3, 4);
            List<Integer> s1 = init(ls);
            List<Integer> s2 = tail(ls);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            List<Boolean> s3 = zipWith(f, s1, s2); 
            p(s3);
        }
        {
            fl();
            List<Integer> ls = Arrays.asList(1, 2, 1, 4);
            List<Integer> s1 = init(ls);
            List<Integer> s2 = tail(ls);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            Boolean b = zipWith(f, s1, s2).stream().reduce(true, (x, y) -> x & y);
            p(b);
        }
        {
            fl();
            List<Integer> ls = Arrays.asList(1, 2, 3);
            List<Integer> s1 = init(ls);
            List<Integer> s2 = tail(ls);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            Boolean b = zipWith(f, s1, s2).stream().reduce(true, (x, y) -> x & y);
            p(b);
        }

//        {
//            fl();
//            List<Integer> ls = Arrays.asList(7, 2, 3, 2, 3);
//            Pair<Integer, Integer> pair= continuousSumFast(ls, 8);
//            p(pair); 
//        }
//        {
//            fl();
//            List<Integer> ls = Arrays.asList(8);
//            Pair<Integer, Integer> pair= continuousSumFast(ls, 8);
//            p(pair); 
//        }
//        {
//            fl();
//            List<Integer> ls = Arrays.asList(9, 7, 1);
//            Pair<Integer, Integer> pair= continuousSumFast(ls, 8);
//            p(pair); 
//        }

        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 1000000;
            List<Integer> list = Arrays.asList(1); 
            List<Pair<Integer, Integer>> ls = continuousSumFastA(list, 1);
            for(Pair<Integer, Integer> pair : ls ){
                p(pair);
            }

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 1000000;
            List<Integer> list = Arrays.asList(1, 2, 4); 
            List<Pair<Integer, Integer>> ls = continuousSumFastA(list, 6);
            for(Pair<Integer, Integer> pair : ls ){
                p(pair);
            }

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 1000000;
            List<Integer> list = Arrays.asList(1, 2, 4, 6); 
            List<Pair<Integer, Integer>> ls = continuousSumFastA(list, 6);
            for(Pair<Integer, Integer> pair : ls ){
                p(pair);
            }

            sw.printTime();
        }
//        {
//            StopWatch sw = new StopWatch();
//            sw.start();
//
//            int max = 1000000;
//            List<Integer> list = geneListInteger(0, max); 
//            Pair<Integer, Integer> pair = continuousSumSlow(list, 200);
//
//            sw.printTime();
//        }



        end();
    }

    public static List<Pair<Integer, Integer>> continuousSumFastA(List<Integer> list, Integer k){
        List<Pair<Integer, Integer>> lp = new ArrayList<>();
        if(list != null){
            int len = list.size();
            if(len > 0){
                Integer s = 0;
                int start = 0;
                int i = 0;
                while(i<len && k != s){
                    if(s < k)
                        s += list.get(i);
                    if(s > k){
                        while(start <= i && s > k){
                            s -= list.get(start);    
                            start++;
                        }
                        // k == s or k < s
                    } 
                    if(k == s){
                        lp.add(new Pair<>(start, i));
                        s = 0;
                        if(start < i)
                        start = i + 1;
                    }

                    i++;
                }
            }
        }
        return lp;
    }
    
    /**
    <pre>
    {@literal
        Given a sum and an Integer list, find the continuous sum from the list
        1. Find all the continuous Integers, and check each sum
        2. Add the sublist to list if their sum is equal to the given sum.
    }
    {@code
            List<Integer> list1 = Arrays.asList(1, 2, 2, 1, 4, 5);
            Integer sum = 5;
            List<List<Integer>> lss = continuousSumA(list1, sum);
            p(lss);
    }
    </pre>
    */ 
    public static List<List<Integer>> continuousSumA(List<Integer> list, Integer sum){
        List<List<Integer>> ls = new ArrayList<>();
        List<Pair<Integer, Integer>> pl = new ArrayList<>();
        if(list != null){
            int len = list.size();
            for(int i=0; i<len; i++){
                for(int k=0; k<len; k++){
                    if(i < k + 1){
                        List<Integer> lss = list.subList(i, k + 1); // "abc" => a, ab, abc, b, bc, c
                        Integer s = lss.stream().reduce(0, (x, y) -> x + y); 
                        if(s == sum){
                            pl.add(new Pair<Integer, Integer>(i, k + 1 - 1));
                            ls.add(lss);
                        }
                    }
                }
            }
        }
        for(Pair<Integer, Integer> s : pl){
            p("k=" + s.getKey() + " v=" + s.getValue());
        }
        return ls;
    }
    
    public static void test1(){
        // immutable list
        List<String> list = Arrays.asList("cat1", "dog1", "cow1");
        List<Integer> lns = IntStream.range(0, list.size()).mapToObj(i -> list.get(i).length()).collect(Collectors.toList());
        Print.p("lns" + lns);
    }
} 

