import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Contact implements Comparable<Contact>{
    int age;
    public Contact(int age){
        this.age = age;
    }
    public int compareTo(Contact c){
        // ordering: 1, 2, 3
        return age - c.age;
    }
}

public class FindKSmallest{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        List<Contact> ls = new ArrayList<>();
        ls.add(new Contact(1));
        ls.add(new Contact(3));
        ls.add(new Contact(2));
        ls.add(new Contact(4));
        ls.add(new Contact(9));
        ls.add(new Contact(0));

        int k = 3;
        List<Contact> list = findKSmallestItem(ls, k);
        for(Contact c : list){
            Print.p("c.age=" + c.age);
        }


        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        Find the kth smallest Contacts from a list of Contacts

        Use PriorityQueue
        Test File: /Users/cat/myfile/bitbucket/java/FindKSmallest.java
    }
    {@code
        class Contact implements Comparable<Contact>{
            int age;
            public Contact(int age){
                this.age = age;
            }
            public int compareTo(Contact c){
                // ordering: 1, 2, 3
                return age - c.age;
            }
        }

        List<Contact> ls = new ArrayList<>();
        ls.add(new Contact(1));
        ls.add(new Contact(3));
        ls.add(new Contact(2));
        ls.add(new Contact(4));
        ls.add(new Contact(9));
        ls.add(new Contact(0));

        int k = 3;
        List<Contact> list = findKSmallestItem(ls, k);
        for(Contact c : list){
            Print.p("c.age=" + c.age);
        }
    }
    </pre>
    */ 
    public static List<Contact> findKSmallestItem(List<Contact> list, int k){
        List<Contact> ret = new ArrayList<>();
        PriorityQueue queue = new PriorityQueue();
        for(Contact c : list){
            queue.add(c);
        }
        while(k > 0){
            Contact c = (Contact)queue.remove();
            ret.add(c);
            k--;
        }
        return ret;
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
} 

