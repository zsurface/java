import java.util.regex.Matcher;
import javafx.util.Pair;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class pivotEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        Integer[] arr = {1, 2};
        int lo = 0, hi = arr.length - 1;
        Pair<Integer, Integer[]> pair = parition2(arr, lo, hi);
        Print.p("pi=" + pair.getKey());
        Print.p(pair.getValue());
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }

    
    /**
    <pre>
    {@literal
        partition array 2, it is easy to understand
        Test File: /Users/cat/myfile/bitbucket/java/pivotEx.java
    }
    {@code
    }
    </pre>
    */ 
    public static Pair<Integer, Integer[]> parition2(Integer[] arr, int lo, int hi){
            int len = (hi - lo + 1);
            Integer[] left = new Integer[len];
            int p = arr[hi];
            int k = lo;
            int r = hi;
            int pi = k;
            for(int i=lo; i<=(hi - 1); i++){
                if(arr[i] <= p ){
                    left[k] = arr[i];
                    k++;
                }else{
                    left[r] = arr[i];
                    r--;
                }
            }
            left[k] = p;
            pi = k;
            return new Pair<Integer, Integer[]>(pi, left);
    }
} 

