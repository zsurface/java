import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class GroupEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            List<String> actual = groupBy(f, "abc");
            List<String> expected = Arrays.asList("abc"); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) == 0;
            List<String> actual = groupBy(f, "abc");
            List<String> expected = Arrays.asList("a", "b", "c"); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) > 0;
            List<String> actual = groupBy(f, "abc");
            List<String> expected = Arrays.asList("a", "b", "c"); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) == 0;
            List<String> actual = groupBy(f, "aac");
            List<String> expected = Arrays.asList("aa", "c"); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) <= 0;
            List<String> actual = groupBy(f, "112231");
            List<String> expected = Arrays.asList("11223", "1"); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) <= 0;
            List<String> actual = groupBy(f, "a");
            List<String> expected = Arrays.asList("a"); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) <= 0;
            List<String> actual = groupBy(f, "");
            List<String> expected = Arrays.asList(""); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) == 0;
            List<String> actual = groupBy(f, "");
            List<String> expected = Arrays.asList(""); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) > 0;
            List<String> actual = groupBy(f, "");
            List<String> expected = Arrays.asList(""); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        {
            beg();
            BiFunction<Character, Character, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            List<String> actual = groupBy(f, "");
            List<String> expected = Arrays.asList(""); 
            Print.pb(actual);
            Print.pb(expected);
            Test.t(actual, expected);
        }
        end();
    }
    public static void test1(){
        beg();
        end();
    }
} 

