import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_diophatine_equation{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();

        StopWatch sw = new StopWatch();

        int n = 300;
        for(int a=1; a < n; a++){
            for(int b=1; b < n; b++){
                for(int c = 1; c < n; c++){
                    for(int d = 1; d < n; d++){
                        if(d > a && d > b && d > c){
                            boolean m1 = !((a % 2 == 1 && b % 2 == 1 && c % 2 == 1) && (d % 2 == 0));
                            if(m1){
                                boolean m2 = !((a % 2 == 0 && b % 2 == 0 && c % 2 == 0) && (d % 2 == 1));
                                if(m2){
                                    boolean m3 = !((a % 2 == 0 && b % 2 == 0 && c % 2 == 1) && (d % 2 == 0));
                                    if(m3){
                                        if(Math.pow(a, 3) + Math.pow(b, 3) + Math.pow(c, 3) == Math.pow(d, 3)){
                                            fl();
                                            pl(a);
                                            pl(b);
                                            pl(c);
                                            pl(d);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        sw.print();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        sw.printTime();
        end();
    }
} 

