import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class RunFromEmacs{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();

		 String str = "palindrome";
		 var s = repeat(len(str), strToListChar(str));
		 var m = outer((x, y) -> x <= y ? 1 : 0, range(0, len(str)), range(0, len(str)));
		 var m1 = mask2(m, s);
		 var m2 = mask2(transpose(m), s);
		 var sm1 = map(x -> charListToStr(x), m1);
		 var sm2 = map(x -> charListToStr(x), m2);
		 pp(sm2);



        end();
    }
    public static void test1(){
        beg();
        end();
    }
} 

