import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Contact implements Comparable<Contact>{
    int age;
    public Contact(int age){
        this.age = age;
    }
    public int compareTo(Contact c){
        // ordering: 1, 2, 3
        return age - c.age;
    }
}

public class try_priorityqueue{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        
        PriorityQueue queue = new PriorityQueue();
        queue.add(new Contact(1));
        queue.add(new Contact(3));
        queue.add(new Contact(2));
        queue.add(new Contact(4));
        queue.add(new Contact(9));
        queue.add(new Contact(0));
        List<Contact> list = new ArrayList<>();
        int k = 3;
        while(k > 0){
            Contact c = (Contact)queue.remove();
            list.add(c);
            k--;
        }
        for(Contact c : list){
            Print.p("c.age=" + c.age);
        }


        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        
        Aron.end();
    }
} 

