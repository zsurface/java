import classfile.*;
import static classfile.Print.*;
import static classfile.Aron.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.*;


/**
   KEY: longest increasing subseqence
   http://xfido.com/html/indexLongestIncreasingSubsequenceRecursion.html

   Tue 13 Jul 15:57:29 2021 
   Fixed a bug in the code, serious bug:)

   TODO: add more test cases
*/
public class LongestIncreasingSubsequence {
    public static void main(String[] args) {
        // test0();
        // test1();
        // test2();
        // test3();
        // test4();
        // test5();
        LIS_test();
    }

    public static int maxList(int[] ls){
        int max = Integer.MIN_VALUE;
        for(int n : ls){
            if(n > max){
                max = n;
            }
        }
        return max;
    }

    public static Integer maxList(Integer[] ls){
        Integer max = Integer.MIN_VALUE;
        for(Integer n : ls){
            if(n > max){
                max = n;
            }
        }
        return max;
    }


    /**
    <pre>
    {@literal
        generate random [0...n-1], generate random list integer
    }
    {@code
    
      KEY: Longest Increasing Subsequence
      Dynamic programming O(n^2)
      
       i < j && [i] < [j]
       

               [i]        [j]
	         [i] < [j]
	      maxArr[j] = max{maxArr[i] + 1, maxArr[j]}
    }
    </pre>
    */ 
    public static Integer[] LIS(Integer[] arr){
        int len = arr.length;
        List<Integer> ls = IntStream.range(0, len).map(x -> 1).boxed().collect(Collectors.toList());
        Integer[] maxArr = listToArrayInteger(ls);

        for(int i = 0; i < len; i++){
            for(int j = i + 1; j < len; j++){
                if(arr[i] < arr[j]){
                    maxArr[j] = Math.max(maxArr[i] + 1, maxArr[j]);
                }
            }
        }
        // printArr(maxArr);
        return maxArr;
    }

    /**
       KEY: similar to Facebook interview question

       print all subsequence of a given string

       ab
       
       (a, b)
       lss: a
       sub(lss, b)
         (b, "")
          lss: ab, b

       Similar problem:
       1. Print permutation of a string
       2. Longest Increasing Subsequence 

       3. Facebook interview question

       URL: http://xfido.com/html/indexLongestIncreasingSubsequenceRecursion.html
     */
    public static void subSequence(List<String> ls, String s){
        if(s.length() > 0){
            Tuple<String, String> t = splitAt(1, s);
            List<String> lss = ls.stream().map(x -> x + t.x).collect(Collectors.toList());
            lss.add(t.x);
            lss.addAll(ls);
            pp(lss);
            subSequence(lss, t.y);
        }else{
            pl("beg");
            Collections.sort(ls);
            // pl(ls);
            pl("end");
        }
    }

    /**
       KEY: similar to facebook interview question
       Use iteration to get the length of k subsequence from a given string
       
       The problem is similar to following problem:
       
       Print the Longest increasing subsequence of an Integer array

       URL: http://xfido.com/html/indexLongestIncreasingSubsequenceRecursion.html
    */
    public static List<String> subSequence2(Integer k, String s){
        int len = s.length();
        List<String> ls = new ArrayList<>();
        for(int i = 0; i < len; i++){
            String sc = s.charAt(i) + "";
            List<String> lss = ls.stream().map(x -> x + sc).collect(Collectors.toList()); 
            if(ls.size() > 0)
                lss.addAll(ls);
            lss.add(sc);
            ls.clear();
            ls.addAll(lss);
        }
        return ls;
    }



    /**
    L[i] = 1 + Max(L[i-1]) where j < i && arr[j] < arr[i]

    [2][3][0][1]
     [arr, 4
        [arr, 1
        ]
        [arr, 2
              [arr, 1
              ]
        ]
        [arr, 3
              [arr, 1
              ]
              [arr, 2
                          [arr, 1
                          ]
              ]
        ]
     ]
    max=2 8 check=true

    */
    public static int LISRecursion_debug(Integer[] array, int len, int k) {
        if(len == 1)
            return 1;
        else {
            int max = 1;
            for(int i=1; i < len; i++) {
                int kk = 2*(k + 1);
                String s = "[" + i;
                leftPadPl(s, len(s) + kk, ' ');
                int m = LISRecursion_debug(array, i, kk);
                leftPadPl("]", len("]") + kk, ' ');
                //pp("array["+(i-1)+"]="+array[i-1]+" array["+(len-1)+"]="+array[len-1]+"\n");
                // pl("("+ array[i-1] + " , "+ array[len-1] + ")");
                if(array[i-1] < array[len-1])
                  max = Math.max(max, m) + 1;
                // pl("max=["+max+"]");
            }
            return max;
        }
    }

    /**
             3    4     1     2
             j_0  j_1   j_2  [endInx]
    */
    public static int LISRecursion_debug2(Integer[] arr, int endInx, int k){
        if(endInx == 0){
            return 1;
        }else{
            int max = 1;
            for(int j = 0; j < endInx; j++){
                String ss = "[" + arr[j];
                printBracket(ss, 2*k, ' ');
                int m = LISRecursion_debug2(arr, j, k + 1);
                printBracket("]", 2*k, ' ');
                if(arr[j] < arr[endInx])
                    max = Math.max(max, m) + 1;
            }
            return max;
        }
    }

    public static void permutation(Integer[] arr, List<Integer> ls){
    }


    public static Integer[] removeIndexArr(int inx, Integer[] arr){
        List<Integer> ls = arrayToList(arr);
        if(0 <= inx && inx < len(ls))
            ls.remove(inx);
        return listToArray(ls);
    }

    /**
            2 3
         3       2
       
    */
    public static void coinChange(Integer[] arr, List<Integer> ls){
        printArr(arr);
        pl("len=" + len(arr));
        pl(ls);
       if(len(arr) == 0){
           fl("printList");
           printList(ls);

           fl("end_printList");
       }else{

           for(int i = 0; i < len(arr); i++){
            Integer[] newArray = removeIndexArr(i, arr);
            coinChange(newArray, append(ls, arr[i]));
           }
       }
    }


    /**
        Find the longest increasing subsequence integers
        {2, 4, 1, 5} => 2->4->5
        {2, 4, 1, 2, 3} => 1->2->3
        L[i] = 1 + Max(L[i-1]) where j < i && arr[j] < arr[i]
    */
    public static int LISRecursion(Integer[] array, int len) {
        if(len == 1)
            return 1;
        else {
            int max = 1;
            for(int i=1; i < len; i++) {
                int m = LISRecursion(array, i);
                if(array[i-1] < array[len-1])
                    m = m + 1;
                max = Math.max(max, m);
            }
            pl("");
            return max;
        }
    }

    public static void LISTree(Integer[] array, int len) {
        if(len == 1) {
            //pl("["+array[len-1]+"]");
        } else {
            for(int i=1; i<len; i++) {
                LISTree(array, i);
                pl("["+array[i-1]+"]->["+array[len-1]+"]");
            }
        }
    }

    public static int LISRecursion2(Integer[] array, int len) {
        int max = 1;
        if(len == 1)
            return max;
        else {
            for(int i=1; i<len; i++) {
                int m = LISRecursion2(array, i);
                pp("array["+(i-1)+"]="+array[i-1]+" array["+(len-1)+"]="+array[len-1]+"\n");
                if(array[i-1] < array[len-1])
                    m = m + 1;
                max = Math.max(max, m);
            }
            pl("-----------------------");
        }
        return max;
    }

    /**
    <pre>
    {@literal
       generate random [0...n-1], generate random list integer

       /Users/aaa/myfile/bitbucket/java/LongestIncreasingSubsequence.java
       Dynamic programming algorithm solves Longest Increasing Subsequence
       with complexity O(n^2)
    }
    {@code
    }
    </pre>
    */ 
    public static int LISDP(Integer[] array) {
        int len = array.length;
        // initialize array to 1
        int[] maxlist = new int[len];
        for(int i=0; i < len; i++)
            maxlist[i] = 1;

        for(int i=1; i < len; i++) {
            for(int j=0; j<i; j++) {       // j < i
                if(array[j] < array[i])    // [j] < [i]
                    maxlist[i] = Math.max(1 + maxlist[j], maxlist[i]);
            }
        }

        int m = Integer.MIN_VALUE;
        for(int i=0; i < len; i++) {
            if(maxlist[i] > m)
                m = maxlist[i];
        }
        return m;
    }

    

    static void test0() {
        Aron.beg();
        Integer[] array = {1, 0, 7, 2, 6};
        int len = array.length;
        Aron.printArray(array);
        int m = LISDP(array);

        pl("LISRecursion");
        int recm = LISRecursion(array, len);
        int len2 = array.length;
        fl("LISRecursion2");
        int recm2 = LISRecursion2(array, len2);
        pl("max LIS=["+m+"]");
        pl("max LISRecursion=["+recm+"]");
        pl("max LISRecursion2=["+recm2+"]");
        fl("LISTree");
        LISTree(array, len2);
        Aron.end();
    }

    static void test1() {
        Aron.beg();
        Integer[] array = {1, 0, 7, 2, 6};
        Aron.printArray(array);
        int len = array.length;
        int max = LISRecursion(array, len);

        pl("LISRecursion=["+ max +"]");
        Aron.end();
    }

    static void test2() {
        beg();
        {
            {
                Integer[] array = {1, 0, 7, 2, 6};
                Aron.printArray(array);
                int len = array.length;
                int max = LISRecursion2(array, len);
                pl("LISRecursion2 max=["+ max +"]" + "   check" + (max == 3));
            }
            {
                Integer[] array = {1, 2, 0};
                Aron.printArray(array);
                int len = array.length;
                int max = LISRecursion2(array, len);
                pl("LISRecursion2 max=["+ max +"]" + "   check" + (max == 2));
            }
        }
        end();
    }

    static void test3() {
        Aron.beg();

        Integer[] array = {1, 0, 7, 2, 6};
        int len = array.length;
        Aron.printArray(array);
        LISTree(array, len);

        Aron.end();
    }

    static void test4() {
        beg();
        {
            {
                int k = 0;
                Integer[] array = {1, 0, 7, 2, 6};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 1 check=" + (max == 3));
            }
            {
                int k = 0;
                Integer[] array = {1};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 2 check=" + (max == 1));
            }
            {
                int k = 0;
                Integer[] array = {1, 2};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 3 check=" + (max == 2));
            }
            {
                int k = 0;
                Integer[] array = {1, 2, 0};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 4 check=" + (max == 2));
            }
            {
                int k = 0;
                Integer[] array = {4, 1, 2};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 5 check=" + (max == 2));
            }
            {
                int k = 0;
                Integer[] array = {4, 1, 2, 5};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 6 check=" + (max == 3));
            }
            {
                int k = 0;
                Integer[] array = {4, 1, 2, 5, 3};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 7 check=" + (max == 3));
            }
            {
                int k = 0;
                Integer[] array = {2, 3, 0, 1};
                int len = array.length;
                printArray(array);
                int max = LISRecursion_debug(array, len, k);
                pl("max=" + max + " 8 check=" + (max == 2));
            }
	    {

	    }
        }
        end();
    }
    public static void test5(){
        beg();
        {
            int k = 1;
            Integer[] array = {1, 2, 4, 3, 7};
            int len = array.length;
            printArray(array);
            pl("");

            String s = "[" + array[len-1];
            leftPadPl(s, len(s) + k, ' ');
            int max = LISRecursion_debug(array, len, k);
            leftPadPl("]", len("]") + k, ' ');

            pl("max=" + max + " 8 check=" + (max == 2));
        }
        {
            {
                fl("LISRecursion_debug2");
                Integer[] arr = {1, 2, 3};
                int endInx = arr.length - 1;
                int k = 1;
                String ss = "[" + arr[endInx];
                printBracket(ss, k, ' ');
                LISRecursion_debug2(arr, endInx, k);
                printBracket("]", k, ' ');
            }
        }    
        {
            {
                fl("removeIndexArr");
                Integer[] arr = {1, 2, 3};
                Integer[] expectedArray = {2, 3};
                Integer[] newArray = removeIndexArr(0, arr);
                t(newArray, expectedArray);
            }
            {
                fl("removeIndexArr");
                Integer[] arr = {1, 2, 3};
                Integer[] expectedArray = {1, 3};
                Integer[] newArray = removeIndexArr(1, arr);
                t(newArray, expectedArray);
            }
            {
                fl("removeIndexArr");
                Integer[] arr = {1, 2, 3};
                Integer[] expectedArray = {1, 2};
                Integer[] newArray = removeIndexArr(2, arr);
                t(newArray, expectedArray);
            }
            {
                fl("removeIndexArr");
                Integer[] arr = {1, 2, 3};
                Integer[] expectedArray = {1, 2, 3};
                Integer[] newArray = removeIndexArr(3, arr);
                t(newArray, expectedArray);
            }
        }
        {
            {
                fl("coinChange");
                Integer[] arr = {1};
                List<Integer> ls = new ArrayList<Integer>(); 
                coinChange(arr, ls);
            }
        }
       end();
    }
    /**
           (0, 1) (0, 1) (0, 1)  = 2^3
           perm(1) 
           perm(2) 
           perm(3) 
           perm(4)
    */
    public static void LIS_test(){
        beg();
        {
            {
                fl("LIS 0");
                Integer[] arr = {2, 2};
                Integer[] expectedArr = {};
                Integer[] maxArr = LIS(arr);
                printArray(maxArr);
                Integer max = maxList(maxArr);
                fl("");
                pl("max=" + maxList(maxArr));
                t(max, 1);
            }
            {
                fl("LIS 1");
                Integer[] arr = {2, 3, 0, 1, -1, 3, 0, 4, 5};
                Integer[] expectedArr = {};
                Integer[] maxArr = LIS(arr);
                printArray(maxArr);
                Integer max = maxList(maxArr);
                fl("");
                pl("max=" + maxList(maxArr));
                t(max, 5);
            }
            {
                fl("LIS 2");
                Integer[] arr = {3, 2, 1, 0};
                Integer[] maxArr = LIS(arr);
                printArray(maxArr);
                fl("");
                pl("max=" + maxList(maxArr));
                Integer max = maxList(maxArr);
                t(max, 1);
            }
            {
                fl("LIS 3");
                Integer[] arr = {3, 2, 2, 1, 0};
                Integer[] maxArr = LIS(arr);
                printArray(maxArr);
                fl("");
                pl("max=" + maxList(maxArr));
                Integer max = maxList(maxArr);
                t(max, 1);
            }
            {
                fl("LIS 4");
                Integer[] arr = {3, 2, 2, 1, 0, 0, 1, 2, 3, 4, 5};
                Integer[] maxArr = LIS(arr);
                printArray(maxArr);
                fl("");
                pl("max=" + maxList(maxArr));
                Integer max = maxList(maxArr);
                t(max, 6);
            }
        }
        {
            {
                fl("perm5 1");
                String s = "";
                perm5("", s);
            }
            {
                fl("perm5 2");
                String s = "a";
                perm5("", s);
            }
            {
                fl("perm5 3");
                String s = "ab";
                perm5("", s);
            }
            {
                fl("perm5 4");
                String s = "abc";
                perm5("", s);
            }
        } 
        {
            {
                fl("perm6");
                int[] arr = {1, 2, 3};
                int k = 0;
                perm6(arr, k);
            }
        }
        {
            {
                fl("chooseK 1");
                String s = "";
                chooseK("", s, 0);
            }
            {
                fl("chooseK 2");
                String s = "a";
                chooseK("", s, 1);
            }
            {
                fl("chooseK 3");
                String s = "ab";
                chooseK("", s, 1);
            }
            {
                fl("chooseK 4");
                String s = "abc";
                chooseK("", s, 2);
            }
            {
                fl("chooseK 5");
                String s = "abc";
                chooseK("", s, 3);
            }
            {
                fl("chooseK 5");
                String s = "abc";
                chooseK("", s, 1);
            }
            {
                fl("chooseK 6");
                String s = "abcd";
                chooseK("", s, 2);
            }
            {
                fl("chooseK 7");
                String s = "1234";
                chooseK("", s, 2);
            }
        }
        {
            {
                fl("chooseK2 1");
                String s = "1234";
                Set<String> set = new HashSet<>();
                chooseK2("", s, 2, set);
                pl(set);
            }
        }
        {
            {
                fl("maxConn 1");
                int[][] arr = {
                    { 1,   0,   0,  1},
                    { 1,   1,   0,  1},
                    { 0,   1,   0,  1},
                    { 0,   1,   1,  0}
                };                                
                int h = 0, w = 0;
                int retMax = connectIsland(arr, h, w);
                t(retMax, 6);

            }
            {
                fl("maxConn 2");
                int[][] arr = {
                    { 1,   1},
                    { 1,   1}
                };                                
                int h = 0, w = 0;
                int retMax = connectIsland(arr, h, w);
                t(retMax, 4);
            }
            {
                fl("maxConn 3");
                int[][] arr = {
                    {1, 1, 0, 0},
                    {1, 1, 0, 0},
                    {1, 1, 0, 0},
                    {1, 1, 0, 0},
                    {1, 1, 0, 0},
                };

                int h = 0, w = 0;
                int retMax = connectIsland(arr, h, w);
                t(retMax, 10);
            }
            {
                fl("maxConn 3");
                int[][] arr = {
                    { 1,   1,   0,  0},
                    { 1,   1,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = connectIsland(arr, h, w);
                t(retMax, 4);
            }
            {
                fl("maxConnection 1");
                int[][] arr = {
                    { 0,   0,   0,  1},
                    { 1,   1,   0,  1},
                    { 0,   1,   0,  1},
                    { 0,   1,   1,  0}
                };                                
                int retMax = maxConnection(arr);
                t(retMax, 5);
            }
            {
                fl("maxConnection 2");
                int[][] arr = {
                    { 0,   0,   0,  1},
                    { 0,   0,   0,  1},
                    { 0,   1,   0,  1},
                    { 0,   1,   1,  0}
                };                                
                int retMax = maxConnection(arr);
                t(retMax, 3);
            }
            {
                fl("maxConnection 3");
                int[][] arr = {
                    { 1,   1,   0,  0},
                    { 1,   1,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int retMax = maxConnection(arr);
                t(retMax, 4);
            }
        }
        {
            {
                fl("maxConn2 1");
                int[][] arr = {
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn2(arr, h, w);
                t(retMax, 1);
            }
            {
                fl("maxConn2 2");
                int[][] arr = {
                    { 1,   1,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn2(arr, h, w);
                t(retMax, 2);
            }
            {
                fl("maxConn2 3");
                int[][] arr = {
                    { 1,   0,   0,  0},
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn2(arr, h, w);
                t(retMax, 2);
            }
            {
                fl("maxConn2 4");
                int[][] arr = {
                    { 1,   1,   0,  0},
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn2(arr, h, w);
                t(retMax, 3);
            }
            {
                fl("maxConn2 44");
                int[][] arr = {
                    { 1,   1,   0,  0},
                    { 1,   1,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = connectIsland(arr, h, w);
                t(retMax, 4);
            }
            {
                fl("maxConn2 45");
                int[][] arr = {
                    { 1,   1,   0,  0},
                    { 1,   1,   0,  0},
                    { 0,   0,   1,  1},
                    { 0,   0,   1,  0}
                };                                
                int h = 0, w = 0;
                int retMax = connectIsland(arr, h, w);
                t(retMax, 4);
            }
            {
                fl("maxConn2 5");
                int[][] arr = {
                    { 0,   1,   0,  0},
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn2(arr, h, w);
                t(retMax, 0);
            }
        }
        {
            {
                fl("maxConn3 1");
                int[][] arr = {
                    { 0,   1,   0,  0},
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn3(arr, h, w);
                t(retMax, 0);
            }
            {
                fl("maxConn3 2");
                int[][] arr = {
                    { 1,   1,   0,  0},
                    { 1,   1,   0,  0},
                    { 0,   0,   1,  0},
                    { 0,   0,   0,  1}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn3(arr, h, w);
                t(retMax, 6);
            }
            {
                fl("maxConn3 3");
                int[][] arr = {
                    { 1,   0,   0,  1},
                    { 0,   1,   0,  1},
                    { 0,   0,   1,  1},
                    { 0,   0,   0,  1}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn3(arr, h, w);
                t(retMax, 7);
            }
            {
                fl("maxConn3 4");
                int[][] arr = {
                    { 1,   0,   1,  0},
                    { 0,   1,   0,  0},
                    { 1,   0,   1,  0},
                    { 0,   0,   0,  1}
                };                                
                int h = 0, w = 0;
                int retMax = maxConn3(arr, h, w);
                t(retMax, 6);
            }
        }
        {
            {
                fl("connectedIsland3d 1");
                int[][][] arr = {
                 {
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 },
                 {
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 }
                };                                
                int h = 0, w = 0, d = 0;
                int retMax = connectedIsland3d(arr, h, w, d);
                t(retMax, 0);
            }
            {
                fl("connectedIsland3d 2");
                int[][][] arr = {
                 {
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 },
                 {
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 }
                };                                
                int h = 0, w = 0, d = 0;
                int retMax = connectedIsland3d(arr, h, w, d);
                t(retMax, 1);
            }
            {
                fl("connectedIsland3d 3");
                int[][][] arr = {
                 {
                    { 1,   0,   0,  0},
                    { 0,   1,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 },
                 {
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 }
                };                                
                int h = 0, w = 0, d = 0;
                int retMax = connectedIsland3d(arr, h, w, d);
                t(retMax, 2);
            }
            {
                fl("connectedIsland3d 4");
                int[][][] arr = {
                 {
                    { 1,   0,   0,  0},
                    { 0,   1,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 },
                 {
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 }
                };                                
                int h = 0, w = 0, d = 0;
                int retMax = connectedIsland3d(arr, h, w, d);
                t(retMax, 3);
            }
            {
                fl("connectedIsland3d 5");
                int[][][] arr = {
                 {
                    { 1,   0,   0,  0},
                    { 0,   1,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 },
                 {
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 },
                 {
                    { 1,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0},
                    { 0,   0,   0,  0}
                 }
                };                                
                int h = 0, w = 0, d = 0;
                int retMax = connectedIsland3d(arr, h, w, d);
                t(retMax, 4);
            }
        }
        end();
    }

    /**
                  a b c
          b c      a c     a b
      b     c    a   c   a     b 

    */
    public static void perm5(String pre, String str){
            if(str.length() == 0){
                pl(pre);
            }else{
                for(int i = 0; i < str.length(); i++){
                    String s = removeIndex(i, str);
                    String cs = str.charAt(i) + "";
                    perm5(pre + cs, s); 
                }
            }
    }

    /**
                     n!
        P(n, k) =  - - - - 
                    (n-k)!

                       n!
        C(n, k) = - - - - - - 
                    (n-k)! k!

    */
    public static void chooseK(String pre, String str, int k){
            if(pre.length() == k){
                pl(pre);
            }else{
                for(int i = 0; i < str.length(); i++){
                    String s = removeIndex(i, str);
                    String cs = str.charAt(i) + "";
                    chooseK(pre + cs, s, k); 
                }
            }
    }
    public static void chooseK2(String pre, String str, int k, Set<String> set){
            if(pre.length() == k){
                String s = sortStr(pre);
                set.add(s);
            }else{
                for(int i = 0; i < str.length(); i++){
                    String s = removeIndex(i, str);
                    String cs = str.charAt(i) + "";
                    chooseK2(pre + cs, s, k, set); 
                }
            }
    }

    /**

    */
    public static void perm6(int[] arr, int k){
        int len = arr.length;
        if(k == len){
            fl("");
            printArrint(arr);
        }else{
            for(int i = k; i < len; i++){
               swap(arr, i, k); 
               perm6(arr, k+1);
               swap(arr, i, k);
            }
        }
    }


    public static int maxConn2(int[][] arr, int h, int w){
        int height = arr.length; 
        int width = arr[0].length;
        int s0 = 0, s1 = 0, s2 = 0, s3 = 0;
        int ret = 0;

        if (arr[h][w] == 1){
            int tmp = arr[h][w];
            arr[h][w] = 0;
            
            for(int i = -1; i <= 1; i++){
                for(int j = -1; j <= 1; j++){
                    if(abs(i) == 1 && abs(j) == 0 || abs(i) == 0 && abs(j) == 1){
                        if(0 <= h + i && h + i < height && 0 <= w + j && w + j < width){
                            ret += maxConn2(arr, h + i, w + j);   
                        }
                    }
                }
            }

            // if(h + 1 < height){
                // s0 = maxConn2(arr, h + 1, w);  // (h + 1, w + 0)  (1, 0)
            // }

            // if(h - 1 >= 0){
                // s1 = maxConn2(arr, h - 1, w);  // (h - 1, w + 0)  (-1, 0)
            // }

            // if(w + 1 < width){
                // s2 = maxConn2(arr, h, w + 1);  // (h + 0, w + 1)  (0, 1)
            // }

            // if(w - 1 >= 0){
                // s3 = maxConn2(arr, h, w - 1);  // (h + 0, w - 1)   (0, -1)
            // }

            ret += 1;
            // arr[h][w] = tmp;
        }
        return ret;
    }

    public static int maxConn3(int[][] arr, int h, int w){
        int height = arr.length; 
        int width = arr[0].length;
        int s0 = 0, s1 = 0, s2 = 0, s3 = 0;
        int ret = 0;

        if (arr[h][w] == 1){
            int tmp = arr[h][w];
            arr[h][w] = 0;
            
            for(int i = -1; i <= 1; i++){
                for(int j = -1; j <= 1; j++){
                    // if(abs(i) == 1 && abs(j) == 0 || abs(i) == 0 && abs(j) == 1){
                    if(abs(i) != 0 || abs(j) != 0){
                        if(0 <= h + i && h + i < height && 0 <= w + j && w + j < width){
                            ret += maxConn3(arr, h + i, w + j);   
                        }
                    }
                }
            }

            ret += 1;
            // arr[h][w] = tmp;
        }
        return ret;
    }
}
