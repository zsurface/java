import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class ValidateBracket{
    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        List<String> list = readFile("/tmp/f1.x");
        p(validateBracket(list) == true);

        sw.printTime();
        end();
    }
    public static void test2(){
        beg();

        String[] arr = {""};
        List<String> list = Arrays.asList(arr); 
        p(validateBracket(list) == true);

        end();
    }
    public static void test3(){
        beg();

        String[] arr = {
            "{"};
        List<String> list = Arrays.asList(arr); 
        p(validateBracket(list) == false);

        end();
    }
    public static void test4(){
        beg();

        String[] arr = {
            "{",
            "{",
            "}",
            "}"
            };
        List<String> list = Arrays.asList(arr); 
        p(validateBracket(list) == true);

        end();
    }
    public static void test5(){
        beg();

        String[] arr = {
            "{",
            "}",
            "}"
            };
        List<String> list = Arrays.asList(arr); 
        p(validateBracket(list) == false);

        end();
    }
} 

