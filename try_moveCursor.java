import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_moveCursor{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();
        {
            fl("move me 1");
            moveCursorRight(10);
            System.out.print("|");
        }
        {
            fl("move me 2");
            moveCursorRight(10);
            moveCursorLeft(5);
            System.out.print("|");
        }
        {
            fl("move me 3");
            moveCursorRight(10);
            System.out.print("|");
            moveCursorRight(20);
            System.out.print("|");
        }
        end();
    }
	public static void moveCursorLeft(int n){
        char c = (char)27;
		var s = "[" + n + "D";
        System.out.print(c + s);
	}
    // Move cursor to the right =>
	public static void moveCursorRight(int n){
        char c = (char)27;
		var s = "[" + n + "C";
        System.out.print(c + s);
		
	}
} 

