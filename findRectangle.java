import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Coord{
    public int x1;
    public int y1;
    public int x2;
    public int y2;
    public void print(){
        Print.p("x1=" + x1);
        Print.p("y1=" + y1);
        Print.p("x2=" + x2);
        Print.p("y2=" + y2);
    }
}
public class findRectangle{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
//        {        
//            int[][] arr = {
//                { 1,   1,   1,  1},
//                { 1,   1,   1,  1},
//                { 1,   1,   1,  1},
//                { 1,   1,   1,  0},
//            };
//            Coord coord = findRectangle(arr);
//            coord.print();
//            Print.fl();
//            Print.p("x1=" + 3);
//            Print.p("y1=" + 3);
//            Print.p("x2=" + 3);
//            Print.p("y2=" + 3);
//            Print.fl();
//            Print.fl();
//        }
//        {        
//            int[][] arr = {
//                { 0,   0,   1,  1},
//                { 0,   0,   1,  1},
//                { 0,   0,   1,  1},
//                { 0,   0,   1,  1},
//            };
//            Coord coord = findRectangle(arr);
//            coord.print();
//            Print.fl();
//            Print.p("x1=" + 0);
//            Print.p("y1=" + 0);
//            Print.p("x2=" + 1);
//            Print.p("y2=" + 3);
//            Print.fl();
//            Print.fl();
//        }
//        {        
//            int[][] arr = {
//                { 0,   0,   1,  1},
//                { 0,   0,   1,  1},
//                { 0,   0,   1,  1},
//                { 1,   1,   1,  1},
//            };
//            Coord coord = findRectangle(arr);
//            coord.print();
//            Print.fl();
//            Print.p("x1=" + 0);
//            Print.p("y1=" + 0);
//            Print.p("x2=" + 1);
//            Print.p("y2=" + 2);
//            Print.fl();
//            Print.fl();
//        }
        Aron.beg();
        {        
            int[][] arr = {
                { 0,   0,   1,  0},
                { 0,   0,   1,  1},
                { 1,   1,   0,  0},
                { 0,   1,   0,  0},
            };
            List<Coord> list = findRectangle(arr);
            for(Coord c: list){
                c.print();
            }
            Print.fl("t1"); 
        }
        {        
            int[][] arr = {
                { 0,   0,   0,  0},
                { 0,   0,   0,  0},
                { 0,   0,   0,  0},
                { 0,   0,   0,  0},
            };
            List<Coord> list = findRectangle(arr);
            for(Coord c: list){
                c.print();
            }
            Print.fl("t2"); 
        }
        {        
            int[][] arr = {
                { 0,   1,   1,  1},
                { 1,   1,   1,  1},
                { 1,   1,   1,  1},
                { 1,   1,   1,  0},
            };
            List<Coord> list = findRectangle(arr);
            for(Coord c: list){
                c.print();
            }
            Print.fl("t3"); 
        }
//        {        
//            int[][] arr = {
//                { 0,   0,   0,  1},
//                { 0,   0,   0,  1},
//                { 0,   0,   0,  1},
//                { 0,   0,   0,  1},
//            };
//            Coord coord = findRectangle(arr);
//            coord.print();
//            Print.fl();
//            Print.p("x1=" + 0);
//            Print.p("y1=" + 0);
//            Print.p("x2=" + 2);
//            Print.p("y2=" + 3);
//            Print.fl();
//            Print.fl();
//        }
//        {        
//            int[][] arr = {
//                { 0,   0,   0,  0},
//                { 0,   0,   0,  0},
//                { 0,   0,   0,  0},
//                { 0,   0,   0,  0},
//            };
//            Coord coord = findRectangle(arr);
//            coord.print();
//            Print.fl();
//            Print.p("x1=" + 0);
//            Print.p("y1=" + 0);
//            Print.p("x2=" + 4);
//            Print.p("y2=" + 4);
//            Print.fl();
//            Print.fl();
//        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        Given two dim array, find all the rectangles 

        Test File: /Users/cat/myfile/bitbucket/java/findRectangle.java
    }
    {@code
        class Coord{
            public int x1;
            public int y1;
            public int x2;
            public int y2;
            public void print(){
                Print.p("x1=" + x1);
                Print.p("y1=" + y1);
                Print.p("x2=" + x2);
                Print.p("y2=" + y2);
            }
        }

        int[][] arr = {
            { 0,   0,   1,  0},
            { 0,   0,   1,  1},
            { 1,   1,   0,  0},
            { 0,   1,   0,  0},
        };
        List<Coord> list = findRectangle(arr);
        for(Coord c: list){
            c.print();
        }

        x1 = 0
        y1 = 0
        x2 = 1 
        y2 = 1 

        x1 = 3 
        y1 = 0
        x2 = 3 
        y2 = 0 

        x1 = 0 
        y1 = 3 
        x2 = 0 
        y2 = 3 

        x1 = 3 
        y1 = 3 
        x2 = 3 
        y2 = 3 

    }
    </pre>
    */ 
    public static List<Coord> findRectangle(int[][] arr){
        List<Coord> list = new ArrayList<>(); 
        if(arr != null && arr.length > 0){
            int ncol = arr.length;
            int nrow = arr[0].length;
            for(int c = 0; c < ncol; c++){
                for(int r = 0; r < nrow; r++){
                    if(arr[c][r] == 0){

                        int rn = 0;
                        int rr = r;
                        while(rr < nrow && arr[c][rr] == 0){
                            rn ++;
                            rr++;
                        }

                        int cn = 0;
                        int cc = c;
                        while(cc < ncol && arr[cc][r] == 0){
                                cn++;
                                cc++;
                        }


                        Coord coord = new Coord();
                        coord.x1 = r;
                        coord.y1 = c;
                        coord.x2 = r + rn - 1;
                        coord.y2 = c + cn - 1;


                        list.add(coord);

                        Print.fl("t1");
                        coord.print();
                        Print.fl("t2");
                        for(int y = coord.y1; y <= coord.y2; y++){
                            for(int x = coord.x1; x <= coord.x2; x++){
                                arr[y][x] = 1;
                            }
                        }
                    }
                }
            }
        }
        return list;
    }
} 

