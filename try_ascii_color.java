import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_ascii_color{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();

        // System.out.print("\u001B[0; 31m Hello \u001b[0;0m");
        System.out.println((char)27 + "[31mThis text would show up red" + (char)27 + "[0m");
        System.out.println((char)27 + "[38;5;206mHello 1" + (char)27 + "[0m");
        System.out.println((char)27 + "[38;5;0;0;33mHello 2" + (char)27 + "[0m");
        System.out.println((char)27 + "[38;5;0;33;44mHello 3" + (char)27 + "[0m");
        System.out.println((char)27 + "[38;5;33mHello 4" + (char)27 + "[0m");
        System.out.println((char)27 + "[38;5;45mHello 5" + (char)27 + "[0m");
        System.out.println((char)27 + "[38;5;95mHello 6" + (char)27 + "[0m");
        System.out.println((char)27 + "[38;5;93mHello 7" + (char)27 + "[0m");
        System.out.println((char)27 + "[226mThis text would show up red" + (char)27 + "[0m");
        System.out.println((char)27 + "[200mThis text would show up red" + (char)27 + "[0m");

        String forground = "[38;5;";
        for(int i = 0; i < 256; i++){
            String str = "[" + i + "]";
            int color = i;
            String s = (char)27 + forground + color + "m" + str + (char)27 + "[0m"; 
            System.out.print(s + " ");
            if((i + 1) % 10 == 0){
                System.out.println();
            }
        }
        end();
    }
} 

