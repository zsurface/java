import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
    Given two dimension m, n array which contains 0 or 1 and find the big connected blocks are 1 
    
    1 0 1 1 1
    1 1 0 0 1
    0 0 0 0 1 
    1 1 1 1 1

    return 10
*/
public class try_connected_island{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        int[][] arr2d = {
            { 0,   0,   0,  1},
            { 1,   1,   0,  1},
            { 0,   1,   0,  1},
            { 1,   1,   1,  0},
        };
        Aron.printArray2D(arr2d);
        int height = arr2d.length;
        int width = arr2d[0].length;
        int h = 0, w = 0; 

        int max = 0;
        for(int i=0; i<height; i++){
            for(int j=0; j<width; j++){
                int m = connectedIsland(arr2d, i, j, height, width);
                if (m > max)
                    max = m;
            }
        }
        Print.p("max=" + max);
        
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }

    /**
        [1][1]
            0, 0             
                 0 1  -> 1
                    0 2 -> 0
                    0 0 -> 0
                    -1 1 -> 0
                    2  1 -> 0

    */
    public static int connectedIsland(int[][] arr, int h, int w, int height, int width){
        if(arr != null && arr[h][w] == 1){
            arr[h][w] = 2;    
            int c =0, c1=0, c2=0, c3=0, c4=0;
            c = 1; 
            if(w + 1 < width)
                c1 = connectedIsland(arr, h, w+1, height, width);
            if(w - 1 >= 0)
                c2 = connectedIsland(arr, h, w-1, height, width);
            if(h + 1 < height)
                c3 = connectedIsland(arr, h + 1, w, height, width);
            if(h - 1 >= 0)
                c4 = connectedIsland(arr, h - 1, w, height, width);

            arr[h][w] = 1;
            return c + c1 + c2 + c3 + c4;
        }
        return 0;
    }
} 

