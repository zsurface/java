import static classfile.Print.*;
import static classfile.Aron.*;
import static classfile.Test.*;
import static classfile.Tuple.*;

import classfile.*;
import classfile.OneURL;
import classfile.Ut;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

import java.io.*;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URL;

/*
String u0 = "<a href='" + URL + "'>Link</a>" + "<br>";
String u1 = "<span>" + ls.get(1) + "</span><br>";
String u2 = "<span>" + ls.get(2) + "</span><br>";
String u3 = "<span>" + ls.get(3) + "</span><br>";
String u4 = "<span>" + ls.get(4) + "</span><br>";
String u5 = "<span>" + ls.get(5) + "</span><br>";
String u6 = "<span style='color:red;'>" + ls.get(6) + "</span>" + "<br><br>";
*/

/*
  TryScaleImage.java
  Read all images file a folder, and get the width and height of images
*/
/*
// MOVE: to $b/javalib/OneURL.java
class OneURL{
	public String partialURL = "";
	public String categoryCode = "";
	public String categoryName = "";
	public String cityCode = "";
	public String cityName = "";
	public String end = "";
	
	public OneURL(){
	}
	public OneURL(String partialURL, String categoryCode, String categoryName, String cityCode, String cityName, String end){
		this.partialURL = partialURL;
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
		this.cityCode = cityCode;
		this.cityName = cityName;
		this.end = end;
	}
	public String fullURL(){
		// String expectedStr = "http://www.vansky.com/info/ZFBG08.html?page=&location=CITY02&direct=&title=";
		String csStrHTML = this.categoryCode + ".html";
		return this.partialURL + csStrHTML + "?page=&location=" + this.cityCode + this.end;
	}
	@Override
	public String toString(){
		String s = "";
		String url = "URL=" + fullURL();
		String catCode = "categoryCode=" + categoryCode;
		String catName = "categoryName=" + categoryName;
		String cCode   = "cityCode=" + cityCode;
		String cName   = "cityName=" + cityName;
		s += url + "\n";
		s += catCode + "\n";
		s += catName + "\n";
		s += cCode + "\n";
		s += cName + "\n";
		return s;
	}
}
*/

class CityAndLink{
	String city;
	String url;
	public CityAndLink(String city, String url){
		this.city = city;
		this.url = url;
	}
	
	@Override
	public String toString(){
		return "toString";
	}
}
class PostItem{
	public String url;
	public String email;
	public String city;
	public String contactPerson;
	public String phone;
	public List<String> descList = new ArrayList<>();
	public Integer price;
	public List<String> imageList = new ArrayList<>();
	public PostItem(){
	}
	public PostItem(String url,
					String email,
					String city,
					String contactPerson,
					String phone,
					List<String> descList,
					Integer price,
					List<String> imageList
					){
		this.url = url;
		this.email = email;
		this.city = city;
		this.contactPerson = contactPerson;
		this.phone = phone;
		this.descList = descList;
		this.price = price;
		this.imageList = imageList;
	}
	@Override
	public String toString(){
		return "";
	}
}

// XXX: A simple Post Item class
class CategoryURL{
	public OneURL oneURL;
	public List<String> lsURL;
	public CategoryURL(OneURL oneURL, List<String> lsURL){
		this.oneURL = oneURL;
		this.lsURL = lsURL;
	}
	
	@Override
	public String toString(){
		String s1 = this.oneURL.toString();
		String s2 = "";
		for(String s : lsURL){
			s2 += s + "\n";
		}
		return s1 + s2;
	}
}

// KEY: Download vansky data, 
// Last Update:
// Tuesday, 08 March 2022 12:51 PST
public class VanSky2 {
	
    public static void main(String[] args) {
		StopWatch sw = new StopWatch();
		System.out.println("VanSky2.java");
		// test0_OneURL();
		// test1_OneURL();

		// test1_1();
		// test2();
		test3();
		sw.printSec();
    }

	/*
	  XXX

	  Get all posts URL from one Category   and    City
	                                ↑               ↑
									+ -> ZFBG08     |
									                + -> Richmond

	 */
	/*
	public static List<String> getAllPostURL(String url) throws IOException{
		List<String> ls = new ArrayList<>();
		// Document doc = Jsoup.connect(url).get();
		Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
		Elements adList = doc.select("meta[itemprop=mainEntityOfPage]");
		Elements partURL = doc.select("meta[itemprop=mainEntityOfPage]").select("meta[content]");
		// Elements partURL = doc.select("meta[content]");
		for(Element e : adList){
			ls.add(e.toString());
		}
		for(Element e : partURL){
			pl("url content=" + e.toString());
			pl("url text content=" + e.text());
			pl("url e.attr=" + e.attr("content"));
			pl("url e.attr abs:content=" + e.attr("abs:content"));
		}
		return ls;
	}
	*/
	
	/*
	  XXX

	  Get all posts URL from one Category   and    City
	                                ↑               ↑
									+ -> ZFBG08     |
									                + -> Richmond

	  List<String> ls = oneCategoryURL(oneURL.fullURL());
	 */
	public static List<String> oneCategoryURL(String url){
		List<String> ls = new ArrayList<>();
		try{
			// Document doc = Jsoup.connect(url).get();
			pl("getallposturl_new: url=" + url);
			Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
			Elements adList = doc.select("meta[itemprop=mainEntityOfPage]");
			Elements partURL = doc.select("meta[itemprop=mainEntityOfPage]").select("meta[content]");
			// Elements partURL = doc.select("meta[content]");
		
			for(Element e : adList){
				// ls.add(e.toString());
			}
			for(Element e : partURL){
				/*
				  pl("url content=" + e.toString());
				  pl("url text content=" + e.text());
				  pl("url e.attr=" + e.attr("content"));
				*/
				String postURL = e.attr("abs:content");
				pl("postURL=" + postURL);
				ls.add(postURL);
			}
		}catch(IOException e){
			pl(e.getMessage());
		}
		return ls;
	}

	/*
	  Get all categories and names
	  E.g.      ZFBG08 -> 出租
	 */
	public static Map<String, String> getCategoryMap(){
		Map<String, String> map = new HashMap<>();
		try{
			String bitbucket = getEnv("b");
			String indexHtml = bitbucket + "/" + "testfile/vansky" + "/" + "index.html";
			File input = new File(indexHtml);
			// Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
			Document doc = Jsoup.parse(input, "UTF-8", "http://x.com");
			Elements elm = doc.select("div.con").select("a[href]");
		   
			for(Element e : elm){
				/*
				pl("text=" + e.text());
				pl("toString=" + e.toString());
				pl("abs:href=" + e.attr("abs:href"));
				pl("href=" + e.attr("href"));
			    */
				String href = e.attr("href");
				String category = dropExt(takeName(href));
				pl("category=" + category + " -> " + e.text());
				map.put(category, e.text());
			}

		}catch (IOException e){
			e.printStackTrace();
		}
		return map;
    }
	
	public static Map<String, String> category(){
		Map<String, String> map = new HashMap<>();
		map.put("ZFBG08", "出租");
		map.put("ZFBG09", "求租");
		map.put("ZFBG05", "家庭旅馆");
		map.put("ZFBG06", "学生寄宿");
		map.put("ZFBG07", "停车仓储");
		map.put("ZFBG01", "地产经纪");
		map.put("ZFBG10", "地产出售");
		map.put("ZFBG11", "地产求购");
		map.put("ZFBG04", "验屋");
		map.put("ZFBG03", "建屋设计");
		map.put("ZFBG12", "商业租赁");
		map.put("ZFBG13", "商产买卖");
		map.put("ZFBG14", "农地买卖");
		map.put("zfbg02", "物业管理");
		return map;
	}
	
	public static Map<String, String> listCity(){
		Map<String, String> map = new HashMap<>();
		map.put("CITY01", "Vancouver");
		map.put("CITY02", "Richmond");
		map.put("CITY03", "Burnaby");
		map.put("CITY04", "Surrey");
		map.put("CITY05", "Coquitlam");
		map.put("CITY06", "New West");
		map.put("CITY07", "W. Vancouver");
		map.put("CITY08", "N. Vancouver");
		map.put("CITY09", "Delta");
		map.put("CITY10", "Port Coq.");
		map.put("CITY11", "Port Moody");
		map.put("CITY12", "Pitt Meadows");
		map.put("CITY13", "Langley");
		map.put("CITY14", "White Rock");
		map.put("CITY15", "Maple Ridge");
		map.put("CITY16", "Anmore");
		map.put("CITY17", "Belcarra");
		map.put("CITY18", "Whistler");
		map.put("CITY19", "Squamish");
		map.put("CITY20", "Mission");
		map.put("CITY21", "Abbotsford");
		map.put("CITY22", "Chilliwack");
		map.put("CITY23", "Kent");
		map.put("CITY24", "Hope");
		map.put("CITY25", "Kelonwa");
		map.put("CITY30", "Other");
		return map;
	}

	// city : CITY01-CITY25, CITY30 other
	public static List<String> geneNpagefromCity(Integer nPage, Map<String, String> map){
		List<String> urlList = new ArrayList<String>();
		
		for(int i = 1; i <= nPage; i++){
			String left = "http://www.vansky.com/info/ZFBG08.html?page=";
			String right = "&location=CITY02&direct=&title=";
			String furl = left + intToStr(i) + right;
			urlList.add(furl);
		}
		
		String left = "http://www.vansky.com/info/ZFBG08.html?page=&location=";
		String end = "&direct=&title=";
		for(Map.Entry<String, String> entry : map.entrySet()){
			String cityCode = entry.getKey();
			String fullURL = left + cityCode + end;
			String city = entry.getValue();
		}
		return urlList;
	}

	public static List<CategoryURL> getAllCategoryURL(){
			fl("oneCategoryURL 1");
			List<CategoryURL> lt = new ArrayList<>();
			String partialURL = "http://www.vansky.com/info/";
			// String categoryName = "ZFBG08";
			// String cityName = "";
			String end = "&direct=&title=";
			Map<String, String> map = listCity();
			// Map<String, String> cmap = category();
			Map<String, String> cmap = getCategoryMap();
			List<OneURL> urlList = new ArrayList<>();
			int count = 0;
			for(Map.Entry<String, String> catEntry : cmap.entrySet()){
				for(Map.Entry<String, String> entry : map.entrySet()){
					String cityCode = entry.getKey();
					String cityName = entry.getValue();
					String cateCode = catEntry.getKey();
					String categoryName = catEntry.getValue();
					OneURL oneURL = new OneURL(partialURL, cateCode, categoryName, cityCode, cityName, end);
					// pl(entry.getKey() + "  " + entry.getValue() + " => " + oneURL.fullURL());
					// pl(oneURL.fullURL());
					// pl(oneURL.toString());
					urlList.add(oneURL);
					
					List<String> ls = oneCategoryURL(oneURL.fullURL());
					CategoryURL catURL = new CategoryURL(oneURL, ls);
					lt.add(catURL);
					
// 				 	count++;
// 					if(count == 3){
// 						break;
// 					}
				}
				
// 				if(count == 3){
// 					break;
// 				}
			}

			OneURL url = urlList.get(1);
			pl(url.toString());

			// a list of URL
			List<String> ls = oneCategoryURL(url.fullURL());
			CategoryURL postItem = new CategoryURL(url, ls);

			return lt;
	}

	/**
	   XXX1:
	   htmlPath = http://www.vansky.com/info/ZFBG08.html?page=1&location=CITY02&direct=&title=
	   TODO: use OneURL replace htmlPath
	   NOTE: DO NOT USE IT, use downloadPostItem_new instead
	 */
	
    public static List<PostItem> downloadPostItem(String postURL){
        String vanskyURL = "http://www.vansky.com/info/";
        List<PostItem> listPost = new ArrayList<>();
		try{
			List<CityAndLink> list = getCityAndAdID(postURL);
			for(CityAndLink cityAndLink : list){
				// http://www.vansky.com/info/ + adfree/2215503.html
				String adURL = vanskyURL + cityAndLink.url;    
				pbl("adURL=" + adURL);
				writeFileAppend("/tmp/f99.x", list(cityAndLink.city + " " + cityAndLink.url));

				// XXX: all adURL under one City
				//                                Full_URL      City
				PostItem postItem = parseHtmlPostCity(adURL);
				if(containStr(postItem.city, "Richmond")){
					listPost.add(postItem);
				}
			}
		}
		catch(IOException io){
			io.printStackTrace();
		}
        return listPost;
    }

	public static PostItem getOnePost(String postURL){
		try{
			PostItem onePost = parseHtmlPostCity(postURL);
			return onePost;
		}catch(IOException e){
			pl(e.getMessage());
		}
		return null;

	}
	
	public static List<PostItem> downloadPostItem_new(OneURL oneURL){
        String vanskyURL = "http://www.vansky.com/info/";
        List<PostItem> listPost = new ArrayList<>();
		try{
			// List<CityAndLink> list = getCityAndAdID(htmlPath);
			List<CityAndLink> list = getCityAndAdID(oneURL.fullURL());
			for(CityAndLink cityAndLink : list){
				PostItem postItem = parseHtmlPostCity_new(oneURL);
				listPost.add(postItem);
			}
		}
		catch(IOException io){
			io.printStackTrace();
		}
        return listPost;
    }
	
	
	/*
    <city, adID>
    <city, adID>
	url = http://www.vansky.com/info/ZFBG08.html?page=1&location=CITY02&direct=&title=
	                                   ↑                           ↑
									   + -> Category               |
									                               + City = Richmond
									   
	XXX: Change the code to pass City and Category, and move the code to VanSky2.java
	*/
    public static List<CityAndLink> getCityAndAdID(String url) throws IOException{
        List<List<String>> cityAdList2d = new ArrayList<>();
		List<CityAndLink> cityAndLinkList = new ArrayList<>();
        // Document doc = Jsoup.connect(url).get();
		Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
        p("cityURL=" + url);
		writeFileList("/tmp/u1.x", list(url));
        Elements elementList = doc.select("tr.freeAdPadding").select("td.adph-font");
        Elements adFreeList = doc.select("tr.freeAdPadding").select("td.hidden-xs").select("div");
		// Elements adList = doc.select("tr[itemprop=itemListElement]");
		Elements adList = doc.select("meta[itemprop=mainEntityOfPage]");
		run("rm /tmp/ss.x");
		run("touch /tmp/ss.x");
		for(Element e : adList){
			writeFileAppendStr("/tmp/ss.x", e.toString() + "\n");
		}
	
	/*
        <td class="hidden-xs">
        <div class="adph-font" style="white-space: nowrap;" href="adfree/802548.html" target="_blank">
        房东
        </div>
        <div class="adsContentFont" style="color: #FA7011;">
        9小时前
        </div>
        </td>
	*/

        if(elementList == null){
            p("elementList is null");
            System.exit(0);
        }else{
            List<String> list = new ArrayList<>(); 

            for(int i=0; i<adFreeList.size(); i++){
                if(adFreeList.get(i).hasAttr("href")){
                    list.add(adFreeList.get(i).attr("href"));
				}
            }
            
            if(list.size() == elementList.size()){
                for(int i=0; i<list.size(); i++){
                    pbl(elementList.get(i).text());
                    pbl(list.get(i));
                    ArrayList<String> ll = new ArrayList<String>(); 

					// city : Richmond
                    ll.add(elementList.get(i).text());
					// adID link: adfree/802548.html
                    ll.add(list.get(i));
                    cityAdList2d.add(ll);
					cityAndLinkList.add(new CityAndLink( elementList.get(i).text(), list.get(i) ));
                }
            }else{
                pbl("ERROR=" + "Number of cities and Number of ads are not matched");
                System.exit(0);
            }
        }
        // return cityAdList2d;
		return cityAndLinkList;
    }
	
	/**
    * @param url  http://www.vansky.com/info/adfree/769897.html
    * @param city city in an Ad http://www.vansky.com/info/ZFBG08.html?page=1&location=&direct=&title=  
    * @return [city, name, phone, content]
	*
	* XXX:
	* TODO: Parse post to List<String>
	* 
    */
    public static PostItem parseHtmlPostCity(String url) throws IOException{
        pl("Fetching " + url);
		writeFileList("/tmp/uu.x", list(url));
		PostItem postItem = new PostItem();
        List<String> onePost = new ArrayList<String>(); 

        // Document doc = Jsoup.connect(url).get();
		Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
        Elements links = doc.select("a[href]");
        Elements media = doc.select("[src]");
        Elements imports = doc.select("link[href]");
        Elements vanContent = doc.select("div.ctent").select("p:not([class])");
        Element headerContent = doc.select("div.contentheader").select("div.cell-h").first();
        Elements contactConents = doc.select("div.contactrow").select("div.col-md-12");
		// Get price here
		
        Element email = doc.select("div.col-md-7").select("a[href]").first();
		Elements elm2 = doc.select("div.col-md-7");
		// Elements elm = doc.select("div.cell.ctent").select("table > tbody > tr > td");
		Elements elm = doc.select("div.cell.ctent").select("table > tbody > tr > td");
		if(elm != null){
			pl("elm.toString()=" + elm.toString());
			pl("elm.text()    =" + elm.text());
			writeFile("/tmp/log.html", "elm=" + elm.toString());
		}else{
			writeFile("/tmp/log.html", "elm is null");
		}

		for(Element e : elm2){
			pl(e.toString());
		}
		
		for(Element e : elm){
			pl("e.toString()=" + e.toString());
			pl("e.text()=" + e.text());
			String s = e.toString() + "\n";
			writeFileAppendStr("/tmp/td.x", "s=" + s);
		}
		writeFileAppendStr("/tmp/td.x", "size=" + elm.size() + "\n");
		List<String> lsPost = new ArrayList<>();
		for(Element e : elm){
			String s = e.text() + "\n";
			lsPost.add(e.text());
			writeFileAppendStr("/tmp/td1.x", "text=" + s);
		}
		
		// Last third one => price 2300
		if(len(lsPost) > 2){
			List<String> lsEnd = takeEnd(3, lsPost);
			String str = lsEnd.get(0);
			String priceStr = lsEnd.get(0);
			String prideDigit = charListToStr(takeWhile(x -> isDigit(x), strToListChar(priceStr)));
			writeFileAppendStr("/tmp/td2.x", "price=" + lsEnd.get(0) + "\n");
		}else{
			writeFileAppendStr("/tmp/td2.x", "price=" + "NO_PRICE" + "\n");
		}

        if(contactConents.size() > 1){
            String name = contactConents.get(0).text();
            String phone = extractPhoneNumber(contactConents.get(1).text());
			
			postItem.url = url;
            onePost.add(url + "\n");
			
			if(email != null){
				postItem.email = email.text();
                onePost.add(email.text() + "\n");
			}
            else{
				postItem.email = "NO_EMAIL";
                onePost.add("NO_EMAIL\n");
			}

            String content = "";
            for(Element element: vanContent){
                content += element.text();
            }
			/**
			http://www.vansky.com/info/adfree/2215191.html
			EMAIL
			Richmond 6小时前
			联系人: 林女士
			0604339455
			列治文全新豪宅独立出入一室一厅出租，独立厨房和卫浴
			850 [price]
			*/
            // onePost.add(city + "\n");
            onePost.add(name + "\n");
            onePost.add(phone + "\n");
            onePost.add(content + "\n");

			// postItem.city = city;
			postItem.contactPerson = name;
			postItem.phone = phone;
			postItem.descList.add(content);
			
			if(len(lsPost) > 2){
				List<String> lsEnd = takeEnd(3, lsPost);
				onePost.add(extractPrice(lsEnd.get(0)));
				postItem.price = strToIntegerNull(extractPrice(lsEnd.get(0)));
			}else{
				onePost.add("NO_PRICE\n");
				postItem.price = 0;
			}
        }
        return postItem;
    }
	
	public static PostItem parseHtmlPostCity_new(OneURL oneURL) throws IOException{
		String url = oneURL.fullURL();
		String city = oneURL.cityName;
		
        pl("Fetching %s..." + url);
		writeFileList("/tmp/uu.x", list(url));
		PostItem postItem = new PostItem();
        List<String> onePost = new ArrayList<String>(); 

        // Document doc = Jsoup.connect(url).get();
		Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
        Elements links = doc.select("a[href]");
        Elements media = doc.select("[src]");
        Elements imports = doc.select("link[href]");
        Elements vanContent = doc.select("div.ctent").select("p:not([class])");
        Element headerContent = doc.select("div.contentheader").select("div.cell-h").first();
        Elements contactConents = doc.select("div.contactrow").select("div.col-md-12");
		// Get price here
		
        Element email = doc.select("div.col-md-7").select("a[href]").first();
		Elements elm = doc.select("div.cell.ctent").select("table > tbody > tr > td");
		if(elm != null){
			pp("elm=" + elm.toString());
			writeFile("/tmp/log.html", "elm=" + elm.toString());
		}else{
			writeFile("/tmp/log.html", "elm is null");
		}
		
		for(Element e : elm){
			pp("e=" + e.toString());
			String s = e.toString() + "\n";
			writeFileAppendStr("/tmp/td.x", "s=" + s);
		}
		writeFileAppendStr("/tmp/td.x", "size=" + elm.size() + "\n");
		List<String> lsPost = new ArrayList<>();
		for(Element e : elm){
			String s = e.text() + "\n";
			lsPost.add(e.text());
			writeFileAppendStr("/tmp/td1.x", "text=" + s);
		}
		
		// Last third one => price 2300
		if(len(lsPost) > 2){
			List<String> lsEnd = takeEnd(3, lsPost);
			String str = lsEnd.get(0);
			String priceStr = lsEnd.get(0);
			String prideDigit = charListToStr(takeWhile(x -> isDigit(x), strToListChar(priceStr)));
			writeFileAppendStr("/tmp/td2.x", "price=" + lsEnd.get(0) + "\n");
		}else{
			writeFileAppendStr("/tmp/td2.x", "price=" + "NO_PRICE" + "\n");
		}

        if(contactConents.size() > 1){
            String name = contactConents.get(0).text();
            String phone = extractPhoneNumber(contactConents.get(1).text());
			
			postItem.url = url;
            onePost.add(url + "\n");
			
			if(email != null){
				postItem.email = email.text();
                onePost.add(email.text() + "\n");
			}
            else{
				postItem.email = "NO_EMAIL";
                onePost.add("NO_EMAIL\n");
			}

            String content = "";
            for(Element element: vanContent){
                content += element.text();
            }
			/**
			http://www.vansky.com/info/adfree/2215191.html
			EMAIL
			Richmond 6小时前
			联系人: 林女士
			0604339455
			列治文全新豪宅独立出入一室一厅出租，独立厨房和卫浴
			850 [price]
			*/
            onePost.add(city + "\n");
            onePost.add(name + "\n");
            onePost.add(phone + "\n");
            onePost.add(content + "\n");

			postItem.city = city;
			postItem.contactPerson = name;
			postItem.phone = phone;
			postItem.descList.add(content);
			
			if(len(lsPost) > 2){
				List<String> lsEnd = takeEnd(3, lsPost);
				onePost.add(extractPrice(lsEnd.get(0)));
				postItem.price = strToIntegerNull(extractPrice(lsEnd.get(0)));
			}else{
				onePost.add("NO_PRICE\n");
				postItem.price = 0;
			}
        }
        return postItem;
    }
	
	public static List<String> parseHtmlPostStr(String url) throws IOException{
		List<String> lsPost = new ArrayList<>();
		
        pl("Fetching %s..." + url);
		writeFileList("/tmp/uu.x", list(url));
		PostItem postItem = new PostItem();
        List<String> onePost = new ArrayList<String>(); 

        // Document doc = Jsoup.connect(url).get();
		Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
        Elements links = doc.select("a[href]");
        Elements media = doc.select("[src]");
        Elements imports = doc.select("link[href]");
        Elements vanContent = doc.select("div.ctent").select("p:not([class])");
        Element headerContent = doc.select("div.contentheader").select("div.cell-h").first();
        Elements contactConents = doc.select("div.contactrow").select("div.col-md-12");
		// Get price here
		
        Element email = doc.select("div.col-md-7").select("a[href]").first();
		Elements elm = doc.select("div.cell.ctent").select("table > tbody > tr > td");
		if(elm != null){
			pp("elm=" + elm.toString());
			writeFile("/tmp/log.html", "elm=" + elm.toString());
		}else{
			writeFile("/tmp/log.html", "elm is null");
		}
		
		for(Element e : elm){
			pp("e=" + e.toString());
			String s = e.toString() + "\n";
			writeFileAppendStr("/tmp/td.x", "s=" + s);
		}
		writeFileAppendStr("/tmp/td.x", "size=" + elm.size() + "\n");

		for(Element e : elm){
			String s = e.text() + "\n";
			lsPost.add(e.text());
			writeFileAppendStr("/tmp/td1.x", "text=" + s);
		}
		return lsPost;
    }

	/*
	   123xxx → 123
	 */
    public static String extractPrice(String s){
		return  charListToStr(takeWhile(x -> isDigit(x), strToListChar(s)));
	}
	
	public static void test0_OneURL(){
		// public OneURL(String partialURL, String categoryName, String cityCode, String cityName, String end){
		String partialURL = "http://www.vansky.com/info/";
		String categoryCode = "ZFBG08";
		String categoryName = "rental";
		String cityCode = "CITY02";
		String cityName = "";
		String end = "&direct=&title=";
		OneURL oneURL = new OneURL(partialURL, categoryCode, categoryName, cityCode, cityName, end);
		String str = oneURL.toString();
		String expectedStr = "http://www.vansky.com/info/ZFBG08.html?page=&location=CITY02&direct=&title=";
		pp(str);
		t(str, expectedStr);
	}
	
	public static void test1_OneURL(){
		beg();
		// public OneURL(String partialURL, String categoryName, String cityCode, String cityName, String end){
		{
			fl("ZFBG08  CITY02");
			String partialURL = "http://www.vansky.com/info/";
			String categoryCode = "ZFBG08";
			String categoryName = "rental";
			String cityCode = "CITY02";
			String cityName = "";
			String end = "&direct=&title=";
			OneURL oneURL = new OneURL(partialURL, categoryCode, categoryName, cityCode, cityName, end);
			String str = oneURL.toString();
			String expectedStr = "http://www.vansky.com/info/ZFBG08.html?page=&location=CITY02&direct=&title=";
			pp(str);
			t(str, expectedStr);
		}

		{
			fl("oneCategoryURL 0");
			String partialURL = "http://www.vansky.com/info/";
			// String categoryName = "ZFBG08";
			// String cityName = "";
			String end = "&direct=&title=";
			Map<String, String> map = listCity();
			// Map<String, String> cmap = category();
			Map<String, String> cmap = getCategoryMap();

			List<OneURL> urlList = new ArrayList<>();
			for(Map.Entry<String, String> catEntry : cmap.entrySet()){
				for(Map.Entry<String, String> entry : map.entrySet()){
					String cityCode = entry.getKey();
					String cityName = entry.getValue();
					String cateCode = catEntry.getKey();
					String categoryName = catEntry.getValue();
					OneURL oneURL = new OneURL(partialURL, cateCode, categoryName, cityCode, cityName, end);
					// pl(entry.getKey() + "  " + entry.getValue() + " => " + oneURL.fullURL());
					// pl(oneURL.fullURL());
					pl(oneURL.toString());
					urlList.add(oneURL);
				}
			}

			OneURL url = urlList.get(40);
			pl(url.toString());

			List<String> ls = oneCategoryURL(url.fullURL());
			fl("oneCategoryURL 00");
			pl(ls);

		}
		
		{
			fl("oneCategoryURL 1");
			String partialURL = "http://www.vansky.com/info/";
			// String categoryName = "ZFBG08";
			// String cityName = "";
			String end = "&direct=&title=";
			Map<String, String> map = listCity();
			// Map<String, String> cmap = category();
			Map<String, String> cmap = getCategoryMap();

			List<OneURL> urlList = new ArrayList<>();
			for(Map.Entry<String, String> catEntry : cmap.entrySet()){
				for(Map.Entry<String, String> entry : map.entrySet()){
					String cityCode = entry.getKey();
					String cityName = entry.getValue();
					String cateCode = catEntry.getKey();
					String categoryName = catEntry.getValue();
					OneURL oneURL = new OneURL(partialURL, cateCode, categoryName, cityCode, cityName, end);
					// pl(entry.getKey() + "  " + entry.getValue() + " => " + oneURL.fullURL());
					// pl(oneURL.fullURL());
					pl(oneURL.toString());
					urlList.add(oneURL);
				}
			}

			OneURL url = urlList.get(40);
			pl(url.toString());

			// a list of URL
			List<String> ls = oneCategoryURL(url.fullURL());
			CategoryURL postItem = new CategoryURL(url, ls);
			fl("oneCategoryURL 00");
			pl(ls);
			fl("postItem.toString()");
			pl(postItem.toString());
		}
		end();
	}

	public static void test2(){
		beg();
		{
			String bitbucket = getEnv("b");
			String catPath = bitbucket + "/" + "testfile" + "/" + "categoryurl.x";
			List<CategoryURL> ls = getAllCategoryURL();
			run("rm /tmp/cat.x");
			for(CategoryURL item : ls){
				writeFileAppend("/tmp/cat.x", list(item.toString() + "\n"));
			}
			
			if(isFile(catPath) == false){
				// Copy cat.x to $b/testfile/categoryurl.x
				run("cp /tmp/cat.x " + catPath);
			}
		}
		{
			
// 			List<CategoryURL> cls = take(1, drop(10, getAllCategoryURL()));
// 			for(CategoryURL cURL : cls){
// 				pl(cURL.toString());
				
				// for(String url : cURL.lsURL){
				// 	pl("url=" + url);
					// List<String> lsStr = parseHtmlPostStr(url);
					// pl(lsStr);
					// List<PostItem> ls = downloadPostItem_new(/* xxx7 */);
				// }

				/*
				  if(cURL.oneURL.cityName.equals("Richmond") && cURL.oneURL.categoryCode.equals("ZFBG08")){
				  List<PostItem> ls = downloadPostItem_new(cURL.oneURL);
				  }
				*/
			//}
		}
		end();
	}
	public static void test3(){
		beg();
		{
			String bitbucket = getEnv("b");
			String catPath = bitbucket + "/" + "testfile" + "/" + "categoryurl.x";
			List<List<String>> lss = splitFileWith("/tmp/cat.x");
			
			for(List<String> ls : lss){
				printList(ls);
				for(int i = 0; i < len(ls); i++){
					if(i <= 4){
						List<String> lt = splitStrWhenChar(ls.get(i), set('='));
						if(len(lt) >= 2){
							List<String> lr = drop(2, lt);
							String str = concat("", lr);
							pp("str=" + str);
						}
					}
				}
			}
			
		}
		end();
	}
	
	public static void test1_1(){
		beg();
		{
			fl("getAllCategoryURL test1_1()");
			List<CategoryURL> ls = take(1, getAllCategoryURL());
			for(CategoryURL item : ls){
				if(item.oneURL.cityName.equals("Richmond") && item.oneURL.categoryCode.equals("ZFBG08")){
					for(String u : item.lsURL){
						PostItem p = getOnePost(u);
						pl(p.toString());
					}					
				}
				// pl(item.toString());
				/*
				if(item.oneURL.cityName.equals("Richmond") && item.oneURL.categoryCode.equals("ZFBG08")){
					pl(item.toString());
				}
				*/
			}
		}		
		end();
	}
	
}
