import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_readfile1{
    public static void main(String[] args) {
        // test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        String name = "/tmp/t.x";
        ArrayList<String> list = new ArrayList<>();
        readFile(name, list);

        writeToFile("/tmp/t1.x", list);


        ArrayList<String> pr = prefix("dog");
        Print.p(pr);
        ArrayList<String> su = suffix("dog");
        Print.p(su);

        Print.p(list);

        Aron.end();
    }
    public static ArrayList<String> prefix(String s){
        ArrayList<String> list = new ArrayList<>();
        for(int i=1; i<=s.length(); i++){
            list.add(s.substring(0, i));
        }
        return list;
    }
    public static ArrayList<String> suffix(String s){
        ArrayList<String> list = new ArrayList<>();
        int len = s.length();
        for(int i=1; i<len; i++){
            list.add(s.substring(i, len)); 
        }
        return list;
    }
    public static void test1(){
        Aron.beg();
        List<Integer> list = Arrays.asList(1, 2, 3);

        Print.p(Aron.take(1, list)); 
        Print.p(Aron.drop(1, list)); 
        Print.p(Aron.take(1, "dog")); 
        Print.p(Aron.drop(1, "dog")); 

        Aron.end();
    }

    public static void writeToFile(String name, ArrayList<String> list){
        if(name != null && list != null){
            try{
                BufferedWriter bw = new BufferedWriter(new FileWriter(name));
                for(String s : list){
                    bw.write(s + "\n");
                }
                bw.close();
            }catch(IOException io){
                io.printStackTrace();
            }
        }
    }

    public static void readFile(String name, ArrayList<String> list){
        try{
            if(name != null && list != null){
                BufferedReader br = new BufferedReader(new FileReader(name));        
                String line = null;
                while( (line = br.readLine()) != null){
                    list.add(line);
                }
            }
        }catch(IOException io){
            io.printStackTrace();
        }
    }
} 

