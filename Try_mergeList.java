import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class Try_mergeList{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

	{
	    int[] arr1 = {1};
	    int[] arr2 = {2};
	    
	    int[] arr = mergeList(arr1, arr2);
	    p(arr);
	}
	{                                      
	    int[] arr1 = {};                  	
	    int[] arr2 = {3};                  	
                                       	
	    int[] arr = mergeList(arr1, arr2); 
	    p(arr);                            	
	}
        {                                      
	    int[] arr1 = {1, 4, 9};                    
	    int[] arr2 = {2, 4, 9};                  
	                                       
	    int[] arr = mergeList(arr1, arr2); 
	    p(arr);                            
	}
	
        sw.printTime();
        end();
    }
    public static int[] mergeList(int[] arr1, int[] arr2){
	int[] arr = null;
	if(arr1 != null && arr2 != null){
	    int len1 = arr1.length;
	    int len2 = arr2.length;
	    int x1 = 0;
	    int x2 = 0;
	    int k = 0;
	    arr = new int[len1 + len2];

	    while(x1 < len1 || x2 < len2){
		if(x1 >= len1){
		    arr[k] = arr2[x2];
		    x2++;
		}else if(x2 >= len2){
		    arr[k] = arr1[x1];
		    x1++;
		}else {
		    if(arr1[x1] <= arr2[x2]){
			arr[k] = arr1[x1];
			x1++;
		    }else{
			arr[k] = arr2[x2];
			x2++;
		    }
		}
		k++;
	    }
		
	}
	return arr;
    }
} 

