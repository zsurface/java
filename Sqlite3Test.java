import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

/**
   
   KEY: sqlite java, java sqlite, connect sqlite, sqlite3
   
   1. Download Sqlite JDBC driver(jar) to -classpath
   
   https://github.com/xerial/sqlite-jdbc#usage

   "CREATE TABLE IF NOT EXISTS ShellHistory (id INTEGER PRIMARY KEY AUTOINCREMENT, shcmd TEXT)" 
   
 */
public class Sqlite3Test {
  public static void main(String[] args) {
      batchInsection();
  }

  public static void slowInsection() {
    String dbfile = "/Users/cat/myfile/bitbucket/testfile/ShellHistory.db";
    String hisFile = "/Users/cat/myfile/bitbucket/shell/dot_bash_history";
    List<String> histList = readHistoryFile(hisFile);
      
    Connection connection = null;
    try {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:" + dbfile);

      Statement statement = connection.createStatement();
      statement.setQueryTimeout(30);  // set timeout to 30 sec.

      statement.executeUpdate("DROP TABLE IF EXISTS ShellHistory");
      statement.executeUpdate("CREATE TABLE IF NOT EXISTS ShellHistory (id INTEGER PRIMARY KEY AUTOINCREMENT, shcmd TEXT)");
      /*
      statement.executeUpdate("insert into person values(1, 'leo')");
      statement.executeUpdate("insert into person values(2, 'yui')");
      */

      ResultSet rs = statement.executeQuery("SELECT * from ShellHistory");
      while(rs.next()) {
        // read the result set
        System.out.println("shcmd = " + rs.getString("shcmd"));
        System.out.println("id = " + rs.getInt("id"));
      }

      StopWatch sw = new StopWatch();

      for(String s : histList){
	  // KEY: escape special character ' => '',  ' => \' does not work
	  String ns = replaceStr(s, "'", "''");
	  // String ns = " ''kkk''"; 
	  String sql_cmd = "INSERT INTO ShellHistory(shcmd) VALUES('" + ns + "')";
	  // p("sql_cmd=" + sql_cmd);
	  statement.executeUpdate(sql_cmd);
      }

      sw.print();
    }
    catch(SQLException e) {
      // if the error message is "out of memory",
      // it probably means no database file is found
      System.err.println(e.getMessage());
    }
    finally {
      try {
        if(connection != null)
          connection.close();
      }
      catch(SQLException e) {
        // connection close failed.
        System.err.println(e.getMessage());
      }
    }
  }

  /**
     KEY: batch insection
     // https://stackoverflow.com/questions/12095176/optimizing-batch-inserts-sqlite
  */
  public static void batchInsection(){
    String dbfile = "/Users/cat/myfile/bitbucket/testfile/ShellHistory.db";
    // String hisFile = "/Users/cat/myfile/bitbucket/shell/dot_bash_history";
    String hisFile = "/Users/cat/myfile/bitbucket/shell/dot_bash_history_test";
    List<String> histList = readHistoryFile(hisFile);
    p(histList);
      
    Connection conn = null;
    try {
      // create a database connection
      conn = DriverManager.getConnection("jdbc:sqlite:" + dbfile);
      Statement statement = conn.createStatement();
      statement.setQueryTimeout(30);  // set timeout to 30 sec.
      statement.executeUpdate("CREATE TABLE IF NOT EXISTS ShellHistory (id INTEGER PRIMARY KEY AUTOINCREMENT, shcmd TEXT)");
      String sql = "INSERT INTO ShellHistory(shcmd) VALUES(?);";

      PreparedStatement prep = conn.prepareStatement(sql);
      //  1 2 3 | 4 5 6 |
      // 0. set auto commit false
      // 1. Preparestatement
      // 2. addBatch
      // 3. execute batch
      // 4. commit
      for(int i=0; i < histList.size(); i++){
	    prep.setString(1, histList.get(i));
	    prep.addBatch();
      }
      conn.setAutoCommit(false);
      int[] updateCounts = prep.executeBatch();
      p("commit");
      p(updateCounts);
      conn.commit();
      conn.setAutoCommit(true);
	    // prep = conn.prepareStatement(sql);
    }
    catch(SQLException e) {
      // if the error message is "out of memory",
      // it probably means no database file is found
      System.err.println(e.getMessage());
    }
    finally {
      try {
        if(conn != null)
          conn.close();
      }
      catch(SQLException e) {
        // connection close failed.
        System.err.println(e.getMessage());
      }
    }
  }
  public static List<String> readHistoryFile(String fname){
      return readFile(fname);
  }
}



