import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_compareLists{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }

    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            List<Integer> ls1 = Arrays.asList(1, 2, 3); 
            List<Integer> ls2 = Arrays.asList(1, 2, 3); 

            BiFunction f = (x, y) -> x == y;
            Boolean b = compareLists(f, ls1, ls2);
            t(b, true);
        }
        {
            BiFunction f = (x, y) -> x == y;
            List<Integer> ls1 = Arrays.asList(1, 2, 3); 
            List<Integer> ls2 = Arrays.asList(1, 2, 4); 

            Boolean b = compareLists(f, ls1, ls2);
            t(b, false);
        }
        {
            List<Integer> ls1 = Arrays.asList(1, 2, 3); 
            List<Integer> ls2 = Arrays.asList(1, 2); 

            BiFunction f = (x, y) -> x == y ;
            Boolean b = compareLists(f, ls1, ls2);
            t(b, false);
        }
        {
            List<Integer> ls1 = Arrays.asList(1, 2, 3); 
            List<Integer> ls2 = Arrays.asList(); 

            BiFunction f = (x, y) -> x == y;
            Boolean b = compareLists(f, ls1, ls2);
            t(b, false);
        }



        sw.printTime();
        end();
    }

    
} 

