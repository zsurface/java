import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// Implement insection operation for HashMap using single linkedlist.
// NOTE: 1. remember to increase the count when a node is inserted
//       2. check the count < max before the insection operation
//       3. If hash key collision occurs, the value need to be checked whether in the list or not
class Node{
    String key;
    String value;
    int max;
    Node next;
    public Node(String key, String value){
	this.key = key;
	this.value = value;
    }
}

class HMap{
    Node[] arr;
    int count;
    int max;
    public HMap(int max){
	count = 0;
	this.max = max;
	arr = new Node[this.max];
    }
    public void insert(String k, String v){
	Integer n = k.hashCode() % max;
	if(count < max){
	    if(arr[n] == null){
		arr[n] = new Node(k, v);
		count++;
	    }else{
		Node curr = arr[n];
		Node prev = curr;
		while(curr != null){
		    if(!curr.value.equals(v)){
			prev = curr;
			curr = curr.next;
		    }else{
			break;
		    }
		}
		if(curr == null){
		    prev.next = new Node(k, v);
		    count++;
		}
	    
	    }
	}
    }
    public void print(){
	for(Node n : arr){
	    while(n != null){
		p("k=" + n.key + " v=" + n.value);
		n = n.next;
	    }
	}
    }
}


public class try_hashmap1{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

	HMap map = new HMap(4);
	map.insert("dog", "cat");
	map.insert("dog", "rat");
	map.insert("dog", "pig");
	map.insert("cat", "catx");
	map.print();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

