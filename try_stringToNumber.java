import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_stringToNumber{
    public static void main(String[] args) {
        test0();
        test1();
        test9();
        test10();
        test11();
    }
    
    public static void test11(){
        beg();
        {
            String s1 = "0";
            String s2 = "0";
            String m = multiStr(s1, s2);
            t(m, "0");
        }
        {
            String s1 = "9";
            String s2 = "0";
            String m = multiStr(s1, s2);
            t(m, "0");
        }
        {
            String s1 = "0";
            String s2 = "9";
            String m = multiStr(s1, s2);
            t(m, "0");
        }
        {
            String s1 = "1";
            String s2 = "9";
            String m = multiStr(s1, s2);
            t(m, "9");
        }
        {
            String s1 = "9";
            String s2 = "1";
            String m = multiStr(s1, s2);
            t(m, "9");
        }
        {
            String s1 = "9";
            String s2 = "9";
            String m = multiStr(s1, s2);
            t(m, "81");
        }
        {
            String s1 = "99";
            String s2 = "99";
            String m = multiStr(s1, s2);
            // (100 - 1)^2
            // 10000 + 1 - 200
            t(m, "9801");
        }
        line();
        {
            String s1 = "99";
            String s2 = "10";
            String m = multiStr(s1, s2);
            t(m, "990");
        }
        line();
        {
            String s1 = "10";
            String s2 = "99";
            String m = multiStr(s1, s2);
            t(m, "990");
        }
        line();
        {
            String s1 = "99";
            String s2 = "99";
            String m = multiStr(s1, s2);
            // (100 - 1)^2 = 10000 + 1 - 200
            t(m, "9801");
        }
        end();
    }
    public static void test10(){
        beg();
        {
            int base = 10;
            int[] arr1 = {0,0};
            int[] arr2 = {0,0};
            int[] arr = sumSameLenIntArr(arr1, arr2, base);
            int[] expArr = {0, 0};
            t(arr, expArr);
        }
        {
            int base = 10;
            int[] arr1 = {0,9};
            int[] arr2 = {0,9};
            int[] arr = sumSameLenIntArr(arr1, arr2, base);
            int[] expArr = {1, 8};
            t(arr, expArr);
        }
        {
            int base = 10;
            int[] arr1 = {0, 9,9};
            int[] arr2 = {0, 9,9};
            int[] arr = sumSameLenIntArr(arr1, arr2, base);
            int[] expArr = {1, 9, 8};
            t(arr, expArr);
        }
        {
            int base = 10;
            int[] arr1 = {0, 0, 9,9};
            int[] arr2 = {0, 1, 9,9};
            int[] arr = sumSameLenIntArr(arr1, arr2, base);
            int[] expArr = {0, 2, 9, 8};
            t(arr, expArr);
        }
        {
            int base = 10;
            int[] arr1 = {0, 1, 9,9};
            int[] arr2 = {0, 0, 9,9};
            int[] arr = sumSameLenIntArr(arr1, arr2, base);
            int[] expArr = {0, 2, 9, 8};
            t(arr, expArr);
        }
        {
            int base = 10;
            int[] arr1 = {0, 9,9};
            int[] arr2 = {0, 0,9};
            int[] arr = sumSameLenIntArr(arr1, arr2, base);
            int[] expArr = {1, 0, 8};
            t(arr, expArr);
        }
        {
            int base = 2;
            int[] arr1 = {0, 1};
            int[] arr2 = {0, 1};
            int[] arr = sumSameLenIntArr(arr1, arr2, base);
            int[] expArr = {1, 0};
            t(arr, expArr);
        }
        end();
    }
    public static void test9(){
        beg();
        {
            String s1 = "0";
            String s2 = "0";
            int[] arr = sumStringToNum(s1, s2);
            int[] expArr = {0, 0};
            t(arr, expArr); 
        }
        {
            String s1 = "0";
            String s2 = "9";
            int[] arr = sumStringToNum(s1, s2);
            int[] expArr = {0, 9};
            t(arr, expArr); 
        }
        {
            String s1 = "7";
            String s2 = "8";
            int[] arr = sumStringToNum(s1, s2);
            int[] expArr = {1, 5};
            t(arr, expArr); 
        }
        {
            String s1 = "99";
            String s2 = "9";
            int[] arr = sumStringToNum(s1, s2);
            int[] expArr = {1, 0, 8};
            t(arr, expArr); 
        }
        {
            String s1 = "99";
            String s2 = "999";
            int[] arr = sumStringToNum(s1, s2);
            int[] expArr = {1, 0, 9, 8};
            t(arr, expArr); 
        }
        {
            String s1 = "999";
            String s2 = "99";
            int[] arr = sumStringToNum(s1, s2);
            int[] expArr = {1, 0, 9, 8};
            t(arr, expArr); 
        }
        end();
    }
    
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();


        sw.printTime();
        end();
    }
} 

