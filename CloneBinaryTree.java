import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// KEY: clone a binary tree
public class CloneBinaryTree{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();

        Node bin = geneBinNoImage();
        Map<Integer,Integer> map = new HashMap<>();
        int k = 1;
        fl("buildMap");
        buildMap(bin, map, k);
        fl("cloneBinTreeFromMap");
        Node root = cloneBinTreeFromMap(map, k);
        printBinTree(root);
        fl("inorder");
        inorder(root);
	
        end();
    }

    public static void test1(){
        beg();
        {
            Node r = geneBinNoImage();
            printBinTree(r);
        }
        {
            List<Integer> ls = list(9, 1, 4, 12, 3, 8, 11);
            BST bst = createBinNoImage(ls);
            printTree(bst.root);
            List<Integer> expList = inorderToList(bst.root);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }
        {
            List<Integer> ls = list(9, 1, 4, 12, 3, 8, 11, 20, 4, 7, 3, 23, 2, 6, 9);
            BST bst = createBinNoImage(ls); 
            printTree(bst.root);
            List<Integer> expList = inorderToList(bst.root);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            // t(expList, sortedList);
            fl("heapSort");
            p(sortedList);
            fl("inorderToList");
            p(expList);
        }
        {
            List<Integer> ls = list(9, 1, 4, 12, 3, 8, 11, 20, 4, 7, 3, 23, 2, 6, 9);
            BST bst = createBinNoImage(ls); 
            printTree(bst.root);
            Node root = cloneBinaryTree(bst.root, (x) -> x + 1);
            fl("root");
            List<Integer> myls = inorderToList(root);
            p(myls);
        }
        {
            List<Integer> ls = list(1);
            BST bst = createBinNoImage(ls); 
            Node node = binLeftMostNode(bst.root);
            t(node.data, 1);
        }
        {
            {
                List<Integer> ls = list(1);
                BST bst = createBinNoImage(ls); 
                Node node = binLeftMostNode(bst.root);
                t(node.data, 1);
            }
            {
                List<Integer> ls = list(1, 2);
                BST bst = createBinNoImage(ls); 
                Node node = binLeftMostNode(bst.root);
                t(node.data, 1);
            }
            {
                List<Integer> ls = list(4, 1);
                BST bst = createBinNoImage(ls); 
                Node node = binLeftMostNode(bst.root);
                t(node.data, 1);
            }
            {
                List<Integer> ls = list(4, 1, 9);
                BST bst = createBinNoImage(ls); 
                Node node = binLeftMostNode(bst.root);
                t(node.data, 1);
            }
            {
                List<Integer> ls = list(4, 1, 9, 3);
                BST bst = createBinNoImage(ls); 
                Node node = binLeftMostNode(bst.root);
                t(node.data, 1);
            }
            fl("binRightMostNode");
            {
                List<Integer> ls = list(1);
                BST bst = createBinNoImage(ls); 
                Node node = binRightMostNode(bst.root);
                t(node.data, 1);
            }
            {
                List<Integer> ls = list(1, 2);
                BST bst = createBinNoImage(ls); 
                Node node = binRightMostNode(bst.root);
                t(node.data, 2);
            }
            {
                List<Integer> ls = list(4, 1);
                BST bst = createBinNoImage(ls); 
                Node node = binRightMostNode(bst.root);
                t(node.data, 4);
            }
            {
                List<Integer> ls = list(4, 1, 9);
                BST bst = createBinNoImage(ls); 
                Node node = binRightMostNode(bst.root);
                t(node.data, 9);
            }
            {
                List<Integer> ls = list(4, 1, 9, 3);
                BST bst = createBinNoImage(ls); 
                Node node = binRightMostNode(bst.root);
                t(node.data, 9);
            }
        }
        end();
}

} 

