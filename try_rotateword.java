import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_rotateword{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        {
            String s1 = "abc";
            String s2 = "bca";
            int n = isrotateWord(s1, s2);
            p(n == 1);
        }
        {
            String s1 = "abc";
            String s2 = "cab";
            int n = isrotateWord(s1, s2);
            p(n == 1);
        }
        {
            String s1 = "abc";
            String s2 = "abc";
            int n = isrotateWord(s1, s2);
            p(n == 1);
        }
        {
            String s1 = "abc";
            String s2 = "abd";
            int n = isrotateWord(s1, s2);
            p(n == -1);
        }
        {
            String s = "abc";
            List<String> list = rotateWord(s);
            p(list);
        }

        end();
    }

    public static int isrotateWord(String s1, String s2){
        if(s1 != null && s2 != null){
            int len = s1.length();
            if(len != s2.length()){
                return -1;
            }else{
                char[] arr = s2.toCharArray();
                for(int k=0; k < len; k++){

                    char[] tmp = new char[len]; 
                    for(int i=0; i < len; i++){
                        // i + 0 % 3 = i
                        // i + 1 % 3 = 
                        // i + 2 % 3
                        // k = 0 => 0 1 2
                        // k = 1 => 1 2 0
                        // k = 2 => 2 0 1
                        // k = 3 => 0 1 2
                        tmp[i] = arr[(i + k) % len]; 
                    }
                    String s = new String(tmp);
                    if(s1.equals(s))
                        return 1;
                }
            }
        }
        return -1;
    }
} 

