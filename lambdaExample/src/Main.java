import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;

import classfile.Print;
import classfile.FileType;
import classfile.Tuple;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.util.stream.*;
import java.util.stream.Collectors;

//interface IMyFunc<T>{
//    boolean test(T num);
//}

class Person {
    Integer age;
    public Person(Integer age){
        this.age = age;
    }
}

class Employee {
  private String name;
  private Integer age;
 
  public Employee(String name, Integer age) {
    this.name = name;
    this.age = age;
  }
 
  //Getters and Setters of name & age go here
  public String toString(){
    return "Employee Name:"+this.name
        +"  Age:"+this.age;
  }

  public Integer getAge(){
      return age;
  }
  public String getName(){
      return name;
  }
 
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof Employee)) {
      return false;
    }
    Employee empObj = (Employee) obj;
    return this.age == empObj.age
        && this.name.equalsIgnoreCase(empObj.name);
  }
 
  @Override
  public int hashCode() {
    int hash = 1;
    hash = hash * 17 + this.name.hashCode();
    hash = hash * 31 + this.age;
    return hash;
  }
}

public class Main {
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
    }

    public static void test0() {
        beg();
	{
	    
	    Random ran = new Random();
	    double total = 0.0;
	    final int max = 1000000;
	    Double[] arr = new Double[max];
	    for(int i=0; i<max; i++) {
		double num = ran.nextDouble();
		arr[i] = num;
	    }
	    List<Double> list = Arrays.asList(arr);

	    Optional<Double> sum = list.stream().reduce(Double::sum);
	    sum.ifPresent(System.out::println);

	    Optional<Double> sum1 = list.stream().reduce((x, y) -> x + y);
	    sum1.ifPresent(System.out::println);

	    Integer[] array = {1, 2, 3, 4};
	    List<Integer> listInt = Arrays.asList(array);

	    // KEY:filter java filter, list filter
	    List<Integer> leftList = listInt.stream().filter(x-> x < 3).collect(Collectors.toList());
	    List<Integer> rightList = listInt.stream().filter(x-> x > 3).collect(Collectors.toList());

	    
	    p(leftList);
	    p(rightList);

	    // KEY: foreach print
	    leftList.forEach(System.out::println);
	    // leftList.forEach(x->p(x));
	    
	}

        {
            // KEY: HashMap filter,
	    
            Map<Integer, Integer> map = new HashMap<Integer, Integer>();
            map.put(1, 4);
            map.put(2, 5);
            map.put(3, 7);

            Map<Integer, Integer> newMap1 = map.entrySet().parallelStream()
                                                        .filter(e->e.getValue() > 1)
                                                        .collect(Collectors.toMap(e->e.getKey(), e->e.getValue()));

            p(newMap1);

            Map<Integer, Integer> newMap2 = map.entrySet().stream()
                                                         .filter(e->e.getValue() > 1)
                                                         .collect(Collectors.toMap(e->e.getKey(), e->e.getValue()));
            p(newMap2);

            // KEY: list of String to list of Integer
            String[] arrStr = {"0", "1", "2", "3"};
            List<String> list1 = Arrays.asList(arrStr);
            List<Integer> strIntList = list1.stream().map(Integer::parseInt).collect(Collectors.toList());
            p(strIntList);
	   
        }
        {
            // KEY: HashMap filter, 
            Map<Integer, Integer> map = new HashMap<Integer, Integer>();
            map.put(1, 4);
            map.put(2, 5);
            map.put(3, 7);
            //Map<Integer, Integer> map1 = map.entrySet.stream().
        }

	{
	    
	    List<Integer> list2 = new ArrayList<Integer>(Arrays.asList(1, 2, 3));
	    boolean ret = list2.stream().anyMatch(s -> s.intValue() > 2);

	    List<String> animalList = Arrays.asList("dog", "cow", "cat", "squirrel");
	    p(animalList);
	    String max11 = animalList.stream().max(Comparator.comparing(String::valueOf)).get();
	    Print.pbl(max11 + " maxlen=" + max11.length());

	    
	    List<String> list4 = Arrays.asList("G","B","F","E");
	    String max4 = list4.stream().max(Comparator.comparing(String::valueOf)).get();
	    p("Max:"+ max4);

	    

	    List<Integer> lin = Arrays.asList(1, 2, 3, 4);
	    List<Integer> lss = lin.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
	    p("lss=" + lss);

	    Integer ssum = lin.stream().reduce(0, (x, y) -> x + y);
	    p("reduce ssum=" + ssum);

	    int[] arr1 = {1, 2, 3, 4};
	    int arrsum = Arrays.stream(arr1).reduce(0, (x, y) -> x + y);
	    p("arrsum=" + arrsum);

	    // join strings
	    List<String> list9 = Arrays.asList("cat1", "dog1", "cow1");
	    String rstr = drop(1, list9.stream().reduce("-", (x, y) -> x + "," + y));
	    p("rstr=" + rstr);
	    

	    List<Integer> list33 = Arrays.asList(1, 2, 3, 4);
	    Optional<Integer> op = list33.stream().reduce((x, y) -> x + y);
	    op.ifPresent(System.out::println);

	    List<Integer> list66 = Arrays.asList(1, 2, 3, 4);
	    Integer ssum44 = list66.stream().reduce(0, (x, y) -> x + y);
	    p(ssum44);
	    
	}
        

        end();
    }
    public static void test1() {
        beg();
        List<Integer> list = Arrays.asList(1, 2, 3); 
        List<Integer> left = list.stream().filter(x -> x < 2).collect(Collectors.toList());
        List<Integer> right = list.stream().filter(x -> x > 2).collect(Collectors.toList());

	
        p(left);
        p(right);
	

        end();
    }

    public static void test2() {
        beg();

        String pattern = "^\\s*--";
        List<String> list = Arrays.asList("cat1", "dog1", " -- cow1");
        List<String> flist = filter4(pattern, list);
        p(flist);

        end();
    }


    /**
    <pre>
    {@literal
        filter list of string with regex
        list = ["dog cat", "big pig"]
        regex = "pig"
        filter(regex, list) => ["big pig"]
    }
    {@code
        String pattern = "^\\s*--";
        List<String> list = Arrays.asList("code 1", "code 2", " -- comment");
        List<String> flist = filter(pattern, list);
        p(flist);
    }
    </pre>
    */ 
    public static List<String> filter4(String pattern, List<String> list){
        Pattern r = Pattern.compile(pattern);
        List<String> flist = list.stream().filter(x -> r.matcher(x).find()).collect(Collectors.toList());
        return flist;        
    }

    public static <T> List<T> myfilter(Function<T, Boolean> f, List<T> list){
       List<T> ls = new ArrayList<T>();
        for(T e : list){
           if(f.apply(e))
            ls.add(e);
       }
       return ls;
    }

    public static <T> List<T> filter3(Predicate<T> f, List<T> list) {
        List<T> ret = new ArrayList<T>();
        for(T e : list) {
            if (f.test(e)) {
                ret.add(e);
            }
        }
        return ret;
    }

    public static void test3() {
        beg();
        {
            List<Integer> list = Arrays.asList(1, 2, 3);
            List<Integer> l = filter(i -> i > 1, list);
            p(l);
        }
        {
            List<String> list = Arrays.asList("dog", "cat", "cow", "");
            List<String> l = filter(e -> e.length() > 0, list);
            p(l);
        }

        end();
    }
    public static void test4() {
        beg();

        Function<Integer, Integer> fun = x -> x + 1;
        BiFunction<Integer, Integer, Boolean> f1 = (x, y) -> x < y;
        Print.p(fun.apply(3) == 4);
        Print.p(f1.apply(1, 2) == true);

        end();
    }
   
    public static void test5() {
        beg();
        List<Employee> employeeList = Arrays.asList(new Employee("Tom Jones", 45),
          new Employee("Harry Major", 26),
          new Employee("Ethan Hardy", 65),
          new Employee("Nancy Smith", 22),
          new Employee("Catherine Jones", 21),
          new Employee("James Elliot", 58),
          new Employee("Frank Anthony", 55),
          new Employee("Michael Reeves", 40));

            Map<Boolean,List<Employee>> employeeMap
                = employeeList
                  .stream()
                  .collect(Collectors.partitioningBy((Employee emp) -> emp.getAge() > 30));
            Print.p("Employees partitioned based on Predicate - 'age > 30'");
            employeeMap.forEach((Boolean key, List<Employee> empList) -> Print.p(key +"->" + empList)); 
            end();
    }
    public static void test6() {
        beg();
	{
            List<String> ls = Arrays.asList("file.htmlkk", "file.html", "kk.html_33", ".html", "html");
            List<String> fls = ls.stream().filter( s -> FileType.isHtml(s) ).collect(Collectors.toList());
            p(fls);

            List<Integer> ls3 = Arrays.asList(1, 2, 3, 4);
            Set<Integer> set = ls3.stream().filter(i -> i >= 2).collect(Collectors.toSet());
            p(set);

            List<Integer> ls5 = Arrays.asList(1, 2, 3, 4);
            String lss9 = tail(ls5.stream().map(i -> i.toString()).reduce("", (x, y) -> x + "," + y));
            p("lss9=" + lss9);

            int n = 10;
            List<Integer> ls4 = IntStream.range(0, n).filter(i -> i % 2 == 0).boxed().collect(Collectors.toList());
            p(ls4);

            List<Double> ls6 = IntStream.range(0, n).filter(i -> i % 2 == 0).asDoubleStream().boxed().collect(Collectors.toList());

            List<String> ls11 = Arrays.asList("cat1", "dog1", "cow1");
            List<Integer> lns = IntStream.range(0, ls11.size()).mapToObj(i -> ls11.get(i).length()).collect(Collectors.toList());
            Print.p("lns" + lns);

            List<String> ls22 = Arrays.asList("cat1", "dog1", "cow1");
            String ss22 = IntStream.range(0, ls22.size())
		.mapToObj(i -> ls22.get(i))
		.reduce("", (x, y) -> x + "," + y); 
            p("ss22=" + ss22);

            p(ls6);
            int num = 3;
            Integer num1 = 4;
            Double db = new Double(3);
            p("db=" + db);
            Double db1 = new Double(num1);
            p("db1=" + db1);
            Double num2 = Math.ceil(db);
            Double num3 = Math.floor(db);
            p("num2=" + num2);
            p("num3=" + num3);

            Integer nnm = 3;
            p("nnm=" + nnm.toString() + nnm.toString());
            Double dm = 3.001;
            p("dm=" + dm.toString() + dm.toString());
	}

	{
	    String ss101 = "a12";
	    Tuple<String, String> p = span(x -> Character.isLetter(x), ss101);
	    t(p.fst(), "a");
	    t(p.snd(), "12");
	}
	{
	    String ss101 = "12";
	    Tuple<String, String> p = span(x -> Character.isLetter(x), ss101);
	    t(p.fst(), "");
	    t(p.snd(), "12");
	}
	{
	    String ss101 = "";
	    Tuple<String, String> p = span(x -> Character.isLetter(x), ss101);
	    t(p.fst(), "");
	    t(p.snd(), "");
	}
	{
	    String ss101 = "";
	    Tuple<String, String> p = span(x -> Character.isLetter(x), ss101);
	    t(p.fst(), "");
	    t(p.snd(), "");
	}
	{
	    List<Person> list = Arrays.asList (new Person(1), new Person(2), new Person(0));    
	    Collections.sort(list, (Person a, Person b) -> a.age - b.age);
	    for(Person pe : list){
		p(pe.age);
	    }
	}
	{
	    // KEY: repeating n integer, repeat n int
	    List<Integer> ls = IntStream.range(0, 3).mapToObj(x -> 0).collect(Collectors.toList());
	    p(ls);
	}
        end();
    }
    public static Tuple<String, String> span(Function<Character, Boolean> f, String s){
        int i=0;
        // "a1"
        while(i<s.length()){ 
            if(!f.apply(s.charAt(i)))
                break;
            i++;
        }
        return new Tuple<String, String>(take(i, s), drop(i, s));
    }

}

