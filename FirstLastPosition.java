import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class FirstLastPosition{
    public static void main(String[] args) {
        test0();
        test_firstLastPosition();
    }
    public static void test0(){
        beg();

        end();
    }
    public static void test_firstLastPosition(){
        beg();
	{
	    Integer[] arr = {1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 0));
	}
	{
	    Integer[] arr = {1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 0));
	}
	{
	    Integer[] arr = {0, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 1));
	}
	{
	    Integer[] arr = {0, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 1));
	}
	{
	    Integer[] arr = {1, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 1));
	}
	{
	    Integer[] arr = {1, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 1));
	}
	{
	    Integer[] arr = {0, 1, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 2));
	}
	{
	    Integer[] arr = {0, 1, 1, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 3));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 3));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 2, 2, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 3));
	}
	{
	    Integer[] arr = {0, 1, 1, 1, 1, 2, 2, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 4));
	}
	{
	    Integer[] arr = {0, 1, 1, 1, 1, 2, 2, 2};
	    Integer n = 2;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(5, 7));
	}
	{
	    Integer[] arr = {0, 1, 1, 1, 1, 2, 2, 2, 3};
	    Integer n = 2;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(5, 7));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 1, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 5));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 1, 1};
	    Integer n = 2;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list());
	}


        end();
    }
} 

