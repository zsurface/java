import classfile.Aron;
import classfile.Print;
import classfile.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.util.stream.Collectors;

public class test_filter {
    public static void main(String[] args) {
        test0();
        test1();
    }

    public static void test0() {
        Aron.beg();
        
        String pattern = "";
        List<String> list = Arrays.asList("cat1");
        List<String> actual_list = Aron.filter(pattern, list);
        Print.printList(actual_list);

        List<String> expected_list1 = new ArrayList<String>(); 

        Test.t(actual_list, expected_list1);
        Print.printList(expected_list1);

        Aron.end();
    }
    public static void test1() {
        Aron.beg();
        
        String pattern = "^\\s*--";
        List<String> list = Arrays.asList("cat1", "dog1", " -- cow1", "--file", " kk--cat");
        List<String> actual_list = Aron.filter(pattern, list);
        Print.printList(actual_list);

        List<String> expected_list1 = Arrays.asList("cat1", "dog1", " kk--cat");

        Test.t(actual_list, expected_list1);

        Aron.end();
    }
}

