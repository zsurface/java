import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class qspartition{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        int arr[] = {1, 5, 3, 2, 2};
        Print.p(arr);
        Print.p("j=" + partition(arr));
        Print.p(arr);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        {
            int arr[] = {1, 5, 3, 2};
            Print.p(arr);
            Print.p("j=" + partition(arr));
            Print.p(arr);
        }
        {
            int arr[] = {1};
            Print.p(arr);
            Print.p("j=" + partition(arr));
            Print.p(arr);
        }
        {
            int arr[] = {2, 3, 2};
            Print.p(arr);
            Print.p("j=" + partition(arr));
            Print.p(arr);
        }

        Aron.end();
    }

    public static int partition(int[] arr){
        // check null here
        int len = arr.length;
        int pivot = arr[len-1];
        int p = 0;
        for (int i=0; i<len; i++){
            if(arr[i] <= pivot){
                int tmp = arr[i];
                arr[i] = arr[p];
                arr[p] = tmp;
                if(i < len - 1)
                    p++;
            }
        }
        return p;
    }
} 

