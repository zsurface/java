import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_sortit{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        
        // immutable list
        List<Integer> ls1 = Arrays.asList(1, 2, 3);
        List<Integer> ls2 = Arrays.asList(1, 4, 1);
        List<Integer> ls3 = Arrays.asList(3, 0, 1);
        List<Integer> ls4 = Arrays.asList(1, 2, 1);
        List<List<Integer>> l2d = of();
        l2d.add(ls1);
        l2d.add(ls2);
        l2d.add(ls3);
        l2d.add(ls4);
        p(l2d);
        BiFunction<List<Integer>, List<Integer>, Integer> f = (x, y) -> x.get(1) - y.get(1); 
        Collections.sort(l2d, f::apply);
        fl();
        p(l2d);


        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

