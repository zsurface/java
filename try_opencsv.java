import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVReader;

public class try_opencsv{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

//        File csvData = new File("/tmp/csv.txt");
//        CSVParser parser = CSVParser.parse(csvData, CSVFormat.EXCEL);
//        for (CSVRecord csvRecord : parser) {
//            // Print.p(CSVRecord);
//        }
        
        String csvFile = "/tmp/csv.csv";

        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        List<String[]> list = new ArrayList<>();
        CSVReader csvReader = new CSVReader(br);
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            list.add(line);
        }
        br.close();
        csvReader.close();
 

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
} 

