import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class GetByteStrEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        try{
            // The input string for this test
            final String string = "Hello World";

            // Check length, in characters
            Print.p("len=" + string.length()); // prints "11"

            // Check encoded sizes
            final byte[] utf8Bytes = string.getBytes("UTF-8");
            Print.p("UTF8=" + utf8Bytes.length); // prints "11"

            final byte[] utf16Bytes= string.getBytes("UTF-16");
            Print.p("UTF-16=" + utf16Bytes.length); // prints "24"

            final byte[] utf32Bytes = string.getBytes("UTF-32");
            Print.p("UTF-32=" + utf32Bytes.length); // prints "44"

            final byte[] isoBytes = string.getBytes("ISO-8859-1");
            Print.p("ISO-8859-1=" + isoBytes.length); // prints "11"

            final byte[] winBytes = string.getBytes("CP1252");
            Print.p("CP1252=" + winBytes.length); // prints "11"
        }catch(UnsupportedEncodingException io){
            io.printStackTrace();
        }
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
} 

