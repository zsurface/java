import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_recurvefun{
    public static void main(String[] args) {
        // test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);

        // immutable list
        List<String> ls = recurveDir("/tmp");
        List<String> lss = ls.stream().map(x -> x + "\n").collect(Collectors.toList());
        writeFileAppend("/tmp/tb.x", lss);
        sw.printTime("diff time");
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        
        // immutable list
        List<String> list = Arrays.asList("cat1", "dog1", "cow1");
        List<String> ls = map(x -> x + "kk", list);
        pp(ls);


        sw.printTime();
        end();
    }
} 

