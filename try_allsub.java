import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_allsub{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();
        List<String> ls = allSub("abc");
        p("ls=" + ls);

        sw.printTime();
        end();
    }
    /**
        "abc"
        a, b, c : len = 1,  0, 1, 2  => k = 1, i=0, 1, 2
        ab, bc  : len = 2,  0, 1     => k = 2, i=0, 1
        abc     : len = 3,  0        => k = 3, i=0
    */
    public static List<String> allSub(String s){
        List<String> ls = new ArrayList<>();
        if(s != null){
            int len = s.length();
            for(int k=1; k<=len; k++){
                for(int i=0; i<len; i++){
                    if(i + k <= len){
                        String sub = s.substring(i, i+k);
                        ls.add(sub);
                    }
                }
            }
        }
        return ls;
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

