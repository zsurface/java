import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_mergeList1{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        
        String dir = "/Users/cat/myfile/bitbucket/java";
        String pattern = "\\.java";
        Map<String, Set<String>> map = Aron.wordPtFilename(pattern, dir); 

        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.print("Enter your name: ");
            String key = scanner.next();
            if(key != null){
                Set<String> list = map.get(key);
                if(list != null)
                    Print.pl(list);
            }
        }


    }

    
    public static void test1(){
        Aron.beg();

        {
            Integer[] arr1 = {1, 4, 7, 13};
            Integer[] arr2 = {4, 6, 9, 12};
            List<Integer> list = mergeArray(arr1, arr2);
            Print.p(list);
        }

        {
            Integer[] arr1 = {1};
            Integer[] arr2 = {};
            List<Integer> list = mergeArray(arr1, arr2);
            Print.p(list);
        }

        {
            Integer[] arr1 = {1};
            Integer[] arr2 = {3};
            List<Integer> list = mergeArray(arr1, arr2);
            Print.p(list);
        }

        Aron.end();
    }

    public static List<Integer> mergeArray(Integer[] arr1, Integer[] arr2){
        List<Integer> list = new ArrayList<>();
        if(arr1 != null && arr2 != null){
            int len1 = arr1.length;
            int len2 = arr2.length;
            int i = 0, j=0;
            while(i < len1 || j < len2){
                if(i >= len1){
                    list.add(arr2[j]);
                    j++;
                }else if(j >= len2){
                    list.add(arr1[i]);
                    i++;
                }else{
                    if(arr1[i] < arr2[j]){
                        list.add(arr1[i]);
                        i++;
                    }else{
                        list.add(arr2[j]);
                        j++;
                    }
                }
            }
        }
        return list;
    }

} 

