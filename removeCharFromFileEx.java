import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.nio.charset.StandardCharsets;

public class removeCharFromFileEx{
    public static void main(String[] args) {
        test1();
        test2();
    }

    public static void test1() {
        Aron.beg();
        {
            String fname = "/Users/cat/myfile/bitbucket/testfile/removeCharFromFile1.txt";
            char ch = ',';
            Aron.removeCharFromFile(fname, ch);
        }
        Aron.end();
    }
    public static void test2() {
        Aron.beg();
        {
            String fname = "/Users/cat/myfile/bitbucket/testfile/removeCharFromFile2.txt";
            char[] charList = {',', ':'};
            Aron.removeCharListFromFile(fname, charList);
        }

        Aron.end();
    }
}

