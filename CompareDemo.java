import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.util.Collections;
import javafx.util.Pair;

/**
<pre>
{@literal
    KEY: Comparable, Comparator, lambda Comparable, lambda Comparator, sort object, collections
    Test File: /Users/cat/myfile/bitbucket/java/CompareDemo.java
}
{@code
}
</pre>
*/ 
class Contact implements Comparable<Contact> {
    String name;
    String addr;
    int    age;
    public Contact(String name, String addr, int age) {
        this.name = name;
        this.addr = addr;
        this.age = age;
    }

    //Minimum heap
    public int compareTo(Contact c) {
        return this.age - c.age;
        //Maximum heap
        //return -(this.age - c.age);
    }

    public String toString() {
        return "["+name+"]["+addr+"]["+age+"]";
    }
}

class Person {
    String name;
    int age;
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
}


// Comparator
class LexicographicComparator implements Comparator<Person> {
    public int compare(Person p1, Person p2) {
        return p1.getName().compareToIgnoreCase(p2.getName());
    }
}

class AgeComparator implements Comparator<Person> {
    public int compare(Person p1, Person p2) {
        return p1.getAge() - p2.getAge() == 0? 0 : p1.getAge() < p2.getAge() ? -1 : 1;
    }
}

public class CompareDemo {
    public static void main(String[] args) {
        test0();
        test1();
        test2();
    }

    public static void test0(){
        p("Hello World!");
        List<Person> listPerson = new ArrayList<Person>(
            Arrays.asList(
                new Person("David", 20),
                new Person("Ann", 19),
                new Person("Michael", 18),
                new Person("Sunny", 6)
            )
        );

        Collections.sort(listPerson, new LexicographicComparator());
        Collections.sort(listPerson, new AgeComparator());
        for(Person p: listPerson) {
            System.out.println(p.getName() + " " + p.getAge());
        }
        List<Contact> listContact = new ArrayList<Contact>(
            Arrays.asList(
                new Contact("David", "Mountain View", 20),
                new Contact("Ann", "Palo Alto", 19),
                new Contact("Michael", "RedWood City", 26)
            )
        );

        fl(); 
        Collections.sort(listContact);
        for(Contact c: listContact) {
            p(c.toString()); 
        }
    }
    public static void test1(){
        fl();
        List<Contact> ls = of(
                new Contact("David", "Mountain View", 20),
                new Contact("Ann", "Palo Alto", 19),
                new Contact("Michael", "RedWood City", 26)
        );
        Collections.sort(ls, (a, b) -> a.name.compareTo(b.name));
        for(Contact c : ls){
            p(c.toString());
        }
    }
    public static void test2(){
        fl();
        List<Contact> ls = of(
                new Contact("David", "Mountain View", 20),
                new Contact("Ann", "Palo Alto", 19),
                new Contact("Michael", "RedWood City", 26)
        );
        BiFunction<Contact, Contact, Integer> f = (a, b) -> a.name.compareTo(b.name);
        Collections.sort(ls, f::apply);
        for(Contact c : ls){
            p(c.toString());
        }
    }
}
