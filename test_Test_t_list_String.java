import classfile.Aron;
import classfile.Print;
import classfile.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.util.stream.Collectors;

public class test_Test_t_list_String {
    public static void main(String[] args) {
        test0();
    }

    public static void test0() {
        Aron.beg();
        
        List<String> list1 = Arrays.asList("cat1", "dog1", "cow1");
        List<String> list2 = Arrays.asList("cat2", "dog2", "cow2");

        Test.f(list1, list2);

        List<String> list11 = Arrays.asList("cat1");
        List<String> list22 = Arrays.asList("cat1");

        Test.t(list11, list22);

        Aron.end();
    }
}

