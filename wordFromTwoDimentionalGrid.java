import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class wordFromTwoDimentionalGrid{
    public static void main(String[] args) {
        test0();

        test1();
        test2();
        test3();
    }

    public static void findWord(Character[][] arr, Integer ih, Integer iw, Set<String> dict, String s, Set<String> set){
        if (arr != null && arr.length > 0 && arr[0].length > 0){
            char c = arr[ih][iw];
            s = s + (c + "");
            arr[ih][iw] = '0';
            if (dict.contains(s)){
                set.add(s);
                pl(s);
            }
            Integer hh = arr.length;
            Integer ww = arr[0].length;
            for(int h = -1; h <= 1; h++){
                for(int w = -1; w <=1; w++){
                    if(0 <= ih + h && ih + h < hh && 0 <= iw + w && iw + w < ww && arr[ih + h][iw + w] != '0')
                        findWord(arr, ih + h, iw + w, dict, s, set);
                }
            }
            arr[ih][iw] = c;
        }
    }

    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        Character[][] arr2d = {
            {'n', 'a', 'c'},
            {'a', 'b', 'n'},
            {'k', 'a', 'f'},
        };

        Integer height = arr2d.length;
        Integer width = arr2d[0].length; 

        Set<String> set = new HashSet<>(); 
        
        Set<String> dict = new HashSet<String>(Arrays.asList("cat", "dog", "nacnf", "acbaf"));
        Integer ih = 0;
        Integer iw = 0;
        String s = "";
        
        fl("arr2d");
        printCharacter2D(arr2d);
        fl("dict");
        pp(dict);
        fl("findWord");
        for(int h = 0; h < height; h++){
            for(int w = 0; w < width; w++){
                findWord(arr2d, h, w, dict, s, set);
            }
        }
        
        fl("set");
        pp(set);
        
        Set<String> set1 = new HashSet<String>(Arrays.asList("cat", "dog"));
        Test.t(set, set1);

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();

        Character[][] arr2d = {
            {'d', 'c', 'a'},
            {'a', 'o', 't'},
            {'k', 'a', 'g'},
        };

        Integer height = arr2d.length;
        Integer width = arr2d[0].length; 

        Set<String> set = new HashSet<>(); 
        
        Set<String> dict = new HashSet<String>(Arrays.asList("cat", "dog"));
        Integer ih = 0;
        Integer iw = 0;
        String s = "";
        
        fl("arr2d");
        printCharacter2D(arr2d);
        fl("dict");
        pp(dict);
        fl("findWord");
        for(int h = 0; h < height; h++){
            for(int w = 0; w < width; w++){
                findWord(arr2d, h, w, dict, s, set);
            }
        }
        
        fl("set");
        pp(set);
        
        Set<String> set1 = new HashSet<String>(Arrays.asList("cat", "dog"));
        Test.t(set, set1);

        end();
    }

    public static void test2(){
        beg();

        Character[][] arr2d = {
            {'d'}
        };

        Integer height = arr2d.length;
        Integer width = arr2d[0].length; 

        Set<String> set = new HashSet<>(); 
        
        Set<String> dict = new HashSet<String>(Arrays.asList("d", "dog"));
        Integer ih = 0;
        Integer iw = 0;
        String s = "";
        
        fl("arr2d");
        printCharacter2D(arr2d);
        fl("dict");
        pp(dict);
        fl("findWord");
        for(int h = 0; h < height; h++){
            for(int w = 0; w < width; w++){
                findWord(arr2d, h, w, dict, s, set);
            }
        }
        
        fl("set");
        pp(set);
        
        Set<String> set1 = new HashSet<String>(Arrays.asList("d"));
        Test.t(set, set1);


        end();
    }

    public static void test3(){
        beg();

        Character[][] arr2d = {
            {'d'},
        };

        Integer height = arr2d.length;
        Integer width = arr2d[0].length; 

        Set<String> set = new HashSet<>(); 
        
        Set<String> dict = new HashSet<String>(Arrays.asList("dogodabkh", "dog"));
        Integer ih = 0;
        Integer iw = 0;
        String s = "";
        
        fl("arr2d");
        printCharacter2D(arr2d);
        fl("dict");
        pp(dict);
        fl("findWord");
        for(int h = 0; h < height; h++){
            for(int w = 0; w < width; w++){
                findWord(arr2d, h, w, dict, s, set);
            }
        }
        
        fl("set");
        pp(set);
        
        Set<String> set1 = new HashSet<String>();
        Test.t(set, set1);

        end();
    }
} 

