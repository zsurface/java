import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/*
/Users/cat/myfile/bitbucket/java/try_stack.java

1. Implement stack with array
   functionarity: push(), pop(), isEmpty(), size()
   reallocate memory when the array is full
   copy the all current elements to a new array
2. Add customized Exception
   if the stack is empty, then throw exception when pop() and top() are called
*/

class EmptyStack extends Exception{
    String msg;
    public EmptyStack(String msg){
        super(msg);
        this.msg = msg;
    }
}

class MyStack{
    int[] arr;
    int count = 0;
    int num = 2;
    public MyStack(){
        arr = new int[num]; 
    }
    public void push(int n){
        if(arr != null){
            if(count < num){
                arr[count] = n;
                count++;
            }else{
                int[] tmp = new int[num];
                for(int i=0; i<num; i++){
                    tmp[i] = arr[i];
                }
                int oldNum = num;
                num = 2*num;
                arr = new int[num];
                for(int i=0; i<oldNum; i++){
                    arr[i] = tmp[i];
                }
                arr[count] = n;
                count++;
            }
        }
    }
    public int pop() throws EmptyStack {
        if(count > 0){
            int inx = count - 1;
            count--;
            return arr[inx];
        }
        else
            throw new EmptyStack("Stack is empty.");
    }
    public int top() throws EmptyStack {
        if(!isEmpty())
            return arr[count - 1];
        else 
            throw new EmptyStack("Stack is empty.");
    }
    public Boolean isEmpty(){
        return count == 0;
    }
    public int size(){
        return count;
    }
    public void print(){
        for(int i=0; i<size(); i++){
            Print.p(arr[i]);
        } 
    }
}

public class try_stack{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        MyStack st = new MyStack();
        st.print();
        try{
            st.pop();
        }catch(Exception e){
            Print.p(e.getMessage());
        }
        st.print();
        st.push(1);
        st.push(2);
        st.push(3);
        st.print();
        Print.fl();
        Print.p("size=" + st.size());
        try{
            st.pop();
            st.pop();
            st.pop();
            st.pop();
        }catch(EmptyStack e){
            Print.p(e.getMessage());
        }
        Print.fl();
        Print.p("size=" + st.size());

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        {
            MyStack st = new MyStack();
            Test.t(st.isEmpty() == true);
        }
        {
            MyStack st = new MyStack();
            st.push(1);
            Test.t(st.isEmpty() == false);
            Test.t(st.size() == 1);
        }
        {
            MyStack st = new MyStack();
            st.push(1);
            try{
                st.pop();
            }catch(EmptyStack e){
                Print.p(e.getMessage());
            }
            Test.t(st.isEmpty() == true);
            Test.t(st.size() == 0);
        }
        {
            MyStack st = new MyStack();
            st.push(1);
            st.push(2);
            st.push(3);
            Test.t(st.isEmpty() == false);
            Test.t(st.size() == 3);
            try{
                Test.t(st.top() == 3);
            }
            catch(EmptyStack e){
                Print.p(e.getMessage());
            }
        }
        {
            MyStack st = new MyStack();
            st.push(1);
            st.push(2);
            st.push(3);
            Test.t(st.isEmpty() == false);
            Test.t(st.size() == 3);
            try{
                st.pop();
                st.pop();
                st.pop();
            }
            catch(EmptyStack e){
                Print.p(e.getMessage());
            }
            Test.t(st.isEmpty() == true);
            Test.t(st.size() == 0);
        }
        Aron.end();
    }
} 

