import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class MaxProduct{
    public static void main(String[] args) {
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
	List<Integer> ls = list(1, 2, 3);
	Integer n = maxProduct(ls);
	pp(n);
        end();
    }

 public static Integer maxProduct(List<Integer> ls){
	Integer max = Integer.MIN_VALUE;
	Integer n1 = 0;
	Integer n2 = 0;
	for(Integer m : ls){
	    for(Integer n : ls){
		if(m*n > max){
		    n1 = m;
		    n2 = n;
		    max = m*n;
		}
	    }
	}
	return n1 + n2;
    }
} 

