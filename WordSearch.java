import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class WordSearch{
    public static void main(String[] args) {
        test1();
    }

    /**
     *
     *     d  o  g
     *        x
     *
     *        o g
     *        o d
     *
     *
     *         x
     *         x
     *     x x A x x
     *         x
     *         x
     *
     *   d -> y
     *   d o -> y
     *   d o g -> y
     */
    public static void wordSearchRight(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(w + 1 < width){
                wordSearchRight(arr, h, w + 1, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }
    public static void wordSearchLeft(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = (c + "") + s;

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(w - 1 >= 0){
                wordSearchLeft(arr, h, w - 1, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }
    public static void wordSearchUp(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(h - 1 >= 0){
                wordSearchUp(arr, h - 1, w, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }
    public static void wordSearchDown(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(h + 1 < height){
                wordSearchDown(arr, h + 1, w, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }

    public static void wordSearch(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(h + 1 < height){
                wordSearch(arr, h + 1, w, ss, set, dict);
            }

            if(h - 1 >= 0){
                wordSearch(arr, h - 1, w, ss, set, dict);
            }

            if(w - 1 >= 0){
                wordSearch(arr, h, w - 1, ss, set, dict);
            }

            if(w + 1 < width){
                wordSearch(arr, h, w + 1, ss, set, dict);
            }

            arr[h][w] = c;
        }
    }

    public static Set<String> wordSearchAll(char[][] arr, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        String ss = "";
        Set<String> set = new HashSet<>();

        for(int h = 0; h < height; h++){
            for(int w = 0; w < width; w++){
                wordSearch(arr, h, w, ss, set, dict);
            }
        }
        pl(set);
        return set;
    }

    public static void test1(){
        beg();
        {
            {
                fl("wordSearchRight 1");
                char[][] arr = {{'a', 'b', 'c', 'd'}};
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("ab");
                dict.add("bc");
                dict.add("bcd");
                String ss = "";
                int h = 0, w = 1;
                wordSearchRight(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("bc", "bcd"));
            }
            {
                fl("wordSearchLeft 1");
                char[][] arr = {{'z', 'a', 'b', 'c'}};
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("ab");
                dict.add("bc");
                dict.add("zab");
                String ss = "";
                int h = 0, w = 2; 
                wordSearchLeft(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("ab", "zab"));
            }
            {
                fl("wordSearchDown 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("b");
                dict.add("bk");
                dict.add("kb");
                dict.add("bke");
                dict.add("a");
                dict.add("ab");
                dict.add("abc");
                String ss = "";
                int h = 0, w = 0;
                wordSearchDown(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("bk", "bke"));
            }
            {
                fl("wordSearchUp 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'a', 'k', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("bk");
                dict.add("kb");
                String ss = "";
                int h = 1, w = 1;
                wordSearchUp(arr, h, w, ss, set, dict);
                pl(set);
            }
        }
        {
            {
                fl("wordSearch 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k"));
            }
            {
                fl("wordSearch 2");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                dict.add("ke");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k", "ke"));
            }
            {
                fl("wordSearch 3");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                dict.add("ke");
                dict.add("kb");
                dict.add("kec");
                dict.add("kecc");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k", "ke", "kb", "kec", "kecc"));
            }
            {
                fl("wordSearch 4");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                dict.add("kecccb");
                dict.add("kecccbabc");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k", "kecccb", "kecccbabc"));
            }
            {
                fl("wordSearchAll 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };

                Set<String> dict = set("k",
									   "kecccb",
									   "kecccbabc",
									   "a",
									   "ab",
									   "abk",
									   "abke",
									   "abcccecbk",
									   "abb",
									   "akc");

                Set<String> set = wordSearchAll(arr, dict);
                t(set, set("k", "kecccb", "kecccbabc", "a", "ab", "abk", "abke", "abcccecbk"));
            }

            {
                fl("wordSearchAll 2");
                char[][] arr = {
                    {'a', 'b', 'c', 'd', 'e'},
                    {'f', 'g', 'h', 'i', 'j'},
                    {'k', 'l', 'm', 'n', 'o'},
                    {'p', 'q', 'r', 's', 't'},
                    {'u', 'v', 'w', 'x', 'y'}
                };
                
                var p = getEnv("b") + "/vim/words.txt";
                var ls = readFile(p);
                Set<String> dict = listToSet(ls);

                Set<String> set = wordSearchAll(arr, dict);
                pl("set.size()=" + set.size());
                for(String s : set){
                    if(s.length() > 1){
                        pl(s);
                    }
                }
            }
			{
                fl("wordSearchAll 3");
                char[][] arr = {
                    {'a', 'b', 'c', 'd', 'e'},
                    {'f', 'g', 'h', 'i', 'j'},
                    {'k', 'l', 'm', 'n', 'o'},
                    {'p', 'q', 'r', 's', 't'},
                    {'u', 'v', 'w', 'x', 'y'}
                };
                
                Set<String> dict = set("ejo", "oje", "joe", "jeo");
				fl("dict=>");
				pp(dict);

                Set<String> set = wordSearchAll(arr, dict);
                pl("set.size()=" + set.size());
                for(String s : set){
                    if(s.length() > 0){
                        pl(s);
                    }
                }
            }
			
			{
                fl("wordSearchAll 4");
                char[][] arr = {
                    {'0', '0', '0', 'd'},
                    {'f', '0', '0', 'i'},
                    {'p', 'q', '0', 's'},
                    {'0', 'v', 'w', '0'}
                };
                
                Set<String> dict = set("dis", "isd", "ids", "sid");
				fl("dict=>");
				pp(dict);

                Set<String> set = wordSearchAll(arr, dict);
                pl("set.size()=" + set.size());
                for(String s : set){
                    if(s.length() > 0){
                        pl(s);
                    }
                }
            }
			/*
			int[][] arr = {
					{ 0,   0,   1,  1},
					{ 1,   0,   1,  1},
					{ 1,   1,   0,  1},
					{ 0,   1,   1,  0},
				};
			*/

        }
		end();
    }
}
