import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import java.util.function.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import classfile.Tuple;
import classfile.Node;
import classfile.BST;

import java.util.stream.*;

/*
class Node{
    Integer data;
    Node left;
    Node right;
    public Node(Integer data){
        this.data = data;
    }
}
*/

class MyNode<T>{
    MyNode next;
    MyNode prev;
    
    T data;
    public MyNode(T data){
	this.data = data;
    }
}

class MyLinkedList{
    MyNode first;
    MyNode last;
    
    public MyLinkedList(){
    }

    public <T> List<T> toList(){
	List<T> ls = new ArrayList<>();
	MyNode curr = first;
	if(curr != null){
	    while(curr != null){
		ls.add((T)curr.data);
		curr = curr.next;
	    }
	}
	return ls;
    }
    public void print(){
	MyNode curr = first;
	if(curr != null){
	    while(curr != null){
		pl(curr.data);
		curr = curr.next;
	    }
	}
    }
    /**
       Add node to the end
     */
    public void append(MyNode node){
	if(first == null){
	    first = last = node;
	}else{
	    MyNode tmpLast = last;
	    last.next = node;
	    node.prev = last;
	    last = node;
	}
	
    }
    /**
       Add node to the font
     */
    public void prepend(MyNode node){
	if(first == null){
	    first = last = node;
	}else{
	    MyNode tmpFirst = first;
	    node.next = tmpFirst;
	    tmpFirst.prev = node;
	    first = node;
	}
    }
    
    public void deleteFirst(){
	if(first != null){
	    MyNode next = first.next;
	    if(next != null){
		// more than one node
		first.next = null;
		next.prev = null;
		first = next;
	    }else{
		// ONLY one node
		first = last = null;
	    }
	}
    }
    
    public void deleteLast(){
	if(last != null){
	    MyNode prev = last.prev;
	    if(prev != null){
		// more than one node
		prev.next = null;
		last.prev = null;
		last = prev;
	    }else{
		// ONLY one node
		first = last = null;
	    }
	}
    }

    public void deleteNode(MyNode node){
	boolean found = false;
	MyNode curr = first;
	while(curr != null){
	    if(curr.data == node.data){
		found = true;
		break;
	    }
	    curr = curr.next;
	}
	
	if(found){
	    // First node
	    if(curr.prev == null){
		deleteFirst();
	    }else if(curr.next == null){
		// Last node
		deleteLast();
	    }else{

		/**
		   "middle" node


		      tmpPrev    curr       tmpNext
		    |------|    |-----|    |------|
		    | p1|n1|    | p|n |    | p2|n2|
		    |------|    |-----|    |------|

		    tmpPrev.next = tmpNext
		    tmpNext.prev = tmpPrev
		    curr.next = curr.prev = null

		 */
		MyNode tmpPrev = curr.prev;
		MyNode tmpNext = curr.next;

		tmpPrev.next = tmpNext;
		tmpNext.prev = tmpPrev;
		curr.next    = curr.prev = null;
	    }
	}
    }
}



class MyPair{
    Integer x;
    Integer y;
    public MyPair(Integer x, Integer y){
        this.x = x;
        this.y = y;
    }
    
    @Override
    public String toString(){
	return "(" + x + " " + y + ")";
    }
}

class TNode{
    Boolean isWord;
    TNode[] arr = new TNode[26];
    public TNode(Boolean isWord){
        this.isWord = isWord;
    }
}



class Tup{
    String fst;
    String snd;
    public Tup(String fst, String snd){
        this.fst = fst;
        this.snd = snd;
    }
}

class Kthlarge{
    PriorityQueue<Integer> queue = new PriorityQueue<>();
    Integer k;
    public Kthlarge(Integer k, Integer[] ls){
        this.k = k;
        for(int i = 0; i < ls.length; i++){
            queue.add(ls[i]);
        }
    }
    public Integer add(Integer n){
        queue.add(n);
        Integer x = null;
        Integer c = k;
        List<Integer> ls = new ArrayList<>();
        while(c > 0){
           x = queue.remove(); 
           ls.add(x);
           c--;
        }

        for(Integer m : ls)
            queue.add(m);

        return x;
    }

}

class Person{
    String name;
    Integer age;
    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
    }
}


class Student{
    String name;
    Integer age;
    public Student(String name, Integer age){
	this.name = name;
	this.age = age;
    }
    @Override
    public String toString(){
	return "Name" + name + " " + "age=" + age;
    }
}


public class Try_2021_07_10{
    public static void main(String[] args) {
        // test0();
	test1();
    }
    public static void test0(){
        beg();
        {
            Tup tup = firstWord("a b");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            Tup tup = firstWord(" a b");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            Tup tup = firstWord("a");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            Tup tup = firstWord("");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            List<String> ls = prefixStr("");
            pp(ls);
            pp("len=" + len(ls));
        }
        {
            fl();
            List<String> ls = prefixStr("a");
            pp(ls);
            pp("len=" + len(ls));
        }
        {
            fl();
            List<String> ls = prefixStr("ab");
            pp(ls);
            pp("len=" + len(ls));
        }
        {
            fl();
            String s = "abc";
            prefixSuffix(s);
        }
        {
            fl();
            {
                String s = "abc e";
                List<String> ls = splitStr(s);
                pp(ls);
                fl();
            }
            {
                String s = " abc e";
                List<String> ls = splitStr(s);
                pp(ls);
                fl();
            }
            {
                String s = " abc e ";
                List<String> ls = splitStr(s);
                pp(ls);
                fl();
            }
        }    
        {
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(2, arr);
                Integer kth = kl.add(0);
                pl("kth=" + kth + " " + (kth == 1));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(2, arr);
                Integer kth = kl.add(9);
                pl("kth=" + kth + " " + (kth == 2));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(2, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 1));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(3, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 2));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(1, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 1));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(4, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 4));
            }
            {
                fl();
                PriorityQueue<Person> queue = new PriorityQueue<>((x, y) -> y.age - x.age);

                queue.add(new Person("David", 3));
                queue.add(new Person("Anne", 9));
                queue.add(new Person("Michael", 2));

                while(!queue.isEmpty()){
                    Person p = queue.remove();
                    pl("name=" + p.name + " age=" + p.age);
                }
                
            }
            {
                {
                    /**
                                    10
                                  5    12
                    */
                    Node root = new Node(10);
                    root.left = new Node(5);
                    root.right = new Node(12);
                    levelTraveral(root);
                }
                {
                    fl();
                    /**
                                  10
                              5             14
                          1     8      11          18
                      0    2  6   9  10    13  17      20

                    */
                    Node root = new Node(10);
                    root.left = new Node(5);
		    root.right = new Node(14);

		    root.left.left = new Node(1);
		    root.left.right = new Node(8);
		    root.right.left = new Node(11);
		    root.right.right = new Node(18);

		    root.left.left.left = new Node(0);
		    root.left.left.right = new Node(2);
		    root.left.right.left = new Node(6);
		    root.left.right.right = new Node(9);
		    root.right.left.left = new Node(10);
		    root.right.left.right = new Node(13);
		    root.right.right.left = new Node(17);
		    root.right.right.right = new Node(20);

                    zigzagTraveral(root);
                }
            }
	    {
		fl("copyBinTree");
		Node root = new Node(10);
		root.left = new Node(5);
		root.right = new Node(14);

		root.left.left = new Node(1);
		root.left.right = new Node(8);
		root.right.left = new Node(11);
		root.right.right = new Node(18);

		root.left.left.left = new Node(0);
		root.left.left.right = new Node(2);
		root.left.right.left = new Node(6);
		root.left.right.right = new Node(9);
		root.right.left.left = new Node(10);
		root.right.left.right = new Node(13);
		root.right.right.left = new Node(17);
		root.right.right.right = new Node(20);
		Node r = copyBinTree(root);
		levelTraveral(r);
	    }
	    {
		{
		    /**
		        10
		      5   
                    */
                    Node root1 = new Node(10);
                    root1.left = new Node(5);
		    /**
		        11
		           9
                    */
		    Node root2 = new Node(11);
                    root2.right = new Node(9);
		    
		    /**
		            21
                         5      9
		       
		     */		    
		    Node root = mergeBinTree(root1, root2);
		    levelTraveral(root);
		}
		{
		    /**
		        10
		      5    12
                               15   
                    */
		    BST bst1 =  createBin(list(10, 5, 12, 15));
		    /**
		        11
		      9
                    */
		    BST bst2 =  createBin(list(11, 9));

		    
		    /**
		            21
                         14      12
                                    15
		       
		     */		    
		    Node root = mergeBinTree(bst1.root, bst2.root);
		    // levelTraveral(root);
		    printBinTree(root);
		    
		}
		{
		    /**
		        10
		      5    12
                               15
                      Can not use insert-image to insert pdf
		      http://localhost/pdf/bin_2704.pdf
                    */
		    BST bst1 =  createBin(list(10, 5, 12, 15));
		    Node root = padBinTree(bst1.root);
		    printBinTree(root);
		}

	    }
        }
	


        end();
    }

    

    /**
       0 0 1 => True
       0 0 1 0 1 => False
    */
    public static Boolean isRepeating(String s){
        Boolean ret = false;
        boolean[] arr = new boolean[10]; // default = false
        for(int i = 0; i < s.length(); i++){
            int c = (int)s.charAt(i) - '0';
            if(c != 0 && c <= 9){
                if(arr[c] == false){
                    arr[c] = true;
                }else{
                    ret = true;
                    break; 
                }
            }
        }
        return ret;
    }

    public static <T> T[] copyArray(T[] arr, int len){
        return Arrays.copyOf(arr, len);
    }

    /**
      Rotate a string to the left

      a b c _

      len = 4
      [(i + 3) % 4] = [i % 4]
      [ 3 ] <- [ 0 ]       when i = 0      _ b c a
      [ 0 ] <- [ 1 ]       when i = 1      b _ c a
      [ 1 ] <- [ 2 ]       when i = 2      b c _ a
      [ 2 ] <- [ 3 ]       when i = 3      b c a _

    */
    public static Integer[] rotateLeft(Integer[] arr){
        Integer[] array = copyArray(arr, arr.length + 1); 

        int len = array.length;
        for(int i = 0; i < len; i++){
            array[ (i + (len - 1)) % len ] = array[ i % len]; 
        }
        return copyArray(array, len - 1);
    }

    /**
        a b c _                 _ a b c
                                c _  a b
                                b c   _ a

        len = 4
        [ i % 4] = [ (i + 3) % 4] 
        [ 0 ] <- [3]  when i = 0   c a b _
        [ 1 ] <- [0]  when i = 1

    */
    public static Integer[] rotateRight(Integer[] arr){
        return null;
    }

    public static void add(TNode root, String s){
        if(s.length() > 0){
            int x = (int)s.charAt(0) - 'a'; 
            if(root.arr[x] == null)
                root.arr[x] = new TNode(false);
            add(root.arr[x], s.substring(1, s.length())); 
        }else{
            root.isWord = true;
        }
    }

    public static Boolean contains(TNode root, String s){
        if(s.length() > 0){
            int x = (int)s.charAt(0) - 'a';
            if(root.arr[x] == null)
                return false;
            else
                return contains(root.arr[x], s.substring(1, s.length()));
        }else{
            return root.isWord;
        }
    }

    public static Integer[][] rotateLeftAll(Integer[] arr){
        Integer[][] ret = new Integer[arr.length][arr.length];
        Integer[] tarr = copyArray(arr, arr.length);
        for(int i = 0; i < tarr.length; i++){
            Integer[] array = rotateLeft(tarr);
            ret[i] = array; 
            tarr = copyArray(array, array.length);
        }
        return ret;
    }

    

    /**
       3  1  4  3  7   8
      [3]   [4]   [7] [8]  => count = 4

    */
    public static Integer increasingCount(Integer inx, Integer[] arr){
        Integer m = 0; // The maximum current Integer
        Integer count = 0;
        int len = arr.length;
        if(len > 0){
	    m = arr[inx];
            count = 1; // count the number of increasing Integer
            for(int i = (inx + 1); i < len; i++){
                if(m < arr[i]){
                    m = arr[i];  // update the current maximum Integer
                    count++;
                }
            }
        }
        return count;
    }

    /**
          '   &1 & 2   & 3 &'
	  [3] [2]  [5]   [3] [0]

           'a&c'
            0 1 2

            0 1
            1 2
            1 1
     */
    public static List<Integer> charIndex(Character c, String s){
	int len = s.length();
	var charls = strToListChar(s);
	var ls = zipWith((x, y) -> y == c ? x : len + 1, range(0, len), charls);
	var fl = ls.stream().filter(x -> x < len).collect(Collectors.toList());
	var cx = concatList(list(0), fl);
	var cy = concatList(fl, list(len - 1));
	var cz = zipWith((x, y) -> y - x, cx, cy);
	return cz;
    }
    
    /**
       Complexity O(n^2)
     */
    public static Integer longestSubsequence(Integer[] arr){
	int len = arr.length;
	Integer max = 0;
	for(int j = 0; j < len; j++){
	    int m = increasingCount(j, arr);
	    max = Math.max(max, m);
	}
	return max;
    }
    
    /**
    KEY: Print the longest increasing subsequence for a given Integer array

        2 4 0 1 7

        2 4        m = 2, j = 1, i = 0
        2 4        m = 4, j = 2, i = 1
        2 4 0      m = 4, j = 2, i = 1 
        2 4 0 1    m = 4, j = 3, i = 1
        2 4 0 1 7  m = 7, j = 4, i = 4  

      
    */
    public static Integer longestIncreasingSubsequence3(Integer[] arr){
        int len = arr.length;
        Integer m = 0;
        if(len > 0){
            m = arr[0];
            int maxInx = 0;
            int maxLen = 1;
            for(int j = 1; j < arr.length; j++){
                if(maxInx < j && m < arr[j]){
                    m = arr[j];
                    maxInx = j;
                }
            }
        }
        return m;
    }

    /**
    Use hashset to solve it
      
      0 1
      0 1 1
    */
    public static Boolean isRepeating2(String s){
        Boolean ret = false;
        Set<Character> set = new HashSet<>();
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) != '0'){
                if(set.contains(s.charAt(i))){
                    ret = true;
                    break;
                }else{
                    set.add(s.charAt(i));
                }
            }
        }
        return ret;
    }

    public static List<String> generateStringSuffix(String s){
        List<String> ls = new ArrayList<>();
        int len = s.length();
        for(int i = 0; i < len; i++){
            ls.add(s.substring(i, len)); 
        }
        return ls;
    }

    public static void test1(){
	    beg();
	    {
            List<String> ls = list("lollipop", "popsicle");
            List<String> lss = ls.stream().map(x -> x + "k").collect(Collectors.toList());
            pp(lss);
		    
	    }
	    {
            List<Integer> ls = IntStream.range(0, 10).filter(x -> x % 2 == 0).boxed().collect(Collectors.toList());
            // List<Integer> ls4 = IntStream.range(0, 10).filter(i -> i % 2 == 0).boxed().collect(Collectors.toList());
	    }
	    {
            {
                fl("subSequence");
                List<String> ls = list();
                String s = "1234";
                subSequence(ls, s);
                pp(ls);
            }
	    }

	    {
            {
                fl("subSequence2");
                Integer k = 2;
                String s = "1";
                List<String> ls = subSequence2(k, s);
                pp(ls);
            }
            {
                fl("subSequence2");
                Integer k = 2;
                String s = "12";
                List<String> ls = subSequence2(k, s);
                pp(ls);
            }
            {
                fl("subSequence2");
                Integer k = 2;
                String s = "123";
                List<String> ls = subSequence2(k, s);
                pp(ls);
            }
	    }

        {
            {
                fl("isRepeating");
                String s = "00101";
                Boolean b = isRepeating(s);
                pp(b == true);
            }
            {
                fl("isRepeating");
                String s = "001";
                Boolean b = isRepeating(s);
                pp(b == false);
            }
        }
        {
            {
                fl("isRepeating2");
                String s = "00101";
                Boolean b = isRepeating2(s);
                pl(b == true);
            }
            {
                fl("isRepeating2");
                String s = "001";
                Boolean b = isRepeating2(s);
                pl(b == false);
            }
        }
        {
            {
                fl("increasingCount");
                Integer[] arr = {1, 4, 0, 2, 8, 3, 9};
                Integer count = increasingCount(0, arr);
                pl(count == 4);
            }
            {
                fl("increasingCount");
                Integer[] arr = {1, 4};
                Integer count = increasingCount(0, arr);
                pl(count == 2);
            }
            {
                fl("increasingCount");
                Integer[] arr = {9, 4, 5, 6, 7};
                Integer count = increasingCount(0, arr);
                pl(count == 1);
            }
	    {
                fl("increasingCount");
                Integer[] arr = {9, 4, 5, 6, 7};
                Integer count = increasingCount(1, arr);
                pl(count == 4);
            }
        }
	{
	    {
		fl("longestSubsequence");
		Integer[] arr = {9, 4, 5, 6, 7};
		Integer max = longestSubsequence(arr);
		pl("max=" + max);
		pl(max == 4);
	    }
	    {
		fl("longestSubsequence");
		Integer[] arr = {9, 4};
		Integer max = longestSubsequence(arr);
		pl("max=" + max);
		pl(max == 1);
	    }
	    {
		fl("longestSubsequence");
		Integer[] arr = {9};
		Integer max = longestSubsequence(arr);
		pl("max=" + max);
		pl(max == 1);
	    }
	    {
		fl("longestSubsequence");
		Integer[] arr = {9, 3, 4};
		Integer max = longestSubsequence(arr);
		pl("max=" + max);
		pl(max == 2);
	    }
	    {
		/**
		      1 9 3 4
                      1 [9] x x
                      1     [3] x
                      1         [4]

                      1 9  9 3 4
                      1 [9]  [9] x x
                      1  3  3 4
                      1  [3]  x [4]
		 */
		fl("longestSubsequence");
		Integer[] arr = {1, 9, 3, 4};
		Integer max = longestSubsequence(arr);
		pl("max=" + max);
		pl(max == 3);
	    }
	    {
		fl("longestSubsequence");
		Integer[] arr = {1, 9, 3, 1, 4};
		Integer max = longestSubsequence(arr);
		pl("max=" + max);
		pl(max == 2);
	    }
	}
	{
	    {
		fl("charIndex");
		String s = "a&c";
		List<Integer> ls = charIndex('&', s);
		pp(ls);
	    }
	    {
		/**
                      [1, 3]
                      [0, 1, 3]
                      [1, 3, 3]
                       1, 2, 0
		 */
		fl("charIndex");
		String s = "a&c&";
		List<Integer> ls = charIndex('&', s);
		pp(ls);
	    }
	}
	{
	    leftPadPl("dog", 10, '-');
	    leftPadPlnChar("dog", 4, '-');
	}
    {
        {
            fl("Rotate a string to the left");
            Integer[] arr = {1, 2, 3};
            Integer[] array = rotateLeft(arr);
            printArr(arr); 
            fl();
            printArr(array);
        }

        {
            fl("Rotate a string to the left");
            Integer[] arr = {1, 2, 3};
            printArr(arr); 

            for(int i = 0; i < arr.length; i++){
                Integer[] array = rotateLeft(arr);
                fl();
                printArr(array);
                arr = copyArray(array, array.length);
            }
        }
        {
            fl("Rotate a string to the left");
            Integer[] arr = {1, 2, 3};
            printArr(arr); 
            fl();
            Integer[][] arr2d = rotateLeftAll(arr);
            printArr2dInteger(arr2d);
        }
        {
            {
                TNode root = new TNode(true);
                String s = "";
                Boolean b = contains(root, s);
                pl("b=" + (b == true));
            }
            {
                TNode root = new TNode(true);
                String s = "a";
                add(root, s);
                Boolean b = contains(root, s);
                pl("b=" + (b == true));
            }
            {
                TNode root = new TNode(true);
                String s = "ab";
                add(root, s);
                Boolean b = contains(root, "a");
                pl("b=" + (b == false));
                Boolean b1 = contains(root, "ab");
                pl("b1=" + (b1 == true));
                Boolean b2 = contains(root, "abc");
                pl("b2=" + (b2 == false));
            }
        }
    }
    {
        fl("prime");
        List<Integer> prime = allPrimes(200);
        pp(prime);
    }
    {
	{
	    fl("partition");
	    Integer[] arr = {2};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partition(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 0);
	}
	{
	    fl("partition");
	    Integer[] arr = {2, 1};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partition(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 0);
	}
	{
	    fl("partition");
	    Integer[] arr = {2, 1, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partition(arr, lo, hi);
	    printArr(arr);
            t(pivot, 2);
	}
	{
	    fl("partition");
	    Integer[] arr = {2, 1, 4, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partition(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 2);
	}
	{
	    fl("partition");
	    Integer[] arr = {2, 1, 1, 4, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partition(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 3);
	}
    }
    {
	{
	    fl("partitionInLine");
	    Integer[] arr = {2};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partitionInLine(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 0);
	}
	{
	    fl("partitionInLine");
	    Integer[] arr = {2, 1};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partitionInLine(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 0);
	}
	{
	    fl("partitionInLine");
	    Integer[] arr = {2, 1, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partitionInLine(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 2);
	}
	{
	    fl("partitionInLine");
	    Integer[] arr = {2, 1, 4, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partitionInLine(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 2);
	}
	{
	    fl("partitionInLine");
	    Integer[] arr = {2, 1, 1, 4, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    Integer pivot = partitionInLine(arr, lo, hi);
	    printArr(arr);
	    t(pivot, 3);
	}
	
    }
    {
	{
	    fl("check prime");
	    List<Integer> ls = allPrimes2(100);
	    List<Integer> pls = ls.stream().filter(x -> isPrime(x - 6) && isPrime(x + 6) && isPrime(x - 10) && isPrime(x + 10))
		.collect(Collectors.toList());

	    // pp(pls);

	    var lss = IntStream.range(7, 200).filter(x -> isPrime(x - 6) && isPrime(x + 6) && isPrime(x - 10) && isPrime(x + 10)).boxed().collect(Collectors.toList());
	    pp(lss);
	}
    
    }
    {
        {
            fl("quckSort");
            Integer[] arr = {4, 2, 3};
            Integer[] expectedArray = {2,3, 4};
            int lo = 0;
            int hi = arr.length - 1;
            quickSort(arr, lo, hi);
            printArr(arr);
            t(arr, expectedArray);
        }
        {
            fl("quckSort");
            Integer[] arr = {4};
            Integer[] expectedArray = {4};
            int lo = 0;
            int hi = arr.length - 1;
            quickSort(arr, lo, hi);
            printArr(arr);
            t(arr, expectedArray);
        }
        {
            fl("quckSort");
            Integer[] arr = {4, 2};
            Integer[] expectedArray = {2, 4};
            int lo = 0;
            int hi = arr.length - 1;
            quickSort(arr, lo, hi);
            printArr(arr);
            t(arr, expectedArray);
        }
        {
            fl("quckSort");
            Integer[] arr = {2, 4};
            Integer[] expectedArray = {2, 4};
            int lo = 0;
            int hi = arr.length - 1;
            quickSort(arr, lo, hi);
            printArr(arr);
            t(arr, expectedArray);
        }
        {
            fl("quckSort");
            Integer[] arr = {2, 4, 1};
            Integer[] expectedArray = {1, 2, 4};
            int lo = 0;
            int hi = arr.length - 1;
            quickSort(arr, lo, hi);
            printArr(arr);
            t(arr, expectedArray);
        }
        {
            fl("quckSort");
            Integer[] arr = {2, 4, 1, 5};
            Integer[] expectedArray = {1, 2, 4, 5};
            int lo = 0;
            int hi = arr.length - 1;
            quickSort(arr, lo, hi);
            printArr(arr);
            t(arr, expectedArray);
        }
    }
    {
        {
            fl("selectionSort");
            Integer[] arr = {2};
            Integer[] expectedArray = {2};
            int lo = 0;
            int hi = arr.length - 1;
            selectionSort(arr, lo, hi);
            t(arr, expectedArray);
        }
        {
            fl("selectionSort");
            Integer[] arr = {2, 4, 1};
            Integer[] expectedArray = {1, 2, 4};
            int lo = 0;
            int hi = arr.length - 1;
            selectionSort(arr, lo, hi);
            t(arr, expectedArray);
        }
        {
            fl("selectionSort");
            Integer[] arr = {2, 4};
            Integer[] expectedArray = {2, 4};
            int lo = 0;
            int hi = arr.length - 1;
            selectionSort(arr, lo, hi);
            t(arr, expectedArray);
        }
        {
            fl("selectionSort");
            Integer[] arr = {2, 4, 1, 5};
            Integer[] expectedArray = {1, 2, 4, 5};
            int lo = 0;
            int hi = arr.length - 1;
            selectionSort(arr, lo, hi);
            t(arr, expectedArray);
        }
        {
            fl("selectionSort");
            Integer[] arr = {2, 4, 1, 5};
            Integer[] expectedArray = {1, 2, 4, 5};
            int lo = 0;
            int hi = arr.length - 1;
            selectionSort(arr, lo, hi);
            t(arr, expectedArray);
        }
    }
    {
        {
            fl("mergeSortedList");
            Integer[] arr1 = {2, 4, 6, 9};
            Integer[] arr2 = {1, 3, 4, 8};
            Integer[] expectedArray = {1, 2, 3, 4, 4, 6, 8, 9};
            Integer[] arr = mergeSortedList(arr1, arr2);
            t(arr, expectedArray);
        }
        {
            fl("mergeSortedList");
            Integer[] arr1 = {};
            Integer[] arr2 = {1, 3, 4, 8};
            Integer[] expectedArray = {1, 3, 4, 8};
            Integer[] arr = mergeSortedList(arr1, arr2);
            t(arr, expectedArray);
        }
        {
            fl("mergeSortedList");
            Integer[] arr1 = {9};
            Integer[] arr2 = {1, 3, 4, 8};
            Integer[] expectedArray = {1, 3, 4, 8, 9};
            Integer[] arr = mergeSortedList(arr1, arr2);
            t(arr, expectedArray);
        }
        {
            fl("mergeSortedList");
            Integer[] arr1 = {9};
            Integer[] arr2 = {8};
            Integer[] expectedArray = {8, 9};
            Integer[] arr = mergeSortedList(arr1, arr2);
            t(arr, expectedArray);
        }
    }
    {
        {
            fl("insertBin2");
            Node root = geneBinNoImage(list(8, 4, 9));
            Node node = new Node(12);
            printTree(root);
            Node r = insertBin2(root, node);
            fl("insertBin2");
            printTree(r);
            fl("insertBin2");
            printTree(r);
        }
        {
            fl("insertBin2");
            Node root = null;
            Node node = new Node(12);
            Node expectedNode = new Node(12);
            Node r = insertBin2(root, node);
            t(r, expectedNode);
        }
        {
            fl("kk1");
            Node n1 = geneBinNoImage(list(1, 2));
            Node n2 = geneBinNoImage(list(1, 3));
            t(n1, n2);
        }
        {
            fl("kk2");
            Node n1 = geneBinNoImage(list(1, 2));
            Node n2 = geneBinNoImage(list(1, 2));
            pp(equalBinaryTree(n1, n2)); 
        }
    }
    {
        {
            fl("generateStringSuffix");
            String s = "able";
            List<String> ls = generateStringSuffix(s);
            pl("len=" + len(ls));
            pp(ls);
        }
    }
    
    {
        {
            fl("maxConnection");
            Integer[][] arr = {{0}};
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection(arr, h, w);
            t(n, 0);
        }
        {
            fl("maxConnection");
            Integer[][] arr = {{1}};
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection(arr, h, w);
            t(n, 1);
        }
        {
            fl("maxConnection");
            Integer[][] arr = {
                {0, 1}, 
                {0, 1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection(arr, h, w);
            t(n, 2);
        }
        {
            fl("maxConnection");
            Integer[][] arr = {
                {1}, 
                {1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection(arr, h, w);
            t(n, 2);
        }
        {
            fl("maxConnection");
            Integer[][] arr = {
                {0, 1, 0}, 
                {0, 0, 1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection(arr, h, w);
            t(n, 1);
        }
        {
            fl("maxConnection");
            Integer[][] arr = {
                {0, 1, 0, 0, 1}, 
                {0, 1, 1, 0, 1},
                {0, 0, 1, 0, 1},
                {0, 0, 1, 0, 1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection(arr, h, w);
            t(n, 5);
        }
    }
    
    {
        {
            fl("maxConnection2");
            Integer[][] arr = {{0}};
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection2(arr, h, w);
            t(n, 0);
        }
        {
            fl("maxConnection2");
            Integer[][] arr = {{1}};
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection2(arr, h, w);
            t(n, 1);
        }
        {
            fl("maxConnection2");
            Integer[][] arr = {
                {0, 1}, 
                {0, 1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection2(arr, h, w);
            t(n, 2);
        }
        {
            fl("maxConnection2");
            Integer[][] arr = {
                {1}, 
                {1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection2(arr, h, w);
            t(n, 2);
        }
        {
            fl("maxConnection2");
            Integer[][] arr = {
                {0, 1, 0}, 
                {0, 0, 1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection2(arr, h, w);
            t(n, 1);
        }
        {
            fl("maxConnection2");
            Integer[][] arr = {
                {0, 1, 0, 0, 1}, 
                {0, 1, 1, 0, 1},
                {0, 0, 1, 0, 1},
                {0, 0, 1, 0, 1}
            };
            Integer w = 0;
            Integer h = 0;
            Integer n = maxConnection2(arr, h, w);
            t(n, 5);
        }
    }
    {
        {
            fl("kSmallestPair");
            Integer[] arr1 = {1};
            Integer[] arr2 = {2};
            List<MyPair> p = kSmallestPair(arr1, arr2, 1);
            t(p.get(0).x, 1);
            t(p.get(0).y, 2);
        }
        {
            fl("kSmallestPair");
            Integer[] arr1 = {1, 3, 5};
            Integer[] arr2 = {2, 6};
            List<MyPair> p = kSmallestPair(arr1, arr2, 1);
            t(p.get(0).x, 1);
            t(p.get(0).y, 2);
        }
        {
            fl("kSmallestPair");
            Integer[] arr1 = {1, 3, 5};
            Integer[] arr2 = {2, 6};
            List<MyPair> p = kSmallestPair(arr1, arr2, 2);
            t(p.get(0).x, 1);
            t(p.get(0).y, 2);
            t(p.get(1).x, 3);
            t(p.get(1).y, 6);
        }
    }
    {
        {
            fl("longestSubstringWithoutRepeatingChar");
            String s = "a";
            Integer m = longestSubstringWithoutRepeatingChar(s);
            t(m, 1);
        }
        {
            fl("longestSubstringWithoutRepeatingChar");
            String s = "aa";
            Integer m = longestSubstringWithoutRepeatingChar(s);
            t(m, 1);
        }
        {
            fl("longestSubstringWithoutRepeatingChar");
            String s = "ab";
            Integer m = longestSubstringWithoutRepeatingChar(s);
            t(m, 2);
        }
    }
    {
        {
            fl("permutation");
            List<Integer> ls = list(1, 2, 3);
            List<Integer> prels = new ArrayList<>();
            permutation(prels, ls);
        }
        {
            fl("houseRobber");
            List<Integer> ls = list(1, 2, 3, 4);
            List<MyPair> prels = new ArrayList<>();
            Integer inx = -10;
            houseRobber(prels, ls, inx);
        }
    }


   {
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "a";
            Boolean b = isSubsequence(gs, s);
            t(b, true);
        }
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "b";
            Boolean b = isSubsequence(gs, s);
            t(b, true);
        }
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "c";
            Boolean b = isSubsequence(gs, s);
            t(b, true);
        }
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "abc";
            Boolean b = isSubsequence(gs, s);
            t(b, true);
        }
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "ac";
            Boolean b = isSubsequence(gs, s);
            t(b, true);
        }
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "d";
            Boolean b = isSubsequence(gs, s);
            t(b, false);
        }
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "abcd";
            Boolean b = isSubsequence(gs, s);
            t(b, false);
        }
        {
            fl("isSubsequence");
            String gs = "abc";
            String s = "";
            Boolean b = isSubsequence(gs, s);
            t(b, true);
        }
    }
    {
	{
        fl("permutationNoAdjacent");
	    List<MyPair> ls = new ArrayList<>();
	    ls.add(new MyPair(0, 1));
	    ls.add(new MyPair(1, 2));
	    ls.add(new MyPair(2, 3));
	    ls.add(new MyPair(3, 4));
            List<MyPair> prels = new ArrayList<>();
            permutationNoAdjacent(prels, ls);
        }
    }
    {
        {
            fl("permutationfb");
            List<Integer> ls = list(1, 2, 3, 4);
            List<Integer> prels = new ArrayList<>();
            permutationfb(prels, ls);
        }
    }
    {
        {
            /**
                       8
                    4     9
            */
            fl("printBFS");
            Node root = geneBinNoImage(list(8, 4, 9));
            printBFS(root);
        }
        {
            /**
                       8
                    4     9
                  3          12
            */
            fl("printBFS");
            Node root = geneBinNoImage(list(8, 4, 9, 12, 3));
            printBFS(root);
        }
    }
    {
        {
            fl("binaryBit");
            int lo = (int)Math.pow(2, 2);
            int hi = (int)Math.pow(2, 3);
            List<String> ls = binaryBit(lo, hi);
            printList(ls);
        }
        {
            fl("binaryBitAll");
            Integer n = 4;
            List<String> ls = binaryBitAll(n);
            pl(ls);
            fl("");
            for(String s : ls){
                pl(leftPad(s, 4, '0'));
            }
        }
    }
    {
	{
            fl("houseRobberDynamic");
            Integer[] arr = {2};
            Integer max = houseRobberDynamic(arr);
	    t(max, 2);
        }
	{
            fl("houseRobberDynamic");
            Integer[] arr = {2, 3};
            Integer max = houseRobberDynamic(arr);
	    t(max, 3);
        }
        {
            fl("houseRobberDynamic");
	    Integer[] arr = {2, 3, 4};
            Integer max = houseRobberDynamic(arr);
	    t(max, 6);
        }
	{
            fl("houseRobberDynamic");
	    Integer[] arr = {2, 3, 4, 1, 9};
            Integer max = houseRobberDynamic(arr);
	    t(max, 15);
        }
    }
    {
	{
	    fl("multiplyArrExceptCurr");
	    Integer[] arr = {2, 3};
	    Integer[] arrExpected = {3, 2};
	    Integer[] ret = multiplyArrExceptCurr(arr);
	    t(ret, arrExpected);
	}
	{
	    fl("multiplyArrExceptCurr");
	    Integer[] arr = {2, 3, 4};
	    Integer[] arrExpected = {12, 8, 6};
	    Integer[] ret = multiplyArrExceptCurr(arr);
	    t(ret, arrExpected);
	}
    }
    {
	{
	    fl("maxContinuousList 1");
	    Integer[] arr = {2};
	    Integer max = maxContinuousList(arr);
	    t(max, 2);
	}
	{
	    fl("maxContinuousList 2");
	    Integer[] arr = {2, -3};
	    Integer max = maxContinuousList(arr);
	    t(max, 2);
	}
	{
	    fl("maxContinuousList 3");
	    Integer[] arr = {2, -3, 4};
	    Integer max = maxContinuousList(arr);
	    t(max, 4);
	}
	{
	    fl("maxContinuousList 4");
	    Integer[] arr = {2, -3, 4};
	    Integer max = maxContinuousList(arr);
	    t(max, 4);
	}
	{
	    fl("maxContinuousList 5");
	    Integer[] arr = {2, 3, -6, 6, 4};
	    Integer max = maxContinuousList(arr);
	    t(max, 10);
	}
    }
    {
	{
	    fl("selectionSort2");
	    Integer[] arr = {2, 1, 3, 0};
	    Integer[] ret = selectionSort2(arr);
	    Integer[] expectedArr = {0, 1, 2, 3};
	    t(ret, expectedArr);

	    printArr(ret);
	}
    }
    {
	{
	    fl("selectionSort3");
	    Integer[] arr = {2, 1, 3, 0};
	    selectionSort3(arr);
	    Integer[] expectedArr = {0, 1, 2, 3};
	    t(arr, expectedArr);
	}
    }
    {
	{
	    fl("mergeList33");
	    Integer[] arr1 = {1, 4, 9};
	    Integer[] arr2 = {2, 3, 7, 8};
	    Integer[] ret = mergeList33(arr1, arr2);
	    Integer[] expectedArray = {1, 2, 3, 4, 7, 8, 9};
	    t(ret, expectedArray);
	    printArr(ret);
	    fl("--");
	    for(Integer n : ret){
		pl(n);
	    }
	}
	{
	    fl("mergeSortedList");
	    Integer[] arr1 = {1, 4, 9};
	    Integer[] arr2 = {2, 3, 7, 8};
	    Integer[] ret = mergeSortedList(arr1, arr2);
	    Integer[] expectedArray = {1, 2, 3, 4, 7, 8, 9};
	    t(ret, expectedArray);
	    printArr(ret);
	    fl("--");
	    for(Integer n : ret){
		pl(n);
	    }
	}
    }
    {
	{
	    fl("mergeSort 1");
	    Integer[] arr = {2};
	    Integer[] expectedArray = {2};
	    int lo = 0;
	    int hi = arr.length - 1;
	    mergeSort2(arr, lo, hi);
	    t(arr, expectedArray);
	}
	{
	    fl("mergeSort 2");
	    Integer[] arr = {2, 3};
	    Integer[] expectedArray = {2, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    mergeSort2(arr, lo, hi);
	    t(arr, expectedArray);
	}
	{
	    fl("mergeSort 3");
	    Integer[] arr = {3, 2};
	    Integer[] expectedArray = {2, 3};
	    int lo = 0;
	    int hi = arr.length - 1;
	    mergeSort2(arr, lo, hi);
	    t(arr, expectedArray);
	}
	{
	    fl("mergeSort 4");
	    Integer[] arr = {3, 2, 4};
	    Integer[] expectedArray = {2, 3, 4};
	    int lo = 0;
	    int hi = arr.length - 1;
	    mergeSort2(arr, lo, hi);
	    t(arr, expectedArray);
	}
	{
	    fl("mergeSort 5");
	    Integer[] arr = {3, 2, 4, 0, 0, 2};
	    Integer[] expectedArray = {0, 0, 2, 2, 3, 4};
	    int lo = 0;
	    int hi = arr.length - 1;
	    mergeSort2(arr, lo, hi);
	    t(arr, expectedArray);
	}
	{
	    {
		fl("MyLinkedList append, toList 1");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node = new MyNode<Integer>(4);
		ll.append(node);
		List<Integer> expectedList = list(4);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList append, toList 2");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.append(node1);
		ll.append(node2);
		List<Integer> expectedList = list(4, 5);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList append, toList 3");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		MyNode<Integer> node3 = new MyNode<Integer>(6);
		ll.append(node1);
		ll.append(node2);
		ll.append(node3);
		List<Integer> expectedList = list(4, 5, 6);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    
	    {
		fl("MyLinkedList prepend, toList 4");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node = new MyNode<Integer>(4);
		ll.prepend(node);
		List<Integer> expectedList = list(4);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList prepend, toList 5");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.prepend(node1);
		ll.prepend(node2);
		List<Integer> expectedList = list(5, 4);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList prepend, toList 6");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		MyNode<Integer> node3 = new MyNode<Integer>(6);
		ll.prepend(node1);
		ll.prepend(node2);
		ll.prepend(node3);
		List<Integer> expectedList = list(6, 5, 4);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	}
	{
	    {
		fl("MyLinkedList deleteFirst, toList 1");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node = new MyNode<Integer>(4);
		ll.append(node);
		ll.deleteFirst();
		
		List<Integer> expectedList = list();
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteFirst, toList 2");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node = new MyNode<Integer>(4);
		ll.append(node);
		ll.deleteFirst();
		ll.deleteFirst();
		
		List<Integer> expectedList = list();
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteFirst, append 3");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.append(node1);
		ll.append(node2);
		ll.deleteFirst();
		
		List<Integer> expectedList = list(5);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteFirst, append 4");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		MyNode<Integer> node3 = new MyNode<Integer>(6);
		ll.append(node1);
		ll.append(node2);
		ll.append(node3);
		ll.deleteFirst();
		
		List<Integer> expectedList = list(5, 6);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	}
	
	{
	    // deleteLast
	    {
		fl("MyLinkedList deleteLast, toList 1");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node = new MyNode<Integer>(4);
		ll.append(node);
		ll.deleteLast();
		
		List<Integer> expectedList = list();
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteLast, toList 2");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node = new MyNode<Integer>(4);
		ll.append(node);
		ll.deleteLast();
		ll.deleteLast();
		
		List<Integer> expectedList = list();
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteLast, append 3");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.append(node1);
		ll.append(node2);
		ll.deleteLast();
		
		List<Integer> expectedList = list(4);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteLast, append 4");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		MyNode<Integer> node3 = new MyNode<Integer>(6);
		ll.append(node1);
		ll.append(node2);
		ll.append(node3);
		ll.deleteLast();
		
		List<Integer> expectedList = list(4, 5);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	}
	{
	    // deleteNode
	    {
		fl("MyLinkedList deleteNode 1");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		ll.append(node1);
		ll.deleteNode(node1);
		
		List<Integer> expectedList = list();
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteNode 2");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.append(node1);
		ll.append(node2);
		ll.deleteNode(node1);
		
		List<Integer> expectedList = list(5);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteNode 3");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.append(node1);
		ll.append(node2);
		ll.deleteNode(node1);
		ll.deleteNode(node2);
		
		List<Integer> expectedList = list();
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteNode 4");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.append(node1);
		ll.append(node2);
		ll.deleteNode(node2);
		
		List<Integer> expectedList = list(4);
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	    {
		fl("MyLinkedList deleteNode 5");
		MyLinkedList ll = new MyLinkedList();
		MyNode<Integer> node1 = new MyNode<Integer>(4);
		MyNode<Integer> node2 = new MyNode<Integer>(5);
		ll.append(node1);
		ll.append(node2);
		ll.deleteNode(node2);
		ll.deleteNode(node1);
		
		List<Integer> expectedList = list();
		List<Integer> ls = ll.toList();
		t(ls, expectedList);
	    }
	}
	{
	    {
		fl("PriorityQueue 1");
		PriorityQueue<Integer> q = new PriorityQueue<>((x, y) -> x - y);
		q.add(3);
		q.add(4);
		while(!q.isEmpty()){
		    Integer n = q.remove();
		    pl(n);
		}
	    }
	    {
		fl("PriorityQueue 2");
		PriorityQueue<Integer> q = new PriorityQueue<>((x, y) -> y - x);
		q.add(3);
		q.add(4);
		while(!q.isEmpty()){
		    Integer n = q.remove();
		    pl(n);
		}
	    }
	    {
		fl("PriorityQueue 3");
		PriorityQueue<Node> queue = new PriorityQueue<Node>((x, y) -> x.data - y.data);
		queue.add(new Node(1));
		queue.add(new Node(2));
		while(!queue.isEmpty()){
		    Node node = queue.remove();
		    pl(node.data);
		}
	    }
	}
	{
	    {
		fl("PriorityQueue Student");
		PriorityQueue<Student> queue = new PriorityQueue<Student>((x, y) -> x.age - y.age);
		queue.add(new Student("David", 3));
		queue.add(new Student("David", 4));
		while(!queue.isEmpty()){
		    Student student = queue.remove();
		    pl(student.toString());
		}
	    }
	    
	}
    }
    {
	{
	    fl("mergeOneNodeToSortedList 1");
	    MyLinkedList ll = new MyLinkedList();
	    MyNode node = new MyNode(0);
	    mergeOneNodeToSortedList(ll, node);
	    List<Integer> expectedList = list(0);
	    List<Integer> ls = ll.toList();
	    t(ls, expectedList);
	    ll.print();
	}
	{
	    fl("mergeOneNodeToSortedList 1");
	    MyLinkedList ll = new MyLinkedList();
	    ll.append(new MyNode(1));
	    MyNode node = new MyNode(0);
	    mergeOneNodeToSortedList(ll, node);
	    List<Integer> expectedList = list(0, 1);
	    List<Integer> ls = ll.toList();
	    t(ls, expectedList);
	    ll.print();
	}
	{
	    fl("mergeOneNodeToSortedList 1");
	    MyLinkedList ll = new MyLinkedList();
	    ll.append(new MyNode(1));
	    ll.append(new MyNode(4));
	    MyNode node = new MyNode(3);
	    mergeOneNodeToSortedList(ll, node);
	    List<Integer> expectedList = list(1, 3, 4);
	    List<Integer> ls = ll.toList();
	    t(ls, expectedList);
	    ll.print();
	}
	{
	    fl("mergeOneNodeToSortedList 2");
	    MyLinkedList ll = new MyLinkedList();
	    ll.append(new MyNode(1));
	    ll.append(new MyNode(5));
	    MyNode node1 = new MyNode(3);
	    MyNode node2 = new MyNode(4);
	    mergeOneNodeToSortedList(ll, node1);
	    mergeOneNodeToSortedList(ll, node2);
	    List<Integer> expectedList = list(1, 3, 4, 5);
	    List<Integer> ls = ll.toList();
	    t(ls, expectedList);
	    ll.print();
	}
    }
    {
	{
	    fl("mergeTwoSortedLinkedList 1");
	    MyLinkedList list1 = new MyLinkedList();
	    MyLinkedList list2 = new MyLinkedList();
	    mergeTwoSortedLinkedList(list1, list2);
	    List<Integer> ls = list2.toList();
	    List<Integer> expectedList = list();
	    t(ls, expectedList);
	    list2.print();
	}
	{
	    fl("mergeTwoSortedLinkedList 2");
	    MyLinkedList list1 = new MyLinkedList();
	    list1.append(new MyNode(2));
	
	    MyLinkedList list2 = new MyLinkedList();
	    list2.append(new MyNode(1));
	    mergeTwoSortedLinkedList(list1, list2);
	    List<Integer> ls = list2.toList();
	    List<Integer> expectedList = list(1, 2);
	    t(ls, expectedList);
	    list2.print();
	}
	{
	    fl("mergeTwoSortedLinkedList 3");
	    MyLinkedList list1 = new MyLinkedList();
	    list1.append(new MyNode(2));
	    list1.append(new MyNode(4));
	
	    MyLinkedList list2 = new MyLinkedList();
	    list2.append(new MyNode(1));
	    mergeTwoSortedLinkedList(list1, list2);
	    List<Integer> ls = list2.toList();
	    List<Integer> expectedList = list(1, 2, 4);
	    t(ls, expectedList);
	    list2.print();
	}
	{
	    fl("mergeTwoSortedLinkedList 4");
	    MyLinkedList list1 = new MyLinkedList();
	    list1.append(new MyNode(1));
	    list1.append(new MyNode(5));
	
	    MyLinkedList list2 = new MyLinkedList();
	    list2.append(new MyNode(2));
	    list2.append(new MyNode(9));
	    mergeTwoSortedLinkedList(list1, list2);
	    List<Integer> ls = list2.toList();
	    List<Integer> expectedList = list(1, 2, 5, 9);
	    t(ls, expectedList);
	    list2.print();
	}
	{
	    {
		fl("mergeTwoSortedLists 1");
		List<Integer> ls1 = list();
		List<Integer> ls2 = list();
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list();
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 2");
		List<Integer> ls1 = list(1);
		List<Integer> ls2 = list();
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1);
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 3");
		List<Integer> ls1 = list(1);
		List<Integer> ls2 = list(2);
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1, 2);
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 4");
		List<Integer> ls1 = list(2);
		List<Integer> ls2 = list(1);
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1, 2);
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 5");
		List<Integer> ls1 = list(2);
		List<Integer> ls2 = list(1);
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1, 2);
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 6");
		List<Integer> ls1 = list(2, 4, 6);
		List<Integer> ls2 = list(1);
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1, 2, 4, 6);
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 7");
		List<Integer> ls1 = list(2, 4, 6);
		List<Integer> ls2 = list(1, 5);
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1, 2, 4, 5, 6);
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 7");
		List<Integer> ls1 = list(2, 4, 6, 6, 6, 8);
		List<Integer> ls2 = list(1, 5);
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1, 2, 4, 5, 6, 6, 6, 8);
		t(ls, expectedList);
	    }
	    {
		fl("mergeTwoSortedLists 8");
		List<Integer> ls1 = list(2, 4, 6, 6, 6, 8);
		List<Integer> ls2 = list(1, 5, 9);
		List<Integer> ls = mergeTwoSortedLists(ls1, ls2);
		List<Integer> expectedList = list(1, 2, 4, 5, 6, 6, 6, 8, 9);
		t(ls, expectedList);
	    }
	}
	{
	    {
		fl("triesInsert 0");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "");
		Boolean b = triesContains(tries.root, "");
		t(b, true);
	    }
	    {
		fl("triesInsert 1");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "a");
		Boolean b = triesContains(tries.root, "a");
		t(b, true);
	    }
	    {
		fl("triesInsert 2");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		Boolean b = triesContains(tries.root, "abc");
		t(b, true);
	    }
	    {
		fl("triesInsert 3");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		Boolean b = triesContains(tries.root, "");
		t(b, true);
	    }
	    {
		fl("triesInsert 4");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		Boolean b = triesContains(tries.root, "a");
		t(b, false);
	    }
	    {
		fl("triesInsert 5");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		Boolean b = triesContains(tries.root, "ab");
		t(b, false);
	    }
	    {
		fl("triesInsert 6");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		Boolean b = triesContains(tries.root, "abc");
		t(b, true);
	    }
	    {
		fl("triesInsert 7");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		triesInsert(tries.root, "abd");
		Boolean b = triesContains(tries.root, "ab");
		t(b, false);
	    }
	    {
		fl("triesInsert 8");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		triesInsert(tries.root, "abd");
		Boolean b = triesContains(tries.root, "a");
		t(b, false);
	    }
	    {
		fl("triesInsert 9");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		triesInsert(tries.root, "abd");
		Boolean b = triesContains(tries.root, "abc");
		t(b, true);
	    }
	    {
		fl("triesInsert 10");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		triesInsert(tries.root, "abd");
		Boolean b = triesContains(tries.root, "abd");
		t(b, true);
	    }
	    {
		fl("triesInsert 11");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		triesInsert(tries.root, "bd");
		Boolean b = triesContains(tries.root, "ab");
		t(b, false);
	    }
	    {
		fl("triesInsert 12");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		triesInsert(tries.root, "bd");
		Boolean b = triesContains(tries.root, "bc");
		t(b, false);
	    }
	    {
		fl("triesInsert 13");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "abc");
		triesInsert(tries.root, "bd");
		Boolean b = triesContains(tries.root, "bd");
		t(b, true);
	    }
	    {
		fl("triesInsert 14");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "apple");
		triesInsert(tries.root, "banana");
		Boolean b = triesContains(tries.root, "banana");
		t(b, true);
	    }
	    {
		fl("triesInsert 15");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "apple");
		triesInsert(tries.root, "banana");
		Boolean b = triesContains(tries.root, "anana");
		t(b, false);
	    }
	    {
		fl("triesInsert 16");
		MyTries tries = new MyTries();
		triesInsert(tries.root, "apple");
		triesInsert(tries.root, "banana");
		triesInsert(tries.root, "banana");
		triesInsert(tries.root, "cherry");
		triesInsert(tries.root, "apple");
		triesInsert(tries.root, "cherry");
		Boolean b1 = triesContains(tries.root, "banana");
		Boolean b2 = triesContains(tries.root, "apple");
		Boolean b3 = triesContains(tries.root, "cherry");
		t(b1, true);
		t(b2, true);
		t(b3, true);
	    }
	}
	{
	    fl("Read macOS dictionary");
	    String dictPath = "/usr/share/dict/words";
	    List<String> ls = readFile(dictPath);
	    MyTries tries = new MyTries();

	    for(String s : ls){
		pl(s);
		triesInsert(tries.root, s);
	    }
	    pl(len(ls));
	    Boolean b1 = triesContains(tries.root, "apple");
	    t(b1, true);
	    Boolean b2 = triesContains(tries.root, "restaurant");
	    t(b2, true);
	    Boolean b3 = triesContains(tries.root, "Apple");
	    t(b3, true);
	}
    }
    end();
    }

    /**
                 5 4 2 1 3
                 2 1 [3] 5 4
             q(2 1)     q(5 4)
           q(2)       q(5)
             
    */
    public static void quickSort(Integer[] arr, int lo, int hi){
        if(lo < hi){
            Integer pivotIndex = partitionInLine(arr, lo, hi);
            quickSort(arr, lo, pivotIndex - 1);
            quickSort(arr, pivotIndex + 1, hi);
        }
    }

    /**
        KEY: Use swap only to partition the array   
   
        http:____ //localhost/image/partitionInLine.png



         |
         |                          x
         |         x            x       x
         -->    x       x                   x  <-
         |  x            x  x
         |----------------------------------------

     */
    public static Integer partitionInLine(Integer[] arr, int lo, int hi){
        int pInx = lo;
        int big = lo;
        int pivot = arr[hi];
        for(int i = lo; i <= hi; i++){
            if(arr[i] <= pivot){
                swap(arr, big, i);
                pInx = big;
                big++;
            }
        }
        return pInx;
    }

    /**

        https://leetcode.com/problems/longest-substring-without-repeating-characters/

        ab   a b x
        aa   a x
    */
    public static Integer longestSubstringWithoutRepeatingChar(String s){
        Integer max = 0;
        Set<Character> set = new HashSet<>();
        for(int i = 0; i < s.length(); i++){
            for(int j = i; j < s.length(); j++){
                char c = s.charAt(j);
                if(set.contains(c)){
                    String sub = s.substring(i, j);
                    max = Math.max(max, sub.length());
                    set.clear();
                }else{
                    set.add(c);
                    if(j == s.length() - 1){
                        String sub = s.substring(i, j + 1);
                        max = Math.max(max, sub.length());
                    }
                }
            }
        }
        return max;
    }


    public static void permutation(List<Integer> ls, List<Integer> als){
        if(als.size() == 0){
            printList(ls);
        }else{
            for(int i = 0; i < als.size(); i++){
                List<Integer> newls = removeIndex(i, als); 
                permutation(append(ls, als.get(i)), newls);
            }
        }
    }

    /**
     */
    public static void permutationNoAdjacent(List<MyPair> ls, List<MyPair> als){
        if(als.size() == 0){
            printList(ls);
        }else{
            for(int i = 0; i < als.size(); i++){
                // permutationNoAdjacent(appendp(ls, als.get(i)), removeIndex(i, als));
		Boolean b = false;
		if(len(ls) > 0){
		    MyPair pair = last(ls);
		    b = als.get(i).x - pair.x == 1 ? true : false;
		}
		permutationNoAdjacent(b ? ls : appendp(ls, als.get(i)), drop(i + 1, als));
            }
        }
    }
    
    public static void permutationfb(List<Integer> prels, List<Integer> ls){
        if(ls.size() == 0){
            printList(prels);
        }else{
            for(int i = 0; i < ls.size(); i++){
                permutationfb(append(prels, ls.get(i)), drop(i + 1, ls));
            }
        }
    }
    

    public static List<MyPair> appendp(List<MyPair> ls, MyPair p){
        List<MyPair> ret = new ArrayList<>();
        for(MyPair pair : ls)
            ret.add(pair);
        ret.add(p);

        return ret;
    }

    /**
        https://leetcode.com/problems/combination-sum/
    */
    public static void combineSum(List<Integer> ls, Integer s){
    }
 
    /**
        https://leetcode.com/problems/subsets/

        Input: nums = [1,2,3]
        Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
    */
    public static void powSet(List<Integer> ls){
    }

    /**
        https://leetcode.com/problems/is-subsequence/

        Check whether a string is a subsequence of given a string

        subsequence => abcdefghij klmn opq rst uvw xyz  => az
                    => guilt => gut

        abc
        ""

        abc
        ab

        abc
        abc

        abc
         bc

        abc
        b

        abc
        d
    */
    public static Boolean isSubsequence(String givenStr, String sub){
        Boolean ret = false;
        int i = 0;
        int j = 0;
        int len = givenStr.length();
        int lens = sub.length();
        if(lens == 0){
            ret = true;
        }else{
            if(len >= lens){
                while(i < len && j < lens){
                    if(givenStr.charAt(i) == sub.charAt(j)){
                        i++;
                        j++;
                    }else{
                        i++;
                    }
                }
                if(j == lens)
                    ret = true;
            }
        }
        return ret;
    }

    /**
       leetcode
       https://leetcode.com/problems/house-robber/

       dynamic programming solution
       {1, 9, 7, 5}
       [0] = 1
       [1] = 9 // max([0], [1])
       [2] = 14 // max([0] + [2], [1])

       {9, 7, 5}


       https://dev.to/urfan/leetcode-house-robber-with-javascript-4ogn

       1 2 4 => 1 + 4 = 5

       1 3 9 6 => 1 9, 3 6, 1 6, 
               => 10, 9, 7
               => 10

                1 3 9 6
           [1]                 [3]           [9]           [6]
         3 9 6               1 9 6          1 3 6         1 3 9
       [3]  [9]  [6]    [1]  [9] [6]     [1] [3]  [6]    [1]  [3]  [9]
      9 6   3 6  3 9    9 6  1 7  1 9   3 6  1 6  1 3   3 9   1 9  1 3

    */
    public static void houseRobber(List<MyPair> pls, List<Integer> ls, Integer inx){
        if(ls.size() == 0){
            for(MyPair p : pls){
                pp("inx=" + p.x + " " + p.y + "--");
            }
            pl("");
        }else{
            for(int i = 0; i < ls.size(); i++){
                MyPair p = new MyPair(i, ls.get(i));
                houseRobber(appendp(pls, p), drop(i + 1, ls), i);
            }
        }
    }

    /**
          4 2 6
     */
    public static Integer houseRobberDynamic(Integer[] arr){
	Integer[] dn = new Integer[arr.length];
	if(arr.length > 0){
	    dn[0] = arr[0];
	    if(arr.length > 1)
		dn[1] = Math.max(dn[0], arr[1]);
	    
	    for(int i = 2; i < arr.length; i++){
		dn[i] = Math.max(dn[i - 1], dn[i - 2] + arr[i]);
	    }
	}
	return dn[dn.length - 1];
    }

    /**
       leetcode
       https://leetcode.com/problems/find-k-pairs-with-smallest-sums/
    */
    public static List<MyPair> kSmallestPair(Integer[] arr1, Integer[] arr2, Integer k){
           List<MyPair> ls = new ArrayList<>();
           Integer kcount = 0;
           MyPair pair = new MyPair(0, 0); 
           Integer min = Integer.MAX_VALUE; 
           Integer px = null;
           Integer py = null;
           int len1 = arr1.length;
           int len2 = arr2.length;
           int x1 = 0;
           int x2 = 0;
           while(x1 < len1 || x2 < len2){
               if(x1 >= len1){
                 if(py == null){
                     py = arr2[x2];
                 }
                 x2++;
               }else if(x2 >= len2){
                 if(px == null){
                     px = arr1[x1];
                 }
                 x1++;
               }else{
                 if(arr1[x1] < arr2[x2]){
                     if(px == null)
                         px = arr1[x1];
                     x1++;
                 }else{
                     if(py == null);
                        py = arr2[x2];
                     x2++;
                 }
               }
               if(px != null && py != null){
                   ls.add(new MyPair(px, py));
                   kcount++; 
                   if(kcount >= k)
                      break; 

                   px = null;
                   py = null;
               }
           }
           return ls;
    }

    /**
          3 1 4 2

    */
    public static void selectionSort(Integer[] arr, int lo, int hi){
        if(lo < hi){
            for(int i = lo + 1; i <= hi; i++){
                for(int j = i; j > lo; j--){
                    if(arr[j-1] > arr[j]){
                        swap(arr, j-1, j);
                    }
                }
            }
        }
    }
    
    public static Integer partition(Integer[] arr, int lo, int hi){
	int len = arr.length;
	Integer[] array = new Integer[len];
	int i = 0;
	int j = len - 1;
	Integer pivot = arr[hi];
	int k = lo;
	while(i < j){
	    if(arr[k] < pivot){
		array[i] = arr[k];      // i -> [x]  pivot
		i++;
	    }else {
		array[j] = arr[k];      // pivot [x]    <- j
		j--;
	    }
	    k++;
	}
	array[i] = pivot;
	
	for(int x = 0; x < len; x++){
	    arr[lo + x] = array[x];
	}
	return i;
    }


    /**
       leetcode question
       https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/
     */
    public static void zigzagTraveral(Node root){
        Queue<Node> q1 = new LinkedList<>();
        Queue<Node> q2 = new LinkedList();
        Stack<Node> stack = new Stack<>();
        if(root != null){
            q1.add(root);

            while(!q1.isEmpty() || !q2.isEmpty()){
                while(!q1.isEmpty()){
                    Node node = q1.remove();
                    pp(node.data + " ");
                    if(node.left != null)
                        q2.add(node.left);
                    if(node.right != null)
                        q2.add(node.right);
                }
                fl();
                
                while(!q2.isEmpty()){
                    Node node = q2.remove();
                    stack.add(node);
                    // pp(node.data + " ");
                    if(node.left != null)
                        q1.add(node.left);
                    if(node.right != null)
                        q1.add(node.right);
                }
                while(!stack.isEmpty()){
                    Node node = stack.pop();
                    pp(node.data + " ");
                }
                fl();
            }
        }
    }
    
    public static void levelTraveral(Node root){
        Queue<Node> q1 = new LinkedList<>();
        Queue<Node> q2 = new LinkedList();
        Stack<Node> stack = new Stack<>();
        if(root != null){
            q1.add(root);

            while(!q1.isEmpty() || !q2.isEmpty()){
                while(!q1.isEmpty()){
                    Node node = q1.remove();
                    pp(node.data + " ");
                    if(node.left != null)
                        q2.add(node.left);
                    if(node.right != null)
                        q2.add(node.right);
                }
                fl();
                
                while(!q2.isEmpty()){
                    Node node = q2.remove();
                    stack.add(node);
                    pp(node.data + " ");
                    if(node.left != null)
                        q1.add(node.left);
                    if(node.right != null)
                        q1.add(node.right);
                }
                while(!stack.isEmpty()){
                    Node node = stack.pop();
                    // pp(node.data + " ");
                }
                fl();
            }
        }
    }

    /**
         "" => []
         "a" => 
    */
    public static List<String> prefixStr(String s){
        List<String> ls = new ArrayList<>();
        for(int i = 0; i < s.length(); i++){
           String str = s.substring(0, i + 1);
           ls.add(str);
        }
        return ls;
    }

    /**
         a b c d e

         a b c [ ]
               [d] e
         
    */
    public static void prefixSuffix(String s){
        List<String> prefix = new ArrayList<>();
        List<String> suffix = new ArrayList<>();
        for(int i = 0; i < s.length(); i++){
            String ps = s.substring(0, i+1);
            String ss = s.substring(i+1, s.length());
            prefix.add(ps);
            suffix.add(ss);
        }
        pl("prefix len=" + len(prefix));
        pp(prefix);
        fl();
        pl("suffix len=" + len(suffix));
        pp(suffix);
    }

    /**
        _ _ ab_  => ["ab"]
        ab _ _   => ["ab"]
        ab _ _ c  => ["ab", "c"]
    */
    public static List<String> splitStr(String s){
        String accumulator = "";
        List<String> ls = new ArrayList<>();
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(c == ' '){
                if(accumulator.length() > 0){
                    ls.add(accumulator);
                    accumulator = "";
                }
            }else{
                String cStr = c + "";
                accumulator += cStr;
            }
        }
        if(accumulator.length() > 0)
            ls.add(accumulator);

        return ls;
    }

    /**
       Copy a binary tree
     */
    public static Node copyBinTree(Node root){
	if(root != null){
	    Node left = copyBinTree(root.left);
	    Node right = copyBinTree(root.right);
	    Node node = new Node(root.data);
	    node.left = left;
	    node.right = right;
	    return node;
	}else{
	    return null;
	}
    }

    /**
         Node 9 only have left subtree, add a right subtree to node 9

             9      =>     9
          4             4    10


	  Nothing to be filled, each node have both subtrees or no subtree
	  
             9       =>     9
          4     11      4       11
     */
    public static Node padBinTree(Node root){
	if(root != null){
	    Node left = padBinTree(root.left);
	    Node right = padBinTree(root.right);
	    Node node = new Node(root.data);
	    if((left == null && right == null) || (left != null && right != null)){
		node.left = left;
		node.right = right;
	    }else{
		node.left = left == null ? new Node(root.data - 1)   : left;
		node.right = right == null ? new Node(root.data + 1) : right;
		
	    }
	    return node;
	}else{
	    return null;
	}
    }

    

    /**
       leetcode question
       https://leetcode.com/problems/merge-two-binary-trees/
       
     */
    public static Node mergeBinTree(Node root1, Node root2){
	if(root1 != null && root2 != null){
	    Node left = mergeBinTree(root1.left, root2.left);
	    Node right = mergeBinTree(root1.right, root2.right);
	    Node node = new Node(root1.data + root2.data);
	    node.left = left;
	    node.right = right;
	    return node;
	}else if(root1 != null){
	    Node left = mergeBinTree(root1.left, null);
	    Node right = mergeBinTree(root1.right, null);
	    Node node = new Node(root1.data);
	    node.left = left;
	    node.right = right;
	    return node;
	}else if(root2 != null){
	    Node left = mergeBinTree(root2.left, null);
	    Node right = mergeBinTree(root2.right, null);
	    Node node = new Node(root2.data);
	    node.left = left;
	    node.right = right;
	    return node;		
	}else{
	    return null;
	}
    }
    
    /**
                0 1 0 0
                0 1 1 0
                1 0 0 1
    */
    public static Integer islandConnection(Integer[][] arr, Integer h, Integer w){
        Integer ret = 0;
        Integer height = arr.length;
        Integer width = arr[0].length;
        if(arr[h][w] == 1){
            Integer tmp = arr[h][w];
            arr[h][w] = 0;
            Integer n0 = 0, n1 = 0, n2 = 0, n3 = 0;
            if(h + 1 < height)
                n0 = islandConnection(arr, h + 1, w);
            if(h - 1 >= 0)
                n1 = islandConnection(arr, h - 1, w);
            if(w + 1 < width)
                n2 = islandConnection(arr, h, w + 1);
            if(w - 1 >= 0)
                n3 = islandConnection(arr, h, w - 1);
            arr[h][w] = tmp;
            ret = n0 + n1 + n2 + n3 + 1;
        }
        return ret;
    }

    /**
            [h-1][w-1] [h-1][w]   [h-1][w+1]
                          |
            [h][w-1] <- [h][w] -> [h][w+1]
	                  |
            [h+1][w-1]  [h+1][w]  [h+1][w+1]

     */
    public static Integer islandConnection2(Integer[][] arr, Integer h, Integer w){
	Integer c = 0;
	Integer height = arr.length;
	Integer width  = arr[0].length;
	if(arr[h][w] == 1){
	    Integer tmp = arr[h][w];
	    arr[h][w] = 0;
	    for(int hh = -1; hh <= 1; hh++){
		for(int ww = -1; ww <= 1; ww++){
		    if(Math.abs(hh) != Math.abs(ww)){
			if((0 <= h + hh && h + hh < height) && (0 <= w + ww && w + ww < width) ){
			    c += islandConnection2(arr, h + hh, w + ww);
			    pl("h=" + h + " w=" + w + " hh=" + hh + " ww=" + ww);
			}
		    }
		}
	    }
	    arr[h][w] = tmp;
	    return c + 1;
	}
	return 0;
    }

    public static Integer maxConnection(Integer[][] arr, Integer h, Integer w){
        Integer max = 0;
        for(int hh = 0; hh < arr.length; hh++){
            for(int ww = 0; ww < arr[0].length; ww++){
                Integer m = islandConnection(arr, hh, ww); 
                max = Math.max(m, max);
            }
        }
        return max;
    }

    public static Integer maxConnection2(Integer[][] arr, Integer h, Integer w){
        Integer max = 0;
        for(int hh = 0; hh < arr.length; hh++){
            for(int ww = 0; ww < arr[0].length; ww++){
                Integer m = islandConnection2(arr, hh, ww); 
                max = Math.max(m, max);
            }
        }
        return max;
    }

   
   /**
      KEY: Lease Common Ancessor, LCA
   */
    public static Node LCA(Node root, Node n1, Node n2){
        if(root != null){
            Node l = null, r = null;
            if(n1.data < root.data)
                l = LCA(root.left, n1, n2);
            else if(n1.data > root.data)
                r = LCA(root.right, n1, n2);
            else
                return root;

            if(n2.data < root.data)
                l = LCA(root.right, n1, n2);
            else if(n2.data > root.data)
                r = LCA(root.right, n1, n2);
            else
                return root;

            if(l != null && r != null)
                return root;
            else if(l != null)
                return l;
            else if(r != null)
                return r;
        }
        return null;
    }

    /**
        add two array integer,

            (1)
            9  9
               9
         ----------
          1 0  8


          (1)(1)
             9  9
             8  9
          ----------
           1 8  8


                9 9
              0 9 9
            0 0 9 9

            8 9 0 0
              x   9
            x   8 


            7 8 9 0 0
                x   9
              x   8
            x   7

            for(int i = len - 1; i >= 0; i--)
               [i + n ] = [i]

    */
    public static Integer addArray(Integer[] arr1, Integer[] arr2){
        BiFunction<Integer[], Integer[], Integer[]> f = (x, y) -> x;
        return 1;
    }

    public static Integer[] mergeSortedList(Integer[] arr1, Integer[] arr2){
        Integer[] arr = new Integer[arr1.length + arr2.length];
        int i = 0;
        int j = 0;
        int k = 0;

        while(i < arr1.length || j < arr2.length){
            if(i >= arr1.length){
                arr[k] = arr2[j];
                j++;
            }else if(j >= arr2.length){
                arr[k] = arr1[i];
                i++;
            }else{
                if(arr1[i] < arr2[j]){
                    arr[k] = arr1[i];
                    i++;
                }else{
                    arr[k] = arr2[j];
                    j++;
                }
            }
            k++;
        }
        return arr;
    }

    public static Integer multiArray(Integer[] arr1, Integer[] arr2){
        return 1;
    }

    public static Integer pow(Integer n, Integer k){
        return 1;
    }

    public static Boolean isBSTRecursion(Node root){
        return true;
    }

    public static Boolean isBSTDefintion(Node root){
        if(root != null){
            if(!isBSTRecursion(root.left))
                return false;
            if(root.left != null && maxLeftsubtree(root.left).data > root.data ||
               root.right != null && maxRighsubtree(root.right).data < root.data)
                return false;

            if(!isBSTRecursion(root.right))
                return false;
        }
        return true;
    }

    public static Integer countLevel(Node root){
        if(root != null){
            Integer l = countLevel(root.left);
            Integer r = countLevel(root.right);
            return Math.max(l, r) + 1;
        }
        return 0;
    }

    public static Node maxLeftsubtree(Node root){
        Node tmp = root;
        while(tmp.right != null)
            tmp = tmp.right;
        return tmp;
    }

    public static Node maxRighsubtree(Node root){
        Node tmp = root;
        while(tmp.left != null)
           tmp = tmp.left; 
        return tmp;
    }

    public static Node insertBin2(Node root, Node node){
        if(root == null)
            return node;
        else{
            insertBinaryTree(root, node);
            return root;
        }
    }

    public static void insertBinaryTree(Node root, Node node){
        if(node.data < root.data){
            if(root.left == null){
                root.left = node;
            }else{
                insertBinaryTree(root.left, node);
            }
        }else{
            if(root.right == null){
                root.right = node;
            }else{
                insertBinaryTree(root.right, node);
            }
        }
    }

    public static List<String> binaryBit(Integer lo, Integer hi){
        List<String> ls = new ArrayList<>();
        for(int i = lo; i < hi; i++){
            ls.add(Integer.toBinaryString(i));
        }
        return ls;
    }
    public static List<String> binaryBitAll(Integer n){
        List<String> ret = new ArrayList<>();
        for(int i = 0; i < n; i++){
            int lo = (int)Math.pow(2, i);
            int hi = (int)Math.pow(2, i+1);
            List<String> ls = binaryBit(lo, hi);
            ret.addAll(ls);
        }
        return ret;
    }


    public static void printBFS(Node root){
        if(root != null){
            Queue<Node> queue = new LinkedList<>();
            queue.add(root);
            breathFirstSearch(queue);
        }
    }

    public static void breathFirstSearch(Queue<Node> queue){
        Queue<Node> q = new LinkedList<>();
        while(!queue.isEmpty()){
            Node node = queue.remove();
            pp(node.data + " ");

            if(node.left != null)
                q.add(node.left); 
            if(node.right != null)
                q.add(node.right); 
        }
        if(!q.isEmpty()){
            pl("");
            breathFirstSearch(q);
        }
    }


    public static Integer kthsmallest(Integer[] arr, Integer lo, Integer hi){
        return 0;
    }

    /**
        heapifty
        bubble up, bubble down
    */
    public static void heapSort(){
    }

    public static Node serializeBinTree(Node r){
        return null;
    }

    public static Node deserializeBinTree(Node r){
        return null;
    }

    public static Integer[] catArr(Integer[] a, Integer[] b){
        Integer lena = a.length;
        Integer lenb = b.length;
        Integer[] arr = new Integer[lena + lenb];
        for(int i = 0; i < lena + lenb; i++){
            arr[i] = i < lena ? a[i] : b[i - lena];
        }
        return arr;
    }


    /**
         _ _ ab _ c
         ab _ _ c
    */
    public static Tup firstWord(String s){
        String acc = "";
        int i = 0;
        for(i=0; i < s.length(); i++){
            if(s.charAt(i) == ' ' && acc.length() == 0){
    
            }else if(s.charAt(i) == ' ' && acc.length() > 0){
                break;
            }else{
               String ss = s.charAt(i) + "";
               acc += ss;
            }
        }
        return new Tup(acc, s.substring(i, s.length()));
    }

    /**
           2 3 4 5

	   |-> 3 4 5
	     |-> 2 4 5
	       |-> 2 3 5
	         |-> 2 3 4

	   [2] 3   4  5
	   2   [3] 4  5
	   2   3  [4] 5
	   2   3   4 [5]

     */
    public static Integer[] multiplyArrExceptCurr(Integer[] arr){
	int len = arr.length;
	Integer[] a = new Integer[len];
	Integer[] b = new Integer[len];
	a[0] = 1;
	b[len-1] = 1;
	for(int i = 0; i < len-1; i++){
	    int j = (len - 1) - i;
	    a[i+1] = a[i] * arr[i];    // [x, x, []]     
	    b[j-1] = b[j] * arr[j];  // [[], x, x]
	}
	for(int i = 0; i < len; i++){
	    a[i] = a[i]*b[i];
	}
	return a;
    }

    /**
         2 3 -4 5 1
	 2 = 2
	 2 + 3 = 5  <-
	 2 3 - 4 = 1, 4
	 2 3 - 4 5 = 6, 9
	 2 3 - 4 5 1 =


	 2 3 -6 3 2 4

	 2 3 -4 3 2 4
         
     */
    public static Integer maxContinuousList(Integer[] arr){
	Integer sum = 0;
	Integer max = 0;
	for(int i = 0; i < arr.length; i++){
	    sum += arr[i];
	    if (sum < 0)
		sum = 0;
	    max = Math.max(max, sum);
	}
        return max;

    }
    /**
       swap from the left side
         4 2 3 1

	 2 4 [3]
	 2 3 4  [1]
	 2 3 1 4
	 2 1 3 4
	 1 2 3 4
     */
    public static Integer[] selectionSort2(Integer[] arr){
	int len = arr.length;
	Integer[] ret = new Integer[len];
	
	for(int i = 0; i < len; i++){
	    ret[i] = arr[i];
	}
	if(len > 1){
	    for(int i = 1; i < len; i++){
		for(int j = i; j >= 0; j--){
		    if(j - 1 >= 0 && ret[j-1] > ret[j]){
			int tmp = ret[j];
			ret[j] = ret[j - 1];
			ret[j - 1] = tmp;
		    }
		}
	    }
	}
	return ret;
    }

    /**
       swap from the right side
     */
    public static void selectionSort3(Integer[] arr){
	int len = arr.length;
	int lastInx = len - 1;
	if(len > 1){
	    for(int i = lastInx - 1; i >= 0; i--){
		for(int j = i ; j < len; j++){
		    if(j + 1 < len && arr[j] > arr[j+1]){
			int tmp = arr[j];
			arr[j] = arr[j+1];
			arr[j+1] = tmp;
		    }
		}
	    }
	}
    }


    public static Integer[] mergeList33(Integer[] arr1, Integer[] arr2){
	int len1 = arr1.length;
	int len2 = arr2.length;
	int i = 0;
	int j = 0;
	int k = 0;
	Integer[] arr = new Integer[len1 + len2];
	
	while(i < len1 || j < len2){
	    if(i >= len1){
		arr[k] = arr2[j];
		j++;
	    }else if(j >= len2){
		arr[k] = arr1[i];
		i++;
	    }else{
		if(arr1[i] < arr2[j]){
		    arr[k] = arr1[i];
		    i++;
		}else{
		    arr[k] = arr2[j];
		    j++;
		}
	    }
	    k++;
	}
	return arr;
    }

    public static void mergeList44(Integer[] arr, int lo, int mid, int hi){
	Integer[] array = new Integer[hi - lo + 1];
	int k = 0;
	int i = lo;
	int j = mid + 1;
	while(i <= mid || j <= hi){
	    if(i > mid){
		array[k] = arr[j];
		j++;
	    }else if(j > hi){
		array[k] = arr[i];
		i++;
	    }else{
		if(arr[i] < arr[j]){
		    array[k] = arr[i];
		    i++;
		}else{
		    array[k] = arr[j];
		    j++;
		}
	    }
	    k++;
	}
	for(int x = 0; x < array.length; x++)
	    arr[lo + x] = array[x];
    }

    /**
          4 1 3 2  5

	4 1 3    2   3

      4 1   2   2       3

    4   1

      1 4  2

      1 2 4          
     */
    public static void mergeSort2(Integer[] arr, int lo, int hi){
	if(lo < hi){
	    int mid = (lo + hi)/2;
	    mergeSort2(arr, lo, mid);
	    mergeSort2(arr, mid+1, hi);
	    mergeList44(arr, lo, mid, hi);
	}
    }

    /**
       Merge two sorted linkedlists
     */
    public static void mergeSortedLinkedList(MyNode first1, MyNode first2){
	
    }

    /**
       
       [3 6 8]   [5]

       [3 6 8]   [5]

            (8, 5)
	3 6   5  8
	 (6, 5)

       3  5 6 8
     */
    public static void mergeOneNodeToSortedList(MyLinkedList ll, MyNode<Integer> node){
	if(ll.first == null){
	    // empty linked list
	    ll.first = ll.last = node;
	}else{
	    // merge to sorted list
	    ll.append(node);
	    MyNode<Integer> curr = ll.last;
	    while(curr != null){
		MyNode<Integer> prev = curr.prev;
		if(prev != null){
		    // swap data
		    if(prev.data > curr.data){
			Integer tmpAge = prev.data;
			prev.data = curr.data;
			curr.data = tmpAge;
		    }
		}
		curr = curr.prev;
	    }
	}
    }
    
    public static void mergeTwoSortedLinkedList(MyLinkedList list1, MyLinkedList list2){
	MyNode curr1 = list1.first;
	MyNode curr2 = list2.last;
	while(curr1 != null){

	    MyNode node = curr1;
	    list1.deleteFirst();
	    mergeOneNodeToSortedList(list2, node);

	    curr1 = list1.first;
	}
    }

    
    public static List<Integer> mergeTwoSortedLists(List<Integer> ls1, List<Integer> ls2){
	List<Integer> ls = new ArrayList<>();
	if(ls1 != null && ls2 != null){
	    int len1 = ls1.size();
	    int len2 = ls2.size();

	    int i = 0, j = 0;
	    while(i < len1 || j < len2){
		if(i >= len1){
		    ls.add(ls2.get(j));
		    j++;
		}else if(j >= len2){
		    ls.add(ls1.get(i));
		    i++;
		}else{
		    if(ls1.get(i) < ls2.get(j)){
			ls.add(ls1.get(i));
			i++;
		    }else{
			ls.add(ls2.get(j));
			j++;
		    }
		}
	    }
	}
	return ls;
    }

    
    /**
       KEY: Tries data structure
       insert string to the tries
     */
    public static void triesInsert(TRNode root, String s){
		if(s.length() > 0){
			char c = s.charAt(0);
			if(root.map.containsKey(c)){
				triesInsert(root.map.get(c), s.substring(1, s.length()));
				pl("c=" + c);
			}else{
				root.map.put(c, new TRNode());
				triesInsert(root.map.get(c), s.substring(1, s.length()));
			}
		}else{
			root.isWord = true;
		}
    }

    public static Boolean triesContains(TRNode root, String s){
	if(s.length() > 0){
	    char c = s.charAt(0);
	    if(root.map.containsKey(c)){
		return triesContains(root.map.get(c), s.substring(1, s.length()));
	    }else{
		return false;
	    }
	}else{
	    return root.isWord;
	}
    }
}

class TRNode{
    public Map<Character, TRNode> map = new HashMap<>();
    public Boolean isWord;
    public TRNode(Boolean isWord){
	this.isWord = isWord;
    }
    public TRNode(){
	this.isWord = false;
    }

}

class MyTries{
    TRNode root;
    public MyTries(){
	root = new TRNode(true);
    }
}
