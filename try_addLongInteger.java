import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
 * Wed Nov 13 10:23:26 2019 
 * KEY: add two long integer, add integer
 *
 */

class Pair{
    int fst;
    int snd;
    public Pair(int fst, int snd){
        this.fst = fst;
        this.snd = snd;
    }
    public String toString(){
        return "(" + fst + " " + snd + ")";
    }
}


public class try_addLongInteger{
    public static void main(String[] args) {
        test0();

        test1();
    }
    public static void add(Integer[] a1, Integer[] a2){
        List<Pair> list1 = new ArrayList<>();
        List<Pair> list2 = new ArrayList<>();
        int len1 = a1.length;
        int len2 = a2.length;
        List<Integer> n1 = Arrays.asList(a1);
        List<Integer> n2 = Arrays.asList(a2);
        BiFunction<Integer, Integer, Integer> reverseF = (x, y) -> -(x - y);   // 3, 2, 1
        List<Integer> s1 = zipWith((x, y) -> new Pair(x, y), n1, range(len1).sort(reverseF));

        List<Pair> ls = mergeSortedListsAny(list1, list2, (x, y) -> -(x.snd - y.snd));  // 3, 2, 1 
        // groupBy
        // foldl 

        pl(ls);
    }
    public static void test0(){
        beg();
        {
            Integer[] arr1 = {1};
            Integer[] arr2 = {2};
            add(arr1, arr2);
        }
        {
            Integer[] arr1 = {1, 2};
            Integer[] arr2 = {1};
            add(arr1, arr2);
        }
        {
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9};
            add(arr1, arr2);
        }
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

