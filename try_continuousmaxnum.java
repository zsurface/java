import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Triple{
    Integer beg;
    Integer end;
    Integer max;
    public Triple(Integer beg, Integer end, Integer max){
        this.beg = beg;
        this.end = end;
        this.max = max;
    }
    public void print(){
        p("beg=" + beg + " end=" + end + " max=" + max);
    }
}

public class try_continuousmaxnum{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();

        {
            List<Integer> list = Arrays.asList(1, 2, -1, 3);
            int max = continuousSum(list);
            p(max == 5);
        }
        {
            List<Integer> list = Arrays.asList(1, -2, -1, 3);
            int max = continuousSum(list);
            p(max == 3);
        }
        {
            List<Integer> list = Arrays.asList(1, -2, -1, 3);
            Triple triple = continuousSumIndex(list);
            triple.print();
        }
        {
            List<Integer> list = Arrays.asList(1, 2, -1, 3);
            Triple triple = continuousSumIndex(list);
            triple.print();
        }
        {
            List<Integer> list = Arrays.asList(1, -1);
            Triple triple = continuousSumIndex(list);
            triple.print();
        }
        {
            List<Integer> list = Arrays.asList(-1, 1);
            Triple triple = continuousSumIndex(list);
            triple.print();
        }
        {
            List<Integer> list = Arrays.asList(1);
            Triple triple = continuousSumIndex(list);
            triple.print();
        }
        {
            List<Integer> list = Arrays.asList(1, -1, 3);
            Triple triple = continuousSumIndex(list);
            triple.print();
        }
        {
            // 0 -> 1
            // 1 -> 2
            // 0 -> 2
            Integer[][] arr2d = {
                { 0,   2,   1},
                { 0,   0,   4},
                { 0,   0,   0}
            };
            int k = 0;
            int max = adjacentMatrix(arr2d, k);
            p(max);

        }

        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    // 1 2 -1 3
    // 1 2 -1 3 => 5
    //
    // -1 2 3
    //  
    public static int continuousSum(List<Integer> list){
        int sum = 0;
        int max = 0;
        if( list != null){
            for(Integer n : list){
                sum += n;
                if(sum < 0){
                    sum = 0;
                }else{
                   max = Math.max(sum, max);
                }
            }
        }
        return max;
    }

    public static Triple continuousSumIndex(List<Integer> list){
        int maxBeg = 0;
        int maxEnd = 0;
        int currBeg = 0;
        int currEnd = 0;
        int sum = 0;
        int max = 0;
        if(list != null){
            for(int i=0; i<list.size(); i++){
                sum += list.get(i);        
                if(sum < 0){
                    sum = 0;
                    if(i + 1 < list.size()){
                        // reset index
                        currBeg = i + 1;
                        currEnd = i + 1;
                    }
                }else{
                    if(sum > max){
                        // find max, update currEnd, maxBeg and maxEnd indexes
                        max = sum;
                        currEnd = i;
                        maxBeg = currBeg;
                        maxEnd = currEnd;
                    }else{
                        currEnd = i;
                    }
                }
            }
        }
        return new Triple(maxBeg, maxEnd, max);
    }

    /***
     * find the maximum path from a given node.
     *
     * @arr - adjacent matrix represents a graphic.
     *
     * @k - represents a node from a graphic, k=0, 1, 2, ...
     *
     * @return - a maximum weight of a path in all paths from a given node.
     */
    public static int adjacentMatrix(Integer[][] arr, int k){
       int max = 0;
       if(arr != null){
           for(int i=0; i<arr[0].length; i++){
             if(arr[k][i] > 0){
                 int n = adjacentMatrix(arr, i) + arr[k][i];
                 max = Math.max(max, n);
             }
           }
       }
       return max;  
    }
} 

