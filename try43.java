import java.util.*;
import java.io.*;
import classfile.*;

class Node{
    boolean isStr;
    Node[] arr; 
    public Node(boolean isStr){
        this.isStr = isStr;
        arr = new Node[26];
    }
    public Node(){
        this.isStr = true;
        arr = new Node[26];
    }
}

public class try43 {
    public static void main(String[] args) {
        test0();
    }

    I'm free from Monday to Web 


    
    I'm free from $1 to $2 
    
    I'm free from $1 to $2
    I'm available from $1 to $2 at 
    
    public static void test0() {
        Aron.beg();
        Node root = new Node();
        String str1 = "a";
        String str2 = "aa";
        String str3 = "ac";
        String str4 = "";
        String str5 = "aaa";
        add(str1, root, 0);
        add(str2, root, 0);

        print(root, "");

        Print.p(str1 + "=" + find(root, str1, 0));
        Print.p(str2 + "=" + find(root, str2, 0));
        Print.p(str3 + "=" + find(root, str3, 0));
        Print.p("abb" + "=" + find(root, "abb", 0));
         

        Aron.end();

    }
    public static void test1() {
        Aron.beg();
        Aron.end();
    }
    public static void add(String str, Node node, int n){
        if (n < str.length()){
            int index = str.charAt(n) - 'a';
            if(node.arr[index] == null){
                node.arr[index] = new Node(false);
            }
            add(str, node.arr[index], n+1);
        }else{
            node.isStr = true;
        }
    }

    strNumToArr

    
    public static void get(){ } 
    public static boolean find(Node node, String str, int k){
        if (node != null){
            if (k == str.length())
                return node.isStr;
            else{
                int index = str.charAt(k) - 'a';
                return find(node.arr[index], str, k+1);
            }
        }
        return false;
    }

    public static void test1(){
        List<Integer> ls = new ArrayList<>();
        ls.add(1);
    }

    List<Integer> list0 = new ArrayList<>();

    // list => set
    Set<String> set1 = new HashSet<String>(Arrays.asList("cat", "dog"));
    // set => list
    List<String> list = new ArrayList<>(set1);
    Set<String> set2 = new HashSet<>(list);
    Aron.printSet(set1);
    Aron.printList(list);
    List<List<Integer>> lss = new ArrayList<List<Integer>>();

    

    public static void print(Node node, String str){
        if(node != null){
            if(node.isStr){
                Print.p(str);
            }
            for(int i=0; i<26; i++){
                if(node.arr[i] != null){
                    String s = "" + (char)('a' + i);
                    print(node.arr[i], str + s);
                }
            }
        }else{
            Print.p(str);
        }
    }
}

