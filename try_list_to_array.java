import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_list_to_array {
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        List<String> list = new LinkedList<String>();
        list.add("dog");
        list.add("cat");
        list.add("pig");

        String[] arr = list.toArray(new String[0]);
        p(arr);

        sw.printTime();
        end();
    }

    public static <T extends Comparator<T>> void quickSort3(List<T> ls, int lo, int hi, BiFunction<T, T, Integer> f) {
        if(lo < hi) {
            int pIndex = partition(ls, lo, hi, f);
            quickSort3(ls, lo, pIndex - 1, f);
            quickSort3(ls, pIndex + 1, hi, f);
        }
    }

    public static <T extends Comparator<T>> int partition(List<T> ls, int lo, int hi, BiFunction<T, T, Integer> f) {
        int big = lo;
        if(ls != null && lo < hi) {
            T pivot = ls.get(hi);
            for(int i=lo; i<=hi; i++) {
                if(f.apply(ls.get(i), ls.get(big)) <= 0) {
                    T t = ls.get(i);
                    ls.set(i, ls.get(big));
                    ls.set(big, t);
                    if(i < hi) {
                        big++;
                    }
                }
            }
        }
        return big;
    }

    public static void test1(){
        beg();

        {
            List<String> list = new LinkedList<String>();
            list.add("dog");
            list.add("cat");
            list.add("pig");
            String[] arr = list.stream().toArray(String[] ::new);
            p(arr);
        }
        {
            List<Integer> list = of(1, 0, 2, 3, 2);
            int lo = 0;
            int hi = list.size() - 1;
            BiFunction<Integer, Integer, Integer> f = (x, y) -> x - y;
            quickSort3(list, lo, hi, f);
            p(list);
        }


        end();
    }
}

