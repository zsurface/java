import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class TestMultiLongInt{
    public static void main(String[] args) {
        test0();
    }

    public static Integer[] newInteger(int row){
        Integer[] arr = new Integer[row];
            for(int r = 0; r < row; r++){
                arr[r] = 0;
            }
        return arr;
    }
    public static Integer[][] newInteger2(int col, int row){
        Integer[][] mat = new Integer[col][row];
        for(int c = 0; c < col; c++){
            for(int r = 0; r < row; r++){
                mat[c][r] = 0;
            }
        }
        return mat;
    }
    public static void test0(){
        beg();
        {
            String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
            StopWatch sw = new StopWatch();

            int n = 9000;
            int[] arr1 = geneArr1ToNInt(1, n);
            int[] arr2 = geneArr1ToNInt(1, n);
            sw.start();

            int[] arr3 = multiLongInt(arr1, arr2);
            sw.printTime();
        }
        {
            String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
            StopWatch sw = new StopWatch();

            int n = 9000;
            Integer[] arr1 = geneArr1ToNInteger(1, n);
            Integer[] arr2 = geneArr1ToNInteger(1, n);
            sw.start();

            Integer[] arr3 = multiLongIntegerType(arr1, arr2);
            sw.printTime();
        }
        end();
    }
} 

