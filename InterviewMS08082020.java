import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// KEY: interview ms, interview microsoft, interview Auguest 08 2020, ms interview
public class InterviewMS08082020{
    public static void main(String[] args) {
        test0();
        test1();
	test2();
	test3();
    }
    public static void test0(){
        beg();
	{
	    // solution1(15);
	    {
		Character c = solution2("abABcabCe");
		t(c == 'C');
	    }
	    {
		Character c = solution2("aA");
		t(c == 'A');
	    }
	    {
		Character c = solution2("aAbbb");
		t(c == 'A');
	    }
	    {
		Character c = solution2("aaBaA");
		t(c == 'A');
	    }
	    {
		Character c = solution2("aaBaACbc");
		t(c == 'C');
	    }
	}
        end();
    }
    public static void test1(){
        beg();
	{
	    {
		solution(30);
	    }
	    {
		Integer n = strList("123");
		p("n=" + n);
		t(n == 5123);
	    }
	}
        end();
    }
    public static void test2(){
	beg();
	{
	    int k = 1;
	    String s = rotateStr("abc", k);
	    t(s, "bca");
	}
	{
	    // [x] = [x + 1]
	    // 0 <- 1, 1 <- 2, 2 <- 0
	    // "abc" => bca => cab
	    //  bca
	    int k = 2;
	    String s = rotateStr("abc", k);
	    t(s, "cab");
	}
	{
	    // [x] = [x + 1]
	    // 0 <- 1, 1 <- 2, 2 <- 0
	    // "abc" => bca => cab
	    //  bca
	    int k = -1;
	    String s = rotateStr("abc", k);
	    t(s, "cab");
	}
	{
	    // [x] = [x + 1]
	    // 0 <- 1, 1 <- 2, 2 <- 0
	    // "abc" => bca => cab
	    //  bca
	    int k = -2;
	    String s = rotateStr("abc", k);
	    t(s, "bca");
	}
	end();
    }
    public static void test3(){
	beg();
	{
	    {
		Integer n = maxValue(0);
		t(n, 50);
	    }
	    {
		Integer n = maxValue(10);
		t(n, 510);
	    }
	    {
		Integer n = maxValue(-10);
		t(n, -105);
	    }
	    {
		Integer n = maxValue(993);
		t(n, 9953);
	    }
	    {
		Integer n = maxValue(100);
		t(n, 5100);
	    }
	    {
		Integer n = maxValue(-100);
		t(n, -1005);
	    }
	}
	end();
    }
    /**
         1 2 3
         5 1 2 3
          6 7 0
          
     */
    
    public static int solution1(int N){
	int max = 0;
	List<Integer> ret = new ArrayList<>();
	if(N > 0){
	    List<Integer> s = toBase(N, 10);
	    int size = s.size();
	    for(int i = 0; i < size; i++){
		List<Integer> list = new ArrayList<>();
		list.addAll(s);
		list.add(i, 5);
		Integer n = toDecimal(list);
		if(n > max){
		    max = n;
		    ret.clear();
		    ret.addAll(list);
		}
	    }
	}else if(N == 0){
	    return 50;
	}else{
	    int min = Integer.MAX_VALUE;
	    List<Integer> s = toBase(-N, 10);
	    int size = s.size();
	    for(int i = 0; i < size; i++){
		List<Integer> list = new ArrayList<>();
		list.addAll(s);
		list.add(i, 5);
		Integer n = toDecimal(list);
		if(n < min){
		    min = n;
		    ret.clear();
		    ret.addAll(list);
		}
	    }
	    return -min;
	}
	return max;
    }
    
    public static Integer toDecimal(List<Integer> ls){
	Integer sum = 0;
	for(int i = ls.size() - 1; i >= 0; i--){
	    int ix = ls.size() - 1 - i;
	    sum += ls.get(i)*(int)Math.pow(10, ix);
	}
	return sum;
    }
    public static List<Integer> toBase(Integer n, Integer m){
	List<Integer> ret = list();
	if(n == 0){
	    ret.add(0, 0);
	}else{
	    while(n > 0){
		Integer q = n / 10;
		Integer r = n % 10;
		ret.add(0, r);
		n = q;
	    }
	}
	return ret;
    }
    /**
    public static void solution(String S){
    }
    */
    public static void solution(int n){
	
	for(int i = 1; i <= n; i++){
	    if(i % 2 == 0 && i % 3 == 0 && i % 5 == 0){
		System.out.println("CodilityTestCoders");
	    }
	    else if(i % 2 == 0 && i % 3 == 0){
		System.out.println("CodilityTest");
	    }
	    else if(i % 2 == 0 && i % 5 == 0){
		System.out.println("CodilityCoders");
	    }
	    else if(i % 3 == 0 && i % 5 == 0){
		System.out.println("TestCoders");
	    }else if(i % 2 == 0){
		System.out.println("Codility");
	    }else if(i % 3 == 0){
		System.out.println("Test");
	    }else if(i % 5 == 0){
		System.out.println("Coders");
	    }else{
		System.out.println(i);
	    }
	}
    }
    
    public static String sortStr(String s) {
        char[] arr = s.toCharArray();
        Arrays.sort(arr);
        return new String(arr); 
    }
    public static Character solution2(String S){
	Character ret = null;
	StringBuffer sbu = new StringBuffer();
	StringBuffer sbl = new StringBuffer();
	for(int i = 0; i < S.length(); i++){
	    if(Character.isUpperCase(S.charAt(i))){
		sbu.append(S.charAt(i));
	    }else{
		sbl.append(S.charAt(i));
	    }
	}
	
	String su = sortStr(sbu.toString());
	String sl = sortStr(sbl.toString());
	p(su);
	p(sl);
	

	int u = su.length() - 1;
	int l = sl.length() - 1;
	while(u >= 0 && l >= 0){
	    Character up = Character.toUpperCase(sl.charAt(l));
	    pp(up);
	    if(su.charAt(u) == up){
		ret = up;
		break;
	    }else{
		if(su.charAt(u) < up){
		    l--;
		}else{
		    u--;
		}
	    }
	}
	return ret;
    }
    
    public static Integer strList(String s){
	List<String> ret = new ArrayList<>();
	int l = s.length();
	int max = 0;
	for(int i = 0; i <= s.length(); i++){
	    String left = s.substring(0, i);
	    String right = s.substring(i, l);
	    pb(left);
	    pb(right);
	    p(left + "5" + right);
	    String sn = left + "5" + right;
	    Integer num = Integer.parseInt(sn);
	    if(max < num)
		max = num;
	}
	return max;
    }
    
    public static Integer maxValue(Integer n){
	Integer mValue = 0;
	String nstr = "";
	if(n > 0)
	    mValue = Integer.MIN_VALUE;
	else if(n < 0)
	    mValue = Integer.MAX_VALUE;
	else
	    mValue = 50;

	if(n < 0){
	    Integer m = -n;
	    nstr = m.toString();
	}else{
	    nstr = n.toString();
	}

	for(int i = 0; i <= len(nstr); i++){
	    // "ab" => "" "ab", "a" "b", "ab" ""
	    //           i=0      i=1     i=2
	    String left = nstr.substring(0, i);
	    String right = nstr.substring(i, len(nstr));
	    Integer num = Integer.parseInt(left + "5" + right);
	    if(n > 0){
		if(num > mValue)
		    mValue = num;
	    }else if(n < 0){
		if(num < mValue)
		    mValue = num;
	    }
	}
	
	return n >= 0 ? mValue : -mValue;
    }
} 

