import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;

public class test_prefixStringMap{
    public static void main(String[] args) {
        test0();
    }

    public static void test0(){
        Aron.beg();

        String pattern  = "\\s*:\\s*|\\s*,\\s*";
        String fName = "/Users/cat/myfile/bitbucket/snippets/snippet_test.m";

        List<List<String>> list2d = readCode(fName);
        Map<String, List<List<String>>> map = buildPrefixMap(list2d, pattern);
        
        for(Map.Entry<String, List<List<String>>> entry : map.entrySet()){
            Print.p("key=" + entry.getKey());
            for(List<String> list : entry.getValue()){
                for(String s : list)
                   Print.p("value=" + s); 
            }
        } 
        Aron.end();
    }

    public static List<List<String>> readCode(String fName){
        List<String> list = Aron.readFileLineByte(fName, 200);
        List<List<String>> list2d = new ArrayList<>();

        List<String> line = new ArrayList<>();
        for(String s : list){
            if(s.trim().length() > 0){
                line.add(s);
            }else{
                if(line.size() > 0) {
                    list2d.add(line);
                    line = new ArrayList<>();
                }
            }
        }
        return list2d;
    }
    
    /**
    * split the header and create prefix strings for searching
    */
    public static Map<String, List<List<String>>> buildPrefixMap(List<List<String>> lists, String pattern){
        Map<String, List<List<String>>> map = new HashMap<>();
        for(List<String> blockText : lists){
            if(blockText.size() > 0){

                // 
                // latex_file :*.txt: cat dog pig, cow => [latex_file, *.txt, dog, cat]
                // List<String> tokens = Aron.split(blockText.get(0).trim(), "\\s*:\\s*|\\s*,\\s*|\\s+");
                // latex_file :*.txt: cat dog pig => [latex_file, *.txt, dog cat pig]
                //                   "cat dog pig"=> ["cat dog pig", "dog pig", "pig"]
                List<String> tokens = keyList(Aron.split(blockText.get(0).trim(), pattern));

                Set<String> uniqueSet = new HashSet<String>();
                for (String token : tokens) {
                    String key = token.trim();
                    
                    List<String> prefixList = Aron.prefix(key);
                    for(String preStr: prefixList){
                        if (!uniqueSet.contains(preStr)){
                            uniqueSet.add(preStr);
                        
                            List<List<String>> listValue = map.get(preStr);

                            if (listValue != null) {
                                listValue.add(blockText);
                            } else {
                                List<List<String>> list2d = new ArrayList<>();
                                list2d.add(blockText);
                                map.put(preStr, list2d);
                            }
                        }
                    }

//                        for (int i = 0; i < key.length(); i++) {
//                            String prefix = key.substring(0, i+1);
//
//                            // filter out same prefix from the block of text
//                            if (!uniqueSet.contains(prefix)){
//                                uniqueSet.add(prefix);
//                            
//                                List<List<String>> listValue = map.get(prefix);
//
//                                if (listValue != null) {
//                                    listValue.add(blockText);
//                                } else {
//                                    List<List<String>> list2d = new ArrayList<>();
//                                    list2d.add(blockText);
//                                    map.put(prefix, list2d);
//                                }
//                            }
//                        }
                }
            }
        }
        return map;
    }

    public static List<String> keyList(List<String> list){
        List<String> mlist = new ArrayList<String>();
        if (list != null && list.size() > 2){
            List<String> first = list.subList(0, 1);
            List<String> last  = list.subList(2, list.size());
            mlist = merge(first, last);
        }
        return mlist;
    }

    public static List<String> merge(List<String> list1, List<String> list2){
        List<String> result = Stream.concat(list1.stream(), list2.stream())
        .collect(Collectors.toList());
        return result;
    }
} 

