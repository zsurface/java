import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

class Person{
    String name;
    int age;
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }
}

public class try_java1{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        List<Person> ls = new ArrayList<>(); 
        ls.add(new Person("a", 1));
        ls.add(new Person("a", 2));
        Collections.sort(ls, (x, y) -> x.age - y.age);
        p(ls);

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        PriorityQueue<Person> queue = new PiorityQueue();
        queue.add(new Person("a", 1));
        queue.add(new Person("a", 2));


        sw.printTime();
        end();
    }
} 

