import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
    find the max or min sorted rotated list
*/
public class try_rotate_sorted{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            int[] arr = {1, 2, 3, 4};
            int width = arr.length; 
            int lo = 0;
            int hi = width-1;
            Print.p("1 max=" + (max2(arr, lo, hi) == 4));
            Print.p("1 min=" + (min2(arr, lo, hi) == 1));
        }
        {
            int[] arr = {1};
            int width = arr.length; 
            int lo = 0;
            int hi = width-1;
            Print.p("2 max=" + (max2(arr, lo, hi) == 1));
            Print.p("2 min=" + (min2(arr, lo, hi) == 1));
        }

        {
            int[] arr = {2, 1};
            int width = arr.length; 
            int lo = 0;
            int hi = width-1;
            Print.p("3 max=" + (max2(arr, lo, hi) == 2));
            Print.p("3 min=" + (min2(arr, lo, hi) == 1));
        }
        {
            int[] arr = {3, 1, 2};
            int width = arr.length; 
            int lo = 0;
            int hi = width-1;
            Print.p("4 max=" + (max2(arr, lo, hi) == 3));
            Print.p("4 min=" + (min2(arr, lo, hi) == 1));
        }
        {
            int[] arr = {2, 3, 4, 1};
            int width = arr.length; 
            int lo = 0;
            int hi = width-1;
            Print.p("5 max=" + (max2(arr, lo, hi) == 4));
            Print.p("5 min=" + (min2(arr, lo, hi) == 1));
        }
        {
            int[] arr = {2, 3, 4, 5, 1};
            int width = arr.length; 
            int lo = 0;
            int hi = width-1;
            Print.p("6 max=" + (max2(arr, lo, hi) == 5));
            Print.p("6 min=" + (min2(arr, lo, hi) == 1));
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    /**
        1. arr = {1}, 
        2. 2 1
             mid = arr[0]
        3. 1 2
             mid = arr[0]

           2 3 4 1  (lo=0, hi=3)
                mid = 1
                    (lo=1, hi =3) -> 3 4 1
                        mid = 2
                            (lo=2, hi=3) -> 4 1
                                mid = 2
                                    arr[2] < arr[2]
                                    max(arr, lo=2, hi=2)  => 4
                            

        rotate sorted string
    */
    public static int max(int[] arr, int lo, int hi){
        if(arr[lo] <= arr[hi]){
            return arr[hi];
        }else{
            int mid = (lo + hi)/2;
            if(arr[lo] < arr[mid]){
                return max(arr, mid, hi);
            }else{
                return max(arr, lo, mid);
            }
        }
    }

    public static int findMinimumIndex(int[] arr, int lo, int hi){
        if( arr[lo] <= arr[hi])
            return arr[lo];
        else{
            // [2, 1]
            // [3, 1, 2]
            // =>[3, 1] => [1]
            int mid = (lo + hi)/2;
            if(arr[hi] < arr[mid])
                return findMinimumIndex(arr, mid + 1, hi);
            else 
                return findMinimumIndex(arr, lo, mid);
        }
    }

    public static int findMaximumIndex(int[] arr, int lo, int hi){
        if(arr[lo] <= arr[hi])
            return arr[hi];
        else{
            // [2, 1]
            int mid = (lo + hi)/2;
            if( arr[lo] < arr[mid])
                return findMaximumIndex(arr, mid, hi);
            else 
                return findMaximumIndex(arr, lo, mid);
        }
    }

    public static int min2(int[] arr, int lo, int hi){
        if(arr[lo] <= arr[hi]){
            return arr[lo];
        }else{
            int m = (lo + hi)/2;
            // max(r) < arr[m]
            if(arr[hi] < arr[m])
                return min2(arr, m+1, hi);
            else
                return min2(arr, lo, m);
        }
    }

    public static int max2(int[] arr, int lo, int hi){
        if(arr[lo] <= arr[hi]){
            return arr[hi];
        }else{
            int m = (lo + hi)/2;
            if(arr[lo] < arr[m])
                return max2(arr, m, hi);
            else
                return max2(arr, lo, m);
        }
    }
} 

