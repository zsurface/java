import java.util.concurrent.*;
import java.util.*;
import classfile.*;

/*
   1. Implement Callable interface
   2. Override call() whatever is inside
   3. Add callables object to a list
   4. Invoke all the callable objects with invokeAll(List<Future<>>) 
*/
public class CallableDemo{
    public CallableDemo(){
        System.out.println("Create callable Demo");
        ExecutorService service = Executors.newFixedThreadPool(10);

        List<MyCallable> futureList = new ArrayList<MyCallable>();
        int nThread = 10;
        Print.p("Create 10 threads and add 10 callables to a future list");
        for ( int i=0; i<nThread; i++){
            MyCallable myCallable = new MyCallable((long)i);
            futureList.add(myCallable);
        }

        System.out.println("Start");
        try{
            List<Future<Long>> futures = service.invokeAll(futureList);  
            for(Future<Long> future : futures){
                try{
                    System.out.println("future.isDone = " + future.isDone());
                    //System.out.println("future: call ="+future.get());
                }
                catch(Exception err1){
                    err1.printStackTrace();
                }
            }
        }catch(Exception err){
            err.printStackTrace();
        }
        service.shutdown();
    }

    class CallableComputation implements Callable<List<String>>{
        Long value = 0L;
        String fname;
        public CallableComputation(String fname){
            this.fname = fname;
        }
        public List<String> call(){
            // 1. Read file
            // 2. Process the file 
            // 3. Output strings to list
            // return the list of string
            return new ArrayList<String>();
        }
    }

    class MyCallable implements Callable<Long>{
        Long id = 0L;
        public MyCallable(Long val){
            this.id = val;
        }
        /*
        It is similar to Run() in Runnable interface => start() method
        Thread t = new Thread(new Runnable())
        t.start()
        Override call()
        */
        public Long call(){
            int a=4, b = 0;
            try{
                Print.fl("call()", id);
                Thread.sleep(1000);
            }catch(InterruptedException e){
            }
            return id;
        }
    }
    public static void main(String args[]){
        CallableDemo demo = new CallableDemo();
    }
}
