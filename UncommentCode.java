import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;



/**
     // KEY: comment and uncomment different language: haskell, java, elsp, bash etc
     
     <pre>
     {@literal
     KEY: alignment lines
        
     NOTE:
     }
     {@code
     
         xⁿ + yⁿ = zⁿ + wⁿ
           x² + y² = z²
             ⇓
         xⁿ + yⁿ = zⁿ + wⁿ
         x² + y² = z²

	 alignment2("=", ls) use "=" as delimiter
	 alignment2("", ls)  use space as delimiter
     }
     
     java_compileToExec.sh $j/Alignment.java  => $b/javaclass
     java_compileToExec.sh $j/CommentCode.java  => $b/javaclass
     java_compileToExec.sh $j/UncommentCode.java => $b/javaclass
     </pre>
*/ 
public class UncommentCode{
  public static void main(String[] args) {
    // $b/tmp/x.x
    String fname = getEnv("mytmp");
    if(len(args) == 2){
	List<String> ls = new ArrayList<>();
	// "-f" is NOT been used any more
	if(args[0].compareTo("-f") == 0){
	    ls = readFileNoTrim(fname);
	}else if(args[0].compareTo("-p") == 0){
	    ls = readStdin();
	}else{
	    
	}
	var lss         = uncommentCode(args[1], ls);	
	var str = len(joinListStr(lss)) > 0 ? joinListStr(lss) : "";
 	System.out.print(str);
    }
    
    // test_uncommentCode();
  }

  /**
     x = y
  x = 4
   
  */
  public static List<String> uncommentCode(String lang, List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      for(int i = 0; i < len(ls); i++){
        String line = ls.get(i);
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', line);
        
        if(lang.equals("haskell")){
          ret.add(t.x + replaceStr(t.y, "^--\\s*", ""));
        } else if(lang.equals("java") || lang.equals("cpp")){
          ret.add(t.x + replaceStr(t.y, "^\\/\\/\\s*", ""));
        }
        else if(lang.equals("elisp")){
          ret.add(t.x + replaceStr(t.y, "^;;\\s*", ""));
        }
		else if(lang.equals("latex")){
          ret.add(t.x + replaceStr(t.y, "^%\\s*", ""));
        }
        else if(lang.equals("bash") || lang.equals("python")){
          ret.add(t.x + replaceStr(t.y, "^#\\s*", ""));
        }
      }
    }
    return ret;
  }

  public static void test_uncommentCode(){
    beg();
    {
      fl("haskell tests");
      {
        String lang = "haskell";
        List<String> ls = ll("- ");
        List<String> expls = ll("- ");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll(" -- fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      
      {
        String lang = "haskell";
        List<String> ls = ll("-- fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("--fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("-- fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("  -- fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      
    }
    
    {
      fl("java tests");
      {
        String lang = "java";
        List<String> ls = ll(" // fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("// fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("//fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("// fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("  // fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("/fold");
        List<String> expls = ll("/fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("/ /fold");
        List<String> expls = ll("/ /fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("/");
        List<String> expls = ll("/");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("/ ");
        List<String> expls = ll("/ ");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    
    {
      fl("elisp tests");
      {
        String lang = "elisp";
        List<String> ls = ll(";;");
        List<String> expls = ll("");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";; ");
        List<String> expls = ll("");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll("; ");
        List<String> expls = ll("; ");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";");
        List<String> expls = ll(";");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(" ;; fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";; fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";;fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";; fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll("  ;; fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    end();
  }
}

