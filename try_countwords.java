import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_countwords{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Map<String, Integer> map = countWords(fname);
        Print.p(map);

        Aron.end();
    }

    
    /**
    <pre>
    {@literal
        Count all the words in a file and form map, e.g.
        dog -> 3
        cat -> 4
        is  -> 2
        my  -> 4
    }
    {@code
    }
    </pre>
    */ 
    public static Map<String, Integer>  countWords(String fname){
        Map<String, Integer> map = new HashMap<>();
        List<String> list = Aron.getWords(fname);
        for(String s : list){
            Integer v = map.get(s);
            if(v == null){
                map.put(s, 1);
            }else{
                map.put(s, v + 1);
            }
        }
        return map;
    }
} 

