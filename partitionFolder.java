import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

class Person{
    String firstName;
    String lastName;
    int age;
    public Person(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    public String toString(){
        return "firstName=" + firstName + " lastName=" + lastName + " age=" + age;
    }
} 


//class Person implements BiFunction<Person, Person, Integer>{
//    String firstName;
//    String lastName;
//    int age;
//    public Person(String firstName, String lastName, int age){
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//    }
//    public Integer apply(Person a, Person b){
//        return a.firstName.compareTo(b.firstName);
//    }
//    public String toString(){
//        return "firstName=" + firstName + " lastName=" + lastName + " age=" + age;
//    }
//}

public class partitionFolder{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        test5();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();
        
        // immutable list
        List<String> list1 = of("cat1", "dog1", "cow1");
        List<String> ls = replaceIndexList(0, "FIX", list1);
        p(ls);

        String s = System.getProperty("path.separator");
        p("s=" + s);

        {
            fl();
            Pair<List<String>, List<String>> pair = splitAt(0, list1);
            p(pair);
        }
        {
            fl();
            Pair<List<String>, List<String>> pair = splitAt(1, list1);
            p(pair);
        }
        {
            fl();
            Pair<List<String>, List<String>> pair = splitAt(10, list1);
            p(pair);
        }
        {
            fl();
            List<String> ls4 = insertIndexList(1, "insert", of("dog", "cat", "fox")); 
            p(ls4);
        }
        {
            fl();
            List<String> ls4 = insertIndexList(0, "insert", of("dog", "cat", "fox")); 
            p(ls4);
        }
        {
            fl();
            List<String> ls4 = insertIndexList(2, "insert", of("dog", "cat", "fox")); 
            p(ls4);
        }

        sw.printTime();
        end();
    }
    
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();
        {
            String currPath = "/Users/cat/try/dir";
            String newPath = "/Users/cat/try/newdir";
            String newDirName = "dir_";
            partitionDirectory(3, currPath, newPath);
        }

        sw.printTime();
        end();
    }
    public static void test2(){
        beg();

        {
            List<Person> ls = of();
            ls.add(new Person("Michell", "Constraint", 1));
            ls.add(new Person("David",   "Continue", 0));
            ls.add(new Person("Michael", "Continua", 2));
            for(Person p : ls){
                p(p.toString());
            }
            fl();
            ls.sort(Comparator.comparing(e -> e.age));
            for(Person p : ls){
                p(p.toString());
            }
        }
        {
            fl();
            List<Person> ls = of();
            ls.add(new Person("Michell", "South American Brazil", 1));
            ls.add(new Person("David",   "South American Venezuela", 0));
            ls.add(new Person("Michael", "South American Cuba", 2));
            ls.add(new Person("Michael", "South American Chile", 2));
            ls.add(new Person("Michael", "South American Peru", 2));

            for(Person p : ls){
                p(p.toString());
            }
            fl();

            BiFunction<Person, Person, Integer> f = (x, y) -> x.firstName.compareTo(y.firstName);
            Collections.sort(ls, f::apply);
            for(Person p : ls){
                p(p.toString());
            }
            fl();
            Collections.sort(ls, (x, y) -> x.lastName.compareTo(y.lastName));

            for(Person p : ls){
                p(p.toString());
            }
            fl();
//            Comparator<Person> c = (Person a, Person b) -> a.firstName.compareTo(b.firstName); 
//            Collections.sort(ls, c);
            ls.sort((a, b) -> a.firstName.compareTo(b.firstName));
        }
        
        end();
    }

    public static void test3(){
        List<Person> ls = of();
        ls.add(new Person("Michell", "South American Brazil", 1));
        ls.add(new Person("David",   "South American Venezuela", 0));
        ls.add(new Person("Michael", "South American Cuba", 2));
        ls.add(new Person("Michael", "South American Chile", 2));
        ls.add(new Person("Michael", "South American Peru", 2));
        ls.sort((a, b) -> a.firstName.compareTo(b.firstName));
        p(ls);
    }
    public static void test4(){
        List<Person> ls = of();
        ls.add(new Person("Michell", "South American Brazil", 1));
        ls.add(new Person("David",   "South American Venezuela", 0));
        ls.add(new Person("Michael", "South American Cuba", 2));
        ls.add(new Person("Michael", "South American Chile", 2));
        ls.add(new Person("Michael", "South American Peru", 2));
        Collections.sort(ls, (a, b) -> a.firstName.compareTo(b.firstName)); 
        p(ls);
    }

    public static void test5(){
        class Person implements Comparable<Person>{
            String firstName;
            String lastName;
            int age;
            public Person(String firstName, String lastName, int age){
                this.firstName = firstName;
                this.lastName = lastName;
                this.age = age;
            }
            public int compareTo(Person other){
                return this.firstName.compareTo(other.firstName);
            }
            public String toString(){
                return "firstName=" + firstName + " lastName=" + lastName + " age=" + age;
            }
        }

        List<Person> ls = of();
        ls.add(new Person("Michell", "South American Brazil", 1));
        ls.add(new Person("David",   "South American Venezuela", 0));
        ls.add(new Person("Michael", "South American Cuba", 2));
        ls.add(new Person("Michael", "South American Chile", 2));
        ls.add(new Person("Michael", "South American Peru", 2));
        Collections.sort(ls); 
        for(Person p : ls){
            p(p.toString());
        }
    }
    public static void test6(){
        class PersonCom implements Comparator<Person>{
            String firstName;
            String lastName;
            int age;
            public int compare(Person p1, Person p2){
                return p1.firstName.compareTo(p2.firstName);
            }
        }

        List<Person> ls = of();
        ls.add(new Person("Michell", "South American Brazil", 1));
        ls.add(new Person("David",   "South American Venezuela", 0));
        ls.add(new Person("Michael", "South American Cuba", 2));
        ls.add(new Person("Michael", "South American Chile", 2));
        ls.add(new Person("Michael", "South American Peru", 2));
        Collections.sort(ls, new PersonCom()); 
        for(Person p : ls){
            p(p.toString());
        }
    }
} 

