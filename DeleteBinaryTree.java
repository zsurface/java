import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


public class DeleteBinaryTree{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static Node myInsert(Node n, int k){
        Node root = null;
        if(n == null){
           root = new Node(k);
        }else{
            insert(n, k);
            root = n;
        }
        return root;
    }
    public static void insert(Node root, int k){
        Node n = root;
        if(k <= n.data){
            if(n.left == null)
                n.left = new Node(k);
            else
                insert(n.left, k);
        }else{
            if(n.right == null)
                n.right = new Node(k);
            else
                insert(n.right, k);
        }
    }
    public static void test0(){
        beg();
        List<Integer>ls = list(8, 9, 2, 3, 8, 8, 4, 6, 6, 8); 
        p(ls, "ls");
        Node root = null;
        for(Integer n : ls){
            root = myInsert(root, n);
        }
        fl("root");
        inorder(root);


        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        sw.printTime();
        end();
    }

    public static void runCmd() {
        String name = "myfile.gv";
        String pdfName = "/tmp/" + dropExt(name) + ".pdf";
        // String command = "/usr/local/Cellar/graphviz/2.44.0/bin/dot -v -Tpdf " + fName + " > " + pdfName;
        String command = "cat /tmp/f.x";
        String openPDF = "open " + pdfName;
        try {
            ProcessBuilder builder = new ProcessBuilder("sh", "-c", "cat /tmp/f.x");
            builder.redirectOutput(new File("/tmp/out.txt"));
            builder.redirectError(new File("out.txt"));
            Process p = builder.start(); // may throw IOExcepti
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
} 

