import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_allprime{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        {
            ArrayList<Integer> list = allPrime(2);
            Print.p(list);
        }
        {
            ArrayList<Integer> list = allPrime(10);
            Print.p(list);
        }
        {
            ArrayList<Integer> list = allPrime(100);
            Print.p(list);
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        {
            List<Integer> listp = Arrays.asList(2, 3, 5, 7);
            ArrayList<Integer> list = nPrime(4);
            Test.t(listp, list);
            Print.p(listp);
        }
        {
            List<Integer> listp = Arrays.asList(2);
            ArrayList<Integer> list = nPrime(1);
            Test.t(listp, list);
            Print.p(listp);
        }

        Aron.end();
    }

    public static ArrayList<Integer> nPrime(int n){
        ArrayList<Integer> list = new ArrayList<Integer>();
        if(n > 0){
            list.add(2);
            int p = 3;
            while(list.size() < n){
                boolean isPrime = true;
                for(int i=0; i<list.size() && isPrime; i++){
                    if(p % list.get(i) == 0)
                        isPrime = false;
                }
                if(isPrime)
                    list.add(p);
                p++;
            }
        }
        return list;
    }
    

    public static ArrayList<Integer> allPrime(int n){
        ArrayList<Integer> list = new ArrayList<Integer>();
        if(n >= 2){
            list.add(2);
            for(int m=3; m<n; m++){
                boolean isPrime = true;
                for(int p=0; p<list.size() && isPrime; p++){
                    if(m % list.get(p) == 0){
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime)
                    list.add(m);
            }
        }
        return list;
    }
}
   
 

