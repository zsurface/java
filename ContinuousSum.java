import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class ContinuousSum{
    public static void main(String[] args) {
        test0();
        //test1();
    }
    public static void test0(){
        beg();

        String s = "a";
        
        // immutable list
        {
            List<Integer> list1 = Arrays.asList(1, 2, 3, 4);
            List<Integer> list2 = Arrays.asList();
            List<Pair<Integer, Integer>> pl = zip(list1, list2);
            for(Pair<Integer, Integer> p : pl){
                p("k=" + p.getKey() + " v=" + p.getValue());
            }
        }
        {
            List<Integer> list1 = Arrays.asList(1, 2, 3, 3, 4);
            int n = monotonicList(list1);
            p("increasing=" + (n == -1));
        }
        {
            List<Integer> list1 = Arrays.asList(1, 2, 0, 4);
            int n = monotonicList(list1);
            p("increasing=" + (n == 0));
        }
        {
            List<Integer> list1 = Arrays.asList(3, 2, 1);
            int n = monotonicList(list1);
            p("decreasing=" + (n == 1));
        }
        {
            fl();
            String s1 = "a";
            List<String> list = substringAll(s1);
            p(list);
        }
        {
            fl();
            List<Integer> ls = Arrays.asList(1, 2, 3, 4);
            List<Integer> s1 = init(ls);
            List<Integer> s2 = tail(ls);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            List<Boolean> s3 = zipWith(f, s1, s2); 
            p(s3);
        }
        {
            fl();
            List<Integer> ls = Arrays.asList(1, 2, 1, 4);
            List<Integer> s1 = init(ls);
            List<Integer> s2 = tail(ls);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            Boolean b = zipWith(f, s1, s2).stream().reduce(true, (x, y) -> x & y);
            p(b);
        }
        {
            fl();
            List<Integer> ls = Arrays.asList(1, 2, 3);
            List<Integer> s1 = init(ls);
            List<Integer> s2 = tail(ls);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x.compareTo(y) < 0;
            Boolean b = zipWith(f, s1, s2).stream().reduce(true, (x, y) -> x & y);
            p(b);
        }

//        {
//            fl();
//            List<Integer> ls = Arrays.asList(7, 2, 3, 2, 3);
//            Pair<Integer, Integer> pair= continuousSumFast(ls, 8);
//            p(pair); 
//        }
//        {
//            fl();
//            List<Integer> ls = Arrays.asList(8);
//            Pair<Integer, Integer> pair= continuousSumFast(ls, 8);
//            p(pair); 
//        }
//        {
//            fl();
//            List<Integer> ls = Arrays.asList(9, 7, 1);
//            Pair<Integer, Integer> pair= continuousSumFast(ls, 8);
//            p(pair); 
//        }

        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 1000000;
            List<Integer> list = Arrays.asList(1); 
            List<Pair<Integer, Integer>> ls = continuousSumFast(list, 1);
            for(Pair<Integer, Integer> pair : ls ){
                p(pair);
            }

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 1000000;
            List<Integer> list = Arrays.asList(1, 2, 4); 
            List<Pair<Integer, Integer>> ls = continuousSumFast(list, 6);
            for(Pair<Integer, Integer> pair : ls ){
                p(pair);
            }

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 1000000;
            List<Integer> list = Arrays.asList(1, 2, 4, 6); 
            List<Pair<Integer, Integer>> ls = continuousSumFast(list, 6);
            for(Pair<Integer, Integer> pair : ls ){
                p(pair);
            }

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 1000000;
            List<Integer> list = Arrays.asList(1, 2, 4, 6, 1, 1, 1, 3, 7, 6, 4); 
            List<Pair<Integer, Integer>> ls = continuousSumFast(list, 6);
            for(Pair<Integer, Integer> pair : ls ){
                p(pair);
            }

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 2000;
            List<Integer> list = geneRandomIntFromTo(1, 100, max); 
            List<Pair<Integer, Integer>> lp = continuousSumSlow(list, 1000);

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            int max = 2000;
            List<Integer> list = geneRandomIntFromTo(1, 100, max); 
            List<Pair<Integer, Integer>> lp = continuousSumFast(list, 1000);

            sw.printTime();
        }



        end();
    }

    
    public static void test1(){
        // immutable list
        List<String> list = Arrays.asList("cat1", "dog1", "cow1");
        List<Integer> lns = IntStream.range(0, list.size()).mapToObj(i -> list.get(i).length()).collect(Collectors.toList());
        Print.p("lns" + lns);
    }
} 

