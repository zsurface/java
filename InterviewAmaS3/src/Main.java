
import java.util.*;
import java.util.Map;
import java.util.HashSet;
import java.util.ArrayList;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.t;

class ColRow{
    int c;
    int r;
    public ColRow(int c, int r){
        this.c = c;
        this.r = r;
    }
}

// KEY: Amazon s3 interview,
public class Main {
    public static void main(String[] args) {
//        test0();
//        test2();
        //   test9();
//        test10();
//        test1();
        // test_excelColumnPermutation();
        test1();
    }

    /*
         00
         01
         10
         11

        arr = {0, 1}
           {0},     {1}
        {0}, {1}   {0}, {1}

     */
    public static void excelColumnPermutation(int[] arr, List<Integer> ls, int len){
        if(ls.size() == len){
            p(ls);
        }else {
            for (int i = 0; i < arr.length; i++) {
                excelColumnPermutation(arr, append(ls, arr[i]), len);
            }
        }
    }
    public static void test_excelColumnPermutation(){
        beg();
        {
            int[] arr = {0, 1, 2};
            List<Integer> ls = new ArrayList<>();
            for(int i=1; i <= arr.length; i++) {
                excelColumnPermutation(arr, ls, i);
            }
        }
        end();
    }
    public static void test9() {
        beg();
        int[][] arr2d = {
                {0, 0, 1, 1},
                {0, 1, 0, 0},
                {0, 0, 0, 1},
                {1, 1, 0, 0},
        };

        int height = arr2d.length;
        int width = arr2d[0].length;
        int h = 0, w = 0;


        int max = 0;
        int numStore = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int num = count(arr2d, i, j, height, width);
                if (num > 0)
                    numStore++;
            }
        }
        pp(numStore);


        end();
    }

    public static void test10() {
        beg();
        {
            int[][] arr2d = {
                    {0, 0, 1, 1},
                    {0, 1, 0, 0},
                    {0, 0, 0, 1},
                    {1, 1, 0, 0},
            };
            int height = arr2d.length;
            int width = arr2d[0].length;
            int rows = height;
            int columns = width;
            List<List<Integer>> grid = new ArrayList<>();
            for (int h = 0; h < height; h++) {
                List<Integer> ls = new ArrayList<>();
                for (int w = 0; w < width; w++) {
                    ls.add(arr2d[h][w]);
                }
                grid.add(ls);
            }

            int n = numberAmazonGoStores(rows, columns, grid);
            t(n, 4);


        }

        end();
    }

    public static int count(int[][] arr, int h, int w, int height, int width) {
        if (arr[h][w] == 1) {
            arr[h][w] = 2;
            int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
            if (h + 1 < height)
                n1 = count(arr, h + 1, w, height, width);
            if (h - 1 >= 0)
                n2 = count(arr, h - 1, w, height, width);
            if (w + 1 < width)
                n3 = count(arr, h, w + 1, height, width);
            if (w - 1 >= 0)
                n4 = count(arr, h, w - 1, height, width);

            return n1 + n2 + n3 + n4 + 1;
        }
        return 0;
    }

    public static void listToArray(int[][] arr, List<List<Integer>> grid) {
        int height = grid.size();
        int width = grid.get(0).size();
        int hh = 0;
        for (List<Integer> ls : grid) {
            int ww = 0;
            for (Integer n : ls) {
                arr[hh][ww] = grid.get(hh).get(ww);
                ww++;
            }
            hh++;
        }
    }

    public static int numberAmazonGoStores(int rows, int column, List<List<Integer>> grid) {
        int height = grid.size();
        int width = grid.get(0).size();

        int[][] arr = new int[height][width];
        listToArray(arr, grid);

        int numStore = 0;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                int num = count(arr, h, w, height, width);
                if (num > 0)
                    numStore++;
            }
        }
        return numStore;
    }

    // METHOD SIGNATURE ENDS
    static void test0() {
        List<String> list = Arrays.asList("mobile", "mouse", "moneypot", "monitor", "mousepad");
        List<List<String>> ls2d = threeKeywordSuggestions(5, list, "mouse");
        p(ls2d);
    }

    static void test1() {
        beg();
        {
            Integer[][] arr2d = {
                    {0, 1, 1, 1},
                    {0, 0, 1, 0}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 2);
        }
        {
            Integer[][] arr2d = {
                    {1, 0, 0, 0, 0}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 4);
        }
        {
            Integer[][] arr2d = {
                    {1, 0, 0, 0, 0},
                    {1, 0, 0, 0, 0}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 4);
        }
        {
            Integer[][] arr2d = {
                    {1, 0, 0, 0, 0},
                    {1, 0, 0, 0, 1}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 2);
        }
        {
            Integer[][] arr2d = {
                    {1, 0, 0, 0, 0},
                    {0, 0, 1, 0, 0},
                    {0, 0, 0, 0, 1}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 2);
        }
        {
            Integer[][] arr2d = {
                    {1, 0, 0, 0, 0},
                    {0, 1, 0, 0, 0},
                    {0, 0, 1, 0, 0},
                    {0, 0, 0, 1, 0},
                    {0, 0, 0, 0, 1}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 4);
        }
        {
            Integer[][] arr2d = {
                    {0, 0, 1, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 1},
                    {0, 0, 0, 0, 0, 0},
                    {0, 1, 0, 0, 0, 0}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 3);
        }
        {
            Integer[][] arr2d = {
                    {1}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, -1);
        }
        {
            Integer[][] arr2d = {
                    {0}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, -1);
        }
        {
            Integer[][] arr2d = {
                    {1, 1}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, -1);
        }

        {
            Integer[][] arr2d = {
                    {0, 0, 1, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 1},
                    {0, 0, 0, 0, 0, 0},
                    {0, 1, 0, 0, 0, 0}
            };
            printArray2d(arr2d);
            int rows = arr2d.length;
            int columns = arr2d[0].length;
            List<List<Integer>> ls2d = array2dToListOfList(arr2d);
            int min = minimumHours(rows, columns, ls2d);
            t(min, 3);
        }


        end();
    }

    static void test2() {
        beg();
        Integer[][] arr2d = {
                {0, 1, 1, 1},
                {0, 0, 1, 0}
        };
        printArray2d(arr2d);
        List<List<Integer>> ls2d = array2dToListOfList(arr2d);
        p(ls2d);
        end();
    }

    /**
     * KEY: Amazon interview, three keyword suggestions, keyword suggestion
     *
     * @param numreviews
     * @param repository
     * @param customerQuery
     * @return
     */
    public static List<List<String>> threeKeywordSuggestions(int numreviews,
                                                             List<String> repository,
                                                             String customerQuery) {
        List<String> prefixList = prefixStr(customerQuery);
        Map<String, Set<String>> map = new HashMap<>();
        for (int i = 0; i < numreviews; i++) {
            String s = repository.get(i);
            List<String> ls = prefixStr(s);
            for (String e : ls) {
                if (e.trim().length() > 1) {
                    Set<String> set = map.get(e);
                    if (set == null)
                        set = new HashSet<>();
                    set.add(s);
                    map.put(e, set);
                }
            }
        }
        List<List<String>> list2d = new ArrayList<>();
        for (String input : prefixList) {
            if (input.trim().length() > 1) {
                Set<String> myset = map.get(input);
                ArrayList<String> tmpList = new ArrayList<>(myset);
                Collections.sort(tmpList);
                if (tmpList.size() <= 3) {
                    list2d.add(tmpList);
                } else {
                    List<String> ls1 = tmpList.subList(0, 3);
                    list2d.add(ls1);
                }
            }
        }
        return list2d;
    }

    /**
     * KEY: Amazon interview, minimum days, update server
     *
     * @param rows
     * @param columns
     * @param grid
     * @return
     */
    public static int minimumDays(int rows, int columns, List<List<Integer>> grid) {
        int day = 0;
        while (true) {

            int c = 0;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if (grid.get(i).get(j) == 0) {
                        c++;
                    } else {
                        if (grid.get(i).get(j) == 2) {
                            grid.get(i).set(j, 1);
                        }
                    }
                }
            }
            if (c == 0) {
                break;
            } else {
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        if (grid.get(i).get(j) == 1) {
                            if (j + 1 < columns && grid.get(i).get(j + 1) == 0)
                                grid.get(i).set(j + 1, 2);

                            if (j - 1 >= 0 && grid.get(i).get(j - 1) == 0)
                                grid.get(i).set(j - 1, 2);

                            if (i + 1 < rows && grid.get(i + 1).get(j) == 0)
                                grid.get(i + 1).set(j, 2);

                            if (i - 1 >= 0 && grid.get(i - 1).get(j) == 0)
                                grid.get(i - 1).set(j, 2);

                        }
                    }
                }
                day++;
            }
        }
        return day;
    }


    public static int minimumDays2(int rows, int columns, List<List<Integer>> grid) {
        int day = 0;
        int c = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (grid.get(i).get(j) == 1) {
                    c++;
                }
            }
        }
        if (c == rows * columns) {
            return -1;
        }
        int setValue = 2;
        while (c < rows * columns) {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if (grid.get(i).get(j) == setValue - 1) {
                        if (j + 1 < columns && grid.get(i).get(j + 1) == 0) {
                            c++;
                            grid.get(i).set(j + 1, setValue);
                        }

                        if (j - 1 >= 0 && grid.get(i).get(j - 1) == 0) {
                            c++;
                            grid.get(i).set(j - 1, setValue);
                        }

                        if (i + 1 < rows && grid.get(i + 1).get(j) == 0) {
                            c++;
                            grid.get(i + 1).set(j, setValue);
                        }

                        if (i - 1 >= 0 && grid.get(i - 1).get(j) == 0) {
                            c++;
                            grid.get(i - 1).set(j, setValue);
                        }
                    }
                }
            }
            setValue++;
            day++;
        }
        return day;
    }

    public static int minimumHours4(int rows, int columns, List<List<Integer>> grid){
        int countHour = -1;
        List<Integer> list = new ArrayList<>();
        if (grid != null && rows > 0 && columns > 0) {
            int countServer = 0;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if (grid.get(i).get(j) == 1) {
                        Integer index = columns*i + j;
                        list.add(index);
                        countServer++;
                    }
                }
            }
            if (countServer == 0)
                return countHour;

            for (int k=0; k < list.size() && list.size() < rows*columns; k++) {
                int i = list.get(k) / columns;
                int j = list.get(k) % columns;
                if (j + 1 < columns && grid.get(i).get(j + 1) == 0) {
                    countServer++;
                    grid.get(i).set(j + 1, 1);
                    list.add(rows * i + (j + 1));
                    countHour++;
                    countServer++;
                }

                if (j - 1 >= 0 && grid.get(i).get(j - 1) == 0) {
                    countServer++;
                    grid.get(i).set(j - 1, 1);
                    list.add(rows * i + (j - 1));
                    countHour++;
                    countServer++;


                }

                if (i + 1 < rows && grid.get(i + 1).get(j) == 0) {
                    countServer++;
                    grid.get(i + 1).set(j, 1);
                    list.add(rows * (i + 1) + j);
                    countHour++;
                    countServer++;

                }

                if (i - 1 >= 0 && grid.get(i - 1).get(j) == 0) {
                    countServer++;
                    grid.get(i - 1).set(j, 1);
                    list.add(rows * (i - 1) + j);
                    countHour++;
                    countServer++;

                }
            }

        }
        return countHour;
    }

    public static int minimumHours(int rows, int columns, List<List<Integer>> grid) {
        int countHour = -1;
        //if (grid != null && grid.size() > 0 && grid.get(0).size() > 0 && rows > 0 && columns > 0) {
        if (grid != null && rows > 0 && columns > 0) {
            int countServer = 0;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if (grid.get(i).get(j) == 1) {
                        countServer++;
                    }
                }
            }
            if (countServer == 0)
                return countHour;

            countHour = 0;
            int newValue = 2;
            while (countServer < rows * columns) {
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        if (grid.get(i).get(j) == newValue - 1) {
                            if (j + 1 < columns && grid.get(i).get(j + 1) == 0) {
                                countServer++;
                                grid.get(i).set(j + 1, newValue);
                            }

                            if (j - 1 >= 0 && grid.get(i).get(j - 1) == 0) {
                                countServer++;
                                grid.get(i).set(j - 1, newValue);
                            }

                            if (i + 1 < rows && grid.get(i + 1).get(j) == 0) {
                                countServer++;
                                grid.get(i + 1).set(j, newValue);
                            }

                            if (i - 1 >= 0 && grid.get(i - 1).get(j) == 0) {
                                countServer++;
                                grid.get(i - 1).set(j, newValue);
                            }
                        }
                    }
                }
                newValue++;
                countHour++;
            }
        }
        return countHour;
    }

    int numberAmazonTreasureTrucks(int rows, int column, List<List<Integer>> grid) {
        int max = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < column; j++) {
                int num = countBlock(grid, i, j);
                if (num > 0)
                    max++;
            }
        }
        return max;
    }

    public static int countBlock(List<List<Integer>> grid, int h, int w) {
        if (grid.get(h).get(w) == 1) {
            int height = grid.size();
            int width = grid.get(0).size();
            grid.get(h).set(w, 2);
            int c1 = 0, c2 = 0, c3 = 0, c4 = 0;
            if (h + 1 < height)
                c1 = countBlock(grid, h + 1, w);
            if (h - 1 >= 0)
                c2 = countBlock(grid, h - 1, w);
            if (w + 1 < width)
                c3 = countBlock(grid, h, w + 1);
            if (w - 1 >= 0)
                c4 = countBlock(grid, h, w - 1);

            return c1 + c2 + c3 + c4 + 1;
        }
        return 0;
    }

    public static int minimumHours3(int rows, int columns, List<List<Integer>> grid) {
        int numHours = -1;
        if (grid != null && grid.size() > 0 && grid.get(0).size() > 0 && rows > 0 && columns > 0) {
            List<Pair> list = new ArrayList<>();
            int c = 0;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if (grid.get(i).get(j) == 1) {
                        list.add(new Pair(i, j));
                    }
                }
            }
            if (c == rows * columns || c == 0)
                return numHours;

            numHours = 0;
            int numPairs = list.size();
            while (numPairs < rows * columns) {


                for (int k = 0; k < numPairs; k++) {
                    Pair p = list.get(k);
                    int i = p.r;
                    int j = p.c;
                    if (j + 1 < columns && grid.get(i).get(j + 1) == 0) {
                        grid.get(i).set(j + 1, 1);
                        list.add(new Pair(i, i+1));
                    }

                    if (j - 1 >= 0 && grid.get(i).get(j - 1) == 0) {
                        grid.get(i).set(j - 1, 1);
                        list.add(new Pair(i, j-1));
                    }

                    if (i + 1 < rows && grid.get(i + 1).get(j) == 0) {
                        grid.get(i + 1).set(j, 1);
                        list.add(new Pair(i + 1, j));
                    }

                    if (i - 1 >= 0 && grid.get(i - 1).get(j) == 0) {
                        grid.get(i - 1).set(j, 1);
                        list.add(new Pair(i - 1, j));
                    }
                }
                numPairs = list.size();
                numHours++;
            }
        }
        return numHours;
    }

}
class Pair{
    int r = 0;
    int c = 0;
    public Pair(int r, int c){
        this.r = r;
        this.c = c;
    }
}
