import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

class LRU {
    int max;
    int count = 0;
    Map<String, Node> map = new HashMap<>();
    LinkedList ll = new LinkedList();

    public LRU(int max) {
        this.max = max;
    }

    public Node get(String key) {
        Node node = map.get(key);
        if(node != null) {
            ll.remove(node);
            ll.append(node);
        }
        return node;
    }
    public void append(String key, Node node) {
        Node n = map.get(key);
        if(n == null) {
            // new key
            if(count < max) {
                count++;
            } else {
                Node h = ll.head;
                String k = ll.head.key;
                ll.remove(h);
                map.remove(k);
            }
            map.put(key, node);
            ll.append(node);
        } else {
            // same key, update the node
            ll.remove(n);
            map.put(key, node);
        }
    }
    public void remove(String key) {
        Node node = map.get(key);
        if(node != null) {
            ll.remove(node);
            map.remove(key);
            count--;
        }
    }
    public void print() {
        for(Map.Entry<String, Node> e : map.entrySet()) {
            p("key=" + e.getKey() + " value=" + e.getValue().print());
        }
    }
}

class Node {
    Node prev;
    Node next;
    String key;
    String value;
    public Node(String key, String value) {
        this.key = key;
        this.value = value;
    }
    public String print() {
        return ("k=" + key + " v=" + value);
    }
}

class LinkedList {
    Node head;
    Node tail;
    public LinkedList() {
        head = null;
        tail = null;
    }
    public void append(Node node) {
        if(head == null) {
            head = tail = node;
        } else {
            tail.next = node;
            tail = tail.next;
        }
    }
    public void remove(Node node) {
        // first node
        if(node.prev == null) {
            // one node only
            if(head == tail)
                head = tail = node.next;
            else // more than one node
                head = node.next;
        } else {
            // last node, more than one node
            if(node.next == null) {
                tail = node.prev;
            } else {
                // 'middle' node
                node.prev.next = node.next.prev;
            }
        }
    }
    public void print() {
        Node tmp = head;
        while(tmp != null) {
            p("k=" + tmp.key + " v=" + tmp.value);
            tmp = tmp.next;
        }
    }
}

public class try_LeaseRecentUsed {
    public static void main(String[] args) {
        test0();
//        test1();
    }
    public static void test0() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            LinkedList ll = new LinkedList();
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            Node n3 = new Node("k3", "v3");
            ll.append(n1);
            ll.append(n2);
            ll.append(n3);
            ll.print();
            fl();
            ll.remove(n1);
            ll.remove(n2);
            fl();
            ll.print();
        }
        {
            fl();
            LinkedList ll = new LinkedList();
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            Node n3 = new Node("k3", "v3");
            ll.append(n1);
            ll.append(n2);
            ll.append(n3);
            ll.remove(ll.head);
            ll.print();
        }
        {
            fl();
            LinkedList ll = new LinkedList();
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            ll.append(n1);
            ll.append(n2);
            ll.remove(ll.head);
            ll.print();
        }


        sw.printTime();
        end();
    }
    public static void test1() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        LRU lru = new LRU(2);

        Node n1 = new Node("k1", "v1");
        Node n2 = new Node("k2", "v2");
        Node n3 = new Node("k3", "v3");
        lru.append("key1", n1);
        lru.append("key2", n2);
        lru.append("key3", n3);
        lru.print();



        sw.printTime();
        end();
    }
}

