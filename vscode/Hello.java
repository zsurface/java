import java.util.*;
import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import classfile.Tuple;
import classfile.Node;

import classfile.*;

class LineInfo{
    Integer lineNum;
    String line;
    public LineInfo(){
	lineNum = 0;
	line = "";
    }
    public LineInfo(Integer lineNum, String line){
        this.lineNum = lineNum;
        this.line = line;
    }

    public void setLineNum(Integer lineNum){
        if(lineNum >= 0){
            this.lineNum = lineNum;
        }
    }
    public Integer getLineNum(){
        return this.lineNum;
    }
    public void setLine(String line){
        //  do some validation
        this.line = line;
    }
    public String getLine(){
        return this.line;
    }


    @Override
    public String toString(){
	String ret = "";
	ret = "lineNum=" + lineNum + " line=" + line;
	return ret;
    }
}

class LineDetail extends LineInfo{
    List<String> list;
    public LineDetail(Integer lineNum, String line){
        super.lineNum = lineNum;
        super.line = line;
    }
}

// transform the data
// write to file
// write to database
// write to json

final class FileFactory{

    public static List<LineInfo> getLines(String fname){
        List<FileLineNum> lineNums = Aron.readFileWithLineNum(fname);

        List<LineInfo> lsLineInfo = new ArrayList<>();
        for(FileLineNum ln : lineNums){
            LineInfo lineInfo = new LineInfo(ln.lineIndex, ln.str);
            lsLineInfo.add(lineInfo);
        }
        return lsLineInfo;
    }

    public static LineDetail lineInfoToLineDetail(LineInfo lineInfo){
        LineDetail lineDetail = new LineDetail(lineInfo.lineNum, lineInfo.line);
        List<String> lsStr = Aron.split(lineInfo.line, "\\s+");
        lineDetail.list = lsStr;
        return lineDetail;
    }

    public static List<LineDetail> lineInfoToLineDetailList(List<LineInfo> lineInfo){
        return lineInfo.stream().map(x -> lineInfoToLineDetail(x)).collect(Collectors.toList());
    }

}


public class Hello {
    public static void main(String[] args) {
       Print.pp("cool");
	   String home = getEnv("HOME");
       String fname = home + "/myfile/bitbucket/snippets/snippet_tmp.hs";    
       List<LineInfo> lineInfo = FileFactory.getLines(fname);
       List<LineDetail> listDetail = FileFactory.lineInfoToLineDetailList(lineInfo);

       Map<String, Set<String>> map = new HashMap<>();

       pl("");
       for(LineDetail detail : listDetail){
		   String line = detail.line;
		   int lineNum = detail.lineNum;

		   if(detail != null && containsSubstr(detail.line, "elliptic")){
			   pl(detail.toString());
		   }
       }

       
       

       // pb(listDetail.toString());
       
        // map.put("cat", value == null ? (new HashSet<String>()).add("kkk") : value.add("kkkkk"));           
       
    }
}
