import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


class MyTuple{
    Boolean isBalanced = true;
    List<List<String>> lss;
    public MyTuple(Boolean isBalanced, List<List<String>> lss){
	this.isBalanced = isBalanced;
	this.lss = lss;
    }
    public MyTuple(){
	this.isBalanced = true;
	this.lss = new ArrayList<>();
    }
}

public class ExtractBracket{
    public static void main(String[] args) {
        test0();
        test1();
    }


    /**
         ( )

	 (  (  )  )
               x  x

	bb :: String -> [String]
	bb = snd . foldr alg (Nothing, [])
	 where
	  alg '(' (Just (1, x), xs) = (Nothing, ('(':x):xs)
	  alg '(' (Just (n, x), xs) = (Just (n - 1, '(':x), xs)
	  alg ')' (Nothing, xs) = (Just (1, ")"), xs)
	  alg ')' (Just (n, x), xs) = (Just (n + 1, ')':x), xs)
	 
     */
    public static MyTuple extractBracket(List<String> ls){
	MyTuple tuple = new MyTuple();
	List<List<String>> lss = new ArrayList<>();
	List<String> tmpList = new ArrayList<String>();
	Queue<String> queue = new LinkedList<>();
	Stack<String> stack = new Stack<>();
	for(String s : ls){
	    if (len(trim(s)) > 0){
		String hs = head(s);
		tmpList.add(hs);
		if(hs.equals("(")){
		    stack.push(hs);
		}else if(hs.equals(")")){
		    String ps = stack.pop();
		    if(ps.equals("(")){
			if(stack.isEmpty()){
			    lss.add(tmpList);
			    tuple.isBalanced = true;
			    tuple.lss.add(tmpList);
			    tmpList = new ArrayList<>();
			}
		    }else{
			tuple.isBalanced = false;
			pp("ERROR: inbalanced bracket.");
			break;
		    }

		}
	    }
	}
	return tuple;
    }
    public static void test0(){
        beg();
	{
	    {
		fl("extractBracket 1");
		List<String> ls = list("(", ")");
		MyTuple  tuple = extractBracket(ls);
		t(tuple.isBalanced, true);
		t(head(tuple.lss), list("(", ")"));
	    }
	    {
		fl("extractBracket 2");
		List<String> ls = list("(", ")", "(", ")");
		MyTuple  tuple = extractBracket(ls);
		t(tuple.isBalanced, true);
		t(flatMap(tuple.lss), list("(", ")", "(", ")"));
	    }

	}
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        sw.printTime();
        end();
    }
} 

