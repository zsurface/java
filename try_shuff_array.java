import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_shuff_array{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        
        int[] arr = {1, 2, 3, 4};
        int width = arr.length; 

        shuffleArray(arr);
        Print.p(arr);
        
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    /**
        [1, 2, 3]
            len - 0
            r = 1
            [1, 3, 2]
            len - 1 
            r = 0 
            len - 2
            [3, 1, 2]
            r = 0
            [3, 1, 2]
    */
    public static void shuffleArray(int[] arr){
        int len = arr.length;
        Random ran = new Random();
        for(int i=0; i<len - i; i++){
            int r = ran.nextInt(len - i);
            int tmp = arr[len - 1 - i];
            arr[len - 1 - i] = arr[r];
            arr[r] = tmp;
        }
    }
} 

