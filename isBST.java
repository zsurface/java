import java.io.*;
import java.lang.String;
import java.util.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Test.*;

class isBST {
    public static void main(String args[]) {
        test0();
        test1_isBSTDef();
        test2_isBSTDef();
        test3_isBSTDef();
        test4_isBSTDef();
        test5_isBSTDef();

    }
    static void test0(){
        beg();
        {
            {
                fl("isBST 1");
                BST b1 = new BST();
                b1.insert(10);
                b1.insert(5);
                b1.insert(15);
                Node[] arr = new Node[1];
                t(isBST(b1.root, arr));
            }
            {
                fl("isBST 2");
                BST b1 = new BST();
                b1.insert(10);
                Node[] arr = new Node[1];
                t(isBST(b1.root, arr));
            }
            {
                fl("isBST 3");
                BST b1 = new BST();
                b1.insert(10);
                b1.insert(11);
                b1.insert(12);
                b1.insert(13);
                Node[] arr = new Node[1];
                t(isBST(b1.root, arr));
            }
            {
                /**
                          9
                      20
                */
                fl("isBST 4");
                Node prev = null;
                Node root = node(9);
                root.left = node(20);
                Node[] arr = new Node[1];
                t(isBST(root, arr) == false);
            }
            {
                /**
                          9
                      20
                  1
                */
                fl("isBST 5");
                Node prev = null;
                Node root = node(9);
                root.left = node(20);
                root.left.left = node(1);
                Node[] arr = new Node[1];
                t(isBST(root, arr) == false);
            }
            {
                /**
                          9
                      20
                  1       8
                */
                fl("isBST 6");
                Node prev = null;
                Node root = node(9);
                root.left = node(20);
                root.left.right = node(8);
                root.left.left = node(1);
                Node[] arr = new Node[1];
                t(isBST(root, arr) == false);
            }
        }
        end();
    }

    static void test1_isBSTDef(){
        beg();
        BST b1 = new BST();
        b1.insert(10);
        inorder(b1.root);
        t(isBSTDef(b1.root));

        end();
    }
    static void test2_isBSTDef(){
        beg();
        BST b1 = new BST();
        b1.insert(10);
        b1.insert(5);
        b1.insert(15);
        inorder(b1.root);
        t(isBSTDef(b1.root));

        end();
    }
    static void test3_isBSTDef(){
        beg();
        BST b1 = new BST();
        inorder(b1.root);
        t(isBSTDef(b1.root));

        end();
    }

    static void test4_isBSTDef(){
        beg();
        BST b1 = new BST();
        b1.root = new Node(9);
        b1.root.left = new Node(20);
        t(isBSTDef(b1.root), false);

        end();
    }

    static void test5_isBSTDef(){
        beg();
        BST b1 = new BST();
        b1.root = new Node(9);
        b1.root.left = new Node(20);
        b1.root.left.left = new Node(1);
        t(isBSTDef(b1.root), false);

        end();
    }
    public static void swap(int m, int n, Node root, Node[] first, Node[] second) {
        if(root != null) {
            swap(m, n, root.left, first, second);
            if(root.data == m) {
                first[0] = root;
            } else if(root.data == n) {
                second[0] = root;
            }
            if( first[0] != null && second[0] != null) {
                int tmp = first[0].data;
                first[0].data = second[0].data;
                second[0].data = tmp;
                first[0] = null;
                second[0] = null;
            }
            swap(m, n, root.right, first, second);
        }
    }
    //[ file=isbststatic.html title=""
    public static Node prev = null;
    public static boolean isBST(Node r) {
        if(r != null) {
            if(!isBST(r.left))
                return false;
            if(prev != null && prev.data >= r.data)
                return false;
            prev = r;
            if(!isBST(r.right))
                return false;
        }
        return true;
    }
    //]

    //[ file=isbst.html title=""
    public static boolean isBST(Node root, Node[] arr) {
        if( root != null) {
            if(!isBST(root.left, arr))
                return false;
            if(arr[0] != null && arr[0].data >= root.data)
                return false;
            arr[0] = root;
            if(!isBST(root.right, arr))
                return false;
        }
        return true;
    }
    //]

    //prev[0] = null
    public static boolean isBST2(Node root, Node[] prev) {
        if( root != null) {
            if(!isBST2(root.left, prev))
                return false;

            if(prev[0] != null && prev[0].data > root.data)
                return false;

            prev[0] = root;
            if(!isBST2(root.right, prev))
                return false;
        }
        return true;
    }
    
    //[ file=isbstdef.html title=""
    //
    // precondition node != null
    public static int max(Node node) {
        if(node.right != null)
            return max(node.right);
        else
            return node.data;
    }

    // precondition node != null
    public static int min(Node node) {
        if(node.left != null)
            return min(node.left);
        else
            return node.data;
    }

    /**
        BST definition
        1. left subtree is BST
        2. right subtree is BST
        3. max(left substree) < parent.data && min(right subtree) > parent.data
    */
    public static boolean isBSTDef(Node node) {
        if ( node != null){
            if(!isBSTDef(node.left))
                return false;

            if((node.left != null && max(node.left) >= node.data))
                return false;
            if(node.right != null && node.data >= min(node.right))
                return false;

            if(!isBSTDef(node.right))
                return false;
        }
        return true;
    }

}
