import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import javafx.util.Pair;
import java.util.stream.Collectors;

public class TokenizeString{
    public static void main(String[] args) {
        test0();
//        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            String s = "\"\"";
            List<Pair<Integer, Integer>> list = matchAllText(s);
            for(Pair<Integer, Integer> p : list){
                Print.p("k=" + p.getKey() + " v=" + p.getValue());
            }
            Print.fl("t1");
        }
        {
            String s = "\"abc\" ef \"123\"";
            List<Pair<Integer, Integer>> list = matchAllText(s);
            for(Pair<Integer, Integer> p : list){
                Print.p("k=" + p.getKey() + " v=" + p.getValue());
            }
            Print.fl("t1");
        }
        {
            String s = "\"a\"\"";
            List<Pair<Integer, Integer>> list = matchAllText(s);
            for(Pair<Integer, Integer> p : list){
                Print.p("k=" + p.getKey() + " v=" + p.getValue());
            }
            Print.fl("t1");
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }

    
    /**
    <pre>
    {@literal
        The method is inspired by Haskell matchAllText
        match all string 
        ""       => [(0, 1)]
        "abc"    => [(0, 3)]
        "" "abc" => [(0, 0), (3, 7)]

        Test File: 


    }
    {@code
    }
    </pre>
    */ 
    public static List<Pair<Integer, Integer>> matchAllText(String s){
        boolean isOpen = true;
        List<Pair<Integer, Integer>> list = new ArrayList<>();
        if(s != null){
            int len = s.length();
            int i=0;
            while(i < len){
                if(s.charAt(i) == '"'){
                    if(isOpen)
                        isOpen = false;
                    int x = 1;

                    while(i + x < len && s.charAt(i + x) != '"'){
                        x++;
                    }
                    if(i + x < len && s.charAt(i + x) == '"')
                        isOpen = true;

                    if(isOpen){
                        Pair<Integer, Integer> p = new Pair<>(i, i + x);                        
                        list.add(p);
                        i = i + x;
                    }
                }
                i++;
            }
        } 
        return list;
    }
} 

