import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static classfile.Aron.*;
import static classfile.Print.*;
 
// KEY: standard input, read from stdin
public class printStdIn {
	public static void main (String args[]) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            String input;
            while((input=br.readLine())!=null) {
                System.out.println(input);
                for(int i = 0; i < len(input); i++){
                    pl(charToInt(input.charAt(i)));
                }
            }
        }
        catch(IOException io) {
            io.printStackTrace();
        }	
  }
}

