import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class RevStr{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();
        {
            pl("test 1");
            String s = "abc";
            String rs = reverseStr(s);
            pl(rs);
        } 
        {
            pl("test 2");
            String s = "";
            String rs = reverseStr(s);
            pl(rs);
        } 
        {
            pl("test 3");
            String s = "a";
            String rs = reverseStr(s);
            pl(rs);
        } 
        {
            pl("test 4");
            String s = "ab";
            String rs = reverseStr(s);
            pl(rs);
        } 
        end();
    }

    public static String reverseStr(String s){
        StringBuffer sb = new StringBuffer(s);
        int len = sb.length();
        for(int i = 0; i < len / 2; i++){
            char c1 = sb.charAt(i);
            char c2 = sb.charAt(len - 1 - i); 
            sb.setCharAt(len - 1 - i, c1); 
            sb.setCharAt(i, c2);
        }
        return sb.toString();
    }

    public static void test1(){
        beg();
        String fname = "/Users/aaa/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

