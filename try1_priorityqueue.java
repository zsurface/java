:import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// class Node{
    // int data;
    // Node left;
    // Node right;
    // public Node(int data){
        // this.data = data;
    // }
// }

class BinTree{
    Node root;
    public void insert(Node node){
        if(root == null){
            root = node;
        }else{
            Node curr = root;
            while(curr != null){
                if(node.data < curr.data){
                    if(curr.left == null){
                        curr.left = node;
                        break;
                    }
                    else
                        curr = curr.left; 
                }else{
                    if(curr.right == null){
                        curr.right = node;
                        break;
                    }
                    else
                        curr = curr.right;
                }
            }
        }
    }
}

class Student{
    int age;
    String name;
    public Student(int age, String name){
        this.age = age;
        this.name = name;
    }
    public String toString(){
        return intToStr(age) + " " + name;
    }
}


public class try1_priorityqueue{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
    }
    public static void test0(){
        beg();

        // PriorityQueue<Student> queue = new PriorityQueue<>((x, y) -> x.name.compareTo(y.name));
        PriorityQueue<Student> queue = new PriorityQueue<>((x, y) -> x.age - y.age);
        queue.add(new Student(1, "David"));
        queue.add(new Student(2, "Jenny"));
        queue.add(new Student(0, "Nick"));
        while(queue.size() > 0){
            Student s = queue.remove();
            System.out.println(s.toString());
        }

        end();
    }
    public static List<String> readFile(String fname){
        List<String> ls = new ArrayList<>();
        try{
            BufferedReader br = new BufferedReader(new FileReader(fname));
            String line = null;

            while((line = br.readLine()) != null){
                ls.add(line);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return ls;
    }
    public static void test1(){
        beg();
        String fname = "/tmp/f.x";
        List<String> ls = readFile(fname); 
        pp(ls);
         
        end();
    }
    public static void test2(){
        beg();

        BinTree bt = new BinTree();
        bt.insert(new Node(9));
        bt.insert(new Node(4));
        bt.insert(new Node(12));
        bt.insert(new Node(6));
        printBinTree(bt.root); 
         
        end();
    }

} 

