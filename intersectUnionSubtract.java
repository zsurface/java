import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Country implements Comparable<Country>{
    private String name;

    public int compareTo(Country other){
        return name.compareTo(other.name);
    }
    public Country(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
}

/**
    Test File: /Users/cat/myfile/bitbucket/java/intersectUnionSubtract.java
*/
public class linkedHashMap{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
    }
    public static void test0(){
        Aron.beg();
        // Order-intersection
        // Keys maintains the order on intersection
        LinkedHashMap<String, Integer> map = new LinkedHashMap();
        map.put("dog", 3);
        map.put("cat", 4);

        Set<String> set = map.keySet();
        for(String k: set){
            Print.p("key=" + k + " v=" + map.get(k));
        }

        Aron.end();
    }

    public static void test1(){
        Aron.beg();
        {
            List<Integer> ls1 = Arrays.asList(1, 2, 2, 3, 3, 7); 
            List<Integer> ls2 = Arrays.asList(2, 2, 3, 6); 
            List<Integer> actual = Aron.intersect(ls1, ls2); 
            List<Integer> expected =  Arrays.asList(2, 3); 

            Test.t(actual, expected);
        }
        {
            List<Integer> ls1 = Arrays.asList(1, 2, 2, 3, 3, 7); // => 1 2 3 7 
            List<Integer> ls2 = Arrays.asList(2, 2, 3, 6);       // => 2 3 6
            List<Integer> actual = Aron.subtract(ls1, ls2);           // ls1 - (ls1 inter ls2)
            List<Integer> expected =  Arrays.asList(1, 7); 

            Test.t(actual, expected);
        }
        {
            List<Integer> ls1 = Arrays.asList(1,7); // => 1 2 3 7 
            List<Integer> ls2 = Arrays.asList(2,6);       // => 2 3 6
            List<Integer> actual = Aron.subtract(ls1, ls2);           // ls1 - (ls1 inter ls2)
            List<Integer> expected =  Arrays.asList(1, 7); 

            Test.t(actual, expected);
        }
        {
            List<Integer> ls1 = Arrays.asList(2); // => 1 2 3 7 
            List<Integer> ls2 = Arrays.asList(2,4);       // => 2 3 6
            List<Integer> actual = Aron.subtract(ls1, ls2);           // ls1 - (ls1 inter ls2)
            List<Integer> expected =  Arrays.asList(); 

            Test.t(actual, expected);
        }

        Aron.end();
    }
    public static void test2(){
        Aron.beg();

        {
            List<Integer> ls1 = Arrays.asList(); // => 1 2 3 7 
            List<Integer> ls2 = Arrays.asList();       // => 2 3 6
            List<Integer> actual = Aron.union(ls1, ls2);           // ls1 - (ls1 inter ls2)
            List<Integer> expected =  Arrays.asList(); 

            Test.t(actual, expected);
        }
        {
            List<Integer> ls1 = Arrays.asList(2); // => 1 2 3 7 
            List<Integer> ls2 = Arrays.asList(2,4);       // => 2 3 6
            List<Integer> actual = Aron.union(ls1, ls2);           // ls1 - (ls1 inter ls2)
            List<Integer> expected =  Arrays.asList(2, 4); 

            Test.t(actual, expected);
        }
        {
            List<Integer> ls1 = Arrays.asList(); // => 1 2 3 7 
            List<Integer> ls2 = Arrays.asList(2,4);       // => 2 3 6
            List<Integer> actual = Aron.union(ls1, ls2);           // ls1 - (ls1 inter ls2)
            List<Integer> expected =  Arrays.asList(2, 4); 

            Test.t(actual, expected);
        }
        {
            List<Integer> ls1 = Arrays.asList(2, 3, 4); // => 1 2 3 7 
            List<Integer> ls2 = Arrays.asList(2,4);       // => 2 3 6
            List<Integer> actual = Aron.union(ls1, ls2);           // ls1 - (ls1 inter ls2)
            List<Integer> expected =  Arrays.asList(2, 3, 4); 

            Test.t(actual, expected);
        }
        Aron.end();
    }

    /**
        Available: United States, Mexico, Canada, Costa Rica, Bermuda, Belgium
        Special: Canada, Mexico, France, Belgium
        Result: Canada, Mexico, Belgium, Bermuda, Costa Rica, United States
    */
    public static void test3(){
        Aron.beg();
        {
            Country a1 = new Country("United States");
            Country a2 = new Country("Mexico");
            Country a3 = new Country("Canada");
            Country a4 = new Country("Costa Rica");
            Country a5 = new Country("Bermuda");
            Country a6 = new Country("Belgium");

            Country c1 = new Country("Canada");
            Country c2 = new Country("Mexico");
            Country c3 = new Country("France");
            Country c4 = new Country("Belgium");

            List<Country> la = Arrays.asList(a1, a2, a3, a4, a5, a6);
            List<Country> ls = Arrays.asList(c1, c2, c3, c4);
            List<Country> actual = AppleSortedCountry(la, ls);
            List<Country> expected = Arrays.asList(c1, c2, c4, a5, a4, a1); 

            Boolean isSame = true;
            Test.t(actual.size(), expected.size());

            for(int i=0; i<actual.size()&& isSame; i++){
                if(actual.get(i).compareTo(expected.get(i)) != 0)
                    isSame = false;
            }

            Test.t(isSame, true);
        }

        Aron.end();
    }

    public static List<Country> AppleSortedCountry(List<Country> a, List<Country> s){
        if(a != null && s != null){
            // S - (S -(A int S))
            List<Country> is = Aron.intersect(a, s);
            for(Country c: is){
                Print.p("is=" + c.getName());
            }
            List<Country> sis = Aron.subtract(s, is);
            for(Country c : sis){
                Print.p("sis=" + c.getName());
            }
            List<Country> ss = Aron.subtract(s, sis);
            for(Country c: ss){
                Print.p("ss=" + c.getName());
            }
            // A - (A int S)
            List<Country> aa = Aron.subtract(a, is);

            for(Country c: aa){
                Print.p("aa=" + c.getName());
            }

            Collections.sort(aa);
            return Aron.concatList(ss, aa);
        }
        throw new IllegalArgumentException("Arguments can not be null.");
    }
} 

