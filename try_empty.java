import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_empty{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        String s = "abc";
        List<Character> ls = strToListChar(s);
        p("ls=" + ls);

        String ss = ls.stream().map(x -> x.toString()).reduce("", (x, y) -> x + y);
        p("ss=" + ss);
//        String ss1 = reduce('k', (x, y) -> x.toString() + y.toString(), ls);
//        p("ss1=" + ss1);
        

        end();
    }
    public static <T> T reduce(T id, BiFunction<T, T, T> f, List<T> list){
        return list.stream().reduce(id, (x, y) -> f.apply(x, y));
    }
    
    public static String append(Character c){
        StringBuilder sb = new StringBuilder();
        return sb.append(c).toString();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        String s = "";
        p("len=" + s.length());

        String t1 = take (0, "ab");
        String t2 = drop (0, "ab");
        p("t1=" + t1);
        p("t2=" + t2);

        end();
    }
} 

