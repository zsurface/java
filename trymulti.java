import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import classfile.Print; 
import classfile.Aron; 

public class trymulti{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        //   8 9 1
        // 8 9 1
        // 9 8 0 1 

        //     9 9
        // 8 9 1
        // 9 0 0 9
        Aron.beg();
        int[] arr1 = {9, 9};
        int[] arr2 = {1, 1};
        int[] list = multi(arr1, arr2);
        for(int i=0; i<list.length; i++){
            System.out.println(list[i]);
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    public static int[] multi(int[] a, int[] b){
        int[] ret = null;
        if( a != null && b != null){
            int la = a.length;
            int lb = b.length;
            int k = la + lb;
            int kInx = k - 1;
            int[][] arr = new int[la][k];
            for(int j=la - 1; j >= 0; j--){
                int jr = la - 1 - j;
                int carry = 0;
                for(int i=lb - 1; i >= 0; i--){
                    int n = a[j]*b[i];
                    int ir = lb - 1 - i;
                    arr[jr][kInx - jr - ir] = (carry + n) % 10;
                    carry = (carry + n) / 10;
                }
                arr[jr][kInx - jr - lb] = carry;
            }
            Aron.printArray2D(arr);

            ret = new int[k];
            int c = 0;
            for(int i=k-1; i >= 0; i--){
                int s = 0;
                for(int j=0; j<la; j++){
                   s += arr[j][i]; 
                }
                ret[i] = (s + c) % 10;
                c = (s + c) / 10;
            }
        }
        return ret;
    }
} 

