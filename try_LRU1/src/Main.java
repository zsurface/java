import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.StopWatch;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

class LRU {
    int max;
    int count = 0;
    Map<String, Node> map = new HashMap<>();
    LinkedList ll = new LinkedList();

    public LRU(int max) {
        this.max = max;
    }

    public Node get(String key) {
        Node node = map.get(key);
        if(node != null) {
            ll.remove(node);
            ll.append(node);
        }
        return node;
    }
    public void append(Node node) {
        // {k, {k, data}}
        if(node != null){
            Node n = map.get(node.key);
            if(n == null) {
                // New key
                if(count < max) {
                    // Cache is not full yet
                    count++;
                } else {
                    // Cache is full
                    Node h = ll.head;
                    String k = ll.head.key;
                    ll.remove(h);
                    map.remove(k);
                }
                // Update the list and map
                ll.append(node);
                map.put(node.key, node);
            } else {
                // Same key, noop 
            }
        }
    }
    public void remove(String key) {
        Node node = map.get(key);
        if(node != null) {
            ll.remove(node);
            map.remove(key);
            count--;
        }
    }
    public void print() {
        for(Map.Entry<String, Node> e : map.entrySet()) {
            p("key=" + e.getKey() + " value=" + e.getValue().print());
        }
    }
}

class Node {
    Node prev;
    Node next;
    String key;
    String value;
    public Node(String key, String value) {
        this.key = key;
        this.value = value;
    }
    public String print() {
        return ("k=" + key + " v=" + value);
    }
}

class LinkedList {
    Node head;
    Node tail;
    public LinkedList() {
        head = null;
        tail = null;
    }
    public void append(Node node) {
        if(head == null) {
            head = tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
    }
    public void remove(Node node) {
        // first node
        if(node.prev == null) {
            // one node only
            if(head == tail)
                head = tail = node.next;
            else {// more than one node
                head = node.next;
                head.prev = null;
            }
        } else {
            // last node, more than one node
            if(node.next == null) {
                tail = node.prev;
            } else {
                // 'middle' node
                node.prev.next = node.next.prev;
            }
        }
    }
    public void print() {
        Node tmp = head;
        while(tmp != null) {
            p("k=" + tmp.key + " v=" + tmp.value);
            tmp = tmp.next;
        }
    }
}

public class Main {
    public static void main(String[] args) {
//        test0();
        test1();
    }
    public static void test0() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            fl("t1");
            LinkedList ll = new LinkedList();
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            Node n3 = new Node("k3", "v3");
            ll.append(n1);
            ll.append(n2);
            ll.append(n3);
            ll.print();
            fl();
            ll.remove(n1);
            ll.remove(n2);
            fl();
            ll.print();
        }
        {
            fl("t2");
            LinkedList ll = new LinkedList();
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            Node n3 = new Node("k3", "v3");
            ll.append(n1);
            ll.append(n2);
            ll.append(n3);
            ll.remove(ll.head);
            ll.print();
        }
        {
            fl("t3");
            LinkedList ll = new LinkedList();
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            ll.append(n1);
            ll.append(n2);
            ll.remove(ll.head);
            ll.print();
        }
        {
            fl("t4");
            LinkedList ll = new LinkedList();
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            Node n3 = new Node("k3", "v3");
            Node n4 = new Node("k4", "v4");
            ll.append(n1);
            ll.append(n2);
            ll.append(n3);
            ll.append(n4);
            ll.print();
        }


        sw.printTime();
        end();
    }
    public static void test1() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            fl("lru1");
            LRU lru = new LRU(1);
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            lru.append(n1);
            lru.append(n2);
            lru.print();
        }
        {
            fl("lru2");
            LRU lru = new LRU(2);
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            Node n3 = new Node("k3", "v3");
            Node n4 = new Node("k4", "v4");
            lru.append(n1);
            lru.append(n2);
            lru.append(n3);
            lru.append(n4);
            lru.print();
        }

        {
            fl("lru3");
            LRU lru = new LRU(3);
            Node n1 = new Node("k1", "v1");
            Node n2 = new Node("k2", "v2");
            Node n3 = new Node("k3", "v3");
            Node n4 = new Node("k4", "v4");
            lru.append(n1);
            lru.append(n2);
            lru.append(n3);
            lru.append(n4);
            lru.print();
        }

        sw.printTime();
        end();
    }
}


