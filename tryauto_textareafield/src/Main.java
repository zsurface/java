import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        TextField textField = new TextField();

        HBox container  = new HBox(textField);
        container.setAlignment(Pos.CENTER);
        container.setPadding(new Insets(10));

        // Set Hgrow for TextField
        HBox.setHgrow(textField, Priority.ALWAYS);

        BorderPane pane = new BorderPane();
        pane.setCenter(container);
        Scene scene = new Scene(pane, 150, 150);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}