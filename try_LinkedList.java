import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
    Thu Jan 24 22:26:58 2019 
    Single LinkedList
*/
class MyNode{
    int data;
    public MyNode(int data){
        this.data = data;
    }
    MyNode prev; 
    MyNode next; 
}

class LinkedList{
    public MyNode head;
    public void insert(MyNode node){
        if(head == null){
            head = node;
        }else{
            MyNode h = head;
            while(h.next != null){
                h = h.next;
            }
            h.next = node;
        }
    }
    /**
        Insert a node in front of a list
    */
    public void insertFront(MyNode node){
        MyNode h = head;
        // if head contains at least one node
        if(h != null){
            MyNode next = h;
            h = node;
            h.next = next;
            head = h;
        }else{
            // list is empty
            head = node;
        }
    }

    /**
        Check whether a list contains a node
    */
    public Boolean contains(MyNode n){
        MyNode h = head;
        if(h == null)
            return false;
        else{
            while(h != null){
                if(h.data == n.data)
                    return true;
                h = h.next;
            }
        }
        return false;
    }
    public void print(){
        MyNode h = head;
        while(h != null){
            Print.pl(h.data);
            h = h.next;
        }
    }
    public void remove(MyNode n){
        if(contains(n)){
            MyNode h = head;
            if(h != null){
                // first node
                if(h.data == n.data){
                    head = h.next;
                }else{
                    boolean done = false;
                    while(h.next != null){
                        if(h.next.data == n.data){
                            MyNode nn = h.next.next;
                            h.next = nn;
                        }else{
                            h = h.next;
                        }
                    }
                }
            }
        }
    }
}

public class try_LinkedList{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
    }
    public static void test0(){
        Aron.beg();
        Print.pp("cool");
        LinkedList ll = new LinkedList();
        ll.insert(new MyNode(3));
        ll.insert(new MyNode(2));
        MyNode h = ll.head;
        while(h != null){
            Print.pl(h.data);
            h = h.next;
        }
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        LinkedList ll = new LinkedList();
        ll.insert(new MyNode(3));
        ll.insertFront(new MyNode(0));
        ll.insertFront(new MyNode(1));

        MyNode h = ll.head;
        while(h != null){
            Print.pl(h.data);
            h = h.next;
        }

        Aron.end();
    }
    public static void test2(){
        Aron.beg();
        LinkedList ll = new LinkedList();
        Print.pl(ll.contains(new MyNode(4)) == false);

        ll.insert(new MyNode(3));
        ll.insertFront(new MyNode(0));
        ll.insertFront(new MyNode(1));
        MyNode h = ll.head;
        Print.pl(ll.contains(new MyNode(4)) == false);
        Print.pl(ll.contains(new MyNode(1)) == true);
        Print.pl(ll.contains(new MyNode(0)) == true);
        Print.pl(ll.contains(new MyNode(3)) == true);

        Aron.end();
    }

    public static void test3(){
        Aron.beg();
        LinkedList ll = new LinkedList();
        ll.insert(new MyNode(3));
        ll.insertFront(new MyNode(0));
        ll.insertFront(new MyNode(1));
        ll.remove(new MyNode(3));
        ll.print();
        Aron.end();
    }
    public static void test4(){
        Aron.beg();
        LinkedList ll = new LinkedList();
        ll.insert(new MyNode(3));
        ll.insertFront(new MyNode(0));
        ll.insertFront(new MyNode(1));
        ll.remove(new MyNode(1));
        ll.print();
        Aron.end();
    }
    public static void test5(){
        Aron.beg();
        LinkedList ll = new LinkedList();
        ll.insert(new MyNode(3));
        ll.insertFront(new MyNode(0));
        ll.insertFront(new MyNode(1));
        ll.remove(new MyNode(0));
        ll.print();
        Aron.end();
    }
    public static void test6(){
        Aron.beg();
        LinkedList ll = new LinkedList();
        ll.insert(new MyNode(3));
        ll.insertFront(new MyNode(0));
        ll.insertFront(new MyNode(1));
        ll.remove(new MyNode(0));
        ll.remove(new MyNode(1));
        ll.remove(new MyNode(3));
        ll.print();
        Aron.end();
    }
} 

