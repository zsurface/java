import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

/**
    [1, 3, 4]
    [5, 6, 9, 10]
    []
    [3]
    [1]

    0. PriorityQueue is empty
       nextId is 1

    2. top > 1 
       nextSlot(1)

    1. top == 1 
       curr = 1
       getNext
          if empty
             curr = curr + 1
          else 
              curr = queue.remove() 
              if(curr - top == 1)
                  getNext
              else
                  nextSlot(top + 1)

*/
public class NextSlot{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        end();
    }
    public static void test1(){
        beg();
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);
            queue.add(3);

            PriorityQueue<Integer> pq = nextSlot(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);
            queue.add(2);

            PriorityQueue<Integer> pq = nextSlot(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);
            queue.add(2);
            queue.add(5);

            PriorityQueue<Integer> pq = nextSlot(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(1);

            PriorityQueue<Integer> pq = nextSlot(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }
        {
            PriorityQueue queue = new PriorityQueue<>();
            queue.add(2);

            PriorityQueue<Integer> pq = nextSlot(queue);
            while(pq.size() > 0){
                p(pq.remove());
            }
            fl();
        }

        end();
    }

    
} 

