import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.lang.StringBuffer;
import java.util.function.BiFunction;

import static classfile.Aron.*; 
import static classfile.Print.*; 
import static classfile.Test.*; 
import classfile.Node; 

// connection island
// priroityQueue 
// serialie, deserialie bin tree
// head
// interval
// permutation
// sudoku solver
// eight queen
// coin changes
// write a tries, autocomplete
//
// level print binary tree

class GNode{
    ArrayList<GNode> ls = new ArrayList<>();
    Integer data;
    public GNode(Integer data){
        this.data = data;
    }
}
class BinTree{
    Node head;
    public BinTree(){
    }
    public void print(){ 
        Node curr = head;
        while(curr != null){
            p(curr.data);
            curr = curr.next;
        }
    }
    public Node insert(Node r){ 
        Node curr = head;
        if(curr == null)
            head = curr = r;
        else{
            while(curr.next != null){
                curr = curr.next;
            }
            curr.next = r;
        }
        return curr;
    }
}

class SLL{
    Node head;
    public SLL(){} 
    public void print(){
        Node curr = head;
        while(curr != null){
            p(curr.data);
            curr = curr.next;
        }
    }
    public void insert(Integer n){
        Node curr = head;
        if(curr == null){
            curr = head = new Node(n);
        }else{
            while(curr.next != null){
                curr = curr.next;
            }
            curr.next = new Node(n);
        }
    }
}


public class facebookInterview{
    public static void main(String[] args) {
//        test0();
//        test1();
//        test2();
//        test3();
//        test4();
//        test5();
//        test6();
//        test7();
//        test8();
//        test9();
//        testMaxContinuous();
//        testnegativeMaxContinuousSum();
//        testrotateSquareArray();
//        testallPaths();
        testmultipleList();
        testallPathsWithSameDistance();
        testfindTheShortestDistancefromGivenTwoNodes();
        testshortestPath();
        testshortestPathBinNum();
        testGeneralTree();
        testmaxContinuousCount();
        testmaxConsecutiveStr();
        testreverseLinkedList();
        testmaxConnectedIsland();
        testfindLongestPath();
        testreadFile();
        testSerializeDeserialized();
        testpathBetweenTwoWords();
        testPerm();
        testisomorphicBinTree();
        testinverseBinTree();
        testrangeNum();
        testpermRangeNum();
        testmergeSortedLinkedList3();
        testfun2();
        testprintLevelBin();
        testwordDiffOneChar();
        testfun3();
        testLISRecursion();
        testtriangle();
        testlongestIncreasingSubsequence();
        testlongestIncreasingSubsequence2();
        testinorderIterate();
        testintersectSortedList();
        testmergeSortedListsAny();
    }

    public static void testmaxContinuousCount(){
        beg();
        {
            int[] arr = {1, 1};
            int m = maxContinuousCount(arr);
            t(m, 2);
        }
        {
            int[] arr = {1};
            int m = maxContinuousCount(arr);
            t(m, 1);
        }
        {
            int[] arr = {1, 2, 1, 1};
            int m = maxContinuousCount(arr);
            t(m, 2);
        }
        {
            int[] arr = {1, 2, 1, 1, 2, 2};
            int m = maxContinuousCount(arr);
            t(m, 2);
        }
        {
            int[] arr = {1, 1, 1};
            int m = maxContinuousCount(arr);
            t(m, 3);
        }
            
        end();
    }

    public static void testmaxConsecutiveStr(){
        {
            String s = "";
            String ret = maxConsecutiveStr(s);
            t(ret, "");
        }
        {
            String s = "a";
            String ret = maxConsecutiveStr(s);
            t(ret, "a");
        }
        {
            String s = "ab";
            String ret = maxConsecutiveStr(s);
            t(ret, "a");
        }
        {
            String s = "aa";
            String ret = maxConsecutiveStr(s);
            t(ret, "aa");
        }
        {
            String s = "abb";
            String ret = maxConsecutiveStr(s);
            t(ret, "bb");
        }
        {
            String s = "abaa";
            String ret = maxConsecutiveStr(s);
            t(ret, "aa");
        }
    }

    public static void print(Node root){
        Node curr = root; 
        while(curr != null){
            System.out.println(curr.data);
            curr = curr.next;
        }
    }
    public static void testreverseLinkedList(){
        {
            BinTree tree = new BinTree();
            tree.insert(new Node(1));
            fl("linkedlist");
            tree.print();

            Node first = reverseLinkedList(tree.head);
            Node head = null;
            if(first != null){
                if(first.next == null){
                    head = first;
                    head.next = null;
                }else{
                    head = first.next;
                    first.next = null;
                }
            }

            fl("reverseLinkedList");
            print(head); 
        }
        {
            BinTree tree = new BinTree();
            tree.insert(new Node(1));
            tree.insert(new Node(2));
            fl("linkedlist");
            tree.print();

            Node first = reverseLinkedList(tree.head);
            Node head = null;
            if(first != null){
                if(first.next == null){
                    head = first;
                    head.next = null;
                }else{
                    head = first.next;
                    first.next = null;
                }
            }

            fl("reverseLinkedList");
            print(head); 
        }
        {
            BinTree tree = new BinTree();
            tree.insert(new Node(1));
            tree.insert(new Node(2));
            tree.insert(new Node(3));
            fl("linkedlist");
            tree.print();

            Node first = reverseLinkedList(tree.head);
            Node head = null;
            if(first != null){
                if(first.next == null){
                    head = first;
                    head.next = null;
                }else{
                    head = first.next;
                    first.next = null;
                }
            }

            fl("reverseLinkedList");
            print(head); 
        }
    }

    /** 
     * Idea: Recurve to the end of the list
     *       return the head node and use the head node to carry the current node so curr node can be attached to the previous node.
     *
     *
     *
     *
     *    [  N1
     *       []
     *       
     *    ]
     *    [ N1 
     *       [ N2
     *          []
     *          
     *       ]
     *       N2.next == null 
     *         head = N2
     *         N2.next = N1
     *         N1.next = head
     *      return N1
     *    ]
     */
    // Use recursion to reverse a linkedlist.
    // use the head to carry the current node
    public static Node reverseLinkedList(Node curr){
        if ( curr != null ){
            Node prev = reverseLinkedList(curr.next);
            if( prev != null){ 
                Node head = prev.next == null ? prev : prev.next;
                prev.next = curr;
                curr.next = head;
            }
        }
        return curr;
    }
    public static String maxConsecutiveStr(String str){
        int c = 0;
        String ret = "";
        int max = 0;
        int currInx = -1;
        int maxInx = -1;
        if(str != null && str.length() > 0){
            max = c = 1;
            maxInx = currInx = 0;
            // a b
            // a b b a
            // a a b b b c
            for(int i=1; i<str.length(); i++){
                if(str.charAt(i-1) == str.charAt(i)) 
                    c++;
                else{
                    c = 1;
                    currInx = i;
                }

                if(c > max){ 
                    max = c;
                    maxInx = currInx;
                }
            }
        }
        if(currInx > -1){
            ret = str.substring(maxInx, maxInx + max);
        }
        return ret;
    }
    public static int maxContinuousCount(int[] arr){
        int c = 0;
        int max = 0;
        if(arr != null && arr.length > 0){
            max = c = 1;
            // a b
            // a b b a
            // a a b b b c
            for(int i=1; i<arr.length; i++){
                if(arr[i-1] == arr[i]) 
                    c++;
                else
                    c = 1;

                if(c > max) max = c;
            }
        }
        return max;
    }
    /**
     *              10
     *    1     2       3      30
     *                  20    30    40
     *
     *
     *
     *
     *
     */
    public static void testGeneralTree(){
        beg();
        GNode r = new GNode(10);
        r.ls.add(new GNode(1));
        r.ls.add(new GNode(2));
        r.ls.add(new GNode(3));

        GNode r1 = new GNode(35);
        ArrayList<GNode> lss = new ArrayList<>();
        lss.add(new GNode(20));
        lss.add(new GNode(30));
        lss.add(new GNode(40));
        r1.ls.addAll(lss);
        
        r.ls.add(r1);

        
        printGeneralTree(r);
        end();
    }
    public static void printGeneralTree(GNode r){
        if(r != null){
            p(r.data);
            for(GNode n : r.ls){
                printGeneralTree(n);
            }
        }
    }
    public static void get(){
        String[] arr = {"techie", "dangling", "cat", "scene", "ancestor", "scene", "descend", "descended", "sibling", "dangling"}; 
        Print.fl("dog", arr); 
    }
    public static String repeat(int n, Character c){
        String s ="";
        for(int i=0; i<n; i++){
            s += (c + "");
        }
        return s;
    }

    // print all prefix string from a given string
    public static void test1(){
        beg();
        List<String> ls = new ArrayList<>();
        String s = "abcaab";
        int len = s.length();
        for(int i=0; i<s.length(); i++){
            String prefix = s.substring(0, i+1);  // (0, 1) => ""    (0, 2) => "a"
            String suffix = s.substring(i+1, len);  // (0, 3) => "abc" (1, 3) => "bc"
            p(prefix);
            p(suffix);
            ls.add(prefix);
            ls.add(suffix);
        }
        Collections.sort(ls);
        fl("ls");
        p(ls);
        end();

    }
    public static void test3(){
        beg();
        List<Integer> ls = printNPrime(10);
        fl("prime");
        p(ls);
        end();
    }

    public static void test4(){
        beg();
        List<Integer> ls = printAllPrint(10);
        fl("prime");
        List<Integer> lss = Arrays.asList(2, 3, 5, 7);
        t(ls, lss); 
        end();
    }
    public static void test2(){
        beg();
        List<String> ls = Arrays.asList("dog", "cat", "god", "pig");
        Map<String, List<String>> map = new HashMap<>();
        for(String s : ls){
            char[] charArr = s.toCharArray();
            Arrays.sort(charArr);
            String str = new String(charArr);
            List<String> v = map.get(str);
            if(v == null)
                v = new ArrayList<String>();
            v.add(s);
            map.put(str, v);
        }
        fl("map");
        p(map);
        end();
    }
    public static void test0(){
        beg();
        // given a list [3, 4, 2, 4] 
        // print out n characters

        Integer[] arr1 = {1, 2, 3, 4};
        
        Character[] arr = {'c', 'a', 't', 's'};
        int width = arr.length; 

        Map<Integer, Character> charMap = new HashMap<>();
        for(int i=1; i<=26; i++){
            char cc = (char)((int)'a' + (i-1));
            charMap.put(i, new Character(cc));
        }
        
        String s = "";
        for(int i=0; i<arr1.length; i++){
            s += repeat(arr1[i], charMap.get(arr1[i]));    
        }
        p(s);
        
        
        end();
    }

    public static List<Integer> printAllPrint(Integer n){
        List<Integer> list = new ArrayList<>(); 
        // n = 2, 3, 4
        if(n >= 2){
            list.add(2);
            for(int k=3; k <= n; k++){
                boolean isPrime = true;
                for(int i=0; i<list.size(); i++){
                    if(k % list.get(i) == 0){
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime)
                    list.add(k);
            }
        }
        return list;
    }
    public static List<Integer> printNPrime(Integer n){
        List<Integer> ls = new ArrayList<>();
        if(n > 0){
            ls.add(2);
            int k = 1;
            int num = 3;
            // n = 1
            // n = 2
            while(k < n){
                boolean isPrime = true;
                for(int i=0; i<ls.size(); i++){
                    if(num % ls.get(i) == 0){
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime){
                    ls.add(num);
                    k++;
                }

                num++;
            }
        }
        return ls;
    }
    
    // -1, -2, -3
    public static Integer negativeMaxContinuousSum(Integer[] arr){
        Integer m2 = Integer.MIN_VALUE, m1 = Integer.MIN_VALUE; 
        if(arr != null && arr.length > 0){

            // 2, -1 =>
            // 2, -3 => 2
            // -1, 2 => 
            m2 = arr[0];
            m1 = arr[0];
            Integer curr = m1;
            for(int i=1; i< arr.length; i++){
                if(arr[i] > m2)
                    m2 = arr[i];

                if(curr < 0)
                    curr = 0;
                curr += arr[i];

                if(curr > m1)
                    m1 = curr;
            }
        }
        return Math.max(m2, m1);
    }
    public static void testnegativeMaxContinuousSum(){
        beg();
        {
            Integer[] arr = {-1};
            Integer m = negativeMaxContinuousSum(arr);
            fl("negativeMaxContinuousSum");
            t(m, -1);
        }
        {
            Integer[] arr = {-2, 3};
            Integer m = negativeMaxContinuousSum(arr);
            fl("negativeMaxContinuousSum");
            t(m, 3);
        }
        {
            Integer[] arr = {-2, -1};
            Integer m = negativeMaxContinuousSum(arr);
            fl("negativeMaxContinuousSum");
            t(m, -1);
        }
        {
            Integer[] arr = {-2, -1, 0};
            Integer m = negativeMaxContinuousSum(arr);
            fl("negativeMaxContinuousSum");
            t(m, 0);
        }
        {
            Integer[] arr = {-2, -1, 0, 1};
            Integer m = negativeMaxContinuousSum(arr);
            fl("negativeMaxContinuousSum");
            t(m, 1);
        }
        {
            Integer[] arr = {-2, -1, 0, 4, -5, 5};
            Integer m = negativeMaxContinuousSum(arr);
            fl("negativeMaxContinuousSum");
            t(m, 5);
        }
        end();
    }


    // -1, -2
    // 2 -3, 4
    // 2, 0  4
    // 2, 3  4
    // 2  6  -5
    public static Integer printMaxContinuousSum(Integer[] arr){
        Integer m = 0;
        Integer curr = 0;
        for(int i=0; i<arr.length; i++){
            if(curr < 0)
                curr = 0;

            curr += arr[i];

            if(curr > m)
                m = curr;
        }
        return m;
    }
    public static void testMaxContinuous(){
        {
            Integer[] arr = {1};
            Integer m = printMaxContinuousSum(arr);
            fl("printMaxContinuousSum");
            t(m, 1);
        }
        {
            Integer[] arr = {-2, 3};
            Integer m = printMaxContinuousSum(arr);
            fl("printMaxContinuousSum");
            t(m, 3);
        }
        {
            Integer[] arr = {1, -2, 3, 4};
            Integer m = printMaxContinuousSum(arr);
            fl("printMaxContinuousSum");
            t(m, 7);
        }
        {
            Integer[] arr = {1, -2, 3, 4};
            Integer m = printMaxContinuousSum(arr);
            fl("printMaxContinuousSum");
            t(m, 7);
        }
        {
            Integer[] arr = {-2};
            Integer m = printMaxContinuousSum(arr);
            fl("printMaxContinuousSum");
            t(m, 0);
        }
    }

    public static Node right(Node root){
        Node curr = root;
        while(curr.right != null){
            curr = curr.right;    
        }
        return curr;
    }
    public static Node left(Node root){
        Node curr = root;
        while(curr.left!= null){
            curr = curr.left;    
        }
        return curr;
    }

    /**
     * 1. null node is BST
     * 2. minimum of left subtree is less than parent and maximum of right subtree is greater than the parent
     * 3. left subtree is BST
     * 4. right subtree is BST
     *
     */
    public static boolean isBST(Node root){
        if(root != null){
            if(root.left != null && right(root.left).data > root.data)
                return false;
            if(root.right != null && left(root.right).data < root.data)
                return false;
            if(!isBST(root.left))
                return false;
            if(!isBST(root.right))
                return false;
        }
        return true;
    }

    public static void insert(Node root, int n){
        if(root == null)
            root = new Node(n);
        else{ 
            if(n < root.data){
                if(root.left == null)                     
                    root.left = new Node(n);
                else
                    insert(root.left, n);
            }else{
                if(root.right == null)
                    root.right = new Node(n);
                else
                    insert(root.right, n);
            }
        }
    }
    
    public static void test5(){
        beg();
        Node r = new Node(9);
        insert(r, 4);
        insert(r, 3);
        insert(r, 1);
        insert(r, 12);
        insert(r, 11);
        insert(r, 14);
        fl("isBST(r)");
        t(isBST(r), true);
        // fl("binImage");
        // binImage(r);
        end();
    }

    public static void testshortestPath(){
        beg();
        Node r = new Node(9);
        insert(r, 4);
        insert(r, 3);
        insert(r, 1);
        insert(r, 12);
        insert(r, 11);
        insert(r, 14);
        fl("isBST(r)");
        //fl("binImage");
        // binImage(r);
        ArrayList<Integer> ls = new ArrayList<>();
        ArrayList<Integer> min = new ArrayList<>();
        shortestPathBin(r, ls, min);
        fl("min");
        p(min);
        end();
    }

    public static void test6(){
        beg();
        Node r = new Node(9);
        fl("isBST(r)");
        t(isBST(r), true);
        end();
    }
    public static void test7(){
        beg();
        Node r = new Node(9);
        insert(r, 4);
        insert(r, 3);
        fl("isBST(r)");
        t(isBST(r), true);
        end();
    }
    public static void test8(){
        beg();

        Node r = new Node(9);
        insert(r, 4);
        insert(r, 3);
        insert(r, 1);
        fl("isBST(r)");
        t(isBST(r), true);

        end();
    }

    public static void testmultipleList(){
	beg();
	{
	    Integer[] arr = {2, 3, 4};
	    Integer[] arr1 = multipleList(arr);
	    Integer[] arr2 = {12, 8, 6};
	    fl("arr1");
	    p(arr1);
	    fl("arr2");
	    p(arr2);
	    fl("test");
	    t(arr1, arr2);
	}
	{
	    Integer[] arr = {2, 3};
	    Integer[] arr1 = multipleList(arr);
	    Integer[] arr2 = {3, 2};
	    fl("arr1");
	    p(arr1);
	    fl("arr2");
	    p(arr2);
	    fl("test");
	    t(arr1, arr2);
	}
	{
	    Integer[] arr = {2, 3, 1};
	    Integer[] arr1 = multipleList(arr);
	    Integer[] arr2 = {3, 2, 6};
	    fl("arr1");
	    p(arr1);
	    fl("arr2");
	    p(arr2);
	    fl("test");
	    t(arr1, arr2);

	}
	end();
	
    }
    
    /**
     *  4   4  [4]
     *  3  [3]  3
     *  [2] 2   2 
     *  a1[0] = 1,  a1[i+1] = a1[i]*a[i]
     *  a2[len-1] = 1
     * 
     */
    public static Integer[] multipleList(Integer[] arr){
        Integer[] arr1 = null;
        Integer[] arr2 = null;
        if(arr != null){
            int len = arr.length;
            arr1 = new Integer[len];
            arr2 = new Integer[len];
            arr1[0] = 1;
            arr2[len-1] = 1;
            for(int i=0; i<len - 1; i++){
                arr1[i+1] = arr1[i]*arr[i];
                // inverse the index
                arr2[len - 1 - (i+1)] = arr2[len - 1 - i] * arr[len - 1 - i];
            }
            fl("arr1");
            p(arr1);
            fl("arr2");
            p(arr2);

            for(int i=0; i<len; i++){
                arr1[i] = arr1[i]*arr2[i];
            }
        }
        return arr1;
    }
}

