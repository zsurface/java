import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class GNode<T>{
    T data;
    List<GNode> list = new ArrayList<>();
}

public class try_generic_tree{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        GNode<Integer> n1= new GNode<>();
        n1.data = 1;

        GNode<Integer> n2= new GNode<>();
        n2.data = 2;
        GNode<Integer> n3= new GNode<>();
        n3.data = 3;

        n1.list = new ArrayList<>();
        n1.list.add(n2);
        n1.list.add(n3);

        printTree(n1);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    
    public static void printTree(GNode curr){
        if(curr != null){
            Print.p(curr.data);
            for(int i=0; i<curr.list.size(); i++){
                printTree((GNode)curr.list.get(i));
            }
        }
    }
} 

