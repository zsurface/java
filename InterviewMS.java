import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class InterviewMS{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
	{
	    
	}
        end();
    }
    public static void test1(){
        beg();
	{
	    var ls = list(1, 2, 3);
	    var rot = rotateLeft(1, ls);
	    t(rot, list(2, 3, 1));
	}
        end();
    }
    public static List<Integer> rotateLeft(Integer n, List<Integer> ls){
	List<Integer> left = new ArrayList<>();
	List<Integer> right = new ArrayList<>();
	Integer k = 0;
	for(Integer e : ls){
	    if(k < n){
		left.add(e);
		k++;
	    }else{
		right.add(e);
	    }
	}
	right.addAll(left);
	return right;
    }
} 

