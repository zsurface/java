import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.BST;
import classfile.Node;
import java.util.stream.*;
import java.util.stream.Collectors;

public class levelorder_without_queue{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        BST bst = new BST();
        bst.insert(5);
        bst.insert(2);
        bst.insert(4);
        bst.insert(8);
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        Integer k = 1;
        createMap(bst.root, map, k);
        Integer l = 0;
        printLevel(map);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }

    public static void createMap(Node r, Map<Integer, Integer> map, Integer k){
        if( r != null){
            map.put(k, r.data);
            createMap(r.left, map, 2*k + 1);
            createMap(r.right, map, 2*k + 2);
        }
    }
    public static Integer pow(Integer n, Integer exp){
        if(exp == 0)
            return 1;
        else{
            if(exp % 2 == 0)
                return pow(n, exp/2)*pow(n, exp/2);
            else
                return n*pow(n, exp/2)*pow(n, exp/2);
        }
    }
    public static void printLevel(Map<Integer, Integer> map){
        int count = 0;
        Integer l = 0;
        while(count < map.size()){
            // Integer p = pow(2, l);
            Integer p = (int)Math.pow(2, l);
            for(int i=0; i<p; i++){
                Integer key = map.get(p + i);
                if(key != null){
                    System.out.print(key + " ");
                    count++;
                }
            }
            System.out.println("");
            l++;
        }
    }
} 

