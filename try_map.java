import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;


class Student implements Comparable<Student>{
    public String name;
    public int compareTo(Student other){
        // 3 2 1
        return  - name.compareTo(other.name);
    }
    
    public Student(String name){
        this.name = name;
    }
    public void print(){
        Print.p("name=" + name);
    }
}

class Person{
    public String name;
    public Person(String name){
        this. name = name;
    }
    public void print(){
        Print.p("name=" + name);
    }
}

class ComPerson implements Comparator<Person>{
    public int compare(Person s1, Person s2){
        return s1.name.compareTo(s2.name); 
    }
}

public class try_map{
    public static void main(String[] args) {
        test0();

        {
            List<Student> list = new ArrayList<>();
            list.add(new Student("David"));
            list.add(new Student("Michael"));
            list.add(new Student("Lion Air"));
            list.add(new Student("Max 737 8"));
            list.add(new Student("Boeing Max 737 8"));
            Collections.sort(list);

            for(Student s : list){
                s.print();
            }
        }
        {
            List<Person> list = new ArrayList<>();
            list.add(new Person("David"));
            list.add(new Person("Michael"));
            list.add(new Person("Michelle"));
            list.add(new Person("Joyce"));
            Collections.sort(list, new ComPerson());
            Print.fl();

            for(Person s : list){
                s.print();
            }
        }


        test1();
    }
    public static void test0(){
        Aron.beg();

        
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
} 

