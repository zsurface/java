import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class tryPrint{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();

        end();
    }
    public static void test1(){
        beg();

        List<List<Integer>> ss = list(list(1, 2), list(4, 5));
        printList(ss, 4);

        end();
    }

  
  public static <T> void printList(List<T> list, Integer nSpace) {
    Node node = new Node();
    String str = "";
    for(T item : list) {
      Class nclass = item.getClass();
      if(nclass.isInstance(node)){
        str += ((Node)item).data + " ";
      }
      else{
        // str += (item.toString() + " ");
        String strFormat = "%" + nSpace + "s";
        str += String.format(strFormat, item.toString());
      }
    }
    String s = "";
    if(str.length() > 0){
      s = Aron.init(str);
    }
    pp("[" + s + "]");
    System.out.println("");
  }
} 

