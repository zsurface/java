import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class deleteIndexFrom{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        // "abcde" -> 
        String s = "abcde";
        int startIndex = 2;
        int endIndex= 4;
        String ss = delete(s, startIndex, endIndex);
        Print.p("s=" + s + " ss=" + ss);

        Aron.end();
    }

    
    /**
    <pre>
    {@literal
        Delete String from startIndex to (endIndex - 1)
    }
    {@code
    }
    </pre>
    */ 
    public static String delete(String s, int startIndex, int endIndex){
        StringBuilder sb = new StringBuilder(s);
        sb.delete(startIndex, endIndex);
        return sb.toString();
    }
        
} 

