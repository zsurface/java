import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class parseRandomNote{
    public static void main(String[] args) {
        test0();
        test1();
    }
    
    public static void test0(){
        beg();
        StopWatch sw = new StopWatch();
        sw.start();

        // String fn = "/Library/WebServer/Documents/zsurface/randomNote.txt";
        String fn = "/Users/cat/myfile/bitbucket/testfile/removeCharFromFile2.txt";
        char[] charList = {':', ',', '/', '.', ';'};
        List<String> list = removeCharListFromFileNotModified(fn, charList);


        List<String> ls = readFile(fn);
        String pat = "^\\s*[-]{10,}";

        List<ArrayList<String>> list2d = splitListRegex(pat, ls);
        p(list2d);
        p("sz=" + list2d.size());

        {
            for(ArrayList<String> row : list2d){
                for(String s : row){
                    List<String> lss = split(s, "\\s+");
                    p(lss);
                }
            }
        }

        
        

        sw.printTime();
        end();
    }
    
    

    public static void test1(){
        char[] chars = {',', '.'};
        String s = deleteCharFromStr("abc,e.", chars);
        p("s=" + s);

    }
} 

