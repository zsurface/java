import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;


class Color{
    public int r;
    public int g;
    public int b;
}

class State {
    public int x;
    public int y;
    public Color color = new Color();
}

class DoUndo{
    Stack<State> dos = new Stack();
    Stack<State> undos = new Stack();
    State curr = new State();
    public DoUndo(){
    }
    public void edit(){
        Random r = new Random();
        curr.x = r.nextInt(2);
        curr.y = r.nextInt(2);
        curr.color.r = r.nextInt(2);
        curr.color.g = r.nextInt(2);
        curr.color.b = r.nextInt(2);
    }
    public void saveOp(){
        if(dos.size() > 0){
            if(isModified(dos.peek())){
                dos.push(curr);
            }
        }else{
            dos.push(curr);
        }

    }
    public void undoOp(){
        if(dos.size() > 0){
            curr = dos.pop();
            undos.push(curr);
        }
        if(dos.size() > 0)
            curr = dos.peek();
    }
    public void redoOp(){
        if(undos.size() > 0){
            curr = undos.pop();
            dos.push(curr);
        }
    }
    private Boolean isModified(State s){
        return !(s.x == curr.x && 
                 s.y == curr.y && 
                 s.color.r == curr.color.r &&
                 s.color.g == curr.color.g &&
                 s.color.b == curr.color.b);
    }
}

/**

    Stack dos = new Stack();
    Stack undos = new Stack();

    void do()
    void undo()
    Status edit()
    Boolean isModified(top, curr)
*/

public class try_do_undo_redo{
    public static void main(String[] args) {
//        standardInput();
        test0();
//        test1();
    }

    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();
        List<String> list = geneFileName("/tmp", 10, "txt");
        p(list);

        List<String> randList = geneRandomStrList(10000000);
        for(String fn : list){
            writeFile(fn, randList);
        }

        sw.printTime();
        end();
    }
    
    public static void test1(){

        DoUndo doit = new DoUndo();

        Scanner scanner = new Scanner(System.in);
        String s = "";
        while(!s.equals("x")){
            if(scanner.hasNext()){
                s = scanner.next();
                if(s.equals("e")){
                    doit.edit();
                }else if(s.equals("s")){
                    doit.saveOp();
                }else if(s.equals("undo")){
                    doit.undoOp();
                }else if(s.equals("redo")){
                    doit.redoOp();
                }
                p("s=" + s);
            }
        }
    }
} 

