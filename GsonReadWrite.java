import java.util.*;
import java.io.*;
import java.util.stream.*;
import org.json.JSONException;
import org.json.JSONObject;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import static classfile.Aron.*;
import static classfile.Print.*;

// KEY: object to json, class to json
class Person {
    private int id;
    private String name;
    private List<Integer> list;

    public Person(int id, String name, List<Integer> list){
        this.id = id;
        this.name = name;
        this.list = list;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getList(){
        return list;
    }

    public void setList(List<Integer> list){
        this.list = list;
    }

    @Override
    public String toString() {
        String s = "";
        for(Integer n : list){
            s += n.toString() + " ";
        }
        return "Person{" + "id=" + id + ", name=" + name + "," + "list=" + s + "}";
    }
}

public class GsonReadWrite {
    public static void main(String[] args) {
        {
            test2();
        }
    }

    /**
     * This is a mixed implementation based on stream and object model. The JSON
     * file is read in stream mode and each object is parsed in object model.
     * With this approach we avoid to load all the object in memory and we are only
     * loading one at a time.
     */

    public static void test2(){
        beg();
        {
            // Object to json
            List<Integer> ls = list(1, 2, 3);
            Gson gson = new Gson();
            Person person = new Person(10, "Leanna", ls);
            String json = gson.toJson(person);
            fl("Person to Json");
            pl("json=" + json);

            // Json to object
            Person p = gson.fromJson(json, Person.class);
            fl("Json to Person");
            pl("Person=" + p.toString());
            
        }
        end();
    }
}

