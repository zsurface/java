import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Person{
    int age;
    String name;
    public Person(int age, String name){
        this.age = age;
        this.name = name;
    }
    public String toString(){
        return intToStr(age) + " " + name;
    }
}

public class try_c1{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();

        PriorityQueue<Person> queue = new PriorityQueue<Person>((x, y) -> x.age - y.age);
        queue.add(new Person(1, "David"));
        queue.add(new Person(2, "Jenny"));
        queue.add(new Person(4, "Jenny"));
        queue.add(new Person(1, "Jenny"));
        queue.add(new Person(9, "Jenny"));

        while(queue.size() > 0){
            Person pe = (Person)queue.remove();
            pp(pe.toString()); 
        }

        end();
    }
} 

