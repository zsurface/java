import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.BST;
import classfile.Node;
import java.util.stream.*;
import java.util.stream.Collectors;

// Mon Feb 18 21:54:10 2019 
public class SerializeBinaryTreeWithBinaryHeap{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        BST bst = new BST(); 

        bst.insert(5);
        bst.insert(2);
        bst.insert(8);
        bst.insert(4);
        Aron.inorder(bst.root);
        Integer k = 0;
        String fname =  "/tmp/bst.txt";
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
            write(bst.root, k, bw);
            bw.close();

            Map<Integer, Integer> map = readFile(fname);
            Node r = buildTree(map, k);

            List<Integer> list = new ArrayList<Integer>(); 
            Aron.inorderToList(r, list);

            // immutable list
            List<Integer> ls = Arrays.asList(2, 4, 5, 8);
            Test.t(ls, list);

        }catch(IOException io){
            Print.p("Ex" + io.getMessage());
        }
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    public static Map<Integer, Integer> readFile(String fname){
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        try{
            BufferedReader bf = new BufferedReader(new FileReader(fname));
            String s;
            while((s = bf.readLine()) != null){
                String[] arr = s.split(":");
                map.put(Integer.parseInt(arr[0].trim()), Integer.parseInt(arr[1].trim()));
            }
            bf.close();
        }catch(IOException e){
        }
        return map;
    }
    public static Node buildTree(Map<Integer, Integer> map, Integer k){
                    if(map.get(k) != null){
                        Node r = new Node(map.get(k));
                        r.left = buildTree(map, 2*k+1);
                        r.right = buildTree(map, 2*k+2);
                        return r;
                    }
                    return null;

    }
    public static void write(Node r, Integer k, BufferedWriter bw){
        try{
            if(r != null){
                bw.write(k + ":" + r.data + "\n");
                write(r.left, 2*k + 1, bw); 
                write(r.right, 2*k + 2, bw);
            }
        }catch(IOException io){
        }
    }
} 

