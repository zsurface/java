import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class PartitionBinaryArray{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
    }

    public static int[] dropArrBoth(int h, int t, int[] arr){
        int[] ret = new int[0];
        int len = arr.length;
        if(h + t <= len){
            int[] tmp = drop(h, arr);
            ret = dropArrEnd(t, tmp);
        }
        return ret;
    }
    public static int[] dropArrEnd(int n, int[] arr){
        int len = arr.length;
        int m = len - n;
        return takeArr(m, arr);
    }
    public static int[] takeArrEnd(int n, int[] arr){
        int[] ret = new int[0];
        int len = arr.length;
        if (n <= len){
            ret = new int[n];
            // a b c d e, n = 2
            // 5 - n = 3 
            int k = 0;
            for(int i = len - n; i < len; i++){
                ret[k] = arr[i];    
                k++;
            }
        }
        return ret;
    }
    /**
    <pre>
    {@literal
        http://localhost/html/indexWhatIdidtoday.html#orgaf31f29
    }
    {@code
    }
    </pre>
    */
    public static int[] partitionBinaryArray(int[] arr){
        int[] ret = {-1, -1};
        int len = arr.length;
        if(len > 2){
            int i = 1;
            int j = 1;
            // a b c d
            //   x   x 
            // head rest tail
            // if head == tail
            //    if rest > tail
            //        j--
            //    if rest == tail
            //        done 
            // if head > tail
            //       j--
            // else
            //    i++
            ///   a b c d
            //             j = 3
            //    (4 - 3)
            while(i + 1 < j){
                int[] head = takeArr(i, arr);
                int[] tail = takeArrEnd(j, arr);
                int[] rest = dropArrBoth(i, j, arr); 
                int hNum = arrayToIntBase(head, 2);
                int tNum = arrayToIntBase(tail, 2);
                int rNum = arrayToIntBase(rest, 2);
                pb("hNum");
                p(head);
                pp(hNum);
                p(tail);
                pb("tNum");
                pp(tNum);
                pb("rNum");
                p(rest);
                pp(rNum);
                pp("\n");
            
                // head rest tail
                if (hNum == tNum){
                    if(rNum > tNum){
                        j++;
                    }else if(rNum == tNum){
                        break;
                    }else{
                        return ret;
                    }
                }else if(hNum > tNum){
                    j++; 
                }else {
                    i++;
                }
            }
            if(i + 1 < j){
                // a b c d
                // i = 1, j = 1
                // (i-1) (len - j - 1)
                ret[0] = i - 1;
                ret[1] = len - i - 1;
            }
        }
        return ret;
    }
    public static void test0(){
        beg();
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = takeArrEnd(2, arr);
            int[] expArr = {3, 4};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = takeArrEnd(1, arr);
            int[] expArr = {1};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = takeArrEnd(0, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = takeArrEnd(2, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        end();
    }
    public static void test1(){
        beg();
        {
            int[] arr = {1};
            int[] ret = dropArrEnd(0, arr);
            int[] expArr = {1};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrEnd(0, arr);
            int[] expArr = {1, 2};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrEnd(2, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrEnd(3, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = dropArrEnd(2, arr);
            int[] expArr = {1, 2};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = dropArrEnd(1, arr);
            int[] expArr = {1, 2, 3};
            t(ret, expArr);
        }
        end();
    }
    public static void test2(){
        beg();
        {
            // int[] arr = {1, 1, 1};
            // int[] ret = partitionBinaryArray(arr);
            // int[] expArr = {0, 2};
            // t(ret, expArr);
        }
        {
            int[] arr = {1, 0, 1, 0, 1, 0};
            int[] ret = partitionBinaryArray(arr);
            int[] expArr = {1, 4};
            t(ret, expArr);
        }
        end();
    }
    public static void test3(){
        beg();
        {
            int[] arr = {1};
            int[] ret = dropArrBoth(1, 1, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = dropArrBoth(1, 2, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrBoth(1, 1, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3};
            int[] ret = dropArrBoth(1, 1, arr);
            int[] expArr = {2};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = dropArrBoth(0, 1, arr);
            int[] expArr = {1, 2, 3};
            t(ret, expArr);
        }
        end();
    }

} 

