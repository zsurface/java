import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import java.io.*;

// KEY: run command, run shell, run shell pipe, run shell optional argument
public class try_cmd {
    public static void main(String args[]) {
        String s = null;
        String[] cmd = {"/usr/local/bin/bash", "-c", "s grep | cx"};

        //         stdout      stderr       exitcode
        //           ↓            ↓            ↓  
        Triple<List<String>, List<String>, Integer> tri = run(cmd);
        pl(tri.x);
    }
}
