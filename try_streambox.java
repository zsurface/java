import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_streambox{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        {
            List<Integer> list = Arrays.asList(1, 4, 5, 6, 7, 9);
            List<Integer> ls = IntStream.range(0, list.size()).mapToObj(i -> 2*list.get(i)).collect(Collectors.toList());
            p(ls);
        }
        {
            
            List<Integer> list = Arrays.asList(1, 4, 5, 6, 7, 9);
            List<Integer> ls = list.stream().map(i -> 2*i).collect(Collectors.toList());
            p(ls);
        }
        end();
    }
    public static void test1(){
        beg();

        List<Integer> list = IntStream.range(0, 10).boxed().collect(Collectors.toList());
        p(list);

        end();
    }
} 

