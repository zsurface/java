import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import static classfile.ImageInfo.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class TryScaleImage{
    public static void main(String[] args) {
		{
			fl("getImageAllDir 0");
			// String cPath = "/tmp/img";
			String cPath = "/Users/aaa/try/img";
			List<List<ImageInfo>> lss = getImageAllDir(cPath);
		
			for(List<ImageInfo> ss : lss){
				fl(cPath);
				for(ImageInfo m : ss){
					pl(m);
				}
			}
		}
		{
			fl("getImageAllDir 1");
			// String cPath = "/tmp/img";
			String cPath = "/Users/aaa/try/img";
			List<List<ImageInfo>> lss = getImageAllDir(cPath);
		
			for(List<ImageInfo> ss : lss){
				fl(cPath);
				List<ImageInfo> ii = getWidthHeight(ss);
				for(ImageInfo m : ii){
					pl(m);
				}
			}
		}

		{
			{
				fl("isSame 0");
				List<Integer> ls = list(1);
				Boolean b = isSame(ls);
				t(b, true);
			}
			{
				fl("isSame 1");
				List<Integer> ls = list(1, 1);
				Boolean b = isSame(ls);
				t(b, true);
			}
			{
				fl("isSame 2");
				List<Integer> ls = list(1, 2);
				Boolean b = isSame(ls);
				t(b, false);
			}
			{
				fl("isSame 3");
				List<Integer> ls = list(1, 1, 2);
				Boolean b = isSame(ls);
				t(b, false);
			}
			{
				fl("isSame 3");
				List<Integer> ls = list(1, 1, 1);
				Boolean b = isSame(ls);
				t(b, true);
			}
		}
		{
			fl("getImageOneDir 0");
			String bitbucket = getEnv("b");
			String path = bitbucket + "/" + "testfile/img";
			List<String> ld = listDir(path);
			if(len(ld) > 0){
				String fullPath = path + "/" + ld.get(0);
				List<ImageInfo> ls = getImageOneDir(fullPath);
				t(len(ls), 3);
				pl(ls);
			}
		}
    }
	
	
	public static List<List<ImageInfo>> scaleImageInfo(List<List<ImageInfo>> ls){
		List<List<ImageInfo>> ret = new ArrayList<>();
		
		return ret;
	}

    public static List<List<ImageInfo>> getImageAllDir(String cPath){
		List<List<ImageInfo>> lss = new ArrayList<>();
		List<String> ls = readDir(cPath);
		for(String p : ls){
			String fullPath = cPath + "/" + p;
			pl(fullPath);
			List<ImageInfo> imgList = getImageOneDir(fullPath);
			lss.add(imgList);
		}
		return lss;
    }
} 

