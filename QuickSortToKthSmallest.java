import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;


class Pair{
    int c;
    int r;
    int value;

    public Pair(int c, int r, int value){
        this.c = c;
        this.r = r;
        this.value = value;
    }

    @Override
    public String toString(){
        String s = "(" + c + "," + r + ")";
        return s;
    }
}

/*
    KEY: k smallest, quicksort to k smallest, kth smallest, nth smallest
*/
public class QuickSortToKthSmallest{
    public static void main(String[] args) {
        // test0();
        // test1();
        // test2();
        // test3();
        // test4();
        test5();
        test6();
        test7();
    }
    public static void test4(){
        beg();
        {
            Pair[] arr = new Pair[8];
            int c = 0;
            queen8(c, arr); 
        }
        end();
    }
    public static void test5(){
        beg();
        {
            int[] arr = {2};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 0;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 2);
        }
        {
            int[] arr = {2, 1};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 0;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 1);
        }
        {
            int[] arr = {1, 2};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 0;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 1);
        }
        {
            int[] arr = {1, 2};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 1;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 2);
        }
        {
            int[] arr = {2, 1, 3};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 0;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 1);
        }
        {
            int[] arr = {3, 2, 1};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 0;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 1);
        }
        {
            int[] arr = {3, 2, 1};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 2;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 3);
        }
        {
            int[] arr = {2, 1, 3};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 1;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 2);
        }
        {
            int[] arr = {2, 1, 3};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 2;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 3);
        }
        {
            int[] arr = {2, 1, 3, 6};
            int lo = 0;
            int hi = arr.length - 1;
            int k = 3;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 6);
        }
        {
            int[] arr = {6, 3, 2, 1};  // 1 2 3 6
            int lo = 0;
            int hi = arr.length - 1;
            int k = 3;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 6);
        }
        {
            int[] arr = {6, 3, 2, 1, 7, 8};  // 1 2 3 6 7 8 
            int lo = 0;
            int hi = arr.length - 1;
            int k = 4;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 7);
        }
        {
            int[] arr = {6, 3, 2, 1, 7, 8};  // 1 2 3 6 7 8 
            int lo = 0;
            int hi = arr.length - 1;
            int k = 5;
            int n = ksmallest(arr, lo, hi, k);
            t(n, 8);
        }
        end();
    }
    public static void test6(){
        beg();
        {
            int[] arr = {2};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortNew(arr, lo, hi);
            int[] expArr = {2};
            t(arr, expArr);
        }
        {
            int[] arr = {3, 2};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortNew(arr, lo, hi);
            int[] expArr = {2, 3};
            t(arr, expArr);
        }
        {
            int[] arr = {2, 3};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortNew(arr, lo, hi);
            int[] expArr = {2, 3};
            t(arr, expArr);
        }
        {
            int[] arr = {2, 3, 1};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortNew(arr, lo, hi);
            int[] expArr = {1, 2, 3};
            t(arr, expArr);
        }
        {
            int[] arr = {2, 3, 1, 4};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortNew(arr, lo, hi);
            int[] expArr = {1, 2, 3, 4};
            t(arr, expArr);
        }
        {
            int[] arr = {5, 4, 3, 2, 1};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortNew(arr, lo, hi);
            int[] expArr = {1, 2, 3, 4, 5};
            t(arr, expArr);
        }
        {
            int[] arr = {1, 2, 3, 4, 5};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortNew(arr, lo, hi);
            int[] expArr = {1, 2, 3, 4, 5};
            t(arr, expArr);
        }
        end();
    }

    public static void test7(){
        beg();
        {
            int[] arr = {5, 4, 3, 2, 1};
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 0;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 1;
            t(kth, expKth);
        }
        {
            int[] arr = {1};
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 0;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 1;
            t(kth, expKth);
        }
        {
            int[] arr = {1, 2};
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 0;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 1;
            t(kth, expKth);
        }
        {
            int[] arr = {2, 1};
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 0;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 1;
            t(kth, expKth);
        }
        {
            int[] arr = {2, 1, 3};
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 1;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 2;
            t(kth, expKth);
        }
        {
            int[] arr = {2, 1, 3};
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 2;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 3;
            t(kth, expKth);
        }
        {
            int[] arr = {2, 1, 3};
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 2;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 3;
            t(kth, expKth);
        }
        {
            int[] arr = {2, 1, 3, 4, 9, 5};  // 1 2 3 4, 5, 9
            int lo = 0;
            int hi = arr.length - 1;
            int kInx = 3;
            int kth = quickSortNewKIndex(arr, lo, hi, kInx);
            int expKth = 4;
            t(kth, expKth);
        }
        end();
    }

    /*
                                           x 
                             arr [4, 2, 1, 3]
                     partition lo, hi, arr
                     1  2  [3] 4
         partition lo, hi, [1 2]    partition lo, hi [4]

         [1 2]     [4]
    */
    public static int partition(int[] arr, int lo, int hi){
        int len = hi - lo + 1; 
        int[] tmpArr = new int[len]; 

        int i = 0;
        int j = len - 1;
        int llo = lo;
        int pivot = arr[hi];
        while(i != j){
            if(arr[llo] < pivot){
                tmpArr[i] = arr[llo];
                i++;
            }else{
                tmpArr[j] = arr[llo];
                j--;
            }
            llo++;
        }
        tmpArr[i] = pivot;
        /*
         [      [lo]       [hi]    ]
          tmpArr[             ] 

        */ 
        for(int k=0; k<len; k++){
            arr[lo + k] = tmpArr[k];
        }
        int newPivotInx = lo + i;
        return newPivotInx;
    }

    public static void quickSortNew(int[] arr, int lo, int hi){
        if(lo < hi){ 
            int pivotInx = partition(arr, lo, hi); 
            quickSortNew(arr, lo, pivotInx - 1);
            quickSortNew(arr, pivotInx + 1, hi);
        }
    }

    public static int quickSortNewKIndex(int[] arr, int lo, int hi, int kInx){
        if(lo < hi){ 
            int pivotInx = partition(arr, lo, hi); 
            if(kInx < pivotInx){
                return quickSortNewKIndex(arr, lo, pivotInx - 1, kInx);
            }else if(kInx == pivotInx){
                return arr[pivotInx];
            }else{
                //         p    kInx
                //  1 2 3 [4] 5  6
                //            x
                return quickSortNewKIndex(arr, pivotInx + 1, hi, kInx - (pivotInx + 1));
            }
        }else{
            return arr[lo];
        }
    }
    public static int ksmallest(int[] arr, int lo, int hi, int kInx){
        if(lo == hi){
            return arr[lo];
        }else if(lo < hi){
            int[] tmpArr = new int[hi - lo + 1];
            // take the last element as pivot
            int pivot = arr[hi];

            // [3, 2]
            // pivot = 2
            // inc = 0
            // dec = 1
            // 
            // [2,3]
            int i = lo;
            // tmpArr does not depend on lo and hi
            int ii = 0;
            int jj = tmpArr.length - 1;
            while(ii != jj){
                if(arr[i] < pivot){
                    tmpArr[ii] = arr[i];
                    ii++;
                }else{
                    tmpArr[jj] = arr[i];
                    jj--;
                }
                i++;
            }

            tmpArr[ii] = pivot;
            int pivotInx = ii;

            /*
                        dec = 3 
               kInx = 6
            
              lo        x       hi 
              [2, 3, 4, 5, 6, 7, 8]
                  kInx > pivotInx
                           6, 7  8
                           lo    hi 
                  x  i
              [2, 3, 4, 5]
              2 - 1 = 1
            */
            
            int nlo = 0;
            int nhi = tmpArr.length - 1;
            if(kInx  < pivotInx ){
                return ksmallest(tmpArr, nlo, pivotInx - 1, kInx);
            }else if(kInx == pivotInx){
                return tmpArr[pivotInx];
            }else{
                return ksmallest(tmpArr, pivotInx + 1, nhi, kInx - (pivotInx + 1));
            }
        }
        throw new IllegalArgumentException("Invalid argument.");
    }


    //  (x, y) = [y][x]
    //   c |
    //     v
    //
    //   r -->
    public static boolean validMove(int c, int r, Pair[] arr){
        for(int i=0; i<c; i++){
            if(arr[i].value == 1){
                int cc = arr[i].c; 
                int rr = arr[i].r; 
                if(Math.abs(c - cc) == Math.abs(r - rr) || r == rr){
                    return false;
                }
            }
        }
        return true;
    }
    public static void queen8(int c, Pair[] arr){
        if(c == 8){
            for(int i=0; i<8; i++){
                pb(arr[i]);
            }
            line();
        }else{
            for(int r=0; r<8; r++){
                if(validMove(c, r, arr)){
                    arr[c] = new Pair(c, r, 1);
                    queen8(c + 1, arr);
                    arr[c].value = 0;
                }
            }
        }
    }

    //  {1, 2}
    public static void coinChange(Map<List<Integer>, Integer> map, List<Integer> ls, List<Integer> sumList, int snum){
        List<Integer> sls = sort(sumList, (x, y) -> x - y);
        Integer key = map.get(sls);
        if(key == null){
            int s = sum(sumList);
            if(s == snum){
                p(sumList);
            }else if (s < snum){
                for(int i=0; i<ls.size(); i++){
                    List<Integer> newList = removeIndex(i, ls);
                    List<Integer> newSumList = append(sumList, ls.get(i));                    
                    coinChange(map, newList, newSumList, snum);
                }
            }
            map.put(sls, s);
        }
    }

    // arr = {1, 2, 3}, n = 0
    //                  n = 1
    public static int[] takeArr(int n, int[] arr){
        int len = arr.length;
        int[] ret = new int[n];
        if(n <= len){
            for(int i=0; i<n; i++){
                ret[i] = arr[i];
            }
        }
        return ret;

    }
    // arr = {1, 2, 3}, n = 0
    //                  n = 1
    public static long[] takeArr(int n, long[] arr){
        int len = arr.length;
        long[] ret = new long[n];
        if(n <= len){
            for(int i=0; i<n; i++){
                ret[i] = arr[i];
            }
        }
        return ret;

    }

    public static float[] takeArr(int n, float[] arr){
        int len = arr.length;
        float[] ret = new float[n];
        if(n <= len){
            for(int i=0; i<n; i++){
                ret[i] = arr[i];
            }
        }
        return ret;

    }

    public static double[] takeArr(int n, double[] arr){
        int len = arr.length;
        double[] ret = new double[n];
        if(n <= len){
            for(int i=0; i<n; i++){
                ret[i] = arr[i];
            }
        }
        return ret;

    }

    public static Integer[] takeArr(int n, Integer[] arr){
        int len = arr.length;
        Integer[] ret = new Integer[n];
        if(n <= len){
            for(int i=0; i<n; i++){
                ret[i] = arr[i];
            }
        }
        return ret;

    }
    public static void test3(){
        beg();
        {
            Map<List<Integer>, Integer> map = new HashMap<>();
            Integer sum = 10;
            List<Integer> sumList = new ArrayList<>();
            List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
            coinChange(map, list, sumList, sum);
        }
        end();
    }
    public static void test2(){
        beg();
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = takeArr(0, arr);
            int[] exp = {};
            t(ret, exp);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = takeArr(1, arr);
            int[] exp = {1};
            t(ret, exp);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = takeArr(2, arr);
            int[] exp = {1, 2};
            t(ret, exp);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = takeArr(4, arr);
            int[] exp = {1, 2, 3, 4};
            t(ret, exp);
        }
        end();
    }
    public static void fun(int[] arr){
        int len = arr.length;
        // [1, 2, 3] 
        // 1, 2, 3  => 1, 12, 123 
        //          => 2, 23, 3
        // 1 2, 2 3
        // 1 2 3
        int k = 1;
        for(int i=0; i<=len; i++){
            for(int j=1; j<=len; j++){
                int s = i;
                int e = i+j;
                // 1, 12, 123
                // 2, 23
                // 3
            }
        }
    }
    public static void test0(){
        Aron.beg();
        { 
            int[] arr = {2, 3, 4};
            int[] marr = multiList(arr);
            p(marr);
        }
        { 
            //           7  6  5
            int[] arr = {2, 3, 4};
            int[] marr = sumList(arr);
            p(marr);
        }

        Aron.end();
    }
    
    
    /**
    <pre>
    {@literal
        
        36 12   4  1
        2  3    4  5
        1  3    12 60

        5 5 5 x 
        4 4 x 4 
        3 x 3 3 
        x 2 2 2

        a = 1  3  12 60
        b = 36 12 4   1
              
    }
    {@code
    }
    </pre>
    */ 
    public static int[] multiList(int[] arr){
        int len = arr.length;
        int[] a = new int[len];
        int[] b = new int[len];
        a[0] = 1;
        b[len-1] = 1;

        // [2 3 4][5]
        int inx = len - 1;
        for(int i=1; i<len; i++){
            a[i] = a[i-1]*arr[i - 1];
            b[inx - i] = b[inx - (i - 1)]*arr[inx - (i - 1)];
        }

        for(int i=0; i<len; i++){
            a[i] = a[i]*b[i];
        }
        return a;
    }

    //       2  3  4
    //  0 + [2, 3]
    //          [3 4] + 0 
    public static int[] sumList(int[] arr){
        int len = arr.length;
        int[] acc1 = new int[len];
        int[] acc2 = new int[len];

        acc1[0] = 0;
        acc2[len - 1] = 0;
        
        int inx = len - 1;
        for(int i=1; i<len; i++){
            acc1[i] = acc1[i-1] + arr[i-1];
            acc2[inx - i] = acc2[inx - (i - 1)] + arr[inx - (i-1)];
        }
        for(int i=0; i<len; i++){
            acc1[i] = acc1[i] + acc2[i];
        }
        return acc1;
    }

    public static void test1(){
        Aron.beg();
        Aron.end();
    }
} 

