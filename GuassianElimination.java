import java.util.*;
import java.io.*;
import classfile.*;

public class GuassianElimination{
    public static void main(String[] args) {
//        test0();
//        test2();
//        test3();
//        test5();
//        test6();
//        upperTriangle_test();
//        GEReduce_test();
//        lowerTriangle_test();
//        inverse_test2();
//          solveEquation_test();
          //upperTriangle_test2();
          //upperTriangle_test3();
//          upperTriangle_test4();
//          upperTriangle_test5();
//          upperTriangle_test6();
//          upperTriangle_test7();
//          upperTriangle_test8();
//          upperTriangle_test9();

//        inverse_test();
//        inverse_test1();
//        inverse_test2();
        inverse_test3();
    }
    public static void test0(){
        Aron.beg();
        double[][] arr = new double[][] {
        {1, 2, 3, 1},
        {4, 5, 6, 2},
        {7, 8, 10, 5},
        };

        double[] vec = new double[] {0, 0, 0};
        double[] A = new double[] {1, 2, 5};
        Aron.printArray2D(arr);

        int len = arr[0].length;
        int h = arr.length;
        for(int i=0; i<h-1; i++){
            for(int n=1; n+i<h; n++){
            double m00 = arr[i][i];
            double m10 = arr[i+n][i];
            for(int j=i; j<len; j++){
                arr[i][j] = arr[i][j]*m10;
                arr[n+i][j] = arr[n+i][j]*m00;
                arr[n+i][j] = arr[i][j] - arr[n+i][j];
            }
            }
        }
        
        Print.p("h=" + h);
        for(int i=0; i<h; i++){
            double sum = 0;
            for(int j=0; j<i+1; j++){
                // div the a[k][k], (assume the matrix is invertable. Thereforce, {a[k][k] != 0, k=1...n})
                if(h-1-i == h-1-j){
                    vec[h-1-i] = (arr[h-1-i][h] - sum)/arr[h-1-i][h-1-j];
                    Print.p("arr[" + (h-1-i) + "][" + (h-i) + "]=" + arr[h-1-i][h]);
                    Print.p("vec[" + (h-1-i) + "]" + vec[h-1-i]);
                }else{
                    sum += arr[h-1-i][h-1-j]*vec[h-1-j];
                    Print.p("->vec[" + (h-1-j) + "]" + vec[h-1-j]);
                    Print.p("sum=" + sum);
                }
            }
            Print.p();
        }

        Aron.printArray2D(arr);
        
        Aron.end();
    }
    public static double[] backwardSubstitution(double[][] arr){
        int h = arr.length;
        int w = arr[0].length;
        double[] vec = new double[h];
        for(int i=0; i<h; i++){
            double sum = 0;
            for(int j=0; j<i+1; j++){
                // div the a[k][k], (assume the matrix is invertable. Thereforce, {a[k][k] != 0, k=1..n})
                if(h-1-i == h-1-j){
                    vec[h-1-i] = (arr[h-1-i][h] - sum)/arr[h-1-i][h-1-j];
                }else{
                    sum += arr[h-1-i][h-1-j]*vec[h-1-j];
                }
            }
            Print.p();
        }
        return vec;
    }

    // convert long[][] to double[][]
    public static double[][] convertlongTodouble(long[][] arr){
        int row = arr.length;
        int col = arr[0].length;
        double[][] m = new double[row][col];
        for(int r=0; r < row; r++){
            for(int c=0; c< col; c++){
                m[r][c] = (double)arr[r][c];    
            }
        }
        return m;
    }
//    public static void lowerTriangle(long [][] m){
//        if(m != null){
//            int len = m.length;
//            int wid = m[0].length;
//            for(int k=len-1; k>=0; k--){
//                for(int i=k-1; i >= 0; i--){
//                    long m00 = m[k][k];
//                    long m10 = m[i][k];
//                    if(m10 != 0 && m00 != 0){
//                        for(int j=k; j >=0; j--){
//                            m[k][j] = m[k][j]*m10;
//                            m[i][j] = m[i][j]*m00;
//                            m[i][j] = m[k][j] - m[i][j];
//                        }
//
//                        Print.p(m);
//                        long gcdk = Aron.gcdList(m[k]);
//                        long gcdi = Aron.gcdList(m[i]);
//                        Print.p("gcdk=" + gcdk);
//                        Print.p("gcdi=" + gcdi);
//                        if(gcdk != 0 && gcdi != 0){
//                            for(int j=k; j>=0; j--){
//                                m[k][j] = m[k][j]/gcdk;
//                                m[i][j] = m[i][j]/gcdi;
//                            }
//                        }else{
//                            if(gcdk != 0){
//                                for(int j=k; j>=0; j--)
//                                    m[k][j] = m[k][j]/gcdk;
//                            }
//                            if(gcdi != 0){
//                                for(int j=k; j>=0; j--)
//                                    m[i][j] = m[i][j]/gcdi;
//                            }
//                        }
//                        Print.p(m);
//                    }
//                }
//            }
//        }
//    }
    
    // if mat is invertable, return the inverse
    // return null otherwise
    public static double[][] inverse(long [][] mat){
       double[][] mm = null;
       if (Aron.isValidMatrix(mat)){
           int w = mat.length;
           long[][] id = Aron.geneIdentitylong(w);
           long[][] m = Aron.concatMatrix(mat, id);
           upperTriangle(m);
           lowerTriangle(m);
           int nrow = m.length;
           int ncol = m[0].length;
           mm = new double[nrow][ncol/2];
           for(int i=0; i<nrow; i++){
               if(m[i][i] != 0){
                   for(int j=0; j<ncol/2; j++)
                       mm[i][j] = (double)m[i][ncol/2 + j]/m[i][i];
               }else{
                   return null;
               }
           }
       }
       return mm;
       
    }

    /**
    *  Solve system equaton
    *  1x + 2y = 1 
    *  3x + 5y = 2 
    *  Form argumented matrix =>
    *  [1, 2, 1]
    *  [3, 5, 2]
    *  Guassian Elimination  =>
    *  [x, x, x]
    *  [0, x, x]
    *  Backward substitution =>
    *  x= c1
    *  y= c2
    */
    public static double[] solveEquation(long[][] m){
        upperTriangle(m);
        double[][] arr = convertlongTodouble(m);
        double[] vec = backwardSubstitution(arr);
        return vec;
    }
    public static void lowerTriangle(long [][] m){
        if(m != null){
            int len = m.length;
            int wid = m[0].length;
            for(int k=len-1; k>=0; k--){
                for(int i=k-1; i >= 0; i--){
                    long m00 = m[k][k];
                    long m10 = m[i][k];
                    if(m10 != 0 && m00 != 0){
                        for(int j=wid-1; j >=0; j--){
                            m[k][j] = m[k][j]*m10;
                            m[i][j] = m[i][j]*m00;
                            m[i][j] = m[k][j] - m[i][j];
                        }

                        //Print.p(m);
                        long gcdk = Aron.gcdList(m[k]);
                        long gcdi = Aron.gcdList(m[i]);
                        if(gcdk != 0 && gcdi != 0){
                            for(int j=wid-1; j>=0; j--){
                                m[k][j] = m[k][j]/gcdk;
                                m[i][j] = m[i][j]/gcdi;
                            }
                        }else{
                            if(gcdk != 0){
                                for(int j=wid-1; j>=0; j--)
                                    m[k][j] = m[k][j]/gcdk;
                            }
                            if(gcdi != 0){
                                for(int j=wid-1; j>=0; j--)
                                    m[i][j] = m[i][j]/gcdi;
                            }
                        }
                        //Print.p(m);
                    }
                }
            }
        }
    }
    public static void GEReduce(long [][] m){
        if(m != null){
            int len = m.length;
            int w = m[0].length;
            for(int k=0; k<len-1; k++){
                for(int i=k+1; i < len; i++){
                    long m00 = m[k][k];
                    long m10 = m[i][k];
                    if(m10 != 0 && m00 != 0){
                        for(int j=k; j < w; j++){
                            m[k][j] = m[k][j]*m10;
                            m[i][j] = m[i][j]*m00;
                            m[i][j] = m[k][j] - m[i][j];
                        }

                        long gcdk = Aron.gcdList(m[k]);
                        long gcdi = Aron.gcdList(m[i]);
                        if(gcdk != 0 && gcdi != 0){
                            for(int j=k; j<w; j++){
                                m[k][j] = m[k][j]/gcdk;
                                m[i][j] = m[i][j]/gcdi;
                            }
                        }else{
                            if(gcdk != 0){
                                for(int j=k; j<w; j++)
                                    m[k][j] = m[k][j]/gcdk;
                            }
                            if(gcdi != 0){
                                for(int j=k; j<w; j++)
                                    m[i][j] = m[i][j]/gcdi;
                            }
                        }
                    }
                }
            }
        }
    }
    public static void upperTriangle(long [][] m){
        if(Aron.isValidMatrix(m)){
            int len = m.length;
            int w = m[0].length;
            for(int k=0; k<len-1; k++){
                for(int i=k+1; i < len; i++){
                    long m00 = m[k][k];
                    long m10 = m[i][k];
                    if(m10 != 0 && m00 != 0){
                        for(int j=k; j < w; j++){
                            m[k][j] = m[k][j]*m10;
                            m[i][j] = m[i][j]*m00;
                            m[i][j] = m[k][j] - m[i][j];
                        }

                        long gcdk = Aron.gcdList(m[k]);
                        long gcdi = Aron.gcdList(m[i]);
                        if(gcdk != 0 && gcdi != 0){
                            for(int j=k; j<w; j++){
                                m[k][j] = m[k][j]/gcdk;
                                m[i][j] = m[i][j]/gcdi;
                            }
                        }else{
                            if(gcdk != 0){
                                for(int j=k; j<w; j++)
                                    m[k][j] = m[k][j]/gcdk;
                            }
                            if(gcdi != 0){
                                for(int j=k; j<w; j++)
                                    m[i][j] = m[i][j]/gcdi;
                            }
                        }
                    }
                }
            }
        }
    }
    public static void upperTriangle_test8(){
        Aron.beg();
        long[][] m = {
            {2},
        };
        upperTriangle(m);
        Print.p(m);
        long[][] expected = {
                             {2},
                            };
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void upperTriangle_test9(){
        Aron.beg();
        long[][] m = {
            {0},
        };
        upperTriangle(m);
        Print.p(m);
        long[][] expected = {
                             {0},
                            };
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void upperTriangle_test2(){
        Aron.beg();
        long[][] m = {
            { 1,   2,   3, 1},
            { 4,   5,   6, 2},
            { 7,   8,   10, 5},
        };
        upperTriangle(m);
        Print.p(m);
        long[][] expected = {
                            {1, 2, 3, 1},
                            {0, 3, 6, 2},
                            {0, 0, 1, 2},
                            };
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void upperTriangle_test3(){
        Aron.beg();
        long[][] m = {
            { 1,   2,   3},
            { 4,   5,   6},
            { 7,   8,   10},
        };
        upperTriangle(m);
        Print.p(m);
        long[][] expected = {
                            {1, 2, 3},
                            {0, 1, 2},
                            {0, 0, 1},
                            };
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void upperTriangle_test4(){
        Aron.beg();
        long[][] m = Aron.geneIdentitylong(3); 
        upperTriangle(m);
        Print.p(m);
        long[][] expected = Aron.geneIdentitylong(3); 
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void upperTriangle_test5(){
        Aron.beg();
        long[][] m = Aron.geneMatrixZerolong(3); 
        upperTriangle(m);
        Print.p(m);
        long[][] expected = Aron.geneMatrixZerolong(3); 
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void upperTriangle_test6(){
        Aron.beg();
        long[][] m = Aron.geneMatrixZerolong(2); 
        upperTriangle(m);
        Print.p(m);
        long[][] expected = Aron.geneMatrixZerolong(2); 
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void upperTriangle_test7(){
        Aron.beg();
        long[][] m = Aron.geneMatrixZerolong(1); 
        upperTriangle(m);
        Print.p(m);
        long[][] expected = Aron.geneMatrixZerolong(1); 
        Aron.writeFileJavaList("/tmp/xx.x", m);
        Test.t(expected, m);
        Aron.end();
    }
    public static void inverse_test(){
        Aron.beg();
        long [][] m = {
            {1, 2, 3 },
            {4, 5, 6 },
            {7, 8, 10},
        };
        Print.p(m);
        double[][] mm = inverse(m);
        Print.p(mm, 6);
        Aron.end();
    }
    public static void inverse_test1(){
        Aron.beg();
        long [][] m = {
            {1, 2, 3 , 3},
            {4, 5, 6 , 9},
            {7, 8, 10, 21},
            {17, 48, 9, 4},
        };
        String f1 = "/tmp/x1.x";
        String f2 = "/tmp/x2.x";
        Print.p(m);
        Aron.writeFileHaskellList(f1, m);
        double[][] mm = inverse(m);
        Print.p(mm, 6);
        Aron.writeFileHaskellList(f2, mm);
        Aron.end();
    }

    public static void inverse_test2(){
        Aron.beg();
        int nrow = 10;
        int start = 10;
        long [][] m = Aron.geneMatrixlong(10, nrow); 
        Print.p(m);
        double[][] mm = inverse(m);
        Print.p(mm, 5);
        Aron.end();
    }
    public static void inverse_test3(){
        Aron.beg();
        long [][] m = {
            {1, 3, 7},
            {2, 4, 8},
            {3, 5, 10},
        };
        Print.p(m);
        double[][] mm = inverse(m);
        if(mm != null)
            Print.p(mm, 6);
        Aron.end();
    }
    public static void inverse_test3(){
        Aron.beg();
        long [][] m = {
            {1, 2, 3},
            {2, 3, 4},
            {3, 4, 5},
        };
        Print.p(m);
        double[][] nn = inverse(m);
        Test.t(nn == null);
        Print.p(nn);
        Aron.end();
    }
    public static void lowerTriangle_test(){
        Aron.beg();
        long [][] m = {
            {1, 2, 3,  1, 0, 0},
            {4, 5, 6,  0, 1, 0},
            {7, 8, 10, 0, 0, 1},
        };
        Print.p(m);
        lowerTriangle(m);
        Print.p(m);
        Aron.end();
    }
    public static void upperTriangle_test11(){
        Aron.beg();
        long[][] m = {
            { 1,   2,   3, 1},
            { 4,   5,   6, 2},
            { 7,   8,   10, 5},
        };
        upperTriangle(m);
        double[][] arr = convertlongTodouble(m);
        Aron.printArray2D(arr);
        double[] vec = backwardSubstitution(arr);
        Aron.end();
    }

    public static void test2(){
        Aron.beg();
        int start = 7;
        int r = 6;
        long[][] m = Aron.geneMatrixlong(start, r, r);
        Aron.printArray2D(m);
        upperTriangle(m);
        Aron.printArray2D(m);
        Aron.end();
    }
    public static void upperTriangle_test(){
        Aron.beg();
        int max = 10;
        int r = 4; 
        //long [][] m = Aron.geneMatrixRandomIntervalLong(1, max, r);
        long [][] m = {
            {1, 2, 3,  1, 0, 0},
            {4, 5, 6,  0, 1, 0},
            {7, 8, 10, 0, 0, 1},
        };
        Aron.printArray2D(m);
        upperTriangle(m);
        Aron.printArray2D(m);
        Aron.end();
    }
    public static void GEReduce_test(){
        Aron.beg();
        int max = 10;
        int r = 4; 
        //long [][] m = Aron.geneMatrixRandomIntervalLong(1, max, r);
        long [][] m = {
            {1, 2, 3,  1, 0, 0},
            {4, 5, 6,  0, 1, 0},
            {7, 8, 10, 0, 0, 1},
        };
        Aron.printArray2D(m);
        GEReduce(m);
        Aron.printArray2D(m);
        Aron.end();
    }
    public static void test5(){
        Aron.beg();
        int max = 10;
        int r = 17; 
        long [][] m = Aron.geneMatrixRandomIntervalLong(1, max, r);
        Aron.printArray2D(m);
        upperTriangle(m);
        Aron.printArray2D(m);
        Aron.end();
    }
    public static void test6(){
        Aron.beg();
        int max = 10;
        int r = 10; 
        long [][] m = Aron.geneMatrixRandomIntervalLong(1, max, r, r+1);
        upperTriangle(m);
        Aron.printArray2D(m);
        double[][] arr = convertlongTodouble(m);
        Aron.printArray2D(arr);
        double[] vec = backwardSubstitution(arr);
        Aron.printArray2D(arr);

        Aron.end();
    }
    public static void solveEquation_test(){
        Aron.beg();
        long[][] m = {
            { 1,   2,   3, 1},
            { 4,   5,   6, 2},
            { 7,   8,   10, 5},
        };
        Aron.printArray2D(m);
        double[] vec = solveEquation(m);
        Print.p(vec);
        Aron.end();
    }
} 

