import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.Aron;
import classfile.Print;

import java.util.stream.*;

public class Main{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        Aron.beg();
        
        Map<String, List<String>> map = new HashMap<>();

        List<String> list = Aron.readFile("/tmp/fh.x");
        //Aron.printList(list);
        for(String fName : list){
            List<String> preList = Aron.prefix(Aron.fileName(fName));
            for(String key : preList){
                List<String> listValue = map.get(key);
                if(listValue == null) {
                    List<String> tmpList = new ArrayList<>();
                    tmpList.add(fName);
                    map.put(key, tmpList);
                }else{
                    listValue.add(fName);
                    map.put(key, listValue);
                }
            }
            //Print.p(preList);
        }
        Print.p("size=" + map.size());

        while(true) {
            try {
                //Enter data using BufferReader
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(System.in));

                // Reading data using readLine
                String name = reader.readLine();
                List<String> listName = map.get(name);
                for(String str : listName){
                    Print.pl(str);
                }

            } catch (IOException e) {

            }
        }

        
    }

}

