import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.Aron;
import classfile.Print;
import java.util.stream.*;
import java.util.stream.Collectors;

// better version: http://localhost/html/indexTriesDataStructure.html
// another tries data structure again!
// add: count all the longest paths
class TNode{
    boolean isWord;
    String word;
    TNode[] arr = new TNode[26];
    // use Map<String, TNode> map;
    public TNode(){
        isWord = false;
    }
}

public class try_tries{
    public static void main(String[] args) {
//        a | b | c |
//                d | 
//            e f | 
          

        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        TNode r = new TNode();
        String s = "a";
        String s1 = "ab";
        String s2 = "abc";
        String s3 = "abd";
        String s4 = "aef";
        int inx = 0;
        insert(r, s, inx);
        insert(r, s1, inx);
        insert(r, s2, inx);
        insert(r, s3, inx);
        insert(r, s4, inx);
        Print.pb(contains(r, "a", inx) == true);
        Print.pb(contains(r, "ac", inx) == false);
        Print.pb(contains(r, "ab", inx) == true);
        Print.pb(contains(r, "abc", inx) == true);
        int count = countPath(r);
        Print.pb(count);
        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        TNode r = new TNode();
        String s = "a";
        String s1 = "ab";
        String s2 = "abc";
        String s3 = "abd";
        String s4 = "aef";
        int inx = 0;
        insert(r, s, inx);
        insert(r, s1, inx);
        insert(r, s2, inx);
        insert(r, s3, inx);
        insert(r, s4, inx);

        String input = "ab";
        List<String> list = autoComplete(r, input, inx);
        Print.pb(list);

        Aron.end();
    }
    public static void insert(TNode r, String s, int inx){
        if (inx < s.length()){
            int index = s.charAt(inx) - 'a';
            if(r.arr[index] == null)
                r.arr[index] = new TNode();
            insert(r.arr[index], s, inx + 1);
        }else{
            r.word = s;
            r.isWord = true;
        }
    }
    public static boolean contains(TNode r, String s, int inx){
        if(inx < s.length()){
            int index = s.charAt(inx) - 'a';
            if(r.arr[index] != null){
                return contains(r.arr[index], s, inx+1);
            }else{
                return false;
            }
        }else{
            return r.isWord;
        }
    }
    /*
    Count the number of paths of Tries
    Input Tries
    Return number of paths or number of words in Tries
    Count the number of longest paths in Tries
    */
    public static int countPath(TNode r){
        if(r != null){
            int s = 0;
            int i = 0;
            for(i=0; i<r.arr.length; i++){
                if(r.arr[i] != null){
                    s += countPath(r.arr[i]);
                }
            }
            if (s == 0) // all nodes are null, we hit the bottom
                return 1;
            else
                return s;
        }else{
            return 0;
        }
    }
    public static void getList(TNode r, List<String> list){
        if(r != null){
            int c = 0;
            for(int i=0; i<r.arr.length; i++){
                if(r.arr[i] != null){
                    getList(r.arr[i], list);
                    c++;
                }
            }
            if(c == 0){
                list.add(r.word); 
            }
        }
    }
    public static List<String> autoComplete(TNode r, String s, int inx){
        if (inx < s.length()){
            int index = s.charAt(inx) - 'a';
            if(r.arr[index] != null){
                return autoComplete(r.arr[index], s, inx + 1);
            }else{
                return new ArrayList<String>();
            }
        }else{
            List<String> list = new ArrayList<String>();
            getList(r, list);
            return list;
        }
    }
} 

