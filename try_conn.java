import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class Solution{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    
    public static int countConnection(int[][] arr, int w, int width, int h, int height) {
        if(arr != null) {
            if(arr[h][w] == 1) {
                arr[h][w] = 2;
                int right = 0, left = 0, up = 0, down = 0;
                if(w + 1 < width)
                    right =countConnection(arr, w+1, width, h, height);
                if(w - 1 >= 0)
                    left = countConnection(arr, w-1, width, h, height);
                if(h - 1 >= 0)
                    up =   countConnection(arr, w, width, h-1, height);
                if(h + 1 < height)
                    down = countConnection(arr, w, width, h+1, height);

                return right + left + up + down + 1;
            }
        }
        return 0;
    }
} 

