import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_subsequence{
    public static void main(String[] args) {
        test0();
        test1();
    }
    /**
        3:40pm
    */
    public static List<String> sequence(String s){
        List<String> list = new ArrayList<>();
        for(int i=1; i<=s.length(); i++){
            for(int j=0; j<s.length(); j++){
                if(j + i <= s.length()){
                    String sub = s.substring(j, j+i); // "abc" => i=1, a, ab, abc | i=2, ab, bc | i=3, abc
                    list.add(sub);
                }
            }
        }
        return list;
    }


    
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            List<String> list = Arrays.asList("b");
            List<String> ls = concatEach("a", list);
            p(ls);
        }

        fl();
        {
            sequence("abc");
            List<String> lss = sub("abc");
            p(lss);
        }

        fl();
        {
            List<String> lss = subsequenceEmpty("");
            pb(lss);
        }
        fl();
        {
            List<String> ls = insert('k', "abc");
            p(ls);
        }
        fl();
        {
            String s = insert(1, 'k', "abc");
            p(s);

            String s1 = insert(1, 20, "abc");
            p(s1);

            String s2 = insert(1, 3.14, "abc");
            p(s2);

            String s3 = insert(1, "cat", "abc");
            p(s3);
        }
        fl();
        {
            String s = stripPrefix("a", "abc");
            t(s, "bc");
            p(s);
        }
        {
            String s = stripPrefix("", "abc");
            t(s, "abc");
            p(s);
        }
        {
            String s = stripPrefix("abc", "abc");
            t(s, "");
            p(s);
        }
        {
            String s = stripPrefix("ac", "abc");
            t(s, "abc");
            p(s);
        }
        fl();
        {
            Boolean b = containsSubstr("abc", "b");
            t(b, true);
        }
        {
            Boolean b = containsSubstr("abc", "a");
            t(b, true);
        }
        {
            Boolean b = containsSubstr("abc", "ab");
            t(b, true);
        }
        {
            Boolean b = containsSubstr("abc", "abc");
            t(b, true);
        }
        {
            Boolean b = containsSubstr("abc", "ac");
            t(b, false);
        }
        {
            Boolean b = containsSubstr("abc", "");
            t(b, true);
        }
        fl();
        {
            String s = removeSubStr("abc", "a");
            t(s, "bc");
        }
        {
            String s = removeSubStr("abc", "abc");
            t(s, "");
        }
        {
            String s = removeSubStr("abc", "");
            t(s, "abc");
        }
        {
            String s = removeSubStr("abc", "b");
            t(s, "ac");
        }
        {
            String s = removeSubStr("abcde", "bcd");
            t(s, "ae");
        }

        fl();
        {
            String s = removeAllSub("abc", "b");
            t(s, "ac");
        }
        {
            String s = removeAllSub("abcb", "b");
            t(s, "ac");
        }
        {
            String s = removeAllSub("abcbbckbc", "bc");
            t(s, "abk");
        }
        {
            String s = removeAllSub("aaaa", "aa");
            t(s, "aa");
        }


        sw.printTime();
        end();
    }
    /**
        "ab"
        "a" "b"
            "b" ""
                ""
    */
//    public static void subsequence(String a, String s, List<String> ls){
//        if(s.length() == 0){
//            ls.add("");
//        }else{
//            subsequence(take(1, s), drop(1, s), ls);
//        }
//    }
    public static List<String> sub(String s){
        List<String> ls = new ArrayList<>();
        List<String> list = new ArrayList<>();
        int len = s.length();
        if(len > 0){
            ls.add("");
            ls.add(last(s) + "");
            for(int i=len-2; i>=0; i--){
                String a = s.charAt(i) + "";

                // copy
                list = new ArrayList<>();
                for(String b : ls){
                    list.add(b);
                }
                // concatenate
                for(String b : ls){
                    list.add(a + b);
                }
                ls = new ArrayList<>();
                for(String c : list){
                    ls.add(c);
                }
            }
        }
        return ls;
    }

    
    
    public static void test1(){
        beg(); 
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        List<String> list = Arrays.asList("dog", "cat", "fox", "cow");
        List<String> ls = sortListString(list);
        p(ls);

        for(int i=0; i<10; i++){
            p(decimalToBinary(i));
        }

        sw.printTime();
        end();
    }
} 

