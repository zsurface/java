import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class PalindromeSubStr{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            Integer n = palindromicSubStrs("a");
            t(n, 1);
        }
        {
            Integer n = palindromicSubStrs("ab");
            t(n, 2);
        }
        {
            Integer n = palindromicSubStrs("abc");
            t(n, 3);
        }
        {
            Integer n = palindromicSubStrs("aa");
            t(n, 2);
        }
        {
            Integer n = palindromicSubStrs("aac");
            t(n, 3);
        }
        {
            Integer n = palindromicSubStrs("aabb");
            t(n, 4);
        }
        {
            Integer n = palindromicSubStrs("aacbb");
            t(n, 5);
        }
        {
            Integer n = palindromicSubStrs("abaaa");
            t(n, 5);
        }

        sw.printTime();
        end();
    }

    
} 

