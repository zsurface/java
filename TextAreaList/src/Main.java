import classfile.Aron;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.*;

import classfile.Print;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;


// TextArea example, mouse click, mouseClick, MouseClicked
//
public class Main {
//    @Override
//    public void start(Stage primaryStage) throws IOException {
//
//        final int numTexAreas = 10 ;
//        TextField[] textFields = new TextField[numTexAreas];
////        PDDocument document = new PDDocument();
////        document.close();
//
//
//        VBox root = new VBox(5);
//        TextArea textArea = new TextArea();
//        root.getChildren().add(textArea);
//
//        textArea.setOnMouseClicked(e -> {
//            System.out.println("Clicked");
//        });
//
//        Scene scene = new Scene(new ScrollPane(root), 250, 600);
//        primaryStage.setScene(scene);
//        primaryStage.show();
//    }




    public static void main(String[] args) throws IOException {
// Create a new empty document
        PDDocument document = new PDDocument();

// Create a new blank page and add it to the document
        PDPage blankPage = new PDPage();
        document.addPage( blankPage );

// Save the newly created document
        document.save("/Users/cat/myfile/bitbucket/math/bilinear.pdf");


// finally make sure that the document is properly
// closed.
        document.close();
        Print.p(3);
    }
}