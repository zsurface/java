// import classfile.*;

import java.net.Inet4Address;
import java.util.*;
import java.util.function.BiFunction;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.t;
import classfile.Node;

import classfile.Aron;
import classfile.BST;
import classfile.Ut;
import classfile.StopWatch;

class NNode<T extends Comparable>{
    int key;
    NNode left;
    NNode right;
    T data;
    public NNode(T data){
        key = data.hashCode() % 100;
        this.data = data;
    }
    @Override
    public String toString(){
        String s ="key=" + key + " " + " data=" + data.toString();
        return s;
    }
}



class Pair{
    List<Integer> left = new ArrayList<>();
    List<Integer> right = new ArrayList<>();
    public Pair(List<Integer> left, List<Integer> right){
        this.left = left;
        this.right = right;
    }
}



public class Main {
    public static void main(String[] args) {
        // test9();
        // test999();
        // test_allPath();
//        test_bstAllPath();
//        test_longestPath();
//        test_allPathFuel();
        // test66();
        // testNonAscii();
        test_buildTree();
    }

    /**
                9

                9
             6
     */
    public static int minNode(Node root){
        if(root != null){
            if(root.left != null){
                return minNode(root);
            }else{
                return root.data;
            }
        }
        return -1;
    }

    /**
                         14
              7                        18
         4         9            16             20

     4 7 9 14 16 18 20

     1: 14  OK
     2: 7   OK
     3: 8
     */
    public static Integer findMin(Node root){
        if(root != null){
            if(root.left == null)
                return root.data;
            else
                return findMin(root.left);
        }
        return -1;
    }

    // There is still some errors here:
    public static Integer findNode2(Node root, int n, Node parent){
        if(root != null){
            if(n < root.data){
                if(root.left != null && n > root.left.data)
                    return root.left.data;
                findNode2(root.left, n, root);
            }else if(n == root.data){
                return root.data;
            }else{
                if(root.right != null && root.right.data > n)
                    return root.data;
                findNode2(root.right, n, root);
            }
        }
        return -1;
    }
    public static Integer findNode(Node root, int n){
        if(root != null && !root.isVisited){
            findNode(root.left, n);
            if(n >= root.data) {
                root.isVisited = true;
                return root.data;
            }
            findNode(root.right, n);
        }
        return null;
    }

    public static void testNonAscii(){
        beg();
        {
            List<Integer> ls = new ArrayList<>();
        }
        {
            String s = "abc";
            boolean b = isNonASCIIStr(s);
            t(b == false);
        }
        {
            String s = "aø";
            boolean b = isNonASCIIStr(s);
            t(b == true);
        }
        {
            String s = "";
            boolean b = isNonASCIIStr(s);
            t(b == false);
        }

        end();
    }

    /**
     *           v
     *     1  2  3 4 5
     *     1 2
     *
     *                  v
     *                  k = 3
     *     1 1 2 2  3 3 3 3 3 3 4 4 4 5
     *                     >= k + 1
     *                    [            ]
     *
     */
    public static int binSearch(int[] arr, int key, int lo, int hi){
        int leftRet = -1;
        int rightRet = -1;
        if(lo < hi){
            int mid = (lo + hi)/2;
            if(key < arr[mid]){
                leftRet = binSearch(arr, key, lo, mid - 1);
            }else if(key > arr[mid]) {
                rightRet = binSearch(arr, key, mid + 1, hi);
            }else {
                leftRet = binSearch(arr, key, lo, mid - 1);
                rightRet = binSearch(arr, key, mid + 1, hi);
            }
        }else {
            if(arr[lo] == key)
                return lo;
            else
                return -1;
        }
        return -1;
    }
    // 1 1 1 1 3 3 3 3 3 3 4
    // 1 1 1 1 3
    //     x
    //       1 3
    //         x
    public static int bin(int[] arr, int key, int lo, int hi){
        if(lo < hi){
            int mid = (lo + hi)/2;
            if(key - 1 <= arr[mid]){
                return bin(arr, key, mid + 1, hi);
            }else{
                return bin(arr, key, lo, mid - 1);
            }
        }else{
            if(key - 1 <= arr[lo])
                return lo;
            else
                return -1;
        }
    }

    public static void test66(){
        beg();
        {
            BiFunction f = (x, y) -> x == y;
            List<Integer> ls1 = Arrays.asList(1, 2, 3);
            List<Integer> ls2 = Arrays.asList(1, 2, 4);

            Boolean b = compareLists(f, ls1, ls2);
            t(b, false);
        }
        {
            BiFunction f = (x, y) -> x == y;
            List<Integer> ls1 = list(1, 2, 3);
            List<Integer> ls2 = list(1, 2, 3);

            Boolean b = compareLists(f, ls1, ls2);
            t(b, true);
        }
        {
            List<List<Integer>> lss = list(list(1, 2), list(2, 4));
            List<Integer> ls = list(1, 2);
            BiFunction<List<Integer>, List<Integer>, Boolean> f = (x, y) -> x == y;

            List<Integer> myls = filter(x -> x > 1, ls);
            pp(ls);
            Ut.l();
            List<List<Integer>> mylss = removeList(lss, ls);
            pp(mylss);

        }
        end();
    }


    public static List<Integer> copy(List<Integer> ls){
        List<Integer> ret = new ArrayList<>();
        for(Integer e : ls){
            ret.add(e);
        }
        return ret;
    }
    /**
     *              9
     *                  10
     *        6
     *
     */
    public static void bstAllPath(List<List<Integer>> lss,  List<Integer> ls,  classfile.Node root){
        if(root != null){
            ls.add(root.data);
            if(root.left == null && root.right == null){
                lss.add(copy(ls)); // 9 6
            }else {
                bstAllPath(lss, ls, root.left);
                if(ls.size() > 0)
                    ls.remove(ls.size() - 1);

                bstAllPath(lss, ls, root.right);
                if(ls.size() > 0)
                    ls.remove(ls.size() - 1);
            }
        }
    }

    public static void test_longestPath(){
        beg();
        {
            classfile.Node root = geneBinNoImage();
            List<List<Integer>> lss = new ArrayList<>();
            List<Integer> ls = new ArrayList<>();
            longestPath(lss, ls, root);
            Ut.l();
            pb(lss);
        }
        end();
    }
    public static void longestPath(List<List<Integer>> lss, List<Integer> ls, classfile.Node root){
        if(root != null){
            ls.add(root.data);
            if(root.left == null && root.right == null){
                if(lss.size() > 0) {
                    if (lss.get(0).size() < ls.size()) {
                        lss.clear();
                        lss.add(copy(ls));
                    }
                }else{
                    if(ls.size() > 0);
                        lss.add(copy(ls));
                }
            }else{
                longestPath(lss, ls, root.left);
                if(ls.size() > 0)
                    ls.remove(ls.size() - 1);

                longestPath(lss, ls, root.right);
                if(ls.size() > 0)
                    ls.remove(ls.size() - 1);
            }
        }
    }


    public static void test_allPathFuel() {
        beg();
        {
            classfile.Node root = geneBinNoImage();
            List<List<Integer>> lss = new ArrayList<>();
            List<Integer> ls = new ArrayList<>();
            int fuel = 9;
            classfile.Node parent = null;

            allPathFuel(lss, ls, root, parent, fuel);

            Ut.l();
            pp(lss);
            pp("size=" + lss.size());
        }
        end();
    }

    /**
     *   fuel = 6
     *
     *               9
     *           6
     *       4             12
     *                 11
     *
     *
     */
    public static List<Integer> add(List<Integer> ls, int n){
        List<Integer> ret = new ArrayList<>();
        for(Integer e : ls){
            ret.add(e);
        }
        ret.add(n);
        return ret;
    }
    public static void allPathFuel(List<List<Integer>> lss, List<Integer> ls, classfile.Node curr, classfile.Node parent, int fuel){
        if(fuel == 0){
            lss.add(copy(ls));
        }else if(fuel > 0) {
            if (curr != null) {
                int diff = 0;
                if (parent != null) {
                    diff = Math.abs(curr.data - parent.data);
                }
                allPathFuel(lss, add(ls, curr.data), curr.left, curr, fuel - diff);
                allPathFuel(lss, add(ls, curr.data), curr.right, curr, fuel - diff);
            }
        }
    }

    public static void test_bstAllPath(){
        beg();
        {
            classfile.Node root = geneBinNoImage();
            List<List<Integer>> lss = new ArrayList<>();
            List<Integer> ls = new ArrayList<>();
            bstAllPath(lss, ls, root);

            Ut.l();
            pb(lss);
            Ut.l();
            pb(ls);
        }
        end();
    }

    /**
     * Article Ltd. interview, similar to facebook question, similar to coin change
     *
     */
    public static void allPath(int[] a, int[] arr, int fuel, int k){
        if(fuel == 0){
            pp("k = " + k + "\n");
            a[0] += 1;
        }else if(fuel > 0) {
            for (int i = k; i < arr.length; i++) {
                int diff = Math.abs(arr[k - 1] - arr[i]);  // [k - 1] < [i]
                pp("[" + (k-1) + "] -> " + i +  " fuel = " + fuel + "  \n");
                // append(ls, arr[i]);
                allPath(a, arr, fuel - diff, i + 1);
                // ls.removeLast();
            }
        }else{
            pp("too much\n");
        }
    }
    public static void test_allPath(){
        beg();
        {
            int a[] = {0};
            int[] arr = {2, 4};
            int fuel = 2;
            int k = 1;
            allPath(a, arr, fuel, k);
            pp("a=" + a[0]);
        }
        {
            Ut.l();
            int a[] = {0};
            int[] arr = {2, 4, 6};
            int fuel = 6;
            int k = 1;
            allPath(a, arr, fuel, k);
            pp("a=" + a[0]);
        }
        {
            Ut.l();
            int a[] = {0};
            int[] arr = {2, 4, 6, 8};
            int fuel = 6;
            int k = 1;
            allPath(a, arr, fuel, k);
            pp("a=" + a[0]);
        }
        {
//            Ut.l();
//            int a[] = {0};
//            int[] arr = {2, 4, 6, 8, 9, 2, 3};
//            int fuel = 8;
//            int k = 1;
//            allPath(a, arr, fuel, k);
//            pp("a=" + a[0]);
        }
        end();
    }
    public static void insert(NNode root, NNode node){
        if(root == null){
            root = node;
        }else if(node.key <= root.key){
            if(root.left == null)
                root.left = node;
            else {
                insert(root.left, node);
            }
        }else{
            if(root.right == null){
                root.right = node;
            }else{
                insert(root.right, node);
            }
        }
    }

    public static void inorder(NNode root){
        if(root != null){
            inorder(root.left);
            pb(root.toString());
            inorder(root.right);
        }
    }

    public static void test999() {
        beg();
        {
            classfile.Node n = new classfile.Node(2);
            pb(n.data);
            BST bst = new BST();
            bst.insert(9);
            bst.insert(4);
            bst.insert(20);
            bst.insert(8);
            bst.insert(12);
            bst.inorder(bst.root);
            bst.print();

            pp("hc=" + bst.hashCode());
        }
        {
            classfile.Node n1 = new classfile.Node(3);
            classfile.Node n2 = new classfile.Node(3);
            int h1 = n1.hashCode();
            int h2 = n2.hashCode();
            t(h1, h2);
            pb("h1=" + h1);
            pb("h2=" + h2);
        }
        {
            classfile.Node n1 = new classfile.Node(1);
            classfile.Node n2 = new classfile.Node(2);
            classfile.Node n3 = new classfile.Node(3);
            List<classfile.Node> ls = list(n1, n2, n3);
            nl();
            pb(ls);
        }
        {
            Ut.l();
            classfile.Node n1 = new classfile.Node(1);
            classfile.Node n2 = new classfile.Node(2);
            List<classfile.Node> ls = list(n1, n2);
            String s = toStr(ls);
            t(s, "1 2");
            pp("s=" + s);
        }
        {
            Ut.l();
            List<classfile.Node> ls = list();
            String s = toStr(ls);
            t(s, "");
            pp("s=" + s);
        }
        {

            /**
             *                         57=> 9
             *                                     71 => 14
             *         54=>6
             *                   55=>7
             *                              69=>12
             *
             *
             *
             */
            Ut.l();
            NNode root = new NNode("9");
            NNode n1 = new NNode("14");
            NNode n2 = new NNode("6");
            NNode n3 = new NNode("7");
            NNode n4 = new NNode("12");
            insert(root, n1);
            insert(root, n2);
            insert(root, n3);
            insert(root, n4);

            inorder(root);

        }
        end();
    }
    public static void test99() {
        beg();
        {
            Set<Integer> set = set(1, 2, 3, 4, 3);
            Map<Integer, Integer> map = map(list(1, 2), list(2, 3));

            pp(map);

            Iterator<Integer> ite = set.iterator();
            while(ite.hasNext()){
                int n = ite.next();
                if(n == 3)
                    ite.remove();
            }
            Iterator<Integer> ite1 = set.iterator();
            while(ite1.hasNext()){
                int n = ite1.next();
                pb(n);
            }

            List<Integer> mylist = setToList(set);
            for(Integer n : mylist){

            }
            set = listToSet(mylist);

        }
        end();
    }
    public static void test9(){
        beg();
        {
            int fuel = 2;
            List<Integer> cities = list(2, 4);
            int n = countRoutes(fuel, cities);
            t(n, 1);
        }
        {
            // 2 4
            // 2 6
            // 2 4 6 2 = 2 + 2 + 4 = 8
            // 2 6 2  = 4 + 4 = 8
            int fuel = 6;
            List<Integer> cities = list(2, 4, 6, 2, 4);
            int n = countRoutes(fuel, cities);
            t(n, 2);
        }
        end();
    }

    public static int countRoutes(int fuel, List<Integer> cities) {
        int[] arr = {0};
        if(cities.size() > 1) {
            int first = cities.get(0);
            cities.remove(0);
            int k = 0;
            allPath(first, arr, fuel, cities, k);
        }
        return arr[0];
    }


    /**
                0 1 2 3

          1 2 3    2 3    3
       2 3    3       3
        3

        0 1,  0 1 2, 0 1 2 3
        0 2 , 0 2 3
        0 3

        1 2
        1 2 3
        2 3
        1 3




     */
    public static void allPath(int first, int[] arr, int fuel, List<Integer> list, int k){
        if (fuel == 0) {
            arr[0] += 1;
            fl();
        } else if (fuel > 0) {
            for (int i = k; i < list.size(); i++) {
                int diff = Math.abs(list.get(i) - first);
                pb(list.get(i));
                allPath(list.get(k), arr, fuel - diff, list, k + 1);
                fl();
            }
        }

    }



    public  static void test7() {
        beg();
        {
            List<Integer> ls = listRand();
            Map<Integer, Integer> map = mapFrequencyCount(ls);

            pp(map);
            Ut.l();
            pp(ls);
        }
        end();
    }


    public  static List<Integer> listRand(){
        List<Integer> ls = new ArrayList<>();
        int x0 = 133;
        ls.add(x0);
        int a = 33;
        int seed = 331;
        int m = 20;

        for(int i=0; i<100; i++) {
            int r = myRandom(a, last(ls), seed, m);
            ls.add(r);
        }
        return ls;
    }
    public static int myRandom(int a, int x0, int increment, int m){
       int rand =  (a * x0 + increment) % m;
       return rand;
    }


    public static void test6(){
        beg();
        {
            Map<Integer, Integer> map = new HashMap<>();
            for(int i=0; i<100000; i++){
                Integer n = longToInteger(Math.round(Math.random()*10));
                Integer v = map.get(n);
                if(v == null){
                    map.put(n, 1);
                }else{
                    v += 1;
                    map.put(n, v);
                }
            }
            pp(map);
        }
        end();
    }
    public static void test5(){
        beg();
        {
            StopWatch sw = new StopWatch();

            int n = 10000;
            int[] arr1 =  geneRandomArrIntFromTo(0, 9, n);
            int[] arr2 =  geneRandomArrIntFromTo(0, 9, n);
            sw.start();

            int[] arr3 = multiLongInt(arr1, arr2);
            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();

            int n = 10000;
            Integer[] arr1 = geneRandomIntegerFromTo(0, 9, n);
            Integer[] arr2 = geneRandomIntegerFromTo(0, 9, n);
            sw.start();

            Integer[] arr3 = multiLongIntegerType(arr1, arr2);
            sw.printTime();
        }
        end();
    }

    public static List<Integer> take(int n, List<Integer> ls){
        List<Integer> ret = new ArrayList<>();
        int k = 0;
        for(Integer e : ls){
            if(k < n)
                ret.add(e);
            k++;
        }
        return ret;
    }
    public static List<Integer> drop(int n, List<Integer> ls){
        List<Integer> ret = new ArrayList<>();
        int k = 0;
        for(Integer e : ls){
            if(k >= n)
                ret.add(e);
            k++;
        }
        return ret;
    }
    public static Node buildTree(List<Integer> preList, List<Integer> inList){
        if(preList.size() > 0){
            Integer n = preList.get(0);
            Pair in = splitList(inList, n);
            int len = in.left.size();
            List<Integer> leftPre = take(len, drop(1, preList));   // [0, 1, 2) [2, 3, 4) // take len preList
            List<Integer> rightPre = drop(len, drop(1, preList));  // drop len preList
            Node left = buildTree(leftPre, in.left);
            Node right = buildTree(rightPre, in.right);
            Node node = new Node(n);
            node.left = left;
            node.right = right;
            return node;
        }
        return null;
    }
    public static Pair splitList(List<Integer> list, int num){
        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();
        for(Integer n : list){
            if(n < num)
                left.add(n);
            else if(n > num)
                right.add(n);
        }
        return new Pair(left, right);
    }
    public static void test_buildTree(){
        beg();
        {
            /*
                      4
                  3      9

             */
            List<Integer> preorder = new ArrayList<>();
            preorder.add(4);
            preorder.add(3);
            preorder.add(9);

            List<Integer> inorder = new ArrayList<>();
            inorder.add(3);
            inorder.add(4);
            inorder.add(9);
            Node root = buildTree(preorder, inorder);
            Aron.inorder(root);
        }
        fl();
        {
            /*
            4
            2        9
            1    3    7    10

            preorder
            4 2 1 3 9 7 10
            4
            4 [2 1 3] [9 7 10]

            inorder
            1 2 3 4 7 9 10
            1 2 3   7 9 10

            left subtree
            [2 1 3]  [1 2 3]
            right subtree
            [9 7 10] [7 9 10]
           */
            List<Integer> preorder = list(4, 2, 1, 3, 9, 7, 10);
            List<Integer> inorder = list(1, 2, 3, 4, 7, 9,10);
            Node root = buildTree(preorder, inorder);
            fl("inorder");

            Aron.inorder(root);
            fl( "preorder");
            Aron.preorder(root);
        }
        fl();
        {
            List<Integer> preorder = list(4);
            List<Integer> inorder = list(4);
            Node root = buildTree(preorder, inorder);
            fl("inorder");
            Aron.inorder(root);
            fl( "preorder");
            Aron.preorder(root);
        }
        end();
    }


//    public static Integer[] multiLongIntegerType(Integer[] col, Integer[] row){
//
//        Integer[][] mtable = {
//            {0, 0,  0,  0,  0,  0,  0,  0,  0,  0},
//            {0, 1,  2,  3,  4,  5,  6,  7,  8,  9},
//            {0, 2,  4,  6,  8, 10, 12, 14, 16, 18},
//            {0, 3,  6,  9, 12, 15, 18, 21, 24, 27},
//            {0, 4,  8, 12, 16, 20, 24, 28, 32, 36},
//            {0, 5, 10, 15, 20, 25, 30, 35, 40, 45},
//            {0, 6, 12, 18, 24, 30, 36, 42, 48, 54},
//            {0, 7, 14, 21, 28, 35, 42, 49, 56, 63},
//            {0, 8, 16, 24, 32, 40, 48, 56, 64, 72},
//            {0, 9, 18, 27, 36, 45, 54, 63, 72, 81}
//        };
//        Integer[][] mat = null;
//        Integer[] ret = null;
//        if(row != null && col != null){
//            Integer lr = row.length;
//            Integer lc = col.length;
//            Integer len = lr + lc;
//            mat = newInteger2(lc, len);
//            ret = newInteger(len);
//            Integer k = len - 1;
//            for(Integer c=0; c<lc; c++){
//                Integer carry = 0;
//                Integer cr = lc - 1 - c;
//                for(Integer r = lr - 1; r >= 0; r--){
//
//                    // Integer n = col[cr]*row[r];
//                    Integer n = mtable[ col[cr] ][ row[r] ];
//                    Integer rr = lr - 1 -r;
//                    mat[c][k - rr - c] = (carry + n) % 10;
//                    carry = (carry + n) / 10;
//                }
//                mat[c][k - lr - c] = carry;
//            }
//
//            Integer c = 0;
//            for(Integer i = lr + lc - 1; i >= 0; i--){
//                Integer s = 0;
//                for(Integer j=0; j < lc; j++){
//                    s += mat[j][i];
//                }
//                ret[i] = (s + c) % 10;
//                c = (s + c) / 10;
//            }
//        }
//
//        if(ret[0] == 0){
//            return drop(1, ret);
//        }else {
//            return ret;
//        }
//    }

}
