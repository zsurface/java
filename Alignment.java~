import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;




 // x  = 3
 // xy = 4



public class Alignment{
  public static void main(String[] args) {
    /*
    String fname = "/tmp/x.x";
    if(len(args) > 0){
      List<String> ls = readFileNoTrim(fname);
      var lss         = alignment2(args[0], ls);
      for(int i = 0; i < len(lss); i++){
        if(i < len(lss) - 1){
          System.out.println(lss.get(i));
        }else if(i == len(lss) - 1){
          System.out.print(lss.get(i));
        }
      }
    }else{
      List<String> ls  = readFileNoTrim(fname);
      List<String> ret = alignment1(ls);       
      for(int i = 0; i < len(ret); i++){
        if(i < len(ret) - 1){
          System.out.println(ret.get(i));
        }else if(i == len(ret) - 1){
          System.out.print(ret.get(i));
        }
      }
    }
    */

    // test_alignment2();
    test_commentCode();
  }
  /**
        a  b  c   e 
         e f   g h
          i j k l
           ⇓
        a b c e
        e f g h
        i j k l
   */
  public static List<String> alignment1(List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      var premx = len(takeWhile(x -> x == ' ', ls.get(0)));
      var lss   = alignment(ls, ' ');
      var flss  = fillColWith(0, x -> repeatToStr(premx, ' ') + x, lss);
      ret       = map(r -> foldStrDrop(r), flss);
    }
    return ret;
  }
  
  /**
         xⁿ + yⁿ = zⁿ + wⁿ
           x² + y² = z²
             ⇓
         xⁿ + yⁿ = zⁿ + wⁿ
         x² + y² = z²
   */
  public static List<String> alignment2(String s, List<String> ls){
    List<String> ret = new ArrayList<String>();
    var table     = map(x -> splitNStr(x), ls);                                                  
    var tabTriple = map(r -> splitIf(sls -> sls.trim().equals(s), r), table);                    
    var lstab     = map(t -> list(concat_new(t.x), concat_new(t.y), concat_new(t.z)), tabTriple);
    var premx     = len(takeWhile(x -> x == ' ', lstab.get(0).get(0)));                          
    var nlstab    = map2(x -> x.trim(), lstab);                                                  
    var tab2      = fillListStr2d(nlstab);

    if(len(tab2) > 0 && len(tab2.get(0)) > 0){
      var tab3 = fillColWith(0, x -> repeatToStr(premx, ' ') + x, tab2);
      for(int i = 0; i < len(tab3.get(0)); i++){
        int m   = max(map(x -> len(x), getCol(i, tab3)));        
        var col = map(x -> rightPad(x, m, ' '), getCol(i, tab3));
        tab3    = replaceIndexCol(i, col, tab3);                 
      }
      ret = map(x -> trimEnd(concat_new(" ", x)), tab3);
    }
    return ret;
  }
  
  public static List<String> commentCode(String lang, List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      for(int i = 0; i < len(ls); i++){
        String line = ls.get(i);
        Tuple t = splitWhileStr(x -> x == ' ', line);
        if(lang.equals("haskell"))
          ret.add(t.x + "-- " + t.y);
        else if(lang.equals("java"))
          ret.add(t.x + "// " + t.y);
        else if(lang.equals("elisp"))
          ret.add(t.x + ";; " + t.y);
      }
    }
    return ret;
  }
  public static void test_commentCode(){
    beg();
    {
      {
        List<String> rfls = ll("  abc ", " ab", " line1");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("  -- abc ", " -- ab", " -- line1");
        t(ls, expls);
        pb(ls);
        
      }
    }
    end();
  }
  public static void test_alignment2(){
    beg();
    {
      {
        List<String> ls = ll(" x = y;");
        var lss         = alignment2("=", ls);
        var expls       = ll(" x = y;");
        t(lss, expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             " x = y;");
        var lss = alignment2("=", ls);
        var expls = ll(" x = y;",
                       " x = y;");
        t(lss, expls);
        fl("lss");
        pp(lss);
        fl("expls");
        pp(expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             " x  = y;");
        var lss   = alignment2("=", ls);
        var expls = ll(" x = y;",
                       " x = y;");
        t(lss, expls);
        fl("lss");
        pp(lss);
        fl("expls");
        pp(expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             "  x = y;");
        var lss   = alignment2("=", ls);
        var expls = ll(" x = y;",
                       " x = y;");
        t(lss, expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             "  x = yz;");
        var lss = alignment2("=", ls);
        var expls = ll(" x = y;",
                       " x = yz;");
        t(lss, expls);
      }
      {
        List<String> ls = ll("  x = y;",
                             " x = yz;");
        var lss = alignment2("=", ls);
        var expls = ll("  x = y;",
                       "  x = yz;");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x = y + u + v ",
                             " x = yz + u + v  ");
        var lss = alignment2("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x  = y + u + v ",
                             " x = yz + u + v  ");
        var lss = alignment2("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x  = y + u + v ",
                             " x  = yz + u + v  ");
        var lss = alignment2("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x= y + u + v ",
                             " x  = yz + u + v  ");
        var lss = alignment2("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
    }
    end();
  }
}

