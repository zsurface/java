import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_noncontinuous{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        int[] arr = {1, 2, 3, 4, 5, 6};
        p(arr);
        fl();
        nonContinuous(arr);

        sw.printTime();
        end();
    }
    public static void nonContinuous(int[] arr){
        if(arr != null){
            int len = arr.length;
            for(int k=2; k<len; k++){
            for(int j=0; j<len; j++){
                for(int i=0; i<len; i++){
                    int inx = j + k*i;
                    if(inx < len){
                        System.out.print(" " + arr[inx]);
                    }
                }
                p("");
            }
            }
        }else{
            throw new IllegalArgumentException("Argument can not be null.");
        }
        
    }
    public static void seqR(String s, String c, int i, int added) {
        if (i == s.length()) {
            if (c.trim().length() > added)
                System.out.println(c);
        } else {
            seqR(s, c + s.charAt(i), i + 1, added + 1);
            seqR(s, c + ' ', i + 1, added);
        }
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        seqR("1234", "", 0, 0);



        sw.printTime();
        end();
    }
} 

