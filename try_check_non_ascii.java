import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharacterCodingException;

public class try_check_non_ascii{
    public static void main (String args[]) throws Exception {
        test0();
    }

    public static void test0(){
        beg();

        String test = "Réal";
        System.out.println(test + " isPureAscii() : " + isPureAscii(test));
        test = "Real";
        System.out.println(test + " isPureAscii() : " + isPureAscii(test));

        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
  public static boolean isPureAscii(String v) {
    byte bytearray []  = v.getBytes();
    CharsetDecoder d = Charset.forName("US-ASCII").newDecoder();
    try {
      CharBuffer r = d.decode(ByteBuffer.wrap(bytearray));
      r.toString();
    }
    catch(CharacterCodingException e) {
      return false;
    }
    return true;
  }
} 

