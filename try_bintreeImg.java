import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_bintreeImg{
    public static void main(String[] args) {
        test0();
        test1();
    }
    
    public static Node geneBinFromList(List<Integer> ls){
        BST b1 = new BST();
        b1.insert(10);
        b1.insert(4);
        b1.insert(14);
        b1.insert(5);
        b1.insert(12);
        b1.insert(7);
        b1.insert(9);
        b1.insert(16);
        b1.insert(11);

        int index = 0;
        boolean isLeaf = true;
        prettyPrint(b1.root, index, isLeaf);
        return b1.root; 
    }

    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);

	List<Integer> ls = list(9, 1, 2, 4);
	Node root = geneBinFromList(ls);
	binImage(root);

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

