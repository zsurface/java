import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_iterator{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
         
        List<String> list = Arrays.asList("cat1", "dog1", "cow1");
        Iterator<String> ite = list.iterator(); 
        while(ite.hasNext()){
            pp(ite.next());
        }

        end();
    }
    public static void test1(){
        beg();

        end();
    }
} 

