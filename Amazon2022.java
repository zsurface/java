import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class SinglyLinkedListNode {
    public int data;
    public SinglyLinkedListNode next;

    public SinglyLinkedListNode(int nodeData) {
        this.data = nodeData;
        this.next = null;
    }
}

class SinglyLinkedList {
    public SinglyLinkedListNode head;
    public SinglyLinkedListNode tail;

    public SinglyLinkedList() {
        this.head = null;
        this.tail = null;
    }

    public void insertNode(int nodeData) {
        SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);

        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }

        this.tail = node;
    }
}

class SinglyLinkedListPrintHelper {
    public static void printList(SinglyLinkedListNode node, String sep, BufferedWriter bufferedWriter) throws IOException {
        while (node != null) {
            bufferedWriter.write(String.valueOf(node.data));

            node = node.next;

            if (node != null) {
                bufferedWriter.write(sep);
            }
        }
    }
}



public class Amazon2022{
    public static void main(String[] args) {
        test0();

    }
    public static void test0(){

        {
            String password = "good";
            long n = findPasswordStrength(password);
            pp("n=" + n);
        }
        {
            fl("");
            String password = "test";
            long n = findPasswordStrength(password);
            pp("n=" + n);
        }
        {
            String s = randomStr(1000);
            long max = findPasswordStrength(s);
            pl("max=" + max);
        }
        {
            fl("maximumPages");
            SinglyLinkedList sl = new SinglyLinkedList();
            sl.insertNode(1);
            sl.insertNode(2);
            int max = maximumPages(sl.head);
            pl("max=" + max);
        }
    }


    public static List<String> allSubstring(String s){
        List<String> list = new ArrayList<>();
        if( s != null){
            int len = s.length();
            for(int k=0; k<len; k++){
                for(int i=0; i<len; i++){
                    if(k < i + 1){
                        String sub = s.substring(k, i+1);  // a, ab, abc, abcd
                        list.add(sub);
                    }
                }
            }
        }
        return list;
    }


    public static long findPasswordStrength(String s) {
        int sum = 0;
        int len = s.length();
        Set<Character> set = new HashSet<>();
        for(int k=0; k<len; k++){
            for(int i=0; i<len; i++){
                if(k < i + 1){
                    String sub = s.substring(k, i+1);  // a, ab, abc, abcd

                    for(int x = 0; x < sub.length(); x++){
                        set.add(sub.charAt(x));
                    }
                    sum += set.size();

                    set.clear();
                }
            }
        }
        return sum;
    }

    public static int maximumPages(SinglyLinkedListNode head) {
        SinglyLinkedListNode curr = head;
        SinglyLinkedListNode  prev = null;

        int max = Integer.MIN_VALUE;
        while(curr != null){

            SinglyLinkedListNode tmpHead = curr;
            SinglyLinkedListNode next = curr.next;
            int s = curr.data;
            while(curr.next != null){
                prev = curr; 
                curr = curr.next;
            }
            s += curr.data; 

            pl("s=" + s);

            if(s > max){
                max = s;
            }

            if(tmpHead != prev){
                curr = next;
                prev.next = null;
            }else{
                curr = null;
            }
        }
        return max;
    }

} 

