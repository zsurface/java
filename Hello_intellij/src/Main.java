import static classfile.Aron.beg;
import static classfile.Aron.end;
import static classfile.Print.pp;

public class Main {
    public static void main(String[] args) {
        pp("Hello World");
        test0();
        test1();
    }
    static void test0(){
        beg();
        end();
    }
    static void test1(){
        beg();
        end();
    }
}
