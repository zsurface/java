import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.t;

import classfile.Node;
import classfile.Ut;


public class Main {
    public static void main(String[] args) {
        pp("Hello World");
        test0();
        test1();
        test2();
    }


    static void test2(){
        {
            Node root = new Node(1);
            root.next = new Node(2);
            root.next.next = new Node(3);
            root.next.next.next = new Node(4);
            Node head = reverseLinkedListRecur(root);
            List<Integer> ls = linkedToList(head);
            List<Integer> expList = Arrays.asList(4, 3, 2, 1);
            t(ls, expList);
        }
        {
            Node root = new Node(1);
            Node head = reverseLinkedListRecur(root);
            List<Integer> ls = linkedToList(head);
            List<Integer> expList = Arrays.asList(1);
            t(ls, expList);
        }
    }
    static void test0(){
        beg();
        List<String> list = readFile("/tmp/f.txt");
        Set<Character> set = listToSet(Arrays.asList('<', '>', ')', '(', '[', ']', ',', ';', '{', '}', '*'));
        for(String s : list){
            String str = replaceSetOfString(s, set, " ").toString();
            pp(str);
        }

        end();
    }
    static void test1(){
        beg();
        end();
    }
}
