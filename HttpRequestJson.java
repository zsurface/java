import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.net.http.HttpClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import classfile.*;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Lexer.*;
import static classfile.Symbol.*;
import static classfile.InxStr.*;
import static classfile.AnsiColor.*;


// KEY: json object, json to object, json to class, json array of array of string
//
// SEE: $jlib/Lexer.java => colorBlock()
// SEE: WaiLib.hs SnippetJSON
// gf   /Users/aaa/myfile/bitbucket/haskelllib/WaiLib.hs
//      data SnippetJSON = SnippetJSON{pidls::[Integer], name::TS.Text, snippet::[[String]]} deriving (Generic, Show)
//
//      ReplySnippet <=> SnippetJSON
//

// KEY: json object, json to object, json to class, json array of array of string
//
// SEE: $jlib/Lexer.java => colorBlock()
// SEE: WaiLib.hs SnippetJSON
//      ReplySnippet <=> SnippetJSON
//
// Thu 10 Aug 01:22:27 2023
// FIX: use head() in string instead of list
class ReplySnippet {
    public Integer[] pidls;
    public String name;
    public String[][] snippet;
    @Override
    public String toString() {
        String ret = "";
        for(String[] arr : snippet) {
            for(String s : arr) {
                ret += s + nl();
            }
            ret += nl() + nl();
        }
        return ret;
    }
}

record rec(Integer blockCount, Integer pid, List<String> block) {};

public class HttpRequestJson {
    public static void main(String[] args) throws IOException, JSONException {
        pl((char)27 + "[1J");
        if(args.length == 2 || args.length == 3 || args.length == 4) {
            String urlPort = args[0];
            JSONObject json = null;
            if(args.length == 2) {
                String qs1 = args[1];
                json = readJsonFromUrl(urlPort + "/snippet?id=o%20" + qs1);
            } else if(args.length == 3) {
                String qs1 = args[1];
                String qs2 = args[2];
                json = readJsonFromUrl(urlPort + "/snippet?id=o%20" + qs1 + "%20" + qs2);
            } else if(args.length == 4) {
                String qs1 = args[1];
                String qs2 = args[2];
                String qs3 = args[3];
                json = readJsonFromUrl(urlPort + "/snippet?id=o%20" + qs1 + "%20" + qs2  + "%20" + qs3);
            }

            // pl(json.toString());
            // pl(json.get("name"));
            // pl(json.get("snippet"));
            // pl(json.length());
            Gson gson = new Gson();

            ReplySnippet snippet = gson.fromJson(json.toString(), ReplySnippet.class);
            // System.out.println(snippet.toString());
            // String[] cmd = {"/usr/local/bin/bash", "-c", "s grep | cx"};

            List<rec> lsx = list();
            int blockCount = 0;
            if(len(snippet.snippet) == len(snippet.pidls)){
                for(int i = 0; i < len(snippet.snippet); i++){
					// Drop all empty lines from the end of a list
                    List<String> ls = arrayToList(snippet.snippet[i]);
					List<String> lt = rev(rev(ls).stream().dropWhile(x -> len(trim(x)) == 0).toList());
					
                    // lt.add(nl());
                    lsx.add(new rec(blockCount, snippet.pidls[i], lt));
                    blockCount++;
                }
            }else{
				System.out.println("ERROR: It might be Invalid format snippet");
			}

            var cx = map (x -> unlines(x.block()), lsx);
            writeFileList(getEnv("t"), cx);
            int ncol = 4;

            // Maximum number of lines per screen
            boolean done = false;
            final int MAXLINE = 40;
            List<rec> allBlock = new ArrayList<>();
            Queue<rec> q = listToQueue(lsx);
            while(q.size() > 0 && !done) {
                rec qs = q.peek();
                var bn = allBlock.stream().map(x -> len(x.block())).collect(Collectors.toList());
                if(sum(bn) + len(qs.block()) <= MAXLINE) {
                    var blockRec = q.remove();
                    allBlock.add(blockRec);
                    if(q.size() == 0) {

                        printBlock(allBlock, ncol);
                        var cmd = trim(stdin());

                        if(cmd.equals("q")) {
                            done = true;
                        } else {
                            var bb = head(cmd);
                            if(bb.equals("b")) {
                                var blockNum = strToInt(last(splitStr(cmd, ' ')));
                                String fn = getEnv("t");
                                writeFile(fn, unlines(lsx.get(blockNum).block()));
                            }
                        }
                    }
                } else {
                    if(len(allBlock) > 0) {
                        printBlock(allBlock, ncol);
                        var cmd = trim(stdin());
                        if(cmd.equals("q")) {
                            done = true;
                        } else {
                            var lx = splitStr(cmd);
                            if(len(lx) > 0) {
                                var bb = head(lx);
                                if(bb.equals("b")) {
                                    var blockNum = strToInt(last(lx));
                                    String fn = getEnv("t");
                                    writeFile(fn, unlines(lsx.get(blockNum).block()));
                                }
                            }
                        }
                    } else {
                        // First block is len(block) > MAXLINE
                        rec blockRec = q.remove();
                        List<String> qz = colorBlock(blockRec.block());
                        var firstLine = qz.get(0);

                        // var str = blockRec.blockCount() + " len=" + len(blockRec.block());
                        var str = blockRec.blockCount() + " len=" + len(blockRec.block());

                        var pidstr   = colorize(YELLOW, "pid=" + blockRec.pid());
                        var colorStr = colorize(RED, "[" + str + " " + pidstr + "] ") + firstLine;
                        qz.remove(0);
                        // qz.set(0, colorStr + firstLine);

                        // Partition a big page, and prepend a head to each block
                        List<ArrayList<String>> lw = partList(MAXLINE, qz);
                        var ly = map (x -> prepend(colorStr, x), lw);
                            for(int i = 0; i < len(ly) && !done; i++){
                                List<String> lx = ly.get(i);
                                int nx = 1;
                                clearScreen();
                                for(String x : lx) {
                                    moveLineCol(nx, ncol);
                                    System.out.println(x);
                                    nx++;
                                }
                                moveLineCol(nx, ncol);
                                // System.out.println("[Next]");
                                pl(colorize(RED, "" + '\u25B6'));

                                nx++;
                                moveLineCol(nx, ncol);

                                var cmd = trim(stdin());
                                if(cmd.equals("q")) {
                                    done = true;
                                } else {
                                    var lu = splitStr(cmd);
                                    if(len(lu) > 0) {
                                        var bb = head(lu);
                                        if(bb.equals("b")) {
                                            var blockNum = strToInt(last(lu));
                                            String fn = getEnv("t");
                                            writeFile(fn, unlines(lsx.get(blockNum).block()));
                                        }
                                    }
                                }
                            }
                    }
                }
            }
        } else {
            System.out.println("Need 2 or 3 or 4 arguments");
            System.out.println("HttpRequestJson http://localhost:8080 latex");
            System.out.println("HttpRequestJson http://localhost:8080 latex frac");
            System.out.println("HttpRequestJson http://localhost:8080 latex frac aligned");
        }
    }

    /**
       Get the number of terminal lines
       NOTE: DOES NOT get the right number of lines from terminal.
     */
    public static Integer terminalLines() {
        Integer n = strToInt(trim(head(run("tput lines"))));
        return n;
    }

    /**
       Get the number of terminal columns
       NOTE: DOES NOT get the right number of columns from terminal.
     */
    public static Integer terminalColumns() {
        Integer n = strToInt(trim(head(run("tput cols"))));
        return n;
    }

    public static String stdin() {
        boolean done = false;
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        return str;
    }
    public static void printUnicode(char c) {
        PrintWriter pw = new PrintWriter(System.out,true);
        char ss = c;
        pw.println(ss);
    }

    public static void printBlock(List<rec> allBlock, int nCol) {
        clearScreen();

        int ln = 1;
        for(rec xx : allBlock) {
            var zx = colorBlock(xx.block());
            var firstLine = zx.get(0);

            var str = xx.blockCount() + " len=" + len(xx.block());
            // var colorStr = colorize(GREEN, "[" + str + "] ");

            var pidstr   = colorize(YELLOW, "pid=" + xx.pid());
            var colorStr = colorize(RED, "[" + str + " " + pidstr + "] ");

            zx.set(0, colorStr + firstLine);

            for(String x : zx) {
                moveLineCol(ln, nCol);
                System.out.println(x);
                ln++;
            }
            ln++;
            moveLineCol(ln, nCol);
            char leftBot  = '\u23A3';
            char rightTop = '\u23A4';

            /*
            ◻
            WHITE MEDIUM SQUARE
            Unicode: U+25FB U+FE0E, UTF-8: E2 97 BB EF B8 8E
            */
            // String s = "" + leftBot + '\u25FB' + rightTop;
            String s = "" + '\u2B1C';
            pl(colorize(RED, s));
            ln++;
            moveLineCol(ln, nCol);
        }
        /*
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        */
        clear(allBlock);
    }

    public static <T> int listSize(List<List<T>> ls) {
        int s = 0;
        for(List<T> x : ls) {
            s += len(x);
        }
        return s;
    }

    public static void moveRight(int n, List<String> ls) {
        for(String s : ls) {
            moveCursorRight(n);
            pl(s);
        }
    }

    public static void moveLineCol(int line, int col) {
        char c = (char)27;
        // var s = "[" + line + ";" + col + "H";
        var s = "[" + line + ";" + col + "f";
        System.out.print(c + s);
    }

    public static void moveCursorDown(int n) {
        char c = (char)27;
        var s = "[" + n + "B";
        System.out.print(c + s);
    }

    public static void moveCursorUp(int n) {
        char c = (char)27;
        var s = "[" + n + "A";
        System.out.print(c + s);
    }

    public static void moveCursorLeft(int n) {
        char c = (char)27;
        var s = "[" + n + "D";
        System.out.print(c + s);
    }

    // Move cursor to the right =>
    public static void moveCursorRight(int n) {
        char c = (char)27;
        var s = "[" + n + "C";
        System.out.print(c + s);

    }

    public static void clearScreen() {
        char c = (char)27;
        var s = "[" + 2 + "J";
        System.out.print(c + s);
    }

    public static void padPrint(List<String> s) {
        var pad = concat("", repeat(10, " "));
        for(String a : s) {
            pl(pad + a);
        }
    }
    public static void test4() {
        String jsonStr = httpJson("http://localhost:8081/snippet?id=o%20test");
        pl(jsonStr);
        // pl(json.get("name"));
        // pl(json.get("snippet"));
    }
    public static void test3() throws IOException, JSONException {
        JSONObject json = readJsonFromUrl("http://localhost:8081/snippet?id=o%20test");
        pl(json.toString());
        pl(json.get("name"));
        pl(json.get("snippet"));
        pl(json.length());
        Gson gson = new Gson();

        ReplySnippet snippet = gson.fromJson(json.toString(), ReplySnippet.class);
        // System.out.println(snippet.toString());
        for(String[] ar : snippet.snippet) {
            for(String s : ar) {
                pl(s);
            }
            pl("--");
        }
    }
}
