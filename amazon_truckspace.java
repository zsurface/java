import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
 * Given a truckspace and a list of packages
 * find two packages with 30 unit space safety fits in the truck.
 *
 * truckspace 90
 * a list of packages [1, 10, 25, 35, 60]
 *
 */
public class amazon_truckspace{
    public static void main(String[] args) {
        List<Integer> ls = Arrays.asList(1, 20, 25, 35, 60, 15, 70, 45);
        List<Integer> mypair = pair(90, ls);
        p(mypair);
        
    }
    public static List<Integer> pair(Integer truckspace, List<Integer> packages){
       List<Integer> p = new ArrayList<>();
       Set<Integer> set = new HashSet<Integer>(); 
       for(Integer n : packages){
           set.add(n);
       }

       Set<List<Integer>> sset = new HashSet<>();

       List<Integer> ls = new ArrayList<>(set);
       for(int i=0; i<ls.size(); i++){
           for(int j=0; j<ls.size(); j++){
               if(i != j){
                   List<Integer> s = new ArrayList<>();
                   if(ls.get(i) < ls.get(j)){
                       s.add(ls.get(i));
                       s.add(ls.get(j));
                   }else{
                       s.add(ls.get(j));
                       s.add(ls.get(i));
                   }
                   sset.add(s);
               }
           }
       }

       Integer ma = 0;
       Integer mi = 0; 
       for(List<Integer> s : sset){
         if(s.get(0) + s.get(1) + 30 == 90){
            // max 
            if(s.get(1) > ma){     
                ma = s.get(1);
                mi = s.get(0);
                p = new ArrayList<>();
                p.add(s.get(0));
                p.add(s.get(1));
            }else if(s.get(1) == ma){   // max == max'
                if(s.get(0) > mi){      // compare the min > min'
                    ma = s.get(1);
                    mi = s.get(0);
                    p = new ArrayList<>();
                    p.add(s.get(0));
                    p.add(s.get(1));
                }
            }
         }
       }
       return p;
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

