import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_array{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
	{
	    // 1   2   3       4
	    //    1x2  1x2x3  1.2.3.4
	    // 1   2   6       24
	    int[] arr = {1, 2, 3, 4};
	    int[] tmp = {2, 3, 4, 5};
	    for(int i = 1; i < len(arr); i++)
		arr[i] = arr[i-1]*arr[i];
	    p(arr);

	    for(int i = 1; i < len(tmp); i++){
		int ix = len(tmp) - 1;
		tmp[ix - i] = tmp[ix - (i - 1)] * tmp[ix - i];
	    }
	    fl();
	    p(tmp);
	    
	    
	}
        end();
    }
} 

