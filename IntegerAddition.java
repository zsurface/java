import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;



/**
<pre>
{@literal
    Add two long Integers
    TestFile: /Users/cat/myfile/bitbucket/java/IntegerAddition.java
}
{@code
}
</pre>
*/ 
public class IntegerAddition{
    public static void main(String[] args) {
        test0();

        test1();
    }

    /**
        11:10am
    */
    public static int[] addInteger(int[] a, int[] b){
       if(a != null && a != null){
           int lena = a.length;
           int lenb = b.length; 
           int maxlen = Math.max(lena, lenb);
           int[] aa = new int[maxlen + 1];
           int[] bb = new int[maxlen + 1];
           int[] c  = new int[maxlen + 1];

           /**
             009
             099
           */
           int ai = 0;
           int bi = 0;
           for(int i=0; i<maxlen + 1; i++){
               if(i < (maxlen + 1 - lena))
                  aa[i] = 0;     
               else{
                  aa[i] = a[ai];
                  ai++;
               }
                
               if(i < (maxlen + 1 - lenb))
                  bb[i] = 0;
               else{
                  bb[i] = b[bi];
                  bi++;
               }
           }
           
           /**
            09
            09
            18
           */
           int carry = 0;
           for(int i=maxlen; i >= 0; i--){
               int s = carry + aa[i] + bb[i]; 
               c[i] = s % 10;
               carry = s/10;
           }
           return c;
       }
       throw new IllegalArgumentException("Arguments can not be null.");
    }

    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();
        {
            int[] arr1 = {9};
            int[] arr2 = {9};
            int[] exp = {1, 8};
            int[] c = addInteger(arr1, arr2);
            t(c, exp);
        }

        {
            int[] arr1 = {0};
            int[] arr2 = {9};
            int[] exp = {0, 9};
            int[] c = addInteger(arr1, arr2);
            t(c, exp);
        }

        {
            int[] arr1 = {9};
            int[] arr2 = {9,9};
            int[] exp = {1, 0, 8};
            int[] c = addInteger(arr1, arr2);
            t(c, exp);
        }
        {
            int[] arr1 = {9, 9};
            int[] arr2 = {9, 9,9};
            int[] exp = {1, 0, 9, 8};
            int[] c = addInteger(arr1, arr2);
            t(c, exp);
        }

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();


        sw.printTime();
        end();
    }
} 

