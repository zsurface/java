import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class multilist {
public static void main(String[] args) {
    System.out.println("Hello world!");
    int[] row = {9, 7};
    int[] col = {1, 2};
    // int[][] m = multi(arr1, arr2);
    // for(int i=0; i<m.length; i++){
    //   for(int j=0; j<m[0].length; j++){
    //     System.out.print(m[i][j] + " ");
    //   }
    //   System.out.println("");
    // }
    int[][] m = multi(row, col);
  }

  static int[][] multi(int[] row, int[] col){
    int[][] m = null;
    if (col != null && row != null){
      int lc = col.length;
      int lr = row.length;
      m = new int[lc][lr + lc];
      for(int c=0; c<lc; c++){
          int[] arr1 = new int[lr + lc];
          multiList(col[c], row, arr1, c);
          m[c] = arr1;
      }
      // left shift
      for(int c=0; c<lc; c++){
        for(int r=0; r<lc + lr; r++){
          System.out.print(m[c][r]);
        }
        System.out.println();
      }

    }
    return m;
  }

  static void multiList(int n, int[] row, int[] arr, int s){
    if (row != null){
      int len = row.length;
      int carry = 0;
      int k=arr.length - 1; 
      for(int i=len - 1; i>=0; i--){
        int m = n*row[i];
        int r = m % 10;
        // left shift: s
        arr[k - s] = carry + r;
        carry = m / 10;
        k--;
      }
      arr[k] = carry;
    }
  }
}
