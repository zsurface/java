import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_bs{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            Integer[] arr = {1, 2, 3, 4};
            int width = arr.length; 
            Aron.end();
            int lo =0;
            int hi = 3;
            boolean ret = binarySearch(2, arr, lo, hi);
            Print.p("ret=" + (ret == true));
        }
        { 
            Integer[] arr = {1, 2, 3, 4};
            int width = arr.length; 
            Aron.end();
            int lo =0;
            int hi = 3;
            int k = 5;
            boolean ret = binarySearch(k, arr, lo, hi);
            Print.p("ret=" + (ret == false));
        }
        { 
            Integer[] arr = {1};
            int width = arr.length; 
            Aron.end();
            int lo =0;
            int hi = 0;
            int k = 1;
            boolean ret = binarySearch(k, arr, lo, hi);
            Print.p("ret=" + (ret == true));
        }
        { 
            Integer[] arr = {1};
            int width = arr.length; 
            Aron.end();
            int lo =0;
            int hi = 0;
            int k = 2;
            boolean ret = binarySearch(k, arr, lo, hi);
            Print.p("ret=" + (ret == false));
        }

    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        Binary Search
    }
    {@code
            Integer[] arr = {1, 2, 3, 4};
            int width = arr.length; 
            Aron.end();
            int lo =0;
            int hi = 3;
            int k = 5;
            boolean ret = binarySearch(k, arr, lo, hi);
            Print.p("ret=" + (ret == false));
    }
    </pre>
    */ 

    public static <T extends Number & Comparable <? super T>> boolean binarySearch(T k, T[] arr, int lo, int hi){
        boolean ret = false;
        if(arr != null){
            if(lo <= hi){
                int mid = (hi + lo)/2;
                if(k.compareTo(arr[mid]) < 0)
                    return binarySearch(k, arr, lo, mid-1);
                else if(k.compareTo(arr[mid]) > 0)
                    return binarySearch(k, arr, mid + 1, hi);
                else 
                    ret = true; 
            }
        }
        return ret;
    }
} 

