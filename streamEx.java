import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

public class streamEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        Stream<String> s1 = Stream.of("dog", "Michael");
        Stream<String> s2 = Stream.of("cat", "Lion Air", "Ethiopian");
        Stream<String> s3 = Stream.concat(s1, s2);
        s3.forEach(System.out::println);
        // String str = Stream.of("dog", "Michael", "Lion Air", "Ethiopian").collect(Collectors.joining());
        // String str = Stream.concat(s1, s2).collect(Collectors.joining());
        // Print.p("str=" + str);

        Aron.end();
    }
    public static void test1(){
        int count = 100;
        Print.p("totoal str: " + count);
        List<String> list = Aron.geneRandomStrList(1, 10, count);
        Aron.beg();
        {
            String sum = "";
            StopWatch sw = new StopWatch();
            sw.start();
            for(String s : list){
                sum += s;
            }
            sw.printTime("normal str concat ");
        }
        {
            String sum = "";
            StringBuilder bw = new StringBuilder();

            StopWatch sw = new StopWatch();
            sw.start();
            for(String s : list){
                bw.append(s);
            }
            sw.printTime("StringBuffer");
        }
        {
            String sum = "";
            StringBuffer sb = new StringBuffer();

            StopWatch sw = new StopWatch();
            sw.start();
            for(String s : list){
                sb.append(s);
            }
            sw.printTime("StringBuilder");
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();
            list.stream().collect(Collectors.joining(","));

            sw.printTime("Stream joining()");
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();
            list.stream().reduce("", String::concat); // foldr(\x y -> x ++ y) list

            sw.printTime("Stream reduce() String::concat");
        }
        {
            String str1 = StringUtils.leftPad("dog", 6);
            Print.pbl(str1);
            String str2 = StringUtils.leftPad("dog", 6, "-");
            Print.pbl(str2);
        }
        {
            String str = "abc";
            Aron.permutation(str);
        }
        {
            String str = "abc";
            List<String> ls = new ArrayList<>();
            Aron.permutation(str, ls);
            Print.fl();
            Print.p(ls);
        }

        Aron.end();
    }
} 

