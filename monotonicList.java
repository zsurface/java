import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class monotonicList{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        end();
    }
    public static void test1(){
        beg();
        
        {
            char ch = head("abc");
            t(ch, 'a');
        }
        {
            char ch = head("a");
            t(ch, 'a');
        }
        {
            char ch = last("ab");
            t(ch, 'b');
        }
        {
            char ch = last("a");
            t(ch, 'a');
        }
        {
            String s = init("a");
            t(s, "");
        }
        {
            String s = tail("a");
            t(s, "");
        }
        {
            List<Integer> list = Arrays.asList(1, 2, 3);
            Integer n = head(list);
            t(n, 1);
        }
        {
            List<Integer> list = Arrays.asList(1, 2, 3);
            Integer n = last(list);
            t(n, 3);
        }
        {
            {
                List<Integer> list = Arrays.asList(1, 2, 3);
                List<Integer> exp = Arrays.asList(1, 2);
                List<Integer> ls = init(list);
                t(ls, exp);
            }
            {
                List<Integer> list = Arrays.asList(1);
                List<Integer> exp = Arrays.asList();
                List<Integer> ls = init(list);
                t(ls, exp);
            }
        }
        {
            {
                List<Integer> list = Arrays.asList(1, 2, 3);
                List<Integer> exp = Arrays.asList(2, 3);
                List<Integer> ls = tail(list);
                t(ls, exp);
            }
            {
                List<Integer> list = Arrays.asList(1);
                List<Integer> exp = Arrays.asList();
                List<Integer> ls = tail(list);
                t(ls, exp);
            }
        }


        end();
    }

    public static char head(String s) {
        return s.charAt(0); 
    }
    public static char last(String s) {
        return s.charAt(s.length() - 1); 
    }

    public static <T> T head(List<T> list) {
        return list.get(0); 
    }

    public static <T> T last(List<T> list) {
        return list.get(list.size() - 1); 
    }

    public static String init(String s) {
        return s.substring(0, s.length() - 1); 
    }

    public static <T> List<T> init(List<T> list) {
        return list.subList(0, list.size() - 1); 
    }

    public static String tail(String s) {
        return s.substring(1, s.length()); 
    }

    public static <T> List<T> tail(List<T> list) {
        return list.subList(1, list.size()); 
    }

    public static int monotonicList_(List<Integer> ls){
        if(ls != null){
            int len = ls.size();
            Boolean in = true;
            Boolean de = true;
            if(len > 1){
                // 3 1 2
                for(int i=0; i<len - 1; i++){
                    if(ls.get(i) < ls.get(i + 1)){
                        in &= true;
                        de &= false;
                    }
                    else if(ls.get(i) > ls.get(i + 1)){
                        de &= true;
                        in &= false;
                    }
                }
            }
            return !(de || in) ? 0 : (in ? -1 : 1);
        }
        throw new IllegalArgumentException("Argument can not be null.");
    }
} 

