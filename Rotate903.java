import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.Print;
import classfile.Aron;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
* KEY:  rotate matrix, rotate array 
* TITLE: rotate matrix 90 degrees 
* DESC:  nice description 
*/
public class Rotate903{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        int[][] arr = {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
        };

        Aron.printArray2D(arr);
        Print.fl();
        rotate90(arr);
        Aron.printArray2D(arr);
            
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    public static void rotate90(int[][] arr){
        if(arr != null){
            int len = arr.length;
            for(int k=0; k<len/2; k++){
                for(int i=k; i<len-1-k; i++){
                    int tmp = arr[k][i];
                    arr[k][i] = arr[len-1-i][k];
                    arr[len-1-i][k] = arr[len-1-k][len-1-i];
                    arr[len-1-k][len-1-i] = arr[i][len-1-k];
                    arr[i][len-1-k] = tmp;
                }
            }
        }
    }
} 

