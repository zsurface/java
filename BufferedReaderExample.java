import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// KEY: Read from standard in or read from pipe, System.in, BufferedReader, java stdin
public class BufferedReaderExample{
    public static void main(String[] args) {
	pp("334");
	List<String> ls = readStdin();
	pp(ls);
    }
    
    public static List<String> readStdin(){
	List<String> ls = new ArrayList<>();

	try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
	    String line = null;
	    while((line = br.readLine()) != null){
		ls.add(line);
		// System.out.println(line);
	    }
	}catch(IOException e){
	    e.printStackTrace();
	}
	return ls;
    }
    public static void test0(){
        beg();

        end();
    }
    public static void test1(){
        beg();

        end();
    }
} 

