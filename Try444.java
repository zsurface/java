import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import classfile.Node;
import classfile.BST;
import java.util.stream.*;

/*
class Node{
    Integer data;
    Node left;
    Node right;
    public Node(Integer data){
        this.data = data;
    }
}
*/


class Tup{
    String fst;
    String snd;
    public Tup(String fst, String snd){
        this.fst = fst;
        this.snd = snd;
    }
}

class Kthlarge{
    PriorityQueue<Integer> queue = new PriorityQueue<>();
    Integer k;
    public Kthlarge(Integer k, Integer[] ls){
        this.k = k;
        for(int i = 0; i < ls.length; i++){
            queue.add(ls[i]);
        }
    }
    public Integer add(Integer n){
        queue.add(n);
        Integer x = null;
        Integer c = k;
        List<Integer> ls = new ArrayList<>();
        while(c > 0){
           x = queue.remove(); 
           ls.add(x);
           c--;
        }

        for(Integer m : ls)
            queue.add(m);

        return x;
    }

}

class Person{
    String name;
    Integer age;
    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
    }
}


public class Try444{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();
        {
            Tup tup = firstWord("a b");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            Tup tup = firstWord(" a b");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            Tup tup = firstWord("a");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            Tup tup = firstWord("");
            pp("[" + tup.fst + "]");
            pp("[" + tup.snd + "]");
        }
        {
            fl();
            List<String> ls = prefixStr("");
            pp(ls);
            pp("len=" + len(ls));
        }
        {
            fl();
            List<String> ls = prefixStr("a");
            pp(ls);
            pp("len=" + len(ls));
        }
        {
            fl();
            List<String> ls = prefixStr("ab");
            pp(ls);
            pp("len=" + len(ls));
        }
        {
            fl();
            String s = "abc";
            prefixSuffix(s);
        }
        {
            fl();
            {
                String s = "abc e";
                List<String> ls = splitStr(s);
                pp(ls);
                fl();
            }
            {
                String s = " abc e";
                List<String> ls = splitStr(s);
                pp(ls);
                fl();
            }
            {
                String s = " abc e ";
                List<String> ls = splitStr(s);
                pp(ls);
                fl();
            }
        }    
        {
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(2, arr);
                Integer kth = kl.add(0);
                pl("kth=" + kth + " " + (kth == 1));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(2, arr);
                Integer kth = kl.add(9);
                pl("kth=" + kth + " " + (kth == 2));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(2, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 1));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(3, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 2));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(1, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 1));
            }
            {
                Integer[] arr = {4, 2, 5, 1};
                Kthlarge kl = new Kthlarge(4, arr);
                Integer kth = kl.add(1);
                pl("kth=" + kth + " " + (kth == 4));
            }
            {
                fl();
                PriorityQueue<Person> queue = new PriorityQueue<>((x, y) -> y.age - x.age);

                queue.add(new Person("David", 3));
                queue.add(new Person("Anne", 9));
                queue.add(new Person("Michael", 2));

                while(!queue.isEmpty()){
                    Person p = queue.remove();
                    pl("name=" + p.name + " age=" + p.age);
                }
                
            }
            {
                {
                    /**
                                    10
                                  5    12
                    */
                    Node root = new Node(10);
                    root.left = new Node(5);
                    root.right = new Node(12);
                    levelTraveral(root);
                }
                {
                    fl();
                    /**
                                  10
                              5             14
                          1     8      11          18
                      0    2  6   9  10    13  17      20

                    */
                    Node root = new Node(10);
                    root.left = new Node(5);
		    root.right = new Node(14);

		    root.left.left = new Node(1);
		    root.left.right = new Node(8);
		    root.right.left = new Node(11);
		    root.right.right = new Node(18);

		    root.left.left.left = new Node(0);
		    root.left.left.right = new Node(2);
		    root.left.right.left = new Node(6);
		    root.left.right.right = new Node(9);
		    root.right.left.left = new Node(10);
		    root.right.left.right = new Node(13);
		    root.right.right.left = new Node(17);
		    root.right.right.right = new Node(20);

                    zigzagTraveral(root);
                }
            }
	    {
		fl("copyBinTree");
		Node root = new Node(10);
		root.left = new Node(5);
		root.right = new Node(14);

		root.left.left = new Node(1);
		root.left.right = new Node(8);
		root.right.left = new Node(11);
		root.right.right = new Node(18);

		root.left.left.left = new Node(0);
		root.left.left.right = new Node(2);
		root.left.right.left = new Node(6);
		root.left.right.right = new Node(9);
		root.right.left.left = new Node(10);
		root.right.left.right = new Node(13);
		root.right.right.left = new Node(17);
		root.right.right.right = new Node(20);
		Node r = copyBinTree(root);
		levelTraveral(r);
	    }
	    {
		{
		    /**
		        10
		      5   
                    */
                    Node root1 = new Node(10);
                    root1.left = new Node(5);
		    /**
		        11
		           9
                    */
		    Node root2 = new Node(11);
                    root2.right = new Node(9);
		    
		    /**
		            21
                         5      9
		       
		     */		    
		    Node root = mergeBinTree(root1, root2);
		    levelTraveral(root);
		}
		{
		    /**
		        10
		      5    12
                               15   
                    */
		    BST bst1 =  createBin(list(10, 5, 12, 15));
		    /**
		        11
		      9
                    */
		    BST bst2 =  createBin(list(11, 9));

		    
		    /**
		            21
                         14      12
                                    15
		       
		     */		    
		    Node root = mergeBinTree(bst1.root, bst2.root);
		    // levelTraveral(root);
		    printBinTree(root);
		    
		}
		{
		    /**
		        10
		      5    12
                               15
                      Can not use insert-image to insert pdf
		      http://localhost/pdf/bin_2704.pdf
                    */
		    BST bst1 =  createBin(list(10, 5, 12, 15));
		    Node root = padBinTree(bst1.root);
		    printBinTree(root);
		}
	    }
        }
        end();
    } 

    /**
       leetcode question
       https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/
     */
    public static void zigzagTraveral(Node root){
        Queue<Node> q1 = new LinkedList<>();
        Queue<Node> q2 = new LinkedList();
        Stack<Node> stack = new Stack<>();
        if(root != null){
            q1.add(root);

            while(!q1.isEmpty() || !q2.isEmpty()){
                while(!q1.isEmpty()){
                    Node node = q1.remove();
                    pp(node.data + " ");
                    if(node.left != null)
                        q2.add(node.left);
                    if(node.right != null)
                        q2.add(node.right);
                }
                fl();
                
                while(!q2.isEmpty()){
                    Node node = q2.remove();
                    stack.add(node);
                    // pp(node.data + " ");
                    if(node.left != null)
                        q1.add(node.left);
                    if(node.right != null)
                        q1.add(node.right);
                }
                while(!stack.isEmpty()){
                    Node node = stack.pop();
                    pp(node.data + " ");
                }
                fl();
            }
        }
    }
    
    public static void levelTraveral(Node root){
        Queue<Node> q1 = new LinkedList<>();
        Queue<Node> q2 = new LinkedList();
        Stack<Node> stack = new Stack<>();
        if(root != null){
            q1.add(root);

            while(!q1.isEmpty() || !q2.isEmpty()){
                while(!q1.isEmpty()){
                    Node node = q1.remove();
                    pp(node.data + " ");
                    if(node.left != null)
                        q2.add(node.left);
                    if(node.right != null)
                        q2.add(node.right);
                }
                fl();
                
                while(!q2.isEmpty()){
                    Node node = q2.remove();
                    stack.add(node);
                    pp(node.data + " ");
                    if(node.left != null)
                        q1.add(node.left);
                    if(node.right != null)
                        q1.add(node.right);
                }
                while(!stack.isEmpty()){
                    Node node = stack.pop();
                    // pp(node.data + " ");
                }
                fl();
            }
        }
    }

    /**
         "" => []
         "a" => 
    */
    public static List<String> prefixStr(String s){
        List<String> ls = new ArrayList<>();
        for(int i = 0; i < s.length(); i++){
           String str = s.substring(0, i + 1);
           ls.add(str);
        }
        return ls;
    }

    /**
         a b c d e

         a b c [ ]
               [d] e
         
    */
    public static void prefixSuffix(String s){
        List<String> prefix = new ArrayList<>();
        List<String> suffix = new ArrayList<>();
        for(int i = 0; i < s.length(); i++){
            String ps = s.substring(0, i+1);
            String ss = s.substring(i+1, s.length());
            prefix.add(ps);
            suffix.add(ss);
        }
        pl("prefix len=" + len(prefix));
        pp(prefix);
        fl();
        pl("suffix len=" + len(suffix));
        pp(suffix);
    }

    /**
        _ _ ab_  => ["ab"]
        ab _ _   => ["ab"]
        ab _ _ c  => ["ab", "c"]
    */
    public static List<String> splitStr(String s){
        String accumulator = "";
        List<String> ls = new ArrayList<>();
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(c == ' '){
                if(accumulator.length() > 0){
                    ls.add(accumulator);
                    accumulator = "";
                }
            }else{
                String cStr = c + "";
                accumulator += cStr;
            }
        }
        if(accumulator.length() > 0)
            ls.add(accumulator);

        return ls;
    }

    /**
       Copy a binary tree
     */
    public static Node copyBinTree(Node root){
	if(root != null){
	    Node left = copyBinTree(root.left);
	    Node right = copyBinTree(root.right);
	    Node node = new Node(root.data);
	    node.left = left;
	    node.right = right;
	    return node;
	}else{
	    return null;
	}
    }

    /**
         Node 9 only have left subtree, add a right subtree to node 9

             9      =>     9
          4             4    10


	  Nothing to be filled, each node have both subtrees or no subtree
	  
             9       =>     9
          4     11      4       11
     */
    public static Node padBinTree(Node root){
	if(root != null){
	    Node left = padBinTree(root.left);
	    Node right = padBinTree(root.right);
	    Node node = new Node(root.data);
	    if((left == null && right == null) || (left != null && right != null)){
		node.left = left;
		node.right = right;
	    }else{
		node.left = left == null ? new Node(root.data - 1)   : left;
		node.right = right == null ? new Node(root.data + 1) : right;
		
	    }
	    return node;
	}else{
	    return null;
	}
    }

    

    /**
       leetcode question
       https://leetcode.com/problems/merge-two-binary-trees/
       
     */
    public static Node mergeBinTree(Node root1, Node root2){
	if(root1 != null && root2 != null){
	    Node left = mergeBinTree(root1.left, root2.left);
	    Node right = mergeBinTree(root1.right, root2.right);
	    Node node = new Node(root1.data + root2.data);
	    node.left = left;
	    node.right = right;
	    return node;
	}else if(root1 != null){
	    Node left = mergeBinTree(root1.left, null);
	    Node right = mergeBinTree(root1.right, null);
	    Node node = new Node(root1.data);
	    node.left = left;
	    node.right = right;
	    return node;
	}else if(root2 != null){
	    Node left = mergeBinTree(root2.left, null);
	    Node right = mergeBinTree(root2.right, null);
	    Node node = new Node(root2.data);
	    node.left = left;
	    node.right = right;
	    return node;		
	}else{
	    return null;
	}
    }

   


    /**
         _ _ ab _ c
         ab _ _ c
    */
    public static Tup firstWord(String s){
        String acc = "";
        int i = 0;
        for(i=0; i < s.length(); i++){
            if(s.charAt(i) == ' ' && acc.length() == 0){
    
            }else if(s.charAt(i) == ' ' && acc.length() > 0){
                break;
            }else{
               String ss = s.charAt(i) + "";
               acc += ss;
            }
        }
        return new Tup(acc, s.substring(i, s.length()));
    }
}


