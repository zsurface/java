import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_negativeContinuousSum{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    /**
        -1, -2, -3 => -1
        -1, 2, -3  => 2
        -1, 2  -1, 3 
    */
    public static int negativeContinuousSum(int[] arr){
        if(arr != null && arr.length > 0){
            int len = arr.length;
            int currmax = arr[0]; 
            int maxsofar = arr[0];
            for(int i=1; i<len; i++){
                maxsofar = Math.max(arr[i], maxsofar + arr[i]);
                currmax = Math.max(currmax, maxsofar);
            }
            return currmax;
        }
        throw new IllegalArgumentException("Argument is not valid.");
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            int[] arr = {-1, 2, -1, 3};
            int width = arr.length; 
            int max = negativeContinuousSum(arr);
            t(max, 4); 
        }
        {
            int[] arr = {-1, -2, -1, -3};
            int width = arr.length; 
            int max = negativeContinuousSum(arr);
            t(max, -1); 
        }
        {
            int[] arr = {4, -2, -1, 4};
            int width = arr.length; 
            int max = negativeContinuousSum(arr);
            t(max, 5); 
        }
        {
            int[] arr = {4, -2};
            int width = arr.length; 
            int max = negativeContinuousSum(arr);
            t(max, 4); 
        }
        {
            int[] arr = {-4, 1};
            int width = arr.length; 
            int max = negativeContinuousSum(arr);
            t(max, 1); 
        }
        {
            int[] arr = {-4, 1, 2, 5,-8, 9};
            int width = arr.length; 
            int max = negativeContinuousSum(arr);
            t(max, 9); 
        }




        sw.printTime();
        end();
    }
} 

