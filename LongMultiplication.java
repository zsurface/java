import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.Print;
import java.util.stream.*;
import java.util.stream.Collectors;

public class LongMultiplication{
    public static void main(String[] args) {
        test0();
        test1();
    }
    //  8 9 1
    //8 9 1
    public static void test0(){
        Aron.beg();
        int[] a = {9, 9};
        int[] b = {9, 9};
        int[] c = longMulti(a, b);
        Aron.printArray(c);
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    public static int[] longMulti(int[] col, int[] row){
        int[] ret = null;
        if(col != null && row != null){
            int lc = col.length;
            int lr = row.length;
            int k = lc + lr;
            int kInx = k - 1;
            int[][] arr = new int[lc][k];
            for(int c=lc-1; c >= 0; c--){
                int carry = 0;
                int rc = lc - 1 - c;
                for(int r=lr-1; r >=0; r--){
                    int n = col[c]*row[r];
                    int rr = lr - 1 - r;
                    arr[c][kInx - rc - rr] = (carry + n) % 10;
                    carry = (carry + n) / 10;
                }
                // carry 
                arr[c][kInx - rc - lr] = carry;
            }
            ret = new int[k];
            int c = 0;
            for(int i=k-1; i >= 0; i--){
                int s = 0;
                for(int j=0; j<lc; j++){
                    s += arr[j][i];
                }
                ret[i] = (c + s) % 10;
                c = (c + s) / 10;
            }
        }
        return ret;
    }
} 

