import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class GNode<T>{
    GNode left;
    GNode right;
    T data;
    public GNode(T t){
	this.data = t;
    }
}

public class EvaluateTree{
    public static void main(String[] args) {
        test0();
        test1();
    }
    /**
       KEY: preorder and postorder traveral
          (- (- 1 6) (* 8 12))
       
       The code is almost identical to serialize binary tree with preorder and postorder traversals
     */
    public static GNode buildTree(Iterator<String> ite){
	GNode<String> node = null;
	if(ite.hasNext()){
	    String s = ite.next();
	    if(isOpt(s)){
		GNode left = buildTree(ite);
		GNode right = buildTree(ite);
		node = new GNode(s);
		node.left = left;
		node.right = right;
	    }else{
		node = new GNode(s);
	    }
	}
	return node;
    }
    public static void preorder(GNode node){
	if(node != null){
	    pp(node.data + " ");
	    preorder(node.left);
	    preorder(node.right);
	}
    }
    public static void inorder(GNode node){
	if(node != null){
	    inorder(node.left);
	    pp(node.data + " ");
	    inorder(node.right);
	}
    }
    public static void inorderToList(GNode<String> node, List<String> ls){
	if(node != null){
	    inorderToList(node.left, ls);
	    ls.add(node.data);
	    inorderToList(node.right, ls);
	}
    }
    
    public static void postorder(GNode node){
	if(node != null){
	    postorder(node.left);
	    postorder(node.right);
	    pp(node.data + " ");
	}
    }
    
    /**
       KEY: evaluate postorder traveral tree
                       -
               -           *
           1      6     8      + 
                            ^        -
                         2    3   1      6
        =>
          (- (- 1 6) (* 8  (+  (^ 2 3) (- 1 6)) ))

        Expr = num
             | var
             | (Op Expr Expr)
             
     */
    public static GNode evalPostOrder(GNode<String> node){
        GNode<String> ret = null;
	if(node != null){
	    ret = evalExpr(node, node.left, node.right);
	}
	return ret;
    }
    public static GNode<String> evalExpr(GNode<String> op, GNode<String> left, GNode<String> right){
	GNode ret = new GNode("");
	pb("op=" + op.data);
	if(op != null && isOpt(op.data)){
	    if(op.data.equals("+") || op.data.equals("-")){
		if(isNum(left.data) && isNum(right.data)){
		    Integer l = strToInt(left.data);
		    Integer r = strToInt(right.data);
		    if(op.data.equals("+")){
			ret.data = intToStr(l + r);
		    }else{
			ret.data = intToStr(l - r);
		    }
		}else{
		    ret.data = op.data + " " + left.data + " " + right.data;
		}
	    }
	}
	return ret;
    }
    
    public static Boolean isNum(String s){
	return isNumeric(s);
    }

    /**
       One letter as a variable
     */
    public static Boolean isVar(String s){
	return isLetter(s);
    }
    
    public static Boolean isOpt(String s){
	return (s.equals("+") || s.equals("-") || s.equals("*") || s.equals("^")) ? true : false;
    }
    public static void test0(){
        beg();
	{
	    fl("buildTree 1");
	    List<String> ls = list("1");
	    Iterator<String> ite = ls.iterator();
	    GNode root = buildTree(ite);
	    fl("order");
	    List<String> list = list();
	    inorderToList(root, list);
	    List<String> expList = list("1");
	    t(list, expList);
	}
	{
	    fl("buildTree 2");
	    List<String> ls = list("x");
	    Iterator<String> ite = ls.iterator();
	    GNode root = buildTree(ite);
	    fl("order");
	    List<String> list = list();
	    inorderToList(root, list);
	    List<String> expList = list("x");
	    t(list, expList);
	}
	{
	    fl("buildTree 3");
	    List<String> ls = list("+", "1", "2");
	    Iterator<String> ite = ls.iterator();
	    GNode root = buildTree(ite);
	    List<String> list = list();
	    inorderToList(root, list);
	    List<String> expList = list("1", "+", "2");
	    t(list, expList);
	}
	{
	    fl("buildTree 4");
	    List<String> ls = list("+", "1", "x");
	    Iterator<String> ite = ls.iterator();
	    GNode root = buildTree(ite);
	    List<String> list = list();
	    inorderToList(root, list);
	    List<String> expList = list("1", "+", "x");
	    t(list, expList);
	}
	{
	    fl("buildTree 5");
	    List<String> ls = list("+", "y", "x");
	    Iterator<String> ite = ls.iterator();
	    GNode root = buildTree(ite);
	    List<String> list = list();
	    inorderToList(root, list);
	    List<String> expList = list("y", "+", "x");
	    t(list, expList);
	}
	{
	    fl("evalPostOrder 1");
	    List<String> ls = list("+", "1", "2");
	    Iterator<String> ite = ls.iterator();
	    GNode<String> node = buildTree(ite);
            GNode<String> ret = evalPostOrder(node);
	    t(ret.data, "3");

	}
//	{
//	    List<String> ls = list("+", "1", "2");
//	    Iterator<String> ite = ls.iterator();
//	    GNode node = buildTree(ite);
//	    fl("order");
//	    inorder(node);
//	}

//	{
//	    List<String> ls = list("+", "1", "2");
//	    Iterator<String> ite = ls.iterator();
//	    GNode node = buildTree(ite);
//	    fl("inorder");
//	    inorder(node);
//	    Ut.l();
//	    fl("preorder");
//	    preorder(node);
//	    fl("postorder");
//	    postorder(node);
//	    fl("evalPostOrder");
//            String s = evalPostOrder(node);
//	    pp("s=" + s);
//	}
//	{
//	    List<String> ls = list("-", "-", "1", "6", "*", "8", "+", "^", "2", "3", "-", "1", "6");
//	    Iterator<String> ite = ls.iterator();
//	    GNode node = buildTree(ite);
//	    fl("inorder");
//	    inorder(node);
//	    Ut.l();
//	    fl("preorder");
//	    preorder(node);
//	}
//	Ut.l();
//	{
//	    List<String> ls = list("-", "-", "1", "6", "*", "8", "+", "^", "2", "3", "-", "1", "6");
//	    Iterator<String> ite = ls.iterator();
//	    GNode node = buildTree(ite);
//	    fl("postorder");
//	    String s = evalPostOrder(node);
//	    pp("s=" + s);
//	    Ut.l();
//	    preorder(node);
//	}
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

