import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.AnsiColor.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// KEY: escape code, ansi code, console color
public class EscapeCodeExample{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
		{
			System.out.println(colorize(RED,   "Avocada"));
			System.out.println(colorize(GREEN, "Avocada"));
			System.out.println(colorize(CYAN,  "Avocada"));
		}
        end();
    }
    public static void test1(){
        beg();
        end();
    }
} 

