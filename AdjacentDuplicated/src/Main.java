import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static classfile.Aron.printSet;


public class Main {
    public static void main(String[] args) {
        getInput("12312323123");
        //getNum();

    }


    // read file line by line to list
    public static List<String> readFile(String fileName) {
        List<String> list = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String str;
            while((str = in.readLine()) != null) {
                list.add(str.trim());

            }
            in.close();
        } catch(IOException io) {
            io.printStackTrace();
        }
        return list;
    }

    static void getInput(String inputStr){
        //String inputStr = "12345678";
        // 1 2 3 45 6 7 8
        Integer[] list = strToDigit(inputStr);

        int k = 0;
        LinkedHashSet<Integer> set = new LinkedHashSet<Integer>();
        PriorityQueue<List<Integer>> queue = new PriorityQueue<List<Integer>>((x, y) -> compare(x, y));
        lotteryNumber(list, k, set, queue);

        while(queue.size() > 0){
            System.out.println(inputStr +  " -> " +  toStr(queue.remove()));
        }
    }
    public static int compare(List<Integer> x, List<Integer> y){
        for(int i=0; i<7; i++){
            if(x.get(i) < y.get(i)){
                return -1;
            }else if(x.get(i) == y.get(i)){

            }else{
                return 1;
            }
        }
        return 0;
    }
    public static String toStr(List<Integer> ls){
        String s = "";
        for(Integer n : ls) {
            s += n + " ";
        }
        String ss = s.trim();
        return ss;
    }
    public static Integer[] strToDigit(String s){
        Integer[] arr = new Integer[s.length()];
        for(int i=0; i<s.length(); i++){
            arr[i] = s.charAt(i) - '0';
        }
        return arr;
    }
    public static LinkedHashSet<Integer> addSet(LinkedHashSet<Integer> set, Integer n){
        LinkedHashSet<Integer> newSet = new LinkedHashSet<Integer>();
        for(Integer e : set){
            newSet.add(e);
        }
        newSet.add(n);

        return newSet;
    }

    public static void lotteryNumber(Integer[] list,  int k, LinkedHashSet<Integer> set, PriorityQueue<List<Integer>> queue) {
        if(set.size() < 7 && k < list.length) {
            if (!set.contains(list[k])) {
                lotteryNumber(list, k + 1, addSet(set, list[k]), queue);
            }

            if(k + 1 < list.length) {
                Integer twoDigits = 10*list[k] + list[k + 1];
                if(twoDigits <= 59 && !set.contains(twoDigits)){
                    lotteryNumber(list, k + 2, addSet(set, twoDigits), queue);
                }
            }

        }else{
            if(set.size() == 7 && (k  == list.length || k == list.length + 1)){
                printSet(set);
                List<Integer> ls = new ArrayList<>();
                for(Integer n : set){
                    ls.add(n);
                }
                queue.add(ls);
            }
        }
    }
    public static void getNum(){
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(num);
        for(int i=0; i<num; i++){
            String str = scan.nextLine().trim();
            getInput(str);
        }


    }
}
