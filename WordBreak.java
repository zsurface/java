import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import java.util.stream.*;
import java.util.stream.Collectors;

record intArr(int inxOfNonZero, int[] arr){};


public class WordBreak{
    public static void main(String[] args) {
        test1();
    }
    public static boolean wordBreak(Set<String> dict, String word){
        boolean ret = false;
        int len = word.length();

        for(int i = 0; i < len; i++){
            String prefix = word.substring(0, i + 1);
            String suffix = word.substring(i + 1, len);
            // pl("prefix=" + prefix);
            // pl("suffix=" + suffix);
            if(dict.contains(prefix)){
                if(suffix.length() > 0){
                    return wordBreak(dict, suffix);
                }else{
                    return true;
                }
                
            }
        }
        return ret;
    }

    public static void test1(){
        beg();
        {

            {
                fl("wordBreak 1");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                boolean ret = wordBreak(dict, "dogcat");
                t(ret, true);
            }

            {
                fl("wordBreak 2");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("ccow");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, false);
            }

            {
                fl("wordBreak 3");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("dogatcow");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, false);
            }
            {
                fl("wordBreak 4");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("cow");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, true);
            }
            {
                fl("wordBreak 5");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("cows");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, false);
            }
            {
                fl("wordBreak 7");
                Set<String> dict = new HashSet<>();
                dict.add("a");
                boolean ret = wordBreak(dict, "a");
                t(ret, true);
            }
            {
                fl("wordBreak 8");
                Set<String> dict = new HashSet<>();
                dict.add("a");
                dict.add("b");
                boolean ret = wordBreak(dict, "ab");
                t(ret, true);
            }

            {
                fl("wordBreak 9");
                Set<String> dict = new HashSet<>();
                dict.add("apple");
                dict.add("pen");
                boolean ret = wordBreak(dict, "applepenapple");
                t(ret, true);
            }
        }
		end();
    }

}
