import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.FileType;
import java.util.stream.*;
import java.util.stream.Collectors;

public class FileTypeEx{
    public static void main(String[] args) {
        test0();
        String file ="";

        file ="html";
        Test.t(FileType.isHtml(file), false);

        file =".html";
        Test.t(FileType.isHtml(file), false);

        file ="a.html";
        Test.t(FileType.isHtml(file), true);

        file ="a.htmlc";
        Test.t(FileType.isHtml(file), false);

        file ="ahtml";
        Test.t(FileType.isHtml(file), false);

        file ="HTML";
        Test.t(FileType.isHtml(file), false);

        file =".HTML";
        Test.t(FileType.isHtml(file), false);

        file ="A.HTML";
        Test.t(FileType.isHtml(file), true);

        file ="A.HTMLC";
        Test.t(FileType.isHtml(file), false);

        test1();
    }
    public static void test0(){
        Aron.beg();

        Aron.end();
    }

    public static void test1(){
        Aron.beg();
        {
            String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
            List<Integer> ls = Arrays.asList(1, 3, 3);
            Integer n = ls.stream().reduce(0, (x, y) -> x + y);
            List<Integer> lss = ls.stream().filter( x -> x < 3).collect(Collectors.toList());

            ls.forEach(x -> System.out.println("x=" + x));

            Pattern pattern = Pattern.compile("\\s+"); 

            Print.p(n);
        }
        {
            List<String> ls = Arrays.asList("dog", "cat", "pig", "Michael", "file.html", "kk.html_33", ".html", "html");
            Pattern pattern = Pattern.compile(".\\.html$");
            List<String> fls = ls.stream().filter( s -> pattern.matcher(s).find()).collect(Collectors.toList());
            Print.p(fls);
        }

        Aron.end();
    }
} 

