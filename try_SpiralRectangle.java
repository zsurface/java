import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_SpiralRectangle{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

	int[][] arr2d = {
        { 1,   2,   3,  4},
        { 5,   6,   7,  8},
        { 9,   10,  11, 12},
        { 13,  14,  15, 16},
	};
	
	int height = arr2d.length;
	int width = arr2d[0].length;
	
	p(arr2d);
	fl();
	spiralRectangle(arr2d);
	p(arr2d);

        sw.printTime();
        end();
    }
    public static void spiralRectangle(int[][] arr){
	if(arr != null && arr.length > 0){
	    int ncol = arr.length;
	    int nrow = arr[0].length;
	    for(int k=0; k<= Math.min(ncol, nrow)/2; k++){
		if(nrow - 2*k == 1){
		    for(int i=k; i<nrow - k; i++)
			p(arr[i][k]);
		   
		}else if(ncol - 2*k == 1){
		    for(int i=k; i<ncol - k; i++)
			p(arr[k][i]);

		}else{
		    for(int i=k; i<nrow - 1 - k; i++){
			p(arr[k][i]);  // ---->
		    }	    
		    for(int i=k; i<ncol - 1 - k; i++){             
		        p(arr[i][nrow - 1 - k]);
		    }
		    for(int i=k; i<nrow - 1 - k; i++){
			p(arr[ncol - 1 - k][nrow - 1 - i]);
		    }
		    for(int i=k; i<ncol - 1 - k; i++){
			p(arr[nrow - 1 - i][k]);
		    }
		}
	    }
	}
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

