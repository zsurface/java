import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// Wed  3 Jun 16:11:54 2020 
// KEY: print spiral of 2d array
public class PrintSpiralRotate{
    public static void main(String[] args) {
        test0();
        test1();
    }

    /**
         1 2 3 4     
         5 6 7 8         
          [1 2 3] [4]
          5  [6 7 8]

          1 2 3 x
          4 5 6 x
          7 8 9 x
    */
    public static void printSpiral(int[][] arr, int k){
        int height = arr.length;
        int width = arr[0].length;

        if(width - 2*k == 1){
            for(int h = k; h < height - k; h++)
                pp(arr[h][k] + " ");
        }else if(height - 2*k == 1){
            for(int w = k; w < width - k; w++)
                pp(arr[k][w] + " ");
        }else if(width - 2*k >= 0 || height - 2*k >= 0){
            for(int w = k; w < width - 1 - k; w++)
                   pp(arr[k][w] + " ");
            for(int h = k; h < height - 1 - k; h++)
                   pp(arr[h][width - 1 - k] + " ");
            for(int w = k; w < width - 1 - k; w++)
                   pp(arr[height - 1 - k][width - 1 - w] + " ");
            for(int h = k; h < height - 1 - k; h++)
                   pp(arr[height - 1 - h][k] + " ");
            printSpiral(arr, k+1);
        }
    }
    public static void test0(){
        beg();
        {
            int[][] arr2d = {
                { 1,   2,   3,  4},
                { 5,   6,   7,  8},
                { 9,   10,  11, 12},
                { 13,  14,  15, 16},
            };
            int k = 0;
            fl("arr2d");
            p(arr2d);
            fl("printSpiral");
            printSpiral(arr2d, k);
        }
        {
            int[][] arr2d = {
                { 1,   2,   3,  4},
                { 5,   6,   7,  8},
                { 9,   10,  11, 12}
            };
            int k = 0;
            fl("arr2d");
            p(arr2d);
            fl("printSpiral");
            printSpiral(arr2d, k);
        }

        {
            int[][] arr2d = {
                { 1,   2,   3,  4}
            };
            int k = 0;
            fl("arr2d");
            p(arr2d);
            fl("printSpiral");
            printSpiral(arr2d, k);
        }
        {
            int[][] arr2d = {
                { 1},
                { 2},
                { 3},
                { 4},
            };
            int k = 0;
            fl("arr2d");
            p(arr2d);
            fl("printSpiral");
            printSpiral(arr2d, k);
        }
        {
            int[][] arr2d = {
                { 1}
            };
            int k = 0;
            fl("arr2d");
            p(arr2d);
            fl("printSpiral");
            printSpiral(arr2d, k);
        }
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

