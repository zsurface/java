import java.util.*;
import java.io.*;
import classfile.*;
public class GaussianElimination{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        int[][] arr = new int[][] {
        {1, 2, 3, 7},
        {4, 5, 6, 1},
        {7, 8, 10, 3},
        {29, 9, 11, 6},
        };

        Aron.printArray2D(arr);

        int len = arr.length;
        for(int i=0; i<len-1; i++){
            for(int n=1; n+i<len; n++){
            int m00 = arr[i][i];
            int m10 = arr[i+n][i];
            for(int j=i; j<len; j++){
                arr[i][j] = arr[i][j]*m10;
                arr[i+n][j] = arr[i+n][j]*m00;
                arr[i+n][j] = arr[i][j] - arr[i+n][j];
            }
            }
        }
        Aron.printArray2D(arr);
        
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
} 

