import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;

import static classfile.Aron.*; 
import static classfile.Print.*; 
import static classfile.Test.*; 


public class TryMM{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        get();
    }
    public static void get(){
        String[] arr = {"techie", "dangling", "cat", "scene", "ancestor", "scene", "descend", "descended", "sibling", "dangling"}; 
        Print.fl("dog", arr); 
    }
    public static String repeat(int n, Character c){
        String s ="";
        for(int i=0; i<n; i++){
            s += (c + "");
        }
        return s;
    }

    // print all prefix string from a given string
    public static void test1(){
        List<String> ls = new ArrayList<>();
        String s = "abcaab";
        int len = s.length();
        for(int i=0; i<s.length(); i++){
            String prefix = s.substring(0, i+1);  // (0, 1) => ""    (0, 2) => "a"
            String suffix = s.substring(i+1, len);  // (0, 3) => "abc" (1, 3) => "bc"
            p(prefix);
            p(suffix);
            ls.add(prefix);
            ls.add(suffix);
        }
        Collections.sort(ls);
        fl("ls");
        p(ls);

    }
    public static void test3(){
        List<Integer> ls = printNPrime(10);
        fl("prime");
        p(ls);
    }

    public static void test4(){
        List<Integer> ls = printAllPrint(10);
        fl("prime");
        List<Integer> lss = Arrays.asList(2, 3, 5, 7);
        t(ls, lss); 
    }
    public static void test2(){
        List<String> ls = Arrays.asList("dog", "cat", "god", "pig");
        Map<String, List<String>> map = new HashMap<>();
        for(String s : ls){
            char[] charArr = s.toCharArray();
            Arrays.sort(charArr);
            String str = new String(charArr);
            List<String> v = map.get(str);
            if(v == null)
                v = new ArrayList<String>();
            v.add(s);
            map.put(str, v);
        }
        fl("map");
        p(map);
    }
    public static void test0(){
        Aron.beg();
        // given a list [3, 4, 2, 4] 
        // print out n characters

        Integer[] arr1 = {1, 2, 3, 4};
        
        Character[] arr = {'c', 'a', 't', 's'};
        int width = arr.length; 

        Map<Integer, Character> charMap = new HashMap<>();
        for(int i=1; i<=26; i++){
            char cc = (char)((int)'a' + (i-1));
            charMap.put(i, new Character(cc));
        }
        
        String s = "";
        for(int i=0; i<arr1.length; i++){
            s += repeat(arr1[i], charMap.get(arr1[i]));    
        }
        p(s);
        
        
        Aron.end();
    }

    public static List<Integer> printAllPrint(Integer n){
        List<Integer> list = new ArrayList<>(); 
        // n = 2, 3, 4
        if(n >= 2){
            list.add(2);
            for(int k=3; k <= n; k++){
                boolean isPrime = true;
                for(int i=0; i<list.size(); i++){
                    if(k % list.get(i) == 0){
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime)
                    list.add(k);
            }
        }
        return list;
    }
    public static List<Integer> printNPrime(Integer n){
        List<Integer> ls = new ArrayList<>();
        if(n > 0){
            ls.add(2);
            int k = 1;
            int num = 3;
            // n = 1
            // n = 2
            while(k < n){
                boolean isPrime = true;
                for(int i=0; i<ls.size(); i++){
                    if(num % ls.get(i) == 0){
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime){
                    ls.add(num);
                    k++;
                }

                num++;
            }
        }
        return ls;
    }

    // 2 -3, 4
    // 2, 0  4
    // 2, 3  4
    // 2  6  -5
    public static void printMaxContinuousSum(int[] arr){
        int m = 0;
        int curr = 0;
        for(int i=0; i<arr.length; i++){

            if(curr < 0)
                curr = 0;

            curr += arr[i];

            if(curr > m)
                m = curr;
        }
    }

} 

