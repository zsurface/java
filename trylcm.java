import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.Aron;
import classfile.BST;
import classfile.Print;

public class trylcm{
    public static void main(String[] args) {
        test0();

        test1();
    }

//        4
//      2   5
//    1   3    7
//    -----------------------------------------------------------------------------------------
//    1, 3
//
//    [4 
//       if()
//           ...
//       [2 
//            if()
//                ...
//            [1]
//            [3]
//            if ...
//       ]
//
//       [5 
//            if()
//                ...
//            [3
//            ]
//       ]
//       if...
//     ]
//     -------------------------------------------------------------------------------- 
//     1, 2
//     
//     [4
//        [2]
//        [5
//            []
//        ]

    // Find the least common ancestor or lca()
    public static Node lca(Node n1, Node n2, Node r){
        if (r != null){
            if (r.data == n1.data || r.data == n2.data){
                return r;
            }else {
                Node left = lca(n1, n2, r.left);
                Node right= lca(n1, n2, r.right);
                if (left != null && right != null){
                    return r;
                }else if(left != null && right == null){
                    return left;
                }else if(left == null && right != null){
                    return right;
                }
            }
        }
        return null;
    }

    public static void test0(){
        Aron.beg();
        BST b = Aron.createBin();
        Node n1 = new Node(9);
        Node n2 = new Node(15);
        Node n = lca(n1, n2, b.root);
        Print.pp(n.data);
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        BST b = Aron.createBin();
        Node n1 = new Node(1);
        Node n2 = new Node(5);
        Node n = lca(n1, n2, b.root);
        Print.pp(n.data);
        Aron.end();
    }
} 

