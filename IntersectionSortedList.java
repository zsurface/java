import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class IntersectionSortedList{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        {
            Integer[] a1 = {2, 2, 3};
            Integer[] a2 = {};

            List<Integer> exp = Arrays.asList(); 
            List<Integer> arr = intersect(a1, a2);

            Print.p(arr);
            Test.t(arr, exp);
        }
        {
            Integer[] a1 = {2, 2, 3};
            Integer[] a2 = {2};

            List<Integer> exp = Arrays.asList(2); 
            List<Integer> arr = intersect(a1, a2);

            Print.p(arr);
            Test.t(arr, exp);
        }
        {
            Integer[] a1 = {2, 2, 3};
            Integer[] a2 = {2, 2};

            List<Integer> exp = Arrays.asList(2); 
            List<Integer> arr = intersect(a1, a2);

            Print.p(arr);
            Test.t(arr, exp);
        }
        {
            Integer[] a1 = {2, 2, 3};
            Integer[] a2 = {2, 2, 4};

            List<Integer> exp = Arrays.asList(2); 
            List<Integer> arr = intersect(a1, a2);

            Print.p(arr);
            Test.t(arr, exp);
        }
        {
            Integer[] a1 = {2, 2, 3};
            Integer[] a2 = {2, 3};

            List<Integer> exp = Arrays.asList(2, 3); 
            List<Integer> arr = intersect(a1, a2);

            Print.p(arr);
            Test.t(arr, exp);
        }
        {
            Integer[] a1 = {1, 2, 3};
            Integer[] a2 = {1, 2, 3};

            List<Integer> exp = Arrays.asList(1, 2, 3); 
            List<Integer> arr = intersect(a1, a2);

            Print.p(arr);
            Test.t(arr, exp);
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }

    /**
        []
        2 => []

        2 
        2 => [2] 

        1
        3 => []

        2 2
        2  => [2]

        2 2 3 
        2 2 2 =>
    */
    public static List<Integer> intersect(Integer[] a1, Integer[] a2){
        List<Integer> list = new ArrayList<>();
        if(a1 != null && a2 != null){
            int len1 = a1.length;
            int len2 = a2.length;
            int i=0, j=0;
            while(i < len1 && j < len2){
                if(a1[i] < a2[j]){
                    i++;
                }else if(a1[i] > a2[j]){
                    j++;
                }else{
                    if(list.size() == 0){
                        list.add(a1[i]);
                    }else{
                        if(list.get(list.size() - 1) != a1[i])
                            list.add(a1[i]);
                    }
                    i++;
                    j++;
                }
            }
        }
        return list;
    }
} 

