import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.util.PriorityQueue;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// KEY: Amazon test, PriorityQueue, top n elements
public class optimizePair{
    public static void main(String[] args) {
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            fl();
            List<List<Integer>> forwardList = of(of(1, 5), of(2, 4), of(3, 1));
            List<List<Integer>> returnList = of(of(1, 6), of(2, 2), of(3, 2));
            p(forwardList);
            p(returnList);
            fl();
            List<List<Integer>> lss = allPair(8, forwardList, returnList);
            p(lss);
        }
        {
            fl("xx");
            List<List<Integer>> forwardList = of(of(1, 5), of(2, 4), of(3, 1));
            List<List<Integer>> returnList = of(of(1, 6), of(2, 2), of(3, 2));
            p(forwardList);
            p(returnList);
            fl();
            List<List<Integer>> lss = allTrip(8, forwardList, returnList);
            p(lss);
        }
        {
            fl("No");
            List<List<Integer>> forwardList = of(of(1, 5), of(2, 4), of(3, 1));
            List<List<Integer>> returnList = of(of(1, 6), of(2, 2), of(3, 2));
            p(forwardList);
            p(returnList);
            fl();
            List<List<Integer>> lss = allTrip(2, forwardList, returnList);
            p(lss);
        }
        {
            List<List<Integer>> lss = new ArrayList<>();
        }



        sw.printTime();
        end();
    }
    public static List<List<Integer>> allTrip(int n, List<List<Integer>> flist, List<List<Integer>> rlist) {
        class Trip{
            List<Integer> p;
            Integer n;
            public Trip(List<Integer> p, Integer n){
                this.p = p;
                this.n = n;
            }
        }

        PriorityQueue<Trip> queue = new PriorityQueue<>((a, b) -> -(a.n - b.n));
        List<List<Integer>> ret = new ArrayList<List<Integer>>();
        for(List<Integer> fl : flist){
            for(List<Integer> rl : rlist){
                Integer sum = fl.get(1) + rl.get(1);
                if(sum <= n){
                    List<Integer> pl = new ArrayList<>();
                    pl.add(fl.get(0));
                    pl.add(rl.get(0));
                    Trip p = new Trip(pl, sum);
                    queue.add(p);
                }
            }
        }


        /**
          4 4 2 1
        */

        Integer max = 0;
        while(queue.size() > 0){
            Trip t = queue.remove();
            if(ret.size() == 0){
                ret.add(t.p);
                max = t.n;
            }else{
                if(t.n == max){
                    ret.add(t.p);
                }
            }
        }
        return ret;
    }
    public static List<List<Integer>> allPair(int n, 
        List<List<Integer>> forwardList, 
        List<List<Integer>> returnList)
    {
        Map<List<Integer>, Integer> map = new HashMap<>();
        List<List<Integer>> ret = new ArrayList<List<Integer>>(); 
        Integer max = 0;
        for(List<Integer> fls : forwardList){
            for(List<Integer> rfs : returnList){
                Integer sum = fls.get(1) + rfs.get(1);
                if(sum <= n){
                    if(sum >= max){

                        List<Integer> pair = new ArrayList<>();
                        pair.add(fls.get(0));
                        pair.add(rfs.get(0));
                        if(ret.size() == 0){
                            ret.add(pair);
                        }else{
                            if(sum > max){
                                ret.clear();
                                ret.add(pair);
                            }else if(sum == max){
                                ret.add(pair);
                            }

                        }
                        max = sum;
                    }
                }
            }
        }
        return ret;
    }
} 

