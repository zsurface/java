import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_stream{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        List<String> list = of("dog");
        p(list);
        fl();

        end();
    }
    
    public static Pair<String, Integer> split(String s){
        int i=0;
        int len = s.length();
        for(i=0; i<len; i++){
            if(Character.isDigit(s.charAt(i)))
                break;
        }
        // [0..i) [i, n)
        return new Pair<String, Integer>(s.substring(0, i), Integer.parseInt(s.substring(i, len)));
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        
        {
            // immutable list
            List<String> list = Arrays.asList("cat1", "dog1", "cow1");
            Stream<String> st = list.stream();
            Object[] arr = st.toArray();
            for(Object o : arr){
                p("o=" + (String)o);
            }
        }
        {
            // immutable list
            List<String> list = Arrays.asList("cat1", "dog1", "cow1", "cow3");
            Map<String, Set<Integer>> map = new HashMap<>();
            for(String s : list){
                Pair<String, Integer> p = split(s);
                Set<Integer> set = map.get(p.getKey());
                if( set == null){
                    set = new HashSet<>();
                }
                set.add(p.getValue()); 
                map.put(p.getKey(), set);
            }
            p(map);
            fl();

            Pair<String, Integer> pp = split("dog1"); 
            Set<Integer> ss = map.get(pp.getKey());    
            if(ss != null){
                if(ss.size() == 1){
                    if(ss.contains(pp.getValue()))
                        map.remove(pp.getKey());
                }else{
                    ss.remove(pp.getValue());
                }
            }
            p(map);
            

        }
        

        end();
    }
} 

