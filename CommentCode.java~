import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;








public class CommentCode{
  public static void main(String[] args) {
    String fname = "/tmp/x.x";
    if(len(args) > 0){
      List<String> ls = readFileNoTrim(fname);
      var lss         = commentCode(args[0], ls);
      for(int i = 0; i < len(lss); i++){
        if(i < len(lss) - 1){
          System.out.println(lss.get(i));
        }else if(i == len(lss) - 1){
          System.out.print(lss.get(i));
        }
      }
    }

    test_commentCode();
    // test_uncommentCode();
  }

  public static List<String> commentCode(String lang, List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      for(int i = 0; i < len(ls); i++){
        String line = ls.get(i);
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', line);
        if(lang.equals("haskell")){
          // Line has been commented  => " -- fold"
          if(len(t.y) >= 2 && take(2, t.y).equals("--")) {
            ret.add(line);
          }else{
            ret.add(t.x + "-- " + t.y);
          }
        }
        else if(lang.equals("java")){
          if(len(t.y) >= 2 && take(2, t.y).equals("//")) {
            ret.add(line);
          }else{
            ret.add(t.x + "// " + t.y);
          }
        }
        else if(lang.equals("elisp")){
          if(len(t.y) >= 2 && take(2, t.y).equals(";;")) {
            ret.add(line);
          }else{
            ret.add(t.x + ";; " + t.y);
          }
        }
      }
    }
    return ret;
  }

  /**
                      (:)
                  (:)     1
              (:)     2   
            []   3


             "   -- foldl ((:)) [] [1, 2
   */
  public static List<String> uncommentCode(String lang, List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      for(int i = 0; i < len(ls); i++){
        String line = ls.get(i);
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', line);
        if(lang.equals("haskell")){
          Tuple<String, String> tt = splitWhileStr(x -> x == '-', t.y);
          var code = dropWhile(x -> x == ' ', tt.y);
          ret.add(t.x + code);
        } else if(lang.equals("java")){
          Tuple<String, String> tt = splitWhileStr(x -> x == '/', t.y);
          var code = dropWhile(x -> x == ' ', tt.y);
          ret.add(t.x + code);
        }
        else if(lang.equals("elisp")){
          Tuple<String, String> tt = splitWhileStr(x -> x == ';', t.y);
          var code = dropWhile(x -> x == ' ', tt.y);
          ret.add(t.x + code);
        }
      }
    }
    return ret;
  }
  public static void test_uncommentCode(){
    beg();
    {
      fl("haskell tests");
      {
        String lang = "haskell";
        List<String> ls = ll(" -- fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("-- fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("--fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("-- fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("  -- fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    {
      fl("java tests");
      {
        String lang = "java";
        List<String> ls = ll(" // fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("// fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("//fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("// fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("  // fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    {
      fl("elisp tests");
      {
        String lang = "elisp";
        List<String> ls = ll(" ;; fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";; fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";;fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";; fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll("  ;; fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    end();
  }
  public static void test_commentCode(){
    beg();
    {
      {
        List<String> rfls = ll("--");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("--");
        t(ls, expls);
      }
      {
        
        List<String> rfls = ll("  abc ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("  -- abc ");
        t(ls, expls);
      }
      
      {
        List<String> rfls = ll("abc ", "ab", "line1");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- abc ", "-- ab", "-- line1");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- ");
        t(ls, expls);
      }
      {
        // " " => " -- "
        List<String> rfls = ll(" ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll(" -- ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("-- ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("--");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("--");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("- ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- - ");
        t(ls, expls);
      }
    }
    {
      {
        List<String> rfls = ll("/ ");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// / ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("//");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("//");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("// ");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("mycode");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// mycode");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("/mycode");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// /mycode");
        t(ls, expls);
      }
    }
    {
      {
        List<String> rfls = ll(";;");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(";;");
        t(ls, expls);
      }
      {
        List<String> rfls = ll(";; ");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(";; ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("mycode");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(";; mycode");
        t(ls, expls);
      }
      {
        List<String> rfls = ll(" mycode");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(" ;; mycode");
        t(ls, expls);
      }
      {
        List<String> rfls = ll(" k;;mycode");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(" ;; k;;mycode");
        t(ls, expls);
      }
    }
    end();
  }
}

