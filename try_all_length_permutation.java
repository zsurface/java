import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_all_length_permutation{
    public static void main(String[] args) {
        test0();
        test1();
	test2();
    }
    public static void test0(){
        beg();

	String prefix = "";
	String s = "abc";
	Set<String> set = new LinkedHashSet<>();
	int k = 0;
	List<String> list = substringAllRecur(prefix, s, k, set);
	pp(list);
	// pp(set);
	
        end();
    }


    public static void test2(){
        beg();

	{
	    String s = "abc";
	    int k = 1;
	    List<String> ls = substringLenK(s, k);
	    List<String> expls = Arrays.asList("a", "b", "c");
	    t(ls, expls);
	}

	{
	    String s = "";
	    int k = 1;
	    List<String> ls = substringLenK(s, k);
	    List<String> expls = Arrays.asList();
	    t(ls, expls);
	}

	{
	    String s = "abc";
	    int k = 2;
	    List<String> ls = substringLenK(s, k);
	    List<String> expls = Arrays.asList("ab", "bc");
	    t(ls, expls);
	}

	{
	    String s = "abc";
	    int k = 3;
	    List<String> ls = substringLenK(s, k);
	    List<String> expls = Arrays.asList("abc");
	    t(ls, expls);
	}
	
        end();
    }
    public static String removeIndex(String s, int inx){
	String ret = "";
	for(int i=0; i<s.length(); i++){
	    if(i != inx){
		ret += s.charAt(i) + "";
	    }
	}
	return ret.trim();
    }
    
    // file:///private/tmp/p.svg
    public static void permutation(String prefix, String s, int k){
	if(s.length() == 0){
	    // pl(prefix);
	}else{
	    for(int i=0; i<s.length(); i++){
		pl("s=" + s);
		String rest = removeIndex(s, i);
		permutation(prefix + (s.charAt(i) + ""), rest, k+1);
	    }
	}
    }

    public static void test1(){
        beg();
	
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();

        String prefix = "";
	String s = "1234";
	int k = 0;
        permutation(prefix, s, k);


        end();
    }
    
    /** 
     *  all substring with k distinct characters
     *  s = "abc" , k = 2
     *  ["ab", "bc"]
     *
     *  s = "aba" , k = 2
     * ["ab", "ba", "aba"] 
     *
     *  s = "aaa", k = 2
     *  [""]
     *
     *  s = "aab", k = 2
     *  ["aab", "ab"]
     *
     */
    public static List<String> perfectSubstring(String s, int k){
	List<String> list = new ArrayList<>();
	Set<String> set = new LinkedHashSet<String>();
	for(int i = 1; i <= s.length(); i++){
	    for(int j=0; j < s.length(); j++){
		if(j + i <= s.length()){
		    String subStr = s.substring(j, j + i);
		    if(toCharStr(subStr).size() == k){
			list.add(subStr);
		    }
		}
	    }
	}
	return list;
    }
    public static Set<String> toCharStr(String s){
	Set<String> set = new LinkedHashSet<String>();
	for(int i = 0; i < s.length(); i++){
	    set.add(s.charAt(i) + "");
	}
	return set;
    }
} 

