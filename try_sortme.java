import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;



public class try_sortme{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();
        
        List<Integer> list = Arrays.asList(1, 2, 3, 0);
        List<Integer> ls = sort(list, (x, y) -> x - y);
        p(ls);

        List<Node> s1 = new ArrayList<>();
        s1.add(new Node(2));
        s1.add(new Node(1));
        List<Node> lnode = sort(s1, (x, y) -> -(x.data - y.data));

        p(lnode);

        int n = (int)gcd((long)3, (long)6);
        p(n);

        int n1 = (int)gcd((long)3, (long)-6);
        p(n1);

        int n2 = (int)gcd((long)-3, (long)-6);
        p(n2);

        int k = (int)lcm(3, 4);
        p(k);

        end();
    }
} 

