import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_parseRandom{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String fn = "/Library/WebServer/Documents/zsurface/randomNote.txt";
        List<String> ls = readFile(fn);
        String pat = "^\\s*[-]{10,}";

        List<ArrayList<String>> list2d = splitList(pat, ls);
        p(list2d);
        p("sz=" + list2d.size());
        

        sw.printTime();
        end();
    }
    
    
    /**
    <pre>
    {@literal
        split list according to Regex
    }
    {@code
        String fn = "/Library/WebServer/Documents/zsurface/randomNote.txt";
        List<String> ls = readFile(fn);
        String pat = "^\\s*[-]{10,}";

        List<ArrayList<String>> list2d = splitList(pat, ls);
        p(list2d);
        p("sz=" + list2d.size());
    }
    </pre>
    */ 
    public static List<ArrayList<String>> splitList(String pat, List<String> ls){
        List<ArrayList<String>> list2d = new ArrayList<ArrayList<String>>();  
        ArrayList<String> block = new ArrayList<>(); 
        for(String s : ls){
            if(!isMatched(pat, s)){
                block.add(s);
            }else{
                if(block.size() > 0){
                    list2d.add(block);
                }
                block = new ArrayList<>(); 
            }
        }
        return list2d;
    }

    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

