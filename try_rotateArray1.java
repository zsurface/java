import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_rotateArray1{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        
        int[][] arr2d = {
            { 0,   0,   0,  1},
            { 1,   1,   0,  1},
            { 0,   1,   0,  1},
            { 0,   1,   1,  0},
        };
        
        p(arr2d);
        rotateArray(arr2d);
        fl();
        p(arr2d);



        sw.printTime();
        end();
    }
    public static void rotateArray(int[][] arr){
        if(arr != null){
            int len = arr.length;
            for(int k=0; k<len/2; k++){
                for(int i=k; i<len - 1 - k; i++){
                    int tmp = arr[k][i];               // |->
                    arr[k][i] = arr[len - 1 - i][k];   //   -|
                    arr[len - 1 - i][k] = arr[len-1-k][len-1-i]; // _|
                    arr[len-1-k][len-1-i] = arr[i][len-1-k];    // |
                    arr[i][len-1-k] = tmp;
                }
            }
        }
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

