import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import classfile.Tuple;
import java.util.stream.*;
import java.util.stream.Collectors;


/**
 KEY: Joseph probelm, rotate array, killing problem, killing in circle problem
 */
public class JosephProblem{
    public static void main(String[] args) {
        // test0();
        // test1();
	
	test_deleteNode();
    }
    
    // Joseph problem, killing problem
    public static void josephProblem(){
       
    }
    /**
       k = 2
       diff = 3
       k = 0
       1 2 3 4 5
       
     */
    public static List<Integer> rotateRight(List<Integer> ls, Integer k){
	Integer rk = k % len(ls);
	List<Integer> left = new ArrayList<>();
	List<Integer> right = new ArrayList<>();
	int i = 0;
	int diff = len(ls) - rk;
	for(Integer n : ls){
	    if(i < diff)
		left.add(n);
	    else
		right.add(n);
	    i++;
	}
	right.addAll(left);
	return right;
	
    }
    public static void test0(){
        beg();
	List<Integer> ls = list(1, 2, 3, 4, 5);
        List<Integer> rls = rotateRight(ls, 2);
	List<Integer> expls = list(4, 5, 1, 2, 3);
	t(rls, expls);
        end();
    }
    public static Node maxLeftSubTree(Node root){
	Node curr = root;
	while(curr.right != null){
	    curr = curr.right;
	}
	return curr;
    }

    public static void deleteNode(Node root, Integer n){
	Node curr = root;
	Node parent = null;
	while(curr != null){
	    if(n < curr.data){
		parent = curr;
		curr = curr.left;
	    }else if(n > curr.data){
		parent = curr;
		curr = curr.right;
	    }else{
		if(parent != null){
		    // curr.right != null
		    // get the max value of right subtree
		    if(curr.right != null){
			Node maxRight = maxLeftSubTree(curr);
			fl("maxRight");
			p(maxRight.data);
			swap(curr, maxRight);
		    }
		}
		break;
	    }
	}
    }
    // abc kabc
    // abc akbc
    // abc abkc
    // abc abck
    public static void test_deleteNode(){
	beg();
	{
	    Node root = geneBinNoImage();
	    deleteNode(root, 5);
	    fl("printTree");
	    printTree(root);
	    
	}
	end();
    }
    public static Integer josephProblem(Integer k){
	Integer ret = 0;
	List<Integer> ls = range(1, k+1);

	/**
	0 1 2 3 4 
	1 2 3 4 5 
         
        0   2   4 
        1   3   5
        5   1   3

        5       3
        3       5
        3

        0 1 
        1 2

        0 1 2
        1 2 3
        
        0   2
        1   3
        3   1
	*/
	while(len(ls) > 1){
	    
	    int l = len(ls);
	    List<Tuple<Integer, Integer>> tuple = filter((t) -> t.x % 2 == 0, zip(range(0, len(ls)), ls));
	    p(ls);
	    fl("tuple");
	    p(tuple);
	    ls = map((t) -> t.y, tuple);
		
	    if(l % 2 == 1){
		ls = Aron.rotateRight(ls, 1);
	    }
	    fl("ls after rotateRight");
	    p(ls);
	}
	return head(ls);
    }
    public static void test1(){
        beg();
	{
	    fl("Joseph test case 1");
	    Integer k = josephProblem(1);
	    t(k, 1);
	}
	{
	    fl("Joseph test case 2");
	    Integer k = josephProblem(2);
	    t(k, 1);
	}
	{
	    fl("Joseph test case 3");
	    Integer k = josephProblem(3);
	    t(k, 3);
	}
	{
	    // 1 2 3 4
	    fl("Joseph test case 3");
	    Integer k = josephProblem(4);
	    t(k, 1);
	}
	{
	    // 1 2 3 4 5 => 1 3 5 => 5 1 3 => 3 5 => 3
	    fl("Joseph test case 3");
	    Integer k = josephProblem(5);
	    t(k, 3);
	}
	{
	    // 1 2 3 4 5 6 => 1 3 5 => 1 5 => 5 1 => 5
	    fl("Joseph test case 3");
	    Integer k = josephProblem(6);
	    t(k, 5);
	}
	{
	    // 1 2 3 4 5 6 7 8 9 10
	    // 1 3 5 7 9
	    // 1   5   9
	    // 9 1 5  r
	    // 9   5
	    // 5   9 r
	    // 5
	    fl("Joseph test case 3");
	    Integer k = josephProblem(10);
	    t(k, 5);
	}
	{
	    // 1 2 3 4 5 6 7 8 9 10 11
	    // 1 3 5 7 9 11   r
	    // 11 1 3 5 7 9  
	    // 11   3   7  r 
	    // 11 7   
	    // 7 11
	    // 7
	    fl("Joseph test case 3");
	    Integer k = josephProblem(11);
	    t(k, 7);
	}
        end();
    }
} 

