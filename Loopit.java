import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;



public class Loopit{
    public static void main(String[] args) {
	p(args);
	p(args.length);
	p(args[0]);
        if (args.length == 1){
            int nMin = strToInt(args[0]);
            int minutes = nMin*1000*60;
            int num = 0;
            sleep(minutes);
	    while(true){
		run("say 'Time is up'");
		sleep(2000);
		standardInput();
		break;
	    }
	    
	    
            p("minutes=" + nMin + "\n");
        }else{
            p("Need one argument such as 10 minutes");
        }
    }
} 

