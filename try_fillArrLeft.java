import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_fillArrLeft{
    public static void main(String[] args) {
        test_fillArrLeft();
	test_fillArrRight();
        test_addLongIntegerNew();
    }
    public static void test_fillArrLeft(){
        beg();
	{
	    {
		fl("fillArrLeft 1");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrLeft(arr, 1, 2);
		Integer[] expArr = listToArrayInteger(list(2, 1));
		t(expArr, farr);
	    }
	    {
		fl("fillArrLeft 2");
		Integer[] arr = listToArrayInteger(list(1, 2, 3));
		Integer[] farr = fillArrLeft(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(2, 2, 1, 2, 3));
		t(expArr, farr);
	    }
	    {
		fl("fillArrLeft 3");
		Integer[] arr = listToArrayInteger(list());
		Integer[] farr = fillArrLeft(arr, 1, 9);
		Integer[] expArr = listToArrayInteger(list(9));
		t(expArr, farr);
	    }
	    {
		fl("fillArrLeft 4");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrLeft(arr, 1, 9);
		Integer[] expArr = listToArrayInteger(list(9, 1));
		t(expArr, farr);
	    }
	}
        end();
    }
    
    public static void test_fillArrRight(){
        beg();
	{
	    {
		fl("fillArrRight 1");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrRight(arr, 1, 2);
		Integer[] expArr = listToArrayInteger(list(1, 2));
		t(expArr, farr);
	    }
	    {
		fl("fillArrRight 2");
		Integer[] arr = listToArrayInteger(list(1, 2, 3));
		Integer[] farr = fillArrRight(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(1, 2, 3, 2, 2));
		t(expArr, farr);
	    }
	    {
		fl("fillArrRight 3");
		Integer[] arr = listToArrayInteger(list());
		Integer[] farr = fillArrRight(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(2, 2));
		t(expArr, farr);
	    }
	    {
		fl("fillArrRight 4");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrRight(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(1, 2, 2));
		t(expArr, farr);
	    }
	}
        end();
    }
    public static void test_addLongIntegerNew(){
        beg();
	{
	    {
		Integer[] arr1 = array(9, 9, 9);
		Integer[] arr2 = array(9, 9);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(1, 0, 9, 8));
	    }
	    {
		Integer[] arr1 = array(0);
		Integer[] arr2 = array(0);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(0, 0));
	    }
	    {
		Integer[] arr1 = array(1);
		Integer[] arr2 = array(0);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(0, 1));
	    }
	    {
		Integer[] arr1 = array(9);
		Integer[] arr2 = array(9);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(1, 8));
	    }
	    {
		Integer[] arr1 = array(9, 9);
		Integer[] arr2 = array(9, 9);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(1, 9, 8));
	    }
	}
        end();
    }

} 

