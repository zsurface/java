import java.util.*;
import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;

/**
<pre>
{@literal
    Fri 29 May 12:30:22 2020 
    KEY: Rotate two dimensions Array 90 degrees to the right.
}
{@code
}
</pre>
*/ 
public class RotateArray4{
    public static void main(String[] args) {
        test00();
    }

    /**
    <pre>
    {@literal
        Rotate two dimensions Array 90 degrees to the right.
    }
    {@code
          1 2 3
          4 5 6
          7 8 9

          [1 2]  3
          4  5   6
          7  8   9
    }
    </pre>
    */ 
    public static void rotateArray90toTheRight(int[][] arr) {
        int len = arr.length;
        for(int k = 0; k < len/2; k++){
            for(int i = k; i< len - 1 - k; i++){
                int tmp = arr[k][i];          // -->
                arr[k][i] = arr[len - 1 - i][k];    // ^
                arr[len-1 - i][k] = arr[len - 1 - k][len - 1 - i]; // <-
                arr[len - 1 - k][len - 1 - i] = arr[i][len - 1 - k]; // <-
                arr[i][len - 1 - k] = tmp;
            }
        }
    }
    static void test00() {
        Aron.beg();

        {

        fl("test 1"); 
        int[][] arr = {
               {1, 2, 3, 4},
               {1, 2, 3, 4},
               {1, 2, 3, 4},
               {1, 2, 3, 4}
              };
              
        int[][] expArr = {
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {3, 3, 3, 3},
                {4, 4, 4, 4}
              };

         p(arr);
         fl();
         rotateArray90toTheRight(arr);
         t(arr, expArr);
         p(arr);
        }

        {
        fl("test 2"); 

        int[][] arr = {
               {1, 2},
               {1, 2},
              };
              
        int[][] expArr = {
                {1, 1},
                {2, 2},
              };

         p(arr);
         fl();
         rotateArray90toTheRight(arr);
         t(arr, expArr);
         p(arr);
        }
        {
        fl("test 3"); 

        int[][] arr = {
               {1}
              };
              
        int[][] expArr = {
                {1}
              };

         p(arr);
         fl();
         rotateArray90toTheRight(arr);
         t(arr, expArr);
         p(arr);
        }

        Aron.end();
    }

    static void test0() {
        System.out.println("\n--------------------------------------------------------------------------------");

        System.out.println("\n--------------------------------------------------------------------------------");
    }
}
