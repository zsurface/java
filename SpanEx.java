import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static java.lang.Character.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class SpanEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        end();
    }
    public static void test1(){
        beg();

        {
            String ss101 = "a12";
            Pair<String, String> p = span(x -> isLetter(x), ss101);
            t(p.getKey(), "a");
            t(p.getValue(), "12");
        }
        {
            String ss101 = "12";
            Pair<String, String> p = span(x -> isLetter(x), ss101);
            t(p.getKey(), "");
            t(p.getValue(), "12");
        }
        {
            String ss101 = "";
            Pair<String, String> p = span(x -> isLetter(x), ss101);
            t(p.getKey(), "");
            t(p.getValue(), "");
        }
        {
            String ss101 = "";
            Pair<String, String> p = span(x -> isLetter(x), ss101);
            t(p.getKey(), "");
            t(p.getValue(), "");
        }
        {
            String ss101 = "abc";
            Pair<String, String> p = span(x -> isLetter(x), ss101);
            t(p.getKey(), "abc");
            t(p.getValue(), "");
        }

        end();
    }
} 

