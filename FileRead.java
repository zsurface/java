import java.util.Vector;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Collections;
//
import java.util.logging.Level;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import java.io.*;
//
import classfile.*; 
import static classfile.Aron.*; 
import static classfile.Print.*; 

// FileInputStream example, FileOutputStream example, 
// FileInputStream, FileOutputStream, read raw byte[] from file, read binary file
// read chunk file, skip file, seek file, 
class FileRead {
    public static void main(String args[]) {
//        test0();
        test1();
//        test2();
    }
    
    static void test0(){
        beg();

        try {
            // Open the file that is the first
            // command line parameter

            String bitbucket = getEnv("b");
            String fName = bitbucket + "/" + "testfile" + "/" + "java" + "/" + "file3.txt";
            pl("fName=" + fName);
            // String fName = "/Users/cat/myfile/github/java/text/file3.txt";
            FileInputStream fstream = new FileInputStream(fName);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                System.out.println (strLine);
            }
            //Close the input stream
            in.close();
        } catch (Exception e) {
            //Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        end();
    }
    static void test1(){
        beg();
        {
            String bitbucket = getEnv("b");
            String fName = bitbucket + "/" + "testfile" + "/" + "java" + "/" + "file3.txt";
            int bufSizeByte = 100;
            List<String> list = readFileLineByte(fName, bufSizeByte); 
            printList(list);
        }
        end();
    }
    public static List<String> readFileLineByte(String fName, int bufSizeByte){
        List<String> list = new ArrayList<String>(); 
        try {
            FileInputStream fstream = new FileInputStream(fName);
            int nByte = 0;
            //Read File Line By Line
            byte[] arr = new byte[bufSizeByte];
            byte[] lineArr = new byte[bufSizeByte];
            int k=0;
            while ((nByte = fstream.read(arr)) != -1) {
                pl("nByte=" + nByte);
                String str = new String(arr);
                for(int i=0; i<nByte; i++){
                    Print.pbl("char=" + arr[i]);
                    if(arr[i] == '\n'){
                        Print.pbl("newline=" + arr[i]);
                        lineArr[k] = arr[i];
                        list.add(new String(lineArr));
                        k = 0;
                        lineArr = new byte[bufSizeByte];
                    }else{
                        lineArr[k] = arr[i];
                        k++;
                    }
                }
            }
            //Close the input stream
            fstream.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return list;
    }
    
    static void test2(){
        beg();
        String currDir = "/Users/cat/myfile/github/java";
        List<String> list = getCurrentDirs(currDir);
        printList(list);
        List<String> flist = getCurrentFiles(currDir);
        Ut.l();
        printList(flist);

        end();
    }
}
