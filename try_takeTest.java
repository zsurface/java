import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_takeTest{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        String s = "abc";
        t(take(1, s), take2(1, s)); 
        t(take(0, s), take2(0, s)); 
        end();
    }
    public static void test1(){
        beg();

        List<String> list = geneRandomStrList(1000000); 
        {
            StopWatch sw = new StopWatch();
            sw.start();

            for(String s : list){
                take2(2, s);
            }

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();

            sw.start();

            for(String s : list){
                take(2, s);
            }

            sw.printTime();
        }


        end();
    }
} 

