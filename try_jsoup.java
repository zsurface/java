import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class try_jsoup{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0 (){
        Aron.beg();
        try{
            Document doc = Jsoup.connect("http://google.com/").get();
            Elements newsHeadlines = doc.select("#mp-itn b a");
            for (Element headline : newsHeadlines) {
              Print.p(headline.attr("title") + " " + headline.absUrl("href"));
            }
        }
        catch(IOException io){
            io.printStackTrace();
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
} 


