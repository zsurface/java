import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_add_long{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {1};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {0,1};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {0};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {0,0};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {1,0, 8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {9,9};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {1,0, 8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {9};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {0,9};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {0};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {0,9};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {9};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {1,8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9, 6};
            Integer[] arr2 = {7};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {1,0,3};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9, 9};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {1,9,8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }


        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        {
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9};
            Integer[] arr = subLongInt(arr1, arr2);
            Integer[] exp = {0, 9, 0};
            Print.p(arr);
            Test.t(arr, exp);
        }
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {9};
            Integer[] arr = subLongInt(arr1, arr2);
            Integer[] exp = {0,0};
            Test.t(arr, exp);
            Print.p(arr);
        }
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {9};
            Integer[] arr = subLongInt(arr1, arr2);
            Integer[] exp = {0,-9};
            Test.t(arr, exp);
            Print.p(arr);
        }
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {0};
            Integer[] arr = subLongInt(arr1, arr2);
            Integer[] exp = {0,0};
            Test.t(arr, exp);
            Print.p(arr);
        }

        Aron.end();
    }

    /**
    <pre>
    {@literal
        add two long integer array
    }
    {@code
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9};
            Integer[] arr = subLongInt(arr1, arr2);
            Integer[] exp = {1, 0, 8};
            Test.t(arr, exp);
    }
    </pre>
    */ 
    public static Integer[] subLongInt(Integer[] n1, Integer[] n2){
        if(n1 != null && n2 != null){
            int len2 = n2.length;
            for(int i=0; i<len2; i++)
                n2[i] = -n2[i];
            return addLongInt(n1, n2); 
        }
        return null;
    }

    /**
    <pre>
    {@literal
        add two long integer array
    }
    {@code
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9};
            Integer[] arr = addLongInt(arr1, arr2);
            Integer[] exp = {1, 0, 8};
            Test.t(arr, exp);
    }
    </pre>
    */ 
    public static Integer[] addLongInt(Integer[] n1, Integer[] n2){
        Integer[] arr = null;
        if(n1 != null && n2 != null){
            int len1 = n1.length;
            int len2 = n2.length;
            int len = Math.max(len1, len2);
            arr = new Integer[len + 1];

            if(len1 > len2){
                // _ x x => 0 x x
                Integer[] n22 = new Integer[len1];
                for(int i=0; i < len1; i++){
                    int diff = len1 - len2;
                    n22[i] = i < diff ?  0 : n2[i - diff] ; 
                }
                
                int c = 0;
                for(int i=0; i<len1; i++){
                    int r = len1 - 1;
                    int s = c + (n1[r - i] + n22[r - i]); 
                    c = s / 10;
                    arr[len + 1 - 1 - i] = s % 10;
                }
                arr[0] = c;

            }else{
                Integer[] n11 = new Integer[len2];
                for(int i=0; i < len2; i++){
                    int diff = len2 - len1;
                    n11[i] = i < diff ? 0 : n1[i - diff] ; 
                }
                
                int c = 0;
                for(int i=0; i<len2; i++){
                    int r = len2 - 1;
                    int s = c + (n11[r - i] + n2[r - i]); 
                    c = s / 10;
                    arr[len + 1 - 1 - i] = s % 10;
                }
                arr[0] = c;

            }
        }
        return arr;
    }


} 

