import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class sortListWithConstrainElement{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            List<Integer> ls = Arrays.asList();
            List<Integer> expectedList = Arrays.asList();
            List<Integer> actualList = sortList(ls);
            t(expectedList, actualList);
        }
        {
            List<Integer> ls = Arrays.asList(-1);
            List<Integer> expectedList = Arrays.asList(-1);
            List<Integer> actualList = sortList(ls);
            t(expectedList, actualList);
        }
        {
            List<Integer> ls = Arrays.asList(0, -1);
            List<Integer> expectedList = Arrays.asList(-1, 0);
            List<Integer> actualList = sortList(ls);
            t(expectedList, actualList);
        }
        {
            List<Integer> ls = Arrays.asList(0, -1, 1);
            List<Integer> expectedList = Arrays.asList(-1, 0, 1);
            List<Integer> actualList = sortList(ls);
            t(expectedList, actualList);
        }
        {
            List<Integer> ls = Arrays.asList(0, 1, -1, 1);
            List<Integer> expectedList = Arrays.asList(-1, 0, 1, 1);
            List<Integer> actualList = sortList(ls);
            t(expectedList, actualList);
        }

        sw.printTime();
        end();
    }
    public static List<Integer> sortList(List<Integer> ls){
        List<Integer> ret = new ArrayList<>();
        Map<Integer, List<Integer>> map = new HashMap<>();
        if(ls != null){
            for(Integer n : ls){
                List<Integer> v = map.get(n);
                if(v == null)
                    v = new ArrayList<Integer>();
                v.add(n);
                map.put(n, v);
            }

            for(Integer e : Arrays.asList(-1, 0, 1)){
                List<Integer> lss = map.get(e);
                if(lss != null)
                    ret.addAll(lss);
            }
        }
        return ret;
    }
} 

