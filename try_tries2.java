import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class TNode{
        Map<Character, TNode> map = new HashMap<>();
        boolean isWord = true;
        public TNode(boolean isWord){
            this.isWord = isWord;
        }
}

class Tries{
    TNode root = new TNode(true);
}

public class try_tries2{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            Tries t = new Tries();
            Integer i=0;
            insert(t.root, "", i);
            Print.p("t.root=" + (contains(t.root, "", i) == true));
        }
        {
            Tries t = new Tries();
            Integer i=0;
            insert(t.root, "a", i);
            Print.p("t.root=" + (contains(t.root, "a", i) == true));
        }

        {
            Tries t = new Tries();
            Integer i=0;
            insert(t.root, "dog", i);
            Print.p("t.root=" + (contains(t.root, "do", i) == false));
        }

        {
            Tries t = new Tries();
            Integer i=0;
            insert(t.root, "dog", i);
            Print.p("t.root=" + (contains(t.root, "dog", i) == true));
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }

    public static void insert(TNode curr, String s, Integer i){
        if( s != null){
            if(i == s.length()){
                curr.isWord = true; 
            }else{
                Character c = new Character(s.charAt(i));
                TNode node = curr.map.get(c);
                if(node == null){
                   node = new TNode(false);
                   curr.map.put(c, node); 
                }
                insert(node, s, i+1);
            }
        }
    }
    public static boolean contains(TNode curr, String s, int i){
        if( i == s.length()){
            return curr.isWord == true;
        }else{
            if(curr != null){
                Character c = new Character(s.charAt(i)); 
                TNode node = curr.map.get(c);
                if(node != null){
                    return contains(node, s, i+1);
                }
            }
            return false;
        }
    }
} 

