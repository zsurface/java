import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class backwardsubstitute{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        
        Double[][] arr2d = {
            {4.0, 2.0},
            {0.0, 6.0}
        };
        Double[] b = {8.0, 2.0}; 
        Double[] x = new Double[b.length];

        Aron.printArray2D(arr2d);
        int height = arr2d.length;
        int width = arr2d[0].length;
        int h = 0, w = 0; 

        backwardSubstitute(arr2d, x, b);
        Print.p(x);

        Aron.end();
    }

    /**
        backward substitute

        a11 a12 a13 x[0] = b[0]
            a22 a23 x[1] = b[1]
                a33 x[2] = b[2]
    */
    public static void backwardSubstitute(Double[][] a, Double[] x, Double[] b){
        // check null here
        int height = a.length;
        int width = a[0].length; 
        for(int h = height - 1; h >= 0; h--){
            int s = 0;
            for(int w = width - 1; w >= h; w--){
                if(w == h){
                    x[h] = b[h]/a[h][w];
                }else{
                    s += a[h][w]*x[w];
                }
            }
        }
    }
} 

