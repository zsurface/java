import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.util.Date;
import java.io.*;
import java.util.stream.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import classfile.*;



class MyLock {
    private boolean isLocked = false;
    public synchronized boolean lock() {
        if(!isLocked) {
            isLocked = true;
            return isLocked;
        }
        return false;
    }
    public synchronized void unlock() {
        isLocked = false;
        notify();
    }
}

class ConcurrentQueue {
    Queue<Integer>queue = new LinkedList<Integer>();
    MyLock lock = new MyLock();
    public void add() {
        if(lock.lock()) {
            Random r = new Random();
			Integer n = r.nextInt(2);
			System.out.println(n);
            queue.add(n);
			lock.unlock();
        }
    }

    public void remove() {
        queue.poll();
    }
    public void print(Thread t) {
		int count = 0;
        while(!queue.isEmpty()) {
            Integer n = (Integer)queue.poll();
			if(n == 0){
				count++;
				if(count == 3){
					System.out.println(t.getName());
				}
			}else{
				count = 0;
			}
        }
    }
}

class QueueThread implements Runnable {
    ConcurrentQueue queue;
    public QueueThread(ConcurrentQueue q) {
        queue = q;
    }
    public void run() {
		for(int i = 0; i < 1000; i++){
			queue.add();
		}
    }
}

public class TestThread{
    public static void main(String[] args) {
          test1();
    }

    public static void test1() {
        ConcurrentQueue queue = new ConcurrentQueue();

        for(int i=0; i<5; i++) {
            Thread t = new Thread(new QueueThread(queue));
            t.start();
			queue.print(t);
        }


    }
}

