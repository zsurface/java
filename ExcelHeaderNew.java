import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import classfile.Ut;
import java.util.stream.*;
import java.util.stream.Collectors;


public class ExcelHeaderNew{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);

	Ut.l();

	t(3, 3);

	{
	    Ut.l();
	    String s1 = modulus26(0);
	    t(s1, "A");
	    Ut.l();
	    String s2 = modulus26(1);
	    t(s2, "B");
	    Ut.l();
	    String s3 = modulus26(2);
	    t(s3, "C");

	    String s4 = modulus26(3);
	    t(s4, "BA");
	}
	{
	    Ut.l();
	    String s = excelNum(1);
	    t(s, "A");
	    Ut.l();
	    String s1 = excelNum(2);
	    t(s1, "B");
	    String s2 = excelNum(3);
	    t(s2, "C");
	    String s3 = excelNum(4);
	    t(s3, "AA");
	    String s4 = excelNum(5);
	    t(s4, "AB");
	    String s5 = excelNum(6);
	    t(s5, "AC");
	    String s6 = excelNum(7);
	    t(s6, "BA");
	}


        sw.printTime();
        end();
    }

    /**
        A B C
        AA AB AC
        BA BB BC
        CA CB CC

        00 01 02
        10 11 12
        20 21 22


        AAA AAB
   
        0 -> A
        1 -> B
        2 -> C
       
      3 9 27
      3 + 9 = 12
      4
      12 - 3 = 9
     */
    // KEY: excel header
    public static String modulus26(int n){
	String ret = "";
	Map<Integer, String> map = new HashMap<>();
	for(int i=0; i<3; i++){
	    String s = intToChar(charToInt('A') + i) + "";
	    map.put(i, s);
	}
	pp(map);
	
	int m = 3;
	List<String> ls = new ArrayList<>();

	if ( n == 0){
	    ls = cons(map.get(n), ls);
	}else{
	    
	    while(n > 0){
		int q = n / m;
		int r = n % m;
		ls = cons(map.get(r), ls);
		n = q;
	    }
	}
	// fold monoid, id = ""
	return foldl((x, y) -> x + y, "", ls);
    }
    public static String excelNum(int num){
	int m = 3;
	String ret = "";
	int k = 1;
	int sum = 0;
	while(true){
	    if(sum + (int)Math.pow(m, k) >= num){
		int diff = 0;
		diff = num - sum;
		pp("diff=" + diff);
		ret = modulus26(diff - 1);
		pp("ret=" + ret);
		break;
	    }else{
		sum += (int)Math.pow(m, k);
	    }
	    k++;
	}
	String rep = concat("", repeat(k - ret.length(), "A"));
	pb("rep=" + rep);
	pb("ret=" + ret);

	//      3^3          3^2           3^1
	//                                 A B C
	//                   AA AB AC
	//                   BA BB BC
	//                   CA CB CC
        //      A(3^2)
	//      B(3^2)
	//      C(3^2)
	//      
	
	return rep + ret;
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();


        sw.printTime();
        end();
    }
} 

