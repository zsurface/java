import javafx.util.Pair;
import java.util.*;
import classfile.*; 
import static classfile.Aron.*; 
import static classfile.Print.*; 

// LRU cache
// least recent used cache
// lru cache  
class Node<T> {
    Node next;
    Node prev;
    String key;
    T data;
    public Node(String key, T data) {
        next = prev = null;
        this.key = key;
        this.data = data;
    }
}

class LRU {
    Node least;
    Node most;
    int maxSize;
    int count;
    Map<String, Node<String>> map = new HashMap<String, Node<String>>();
    public LRU(int size) {
        least = most = null;
        maxSize = size;
        count = 0;
    }

    Node get(String key) {
        Node<String> node = map.get(key);
        if( node != null) {
            if(node != most) {
                remove(node.key);
                append(key, node.data);
            }
        }
        return node;
    }
    void insert(String key, String data) {
        if(!map.containsKey(key)) {
            append(key, data);
        } else {
            Node node = map.get(key);
            remove(key);
            append(key, data);
        }
    }
    void remove(String key) {
        Node curr = map.get(key);
        if( curr != null) {
            Node prev = curr.prev;
            Node next = curr.next;
            if(prev == null && next == null) {
                least = most = null;
                curr.next = curr.prev = null;
            } else if(prev != null && next == null) {
                most = prev;
                most.next = null;
                curr.next = curr.prev = null;
            } else if(prev == null && next != null) {
                least = next;
                next.prev = null;
                curr.next = curr.prev = null;
            } else {
                prev.next = next;
                next.prev = prev;
                curr.next = curr.prev = null;
            }
            map.remove(key);
            count--;
        }
    }
    void append(String key, String data) {
        if(least == null && count < maxSize) {
            Node<String> node = new Node<String>(key, data);
            least = most = node;
            map.put(key, node);
            count++;
        } else {
            // least < -- < most < new Node < new Node
            if(count < maxSize) {
                Node<String> node = new Node<String>(key, data);
                most.next = node;
                node.prev = most;
                most = node;
                map.put(key, node);
                count++;
            } else {
                Node next = least.next;
                least.next = null;
                if(next != null)
                    next.prev = null;
                else
                    least = most = null;
                least = next;
                count--;
                append(key, data);
            }
        }
    }
    public List<Pair<String, String>> toList(){
        List<Pair<String, String>> list = new ArrayList<>();
        Node lt = least;
        while(lt != null){
           list.add(new Pair<String, String>(lt.key, (String)lt.data)); 
           lt = lt.next;
        }
        return list;
    }
    void print() {
        Node curr = least;
        while(curr != null) {
            System.out.println("<"+curr.key+","+curr.data+">");
            curr = curr.next;
        }
    }
}

public class LRUCache {
    public static void main(String[] args) {
        test0();
    }
    
    static void test0(){
        beg();

        {
            p("Least Recent Used Cache");
            LRU  lru = new LRU(4);
            lru.insert("key1", "mydata1");
            lru.print();
            fl();
            List<Pair<String, String>> list = lru.toList();
            p(list);
        }

//        {
//            p("Least Recent Used Cache");
//            LRU  lru = new LRU(4);
//            lru.insert("key1", "mydata1");
//            lru.insert("key2", "mydata2");
//            lru.insert("key3", "mydata3");
//            lru.insert("key4", "mydata4");
//            lru.insert("key5", "mydata5");
//            lru.get("key2");
//            lru.insert("key3", "mydatakey3");
//            lru.print();
//        }

        end();
    }
    static void test1(){
        beg();

        end();
    }
    static void test2(){
        beg();
        end();
    } 
}
