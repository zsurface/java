
import classfile.Print;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.t;

import java.util.*;
import java.util.List;
//


public class Main {
    public static void main(String[] args) {
        pp("Hello World");
        test0();
        test1();
        test2();
        test3();
        test4();
    }

    public static void test4(){
        beg();
        {
            int[] arr = {1, 2, 3, 4};
            Print.p(arr);
            int n = 3;
            Print.pb(n, '<');
            Print.pb(n, '{');
            Print.pb(n, '(');
        }
        end();
    }


    public static int[] solution4(int N) {
        int[] entries = new int[N];
        int i = 1;
        int j = 1;

        entries[0] = (1 == N) ? 0: (N - 1) * (N - 1);

        while (i < N) {
            entries[i++] = -j;
            j += 2;
        }
        return entries;
    }


    /**
     *  A B C   D E  F  G    H  J K
     *
     *  1A, 2B, 1C
     *
     *
     *
     * @param N
     * @param S
     * @return
     */
    public static int solution3(int N, String S) {
        List<String> list = Arrays.asList(S.split("\\s+"));
        Map<Integer, Set<String>> map = new HashMap<>();
        for(String s : list) {
            if(s.trim().length() > 0) {
                Integer r = Integer.parseInt(s.charAt(0) + "");
                if (s.charAt(1) != 'A' && s.charAt(1) != 'K') {
                    Set<String> set = map.get(r);
                    if (set == null) {
                        set = new HashSet<>();
                    }
                    set.add(s);
                    map.put(r, set);
                }
            }
        }

        int sum = 0;
        for(int i=1; i<=N; i++){
            Set<String> set = map.get(i);
            if(set == null){
                sum += 2;
            }else{
                List<String> ls = new ArrayList<>();
                for(String s : set){
                    ls.add(s);
                }
                Collections.sort(ls, (x, y) -> x.charAt(1) - y.charAt(1));
                int n = countRow(ls);
                sum += n;
            }
        }
        return sum;
    }

    /**
     * input sorted list
     * @param list
     * @return
     */
    public static int countRow(List<String> list){
        if(list.size() == 0){
            return 2;
        }else if (list.size() == 1) {
            return 1;
        }else if(2 <= list.size()  && list.size() <= 4){
            String leftSeat = list.get(0).charAt(1) + "";
            String rightSeat = list.get(list.size() - 1).charAt(1) + "";
            if( (rightSeat.compareTo("E") == -1 || rightSeat.compareTo("E") == 0) ||
                (leftSeat.compareTo("F") == 1  || leftSeat.compareTo("F") == 0)
            )
            {
                return 1;
            }
        }
        return 0;
    }
//    public static int countRow(List<String> list){
//        if(list.size() == 0){
//            return 2;
//        }else if (list.size() == 1) {
//            return 1;
//        }else if(2 <= list.size()  && list.size() <= 4){
//            String leftSeat = list.get(0).charAt(1) + "";
//            String rightSeat = list.get(list.size() - 1).charAt(1) + "";
//            if( (rightSeat.compareTo("F") == -1 || rightSeat.compareTo("F") == 0) ||
//                    (leftSeat.compareTo("G") == 1  || leftSeat.compareTo("G") == 0)
//            )
//            {
//                return 1;
//            }
//        }
//        return 0;
//    }

    public static List<String> split(String str, String pattern){
        String[] array = str.split(pattern);
        List<String> list = new ArrayList<String>();
        for(String s : array){
            if(s.trim().length() > 0)
                list.add(s);
        }
        return list;
    }


    public static String solution2(String S) {
        if(S != null) {
            List<String> list = new ArrayList<>();
            for (int i = 0; i < S.length(); i++) {
                list.add(removeIndexStr(S, i));
            }
            Collections.sort(list);
            if(list.size() > 0)
            return list.get(0);
            else
                throw new IllegalArgumentException("Invalid argument.");
        }else{
            throw new IllegalArgumentException("Argument can not be null.");
        }
    }

    public static String removeIndexStr(String s, int index) {
        StringBuilder sb = new StringBuilder(s);
        sb.deleteCharAt(index);
        return sb.toString();
    }



    public static int solution(int[] A) {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        for(int i=0; i<A.length; i++){
            if(A[i] > 0){
                list.add(A[i]);
            }
        }
        Collections.sort(list);
        int sm = list.get(list.size() - 1) + 1;
        for(int i=0; i<list.size() - 1; i++){
            int diff = list.get(i+1) - list.get(i);
            if(diff > 1){
                sm = list.get(i) + 1;
                break;
            }
        }
        return sm;
    }
    static void test0(){
        beg();
        {
            int[] arr = {1, 2, 3, 4};
            int small = solution(arr);
            t(small, 5);
        }
        {
            int[] arr = {1, 3, 4};
            int small = solution(arr);
            t(small, 2);
        }
        {
            int[] arr = {2, 3, 4};
            int small = solution(arr);
            t(small, 1);
        }

        end();
    }
    static void test1(){
        beg();
        {
            int n = solution3(1, "1A");
            t(n, 2);
        }
        {
            int n = solution3(1, "1C");
            t(n, 1);
        }
        {
            int n = solution3(1, "1C 1E");
            t(n, 1);
        }
        {
            int n = solution3(1, "1C 1E");
            t(n, 1);
        }
        {
            int n = solution3(1, "1C 1E 1F 1G");
            t(n, 0);
        }
        {
            int n = solution3(1, "1A 1B 1C 1E");
            t(n, 1);
        }
        {
           int n = solution3(1, "1A 1B 1C 1E 1F 1G");
            t(n, 0);
        }
        {
            int n = solution3(1, "1A 1G");
            t(n, 1);
        }
        {
            int n = solution3(1, "1A 1F");
            t(n, 1);
        }
        {
            int n = solution3(1, "1G 1H 1J 1K");
            t(n, 1);
        }
        {
            int n = solution3(1, "1G 1H 1J 1K 1L");
            t(n, 1);
        }
        {
            int n = solution3(1, "1G 1K");
            t(n, 1);
        }
        {
            int n = solution3(1, "1F 1G");
            t(n, 1);
        }
        end();
    }
    public static void test2(){
        beg();

        {
            int n = solution3(2, "1E 1F");
            t(n, 2);
        }
        {
            int n = solution3(3, "1E 1F");
            t(n, 4);
        }
        {
            int n = solution3(3, "1F");
            t(n, 5);
        }
        {
            int n = solution3(3, "1A, 1K, 1F");
            t(n, 5);
        }
        {
            // 1 => 1
            // 2 => 0
            // 3 = 2
            // 4 = 2
            int n = solution3(4, "1A, 1K, 1F, 2E, 2F, ");
            t(n, 5);
        }
        {
            // 1 => 1
           // 2 => 1
            int n = solution3(2, "1A 1K 1F 2F 2G 2H 2J 2K ");
            t(n, 2);
        }
        end();
    }

    public static void test3() {
        beg();
        {
            int[] arr = uniqueNInteger2(1);
            for(int i=0; i<arr.length; i++){
                pb(arr[i]);
            }

        }
        line();
        {
            int[] arr = uniqueNInteger2(2);
            for(int i=0; i<arr.length; i++){
                pb(arr[i]);
            }
        }
        line();
        {
            int[] arr = uniqueNInteger2(3);
            for(int i=0; i<arr.length; i++){
                pb(arr[i]);
            }
        }
        end();
    }
        /**
         * n = 1 => 0
         * n = 2 => -1, 1
         * n = 3 => 3/2 = 1 => -1, 1, 0
         * n = 4 => 4/2 = 2 => -1, 1, -2, 2
         * n = 5 => 5/2 = 2 => -1, 1, -2, 2, 0
         * @param n
         * @return
         */
    public static int[] uniqueNInteger(int N){
        List<Integer> list = new ArrayList<>();
        int[] arr = new int[N];
        if(N % 2 == 0){
            for(int i=1; i <= N/2; i++){
                list.add(i);
                list.add(-i);
            }
        }else{
            for(int i=1; i <= N/2; i++){
                list.add(i);
                list.add(-i);
            }
            list.add(0);
        }

//        pp("list - 1\n");
//        pp(list);
//        pp("list - 2\n");


        int[] array = new int[N];
        int k = 0;
        for(Integer n : list){
            arr[k] = list.get(k);
            k++;
        }
        return arr;
    }
    public static int[] uniqueNInteger2(int N){
        int[] arr = new int[N];
        if(N % 2 == 0){
            int k = 0;
            for(int i=1; i <= N/2; i++){
                arr[k] = i;
                k++;
                arr[k] = -i;
                k++;
            }
        }else{
            int k = 0;
            for(int i=1; i <= N/2; i++){
                arr[k] = i;
                k++;
                arr[k] = -i;
                k++;
            }
            arr[k] = 0;
        }


        return arr;
    }
}
