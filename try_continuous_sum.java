import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

interface A<T>{
}

enum MyType implements A<String>{
}


public class try_continuous_sum{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            int[] arr = {1, -2, 3, 4};
            int width = arr.length;
            int max = continuousSum(arr);
            Test.t(max == 7); 
        }
        {
            int[] arr = {1};
            int width = arr.length;
            int max = continuousSum(arr);
            Test.t(max == 1); 
        }
        { 
            int[] arr = {1, 2, 3, 4};
            int width = arr.length;
            int max = continuousSum(arr);
            Test.t(max == 10); 
        }
        { 
            int[] arr = {1, -2, -3, 20};
            int width = arr.length;
            int max = continuousSum(arr);
            Test.t(max == 20); 
        }
        { 
            int[] arr = {-1, -2, -3, -20};
            int width = arr.length;
            int max = continuousSum(arr);
            Test.t(max == 0); 
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    // -1, 2, -1, 4 
    public static int continuousSum(int[] arr){
        int max = 0;
        if(arr != null){
            int len = arr.length;
            int cur = 0;
            for(int i=0; i<len; i++){
                if(cur < 0)
                    cur = 0;

                cur = cur + arr[i];
                // compare: [curr value, curr accumulator, max] 
                max = Math.max(max, arr[i]);
                max = Math.max(max, cur);
            }
        }
        return max;
    }
} 

