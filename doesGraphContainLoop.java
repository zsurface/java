import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class doesGraphContainLoop{
    public static void main(String[] args) {
        test0();
        test1();
    }
    /*
        1 -> 1
        1 -> 2
        2 -> 1

        set.add(curr)

        1 -> {3, 2}
        2 -> 1
        3 -> x

        Set: 1
               2
                 1



    */
    public static boolean containLoop(Node curr, Map<Node, List<Node>> map, Set<Node> set){
        if(set.contains(curr)){
            return true;
        }else{
            set.add(curr);
            List<Node> list = map.get(curr); 
            if( list != null){
                boolean bo = false;
                for(Node node : list){
                    bo = containLoop(node, map, set);
                    if(bo)
                        return bo;
                }
            }
            set.remove(curr);
        }
        return false;
    }
    public static void test0(){
        beg();
        {
            fl("t1");
            Map<Node, List<Node>> map = new HashMap<>(); 
            List<Node> ls = list();
            ls.add(new Node(1));
            map.put(new Node(1), ls);
            Node curr = new Node(1);
            Set<Node> set = new HashSet<>();
            boolean bo = containLoop(curr, map, set);
            t(bo, true);
        }
        {
            fl("t2");
            Map<Node, List<Node>> map = new HashMap<>(); 

            List<Node> ls = list();
            ls.add(new Node(2));
            map.put(new Node(1), ls);
            Node curr = new Node(1);
            Set<Node> set = new HashSet<>();
            boolean bo = containLoop(curr, map, set);
            t(bo, false);
        }
        {
            /*
                1 -> {2, 3} 

             */
            fl("t3");
            Map<Node, List<Node>> map = new HashMap<>(); 
            List<Node> ls = list();
            ls.add(new Node(2));
            ls.add(new Node(3));
            Node curr = new Node(1);
            map.put(curr, ls);
            Set<Node> set = new HashSet<>();
            boolean bo = containLoop(curr, map, set);
            t(bo, false);
        }
        {
            /*
                1 -> {2, 3} 
                2 -> {3}

             */
            fl("t4");
            Map<Node, List<Node>> map = new HashMap<>(); 
            List<Node> ls = list();
            Node node2 = new Node(2);
            Node node3 = new Node(3);
            ls.add(node2);
            ls.add(node3);
            List<Node> ls2 = list();
            ls2.add(node3);
            Node node1 = new Node(1);
            map.put(node1 , ls);
            map.put(node2, ls2);
            Set<Node> set = new HashSet<>();
            boolean bo = containLoop(node1, map, set);
            t(bo, false);
        }
        {
            /*
                1 -> {2, 3} 
                2 -> {3, 1}

             */
            fl("t5");
            Map<Node, List<Node>> map = new HashMap<>(); 
            List<Node> ls = list();
            Node node2 = new Node(2);
            Node node3 = new Node(3);
            ls.add(node2);
            ls.add(node3);
            Node node1 = new Node(1);
            List<Node> ls2 = list();
            ls2.add(node3);
            ls2.add(node1);
            map.put(node1 , ls);
            map.put(node2, ls2);
            Set<Node> set = new HashSet<>();
            boolean bo = containLoop(node1, map, set);
            t(bo, true);
        }
        {
            /*
                1 -> {2, 3} 
                2 -> {3, 4}
                3 -> {1}
            */
            fl("t6");
            Map<Node, List<Node>> map = new HashMap<>(); 
            List<Node> ls = list();
            Node node2 = new Node(2);
            Node node3 = new Node(3);
            ls.add(node2);
            ls.add(node3);
            Node node1 = new Node(1);
            Node node4 = new Node(4);
            List<Node> ls2 = list();
            ls2.add(node3);
            ls2.add(node4);

            List<Node> ls3 = list();
            ls3.add(node1);
            map.put(node1 , ls);
            map.put(node2, ls2);
            map.put(node3, ls3);
            Set<Node> set = new HashSet<>();
            boolean bo = containLoop(node1, map, set);
            t(bo, true);
        }
        end();
    }
    public static Map<Integer, List<Integer>>  geneGraphFromTo(Integer key, Integer[] arr){
        Map<Integer, List<Integer>> map = new HashMap<>();
        List<Integer> ls = Arrays.asList(arr);
        map.put(key, ls);
        return map; 
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

