import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import java.util.stream.*;
import java.util.stream.Collectors;


public class SetMatrixZero{
    public static void main(String[] args) {
        test1();
    }

    public static int[][] setMatrix(int[][] arr){
        int height = arr.length;
        int width = arr[0].length; 

        List<Tuple<Integer, Integer>> ls = new ArrayList<>();
        Set<Integer> setRow = new HashSet<>();
        Set<Integer> setCol = new HashSet<>();

        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(arr[i][j] == 0){
                    setCol.add(j);
                    setRow.add(i);
                }
            }
        }
        for(Integer r : setRow){
            for(int i = 0; i < width; i++){
                arr[r][i] = 0;
            }
        }

        for(Integer c : setCol){
            for(int i = 0; i < height; i++){
                arr[i][c] = 0;
            }
        }
        return null;
    }


    public static void test1(){
        beg();
        {

            {
				fl("setMatrix");
                int[][] arr = {
                    { 0,   1,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   1,   1,  1}
                };

                setMatrix(arr);
                printArr2d(arr);

            } 
            {
				fl("setMatrix");
                int[][] arr = {
                    { 0,   0,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   1,   1,  1}
                };

                setMatrix(arr);
                printArr2d(arr);

            } 

            {
				fl("setMatrix");
                int[][] arr = {
                    { 0,   0,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   0,   1,  1},
                    { 1,   1,   1,  1}
                };

                setMatrix(arr);
                printArr2d(arr);

            } 

            {
				fl("setMatrix");
                int[][] arr = {
                    { 0,   0,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   0,   1,  1},
                    { 1,   1,   1,  1}
                };

                setMatrix(arr);
                printArr2d(arr);

            } 

            {
				fl("setMatrix 1");
                int[][] arr = {
                    { 0,   0,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   0,   1,  1},
                    { 1,   1,   1,  0}
                };

                setMatrix(arr);
                printArr2d(arr);

            } 

            {
				fl("setMatrix 2");
                int[][] arr = {
                    { 0,   0,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   0,   1,  1},
                    { 1,   1,   1,  1},
                    { 1,   1,   1,  1}
                };

                setMatrix(arr);
                printArr2d(arr);

            } 
        }
	    end();
    }
}
