import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Node {
    Node prev;
    Node next;
    int data;
    public Node(int data) {
        this.data = data;
    }
}
class LinkedList {
    Node head;
    public LinkedList() {
    }
    public void append(Node n) {
        if(head == null)
            head = n;
        else {
            Node curr = head;
            while(curr.next != null) {
                curr = curr.next;
            }
            curr.next = n;
        }
    }
    public void addFront(Node nnode, Node n) {
        if(head == null)
            head = n;
        else {
            Node curr = head;
            while(curr != null && curr.data != n.data) {
                curr = curr.next;
            }
            if(curr != null) {
                // curr is Not the first node
                if(curr.prev != null) {
                    curr.prev.next = nnode;
                    nnode.next = curr;
                } else {
                    // curr is first node
                    head = nnode;
                    head.next = curr;
                }
            }
        }
    }
    public void remove(Node n) {
        Node curr = head;
        while(curr != null) {
            if(curr.data == n.data) {
                // first node is n
                if(curr.prev == null) {
                    head = curr.next;
                    break;
                } else if(curr.next == null) {
                    // last node is n
                    curr.prev.next = null;
                    break;
                } else {
                    // n has prev and next node
                    Node prev = curr.prev;
                    Node next = curr.next;
                    prev.next = next;
                    break;
                }
            }
            curr = curr.next;
        }
    }
    public List<Node> toList() {
        List<Node> ls = new ArrayList<>();
        Node curr = head;
        while(curr != null) {
            ls.add(curr);
            curr = curr.next;
        }
    }
    public void print() {
        Node curr = head;
        while(curr != null) {
            p(curr.data);
            curr = curr.next;
        }
    }
}

// Implement insection in single linked list
public class try_LinkedList2 {
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        LinkedList ll = new LinkedList();
        fl("append 1 2 4 9");
        ll.append(new Node(1));
        ll.append(new Node(2));
        ll.append(new Node(4));
        ll.append(new Node(9));
        fl("print");
        ll.print();
        fl("remove 1");
        ll.remove(new Node(1));
        ll.print();
        fl("addFront 4 20");
        ll.addFront(new Node(20), new Node(4));
        ll.print();
        fl("remove 20");
        ll.remove(new Node(20));
        ll.print();
        fl("remove 4 9");
        ll.remove(new Node(4));
        ll.remove(new Node(9));
        ll.print();

        sw.printTime();
        end();
    }
    public static void test1() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();


        sw.printTime();
        end();
    }
}

