import classfile.Ut;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static classfile.Aron.*;
import static classfile.Print.pp;
import static classfile.Test.t;
import static java.util.Collections.copy;

public class Main{
    public static void main(String[] args) {
        // test0();
        test1();
    }

    public static <T> List<T> append(List<T> ls, T t){
        List<T> retList = new ArrayList<>();
        copy(retList, ls);
        retList.add(t);
        return retList;
    }
    public static <T> Set<T> append(Set<T> set, T t){
        Set<T> newSet = set.stream().collect(Collectors.toSet());
        newSet.add(t);
        return newSet;
    }

    public static <T> Set<T> concatSets(Set<T> s1, Set<T> s2){
        Set<T> result = Stream.concat(s1.stream(), s2.stream())
                .collect(Collectors.toSet());
        return result;
    }

    public static void test0(){
        beg();
        {


        }
        {
            String s1 = "dog";
            String s2 = "dogs";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "";
            String s2 = "";
            t(diffOneChar(s1, s2), false);
        }
        {
            String s1 = "";
            String s2 = "a";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "abc";
            String s2 = "abec";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "abc";
            String s2 = "aebc";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "abc";
            String s2 = "aabc";
            t(diffOneChar(s1, s2), true);
        }
        {
            String s1 = "aaa";
            String s2 = "aaaa";
            t(diffOneChar(s1, s2), true);
        }

        end();
    }

    public static Set<String> copySet(Set<String> set){
        Set<String> retSet = new HashSet<>();
        for(String s : set){
            retSet.add(s);
        }
        return retSet;
    }

    /**
     <pre>
     {@literal
     longest chain of words from a dictionary

      + FaceBook interview question

      + The problem is similar to maximum or minimum numbers of coins to sum to \[ $S$ \]

      + http://localhost/html/indexCoinChangeDynamicProgramming.html
     }
     {@code
     }
     {@link #concat(List<T> list1, List<T> list2)}

     </pre>
     */
    public static void longestChainOfWords(Set<String> set, String s1, List<String> ls, List<List<String>> lss){
        String foundStr = null;
        for(String s2 : set){
            // s2 > s1
            boolean b = diffOneChar(s1, s2);
            if(b){
                Set<String> newSet = copySet(set);
                // pb(s2);
                List<String> mylist = append(ls, s2);
                if(lss.size() > 0){
                    if(lss.get(0).size() < mylist.size()){
                        lss.clear();
                        lss.add(mylist);
                    }
                }else{
                    lss.add(mylist);
                }
                newSet.remove(s2);
                longestChainOfWords(newSet, s2, mylist, lss);
            }
        }
    }
    public static void test1(){
        beg();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<>(Arrays.asList("ab"));
            longestChainOfWords(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<>(Arrays.asList("ab", "ac"));
            longestChainOfWords(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<>(Arrays.asList("ab", "ac", "acc", "kab"));
            longestChainOfWords(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<>(Arrays.asList("ab", "ac", "acc", "kab", "kkk", "kkb", "akcc"));
            longestChainOfWords(set1, s1, ls, lss);
            pp(lss);
        }
        Ut.l();
        {
            List<String> ls = new ArrayList<>();
            List<List<String>> lss = new ArrayList<>();
            String s1 = "a";
            Set<String> set1 = new HashSet<>(Arrays.asList("ab", "ac", "acc", "kab", "kkk", "kkb", "akcc", "bb", "cc", "eakcc", "akcfc"));
            longestChainOfWords(set1, s1, ls, lss);
            pp(lss);
        }
        end();
    }
}

