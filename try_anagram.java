import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.util.Arrays;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_anagram{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        ArrayList<String> list = new ArrayList<String>(); 
        list.add("dog");
        list.add("cat");
        list.add("odg");
        list.add("cats");
        list.add("space");
        list.add("spacex");
        list.add("Elon Musk");
        Print.p(findAnagram("dog", list));

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }

    /**
    <pre>
    {@literal
        Given a list of strings and a word, find all the strings are anagram of the word 

        1. Take each words from the list 
        2. Sort the word
        4. Insert each string as a key to a Hashmap
            If the key exist, then append the string to the list
        5.  Else Add the key and a list with the string in the map
        6. After the map is built
        7. Sort the word as key 
            If the map contains the sorted word, return the list 
    }
    {@code
        no code
    }
    </pre>
    */ 
    public static ArrayList<String> findAnagram(String word, ArrayList<String> list){
        Map<String, ArrayList<String>> map = new HashMap<>();

        for(String s : list){
            String ss = Aron.sortStr(s); 
            List<String> ls = map.get(ss);
            if(ls != null){
                ls.add(s);
            }else{
                ArrayList<String> li = new ArrayList<String>();
                li.add(s);
                map.put(ss, li);
            }
        }
        String skey = Aron.sortStr(word); 
        return map.get(skey);
    }
} 

