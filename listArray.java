import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class listArray{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    public static <T> void printList(List<T> list){
        for(T n : list) {
            System.out.print("[" + n + "]");
        }
    }
    public static <T> void printList(List<T[]> list){
        for(T[] arr: list) {
            for(T n : arr){
                System.out.print("[" + n + "]");
            }
        }
    }
} 

