import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class PrefixMapCommandLine{
    public static void main(String[] args) {
        test0();
        // test1();
    }
    public static void test0(){
        beg();
		String spc = repeatToStr(10, ' ');
        String home = getEnv("b");
        String path = home + "/vim/myword.utf-8.add";
		List<String> fls = readFile(path);
		// Read word file and generate a set of prefix substrings
		Map<String, Set<String>> map = prefixMapSet(fls);
 		pp(spc + "prefixMapSet Size = " + map.size());
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
			String line = null;
			System.out.println();
			moveCursorX(10);
			String str = "|";
			
			System.out.print("\n\n\n\n\n\n");
			System.out.print("\u001b[4A");
			System.out.print("\u001b[10C");
			System.out.print(str);

			boolean done = false;
			// Read user input from stdin
			while((line = br.readLine()) != null || done != true){
				String s = trim(lower(line));
				if (s.equals(":q") || s.equals(":quit")){
					break;
				}else{
					Set<String> tmpSet = map.get(s);
					if(tmpSet != null){
						List<String> tmpls = setToList(tmpSet);
						List<String> ls2 = tmpls.stream().map(x -> spc + x).collect(Collectors.toList());
						pl(ls2);
					}else{
						pp("tmpSet is null");
					}
				
					System.out.print("\n\n\n\n\n\n");
					System.out.print("\u001b[4A");
					System.out.print("\u001b[10C");
					System.out.print(str);					
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
        end();
    }

	public static void moveCursorXY(int x, int y){
		System.out.print("\u001b[" + toStr(x) + ";" + toStr(y) + "H");
	}
	
	public static void moveCursorX(int x){
		System.out.print("\u001b[" + toStr(x) + "C");
	}
	
	public static void moveCursorY(int n){
		System.out.print("\u001b[" + toStr(n));
	}
	public static <T> List<T> unique(List<T> ls){
		Set<T> set = new HashSet<>();
		for(T s : ls){
			set.add(s);
		}
		return new ArrayList<T>(set);
	}
	
    public static void test1(){
        beg();
        end();
    }
} 

