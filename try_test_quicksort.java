import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_test_quicksort{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        end();
    }
    public static void test1(){
        beg();
        int max = 300000;
        List<Integer> list = geneRandomIntFromTo(1, 100, max); 
//        {
//            StopWatch sw = new StopWatch();
//
//            sw.start();
//            List<Integer> ls = quickSort(list);
//
//            sw.printTime();
//        }
        {
            int lo = 0;
            int hi = list.size() - 1;
            Integer[] arr = list.stream().toArray(Integer[]::new);

            StopWatch sw = new StopWatch();
            sw.start();
            quickSort(arr, lo, hi);

            sw.printTime();
        }

        end();
    }
} 

