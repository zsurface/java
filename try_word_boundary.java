import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_word_boundary{
    public static void main(String[] args) {
        test0();
        test1();
    }
    
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String s = "My dog eats my cat";
        p(splitAt(2, s));

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        {
            // KEY: word boundary 
            String reg = "\\bdog\\b"; // word boundary \b word \b
            String str = "dog cow pig mydog";
            String out = str.replaceAll(reg, "X"); 
            Print.p(str + " [" + reg + "] " + " => " + out);
        }

        end();
    }
} 

