import java.util.ArrayList;
import java.util.*;
import java.util.HashMap;
import java.util.Map;
import static classfile.Aron.*;
import static classfile.Print.*;

public class Solution1 {
        public static void main(String[] args) {
            ArrayList<Integer> ls = new ArrayList<>();
            ls.add(1);
            ls.add(20);
            ls.add(25);
            ls.add(35);
            ls.add(15);
            ls.add(45);
            ls.add(60);
            ArrayList<Integer> mypair = IDsOfPackages(90, ls);
            p(mypair);

        }
        public static ArrayList<Integer> IDsOfPackages(int truckSpace, ArrayList<Integer> packagesSpace) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int i = 0; i < packagesSpace.size(); i++) {
                map.put(packagesSpace.get(i), i);
            }

            ArrayList<Integer> ans = new ArrayList<>();

            int sum = truckSpace - 30;
            for (int i = 0; i < packagesSpace.size(); i++) {
                int item1 = packagesSpace.get(i);
                if (map.containsKey(sum - item1)) {
                    ans.add(i);
                    ans.add(map.get(sum - item1));
                    return ans;
                }
            }
            return ans;
    }
}

