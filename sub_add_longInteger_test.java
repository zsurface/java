import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class sub_add_longInteger_test{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {1};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {0,1};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {0};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {0,0};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {1,0, 8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {9,9};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {1,0, 8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {9};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {0,9};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {0};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {0,9};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {9};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {1,8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9, 6};
            Integer[] arr2 = {7};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {1,0,3};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }
        Aron.beg();
        {
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9, 9};
            Integer[] arr = Aron.addLongInt(arr1, arr2);
            Integer[] exp = {1,9,8};
            Test.t(arr, exp);
            Print.p(arr);
            Print.p("----");
        }


        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        {
            Integer[] arr1 = {9, 9};
            Integer[] arr2 = {9};
            Integer[] arr = Aron.subLongInt(arr1, arr2);
            Integer[] exp = {0, 9, 0};
            Print.p(arr);
            Test.t(arr, exp);
        }
        {
            Integer[] arr1 = {9};
            Integer[] arr2 = {9};
            Integer[] arr = Aron.subLongInt(arr1, arr2);
            Integer[] exp = {0,0};
            Test.t(arr, exp);
            Print.p(arr);
        }
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {9};
            Integer[] arr = Aron.subLongInt(arr1, arr2);
            Integer[] exp = {0,-9};
            Test.t(arr, exp);
            Print.p(arr);
        }
        {
            Integer[] arr1 = {1};
            Integer[] arr2 = {8,9};
            Integer[] arr = Aron.subLongInt(arr1, arr2);
            Integer[] exp = {0,-8,-8};
            Test.t(arr, exp);
            Print.p(arr);
        }
        {
            Integer[] arr1 = {0};
            Integer[] arr2 = {0};
            Integer[] arr = Aron.subLongInt(arr1, arr2);
            Integer[] exp = {0,0};
            Test.t(arr, exp);
            Print.p(arr);
        }

        Aron.end();
    }
} 

