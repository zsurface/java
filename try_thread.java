import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class FirstThread implements Runnable{
    public void run(){
        Print.p("thread name=" + Thread.currentThread().getName());
        try{
            for(int i=0; i<4; i++){
                Thread.sleep(3000);
                Print.p("i=" + i);
            }
        }
        catch(InterruptedException e){
        }
    }
}

public class try_thread{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        for(int i=0; i<4; i++){
            Thread t1 = new Thread(new FirstThread());
            t1.start();
        }
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
} 

