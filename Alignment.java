import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;




 // x  = 3
 //  xy = 4


/**
     <pre>
     {@literal
     KEY: alignment lines
        
     NOTE:
     }
     {@code
     
         xⁿ + yⁿ = zⁿ + wⁿ
           x² + y² = z²
             ⇓
         xⁿ + yⁿ = zⁿ + wⁿ
         x² + y² = z²

	 alignmentNonSPC("=", ls) use "=" as delimiter
	 alignmentNonSPC("", ls)  use space as delimiter
     }

     Tuesday, 16 November 2021 10:55 PST
     BUG: Unicode or non-ANSCII DOES NOT WORK
     
     java_compileToExec.sh $j/Alignment.java  → $b/javaclass
     java_compileToExec.sh $j/CommentCode.java  → $b/javaclass
     java_compileToExec.sh $j/UncommentCode.java → $b/javaclass

      Old:
      Alignment.sh list_of_data      // Use space as delimiter
      Alignment.sh "=" list_of_data  // Use "=" as delimiter

      Update:

	    Alignment.sh -f  ""
	    Alignment.sh -f  "="
	                  |
			  ↓
			  + → →  + read getEnv("mytmp") ← from local file

	    Alignment.sh -p  ""
	    Alignment.sh -p  "=" 
			  |           
			  ↓     
			  + → read Stdin from pipe  ← from HTTP
      
               args[0] args[1]
      main        -f    ""
      main        -f    "="
      main        -p    ""
      main        -p    "="
                         |
                         ↓
		     args[1]
			 ↓            
	    argument2   ""   list_of_data 
	    argument2   "="  list_of_data
			  |            ↑
			  |            | ←  read Stdin from pipe, data from HTTP
			  ↓            |  
			  + → →  + read getEnv("mytmp") ← from local file

     (ghci ":i map")

    Alignment.sh (Old code)
    java -cp "/Users/aaa/myfile/bitbucket/javalib/jar/*":. Alignment ${1}
      |
      ⇓          (New code)
    java -cp "/Users/aaa/myfile/bitbucket/javalib/jar/*":. Alignment -p ${1}

    </pre>
*/ 
public class Alignment{
  public static void main(String[] args) {
      if(len(args) == 2){
	  List<String> ls = new ArrayList<>();
	  String flags = args[0];
	  String delimiter = args[1];
	  if (flags.compareTo("-f") == 0){
	      // read from "mytmp"
	      String fname = getEnv("mytmp");
	      ls = readFileNoTrim(fname);
	      logFileG(ls);
	  }else if(flags.compareTo("-p") == 0){
	      // read from Stdin or pipe
	      ls = readStdin();
	      logFileG(ls);
	  }
	  var lss         = args[1].compareTo(" ") == 0 ? alignmentSPC(ls) : alignmentNonSPC(args[1], ls);
	  printListNoNewLine(lss);
      }else{
	  System.out.println("ERROR: Invalid number of argument");
      }
      

    // if(len(args) > 0){
      // List<String> ls = readFileNoTrim(fname);
      // logFileG(list(fname));
      // logFileG(ls);
      // var lss         = alignmentNonSPC(args[0], ls);
      // printListNoNewLine(lss);
    // }else{
      // List<String> ls  = readFileNoTrim(fname);
      // List<String> ret = alignmentSPC(ls);
      // printListNoNewLine(ret);
    // }
    

    // test_alignmentNonSPC();
    // test_commentCode();
  }
  /**
        a  b  c   e 
         e f   g h
          i j k l
           ⇓
        a b c e
        e f g h
        i j k l
   
   */
  public static List<String> alignmentSPC(List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      var premx = len(takeWhile(x -> x == ' ', ls.get(0)));
      var lss   = alignment(ls, ' ');
      var flss  = fillColWith(0, x -> repeatToStr(premx, ' ') + x, lss);
      ret       = map(r -> foldStrDrop(r), flss);
    }
    return ret;
  }

    public static void printListNoNewLine(List<String> ls){
	String s = joinListStr(ls);
	System.out.print(s);
    }
}

