import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class maxContinuousSum{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        
        {
            Integer[] arr = {1, -2, -3, 4};
            Integer exp = 4;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }
        {
            Integer[] arr = {1, -2, 3, 4};
            Integer exp = 7;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }
        {
            Integer[] arr = {1, -2, -3, -4};
            Integer exp = 1;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }
        {
            Integer[] arr = {1};
            Integer exp = 1;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }
        end();
    }
    
    /**
    <pre>
    {@literal
        Find the maximum continuous sum from a given Integer[] array
    }
    {@code
        {
            Integer[] arr = {1, -2, -3, 4};
            Integer exp = 4;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }
        {
            Integer[] arr = {1, -2, 3, 4};
            Integer exp = 7;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }
        {
            Integer[] arr = {1, -2, -3, -4};
            Integer exp = 1;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }
        {
            Integer[] arr = {1};
            Integer exp = 1;
            Integer max = maxContinuousSum(arr);
            t(max, exp); 
        }

    }
    </pre>
    */ 
    public static Integer maxContinuousSum(Integer[] arr){
        if(arr != null){
            int len = arr.length;
            Integer[] ml = new Integer[len];
            if(len > 0){
                int max = arr[0];
                ml[0] = arr[0];
                for(int i=1; i<len; i++){
                    ml[i] = ml[i-1] + arr[i];
                    if(ml[i] < 0)
                        ml[i] = 0;

                    int lmax = Math.max(ml[i], arr[i]);
                    if(lmax > max)
                        max = lmax;
                }
                return max;
            }
        }
        throw new IllegalArgumentException("Array can not be null or empty.");
    }
} 

