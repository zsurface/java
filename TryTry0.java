import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;

public class TryTry0{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        int[][] arr = {{}};
        int len = arr.length;
        int wlen = arr[0].length;
        Print.p(len);
        Print.p(wlen);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        int[][] array = new int[0][0];
        int alen = array.length;
        int walen = array[0].length;
        Print.p("alen=" + alen);
        Print.p("walen=" + walen);

        Aron.end();
    }
} 

