import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class QuickSortProfile{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();

        StopWatch sw = new StopWatch();
        List<Integer> ls = geneRandomInteger(1000000);
        int lo = 0;
        int hi = len(ls) - 1;
        quickSort(ls, lo, hi);

        sw.printTime();
        end();
    }
} 

