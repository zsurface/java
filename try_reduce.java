import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_reduce{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();

        List<Double> list = Arrays.asList(1.0, 2.2, 3.0);
        Optional<Double> sum1 = list.stream().reduce((x, y) -> x + y);
        sum1.ifPresent(System.out::println);

        Integer[] arr = {2, 3, 3};
        Integer n = Arrays.stream(arr).reduce(0, (x, y) -> x + y);
        p("n=" + n);

        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        end();
    }
} 

