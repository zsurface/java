import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;


//class SortedPerson implements Comparable<Person>{
//    Person person;
//    public int compareTo(Person other){
//        return person.age - other.age;
//    }
//}

class Person implements Comparable<Person>{
    String name;
    Integer age;
    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
    }

    public int compareTo(Person other ){
        return age - other.age; 
    }
    public void print(){
        System.out.println("name=" + name + " age=" + age);
    }
}

class MyPerson implements Comparator<Person>{
    public int compare(Person first, Person second){
        return first.age - second.age;
    }
}

public class try_comparable{
    public static void main(String[] args) {
        test0();
        
        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
            
        List<Person> list1 = new ArrayList<Person>();
        list1.add(new Person("David", 20));
        list1.add(new Person("Michael", 30));
        list1.add(new Person("Michelle", 30));
        list1.add(new Person("Mexico", 10));

        List<Person> list2 = new ArrayList<Person>();
        list2.add(new Person("David", 20));
        list2.add(new Person("Michael", 30));
        list2.add(new Person("Michelle", 30));
        list2.add(new Person("Mexico", 10));

        Collections.sort(list1);
        Collections.sort(list2, new MyPerson());

        for(Person p : list1){
            p.print();
        }
        Print.ln();
        for(Person p : list2){
            p.print();
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
} 

