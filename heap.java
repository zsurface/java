import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import static classfile.Aron.*;
import static classfile.Test.*;

/**
 *   
 *   k = 1
 *   2*k = leftchild
 *   2*k + 1 = rightchild
 *   parent = leftchild/2 or rightchild/2 
 *
 */
class MinHeap {
    Integer max = 100;
    BiFunction<Integer, Integer, Integer> f;
    Integer[] arr = new Integer[max];
    Integer inx;
    Integer size;
    public MinHeap(Integer max, BiFunction<Integer, Integer, Integer> f) {
        this.f = f;
        this.max = max;
    }
    public MinHeap() {
        inx = 0;  // inx does not store any thing
    }

    public void insert(List<Integer> ls) {
        for(Integer n : ls){
            if(inx < ls.size()){
                inx++;
                arr[inx] = ls.get(n);
                heapify(inx, f);
            }
        }
    }
    public void insert(Integer n) {
        if(inx < max) {
            inx++;
            arr[inx] = n;
            heapify(inx, f);
        }
    }
    public boolean isEmpty(){
        return inx == 0;
    }
    public Integer top(){
        if(!isEmpty())
            return arr[1];
        else
            throw new IllegalArgumentException("heap is empty");
    }

    /**
     * Remove the top element
     * 1. arr[1] is the top element
     * 2. swap 1 and inx
     * 3. do bubble down or heapify down
     */
    public Integer removeTop() {
        Integer top = -1; 
        if(inx == 1){
            top = arr[1];
            inx--;
        }
        else{
            if(inx > 1){
                arr[1] = arr[inx];
                inx--;
                heapDown(1, f);
            }
        }
        return top;
    }
    public List<Integer> toList() {
        List<Integer> ls = new ArrayList<>(); 
        for(Integer i=1; i<=inx; i++) {
            ls.add(arr[i]); 
        }
        return ls;
    }
    public void print() {
        for(int i=1; i<=inx; i++) {
            System.out.println("arr[" + i + "]=" + arr[i]);
        }
    }

    /**
     *  1. find the smallest child to do the swap                  
     *              
     * (a, b) -> a < b ? -1 : (a == b ? 0 : 1) 
     *
     */
    private void heapDown(Integer index) {
        Integer leftchild = index*2;
        Integer rightchild = index*2 + 1;

        if(rightchild <= inx) {
            // if(arr[leftchild] < arr[rightchild]) {
            if(f.apply(arr[leftchild], arr[rightchild]) == -1) {  // 2 < 3
                if(f.apply(arr[index], arr[leftchild]) == 1) {    // 3 > 2 
                    swap(arr, index, leftchild);
                    heapDown(leftchild, f);
                }
            }
            else {
                if(arr[index] > arr[rightchild]) {   
                    swap(arr, index, rightchild);
                    heapDown(rightchild, f);
                }
            }
        }
        else {
            if(leftchild <= inx) {
                if(arr[index] > arr[leftchild]) {   
                    swap(arr, index, leftchild);
                    heapDown(leftchild, f);
                }
            }
        }
    }
    /**
     * Dubble up
     *
     *
     */
    private void heapify(Integer index, BiFunction<Integer, Integer, Integer> ff) {
        Integer parent = index/2;
        if(parent >= 1) {
            if(ff.apply(arr[index], arr[parent]) == -1) {
                swap(arr, index, parent);
                heapify(parent, ff);
            }
        }
    }
}

class heap {
    public static void main(String args[]) {
        test1();
    }
    public static void test1(){
        beg();
        {
            MinHeap h = new MinHeap(10, (a, b) -> a - b);
            h.insert(11);

            Integer top = h.removeTop();
            t(top, 11);
            t(h.isEmpty(), true);
        }

        // {
            // MinHeap h = new MinHeap();
            // h.insert(2);
            // h.insert(1);

            // t(h.top(), 1);
            // t(h.isEmpty(), false);
        // }
        // {
            // MinHeap h = new MinHeap();
            // h.insert(2);
            // h.insert(1);
            // h.insert(3);

            // t(h.top(), 1);
            // t(h.isEmpty(), false);
        // }
        // {
            // MinHeap h = new MinHeap();
            // h.insert(2);
            // h.insert(99);
            // h.insert(1);
            // h.insert(3);

            // t(h.top(), 1);
            // t(h.isEmpty(), false);
        // }
        // {
            // MinHeap h = new MinHeap();
            // h.insert(2);
            // h.insert(99);
            // h.insert(1);
            // h.insert(3);
            // h.removeTop();

            // t(h.top(), 2);
            // t(h.isEmpty(), false);
        // }
        // {
            // MinHeap h = new MinHeap();
            // h.insert(2);
            // h.insert(99);
            // h.insert(1);
            // h.insert(3);
            // h.removeTop();
            // h.removeTop();

            // t(h.top(), 3);
            // t(h.isEmpty(), false);
        // }
        // {
            // MinHeap h = new MinHeap();
            // h.insert(2);
            // h.insert(99);
            // h.insert(1);
            // h.insert(3);
            // h.removeTop();
            // h.removeTop();
            // h.removeTop();

            // t(h.top(), 99);
            // t(h.isEmpty(), false);
        // }
        // {
            // MinHeap h = new MinHeap();
            // h.insert(2);
            // h.insert(99);
            // h.insert(1);
            // h.insert(3);
            // h.removeTop();
            // h.removeTop();
            // h.removeTop();
            // h.removeTop();

            // t(h.isEmpty(), true);
        // }

        end();
    }
}
