import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


public class try_readfile3{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        try{
            BufferedReader br = new BufferedReader(new FileReader("/tmp/f1.x"));
            String s = null;
            while((s = br.readLine()) != null){ 
                p(s);
                String[] arr = s.split("\\s+");
                p(arr);
            }
        }catch(IOException e){
            e.printStackTrace();
        }



        sw.printTime();
        end();
    }
} 

