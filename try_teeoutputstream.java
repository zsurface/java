import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import org.apache.commons.io.output.*;


// KEY: write file and write to stdout, write to both stdout and file
public class try_teeoutputstream{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();

		boolean done = false;
		while(!done){
			Scanner sc = new Scanner(System.in);
			String line = sc.nextLine();
			if(line.equals("q")){
				done = true;
			}else{
				writeFileBothX("/tmp/xx.x", lines(line));
			}
		}
        end();
    }
	
	public static void writeFileBothX(String fName, List<String> list) {
        try {
            File file = new File(fName);
            FileOutputStream fos = new FileOutputStream(file);
            TeeOutputStream bothOut = null;
            bothOut = new TeeOutputStream(System.out, fos);

            for(String str: list) {
                byte[] contentInBytes = str.getBytes();
                //we will want to print in standard "System.out" and in "file"
                bothOut.write(contentInBytes);
            }

            if(fos != null){
                fos.flush();
                fos.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


} 

