import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// pass by reference
public class try_list1{
    public static void main(String[] args) {
        test0();
    }
    public static Tuple<List<Integer>, List<Integer>> fun(List<Integer> ls){
        Tuple<List<Integer>, List<Integer>> pair = new Tuple<>(of(), of());
        pair = new Tuple<>(of(), listCopy(ls)); 
        return pair;
    }
    public static void test0(){
        beg();

        List<Integer> ls = new ArrayList<Integer>();
        ls.add(1);
        ls.add(2);
        ls.add(3);
        Tuple<List<Integer>, List<Integer>> tuple = fun(ls);

        ls.add(4);

        fl("ls");
        p(ls);
        fl("tuple");
        p(tuple.x);
        p(tuple.y);

        end();
    }
} 

