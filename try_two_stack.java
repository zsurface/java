import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class MyStack{
    ArrayList<Integer> list = new ArrayList<Integer>();
    public MyStack(){}
    public void push(Integer n){
        list.add(n);
    }
    public Integer pop() throws Exception{
        if(list.size() > 0){
            Integer n = list.get(list.size() - 1);
            list.remove(list.size() - 1);
            return n;
        }
        else
            throw new Exception("The stack is empty"); 
    }
    public boolean isEmpty(){
        return list.size() == 0;
    }
    public Integer top() throws Exception{
        if(list.size() > 0)
            return list.get(list.size() - 1);
        else
            throw new Exception("The stack is empty"); 
    }
}

class GetMax{
    public MyStack st = new MyStack();
    public MyStack ms = new MyStack();
    
    public void push(Integer n) throws Exception{
        if (ms.isEmpty() == false){
            if(ms.top() <= n)
                ms.push(n);
        }else{
            ms.push(n);
        }
        st.push(n);
    }
    public Integer pop() throws Exception{
        if(ms.isEmpty() == false){
            if(st.top() == ms.top()){
                return ms.pop();
            }
            return st.pop();
        }
        throw new Exception("The stack is empty");
    }
    public Integer getMax() throws Exception{
        if(ms.isEmpty() == false){
            return ms.top();
        }else{
            throw new Exception("Stack is empty");
        }
    }
    public boolean isEmpty(){
        return st.isEmpty();
    }

}


public class try_two_stack{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        GetMax s = new GetMax();
        try{
            s.push(3);
            s.push(4);
            Test.t(s.getMax(), 4);
            Print.p("max1=" + s.getMax());
            s.pop();
            Test.t(s.getMax(), 3);
            s.pop();
            Test.t(s.isEmpty(), true);
            s.pop();
        }catch(Exception e){
            Print.p("e.message=" + e.getMessage());
        }
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        MyStack s = new MyStack();
        try{
            s.push(1);
            s.push(2);
            Test.t(s.top(), 2);
            Test.t(s.isEmpty(), false);
            s.pop();
            Test.t(s.top(), 1);
            Test.t(s.isEmpty(), false);
            s.pop();
            Test.t(s.isEmpty(), true);
        }catch(Exception e){
            Print.p("e.message=" + e.getMessage());
        }
        Aron.end();
    }
} 

