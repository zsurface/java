import java.util.*;

/**
   How to compile and run?

   Run a shell script: run.sh
   ./run.sh

   or 
   javac Shape2d.java
   java Shape2D
*/

class Point{
  final Integer x;
  final Integer y;
  
  public Point(Integer x, Integer y){
    this.x = x;
    this.y = y;
  }
  
  public Boolean equals(Point point){
    Boolean ret = false;
    if(this == point)
      ret = true;
    else{
      if(this.x.equals(point.x) && this.y.equals(point.y))
        ret = true;
    }
    
    return ret;
  }
}

interface Shape{
  public Boolean isInside(Point point);
  public Boolean equals(Shape o);
}

class Rectangle implements Shape{
  private final Point topLeft;
  private final Point bottomRight;

  /**
     Assume the 2D coordinate system is the standard Cartesian Coordinate System.
                      
                    ↑ +y
                    |
                    |
                    |
     -x <-----------+-------------> +x
                    |
                    |
                    |
                    ↓ -y
  */
  public Rectangle(Point topLeft, Point bottomRight){
    if(topLeft.x <= bottomRight.x && topLeft.y >= bottomRight.y){
      this.topLeft = topLeft;
      this.bottomRight = bottomRight;
    }else{
      throw new IllegalArgumentException("Invalid Points.");
    }
  }
  
  /**
     topLeft
     ↓           
     + ---------+
     |          |
     |          |
     +----------+ ← bottomRight
           
     Assume if the rectangle is zero width and zero height then it is also a rectangle.
  */
  public Boolean isInside(Point point){
    return (topLeft.x <= point.x && point.x <= bottomRight.x &&
            topLeft.y >= point.y && point.y >= bottomRight.y) ? true : false;
  }
  
  public Boolean equals(Shape shape){
    Boolean ret = false;
    Rectangle rect = (Rectangle)shape;

    if(this == rect){
      ret = true;
    }
    else{
      if(this.topLeft.equals(rect.topLeft) &&
         this.bottomRight.equals(rect.bottomRight))
        ret = true;
    }

    return ret;
  }
}

class Circle implements Shape{
  Boolean ret = false;
  private final Point center;
  private final Integer radius;

  public Circle(Point point, Integer radius){
    center = point;
    if(radius >= 0){
      this.radius = radius;
    }else{
      throw new IllegalArgumentException("Radius can not be negative.");
    }
  }
  
  public Boolean equals(Shape shape){
    Circle circle = (Circle)shape;
    if(this == circle)
      ret = true;
    else{
      if(this.center.equals(circle.center) && this.radius.equals(circle.radius)){
        ret = true;
      }
    }

    return ret;
  }
  
  public Boolean isInside(Point point){
    Boolean ret = false;
    Integer norm = (center.x - point.x)*(center.x - point.x) +
                   (center.y - point.y)*(center.y - point.y);
    Double dist = Math.sqrt(norm.doubleValue());
    if(dist <= radius)
      ret = true;

    return ret;
  }
}


public class Shape2D{
  public static void main(String[] args) {
    test_point_equals();
    test_rectangle_equals();
    test_rectangle_isInside();
  }

  public static void test_point_equals(){
    {
      {
        System.out.println("Two points are equals.");
        Point p0 = new Point(0, 0);
        Point p1 = new Point(0, 0);
        System.out.println(p0.equals(p1));
      }
      {
        System.out.println("Two points are equals.");
        Point p0 = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Point p1 = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        System.out.println(p0.equals(p1));
      }
      {
        System.out.println("Two points are equals.");
        Point p0 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p1 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        System.out.println(p0.equals(p1));
      }
      {
        System.out.println("Two points are equals.");
        Point p0 = new Point(-1, -1);
        Point p1 = new Point(-1, -1);
        System.out.println(p0.equals(p1));
      }
      {
        System.out.println("Two points are NOT equals.");
        Point p0 = new Point(0, 0);
        Point p1 = new Point(0, 1);
        System.out.println(p0.equals(p1) == false);
      }
      {
        System.out.println("Two points are NOT equals.");
        Point p0 = new Point(0, 0);
        Point p1 = new Point(0, -1);
        System.out.println(p0.equals(p1) == false);
      }
      {
        System.out.println("Two points are NOT equals.");
        Point p0 = new Point(1, 4);
        Point p1 = new Point(-3, -1);
        System.out.println(p0.equals(p1) == false);
      }
    }
  }
  
  public static void test_rectangle_equals(){
    {
      {
        System.out.println("Two rectangles are equals.");
        Point p0 = new Point(0, 0);
        Point p1 = new Point(0, 0);

        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p0, p1);
        System.out.println(rect0.equals(rect1));
      }
      {
        System.out.println("Two rectangles are equals.");
        Point p0 = new Point(0, 0);
        Point p1 = new Point(0, 0);

        Point p2 = new Point(0, 0);
        Point p3 = new Point(0, 0);
        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p0, p1);
        System.out.println(rect0.equals(rect1));
      }
      {
        System.out.println("Two rectangles are equals.");
        Point p0 = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Point p1 = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);

        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p0, p1);
        System.out.println(rect0.equals(rect1));
      }
      {
        System.out.println("Two rectangles are equals.");
        Point p0 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p1 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p0, p1);
        System.out.println(rect0.equals(rect1));
      }
      {
        System.out.println("Two rectangles are equals.");
        Point p0 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p1 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        
        Point p2 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p3 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p2, p3);
        System.out.println(rect0.equals(rect1));
      }
      {
        System.out.println("Two rectangles are equals.");
        Point p0 = new Point(-1, -2);
        Point p1 = new Point(2, -3);
        Point p2 = new Point(-1, -2);
        Point p3 = new Point(2, -3);

        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p2, p3);
        System.out.println(rect0.equals(rect1));
      }
      {
        System.out.println("Two rectangles are NOT equals.");
        Point p0 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p1 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p2 = new Point(0, 0);
        Point p3 = new Point(0, 0);

        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p2, p3);
        System.out.println(rect0.equals(rect1) == false);
      }
      {
        System.out.println("Two rectangles are NOT equals.");
        Point p0 = new Point(0, 0);
        Point p1 = new Point(0, 0);

        Point p2 = new Point(0, 1);
        Point p3 = new Point(0, 0);
        Rectangle rect0 = new Rectangle(p0, p1);
        Rectangle rect1 = new Rectangle(p2, p3);
        System.out.println(rect0.equals(rect1) == false);
      }
      
    }
  }
  public static void test_rectangle_isInside(){
    {
      {
        System.out.println("Point is inside a rectangle.");
        Point p  = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Point p0 = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Point p1 = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);

        Rectangle rect = new Rectangle(p0, p1);
        System.out.println(rect.isInside(p));
      }
      {
        System.out.println("Point is inside a rectangle.");
        Point p  = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p0 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        Point p1 = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

        Rectangle rect = new Rectangle(p0, p1);
        System.out.println(rect.isInside(p));
      }
      {
        System.out.println("Point is inside a rectangle.");
        Point p  = new Point(0, 0);
        Point p0 = new Point(0, 0);
        Point p1 = new Point(0, 0);

        Rectangle rect = new Rectangle(p0, p1);
        System.out.println(rect.isInside(p));
      }
      {
        System.out.println("Point is inside a rectangle.");
        Point p  = new Point(0, -1);
        Point p0 = new Point(0, 0); 
        Point p1 = new Point(0, -1);

        Rectangle rect = new Rectangle(p0, p1);
        System.out.println(rect.isInside(p));
      }
      {
        System.out.println("Point is inside a rectangle.");
        Point p  = new Point(4,  -4); 
        Point p0 = new Point(0,  0);  
        Point p1 = new Point(10, -10);
        
        Rectangle rect = new Rectangle(p0, p1);
        System.out.println(rect.isInside(p));
      }
      {
        System.out.println("Point is outside a rectangle.");
        Point p  = new Point(0, -1);
        Point p0 = new Point(0, 0); 
        Point p1 = new Point(0, 0); 

        Rectangle rect = new Rectangle(p0, p1);
        System.out.println(rect.isInside(p) == false);
      }
      {
        System.out.println("Point is outside a rectangle.");
        Point p  = new Point(2, 3); 
        Point p0 = new Point(0, 0); 
        Point p1 = new Point(0, -1);

        Rectangle rect = new Rectangle(p0, p1);
        System.out.println(rect.isInside(p) == false);
      }
    }
  }
} 

