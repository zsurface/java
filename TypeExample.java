import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/*
-------------------------------------------------------------------------------- 
Tue Nov 20 10:57:59 2018 
Java typing, java type, about typing, about type 
<? extends Number> <? super Number>
-------------------------------------------------------------------------------- 
*/
public class TypeExample{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
    // gx https://docs.oracle.com/javase/tutorial/java/generics/subtyping.html
        Aron.beg();
        // it does work
//        List<Number> listNum = new ArrayList<>();
//        List<Integer> listInt = new ArrayList<>();
//        listNum = listInt;

        // it works
        List<? extends Number> listNum = new ArrayList<>();
        List<? extends Integer> listInt = new ArrayList<>();
        listNum = listInt;

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        List<Number> listNum = new ArrayList<>();
        List<Integer> listInt = new ArrayList<>();
        listNum = listInt;
        Aron.end();
    }
} 

