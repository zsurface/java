import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
        a
   b         c
           e  
      d

*/
/**
    KEY: minimum path in a graphic

    Given a graph and two nodes, and find the minimum path from one node to other node
*/
public class MiniPath{
    public static void main(String[] args) {
        // test0();
        // test1();
	test2();
    }
    public static void test1(){
        beg();
        String[] arr = { 
                         "a -> b c",
                         "b -> d",
                         "c -> e",
                         "e -> d"
                       };
        Map<String, List<String>> map = geneGraphList(arr); 
        p(map, "map");
        List<String> ls = map.get("a");
        if(ls == null){
            pp("ls is null");
        }else{
            pp("ls is not null");
        }
        end();
    }
    public static void test0(){
        beg();
	{
	    List<String> miniList = list(); 
	    List<String> buffer = list(); 
	    String[] arr = { 
		"a -> b c",
		"b -> d",
		"c -> e",
		"e -> d"
	    };
	    Map<String, List<String>> map = geneGraphList(arr); 

	    p(map, "map");

	    String s1 = "a";
	    String s2 = "d";
	    buffer.add(s1);
	    minimumPath(s1, s2, map, miniList, buffer);
	    fl("mini");
	    p(miniList);
	}
        end();
    }
    
    public static void test2(){
        beg();
	{
	    String[] arr = { 
		"a -> b c",
		"b -> d",
		"c -> e",
		"e -> d"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    String s1 = "a";
	    String s2 = "b";
	    Set<String> set = new HashSet<>();
	    set.add(s1);
	    boolean bo = checkCycle(s1, s2, map, set);
	
	    t(bo, false);
	}
	{
	    String[] arr = { 
		"a -> a"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    String s1 = "a";
	    String s2 = "b";
	    Set<String> set = new HashSet<>();
	    set.add(s1);
	    boolean bo = checkCycle(s1, s2, map, set);
	
	    t(bo, true);
	}
	{
	    String[] arr = { 
		"a -> b",
		"b -> a"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    String s1 = "a";
	    String s2 = "e";
	    Set<String> set = new HashSet<>();
	    set.add(s1);
	    boolean bo = checkCycle(s1, s2, map, set);
	
	    t(bo, true);
	}
	
	{
	    List<String> ls = list("a");
	    String[] arr = { 
		"a -> a"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    Set<String> set = new HashSet<>();
	    String s1 = "a";
	    set.add(s1);
	    boolean bo = cycle(s1, map, set);
	    t(bo, true);
	}
	{
	    List<String> ls = list("a", "b");
	    String[] arr = { 
		"a -> b"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    Set<String> set = new HashSet<>();
	    String s1 = "a";
	    set.add(s1);
	    boolean bo = cycle(s1, map, set);
	    t(bo, false);
	}
	
	{
	    List<String> ls = list("a", "b");
	    String[] arr = { 
		"a -> b",
		"b -> a"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    Set<String> set = new HashSet<>();
	    String s1 = "a";
	    set.add(s1);
	    boolean bo = cycle(s1, map, set);
	    t(bo, true);
	}
	{
	    List<String> ls = list("a", "b", "c");
	    String[] arr = { 
		"a -> b",
		"b -> c"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    Set<String> set = new HashSet<>();
	    String s1 = "a";
	    set.add(s1);
	    boolean bo = cycle(s1, map, set);
	    t(bo, false);
	}
	{
	    String[] arr = { 
		"a -> b c",
		"b -> d",
		"c -> e",
		"e -> d"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    Set<String> set = new HashSet<>();
	    String s1 = "a";
	    set.add(s1);
	    boolean bo = cycle(s1, map, set);
	    t(bo, false);
	}
	{
	    String[] arr = { 
		"a -> b c",
		"b -> d",
		"c -> e",
		"e -> d",
		"d -> e",
		"e -> a"
	    };
	    Map<String, List<String>> map = geneGraphList(arr);
	    Set<String> set = new HashSet<>();
	    String s1 = "a";
	    set.add(s1);
	    boolean bo = cycle(s1, map, set);
	    t(bo, true);
	}

        end();
    }
    
    public static void test3(){
        beg();
	{
	    
	}
	end();
    }

    /**
       check graph cycle with Set
    */
    public static boolean checkCycle(String s1, String s2, Map<String, List<String>> map, Set<String> set){
	if(s1.equals(s2)){
	    p("s1=" + s1);
   	    p("s2=" + s2);
	}else{
	    List<String> ls = map.get(s1);
	    if(ls != null){
		for(String s : ls){
		    if(set.contains(s)){
			return true;
		    }else{
			return checkCycle(s, s2, map, append(set, s));
		    }
		}
	    }
	}
	return false;
    }

    /**
       KEY: given a node => k and a graph => map, check whether the node => k is in an cycle, only this node
     */
    public static boolean cycle(String k, Map<String, List<String>> map, Set<String> set){
	List<String> ls = map.get(k);
	if(ls != null){
	    for(String s : ls){
		if(set.contains(s)){
		    return true;
		}else{
		    return cycle(s, map, append(set, s));
		}
	    }
	}
	return false;
    }

    /**
       KEY: check cycle from a given graph and a list of nodes
     */
    public static boolean checkCycle2(List<String> ls, Map<String, List<String>> map){
	for(String k : ls){
	    Set<String> myset = new HashSet<>();
	    myset.add(k);
	    if(cycle(k, map, myset)){
		return true;
	    }
	}
	return false;
    }

    /**
        1. is any cycle in the graph?
            check cycle with Set<String>
        2. there might be no edge from one node to other node
        3. use Map<String, List<String>> linkedHashMap = new LinkedHashMap<>();
    */
    public static void minimumPath(String first, String second, Map<String, List<String>> map, List<String> miniList, List<String> buffer){
        p("first=" + first);
        p("second=" + second);
        if(first.equals(second)){
            fl("buffer");
            p(buffer);
            if(len(miniList) == 0){
                copyListTo(miniList, buffer);
            }else{
                if(len(buffer) < len(miniList)){
                    copyListTo(miniList, buffer);
                }
            }

        }else{
            List<String> valueList = map.get(first);
            if(valueList != null){
                p(valueList, "valueList");
                for(String child : valueList){
                    minimumPath(child, second, map, miniList, append(buffer, child));
                }
            }else{
                p("first=" + first);
                p("valueList == null");
            }
        }

    }
} 

