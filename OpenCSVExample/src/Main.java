import classfile.Print;
import classfile.Aron;
import com.opencsv.CSVReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * /Users/cat/myfile/bitbucket/java/OpenCSVExample/src/Main.java
 * read csv file with opencsv lib 4.0, jar file under  $b/javalib/jar/opencsv-4.0.jar
 */
public class Main {
    public static void main(String[] args) {
        Print.p("Hello World");
        test0();
        test1();
    }

    
    /**
    <pre>
    {@literal
        Read CSV file to List<String[]> 

        depend on opencsv-4.0.jar

        Test file: /Users/cat/myfile/bitbucket/java/OpenCSVExample/src/Main.java
    }
    {@code
        String csvFile = "/Users/cat/myfile/bitbucket/testfile/csv.csv";
        readCSV(csvFile);
        List<String[]> list = readCSV(csvFile);
        Print.pArr(list);
    }
    </pre>
    */ 
    public static List<String[]> readCSV(String csvFile){
        List<String[]> list = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(csvFile));
            CSVReader csvReader = new CSVReader(br);
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                list.add(line);
                Print.p(line);
            }
            br.close();
            csvReader.close();
        }catch (IOException io){
            io.printStackTrace();
        }
        return list;

    }
    static void test0(){
        Aron.beg();

        String csvFile = "/Users/cat/myfile/bitbucket/testfile/csv.csv";
        List<String[]> list = Aron.readCSV(csvFile);
        Print.pArr(list);

        Aron.end();
    }
    static void test1(){
        Aron.beg();

        Integer[] arr = {1, 2, 3, 4};
        int width = arr.length; 
        //Print.p(arr);
        List<Integer[]> list = new ArrayList<>();
        list.add(arr);
        Aron.line();
        Print.pArr(list);

        Aron.end();
    }
}
