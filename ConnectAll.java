import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class ConnectAll{
  public static void main(String[] args) {
    test0();
    // test1();
  }
  
  public static List<Integer> div(List<Integer> ls, Integer n){
    return map(x -> x/n, ls);
  }
  public static List<Double> div(List<Double> ls, Double n){
    return map(x -> x/n, ls);
  }
  
  public static Tuple<Double, Double> div(Tuple<Double, Double> t, Double n){
    return tuple(t.x/n, t.y/n);
  }

  public static Point2<Double> div(Point2<Double> p, Double n){
    return new Point2(p.x/n, p.y/n, p.z/n);
  }
  
  public static <T> List<List<T>> mask(Function<Integer, Boolean> f, List<List<T>> s1, List<List<Integer>> ma){
    List<List<T>> ret = ll();
    if(dim(s1) == dim(ma)){
      for(int i = 0; i < len(s1); i++){
        for(int j = 0; j < len(ma); j++){
          List<T> row = ll();
          if(f.apply(ma.get(i).get(j))){
            row.add(s1.get(i).get(j));
          }
          ret.add(row);
        }
      }
    }
    return ret;
  }
  
  public static <T> List<List<T>> mask(List<List<T>> s1, List<List<Integer>> ma){
    List<List<T>> ret = ll();
    Function<Integer, Boolean> f = x -> x == 1;
    if(dim(s1) == dim(ma)){
      for(int i = 0; i < len(s1); i++){
        for(int j = 0; j < len(ma); j++){
          List<T> row = ll();
          if(f.apply(ma.get(i).get(j))){
            row.add(s1.get(i).get(j));
          }
          ret.add(row);
        }
      }
    }
    return ret;
  }

  public static List<List<Integer>> maskCompose(BiFunction<Integer, Integer, Boolean> f, List<List<Integer>> m1, List<List<Integer>> m2){
    List<List<Integer>> ret = ll();
    if(dim(m1) == dim(m2)){
      for(int i = 0; i < len(m1); i++){
        List<Integer> row = ll();
        for(int j = 0; j < len(m2); j++){
          if(f.apply(m1.get(i).get(j), m2.get(i).get(j))){
            row.add(1);
          }else{
            row.add(0);
          }
        }
        ret.add(row);
      }
    }
    return ret;
  }
  public static void test0(){
    beg();
    {
      // {
        // var ls = connectAll(3, ll(1, 2, 3, 4, 5));
        // pp(ls);
      // }
      {
        var p1 = new Point2(1.0, 2.0, 0.0);
        var p2 = new Point2(2.0, 3.0, 0.0);
        var p3 = new Point2(4.0, 9.0, 0.0);
        var p4 = new Point2(5.0, 1.0, 0.0);
        var seg = new Segment(p1, p2);
        var seg1 = segment(p1, p2);
        pl(seg1.p1.toStrSPC());
        pl(seg1.p2.toStrSPC());
        List<Segment> ls = connectAll(p4, ll(p1, p2, p3));
        fl("ls");
        pp(ls);
        List<Tuple<Segment, Double>> lsSeg = map(s -> tuple(s, dist(s.p1, s.p2)), ls);
        List<Tuple<Segment, Double>> sortls = sort(lsSeg, (t1, t2) -> t1.y.compareTo(t2.y));
        fl("sortls");
        pp(sortls);
        
        // pp(ls);
        // var s = tuple(3.0, 2.0);
        // var tu = div(s, 10.0);
        // Tuple<Tuple<Double, Double>, Tuple<Double, Double>> s4 = tuple(div(tu, 10.0), div(tu, 3.0));
        // List<Tuple<Point2, Point2>> lr = map(t -> tuple(div(t.x, 10.0), div(t.y, 10.0)), ls);
        // fl("lr");
        // pp(lr);
        // pp(ls);
      }
      {
        var t1 = tuple(2.1, 3.1);
        var t2 = tuple(3.1, 4.0);
        var ls = ll(t1, t2);
        var sortls = sort(ls, (x, y) -> x.y.compareTo(y.y));
      }
      {
        var p1 = new Point2(1.0, 2.0, 0.0);
        var p2 = new Point2(2.0, 3.0, 0.0);
        var p3 = new Point2(4.0, 9.0, 0.0);
        var ls = ll(p1, p2, p3);
        var lsIx = zip(range(0, 10), ls);
        var lss = outer((t1, t2) -> tuple(t1, t2), lsIx, lsIx);
        // var fls = filter((t1, t2) -> t1.x > t2.x, flatMap(lss));
        fl("lss");
        pp(lss);
      }
      {
        var s1 = zip(range(0, 10), ll("a", "b", "c", "d", "e"));
        var s2 = zip(range(0, 10), ll("aa",  "bb", "cc", "dd"));
        s1.addAll(s2);
        fl("ss");
        pp(s1);
        var ls2 = outer((t1, t2) -> tuple(t1, t2), s1, s2);
        var fm = flatMap(ls2);
        fl("ls2");
        pp(ls2);
        fl("fm");
        pp(fm);
        var filt = filter(t -> t.x.x == t.y.x, fm);
        fl("filt");
        pp(filt);
      }
      {
        var s1 = range(0, 3);
        var s2 = range(0, 3);
        var out = outer((x, y) -> tuple(x, y), s1, s2);
        fl("out");
        pp(out);
        var out1 = outer((x, y) -> x > y ? 1 : 0, s1, s2);
        fl("out1");
        pp(out1);
        var out2 = outer((x, y) -> x == y ? 1 : 0, s1, s2);
        fl("out2");
        pp(out2);
        var out3 = outer((x, y) -> x < y ? 1 : 0, s1, s2);
        fl("out3");
        pp(out3);
      }
      {
        var lss = outerMask((x, y) -> x < y ? 1 : 0, 3, 3);
        fl("x < y ? 1 : 0");
        pp(lss);
      }
      {
        var lss = outerMask((x, y) -> x == y ? 1 : 0, 3, 3);
        fl("x == y ? 1 : 0");
        pp(lss);
      }
      {
        var lss = outerMask((x, y) -> x > y ? 1 : 0, 3, 3);
        fl("x < y ? 1 : 0");
        pp(lss);
        var lsr = rotate(lss);
        fl("rotate");
        pp(lsr);
        fl("transpose");
        pp(transpose(lsr));
      }

      {
        fl("s");
        var z = zipWith2d((x, y) -> tuple(x, y), ll(1, 2, 3, 4), ll(4, 5, 6, 7));
        var s = outer((x, y) -> x * y, range(0, 4), range(0, 4));
        pp(s);
        var m = outerMask((c, r) -> c < r ? 1 : 0, 4, 4);
        pp(m);
        var ma = mask(s, m);
        fl("ma");
        pp(ma);
        var fm = flatMap(ma);
        fl("fm");
        pp(fm);
        var mb = flatMap(mask(z, m));
        fl("z");
        pp(mb);
      }
      {
        var m1 = outerMask((c, r) -> c < r ? 1 : 0, 4, 4);
        var m2 = rotate(m1);
        var m3 = maskCompose((x, y) -> x == 1 || y == 1, m1, m2);
        fl("m3");
        pp(m3);
        
      }
    }
    end();
  }
  
  // public static <A, B> List<Tuple<A, B>> filter2(BiFunction<A, B, Boolean>, List<A> s1, List<B> s2){
    
  // }
  public static List<Segment> connectAll(Point2 p1, List<Point2> ls){
    List<Segment> ret = new ArrayList<>(); 
    for(int i = 0; i < len(ls); i++){
      Point2 ele = ls.get(i);
      List<Point2> ss = removeIndex(i, ls); 
      List<Segment> pls = map(x -> segment(ele, x), ss);
      ret.addAll(pls);
    }
    return ret;
  }
  
  public static void test1(){
    beg();
    end();
  }
} 

