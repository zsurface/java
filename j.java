













<T> void empty(List<T> ls){
    ls.clear();
}

<T> void empty(Set<T> set){
    set.clear();
}

<A, B> void empty(Map<A, B> map){
    map.clear();
}

<T> void empty(Queue<T> q){
    q.clear();
}

<T> void empty(Stack<T> s){
    s.clear();
}

<T> void empty(LinkedList<T> ll){
    ll.clear();
}


<T> void clear(List<T> ls){
    ls.clear();
}

<T> void clear(Set<T> set){
    set.clear();
}

<A, B> void clear(Map<A, B> map){
    map.clear();
}

<T> void clear(Queue<T> q){
    q.clear();
}

<T> void clear(Stack<T> s){
    s.clear();
}

<T> void clear(LinkedList<T> ll){
    ll.clear();
}

