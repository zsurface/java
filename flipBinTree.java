import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class flipBinTree{
    public static void main(String[] args) {
	test_flipBinTree();
	test_flipBinTreeMutable();
    }
    public static void test0(){
        beg();

        end();
    }
    
    /**
                 8
            4        9
         1     6
	 
                 8
             9        4
                   6     1
    */
    public static void test_flipBinTree(){
        beg();
	{
	    {
		Node node = null;
		Node flipNode = flipBinTree(node);
		printTree(flipNode);
		List<Integer> ls = inorderToList(flipNode);
		t(ls, list());
	    }
	    {
		Node node = geneBinNoImage(list(8));
		Node flipNode = flipBinTree(node);
		printTree(flipNode);
		List<Integer> ls = inorderToList(flipNode);
		t(ls, list(8));
	    }
	    {
		Node node = geneBinNoImage(list(8, 1));
		Node flipNode = flipBinTree(node);
		printTree(flipNode);
		List<Integer> ls = inorderToList(flipNode);
		t(ls, list(8, 1));
	    }
	    {
		Node node = geneBinNoImage(list(1, 2, 3));
		Node flipNode = flipBinTree(node);
		printTree(flipNode);
		List<Integer> ls = inorderToList(flipNode);
		t(ls, list(3, 2, 1));
	    }
	    {
		Node node = geneBinNoImage(list(8, 1, 9));
		Node flipNode = flipBinTree(node);
		printTree(flipNode);
		List<Integer> ls = inorderToList(flipNode);
		t(ls, list(9, 8, 1));
	    }
	    {
		Node node = geneBinNoImage(list(8, 4, 1, 6, 9));
		Node flipNode = flipBinTree(node);
		printTree(flipNode);
		List<Integer> ls = inorderToList(flipNode);
		t(ls, list(9, 8, 6, 4, 1));
	    }
	}
        end();
    }
    public static void test_flipBinTreeMutable(){
	beg();
	{
	    {
		Node node = null;
		flipBinTreeMutable(node);
		printTree(node);
		List<Integer> ls = inorderToList(node);
		t(ls, list());
	    }
	    {
		Node node = geneBinNoImage(list(8));
		flipBinTreeMutable(node);
		printTree(node);
		List<Integer> ls = inorderToList(node);
		t(ls, list(8));
	    }
	    {
		Node node = geneBinNoImage(list(8, 1));
		flipBinTreeMutable(node);
		printTree(node);
		List<Integer> ls = inorderToList(node);
		t(ls, list(8, 1));
	    }
	    {
		Node node = geneBinNoImage(list(1, 2, 3));
		flipBinTreeMutable(node);
		printTree(node);
		List<Integer> ls = inorderToList(node);
		t(ls, list(3, 2, 1));
	    }
	    {
		Node node = geneBinNoImage(list(8, 1, 9));
		flipBinTreeMutable(node);
		printTree(node);
		List<Integer> ls = inorderToList(node);
		t(ls, list(9, 8, 1));
	    }
	    {
		Node node = geneBinNoImage(list(8, 4, 1, 6, 9));
		flipBinTreeMutable(node);
		printTree(node);
		List<Integer> ls = inorderToList(node);
		t(ls, list(9, 8, 6, 4, 1));
	    }
	}
        end();
    }
    
} 

