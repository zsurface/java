import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


class Node{
    Node prev;
    Node next;
    int data;
    public Node(int data) {
        this.data = data;
    }
}

class DLL {
    Node head;
    Node tail;
    public void append(Node node) {
        if(head == null) {
            head = tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
    }
    public void addToSortedList(Node node) {
        Node curr = head;
        // empty list
        if(curr == null){
            head = tail = node;
        }else{
           while(curr != null){
               if(node.data < curr.data){
                   // head node
                   if(curr.prev == null){
                       Node tmp = head;
                       head = node;
                       head.next = tmp;
                       tmp.prev = head;
                       break;
                   }else{
                        if(curr.prev.data < node.data){
                           Node prev = curr.prev;
                           prev.next = node;
                           node.prev = prev;
                           node.next = curr;
                           curr.prev = node;

                           break;
                        }
                   }
               }else{
                   // tail node
                   if(curr.next == null){
                       Node tmp = tail;
                       tail = node;
                       tmp.next = tail;
                       tail.prev = tmp;
                       break;
                   }
               }
               curr = curr.next;
           }
        }
    }
    public void remove(Node node) {
        if(node != null && head != null) {
            Node curr = head;
            while(curr.data != node.data) {
                curr = curr.next;
            }
            if(curr != null) {
                // found node
                if(curr.prev == null) {
                    // first node
                    Node next = curr.next;
                    if(next == null) {
                        // only one node
                        head = tail = null;
                    } else {
                        // # of node > 1
                        head = next;
                        head.prev = null;
                    }
                } else if(curr.next == null) {
                    // last node;
                    Node prev = curr.prev;
                    prev.next = null;
                    tail = prev;
                } else {
                    // "middle" node
                    Node prev = curr.prev;
                    Node next = curr.next;
                    prev.next = next;
                    next.prev = prev;
                    node = null;
                }
            }
        }
    }
    public void print(){
        Node curr = head;
        while(curr != null){
            System.out.println(curr.data);
            curr = curr.next;
        }
    }
}


public class try_DoubleLinkedList {
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        fl("1 2 3");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(2));
            dll.append(new Node(3));
            dll.print();
        }
        fl("remove(2) => 1 3");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(2));
            dll.append(new Node(3));
            dll.remove(new Node(2));
            dll.print();
        }
        fl("remove(1) => 2 3");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(2));
            dll.append(new Node(3));
            dll.remove(new Node(1));
            dll.print();
        }
        fl("remove(1, 3) => 2");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(2));
            dll.append(new Node(3));
            dll.remove(new Node(1));
            dll.remove(new Node(3));
            dll.print();
        }
        fl("remove(1, 2, 3) => ");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(2));
            dll.append(new Node(3));
            dll.remove(new Node(1));
            dll.remove(new Node(2));
            dll.remove(new Node(3));
            dll.print();
        }



        sw.printTime();
        end();
    }
    public static void test1() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        fl("addToSortedList(1) => ");
        {
            DLL dll = new DLL();
            dll.addToSortedList(new Node(1));
            dll.print();
        }
        fl("addToSortedList(0, 1) => ");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.addToSortedList(new Node(0));
            dll.print();
        }
        fl("addToSortedList(1, 2) => ");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.addToSortedList(new Node(2));
            dll.print();
        }
        fl("addToSortedList(1, 2, 3) => ");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(3));
            dll.addToSortedList(new Node(2));
            dll.print();
        }

        fl("addToSortedList(1, 3, 4, 4) => ");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(3));
            dll.append(new Node(4));
            dll.addToSortedList(new Node(4));
            dll.print();
        }
        fl("addToSortedList(1, 3, 4, 4, 4) => ");
        {
            DLL dll = new DLL();
            dll.append(new Node(1));
            dll.append(new Node(3));
            dll.append(new Node(4));
            dll.addToSortedList(new Node(4));
            dll.addToSortedList(new Node(4));
            dll.print();
        }

        sw.printTime();
        end();
    }
}

