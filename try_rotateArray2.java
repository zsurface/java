import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_rotateArray2{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

	int[][] arr = {
	    {1, 2, 3},
	    {4, 5, 6},
	    {7, 8, 9}
	};
	p(arr);
	fl();    
	rotateArray(arr);
	p(arr);
	 
        sw.printTime();
        end();
    }
    // - - - x
    // x - x x
    // x x 0 x
    // x 0 0 0
    // 
    public static void rotateArray(int[][] arr){
	if(arr != null && arr.length > 0){
	    int len = arr.length;
	    for(int k=0; k<len/2; k++){
		for(int i=k; i<len - 1 - k; i++){
		    int tmp = arr[k][i];
		    arr[k][i] = arr[len - 1 - i][k];
		    arr[len - 1 - i][k] = arr[len - 1 - k][len - 1 - i];
		    arr[len-1-k][len-1-i] = arr[i][len-1-k];
		    arr[i][len-1-k] = tmp;
		}
	    }
	    
	}
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();


	        int[][] arr = {  
	            {1}             
	        };               
	        p(arr);          
	        fl();            
	        rotateArray(arr);
	        p(arr);          

        sw.printTime();
        end();
    }
} 

