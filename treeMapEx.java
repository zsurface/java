import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Student{
    String name;
    Integer age;
    public Student(String name, Integer age){
        this.name = name;
        this.age = age;
    }
}

class CompareTree implements Comparator<Student>{
    public int compare(Student s1, Student s2){
        return s1.name.compareTo(s2.name);
    }
}

public class treeMapEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        TreeMap<Student, String> map = new TreeMap<>(new CompareTree());
        map.put(new Student("David", 3), "good");
        map.put(new Student("Michael", 4), "bad");

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
} 

