import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Node{
    String key;
    String data;
    Node next;
    public Node(String key, String data){
        this.key = key;
        this.data = data;
    }
}
class HashMap{
    int size;
    int c;
    Node[] arr;
    public HashMap(int size){
        this.size = size;
        c = 0;
        arr = new Node[this.size];
    }
    public void print(){
        for(int i=0; i<size; i++){
            Node curr = arr[i];
            while(curr != null){
                Print.pb(curr.data);
                curr = curr.next;
            }
            Print.pl("--");
        }
    }
    public int size(){
        return c;
    }
    public void remove(String key){
    }
    public void insert(String key, String data){
        if(key != null && data != null){
            if(c < (size + 10)){
                int value = key.hashCode() % size;
                Node curr = arr[value];
                if(curr == null){
                    arr[value] = new Node(key, data);
                    c++;
                }
                else{
                    boolean isSameKey = false;
                    Node prev = curr;
                    while(curr != null && !isSameKey){
                        if(curr.key.equals(key)){
                            curr.data = data;
                            isSameKey = true;
                        }
                        prev = curr;
                        curr = curr.next;
                    }
                    if(!isSameKey){
                        prev.next = new Node(key, data); 
                        c++;
                    }
                }
            }
        }
    }
}

public class HashMapEx{
    public static void main(String[] args) {
        test0();
        HashMap map = new HashMap(4);
        map.insert("k1", "dog");
        map.insert("k2", "cat");
        map.insert("k3", "pig");
        map.insert("k4", "fox");
        map.insert("k5", "cow");
        map.insert("k6", "rat");
        map.insert("k7", "ape");
        map.insert("k8", "eel");
        map.insert("k9", "cod");
        map.insert("k1", "god");

        map.print();
        Print.p("size=" + map.size());

        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
} 

