import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_replace{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        StringBuilder sb = new StringBuilder("Sellenium");
        int start = 1;
        int end = 2; // exclusive
        Print.p(replace(start, end, sb, "KKK").toString());

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        replace a string with other string
    }
    {@code
        StringBuilder sb = new StringBuilder("Sellenium");
        int start = 1;
        int end = 2; // exclusive
        Print.p(replace(start, end, sb, "KKK").toString());
        => SKKKllenium

        The code uses StringBuilder replace(int start, int end, String s)
    }
    </pre>
    */ 
    public static StringBuilder replace(int start, int end, StringBuilder sb, String rep){
        return sb.replace(start, end, rep);
    }
} 

