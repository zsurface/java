import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


class MyNode<T>{
    T data;
    Integer hash;
    MyNode left;
    MyNode right;
    public MyNode(){
    }
    public MyNode(T t){
	hash = t.hashCode();
	data = t;
    }
    @Override
    public String toString(){
	String s = "";
	if(data != null && hash != null){
	  s += "data=" + data.toString() + " ";
	  s += "hash=" + hash.toString();
	}
	return s;
    }
	
}

// KEY: node with hashcode,
public class try_bintree{
    public static void main(String[] args) {
        test0();
    }
    public static void insert(MyNode root, MyNode node){
	MyNode curr = root;
	if(curr == null)
	    curr = root = node;
	else{
	    while(curr != null){
		if(node.hash < curr.hash){
		    if(curr.left == null){
			curr.left = node;
			break;
		    }
		    else
			curr = curr.left;
		}else{
		    if(curr.right == null){
			curr.right = node;
			break;
		    }else
			curr = curr.right;
		}
	    }
	}
    }
    
    public static <T> void inorderRecurve(MyNode<T> root, List<T> ls){
	if(root != null){
	    inorderRecurve(root.left, ls);
	    ls.add(root.data);
	    inorderRecurve(root.right, ls);
	}
    }
    
    public static <T> List<T> inorder(MyNode<T> root){
	List<T> ls = new ArrayList<>();
	inorderRecurve(root, ls);
	return ls;
    }
    public static <T> Boolean contain(MyNode<T> root, MyNode<T> node){
	Boolean ret = false;
	MyNode<T> curr = root;
	if(curr == null){
	    return false;
	}else{
	    while(curr != null && !ret){
		if(node.hash < curr.hash){
		    curr = curr.left;
		}else if(node.hash > curr.hash){
		    curr = curr.right;
		}else{
		    ret = true;
		}
	    }
	}
	return ret;
    }
    public static void test0(){
        beg();
	{
	    MyNode<String> node = new MyNode();
	    pb(node.toString());
	}
	{
	    MyNode<String> node = new MyNode("abc");
	    pb(node.toString());
	}
	{
	    MyNode<String> node = new MyNode("abC");
	    pb(node.toString());
	}
	{
	    MyNode<String> root = new MyNode("1");
	    MyNode<String> node1 = new MyNode("2");
	    MyNode<String> node2 = new MyNode("3");
	    MyNode<String> node3 = new MyNode("4");
	    MyNode<String> node4 = new MyNode("5");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    List<String> ls = inorder(root);
	    pb(ls);
	}
	{
	    MyNode<String> root = new MyNode("2");
	    MyNode<String> node1 = new MyNode("1");
	    MyNode<String> node2 = new MyNode("3");
	    MyNode<String> node3 = new MyNode("4");
	    MyNode<String> node4 = new MyNode("5");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    List<String> ls = inorder(root);
	    pb(ls);
	}
	{
	    MyNode<String> root = new MyNode("2");
	    MyNode<String> node1 = new MyNode("1");
	    MyNode<String> node2 = new MyNode("3");
	    MyNode<String> node3 = new MyNode("5");
	    MyNode<String> node4 = new MyNode("4");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    List<String> ls = inorder(root);
	    pb(ls);
	}
	{
	    MyNode<String> root = null;
	    MyNode<String> node = new MyNode("4");
	    Boolean b = contain(root, node);
	    t(b, false);
	}
	{
	    MyNode<String> root = new MyNode<>("0");
	    MyNode<String> node = new MyNode("0");
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    MyNode<String> root = new MyNode<>("0");
	    MyNode<String> node = new MyNode("1");
	    Boolean b = contain(root, node);
	    t(b, false);
	}
	{
	    MyNode<String> root = new MyNode<>("5");
	    MyNode<String> node1 = new MyNode("6");
	    MyNode<String> node2 = new MyNode("4");
	    insert(root, node1);
	    insert(root, node2);
	    
	    MyNode<String> node = new MyNode("4");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    MyNode<String> root = new MyNode<>("5");
	    MyNode<String> node1 = new MyNode("6");
	    MyNode<String> node2 = new MyNode("4");
	    insert(root, node1);
	    insert(root, node2);
	    
	    MyNode<String> node = new MyNode("5");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    MyNode<String> root = new MyNode<>("5");
	    MyNode<String> node1 = new MyNode("6");
	    MyNode<String> node2 = new MyNode("4");
	    insert(root, node1);
	    insert(root, node2);
	    
	    MyNode<String> node = new MyNode("6");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    MyNode<String> root = new MyNode<>("5");
	    MyNode<String> node1 = new MyNode("6");
	    MyNode<String> node2 = new MyNode("4");
	    insert(root, node1);
	    insert(root, node2);
	    
	    MyNode<String> node = new MyNode("5");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    /**
	                    9
                      5          11
                   2     7    10      13
	     */
	    MyNode<String> root = new MyNode<>("9");
	    MyNode<String> node1 = new MyNode("5");
	    MyNode<String> node2 = new MyNode("11");
	    MyNode<String> node3 = new MyNode("2");
	    MyNode<String> node4 = new MyNode("7");
	    MyNode<String> node5 = new MyNode("10");
	    MyNode<String> node6 = new MyNode("13");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    insert(root, node5);
	    insert(root, node6);
	    
	    MyNode<String> node = new MyNode("5");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    /**
	                    9
                      5          11
                   2     7    10      13
	     */
	    MyNode<String> root = new MyNode<>("9");
	    MyNode<String> node1 = new MyNode("5");
	    MyNode<String> node2 = new MyNode("11");
	    MyNode<String> node3 = new MyNode("2");
	    MyNode<String> node4 = new MyNode("7");
	    MyNode<String> node5 = new MyNode("10");
	    MyNode<String> node6 = new MyNode("13");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    insert(root, node5);
	    insert(root, node6);
	    
	    MyNode<String> node = new MyNode("13");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    /**
	                    9
                      5          11
                   2     7    10      13
	     */
	    MyNode<String> root = new MyNode<>("9");
	    MyNode<String> node1 = new MyNode("5");
	    MyNode<String> node2 = new MyNode("11");
	    MyNode<String> node3 = new MyNode("2");
	    MyNode<String> node4 = new MyNode("7");
	    MyNode<String> node5 = new MyNode("10");
	    MyNode<String> node6 = new MyNode("13");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    insert(root, node5);
	    insert(root, node6);
	    
	    MyNode<String> node = new MyNode("11");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    /**
	                    9
                      5          11
                   2     7    10      13
	     */
	    MyNode<String> root = new MyNode<>("9");
	    MyNode<String> node1 = new MyNode("5");
	    MyNode<String> node2 = new MyNode("11");
	    MyNode<String> node3 = new MyNode("2");
	    MyNode<String> node4 = new MyNode("7");
	    MyNode<String> node5 = new MyNode("10");
	    MyNode<String> node6 = new MyNode("13");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    insert(root, node5);
	    insert(root, node6);
	    
	    MyNode<String> node = new MyNode("9");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    /**
	                    9
                      5          11
                   2     7    10      13
	     */
	    MyNode<String> root = new MyNode<>("9");
	    MyNode<String> node1 = new MyNode("5");
	    MyNode<String> node2 = new MyNode("11");
	    MyNode<String> node3 = new MyNode("2");
	    MyNode<String> node4 = new MyNode("7");
	    MyNode<String> node5 = new MyNode("10");
	    MyNode<String> node6 = new MyNode("13");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    insert(root, node5);
	    insert(root, node6);
	    
	    MyNode<String> node = new MyNode("5");
		    
	    Boolean b = contain(root, node);
	    t(b, true);
	}
	{
	    /**
	                    9
                      5          11
                   2     7    10      13
	     */
	    MyNode<String> root = new MyNode<>("9");
	    MyNode<String> node1 = new MyNode("5");
	    MyNode<String> node2 = new MyNode("11");
	    MyNode<String> node3 = new MyNode("2");
	    MyNode<String> node4 = new MyNode("7");
	    MyNode<String> node5 = new MyNode("10");
	    MyNode<String> node6 = new MyNode("13");
	    insert(root, node1);
	    insert(root, node2);
	    insert(root, node3);
	    insert(root, node4);
	    insert(root, node5);
	    insert(root, node6);
	    
	    MyNode<String> node = new MyNode("14");
		    
	    Boolean b = contain(root, node);
	    t(b, false);
	}
	
        end();
    }


} 

