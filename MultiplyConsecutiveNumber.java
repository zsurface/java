import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class MultiplyConsecutiveNumber{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();
        StopWatch sw = new StopWatch();
        sw.start();

        for(int i = 2; i < 1000000; i++){
           for(int j = 2; j < 1000000; j++){
                if(isIncNumber(i) && isIncNumber(j)){
                  long n = i*j;
                  if(isIncNumber(n)){
                    pl("i=" + i + "  j=" + j + " n=" + n);
                  }
                }
            }
         }
        sw.printTime();
        end();
    }
} 

