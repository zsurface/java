import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class MaxNestedBracket{
    public static void main(String[] args) {
        test0();
        test1();
        test_balancedBracket();
        test_maxNestedBreacket();
    }
  public static Boolean balancedBracket(String str){
    int max = 0;
    Stack<String> s = new Stack();
    for(int i = 0; i < len(str); i++){
      if(str.charAt(i) == '('){
        s.add("(");
      }else if(str.charAt(i) == ')'){
        s.pop();
      }
    }
    return s.isEmpty();
  }
  public static int maxNestedBracket(String str){
    int max = 0;
    Stack<String> s = new Stack();
    for(int i = 0; i < len(str); i++){
      if(str.charAt(i) == '('){
        s.add("(");
        if(s.size() > max)
          max = s.size();
      }else if(str.charAt(i) == ')'){
        s.pop();
      }
    }
    return max;
  }

  public static void test_maxNestedBreacket(){
    beg();
    {
      String s = "";
      int max = maxNestedBracket(s);
      t(max, 0);
    }
    {
      String s = "()";
      int max = maxNestedBracket(s);
      t(max, 1);
    }
    {
      String s = "()()";
      int max = maxNestedBracket(s);
      t(max, 1);
    }
    {
      String s = "()()()";
      int max = maxNestedBracket(s);
      t(max, 1);
    }
    {
      String s = "(())";
      int max = maxNestedBracket(s);
      t(max, 2);
    }
    {
      String s = "(())()";
      int max = maxNestedBracket(s);
      t(max, 2);
    }
    {
      String s = "(()(()))";
      int max = maxNestedBracket(s);
      t(max, 3);
    }
    end();
  }
  
  public static void test_balancedBracket(){
    beg();
    {
      String s = "";
      Boolean b = balancedBracket(s);
      t(b, true);
    }
    {
      String s = "(";
      Boolean b = balancedBracket(s);
      t(b, false);
    }
    {
      String s = "()";
      Boolean b = balancedBracket(s);
      t(b, true);
    }
    {
      String s = "(())";
      Boolean b = balancedBracket(s);
      t(b, true);
    }
    {
      String s = "(()()";
      Boolean b = balancedBracket(s);
      t(b, false);
    }
    {
      String s = "(()()";
      Boolean b = balancedBracket(s);
      t(b, false);
    }
    {
      String s = "(()())";
      Boolean b = balancedBracket(s);
      t(b, true);
    }
    end();
  }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        String path = System.getProperty("java.class.path");
        pp(path);

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        sw.printTime();
        end();
    }
} 

