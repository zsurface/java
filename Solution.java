import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class Solution{
    public static void main(String[] args) {
        test1();
        test0();
        List<String> ls = new ArrayList<>();
        ls.add("dog");
        fun(ls);

        test00();
    }
    public static void test00(){
        {
            List<Integer> ls = list(1, 2, 3);
            p(ls);
        }
        {
            List<String> ls = list("dog", "cat");
            p(ls);
        }
    }

    @SafeVarargs // Not actually safe!
    static void fun(List<String>... stringLists) {
       p(stringLists);
//       Object[] array = stringLists;
//       List<Integer> tmpList = Arrays.asList(42);
//       array[0] = tmpList; // Semantically invalid, but compiles without warnings
//       String s = stringLists[0].get(0); // Oh no, ClassCastException at runtime!
    }

    public static void test0(){
        {
            List<Integer> ls = Arrays.asList(1, 2, 3);
        }
        {
            Integer[] arr2d = {1};
            List<Integer> lot = Arrays.asList(arr2d);
        }
        {
            Integer[][] arr2d = {{1}};
            ArrayList<List<Integer>> lot = new ArrayList<>(); 
            for(int i=0; i<1; i++){
                List<Integer> ls = new ArrayList<>();
                for(int j=0; j<1; j++){
                    ls.add(arr2d[i][j]);
                }
                lot.add(ls);
            }
        }
        {
            Integer[][] arr2d = {{1}};
            ArrayList<List<Integer>> lot = new ArrayList<>(); 
            List<Integer> ls = new ArrayList<>();
            for(int i=0; i<1; i++){
                lot.add(Arrays.asList(arr2d[i]));
            }
        }
    }
    public static void test1(){
        {
//            fl();
//            Integer[][] arr2d = {
//                        {1},
//                        {1}
//                    }; 
//
//            // ArrayList<ArrayList<Integer>> lot = arrayToList2a(arr2d);
//            ArrayList<ArrayList<Integer>> lot = new ArrayList<>(); 
//            int height = arr2d.length;
//            int width = arr2d[0].length;
//
//            for(int h=0; h < height; h++){
//                ArrayList<Integer> ls = new ArrayList<>();
//                for(int w=0; w < width; w++){
//                    ls.add(arr2d[h][w]);
//                }
//                lot.add(ls);
//            }
//            int min = minimumDistance(height, width, lot);
//            t(min == -1); 
        }

//        {
//            fl();
//            int[][] arr2d = {
//                {1, 1, 1, 1},
//                {0, 1, 1, 1},
//                {0, 1, 0, 1},
//                {1, 1, 9, 1},
//                {0, 0, 1, 1}
//            }; 
//            List<List<Integer>> lot = Arrays.asList(arr2d);
//            int height = arr2d.length;
//            int width = arr2d[0].length;
//            int min = minimumDistance(height, width, lot);
//            t(min == 5); 
//        }
//
//        {
//            fl();
//            int[][] arr2d = {
//                {1, 1, 1},
//                {0, 0, 1},
//                {0, 0, 9}
//            }; 
//            List<List<Integer>> lot = Arrays.asList(arr2d);
//            int height = arr2d.length;
//            int width = arr2d[0].length;
//            int min = minimumDistance(height, width, lot);
//            t(min == 4); 
//        }
//
//        {
//            fl();
//            int[][] arr2d = {
//                {9}
//            }; 
//            List<List<Integer>> lot = Arrays.asList(arr2d);
//            int height = arr2d.length;
//            int width = arr2d[0].length;
//            int min = minimumDistance(height, width, lot);
//            t(min == 0); 
//        }
//
//
//        {
//            fl();
//            int[][] arr2d = {
//                {0}
//            }; 
//            List<List<Integer>> lot = Arrays.asList(arr2d);
//            int height = arr2d.length;
//            int width = arr2d[0].length;
//            int min = minimumDistance(height, width, lot);
//            t(min == -1); 
//        }
//
//
//        {
//            fl();
//            int[][] arr2d = {
//                {1}
//            }; 
//            List<List<Integer>> lot = Arrays.asList(arr2d);
//            int height = arr2d.length;
//            int width = arr2d[0].length;
//            int min = minimumDistance(height, width, lot);
//            t(min == -1); 
//        }


    }
    public static int minimumDistance(int numRows, int numColumns, ArrayList<ArrayList<Integer>> lot){
        int c = 0;
        int r = 0;
        int min = search(numRows, r, numColumns, c, lot);
        fl();
        p(min);
        if(min > numRows * numColumns)
            return -1;
        else 
            return min;

    }
    public static int search(Integer numRows, Integer r, Integer numColumns, Integer c, ArrayList<ArrayList<Integer>> lot){
            int max = numRows * numColumns;
        if(lot != null){
            if(lot.get(r).get(c) == 9){
                return 0;
            } else if(lot.get(r).get(c) == 1){
                lot.get(r).set(c, 0);
                int c1 = max, c2 = max, c3 = max, c4 = max;
                if(r + 1 < numRows) 
                  c1 = search(numRows, r+1, numColumns, c, lot);   
                if(r - 1 >= 0)     
                  c2 = search(numRows, r-1, numColumns, c, lot);   
                if(c + 1 < numColumns)
                  c3 = search(numRows, r, numColumns, c + 1, lot);   
                if(c - 1 >= 0)
                  c4 = search(numRows, r, numColumns, c - 1, lot);   
                return Math.min(Math.min(c1, c2), Math.min(c3, c4)) + 1;
            }
                 
        }
        return max;
    }
    
    public static int countConnection(int[][] arr, int w, int width, int h, int height) {
        if(arr != null) {
            if(arr[h][w] == 1) {
                arr[h][w] = 2;
                int right = 0, left = 0, up = 0, down = 0;
                if(w + 1 < width)
                    right =countConnection(arr, w+1, width, h, height);
                if(w - 1 >= 0)
                    left = countConnection(arr, w-1, width, h, height);
                if(h - 1 >= 0)
                    up =   countConnection(arr, w, width, h-1, height);
                if(h + 1 < height)
                    down = countConnection(arr, w, width, h+1, height);

                return right + left + up + down + 1;
            }
        }
        return 0;
    }
} 

