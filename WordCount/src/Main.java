import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import classfile.Tuple;
import java.util.stream.*;
import java.util.stream.Collectors;


// KEY: run command, run cmd, shell command, exec command, system
public class Main {
    public static void main(String args[]) {
        Tuple<List<String>, List<String>> pair = exec("ls");
        fl("ls");
        p(unlines(pair.x));

        List<String> output = run("ls");
        p(output);

    }
    public static List<String> run(String cmd) {
        Tuple<List<String>, List<String>> pair = exec(cmd);
        return pair.x;
    }

    public static Tuple<List<String>, List<String>>  exec(String cmd){
        String s = null;
        List<String> output = of();
        List<String> error = of();

        try {
            // using the Runtime exec method:
            Process p = Runtime.getRuntime().exec(cmd);

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            // read the output from the command
            while ((s = stdInput.readLine()) != null) {
                output.add(s);
            }

            // read any errors from the attempted command
            while ((s = stdError.readLine()) != null) {
                error.add(s);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        Tuple<List<String>, List<String>> pair = new Tuple(output, error);
        return pair;
    }


}