import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;


class Country implements Comparable<Country>{
    private String name;

    public int compareTo(Country other){
        return name.compareTo(other.name);
    }
    public Country(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return name;
    }
}
public class try_map1{
    public static void main(String[] args) {
//        test0();
//        test1();
        test2();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        streamCopyFile("/tmp/11.x", "/tmp/99.x");

        Aron.end();
    }

    public static void test1(){
        Aron.beg();

        Map<String, String> map = new HashMap<>();
        map.put("k1", "dog");
        map.put("k2", "cat");

        for(Map.Entry<String, String> entry : map.entrySet()){
            Print.p("k=" + entry.getKey() + "v=" + entry.getValue());
        }

        String fname = "/tmp/11.x";
        List<List<String>> ls = readFile(fname);

        writeToFile("/tmp/22.x", ls);


        Print.p(ls);

        Aron.end();
    }


    //    /*
    //     * To execute Java, please define "static void main" on a class
    //     * named Solution.
    //     *
    //     * If you need more classes, simply define them inline.
    //     */
    //
    //  /**
    //   * Takes a list of available countries and returns a list of its elements re-ordered.  
    //   * 
    //   * The new order of the elements is as follows:
    //   * 
    //   * First the special countries in their original order from the special countries list.
    //   * Then the list of available countries, sorted alphabetically by name.
    //   * 
    //   * Not all of the special countries may appear in the list of available countries.
    //   * 
    //   * Available: United States, Mexico, Canada, Costa Rica, Bermuda, Belgium
    //   * Special: Canada, Mexico, France, Belgium
    //   * Result: Canada, Mexico, Belgium, Bermuda, Costa Rica, United States
    //   */
    public static void test2(){
//        Available: United States, Mexico, Canada, Costa Rica, Bermuda, Belgium
//        Special: Canada, Mexico, France, Belgium
        Country a1 = new Country("United States");
        Country a2 = new Country("Mexico");
        Country a3 = new Country("Canada");
        Country a4 = new Country("Costa Rica");
        Country a5 = new Country("Bermuda");
        Country a6 = new Country("Belgium");

        Country c1 = new Country("Canada");
        Country c2 = new Country("Mexico");
        Country c3 = new Country("France");
        Country c4 = new Country("Belgium");

        List<Country> la = Arrays.asList(a1, a2, a3, a4, a5, a6);
        List<Country> ls = Arrays.asList(c1, c2, c3, c4);
        List<Country> list = AppleSortedCountry(la, ls);
        for(Country c : list){
            Print.p("list=" + c.getName());
        }

    }

    /** 
        S - (S - (A int S))  -> keep the order of Special 
        A - (A int S)  -> sort the Available

        Belgium
        Bermuda
        Canada
        Costa Rica
        Mexico
        United States

    //   * Available: United States, Mexico, Canada, Costa Rica, Bermuda, Belgium
    //   * Special: Canada, Mexico, France, Belgium
    //   * Result: Canada, Mexico, Belgium, Bermuda, Costa Rica, United States

    */
    public static List<Country> AppleSortedCountry(List<Country> a, List<Country> s){
        if(a != null && s != null){
            // S - (S -(A int S))
            List<Country> is = Aron.intersect(a, s);
            for(Country c: is){
                Print.p("is=" + c.getName());
            }
            List<Country> sis = Aron.subtract(s, is);
            for(Country c : sis){
                Print.p("sis=" + c.getName());
            }
            List<Country> ss = Aron.subtract(s, sis);
            for(Country c: ss){
                Print.p("ss=" + c.getName());
            }
            // A - (A int S)
            List<Country> aa = Aron.subtract(a, is);

            for(Country c: aa){
                Print.p("aa=" + c.getName());
            }

            Collections.sort(aa);
            return Aron.concatList(ss, aa);
        }
        throw new IllegalArgumentException("Arguments can not be null.");
    }


    public static List<List<String>> readFile(String fname){
        List<List<String>> ls = new ArrayList<>();
        try{
            BufferedReader br = new BufferedReader(new FileReader(fname));
            String line = null;
            while( (line = br.readLine()) != null){
                String[] arr = line.split("\\s+");
                List<String> ss = Arrays.asList(arr); // array to list
                ls.add(ss);
                // Print.p(arr);
            }

            if(br != null)
                br.close();

        }catch(IOException io){
            io.printStackTrace();
        }
        return ls;
    }

    public static void writeToFile(String fname, List<List<String>> ls){
        if(fname != null && ls != null){
            try{
                BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
                for(List<String> line: ls){
                    for(String s: line){
                        bw.write(s);
                    }
                    bw.write("\n");
                }
                if(bw != null)
                    bw.close();

            }catch(IOException io){
                io.printStackTrace();
            }
        }
    }
    
    /**
    <pre>
    {@literal
        Copy file1 to file2
        stream copy file, FileInputStream, FileInputStream
    }
    {@code
        String fname1 = "/tmp/f1.x"
        String fname2 = "/tmp/f2.x"
        streamCopyFile(fname1, fname2);
    }
    </pre>
    */ 
    public static void streamCopyFile(String fin, String fout){
        if(fin != null && fout != null){
            try{
                FileInputStream is = new FileInputStream(fin);
                FileOutputStream os = new FileOutputStream(fout);
                int c = -1;
                while((c = is.read()) != -1){
                    os.write(c);
                }
                if(is != null)
                    is.close();
                if(os != null)
                    os.close();

            }catch(IOException io){
                io.printStackTrace();
            }
        }else{
            throw new IllegalArgumentException("File name can not be null");
        }
    }
} 

