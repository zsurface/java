import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

class Person{
    Integer age;
    public Person(Integer age){
        this.age = age;
    }
}
public class QuickSortFun{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            Person[] arr = {new Person(0)};
            int lo = 0;
            int hi = arr.length - 1;
            quickSortFun((x, y) -> x.age <= y.age, arr, lo, hi);

            List<Integer> ln = new ArrayList<>();
            List<Integer> exp = Arrays.asList(0); 
            for(Person pp : arr){
                ln.add(pp.age);
            }
            t(ln, exp);
        }
        {
            Person[] arr = {new Person(0), 
                            new Person(1)
                            };
            int lo = 0;
            int hi = arr.length - 1;
            quickSortFun((x, y) -> x.age <= y.age, arr, lo, hi);

            List<Integer> ln = new ArrayList<>();
            List<Integer> exp = Arrays.asList(0, 1); 
            for(Person pp : arr){
                ln.add(pp.age);
            }
            t(ln, exp);
        }

        {
            Person[] arr = {new Person(0), 
                            new Person(2), 
                            new Person(1)
                            };
            int lo = 0;
            int hi = arr.length - 1;
            quickSortFun((x, y) -> x.age <= y.age, arr, lo, hi);

            List<Integer> ln = new ArrayList<>();
            List<Integer> exp = Arrays.asList(0, 1, 2); 
            for(Person pp : arr){
                ln.add(pp.age);
            }
            t(ln, exp);
        }

        sw.printTime();
        end();
    }

    
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        sw.printTime();
        end();
    }
} 

