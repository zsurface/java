import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_spiral{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        {
            int[][] arr2d = Aron.geneMatrix1toN(1, 1);
            Print.p(arr2d);

            int height = arr2d.length;
            int width = arr2d[0].length; 
            
            List<Integer> list1 = Arrays.asList(1);
            ArrayList<Integer> list = printSpiral(arr2d);
            Test.t(list1, list);
        }
        {
            // { 
            //  {1},
            //  {2}
            // } 
            int[][] arr2d = Aron.geneMatrix1toN(2, 1);
            Print.p(arr2d);

            int height = arr2d.length;
            int width = arr2d[0].length; 

            List<Integer> list1 = Arrays.asList(1, 2);

            ArrayList<Integer> mylist = printSpiral(arr2d);
            Test.t(list1, mylist);
            Print.p(arr2d);
        }
        {
            // { 
            //  {1, 2},
            // } 
            int[][] arr2d = Aron.geneMatrix1toN(1, 2);
            Print.p(arr2d);

            int height = arr2d.length;
            int width = arr2d[0].length; 

            List<Integer> list1 = Arrays.asList(1, 2);

            ArrayList<Integer> mylist = printSpiral(arr2d);
            Test.t(list1, mylist);
            Print.p(arr2d);
        }
        {
            // { 
            //  {1, 2},
            //  {3, 4}
            // } 
            int[][] arr2d = Aron.geneMatrix1toN(2, 2);
            Print.p(arr2d);

            int height = arr2d.length;
            int width = arr2d[0].length; 

            List<Integer> list1 = Arrays.asList(1, 2, 4, 3);

            ArrayList<Integer> mylist = printSpiral(arr2d);
            Test.t(list1, mylist);
            Print.p(arr2d);
        }
        {
            // { 
            //  {1, 2, 3},
            //  {4, 5, 6}
            // } 
            int[][] arr2d = Aron.geneMatrix1toN(2, 3);
            Print.p(arr2d);

            int height = arr2d.length;
            int width = arr2d[0].length; 

            List<Integer> list1 = Arrays.asList(1, 2, 3, 6, 5, 4);

            ArrayList<Integer> mylist = printSpiral(arr2d);
            Test.t(list1, mylist);
            Print.p(arr2d);
        }
        {
            // { 
            //  {1, 2},
            //  {3, 4},
            //  {5, 6}
            // } 
            int[][] arr2d = Aron.geneMatrix1toN(3, 2);
            Print.p(arr2d);

            int height = arr2d.length;
            int width = arr2d[0].length; 

            List<Integer> list1 = Arrays.asList(1, 2, 4, 6, 5, 3);

            ArrayList<Integer> mylist = printSpiral(arr2d);
            Test.t(list1, mylist);
            Print.p(arr2d);
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        Given 2d array m and n, print the spiral from the array 
        If the 2d array is square, then it can be used rotate 90 degrees
        algorithm to print all the elements out

        If 2d array is NOT square
            if the height <= width of the array
                the height of array will be shrunk to 1 if the height is odd
            similarly, it is applied on width
    }
    {@code

            // { 
            //  {1, 2, 3},
            //  {4, 5, 6}
            // } 
            int[][] arr2d = Aron.geneMatrix1toN(2, 3);
            Print.p(arr2d);

            int height = arr2d.length;
            int width = arr2d[0].length; 

            List<Integer> list1 = Arrays.asList(1, 2, 3, 6, 5, 4);

            ArrayList<Integer> mylist = printSpiral(arr2d);
            Test.t(list1, mylist);
            Print.p(arr2d);
    }
    </pre>
    */ 
    public static ArrayList<Integer> printSpiral(int[][] arr){
        ArrayList<Integer> list = new ArrayList<Integer>();
        int height = arr.length;
        int width  = arr[0].length; 
        int k = 0;
        while(k <= Math.min(height, width)/2){
                if(height - 2*k == 1){
                    for(int j=k; j<width - k; j++){
                        // Print.pp(arr[k][j] + " ");        
                        list.add(arr[k][j]);
                    }
                }else if(width - 2*k == 1){
                    for(int j=k; j<height - k; j++){
                        // Print.pp(arr[j][k] + " ");        
                        list.add(arr[j][k]);
                    }
                }else{
                    for(int j=k; j<width - 1 - k; j++){
                        // Print.pp(arr[k][j] + " ");        
                        list.add(arr[k][j]);
                    }
                    for(int j=k; j<height - 1 - k; j++){
                        // Print.pp(arr[j][width - 1 - k] + " ");        
                        list.add(arr[j][width - 1 - k]);
                    }
                    for(int j=k; j<width - 1 - k; j++){
                        // Print.pp(arr[height - 1 - k][width - 1 - j] + " ");
                        list.add(arr[height - 1 - k][width - 1 - j]);
                    }
                    for(int j=k; j<height - 1 - k; j++){
                        // Print.pp(arr[height - 1 - j][k] + " ");
                        list.add(arr[height - 1 - j][k]);
                    }
                }
                k++;
        }
        return list;
    }
} 

