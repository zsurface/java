import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_pa{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        Integer n = palindrome("abaaa");
        p("n=" + n);


        sw.printTime();
        end();
    }

    public static Integer palindrome(String s) { 
        TreeMap<String , Integer> map = new TreeMap<>(); 
        int len = s.length(); 
        int[][] rst = new int[2][len+1]; 
        s = "@" + s + "#"; 
        for (int j = 0; j <= 1; j++) { 
            int count = 0;   
            rst[j][0] = 0; 
       
            int i = 1; 
            while (i <= len) { 
                while (s.charAt(i - count - 1) == s.charAt(i +  j + count)) count++;  
                rst[j][i] = count; 
                int k = 1; 
                while ((rst[j][i - k] != count - k) && (k < count)) { 
                    rst[j][i + k] = Math.min(rst[j][i - k],  
                                              count - k); 
                    k++; 
                } 
                count = Math.max(count - k,0); 
                i += k; 
            } 
        } 
       
        s = s.substring(1, s.length()-1); 
       
        map.put(s.substring(0,1), 1); 
        for (int i = 1; i < len; i++) { 
            for (int j = 0; j <= 1; j++) 
                for (int count = rst[j][i]; count > 0; count--) 
                   map.put(s.substring(i - count - 1,  i - count - 1 + 2 * count + j), 1); 
            map.put(s.substring(i, i + 1), 1); 
        } 
       
      return map.size();
    } 
} 

