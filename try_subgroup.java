import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_subgroup{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
	{
	    Integer n = 0;
	    Integer m = 2;
	    var ls = toBase(n, m);
	    t(ls, list(0));
	}
	{
	    Integer n = 1;
	    Integer m = 2;
	    var ls = toBase(n, m);
	    t(ls, list(1));
	}
	{
	    Integer n = 2;
	    Integer m = 2;
	    var ls = toBase(n, m);
	    t(ls, list(1, 0));
	}
	{
	    Integer n = 9;
	    Integer m = 2;
	    var ls = toBase(n, m);
	    t(ls, list(1, 0, 0, 1));
	}
	{
	    Integer n = 9;
	    Integer m = 2;
	    var ls = toBase(n, m);
	    t(ls, list(1, 0, 0));
	}
        end();
    }
    /**
           0 1 2  => i / 3x1=> 0
           3 4 5 6 7 8  i / 3x2 => 1
     */
    public static void test1(){
        beg();
	{
	    Integer m = 3;
	    Integer count = 3;
	    for(int i = 0; i < count; i++){
		int k = i/m;
		p(shiftLeft(toBase(i, m), 0));
	    }
	}
	{
	    
	}
        end();
    }
    /**
           9 
           9 % 2 => 1
           9 / 2 =  4
          
           4 % 2 => 0
           4 / 2    2

           2 % 2 => 0
           2 / 2    1

           1 % 2 => 1
           1 / 2    0

           9 =>     1001


           0 1 2 
           0 1 2 3
           3 / 3 => 1, 3 % 3 = 0
           3^1 => 0
        
           4 / 3 => 1, 4 % 3 = 1
           1

	   n: 0 1 2
           k = n/3
           n % 3*(k+1)  => 0 1 2

           n: 3
           k = n/3 => 1
           n % 3*(1+1)  =>


         1 2 3 
         0 1 2 
     */
    public static List<Integer> toBase(Integer n, Integer m){
	List<Integer> ret = list();
	if(n == 0){
	    ret = cons(0, ret);
	}else{
	    Integer q = 0;
	    while(n > 0){
		Integer qq = n / m;
		if(qq > q){
		    n = n - pow(m, (q + 1));
		    q = qq;
		}else{
		    Integer r = n % m;
		    ret = cons(r, ret);
		    n = qq;
		}
	    }
	}
	return ret;
    }

    
    public static void fung(){
	Integer m = 3;
	Integer q = 0;
	Integer n = 10;
	while(n > 0){
	    Integer qq = n / m;
	    Integer r = n % m;
	    if(qq > q)
		n = n - pow(m, (q + 1));
	    else{
		n = qq;
	    }
	}
    }
    public static Integer fun(Integer n, Integer b){
	Integer sum = 0;
	for(int i= 1; i < n; i++){
	    sum += pow(b, i);
	}
	return sum;
    }
} 

