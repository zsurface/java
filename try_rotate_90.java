import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_rotate_90{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        
        int[][] arr2d = {
            { 1,   2,   3,  4},
            { 5,   6,   7,  8},
            { 9,   10,  11, 12},
            { 13,  14,  15, 16},
        };
        Aron.printArray2D(arr2d);
        int height = arr2d.length;
        int width = arr2d[0].length; 

        rotate90degree(arr2d);
        Print.p(arr2d);
        

        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        {
            int[][] arr2d = Aron.geneMatrix1toN(5, 5); 

            Aron.printArray2D(arr2d);
            int height = arr2d.length;
            int width = arr2d[0].length; 

            rotate90degree(arr2d);
            Print.p(arr2d);
        }
        {
            int[][] arr2d = Aron.geneMatrix1toN(1, 1); 

            Aron.printArray2D(arr2d);
            int height = arr2d.length;
            int width = arr2d[0].length; 

            rotate90degree(arr2d);
            Print.p(arr2d);
        }

        Aron.end();
    }
    public static void rotate90degree(int[][] arr){
        int len = arr.length;
        // reflect on y = x 
        for(int i=0; i<len; i++){
            for(int j=i; j<len; j++){
                int tmp = arr[i][j];
                arr[i][j] = arr[j][i];
                arr[j][i] = tmp;
            }
        }
        // reflect on x = len/2 
        for(int i=0; i<len; i++){
            for(int j=0; j<len/2; j++){
                int tmp = arr[i][j];
                arr[i][j] = arr[i][len-1-j];
                arr[i][len-1-j] = tmp;
            }
        }
    }
} 

