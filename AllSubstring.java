import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.Aron;
import classfile.Print;
import java.util.stream.*;
import java.util.stream.Collectors;

public class AllSubstring{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String s = "abcd";
        List<String> ls = substringAll(s);
        Print.p("ls=" + ls);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        List<Integer> lin = Arrays.asList(1, 2, 3, 4);
        List<String> lsss = lin.stream().map(Aron::intToStr).collect(Collectors.toList());
        Print.p("lsss=" + lsss);

        String ss = "";
        for(Integer n : lin){
            ss += "" + n;
        }
        Print.p("ss=" + ss);

        List<Integer> lss = lin.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
        Print.p("lss=" + lss);

        Integer sum = lin.stream().reduce(0, (x, y) -> x + y);
        Print.p("sum=" + sum);


        // join strings
        List<String> list1 = Arrays.asList("cat1", "dog1", "cow1");
        String rstr = Aron.drop(1, list1.stream().reduce("", (x, y) -> x + "," + y));
        Print.p("rstr=" + rstr);

        List<String> ls = list1.subList(0, 2);
        Print.p("ls=" + ls);

        int len = list1.size();
        for(int i=0; i<len; i++){
            Print.p(list1.subList(0, i+1));
        } 
        

        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        Generate all substrings, empty string is exclusive 
    }
    {@code
    }
    </pre>
    */ 
    public static List<String> substringAll(String s){
        List<String> list = new ArrayList<>();
        if( s != null){
            int len = s.length();
            for(int k=0; k<len; k++){
                for(int i=0; i<len; i++){
                    if(k < i + 1){
                        String sub = s.substring(k, i+1);  // a, ab, abc, abcd
                        list.add(sub);
                    }
                }
            }
        }
        return list;
    }
} 

