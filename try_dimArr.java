import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import classfile.Tuple;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.lang.reflect.Array;


public class try_dimArr{
    public static void main(String[] args) {
        test0();
    }

    public static void checkArray(){
          int[] arr = {6, 1, 9, 3, 7};
          Class c = arr.getClass();
          if (c.isArray()) {
             Class arrayType = c.getComponentType();
             System.out.println("The array is of type: " + arrayType);
             System.out.println("The length of the array is: " + Array.getLength(arr));
             System.out.print("The array elements are: ");
             for(int i: arr) {
                System.out.print(i + " ");
             }
          }
    }
    public static void test0(){
        beg();
        {
            Integer[][] arr = {{1}};
            Tuple<Integer, Integer> tup = dimArr2(arr);
            t(tup.x, 1);
            t(tup.y, 1);
        }
        {
            int[][] arr = {
            {1, 2},
            {1, 2},
            {1, 2}
            };
            Tuple<Integer, Integer> tup = dimArr2(arr);
            t(tup.x, 3);
            t(tup.y, 2);
        }

        end();
    }
} 

