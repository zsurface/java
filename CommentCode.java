import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
   <pre>
   {@literal
   Wednesday, 21 December 2022 22:56 PST
   KEY: alignment lines
        
   NOTE:
   }
   {@code
     
   xⁿ + yⁿ = zⁿ + wⁿ
   x² + y² = z²
   ⇓
   xⁿ + yⁿ = zⁿ + wⁿ
   x² + y² = z²

   alignment2("=", ls) use "=" as delimiter
   alignment2("", ls)  use space as delimiter
   }
   java_compileToExec.sh $j/Alignment.java  => $b/javaclass
   java_compileToExec.sh $j/CommentCode.java  => $b/javaclass
   java_compileToExec.sh $j/UncommentCode.java => $b/javaclass
   </pre>
*/ 
public class CommentCode{
  public static void main(String[] args) {
    // $b/tmp/x.x
    String fname = getEnv("mytmp");
    if(len(args) == 2){
	List<String> ls = new ArrayList<>();
	// "-f" is NOT been used any more
	if(args[0].compareTo("-f") == 0){
	    ls = readFileNoTrim(fname);
		
	// "-p" => read from stdin or pipe
	// Read from pipe, SEE: Wailib.hs 
	}else if(args[0].compareTo("-p") == 0){
	    ls = readStdin();
	}
	    
	
	var lss         = commentCode(args[1], ls);	
	var str = len(joinListStr(lss)) > 0 ? joinListStr(lss) : "";
 	System.out.print(str);
    }
    
    // test_commentCode();
    // test_uncommentCode();
  }

  public static List<String> commentCode(String lang, List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      for(int i = 0; i < len(ls); i++){
        String line = ls.get(i);
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', line);
        if(lang.equals("haskell")){
          // Line has been commented  => " -- fold"
          if(isEmptyLine(line) || (len(t.y) >= 2 && take(2, t.y).equals("--"))) {
            ret.add(line);
          }else{
            ret.add(t.x + "-- " + t.y);
          }
        }
        else if(lang.equals("java") || lang.equals("cpp")){
          if(isEmptyLine(line) || (len(t.y) >= 2 && take(2, t.y).equals("//"))) {
            ret.add(line);
          }else{
            ret.add(t.x + "// " + t.y);
          }
        }
        else if(lang.equals("elisp")){
          if(isEmptyLine(line) || (len(t.y) >= 2 && take(2, t.y).equals(";;"))) {
            ret.add(line);
          }else{
            ret.add(t.x + ";; " + t.y);
          }
        }
		else if(lang.equals("latex")){
          if(isEmptyLine(line) || (len(t.y) >= 1 && take(1, t.y).equals("%"))) {
            ret.add(line);
          }else{
            ret.add(t.x + "% " + t.y);
          }
        }
        else if(lang.equals("bash") || lang.equals("python")){
          if(isEmptyLine(line) || (len(t.y) >= 1 && take(1, t.y).equals("#"))) {
            ret.add(line);
          }else{
            ret.add(t.x + "# " + t.y);
          }
        }
      }
    }
    return ret;
  }

  
  /**
   */
  public static List<String> uncommentCode(String lang, List<String> ls){
    List<String> ret = new ArrayList<>();
    if(len(ls) > 0){
      for(int i = 0; i < len(ls); i++){
        String line = ls.get(i);
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', line);
        if(lang.equals("haskell")){
          Tuple<String, String> tt = splitWhileStr(x -> x == '-', t.y);
          var code = dropWhile(x -> x == ' ', tt.y);
          ret.add(t.x + code);
        } else if(lang.equals("java") || lang.equals("cpp")){
          Tuple<String, String> tt = splitWhileStr(x -> x == '/', t.y);
          var code = dropWhile(x -> x == ' ', tt.y);
          ret.add(t.x + code);
	    } else if(lang.equals("latex")){
          Tuple<String, String> tt = splitWhileStr(x -> x == '%', t.y);
          var code = dropWhile(x -> x == ' ', tt.y);
          ret.add(t.x + code);
        }
        else if(lang.equals("elisp")){
          Tuple<String, String> tt = splitWhileStr(x -> x == ';', t.y);
          var code = dropWhile(x -> x == ' ', tt.y);
          ret.add(t.x + code);
        }else if (lang.equals("bash") || lang.equals("python")){
			Tuple<String, String> tt = splitWhileStr(x -> x == '#', t.y);
			var code = dropWhile(x -> x == ' ', tt.y);
			ret.add(t.x + code);
		}
      }
    }
    return ret;
  }
  public static void test_uncommentCode(){
    beg();
    {
      fl("haskell tests");
      {
        String lang = "haskell";
        List<String> ls = ll(" -- fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("-- fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("--fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("-- fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "haskell";
        List<String> ls = ll("  -- fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    {
      fl("java tests");
      {
        String lang = "java";
        List<String> ls = ll(" // fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("// fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("//fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("// fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "java";
        List<String> ls = ll("  // fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    {
      fl("elisp tests");
      {
        String lang = "elisp";
        List<String> ls = ll(" ;; fold");
        List<String> expls = ll(" fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";; fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";;fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll(";; fold");
        List<String> expls = ll("fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
      {
        String lang = "elisp";
        List<String> ls = ll("  ;; fold");
        List<String> expls = ll("  fold");
        List<String> ss = uncommentCode(lang, ls);
        t(ss, expls);
      }
    }
    end();
  }
  public static void test_commentCode(){
    beg();
    {
      {
        List<String> rfls = ll("--");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("--");
        t(ls, expls);
      }
      {
        
        List<String> rfls = ll("  abc ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("  -- abc ");
        t(ls, expls);
      }
      
      {
        List<String> rfls = ll("abc ", "ab", "line1");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- abc ", "-- ab", "-- line1");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- ");
        t(ls, expls);
      }
      {
        // " " => " -- "
        List<String> rfls = ll(" ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll(" -- ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("-- ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("--");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("--");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("- ");
        List<String> ls = commentCode("haskell", rfls);
        List<String> expls = ll("-- - ");
        t(ls, expls);
      }
    }
    {
      {
        List<String> rfls = ll("/ ");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// / ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("//");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("//");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("// ");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("mycode");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// mycode");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("/mycode");
        List<String> ls = commentCode("java", rfls);
        List<String> expls = ll("// /mycode");
        t(ls, expls);
      }
    }
    {
      {
        List<String> rfls = ll(";;");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(";;");
        t(ls, expls);
      }
      {
        List<String> rfls = ll(";; ");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(";; ");
        t(ls, expls);
      }
      {
        List<String> rfls = ll("mycode");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(";; mycode");
        t(ls, expls);
      }
      {
        List<String> rfls = ll(" mycode");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(" ;; mycode");
        t(ls, expls);
      }
      {
        List<String> rfls = ll(" k;;mycode");
        List<String> ls = commentCode("elisp", rfls);
        List<String> expls = ll(" ;; k;;mycode");
        t(ls, expls);
      }
    }
    end();
  }
}

