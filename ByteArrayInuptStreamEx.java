import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.nio.charset.StandardCharsets;

public class ByteArrayInuptStreamEx {
    public static void main(String[] args) {
        test0();
        // test1();
        test2();
    }
    public static void test2() {
        Aron.beg();

//        {
//            String fname = "/tmp/gg.x";
//            List<String> list = Aron.geneRandomStrList(100);
//            Aron.writeFile(fname, list);
//            List<String> listBuf = Aron.readFileLineByte(fname, 20);
//            Print.p(listBuf);
//        }
        {
            String fname = "/tmp/gg1.x";
//            List<String> list = Aron.geneRandomStrList(100);
//            Aron.writeFile(fname, list);
            List<byte[]> listBuf = readFileByteList(fname, 2);
            Print.pListByte(listBuf);
        }

        Aron.end();
    }

    public static void test0() {
        Aron.beg();
        byte[] buffer = {71, 69, 70, 75, 83};
        ByteArrayInputStream bStream = new ByteArrayInputStream(buffer); 
        int c = 0;
        while( (c = bStream.read()) != -1){
            Print.p((char)c);
        }

        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        String to byte array, string to byte[] with CharacterCharsets.UTF_8
    }
    {@code
    }
    </pre>
    */ 
    public static byte[] strToByteArray(String s){
        return s.getBytes(StandardCharsets.UTF_8);
    }
    

    
    /**
    <pre>
    {@literal
        read file to list of Byte[]
    }
    {@code
    String = "cat";
    }
    </pre>
    */ 
    public static List<byte[]> readFileByteList(String fName, int bufSize){
        List<byte[]> listByte = new ArrayList<>(); 
        try {
            FileInputStream fstream = new FileInputStream(fName);
            int nbyte = 0;
            //Read File Line By Line
            // StringBuilder is not sync, StringBuffer is sync
            StringBuilder sb = new StringBuilder();  
            byte[] buffer = new byte[bufSize];
            while ((nbyte = fstream.read(buffer)) != -1) {
                for(int i=0; i<nbyte; i++){
                    //
                    // in Window newline is "\r\n"
                    // bufffer[i] == '\r' && i+1 < nbyte && bufffer[i+1] == '\n')
                    //
                    if(buffer[i] == '\n'){
                        sb.append((char)buffer[i]);
                        // string to byte[], add to list
                        listByte.add(strToByteArray(sb.toString()));
                        sb = new StringBuilder();
                    }else{
                        sb.append((char)buffer[i]);
                    }
                }
            }
            //Close the input stream
            fstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listByte;
    }
    public static void test1() throws Exception {

//        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
//
//        byte[] buffer = {71, 69, 69, 75, 83};
//        ByteArrayInputStream geek = null;
//        try {
//            geek = new ByteArrayInputStream(buffer);
//
//            // Use of available() method : telling the no. of bytes to be read
//            int number = geek.available();
//            System.out.println("Use of available() method : " + number);
//
//
//            // Use of read() method : reading and printing Characters one by one
//            System.out.println("\nChar : "+(char)geek.read());
//            System.out.println("Char : "+(char)geek.read());
//            System.out.println("Char : "+(char)geek.read());
//
//            // Use of mark() :
//            geek.mark(0);
//
//            // Use of skip() : it results in skiping 'k' from "GEEKS"
//            geek.skip(1);
//            System.out.println("skip() method comes to play");
//            System.out.println("mark() method comes to play");
//            System.out.println("Char : "+(char)geek.read());
//
//            // Use of markSupported
//            boolean check = geek.markSupported();
//            System.out.println("\nmarkSupported() : " + check);
//            if(geek.markSupported()) {
//                // Use of reset() method : repositioning the stram to marked positions.
//                geek.reset();
//                System.out.println("\nreset() invoked");
//                System.out.println("Char : "+(char)geek.read());
//                System.out.println("Char : "+(char)geek.read());
//            }
//            else {
//                System.out.println("reset() method not supported.");
//            }
//
//            System.out.println("geek.markSupported() supported reset() : "+check);
//
//        }
//        catch(IOException excpt) {
//            throw new IOException("what the hell"); 
//        }
//        finally {
//            if(geek!=null) {
//                geek.close();
//            }
//        }

    }
}

