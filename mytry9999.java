import java.util.*;
import java.io.*;
import classfile.*;

interface Face{
    public String fun();
}

class MyFace implements Face{
    public String fun(){
        return "myface";
    }
}

public class mytry9999{
    public static void main(String[] args) {
        List<Face> list = new ArrayList<Face>();
        list.add(new MyFace());
        for(Face f : list){
            Print.p(f.fun());
        }
    }
    static void test0(){
        Aron.beg();
        Aron.end();
    }
}

