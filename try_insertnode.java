import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// Find all children at distance k from a given node
// Find all children that are k distance from a given node
// Print all children at distance k from a given node


// class Node{
//    Node left;
//    Node right;
//    Node parent;
//    int data;
//    public Node(int data){
//	this.data = data;
//    }
//}

class Binary{
    Node root;
    public Binary(Node node){
	this.root = node;
    }
    public Binary(){}
    public void insert(Node node){
	if(this.root == null){
	    this.root = node;
	    this.root.parent = null;
	}else{
	    Node tmp = root;
	    while(tmp != null){
		if(node.data <= tmp.data){
		    if(tmp.left == null){
			tmp.left = node;
			tmp.left.parent = tmp;
			break;
		    }
		    else
			tmp = tmp.left;
		}else{
		    if(tmp.right == null){
			tmp.right = node;
			tmp.right.parent = tmp;
			break;
		    }
		    else
			tmp = tmp.right;
		}
	    }
	}
    }
}

public class try_insertnode{
    public static void main(String[] args) {
        
        
	test3();
    }
    // root will be modified
    //   1. root = null => node
    //   2. [3] , insert node = 2
    //   3. [1],  insert node = 2
    //        3
    //      2          insert node = 1
    public static void insert(Node root, Node node){
	if(root == null){
	    root = node;
	    root.parent = null;
	}else{
	    if(node.data <= root.data){
		if(root.left == null){
		    root.left = node;
		    root.left.parent = root;
		}
		else
		    insert(root.left, node);
	    }else{
		if(root.right == null){
		    root.right = node;
		    root.right.parent = root;
		}
		else
		    insert(root.right, node);
	    }
        }
    }
    public static void inorder(Node root){
	if(root != null){
	    inorder(root.left);
	    p(root.data);
	    inorder(root.right);
	}
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

	Node root = new Node(3);
	insert(root, new Node(2));
	insert(root, new Node(1));
	insert(root, new Node(4));
	
	inorder(root);



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();
	
	Node root = null;                        	
	insert(root, new Node(1));
	inorder(root);

        sw.printTime();
        end();
    }
    public static void kDistance(Node root, int k, List<Node> list){
	if(root != null){
	    if(k > 0){
            kDistance(root.parent, k-1, list);
            kDistance(root.left, k-1, list);
            kDistance(root.right, k-1, list);
	    }else if (k == 0){
            list.add(root);
	    }
	}
    }
    public static Node findNode(Node root, Node node){
	// traveral left, right and up dir to find all the nodes are k distance from the node k.
        Node ret = null;
        if(root != null){
            if(root.data == node.data)
                ret = root; 
            else{
                ret = findNode(root.left, node);
                if(ret == null){
                    ret = findNode(root.right, node);
                }
            }
            
        }    
        return ret;
    }
    public static void test2(){
	beg();
	Binary bin = new Binary();
	Node n1 = new Node(10);
	Node n2 = new Node(8);
	Node n3 = new Node(12);
	Node n4 = new Node(6);
	Node n5 = new Node(16);
	Node n6 = new Node(20);
	Node n7 = new Node(14);
	Node n8 = new Node(5);
	Node n9 = new Node(2);
	Node n10 = new Node(1);
	
	bin.insert(n1);
	bin.insert(n2);
	bin.insert(n3);
	bin.insert(n4);
	bin.insert(n5);
	bin.insert(n6);
	bin.insert(n7);
	bin.insert(n8);
	bin.insert(n9);
	bin.insert(n10);
	
        binImage(bin.root);
	
	
	inorder(bin.root);
	end();
    }
     public static void test3(){          
         beg();                           
         Binary bin = new Binary();       
         Node n1 = new Node(10);          
         Node n2 = new Node(8);           
         Node n3 = new Node(12);          
         Node n4 = new Node(6);           
         Node n5 = new Node(16);          
         Node n6 = new Node(20);          
         Node n7 = new Node(14);          
         Node n8 = new Node(5);           
         Node n9 = new Node(2);           
         Node n10 = new Node(1);          
                                          
         bin.insert(n1);                  
         bin.insert(n2);                  
         bin.insert(n3);                  
         bin.insert(n4);                  
         bin.insert(n5);                  
         bin.insert(n6);                  
         bin.insert(n7);                  
         bin.insert(n8);                  
         bin.insert(n9);                  
         bin.insert(n10);                 
                                          
         binImage(bin.root);              
                                          
         fl("inorder");                                
         inorder(bin.root);

         List<Node> list = new ArrayList<>();

         fl("print list");
         Node fn = new Node(300);
         p("bin.root.data=" + bin.root.data);
         Node node = findNode(bin.root, fn);
         if(node != null){
             p("node.data=" + node.data);
             kDistance(node, 1, list);
             for(Node n : list){
                 p(n.data);
             }
         }
         end();                           
     }                                    
} 

