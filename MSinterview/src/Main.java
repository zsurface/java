import classfile.Node;

import javax.swing.text.html.parser.Entity;
import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

import static classfile.Aron.*;
import static classfile.Aron.*;
import static classfile.Print.*;

public class Main {
    public static void main(String[] args) {
        pp("Hello World");
//        test0();
//        test1();
//        test2();
//        test3();
    }
    static void test11() {
        beg();
        end();
    }
    static void test0(){
        /**
         *         6
         *     4         8
         *   1    5     7    9
         *
         *
         */

        beg();
        {
            List<Node> preorder = new ArrayList<>();
            preorder.add(new Node(1));

            List<Node> inorder = new ArrayList<>();
            inorder.add(new Node(1));

            Node node = buildMyTree(preorder, inorder);
            pb(node);
            line();
        }
        {
            List<Node> preorder = new ArrayList<>();
            preorder.add(new Node(6));
            preorder.add(new Node(4));
            preorder.add(new Node(8));

            List<Node> inorder = new ArrayList<>();
            inorder.add(new Node(4));
            inorder.add(new Node(6));
            inorder.add(new Node(8));

            Node node = buildMyTree(preorder, inorder);
            inorder(node);
            line();
        }

        end();
    }

    public static List<Integer> myfilter(Function<Integer, Boolean> f, List<Integer> list){
        List<Integer> retList = of();
        for(Integer n : list){
            if(f.apply(n)){
                retList.add(n);
            }
        }
        return retList;
    }

    /**
     * n = 1 .. list.size()
     * @param n
     * @param list
     * @return
     */
    public static List<Integer> takeN(Integer n, List<Integer> list){
        List<Integer> retList = of();
        Integer c = 0;
        for(Integer e : list){
            if(c < n){
                retList.add(e);
                c++;
            }else{
                break;
            }
        }
        return retList;
    }
    public static List<Integer> dropN(Integer n, List<Integer> list){
        List<Integer> retList = of();
        Integer c = 0;
        for(Integer e : list){
            if(c >= n){
                retList.add(e);
                c++;
            }
        }
        return retList;
    }

    public static Integer headN(List<Integer> list){
        if(list.size() > 0)
            return list.get(0);
        else
            throw new IllegalArgumentException("List can not be empty.");
    }

    static void test1(){
        beg();

        Node root = geneBinNoImage();

        String fname = "/tmp/x.x";
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
            writeBinToFile(root, bw);
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }

        end();
    }

    static void test2(){
        beg();
        
        String fname = "/tmp/x.x";
        try {
            BufferedReader br = new BufferedReader(new FileReader(fname));
            List<String> list = readFileToList(br);
            pp(list);
            line();
            Node root = readFileToBin(list.iterator());
            inorder(root);
        }catch (IOException e){
            e.printStackTrace();
        }
        

        end();
    }

    public static void test3(){
        beg();
        
        {
            Integer[][] arr = {
                { 0,   0,   1,  1},
                { 1,   0,   0,  1},
                { 1,   1,   0,  1},
                { 1,   1,   1,  0},
            };

            Integer height = arr.length;
            Integer width = arr[0].length; 
            int h = 0;
            int c = 0;
            printArray2d(arr);
            line();
            fun(arr, h, c); 
        }
        {
            line();
            for(Integer n : range(1, 3)){
                pb(n);
            }
            line();
            for(Integer n : IntStream.range(1, 4).toArray()){
                pb(n);
            }
        }
        end();
    }
    public static Map<String, Set<String>> prefixStringFromList(List<String> ls){
        Set<String> set = new HashSet<String>(ls);
        Map<String, Set<String>> prefixMap = new HashMap<String, Set<String>>();
        prefixStringMap(ls, prefixMap);
        return prefixMap;
    }

    /**
     * prefix string map to list of string
     *
     * List<String> list might contain repeated elements
     *
     * Use Map<String, Set<String>> mapSet instead
     * <code>
     *     Map<String, List<String>> map
     * </code>
     *
     *
     * set to list
     * Set<String> set = new HashSet<String>()
     * List<String> list = new ArrayList<String>(set);
     * @param ls
     * @return
     */
    public static Map<String, List<String>> prefixStringFromListToList(List<String> ls){
        Map<String, Set<String>> map = new HashMap<>();

        prefixStringMap(ls, map);
        pp(map);

        Map<String, List<String>> mapList = new HashMap<>();
        for(Map.Entry<String, Set<String>> entry : map.entrySet()){
            pb(entry.getKey());
            mapList.put(entry.getKey(), new ArrayList<>(entry.getValue()));
        }
        return mapList;
    }
    public static Node buildMyTree(List<Node> preorder, List<Node> inorder){
        if(preorder.size() == 0){
            return null;
        }else{
            Node node = head(preorder);
            List<Node> inLeft = filter((x) -> x.data < node.data, inorder);
            List<Node> inRight = filter((x) -> x.data > node.data, inorder);
            List<Node> preLeft = take(inLeft.size(),  tail(preorder));
            List<Node> preRight = drop(inLeft.size(), tail(preorder));
            node.left =  buildMyTree(preLeft, inLeft);
            node.right = buildMyTree(preRight, inRight);
            return node;
        }
    }

    public static List<String> readFileToList(BufferedReader br){
        List<String> list = of();
        try{
            String line = null;
            while((line = br.readLine()) != null){
                String[] arr = line.split("\\s+");
                for(String s : arr){
                    if(s.trim().length() > 0){
                        list.add(s);
                    }
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * I might need Iterator to replace list
     * Need Iterator instead of List here
     * see pic why
     *  /Library/WebServer/Documents/xfido/image/serialize_binary_tree_with_marker.png
     */
    public static Node readFileToBin(Iterator<String> ite){
        if(ite.hasNext()){
            String n = ite.next();
            if(n.equals("#") == false){
                Node root = new Node(Integer.parseInt(n));
                root.left = readFileToBin(ite);
                root.right = readFileToBin(ite);
                return root;
            }
        }
        return null;
    }
    public static void writeBinToFile(Node root, BufferedWriter bw){
        try{
            if(root != null){
                String s = "" + root.data;
                bw.write(s + " ");
                writeBinToFile(root.left, bw);
                writeBinToFile(root.right, bw);
            }else{
                bw.write("# ");
            }

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     *   -1, 0, 1
     *   -1, 0, 1
     *   9 pairs
     *   (-1, -1), (-1, 1) (1, -1) (1, 1)
     *
     *
     * @param arr
     * @param col
     * @param row
     */
    public static void fun(Integer[][] arr, int h, int c){
        int height = arr.length;
        int width = arr[0].length;

        if(arr[h][c] == 0){
            int tmp = arr[h][c];
            pl(arr[h][c] + " -> " + "(" + h + "," + c + ")\n");
            arr[h][c] = 1;
            for(Integer hh : Arrays.asList(-1, 0, 1)){
                for(Integer cc : Arrays.asList(-1, 0, 1)){
                    // down
                    int hx = h + hh;
                    int cx = c + cc;

                    // diagonal
                    // { (-1, 0), (1, 0), (0, 1), (0, -1) }
                    if(Math.abs(hh) != Math.abs(cc)) {
                        if (0 <= hx && hx < height && 0 <= cx && cx < width) {
                            fun(arr, hx, cx);
                        }
                    }else{
                    }
                    
                }
            }
            arr[h][c] = tmp;
        }
    }
}
