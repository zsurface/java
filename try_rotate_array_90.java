import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_rotate_array_90{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        int[][] arr = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
            };
        Print.p(arr);
        rotateArr90(arr);
        Print.p(arr);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }

    /**
    * 1. swap x, y. e.g. (x, y) -> (y, x)
    * 2. swap elements: [k][i], [k][len-1-i]. 
    *    e.g tmp = arr[k][i], arr[k][i] = arr[k][len - 1 - i], arr[k][len-1-i] = tmp
    */
    public static void rotateArr90(int[][] arr){
        if(arr != null){
            // assume the lengths of row and column are the same
            int len = arr.length;

            // (x, y) -> (y, x)
            for(int i=0; i<len; i++){
                for(int j=i; j<len; j++){
                    int tmp = arr[i][j];
                    arr[i][j] = arr[j][i];
                    arr[j][i] = tmp;
                }
            }

            // flip all rows
            for(int i=0; i<len; i++){
                for(int j=0; j<len/2; j++){
                    int tmp = arr[i][j];
                    arr[i][j] = arr[i][len - 1 - j];
                    arr[i][len - 1 - j] = tmp;
                }
            }
        }
    }
} 

