import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;


public class partitionGenericEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        {
            Integer[] arr = {2, 1, 3};
            Integer[] exp = {1, 2, 3};
            int lo = 0, hi = arr.length - 1;
            Aron.quickSortT(arr, lo, hi);
            Test.t(arr, exp);
        }
        {
            Integer[] arr = {1};
            Integer[] exp = {1};
            int lo = 0, hi = arr.length - 1;
            Aron.quickSortT(arr, lo, hi);
            Test.t(arr, exp);
        }
        {
            Integer[] arr = {1, 2};
            Integer[] exp = {1, 2};
            int lo = 0, hi = arr.length - 1;
            Aron.quickSortT(arr, lo, hi);
            Test.t(arr, exp);
        }
        {
            Integer[] arr = {2, 1};
            Integer[] exp = {1, 2};
            int lo = 0, hi = arr.length - 1;
            Aron.quickSortT(arr, lo, hi);
            Test.t(arr, exp);
        }
        {
            Integer[] arr = {2, 1, 3, 5, 7, 9};
            Integer[] exp = {1, 2, 3, 5, 7, 9};
            int lo = 0, hi = arr.length - 1;
            Aron.quickSortT(arr, lo, hi);
            Test.t(arr, exp);
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }

} 

