import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static java.lang.Character.*; 
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;


/**
    tokenize string 
    there are two functions so far
    tokenize()
    tokenize2()
*/
public class TokenizeStr{
    public static void main(String[] args) {
        test00_tokenize();
        test0_tokenize();
        test1_tokenize();
        test11_tokenize();
        test2_tokenize();
        test22_tokenize();
        test3_tokenize();
        test4_tokenize();
        test0_tokenize2();
        test1_tokenize2();
        test2_tokenize2();
        test3_tokenize2();
        test4_tokenize2();
    }
    static void test00_tokenize(){
        beg();
        String str = "<a>b";
        List<String> list = tokenize(str);
        List<String> exp = Arrays.asList("<a>", "b");
        p(list);

        t(list, exp);

        end();
    }
    static void test0_tokenize(){
        beg();
        String str = "<a>";
        List<String> list = tokenize(str);
        List<String> exp = Arrays.asList("<a>");
        p(list);

        t(list, exp);

        end();
    }
    static void test11_tokenize(){
        beg();
        String str = "<b>c</b>";
        List<String> list = tokenize(str);
        List<String> exp = Arrays.asList("<b>", "c", "</b>");
        p(list);

        t(list, exp);

        end();
    }
    static void test1_tokenize(){
        beg();
        String str = "<a>b</a>";
        List<String> list = tokenize(str);
        p(list);

        end();
    }
    static void test2_tokenize(){
        beg();
        String str = "<na>da</na>";
        List<String> list = tokenize(str);
        p(list);

        end();
    }
    static void test22_tokenize(){
        beg();
        String str = "<name>a</name>";
        List<String> list = tokenize(str);
        p(list);

        end();
    }
    static void test3_tokenize(){
        beg();
        String str = "<name>david</name><addr>abc</addr>";
        List<String> list = tokenize(str);
        p(list);

        end();
    }
    static void test4_tokenize(){
        beg();
        String str = "<name>david</name>" 
                    +"<addr>abc"
                    +"<phone>cc</phone>"
                    +"<dog>dog</dog>"
                    +"</addr>";
        List<String> list = tokenize(str);
        p(list);

        end();
    }
    public static char lookAhead(String str, int index){
        int i = 1;
        while(str.charAt(index + i) == ' ')
            i++;

        return str.charAt(index + i);
    }

    //[ file=tokenizestr.html title=""
    /**
     * tokenize xml file like format.
     * read character one by one and feed it to state machine.
     * if token is found, add to a list. Otherwise terminate
     * the process and return null.
     * for example, <name>david</name> => [<name>, david, </name>]
     * <a> is not valid
     * <a  is not valid 
     * <a>b<a is not valid
     * @param strChar xxx
     *
     * @return list of tokens if all tokens are valid, otherwise return null
     */
    public static List<String> tokenize(String strChar){
        List<String> list = new ArrayList<String>();
        int state = 0;
        int curr = state;
        int len = strChar.length();
        String str = "";
        boolean isValid = true;
        for(int i=0; i<len && isValid; i++){
            if(curr == 0){
                if(len > 0){
                    list.add(str);
                    str = "";
                }

                if(strChar.charAt(i) == '<'){
                    str += strChar.charAt(i);
                    curr = 1;
                }else if(isLetter(strChar.charAt(i))){
                    str += strChar.charAt(i);
                    curr = 2;
                    if(i + 1 < len && lookAhead(strChar, i) == '<'){
                        curr = 0;
                    }
                }else{
                    isValid = false;
                }
            }else if(curr == 2){
                if(isLetter(strChar.charAt(i))){
                    str += strChar.charAt(i);
                    curr = 2;
                    if(i + 1 < len && lookAhead(strChar, i) == '<'){
                        curr = 0;
                    }
                }else{
                    isValid = false;
                }
            }else if(curr == 1){
                if(isLetter(strChar.charAt(i))){
                    str += strChar.charAt(i);
                    curr = 3;
                }
                else if(strChar.charAt(i) == '/'){
                    str += strChar.charAt(i);
                    curr = 4;
                }else{
                    isValid = false;
                }
            }else if(curr == 3){
                if(strChar.charAt(i) == '>'){
                    str += strChar.charAt(i);
                    curr = 5; // final
                    curr = 0;
                }else if(isLetter(strChar.charAt(i))){
                    str += strChar.charAt(i);
                    curr = 3;
                }else{
                    isValid = false;
                }
            }else if(curr == 4){
                if(isLetter(strChar.charAt(i))){
                    str += strChar.charAt(i);
                    curr = 7;
                }else{
                    isValid = false;
                }

            }else if(curr == 7){
                if(isLetter(strChar.charAt(i))){
                    str += strChar.charAt(i);
                }else if(strChar.charAt(i) == '>'){
                    str += strChar.charAt(i);
                    curr = 8; // final
                    list.add(str);
                    str = "";
                    curr = 0;
                }else{
                    isValid = false;
                }
            }
        }

        if(curr == 2){
            if(str.length() > 0){
                list.add(str);
                str = "";
            }
        }

        return isValid ? list : null;
    }
    //]

    // gx /Users/cat/myfile/github/math/StateMachine2.png
    public static List<String> tokenize2(String strChar){
        List<String> list = new ArrayList<String>();
        int state = 0;
        int curr = state;
        String str = "";
        boolean isValid = true;
        int len = strChar.length();
        for(int i=0; i<len && isValid; i++){
            char currChar = strChar.charAt(i);
            if(currChar != ' '){
                if(curr == 0){
                    if(currChar == '['){
                        list.add(currChar + "");
                        curr = 1;
                    }
                }else if(curr == 1){
                    if(currChar == '['){
                        list.add(currChar + "");
                    }else if(currChar == ']'){
                        curr = 3;
                        list.add(currChar + "");
                    }else if(isLetterOrDigit(currChar)){
                        curr = 2;
                        str += currChar + "";
                        if(i < len - 1){
                            if(lookAhead(strChar, i) == ']' || lookAhead(strChar, i) == '['){
                                list.add(str);
                                str = "";
                            }
                        }
                    }
                }else if(curr == 2){
                    if(isLetterOrDigit(currChar)){
                        str += currChar + "";
                        if(i < len - 1){
                            if(lookAhead(strChar, i) == ']' || lookAhead(strChar, i) == '['){
                                list.add(str);
                                str = "";
                            }
                        }
                    }else if(currChar == ']'){
                       curr = 3;
                       list.add(currChar + ""); 
                    }else if(currChar == '['){
                        curr = 1;
                        list.add(currChar + "");
                    }
                }else if(curr == 3){
                    if(currChar == ']')
                        list.add(currChar + "");
                    else if(currChar == '['){
                        curr = 1;
                        list.add(currChar + "");
                    }
                }
            }
        }
        return list;
    }
    static void test0_tokenize2(){
        beg();
        String str = "[ 1 ]";
        List<String> list = tokenize2(str);
        p(list, "(" );

        end();
    }
    static void test1_tokenize2(){
        beg();
        String str = "[ 1 "
                    +" [ 2 ]"
                    +" [ 3 ]"
                    +" ]";
        List<String> list = tokenize2(str);
        p(list, " " );
        end();
    }
    static void test2_tokenize2(){
        beg();
        String str = "[ 1 "
                    +" [ 2 ]"
                    +" [ 3 "
                    +"  [ 4 ]"
                    +"  [ 5 ]"
                    +" ]"
                    +"]";
        List<String> list = tokenize2(str);
        p(list, "<" );
    }
    static void test3_tokenize2(){
        beg();
        String str = "[ abc "
                    +" [ 123 ]"
                    +" [ 3 "
                    +"  [ 4 ]"
                    +"  [ 5 ]"
                    +" ]"
                    +" [ 6 ]"
                    +" ]";
        List<String> list = tokenize2(str);
        p(list, " " );

        end();
    }
    static void test4_tokenize2(){
        beg();
        String str = "[abc"
                    +"[123]"
                    +"[3"
                    +"[4]"
                    +"[5]"
                    +"]"
                    +"[6]"
                    +"]";
        List<String> list = tokenize2(str);
        p(list, " " );

        end();
    }
}

