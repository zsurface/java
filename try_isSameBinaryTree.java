import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Pair{
    boolean isSame;
    int n;
    public Pair(boolean isSame, int n){
        this.isSame = isSame;
        this.n = n;
    }
    public String toString(){
        return "(" + isSame + " , " + n + ")";
    }
}

public class try_isSameBinaryTree{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        {
            Node r1 = geneBinNoImage();
            Node r2 = geneBinNoImage();
        }

        end();
    }
    public static void test1(){
        beg();

        end();
    }

    /**
     * count(null) => 0
     */
    public static int count(Node r){
        return r == null ? 0 : 1 + count(r.left) + count(r.right);
    }

    /**
     *  KEY: check two binary tree are the same tree or not
     *  1. if r1 and r2 are both null, return true
     *  2. if r1 and r2 are both not null, compare the left and right subtrees of each binary tree
     *
     *  1. r1, r2 = null => true
     * 
     *  r1   9           r2  9
     *     2              3     => false
     *
     *  r1   9           r2  9
     *     2              2     => true 
     *
     */
    public static boolean isSameBinTree(Node r1, Node r2, int x){
        return false; 
    }
    
} 

