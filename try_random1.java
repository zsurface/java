import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_random1{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();

        List<Integer> list = geneRandomIntFromTo(0, 2, 4); 
        p(list);

        end();
    }
    public static void test1(){
        int count = 100000000;
        beg();
        {
            StopWatch sw = new StopWatch();
            sw.start();

            Random ran = new Random();
            int max = 100;
            List<Integer> list = IntStream.range(0, count).mapToObj(i -> ran.nextInt(max))
                                            .collect(Collectors.toList()); 

            sw.printTime();
        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            Random ran = new Random();
            int max = 100;
            List<Integer> list = new ArrayList<Integer>(); 
            for(long i=0; i<count; i++){
                list.add(ran.nextInt(max));
            } 

            sw.printTime();
        }

        end();
    }
} 

