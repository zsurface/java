import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

import java.io.*;
import classfile.*;
import java.util.stream.*;
import classfile.*;

public class XQueueTest{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
    }
    public static void test0(){
        Aron.beg();
        XQueue<String> q = new XQueue<String>();
        q.enqueue("cat");
        q.enqueue("dog");
        q.enqueue("cow");
        
        // immutable list
        List<String> expected = Arrays.asList("cat", "dog", "cow");

        List<String> actual  = q.queueToList();
        

        Test.t(actual, expected);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        XQueue<String> q = new XQueue<String>();
        Test.t(q.isEmpty(), true);
        Test.t(q.size(), 0);
        Aron.end();
    }

    public static void test2(){
        Aron.beg();
        XQueue<String> q = new XQueue<String>();
        q.enqueue("dog");
        Test.t(q.size(), 1);
        Aron.end();
    }
    public static void test3(){
        Aron.beg();
        XQueue<String> q = new XQueue<String>();
        q.enqueue("dog");
        q.dequeue();
        Test.t(q.size(), 0);
        Aron.end();
    }

    public static void test4(){
        Aron.beg();
        XQueue<String> q = new XQueue<String>();
        q.enqueue("dog");
        q.enqueue("cat");
        q.enqueue("cow");
        q.dequeue();

        // immutable list
        List<String> expected = Arrays.asList("cat", "cow");
        List<String> actual  = q.queueToList();
        Test.t(actual, expected);

        Aron.end();
    }
} 

