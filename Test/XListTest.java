import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

import java.io.*;
import classfile.*;
import java.util.stream.*;
import classfile.*;

public class XListTest{
    public static void main(String[] args) {
        test1();
        test2();
    }
    public static void test0(){
        Aron.beg();
        
        long[] arr = {1, 2, 3, 4, 5};
        int width = arr.length; 

        long[][] expected = {
            {1, 2},
            {3, 4},
            {5, 0},
        };

        long[][] actual = XList.partition(arr, 2);
        Print.p(actual);
        Test.t(actual, expected);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        int[] arr1 = {1, 2, 3, 4};
        int[] arr2 = {1, 2, 3, 4};
        Test.t(arr1, arr2);

        Aron.end();
    }
    public static void test2(){
        Aron.beg();
        int[][] arr1 = {
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1},
        };
        int[][] arr2 = {
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1},
        };
        Test.t(arr1, arr2);
        Aron.end();
    }
    
} 

