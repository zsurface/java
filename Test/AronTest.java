import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

import java.io.*;
import classfile.*;
import java.util.stream.*;
import classfile.*;

public class AronTest{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
        test8();
        test9();
        test10();
        test11();
        test12();
        test13();
        test14();
        test15();
        test16();
        test160();
        test17();
        test18();
        test19();
        test20();
        test21();
        test22();
        test23();
        geneArr1ToNint_test();
        concatArr_test();
        concatMatrix_test();
        writeFile_test1();
        writeFile_test2();
        writeFile_test3();
        writeFile_test4();
        equalDouble_test();
    }
    public static void test0(){
        Aron.beg();
        Test.t(Aron.gcd(0, 2), 2);
        Test.t(Aron.gcd(2, 0), 2);
        Test.t(Aron.gcd(1, 1), 1);
        Test.t(Aron.gcd(4, 8), 4);
        Test.t(Aron.gcd(7, 11), 1);
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Test.t(Aron.lcm(0, 2), 0);
        Test.t(Aron.lcm(2, 0), 0);
        Test.t(Aron.lcm(1, 1), 1);
        Test.t(Aron.lcm(2, 3), 6);
        Test.t(Aron.lcm(2, 8), 8);
        Test.t(Aron.lcm(4, 8), 8);
        Test.t(Aron.lcm(4, 12), 12);
        Test.t(Aron.lcm(16, 12), 48);
        Aron.end();
    }
    public static void test2(){
        Aron.beg();
        long[] arr = {1, 2, 3, 4};
        long gcd = Aron.gcdList(arr);
        Test.t(gcd, 1);
        Aron.end();
    }
    public static void test3(){
        Aron.beg();
        long[] arr = {1, 0, 3, 4};
        long gcd = Aron.gcdList(arr);
        Test.t(gcd, 1);
        Aron.end();
    }
    public static void test4(){
        Aron.beg();
        long[] arr = {2, 4, 6, 20};
        long gcd = Aron.gcdList(arr);
        Test.t(gcd, 2);
        Aron.end();
    }
    public static void test5(){
        Aron.beg();
        long[] arr = {25, 11, 6, 20};
        long gcd = Aron.gcdList(arr);
        Test.t(gcd, 1);
        Aron.end();
    }
    public static void test6(){
        Aron.beg();
        long[] arr = {24, 12, 8, 20};
        long gcd = Aron.gcdList(arr);
        Test.t(gcd, 4);
        Aron.end();
    }
    public static void test7(){
        Aron.beg();
        long[] arr = {1, 2};
        long gcd = Aron.gcdList(arr);
        Test.t(gcd, 1);
        Aron.end();
    }
    public static void test8(){
        Aron.beg();
        long[] arr = {10, 20, 30};
        long gcd = Aron.gcdList(arr);
        Test.t(gcd, 10);
        Aron.end();
    }
    public static void test9(){
        Aron.beg();
        long[] arr = {0, 1};
        long lcm = Aron.lcmList(arr);
        Test.t(lcm, 0);
        Aron.end();
    }
    public static void test10(){
        Aron.beg();
        long[] arr = {0, 1, 2};
        long lcm = Aron.lcmList(arr);
        Test.t(lcm, 0);
        Aron.end();
    }
    public static void test11(){
        Aron.beg();
        long[] arr = {2, 4, 6};
        long lcm = Aron.lcmList(arr);
        Test.t(lcm, 12);
        Aron.end();
    }
    public static void test12(){
        Aron.beg();
        long[] arr = {2, 4, 6, 100};
        long lcm = Aron.lcmList(arr);
        Test.t(lcm, 300);
        Aron.end();
    }
    public static void test13(){
        Aron.beg();
        double gcd = Aron.gcd(0.0, 4.0);
        Test.t(gcd, 4.0);

        gcd = Aron.gcd(2.0, 4.0);
        Test.t(gcd, 2.0);

        gcd = Aron.gcd(2.0, 1.0);
        Test.t(gcd, 1.0);

        gcd = Aron.gcd(2.0E10, 1.0E10);
        Test.t(gcd, 1.0E10);

        gcd = Aron.gcd(2.2E10, 2.2E10);
        Test.t(gcd, 2.2E10);

        gcd = Aron.gcd(0.0E10, 1.2E10);
        Test.t(gcd, 1.2E10);
        
        gcd = Aron.gcd(0.0, 0.0);
        Test.t(gcd, 0.0);

        Aron.end();
    }
    public static void test14(){
        Aron.beg();
        double[] arr = {0.0, 1.0};
        double gcd = Aron.gcdList(arr);
        Test.t(gcd, 1.0);
        Aron.end();
    }
    public static void test15(){
        Aron.beg();
        double[] arr = {2.0, 10.0};
        double gcd = Aron.gcdList(arr);
        Test.t(gcd, 2.0);
        Aron.end();
    }
    public static void test16(){
        Aron.beg();
        double[] arr = {20.0, 10.0, 4.0};
        double gcd = Aron.gcdList(arr);
        Test.t(gcd, 2.0);
    }
    public static void test160(){
        Aron.beg();
        double[] arr = {0.0, 0.0, 4.0};
        double gcd = Aron.gcdList(arr);
        Test.t(gcd, 4.0);
    }
    public static void test17(){
        Aron.beg();
        double[] arr = {20.0, 10.0, 5.0, 300.0};
        double gcd = Aron.gcdList(arr);
        Test.t(gcd, 5.0);
        Aron.end();
    }
    public static void test18(){
        Aron.beg();
        double[] arr = {20.0, 10.0, 5.0, 300.0};
        double lcm = Aron.lcmList(arr);
        Test.t(lcm, 300.0);
    }
    public static void test19(){
        Aron.beg();
        double[] arr = {20.0, 0.0};
        double lcm = Aron.lcmList(arr);
        Test.t(lcm, 0.0);
        Aron.end();
    }
   
    public static void test20(){
        Aron.beg();
        double[] arr = {15.0, 40.0};
        double lcm = Aron.lcmList(arr);
        Test.t(lcm, 120.0);
        Aron.end();
    }
    public static void test21(){
        Aron.beg();
        long m=0;
        long n=1;
        long gcd = Aron.gcd(m, n);
        Test.t(gcd, 1);

        m=2;
        n=0;
        gcd = Aron.gcd(m, n);
        Test.t(gcd, 2);

        m=2;
        n=3;
        gcd = Aron.gcd(m, n);
        Test.t(gcd, 1);

        m=12;
        n=15;
        gcd = Aron.gcd(m, n);
        Test.t(gcd, 3);

        m=12;
        n=16;
        gcd = Aron.gcd(m, n);
        Test.t(gcd, 4);

        m=100;
        n=110;
        gcd = Aron.gcd(m, n);
        Test.t(gcd, 10);
        Aron.end();
    }

    public static void test22(){
        Aron.beg();
        int[] arr1 = {1, 2, 3, 4};
        int[] arr2 = {1, 2, 3, 4};
        int[] exp = {1, 2, 3, 4, 1, 2, 3, 4};
        int[] act = Aron.concatArr(arr1, arr2);
        Test.t(act, exp);
        Test.t(act.length, arr1.length + arr2.length);
        Print.p(act);
        Aron.end();
    }
    public static void test23(){
        Aron.beg();
        int[] arr1 = {};
        int[] arr2 = {1, 2, 3, 4};
        int[] exp = {1, 2, 3, 4};
        int[] act = Aron.concatArr(arr1, arr2);
        Test.t(act, exp);
        Test.t(act.length, arr1.length + arr2.length);
        Print.p(act);

        Aron.end();
    }
    public static void geneArr1ToNint_test(){
        Aron.beg();
        int[] act = Aron.geneArr1ToNint(0, 0);
        int[] exp = {0};
        Test.t(act, exp);

        int[] act1 = Aron.geneArr1ToNint(1, 3);
        int[] exp1 = {1, 2, 3};
        Test.t(act1, exp1);

        Aron.end();
    }
    public static void concatArr_test(){
        Aron.beg();
        int[] arr1 = {};
        int[] arr2 = Aron.geneArr1ToNint(1, 4);
        int[] exp0 = Aron.geneArr1ToNint(1, 4);
        int[] act0 = Aron.concatArr(arr1, arr2);
        Test.t(act0, exp0);
        Test.t(act0.length, arr1.length + arr2.length);
        Print.p(act0);

        Aron.end();
    }
//    public static void concatMatrix_test(){
//        Aron.beg();
//        int nrow1 = 2;
//        int ncol1 = 3;
//        int[][] m1 = Aron.geneMatrix1toN(nrow1, ncol1);
//
//        int nrow2 = 2;
//        int ncol2 = 4;
//        int[][] m2 = Aron.geneMatrix1toN(nrow2, ncol2);
//        int[][] m = Aron.concatMatrix(m1, m2);
//        
//        int[][] exp = {
//            { 1,   2,   3, 1, 2,   3,   4},
//            { 4,   5,   7, 5, 6,   7,   8},
//        };
//        int[][] act = Aron.concatMatrix(m, exp);
//        Test.t(act, exp); 
//
//
//        Aron.end();
//    }
    // check two matrices whether they are the same
    // check element(entities) by element 
    public static void concatMatrix_test(){
        Aron.beg();
        int nrow1 = 2;
        int ncol1 = 3;
        int[][] m1 = Aron.geneMatrix1toN(2, 3);
        int[][] m2 = Aron.geneMatrix1toN(2, 4);
        int[][] act = Aron.concatMatrix(m1, m2);
        int[][] exp = {
            { 1,   2,   3, 1, 2,   3,   4},
            { 4,   5,   6, 5, 6,   7,   8},
        };
        Test.t(act, exp);
        Print.p(act);
        Print.p(exp);

        int[][] m11 = Aron.geneMatrix1toN(2, 2);
        int[][] m21 = Aron.geneIdentity(2);
        int[][] act1 = Aron.concatMatrix(m11, m21);
        int[][] exp1 = {
            { 1,   2,   1, 0},
            { 3,   4,   0, 1},
        };
        Test.t(act1, exp1);
        Print.p(act1);
        Print.p(exp1);

        Aron.end();
    }
    
    static void writeFile_test1(){
        Aron.beg();
        String file="/tmp/x1.x"; 
        long[] arr = {1, 2, 3, 4};
        Aron.writeFile(file, arr);
        Print.p("=>" + file);
        Aron.end();
    }
    static void writeFile_test2(){
        Aron.beg();
        String file="/tmp/x2.x"; 
        long[][] arr = {
            {1, 2, 3, 4},
            {1, 2, 3, 4},
            {1, 2, 3, 4},
            {1, 2, 3, 4},
        };
        Aron.writeFile(file, arr);
        Print.p("=>" + file);
        Aron.end();
    }
    static void writeFile_test3(){
        Aron.beg();
        String file="/tmp/x3.x"; 
        double[][] arr = {
            {1, 2.00440, 3, 4},
            {1, 2, 3, 4},
            {1, 2, 3, 4},
            {1, 2, 3, 4},
        };
        Aron.writeFile(file, arr);
        Print.p("=>" + file);
        Aron.end();
    }
    static void writeFile_test4(){
        Aron.beg();
        String file="/tmp/x4.x"; 
        double[][] arr = {
            {1, 2.00440, 3, 4},
            {1, 2, 3, 4},
            {1, 2, 3, 4},
            {1, 2, 3, 4},
        };
        Aron.writeFileHaskellList(file, arr);
        Print.p("=>" + file);
        Aron.end();
    }

    static void equalDouble_test(){
        Aron.beg();
        double d1 = 0.1;
        double d2 = 0.1;
        double tolerance = 1E-7; // 0.0000001
        Print.p("d1=" + d1);
        Print.p("d2=" + d2);
        Print.p("tolerance=" + tolerance);
        Test.t(Aron.equalDouble(d1, d2, tolerance), true);

        Print.p();

        d1 = 0.1;
        d2 = 0.0001;
        Print.p("d1=" + d1);
        Print.p("d2=" + d2);
        Print.p("tolerance=" + tolerance);
        Test.t(Aron.equalDouble(d1, d2, tolerance), false);

        Print.p();
        d1 = 0.1;
        d2 = 0.000001; // d1 - d2 = 0.000009
        Print.p("d1=" + d1);
        Print.p("d2=" + d2);
        Print.p("tolerance=" + tolerance);
        Test.t(Aron.equalDouble(d1, d2, tolerance), false);

        Print.p();
        d1 = 0.1;
        d2 = 0.0000001; // d1 - d2 = 0.0999999
        Print.p("d1=" + d1);
        Print.p("d2=" + d2);
        Print.p("tolerance=" + tolerance);
        Test.t(Aron.equalDouble(d1, d2, tolerance), false);

        Print.p();
        d1 = 0.1;
        d2 = 0.09999999; // d1 - d2 = 0.0000001
        Print.p("d1=" + d1);
        Print.p("d2=" + d2);
        Print.p("diff=" + (d1 - d2));
        Print.p("tolerance=" + tolerance);
        Test.t(Aron.equalDouble(d1, d2, tolerance), true);
        Aron.end();
    }
} 

