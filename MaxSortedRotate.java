import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// Tue 26 May 03:04:36 2020 
// KEY: maximum element from a sorted rotated array
// It support repeating elements
public class MaxSortedRotate{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        {
            Integer[] arr = {1, 2, 3, 4};
            int lo = 0;
            int hi = arr.length - 1; 
            int max = maxRotateSortedArray(arr, lo, hi);
            t(max, 4);
        }
        {
            Integer[] arr = {2, 3, 4, 4, 4, 1};
            int lo = 0;
            int hi = arr.length - 1; 
            int max = maxRotateSortedArray(arr, lo, hi);
            t(max, 4);
        }
        {
            Integer[] arr = {4, 1, 1, 2, 2, 3};
            int lo = 0;
            int hi = arr.length - 1; 
            int max = maxRotateSortedArray(arr, lo, hi);
            t(max, 4);
        }
        {
            Integer[] arr = {2, 2, 2, 3, 3, 4, 1, 1, 1};
            int lo = 0;
            int hi = arr.length - 1; 
            int max = maxRotateSortedArray(arr, lo, hi);
            t(max, 4);
        }
        {
            Integer[] arr = {4, 1};
            int lo = 0;
            int hi = arr.length - 1; 
            int max = maxRotateSortedArray(arr, lo, hi);
            t(max, 4);
        }
        {
            Integer[] arr = {4};
            int lo = 0;
            int hi = arr.length - 1; 
            int max = maxRotateSortedArray(arr, lo, hi);
            t(max, 4);
        }

        end();
    }
    // Find a maximum element from a sorted rotated array
    public static Integer maxRotateSortedArray(Integer[] arr, int lo, int hi){
        if(lo < hi){
            if( arr[lo] < arr[hi]) { // no rotatation
                return arr[hi];
            } else{
                int mid = (lo + hi)/2;
                if ( arr[lo] < arr[mid]) {// 1 2 3 4 =>  2 3 4 1
                    return maxRotateSortedArray(arr, mid, hi);
                } else if ( arr[lo] > arr[mid]) {  // 1 2 3 4 => 4 1 2 3
                    return maxRotateSortedArray(arr, lo, mid);
                }else{                          // 2 1 => arr[lo] == arr[mid] , lo == mid
                    return arr[lo];   
                }
            }
        }else{
            return arr[lo];                   // one element
        }
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

