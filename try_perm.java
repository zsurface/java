import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_perm{
    public static void main(String[] args) {
        // test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String s = "abc";
        perm(s, 0);
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String s = "1234";
//        Print.p("take=" + take(1, s));
//        Print.p("drop=" + drop(1, s));
//        Print.p("dropIndex=" + dropIndex(0, s));
//        Print.p("dropIndex=" + dropIndex(2, s));

        HashSet<String> set = new HashSet<String>();
        permPrefixChoose("", s, 2, set);
        for(String ss: set){
            Print.p("ss=" + ss);
        }

//        permPrefix("", s);
        Aron.end();
    }
    public static void swap(StringBuffer sb, int i, int inx){
        char tmp = sb.charAt(inx);
        sb.setCharAt(inx, sb.charAt(i));
        sb.setCharAt(i, tmp);
    }
    public static void perm(String s, int inx){
        if(s != null){
            int len = s.length();
            StringBuffer sb = new StringBuffer(s);
            if( inx == len){
                System.out.println("s=" + s); 
            }else{
                for(int i=inx; i<len; i++){
                    swap(sb, i, inx);
                    perm(sb.toString(), inx + 1);
                    swap(sb, i, inx);
                }
            }
        }
    }
    public static String take(int inx, String s){
        return s.substring(0, inx);
    }
    public static String drop(int inx, String s){
        return s.substring(inx, s.length());
    }
    public static String dropIndex(int i, String s){
       return s.substring(0, i) + s.substring(i+1, s.length());
    }
    public static void permPrefix(String prefix, String s){
        if(s != null){
            if(s.length() == 2){
                // Print.p("prefix=" + prefix);
                Print.p("      s=" + s);
            }else{
                for(int i=0; i<s.length(); i++){
                    String str = s.charAt(i) + "";
                    permPrefix(prefix + str, dropIndex(i, s));
                }
            }
        }
    }
    public static void permPrefixChoose(String prefix, String s, int k, HashSet<String> set){
        if(s != null){
            if(s.length() == k){
                set.add(s);
            }else{
                for(int i=0; i<s.length(); i++){
                    String str = s.charAt(i) + "";
                    permPrefixChoose(prefix + str, dropIndex(i, s), k, set);
                }
            }
        }
    }
    
} 

