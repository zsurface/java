let SessionLoad = 1
if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
inoremap <F9> :w! | :let buildAndRun = 'c' | :call CompileJava(buildAndRun)
inoremap <Right> :tabn
inoremap <Left> :tabp
inoremap <F7> :call ToggleChangeColorScheme(1)
inoremap <F6> :call ToggleChangeColorScheme(0)
inoremap <F3> :call HttpSnippetCall()
inoremap <F2> =ToggleCompletefunc()
imap <C-B> :call AddBegin()
inoremap <C-X><C-M> =MyComplete()
map! <D-v> *
nnoremap <silent>  :call Match2Delete()
map ,m :call ChangeJavaMaven()
map ,<F9> :w! | :let buildAndRun = 'r' |  :call CompileJava(buildAndRun)
map ,n :b #
noremap ,i :call ToggleIgnoreCase()
map ," :exec 'normal vaw str ' 
map ,< :.,$s/\(=.*<\)\([^<>]\+\)/\1/gc | :nohlsearch
map ,( :.,.s/([^()]\+)/()/ | :nohlsearch
map ,; :.,.s/\S.*\S/\0;/ | :nohlsearch
noremap ,s :call ToggleSpell()
nmap <silent> ,c :call CopyJavaMethod()
nmap <silent> ,p :!vopen <cfile>
nmap <silent> ,d :!open dict://<cword>
vnoremap a$ F$of$
onoremap <silent> a$ :normal! F$vf$
vmap gx <Plug>NetrwBrowseXVis
nmap gx <Plug>NetrwBrowseX
nnoremap gO :!open <cfile>
vnoremap i$ T$ot$
onoremap <silent> i$ :normal! T$vt$
vmap xu :s/\%V\_^\s*\zs\/\/\s*\%V//g 
vmap xx :s/\%V\_^\s*\zs\S\%V/\/\/ \0/g 
map <F9> :w! | :let buildAndRun = 'c' | :call CompileJava(buildAndRun)
vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(netrw#GX(),netrw#CheckIfRemote(netrw#GX()))
map <F12> :Bf o
map <F8> :w!
map <F5> :tabnew $HOME/myfile/bitbucket/snippets/snippet.hs 
map <PageDown> :tabc
map <PageUp> :tabnew
nnoremap <Right> :tabn
nnoremap <Left> :tabp
vmap <F2> "*y | :echo 'copy [' . @* . '] to clipboard' 
noremap <F1> :call ChangeBGColor()
noremap <F7> :call ToggleChangeColorScheme(1)
noremap <F6> :call ToggleChangeColorScheme(0)
noremap <F3> :call HttpSnippetCall()
nnoremap <F2> :call ToggleCompletefunc()
onoremap <F2> :call ToggleCompletefunc()
nnoremap <silent> <C-L> :call Match2Delete()
vmap <S-F8> :call AddAbbr()
vmap <BS> "-d
vmap <D-x> "*d
vmap <D-c> "*y
vmap <D-v> "-d"*P
nmap <D-v> "*P
imap  :call AddBegin()
inoremap  =MyComplete()
cmap "" :call Surround('"', '"')  
cmap $$ :call Surround('$', '$')  
cmap '' :call Surround("'", "'")  
cmap (( :call Surround('(', ')')  
cmap ** :call Surround('*', '*')  
inoremap ,s =ToggleSpell()
inoremap ,i =ToggleIgnoreCase()
cmap << :call Surround('<', '>')  
cmap BB s//\[\0\]/  
cmap EE s//\[\0\]/  
cmap FF s/^.*$/\=substitute(getline('.'), '\S\+', '$' . '\0' . '$', 'g')/gc  
cmap NN s/^.*$/\=substitute(getline('.'), '\S\+', '$' . '\0' . '$', 'g')/gc  
cmap SV vim // **/*.m
cmap SS .,$s///gc
cmap White /\S\zs\s\+$
cmap WW s//$\0$/  
cmap [[ :call Surround('\[', '\]')  
cmap hh s//\\text{ \0 }/  
cmap s2 :call Match2()
cmap yy eAppendSome()
cmap {{ :call Surround('{', '}')  
iabbr <expr> jll_ListOfList 'ArrayList<ArrayList<String>> list2d = new ArrayList<ArrayList<String>>();' . "\" . "^"
iabbr <expr> jm_HashMap 'Map<String, Integer> map = new HashMap<String, Integer>();' . "\" . "^"
iabbr <expr> jl 'List<String> list = new ArrayList<String>();' . "\" . "^" . ":.,.s/String/Integer/gc" . ""
iabbr <expr> jim 'import java.io.*;import java.lang.String;import java.util.*;' . "\" . "^"
iabbr <expr> for2_two_for_loop 'for(int xxx=0; xxx < 9; xxx++){for(int xxx=0; xxx < 9; xxx++){}}' . "\" . "3k" . "^" . ":.,.s/xxx/i/gc" . ""
iabbr <expr> forr_one_for_loop 'for(int xxx=0; xxx<10; xxx++){}' . "\" . "1k" . "^". ":.,.s/xxx/i/gc" . ""
iabbr <expr> jprr_system_out_println 'System.out.println(xxx)' . "\" . "^" . ":.,.s/xxx/i/gc" . ""
iabbr <expr> r- repeat('-', 80) . '' 
iabbr <expr> r= '80i='
cabbr qvim :source $HOME/myfile/bitbucket/vim/quick.vimrc
cabbr vnote :Bf o
iabbr <expr> --- repeat('-', 80) . '\<CTRL-0>'
iabbr <expr> ddd strftime('%c')
iabbr vImply ⟺
iabbr vimply ⟷
iabbr vdownwardsarrow ↓
iabbr vupwardsarrow ↑
iabbr vRightarrow ⟹ 
iabbr vrightarrow →
iabbr vface 😀
cabbr se :call SetEnvVar() 
cabbr Ma :call MaximizeToggle() 
cabbr Ela :tabnew /Library/WebServer/Documents/$wlocalhost/html/indexLatexMatrix.html
cabbr Info :tabnew ~/.viminfo
cabbr Ta call TagsSymlink() 
cabbr Tb :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/bash & 
cabbr Tf :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/fish & 
cabbr Tz :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/zsh & 
cabbr Ky :let @*=expand("%:p")
cabbr Job :tabnew $HOME/GoogleDrive/job/recruiter_email.txt 
cabbr Ran :tabnew /Library/WebServer/Documents/$wlocalhost/randomNote.txt
cabbr No :tabnew $HOME/myfile/bitbucket/note/noteindex.txt 
cabbr Ep :tabnew $HOME/myfile/vimprivate/private.vimrc
cabbr Ec :tabe /Library/WebServer/Documents/$wlocalhost/html/indexCommandLineTricks.html  
cabbr Eng :tabe /Library/WebServer/Documents/$wlocalhost/html/indexEnglishNote.html  
cabbr Evimt :tabe /Library/WebServer/Documents/$wlocalhost/html/indexVimTricks.html
cabbr Enote :tabe /Library/WebServer/Documents/$wlocalhost/html/indexDailyNote.html
cabbr Esty :tabe /Library/WebServer/Documents/$wlocalhost/style.css
cabbr Sty :%!astyle --style=java 
cabbr Word :tabe $HOME/myfile/bitbucket/vim/myword.utf-8.add    " My words file
cabbr jf :tabe /tmp/f.x
cabbr mm :marks
cabbr qv :tabe $HOME/myfile/bitbucket/script/setenv.sh " quick set environment variables 
cabbr qw :tabe $HOME/myfile/bitbucket/note/quickwrite.xxx " quick write up, post on redit 
cabbr ess :tabe $HOME/myfile/vimsession/sess.vim 
cabbr ssess :source $HOME/myfile/vimsession/sess.vim
cabbr mk mksession! $HOME/myfile/vimsession/sess.vim
cabbr ep :tabnew /etc/profile 
cabbr mmax printf '\ePtmux;\e\e[9;1t\e\\'
cabbr mhalf printf '\ePtmux;\e\e[8;100;200t\e\\'
cabbr full printf '\e[10;1t' " full-screen mode 
cabbr umax printf '\e[10;0t' " undo full-screen mode 
cabbr half printf '\e[8;100;200t \e[3;1410;0t' " resize to 200x100/move to right
cabbr maxh printf '\e[9;3t' " maximize horizontally
cabbr maxv printf '\e[9;2t' " maximize vertically
cabbr max printf '\e[9;1t' " maximize
cabbr min printf '\e[2t' " minimize window
cabbr hlib exec ':tabnew ' . g:haskellAronFile
cabbr jlib exec ':tabnew ' . g:javaAronFile
cabbr bk :tabnew $HOME/myfile/bitbucket/bookmark/bookmark.txt
cabbr sll :call StatusLineManager()
cabbr ev :tabe $HOME/myfile/bitbucket/vim/vim.vimrc
cabbr eh :tabe $HOME/myfile/bitbucket/shell/dot_bash_history
cabbr sv :source $HOME/myfile/bitbucket/vim/vim.vimrc 
cabbr big :tabe  $HOME/cat/myfile/github/java/big.java
cabbr kk .g/\S*\%#\S*/y | let @*=@" 
cabbr ip :call  InitDir()
cabbr fp :call  CopyCurrentFilePath()
cabbr filewatcher :tabnew $HOME/myfile/bitbucket/haskell/filewatcher.hs
cabbr aa %s/\(,\)\(\w\)/\1\r\2/g
cabbr mmm %s/--- a/\/home\/longshu\/myfile\/workspace\/ContraCogsFunding\/src\/ContraCogsFundingBookingLibrary/g
cnoreabbr <expr> rmcab RemoveAllcabbr() 
cnoreabbr al AddLine 
iabbr cmain #include <stdio.h>void main(void) { printf("Hello World\n");}if [ "$#" -eq 1 ]; then    echo "hi"elif    echo "hi"else    echo "ho"fi
iabbr myshell #include <stdio.h>void main(void) { printf("Hello World\n");}if [ "$#" -eq 1 ]; then    echo "hi"
cabbr eb :tabe ~/.bashrc
let &cpo=s:cpo_save
unlet s:cpo_save
set autochdir
set autoindent
set autoread
set background=dark
set backspace=2
set complete=k$HOME/myfile/bitbucket/javalib/*.java,k$HOME/myfile/bitbucket/jsource/java/util/*.java,k$HOME/myfile/bitbucket/jsource/java/lang/*.java,k$HOME/myfile/bitbucket/jsource/java/math/*.java
set completefunc=CompleteJavaLib
set dictionary=~/myfile/bitbucket/vim/words.txt
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set helplang=en
set hlsearch
set ignorecase
set iskeyword=@,48-57,_,192-255,<,>
set laststatus=2
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set path=.,/usr/include,,,**
set ruler
set shell=/bin/bash\ --rcfile\ /etc/profile
set shiftwidth=4
set showcmd
set smartindent
set spellfile=~/myfile/bitbucket/vim/myword.utf-8.add
set statusline=%t\ [%1.5n]\ %l:%c\ %r\ %m
set noswapfile
set tabstop=4
set tags=./tags,tags,~/.vim/tags/java.tags,~/myfile/bitbucket/java/.tag,~/myfile/bitbucket/javalib/.tag
set textwidth=1200
set notimeout
set timeoutlen=100
set ttimeout
set viminfo='100,<50,s10,h,n$HOME/myfile/bitbucket/vim/my_viminfo.vimrc
set wildmenu
set nowritebackup
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/myfile/bitbucket/java
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
argglobal
%argdel
$argadd EvaluateTree.java
edit ~/myfile/bitbucket/java/EvaluateTree.java
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
vmap <buffer> str :s/\%V.*\%V/"\0"/ 
let s:cpo_save=&cpo
set cpo&vim
map <buffer> <F4> va}Vy  
iabbr <buffer> phone_cell 778-232-2175
iabbr <buffer> phone_US 408-844-4280Phone: 408-844-4280
iabbr <buffer> redis_install Tue Apr  2 18:09:27 2019Install Redis on MacOS--brew install redisredis-server /usr/local/etc/redis.confredis-cli ping  # If it replies good to go!-------------------------------------------------------------redis select databaseredis-clidatabase 0    # select database 0find key:keys try*     # all key starts with try-------------------------------------------------------------redis-cli --stat     # show all redis status-------------------------------------------------------------
iabbr <buffer> apache_restart sudo service apache2 restart/etc/apache2/sites-available           # freebsd apache/usr/local/etc/rc.d/apache24 restart   # MacOS apache config--/etc/apache2/httpd.conf                # MacOS restart apache, restart apachactlsymbolink to/Users/cat/myfile/bitbucket/publicfile/httpd.conf_apacheapachectl restart---/etc/apache2/httpd.conf-- change DocumentRoot to your Html root.DocumentRoot "/Library/WebServer/Documents/xfido"   # DocumentRoot<Directory "/Library/WebServer/Documents/xfido"></Directory>--
iabbr <buffer> mysql_create_table DROP TABLE person;CREATE TABLE person(id INTEGER NOT NULL AUTO_INCREMENT,name TEXT NOT NULL,email TEXT NOT NULL,PRIMARY KEY(id));INSERT INTO person(name, email) VALUES("name1", "mail@gmail.com");INSERT INTO person(name, email) VALUES("name2", "mail@gmail.com");INSERT INTO person(name, email) VALUES("name3", "mail@gmail.com");----------------SELECT * from person;----------------------# table with NULL value and empty stringDROP TABLE person2;CREATE TABLE person2(id INTEGER NOT NULL AUTO_INCREMENT,name TEXT NOT NULL,middle_name TEXT,email TEXT NOT NULL,PRIMARY KEY(id));INSERT INTO person2(name, middle_name, email) VALUES("name1", "middle1", "mail@gmail.com");INSERT INTO person2(name, middle_name, email) VALUES("name2", "middle2", "mail@gmail.com");INSERT INTO person2(name, middle_name, email) VALUES("name3", "", "mail@gmail.com");INSERT INTO person2(name, middle_name, email) VALUES("name3", NULL, "mail@gmail.com");----------------------SELECT * FROM person2 WHERE middle_name = "";SELECT * FROM person2 WHERE middle_name IS NULL;----------------------
iabbr <buffer> emacs_key ALT-Up/Down                  # Move list up and down, very usefulALT-Left/Right               # Shift list to left or right--------------------------------------------------------------------------------C-x r   a              # Use letter 'a' to mark locationC-x r j a                    # Jump back to location 'a'--------------------------------------------------------------------------------M-x intero-restart           # restart intero--------------------------------------------------------------------------------M-,                          # Jump back to previous location in Haskell IntroM-.                          # Jump to defintion in Haskell Intero--------------------------------------------------------------------------------C-x 8 RET unicode number     # input unicode in org mode, unicode input--------------------------------------------------------------------------------M =>                         # caps lock on MacBookP and Big KeyboardM-x org-mode                 # load orgmodeC-c C-x C-l                  # run Latex snippet--------------------------------------------------------------------------------C-s char                     # jump to charM-% string RET newString RET # search and replaceM-x eval-buffer              # reload abbrev_defsC-x -/+                      # increase/decrease font sizeC-x e                        # evaluate lispC-x C-e                      # evaluate lisp from current cursorM-x abbre-mode               # enable abbrev modeM-/                          # auto completeC-x RET C-\ TeX RET          # input latex char, latex inputM-!                          # shell_command => Vim:ls---------------------------------------------------------------------------------- MovingC-l moving to bottom => l = lower?C-v M-v                      # page up, page down--------------------------------------------------------------------------------C-x 1                        # maximize current bufferC-x 2                        # split window horizonallyC-x 3                        # split window verticallyC-x 0                        # close current bufferC-z                          # goto background fg => back to foreground--------------------------------------------------------------------------------M-x shell                    # run shell within EmacsM-x !                        # run shell commandM-x :                        # run lisp codeM-x %                        # search and replaceM-k                          # kill line, emacs kill line, emacs delete line--------------------------------------------------------------------------------[see => ~/.emacs]C-a C-e                      # move cursor to begin and End of lineC-n C-p                      # move line down, line upC-f                          # move cursor word forwardC-b                          # move cursor word backward--------------------------------------------------------------------------------C-k                          # kill all word to end of line or end of sentenceC-u/C-_                      # undoC-r C-r                      # backward search goto next occurrentC-s C-s                      # search goto next occurrentC-x C-f                      # find fileC-s                          # search file I-searchC-x C-s                      # save fileC-x b                        # switch bufferC-x o                        # switch to other window--------------------------------------------------------------------------------C-x Spa                      # set mark, selection in vimS-Left or S-Right            # select with Left and Right key, it is much easier.M-w                          # copyC-y                          # paste  => Vim y => yanked--------------------------------------------------------------------------------S-Left/Right                 # select textC-x Tab                      # shift text, shift line, alignment, indentation modeS-Left/Right                 # indentation--------------------------------------------------------------------------------C-x C-b                      # list bufferM-:                          # MacOS (alt shift) :M-:                          # caps lock : => evaluate elispM-g M-g                      # Alt g, Alt g => goto-lineC-x C-c                      # exit EmacsF1                           # help commandC-x -/+                      # increase decrease font--------------------------------------------------------------------------------M-x load-theme RET themename                     # load theme, color schemeM-x list-packages  'i' => mark, 'x' => install   # select package to installC-c C-c => compile, C-c C-c => view              # LatexC-c C-h => See all Auctex shortcut               # AuctexC-c ] => close latex environmentF3                                               # start define MacroF4                                               # end define MacroF4                                               # repeat Macro----------------------------------------------add the followig to .emacs file for agda-mode in Emacs(load-file (let ((coding-system-for-read 'utf-8))(shell-command-to-string "/usr/local/Cellar/agda/2.5.4.1/bin/agda-mode locate")))--------------------------------------------------------------------------------M-x describe-input-methodfind TeX inputM-x describe-input-method => TeX => see all Tex Input in Emacsremaping many Emacs default keys binding--------------------------------------------------------------------------------C-h v                              # Describle variable: user-init-file--------------------------------------------------------------------------------M-x describe-function RETprint--------------------------------------------------------------------------------C-s => forward search char 'a' => 'c' => press DEL  # delete selection--------------------------------------------------------------------------------C-x Space => M-w => C-y            # select => copy => paste--------------------------------------------------------------------------------C-x [                              # goto the begin of a fileC-x ]                              # goto the end of a file--------------------------------------------------------------------------------C-x {                              # shrink the width of a windowC-x }                              # enlarge the width of a window-------------------------------------------------------------------------------C-x z => z                         # repeat last command-------------------------------------------------------------------------------C-x eval-buffer                    # elisp on scratch buffer-------------------------------------------------------------------------------(thing-at-point 'word 'properties) # get current cursor word(shell-command-to-string '/Users/cat/myfile/symbin/redis_query.hs')C-c C-e h h-------------------------------------------------------------------------------M-x kill-buffer                    # kill buffer, delete file from buffer :bd file in Vim-------------------------------------------------------------------------------C-h k                              # find key mappingC-h w                              # which key a function is mapping to, e.g  forward-lineC-h v                              # variable definition-------------------------------------------------------------------------------
iabbr <buffer> python_import import osimport syssys.path.insert(0,'/Users/cat/myfile/bitbucket/python')import AronLib as aimport subprocessimport itertools as it
iabbr <buffer> agda_example C-c C-x C-c => Choose GHC         # compile agda, agda compile, agda emacsC-c C-n                           # run your function in minibuffer--------------------------------------------1. run: agda-mode setup from command line2. M-x load-library3. M-x agde2 => M-x agda2-mode    # agda modeif agda2-mode can not be loaded,delete agda2-mode.elc and agda2-highlight.elc filerun agda-mode compile to find the pathagda-mode locate  => show path to agda2.el file for emacs modeM-x describe-char => find out char input in agda mode--------------------------------------------
iabbr <buffer> haskell_stack stack install --local-bin-path /tmp/stack path --local-install-root   # show binary file path, show executable pathstack build --fast                # no optimizationstack build --file-watch          # build and file watcher
iabbr <buffer> shell_logic_operator if [[ "$num" =~ [0-9]+ ]]; then echo "number"; else "not a number"; fiif [[ "$str" =~ [a-z]+ ]]; then echo "string"; else "not a string"; fiif [[ "$str" =~ [_]    ]]; then echo "contains _"; else "not contain _"; fiif [[ "$str" =~ [0-9]+ || "$str" =~ [a-z]+ ]]; then echo "str or num"; else "neither a str or a num"; fiif [[ "$#" -eq 2 && "$2" =~ [0-9]+ ]]; then echo "2 arguments, and $2 is num"; else echo "false"; fiif [[ "$#" -eq 1 && "$1" == '-m' || "$1" == '-p' ]]; then echo "hi"; fi
iabbr <buffer> java_array_dim import java.lang.reflect.Array;public class Demo {public static void main (String args[]) {int[] arr = {6, 1, 9, 3, 7};Class c = arr.getClass();if (c.isArray()) {Class arrayType = c.getComponentType();System.out.println("The array is of type: " + arrayType);System.out.println("The length of the array is: " + Array.getLength(arr));System.out.print("The array elements are: ");for(int i: arr) {System.out.print(i + " ");}}}}
iabbr <buffer> jimport_import --import static classfile.Aron.*;import static classfile.Print.*;import static classfile.Test.*;import classfile.Node;import classfile.Tuple;--import java.util.Vector;import java.util.ArrayList;import java.util.List;import java.util.StringTokenizer;import java.util.Arrays;import java.util.List;import java.util.Queue;import java.util.LinkedList;import java.util.concurrent.atomic.AtomicInteger;import java.util.Collections;--import java.util.logging.Level;import java.util.logging.FileHandler;import java.util.logging.Formatter;import java.util.logging.Level;import java.util.logging.Logger;import java.util.logging.SimpleFormatter;import java.io.IOException;
iabbr <buffer> Java_list_to_array List<String> list = new LinkedList<String>();list.add("dog");list.add("cat");list.add("pig");String[] arr = list.toArray(new String[0]);p(arr);//List<String> list = new LinkedList<String>();list.add("dog");list.add("cat");list.add("pig");String[] arr = list.stream().toArray(String[] ::new);p(arr);
iabbr <buffer> java_classfile import static classfile.Aron.*;import static classfile.Print.*;import static classfile.Test.*;import classfile.Tuple;import classfile.Node;
iabbr <buffer> awk_shell awk '{print $1 $2}' file.txt                   # awk printawk '{for(i=0; i<NF; i++) print $1}' inputfile # awk print the first column from fileawk -F ":" '{for(i=0; i<NF; i++) print $1}'    # awk change field separator to :awk -F, '{print $2 " " $3 }'                   # awk change filed separator to ,echo "$a,$b,$c" | awk -F, '{ print "A="$1 "\nB="$2 "\nC="$3 }'   # awk commakill -9 $(ps aux | awk '/Safari/ {print $2}')  // kill Safari browser  # ps aux awkls -l $jarpath/*.jar | awk '{print $6 " " $7 " " $8 "\t" $9}'          # awk jar---------------------------------------------------------------------cat file.txta phone blrb call mumc telephone chen--awk 'BEGIN{OFS="|"} {print $1,$2,$3}' file.txt     # awk OFSa|phone|blrb|call|mumc|telephone|chen---------------------------------------------------------------------file:///Library/WebServer/Documents/xfido/image/awk_programming.png
iabbr <buffer> emacs_mode M-x describe-variable RET major-mode
iabbr <buffer> java_gradle <pre class="cc">create Lib under [root.Project.name="MyProject"] from setting.gradleadd following to build.gradledependencies {compile fileTree('Lib')  // this includes all the files under 'Lib'}</pre>
iabbr <buffer> unicode_macos CMD CTRL SPACE     # input unicode from macOS
iabbr <buffer> intellij_keybinding https://bitbucket.org/zsurface/intellij/src/master/keymap/
iabbr <buffer> find_man find -E -f $b/cpp $b/cpplib  -regex '.*/*\.(cpp|c)'  -print          # extension regexfind [-E] [-f path]  pathfind  -E  -f $b/cpp  $b/cpplib -regex '.*/.*\.cpp|*/\.*\.c' -print   # ERROR regex extension------------------------------------------------------find -E -f $b/cpp  $b/cpplib -regex '(.*/.*\.cpp)|(.*/*\.c)' -print  # regular regex, *.cpp or *.c
iabbr <buffer> shell_readfile filename=/tmp/ff.xwhile read -r linedocat $line >> $tt/ion.xdone < "$filename"
iabbr <buffer> js_path_hell <!-- It seems to me the path has to be relative. Absoluate path DOES NOT work here --><!-- javascript file CAN NOT be out side of www or root folder --><!-- One option: create a symlink inside your www or root folder -->
iabbr <buffer> js_script style="display:none;font-size:18px;"        # hide your textarea<textarea name="header" rows="20" id='f1' style="display:none;font-size:18px;">hidden text line 1hidden text line 2</textarea>
iabbr <buffer> jmap_ListList_init_iterator Map<String, List<List<String>>> map1 = new HashMap<>();Map<String, List<List<String>>> map2 = new HashMap<>();// mutable listList<String> list1 = new ArrayList<>(Arrays.asList("cat", "dog", "cow"));List<String> list2 = new ArrayList<>(Arrays.asList("cat1", "dog1", "cow1"));List<List<String>> listList1 = new ArrayList<>();listList1.add(list1);List<List<String>> listList2 = new ArrayList<>();listList2.add(list2);//map1.put("key1", listList1);map2.put("key2", listList2);Map<String, List<List<String>>> map = mergeMapListList(map1, map2);//for(Map.Entry<String, List<List<String>>> entry : map.entrySet()){System.out.println("[" + entry.getKey());for(List<String> list : entry.getValue()){for(String s : list)System.out.println("[" + s + "]");}}
iabbr <buffer> jarr_Character Character[] arr = {'c', 'a', 't', 's'};int width = arr.length;
iabbr <buffer> jarr_arr2d_4x1_1_to_n int[][] arr2d = {{ 1},{ 2},{ 3},{ 4},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_String String[] arr = {"techie", "dangling", "cat", "scene", "ancestor","scene", "descend", "descended", "sibling", "dangling"};
iabbr <buffer> latex_log \[\begin{aligned}a^b + 1 &= a^b + a^0 \\b^a + 1 &= b^a + b^0 \\a^x a^y &= a^{x + y}  \qquad \text{By } \log \text{definition} \\\log_a (a^x a^y) &= x + y \\\log_a a^x a^y &= x + y  = \log_a a^x + \log_a a^y \\\Rightarrow \log a^x a^y &= \log a^x + \log a^y\end{aligned}\]
iabbr <buffer> jlist_list_of_list List<List<Integer>> lss = new ArrayList<List<Integer>>();List<List<String>> lss = new ArrayList<List<String>>();
iabbr <buffer> vancouver_lib 21383027629265
iabbr <buffer> jlist_to_set // list => setSet<String> set1 = new HashSet<String>(Arrays.asList("cat", "dog"));// set => listList<String> list = new ArrayList<>(set1);Set<String> set2 = new HashSet<>(list);Aron.printSet(set1);Aron.printList(list);// list of listList<List<Integer>> lss = new ArrayList<List<Integer>>();
iabbr <buffer> emacs_latex C-c C-c  # compile latexC-c C-l  # view pdf file-------------------------------------M-tab    # auctex autocomplete symbolC-c C-e  # autocomplete environmentC-c C-m  # autocomplete macroC-c C-s  # autocomplete section`        # autocomplete math mode, alpha
iabbr <buffer> jsubstring_substring_subList_prefix_suffix String line = "0123";for(int i=0; i<line.length(); i++){String prefix = line.substring(0, i);String suffix = line.substring(i, line.length());Print.pbl("prefix=" + prefix + " suffix=" + suffix);}// mutable listList<String> list = new ArrayList<>(Arrays.asList("cat", "dog", "cow"));for(int i=0; i<list.size(); i++){List<String> subList = list.subList(0, i+1);Aron.printList(subList);}
iabbr <buffer> java_sort_str String str = "cba";char[] chars = str.toCharArray();Arrays.sort(chars);String str1 = new String(chars);
iabbr <buffer> java_jshell jshell --class-path $(ls -d $b/javalib/jar/* | tr '\n' ':')      # load all jar filesimport static classfile.Aron.*;                                  # import Aron.java
iabbr <buffer> jpriorityqueue_Interval_Example class Interval implements Comparable<Interval>{int start;int end;int data;public Interval(int start, int end, int data){this.start = start;this.end = end;this.data = data;}public int compareTo(Interval e){if(e != null){return start - e.start;}throw new IllegalArgumentException("e must not be null");}public String toString(){return "[" + start + "][" + end + "][" + data + "]";}}//public class Try102{//    public static void main(String[] args) {//        test0();//    }//    static void test0(){//        Aron.beg();//        Queue<Interval> queue = new PriorityQueue<Interval>();//        Interval ter1 = new Interval(1, 4, 10);//        Interval ter2 = new Interval(4, 4, 2);//        Interval ter3 = new Interval(2, 8, 10);//        Interval ter4 = new Interval(3, 4, 5);//        queue.add(ter1);//        queue.add(ter2);//        queue.add(ter3);//        queue.add(ter4);////        while(!queue.isEmpty()){//            Print.p(queue.poll());//        }//        Aron.end();//    }//}//Queue<Interval> queue = new PriorityQueue<Interval>();Interval ter1 = new Interval(1, 4, 10);Interval ter2 = new Interval(4, 4, 2);Interval ter3 = new Interval(2, 8, 10);queue.add(ter1);queue.add(ter2);queue.add(ter3);while(!queue.isEmpty()) {Print.p(queue.poll());}//class Person{String name;Integer age;public Person(String name, Integer age){this.name = name;this.age = age;}}//// lambda priorityqueue lambdaPriorityQueue queue = PriorityQueue<>((x, y) -> x.age - y.age);queue.add(new Person("David", 100));queue.add(new Person("Michael, 20));queue.add(new Person("Paut", 30));queue.add(new Person("Kelvin", 40));//while(queue.size() > 0){Person pe = queue.remove();p(pe.name);p(pe.age);}
iabbr <buffer> lisp_elisp -- define a function(defun f (a b)(+ a b))--(message (number-to-string (f 1 s))-- get current buffer name(buffer-file-name) // Get current buffer name
iabbr <buffer> emacs_org *text* _underline_ =verbatim= ~code~
iabbr <buffer> find_command find . -name "*" | xargs -n1 perl -i -ane 's/images/img/g; print;'find . -name "*.txt" -type f  -print0 | xargs -0 echofind . -name "*.m" -exec sed -i backup 's/old/new/g' {} + # backup is ext for backup file, unknown primary op errorfind . -name "*.m" -exec sed -i '' 's/SimpleApp/DeviceInfoApp/g' '{}' \; # man find execfind . -name "abc*" ! -name "*.cpp"                 # file start with abc except all *.cpp filefind . -name "*.txt" -o -name "*.cpp"               # -o OR operatorfind . -name "*.txt" -exec rm -f {} \;              # remove all *.txtfind . -maxdepth 1 -type f -perm +111 -exec rm -f {} \;  # remove all executable files in curr dirfind all the files or directories in the current path and copy them to new directoryfind ./ -name \*.mp3 -exec cp {} /home/user/song \;find ./ -name "*.txt" -mtime -4  -printfind $(find ./ -name *.java -type f)find ./ -name "dirName"find $PWD -iname '*.java'------------------------------------------------------exclude paths contains '.git'find $PWD -not -iwholename "*.git*"------------------------------------------------------
iabbr <buffer> google_bookmark file:///Users/cat/Library/Application%20Support/Google/Chrome/Default/Bookmarks
iabbr <buffer> vim_fold default marker {{{ -- ,}}}zo - openza - close" Ex mode, fold line from current to next 20 lines:.,+20/fold" fold line from 1 to N:1;/pattern/fold" in Normal modeggzf20G"" fold line foldtext:set foldtext=MyFoldText():function MyFoldText():   let n = v:foldend - v:foldstart + 1:   return 'This is a fold of ' . n . ' lines':endfunction"" fold regex:set foldexpr=v:lnum<=20&&getline(v:lnum)=~'pattern'http://vim.1045645.n5.nabble.com/hide-lines-from-view-td1143659.html
iabbr <buffer> shell_script_history history => #!/bin/bash -i ,  history does not work in shell script
iabbr <buffer> shell_modulo if (( $n % 5 == 0 ))thenecho $nfi
iabbr <buffer> sed_shell sed 's/.*\(Name=\"[^"]*\"\)\s\(MyDir=\"[^"]*\"\).*/\1 \2/g' f1.txt > f2.txt //Match Name=xx, MyDir=xx 2 colsed -i -e 's/oldstring/newstring/g' *  // replace oldstr to newstr, replace all, sed replacesed '/line3/,/line9/d' oldfile > newfile  // delete lines from line contains line3 to line contains line9sed -E '/grep|vim/d'  // posix regular extension -E---- sed on MacOS and FreeBSD does not support -E which is regular expression extension, (GNU)gsed -i_backup 's/oldstr/newstr/' *.html  // file is backed up with *.html_backupsed 's/ *//g'  # remove all whitespace, sed -E 's/\s*//g' does not work on MacOSsed 's/[[:space:]]*//g' # it works on MacOS---- remove whitespaces from all files in current directory.for i in *; do var=$(echo $i | sed 's/[[:space:]]*//g'); mv "$PWD/$i" "$PWD/$var"; done
iabbr <buffer> lynx_browser dump html to filelynx -dump url > $t
iabbr <buffer> java_buffered BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));int radiusCount = Integer.parseInt(bufferedReader.readLine().trim());List<Integer> radius = IntStream.range(0, radiusCount).mapToObj(i -> {try {return bufferedReader.readLine().replaceAll("\\s+$", "");} catch (IOException ex) {throw new RuntimeException(ex);}}).map(String::trim).map(Integer::parseInt).collect(toList());----------------------------------------int numberOfSegments = Integer.parseInt(bufferedReader.readLine().trim());String result = Result.largestSegment(radius, numberOfSegments);bufferedWriter.write(result);bufferedWriter.newLine();bufferedReader.close();bufferedWriter.close();
iabbr <buffer> osversion_os uname -a
iabbr <buffer> git_server ---------------------------------------------------------------------------------- create user called 'git' in your FreeBSD-- create repos directory called 'xfido'su gitcd /home/git/mkdir xfidocd xfidogit init --bare --shared---------------------------------------------------------------------------------- On you local machine, create a respo.git initgit remote add origin git@uwspace.com:/home/git/xfido--------------------------------------------------------------------------------
iabbr <buffer> git_mygit [core]repositoryformatversion = 0filemode = truebare = falselogallrefupdates = trueignorecase = trueprecomposeunicode = true[remote "origin"]#url = git@github.com:bsdshell/bsdshell.github.io.git#url = https://zsurface@bitbucket.org/zsurface/zsurface.giturl = git@uwspace.com:/home/git/xfidofetch = +refs/heads/*:refs/remotes/origin/*
iabbr <buffer> latex_link http://localhost/html/indexLatexMatrix.html
iabbr <buffer> latex_cases f(x)=\begin{cases}\frac{x^2-x}{x},& \text{if } x\geq 1\\0,              & \text{otherwise}\end{cases}
iabbr <buffer> note_database Java connects to mysql mysql database java database java mysql---------------------------------------------------------------See File /Users/cat/myfile/bitbucket/java/SqlExample/src/Main.java------------------------------------------------------------------Java connect to mysql it is always painful processing1. The old Java Connector Driver is not working any moreso I need to download NEW Java Connection Driver mysql-connector-java-8015jarthe old mysql connector is not longer working in the current MySql 8012  Check version mysql -u root2. The new Java Connector needs SSL by default so you need to explicitly specify "useSSL=false"3. The new Java Connector needs timezone for the connector string eg  "Timezone=UTC"------------------------------------------------------------------------Finally SqlExample can make a conneciton to my local SqlServer on My MacOS 10.13.4
iabbr <buffer> rent https://www.vansky.com/info/adfree/1165436.htmlhttps://www.vansky.com/info/adcommercial/11833.html 700https://www.vansky.com/info/adfree/1664999.html 850, 6th roadhttps://www.vansky.com/info/adfree/1664626.htmlhttps://www.vansky.com/info/adfree/1665275.html 900https://www.vansky.com/info/adfree/1664727.html 650https://www.vansky.com/info/adfree/1663783.html 750 good one, need to call---https://www.vansky.com/info/adfree/1670334.html
iabbr <buffer> cpp_to_float ----------------------------------------------------atof(string s) - Convert string to doubleatoi(string s) - Convert string to integeratol(stirng s) - Convert string to long integeratoll(stirng s) -  Convert string to long long integerstrtod(string s) - Convert string to doublestrtof(string s) - Convert string to floatstrtol(string s) - Convert string to long integerstrtold(stirng s) - Convert string to long doublestrtoll(string s) - Convert string to long long integerstrtoul(string s) - Convert string to unsigned long integerstrtoull(string s) - Convert string to unsigned long long integer----------------------------------------------------http://www.cplusplus.com/reference/cstdlib/----------------------------------------------------string to_string (int val);string to_string (long val);string to_string (long long val);string to_string (unsigned val);string to_string (unsigned long val);string to_string (unsigned long long val);string to_string (float val);string to_string (double val);string to_string (long double val);----------------------------------------------------
iabbr <buffer> vim_script " Fix the missing -cp option in java" Assume java file is in current bufferfunction! CompileJava()let path = expand("%")exec ":!javac " . path let jclassName = expand("%:p:t:r")let className = expand("%:p:t:r")let cwd = getcwd()let full = ":!java -cp " . cwd . ":. " . jclassNameexec  fullendfunction//:str =~ "n"    //compare string, return 1 if str == "n" else return 0if str =~ "\s*"" str is empty or whitespaceendif//*expand-example*:expand("%")       => file.txt:expand("%:p")     => /home/file.txt:expand("%:p:t")   => file.txt:expand("%:p:r")   => /home/file:expand("%:p:t:r") => file:fnamemodify('/dog/f.txt', ':h') => /dog//" get current file dir:getcwd()"/home//" read file:readfile(fname)//" check whether a file with the name exists:filereadable(fname)//" write file:writefile(fname, "a"):writefile(fname, "b") "b" binary mode//[:h Dictionary]" dictionarylet dict={'key': 'value'}//" replace valuedict.key = 'dog'//" Iterate through Dictionaryfor [key, value] in items(dict)echo key . ':' . valueendfor//" remove keylet value = remove(dict, 'key')echo value  " dog//[:h List]" list as valuelet list = ['a', 'b']add(dict.key, 'abc')echo dict.key   " ['a', 'b', 'abc']//" list, string, length, lenlet list = []let list = ["cat", "dog"]let len = len(list)let fname = split(expand('%:p'), '/')let fname = split("dog cat", '\s\+')  OKlet fname = split('dog cat', '\s\+')  OKNOTE: split("dog cat", "\s\+")        Wronglet length = length("this is cool")//" remove list item:unlet list[3] " remove fourth item:unlet list[3:] " remove foruth to last//" remove dictionary item:unlet dict['two']:unlet dict.two//" This is especially useful to clean up global variables:unlet global_var//" sort a listlet list = ['b', 'a']sort(list)//" sort a list in-place, a list remain unmodifiedlet copylist = sort(copy(list))//" Iterate buffer listlet bufcount = bufr("$")let curr = 1while curr <= bufcountlet currbufname = bufname(curr)if buflisted(currbufname) && strlen(currbufname) > 0echo currbufnameendiflet curr = curr + 1endwhile//" Iterate through Listfor item in mylistecho itemendfor//" for looplet index = 0while index < len(mylist)echo mylist[index]let index = index + 1endwhile//" list of open windows:echo range(1, winnr('$'))//" To determine how many split-windows are openwinnr('$')//" How many in the current pagetabpagewinnr(tabpagenr(), '$')//http://vim.1045645.n5.nabble.com/how-to-get-list-of-open-windows-td1164662.html" List all split window by tab pagewhile t <= tabpagenr('$')echo 'tab' t . ':'let bufs = tabpagebuflist(t)let w = 0while w < tabpagewinnr(t, '$')echo "\t" . bufname(bufs[w])let w += 1endwhilelet t += 1endwhileunlet t w bufs//" split window on the right hand side:set splitright//" statusline format%-0{minwid}.{maxwid}{item}The - forces the item to be left alignedThe 0 forces numeric value to be left padded with zeros//" statusline evaludate functionstatusline+=%10{expand('%:p:h')}//// vim variableb:	  Local to the current buffer.w:	  Local to the current window.t:	  Local to the current tab page.g:	  Global.l:	  Local to a function.s:	  Local to a :source ed Vim script.a:	  Function argument (only inside a function).v:	  Global, predefined by Vim.//:let l:pos = getpos(".")    // keep cursor position, restore cursor position:call setpos(".", l:pos)//:filter /^\u\w\+/ cab    // show all first letter of command abbreviations is uppercase.//*v_dict*  *vscr_dict* *vim_dict*:if has_key(dict, 'foo')	" TRUE if dict has entry with key "foo":if empty(dict)			" TRUE if dict is empty:let l = len(dict)		" number of items in dict:let big = max(dict)		" maximum value in dict:let small = min(dict)		" minimum value in dict:let xs = count(dict, 'x')	" count nr of times 'x' appears in dict:let s = string(dict)		" String representation of dict:call map(dict, '">> " . v:val')  " prepend ">> " to each item//*v_change_dir* *v_dir*:autochdir acd noautochdir-----------------------------------------" functional programming, map, filter, join, lambad" concat stringjoin(['dog', 'cat', 'cow'])      => 'dog cat cow'join(['dog', 'cat', 'cow', '\n') => 'dog\ncat\cow'join => Concat-----------------------------------------filter(['dog', 'cat', 'cow'], 'v:val != "dog"') => ['cat, 'cow']------------------------------------------let dict = {1:'dog', 2:'cat', 3:'cow'}filter(dict, 'k:key > 1')  => [2:'cat', 3:'cow']map(['dog','cat'], ' ">" . v:val . "<"') => ['>dog<', '>cat<']map(['dog', 'cat'], {x -> '>' . v:val . '<'}) => ['>dog<', '>cat<']map(['dog', 'cat'], {x -> '>' . x . '<'})     => ['>0<', '>1<']
iabbr <buffer> java_compile_jar JVM	Major Version (decimal)Java 1.4	48Java 5	49Java 6	50Java 7	51Java 8	52Java 9	53--This major version number is stored in the header of the .class file, at byte 7.Determining major version--javap -verbose MyClass | grep "major"--------------------------------------- check java JDK, check java runtime version and morejava -XshowSettings:properties -version
iabbr <buffer> haskell_wai http://hackage.haskell.org/package/http-types0.12.3/docs/src/Network.HTTP.Types.Header.html#RequestHeaders
iabbr <buffer> git_conf [core]repositoryformatversion = 0filemode = truebare = falselogallrefupdates = trueignorecase = trueprecomposeunicode = false[remote "origin"]# public key sshurl = git@bitbucket.org:zsurface/script.git# https does not work for ssh public keyurl = https://zsurface@bitbucket.org/zsurface/tiny3.gitfetch = +refs/heads/*:refs/remotes/origin/*[branch "master"]remote = originmerge = refs/heads/master
iabbr <buffer> shell_script IFS=$'\n' read -d '' -r -a lines < /etc/passwd       # split string to array--printf "line 1: %s\n" "${lines[0]}"printf "line 5: %s\n" "${lines[4]}"# all linesecho "${lines[@]}"
iabbr <buffer> shell_screen When you use screen you need to detach with CTRL+A+D before you exit ssh.--Alternatively, if you want to run a process directly with screen you can use--screen -dmSL [session name] [commands]-d starts a screen session and immediately detaches from it-m forces creating a new screen session-S lets you give the session a name-L turns on logging to ~/screenlog.0example:--screen -dmSL workstuff myscript.shYou can then either:--resume later using screen -x workstuff--or--check the log file less -r ~/screenlog.0--nohup myrun &  // run process in background-- rename screen sessionC-a :sessionname new_name
iabbr <buffer> git_command_all_cmds [Git command git branch show on command prompt]git branch myfeature_branchcreate                   // create new feature branchgit branch -d delete_branchdelete                   // [git delete] feature branchgit branch -D delete_branchdelete                   // unmerged feature branchgit checkout master                                 // switch to master branchgit merge --no-ff feature_branch                    // merge feature_branch to master without fast forwrad.git merge feature_branch                            // merge feature_branch to master with fast forward.git rm -r --cached myfile                           // remove file from your indexgit refloghow                                       // all the reference loggit reset --hard 828c8c0b1995                       // revert to previous commitgit diff --stat hash1  hash2                        // diff two commits from two hashesgit log -- specific_file                            // get log on specific filegit commit --amend -m "new message"                 // change the most recent message, amend your most recent message.git rev-list --all --remotes --pretty=oneline foo   // show all previous commit on foogit rev-list --all foo                              // show all previous commit on foogit stash                                           // save your modified tracked files to a stack and that you can reapply late.git stash list                                      // list all the stashes--------------------------------------------------------------------git rm -r myfile                                    // remove file from local repository AND file system.git rm -r --cached file                       // remove file only from local repository but NOT from file system.--------------------------------------------------------------------git commit -m "msg"git/info/exclude                                   // for local file that does't need to be shared, just add file or dir pattern//// update .gitignore filegit rm --cached <file>                              // remove specific tracked filegit rm --cached -r .                                // remove all tracked filesgit add .                                          // track all filesparse_git_branch(){git branch 2> /dev/null | sed -e 's/* \(.*\)/ \(\1)/'}export PS1="\u$(parse_git_branch)\w$"\u - Username\w - Full path\h - Host nameimg,/Users/cat/try/gitfastforward.png
iabbr <buffer> format_c long n;unsigned long un;printf("%ld", n); // signedprintf("%lu", un); // unsigned
iabbr <buffer> cpp_header /Users/cat/myfile/bitbucket/cpplib
iabbr <buffer> ssh_keygen ssh-keygen             # with hostname inside the keyssh-keygen -C noname   # without hostname inside the key
iabbr <buffer> vm_ubuntu ssh catman@206.189.166.163rsync -avzh dir catman@206.189.166.163:/home/catman/try
iabbr <buffer> haddoc_info https://stackoverflow.com/questions/46895199/no-output-will-be-generated-because-there-is-no-main-module
iabbr <buffer> mysql_database -- start mysql on MacOS/usr/local/bin/mysql.server startmysql -u root
iabbr <buffer> xcode_shortcut Shift ALT Command Left/Right  fold all code/unfolded all codeCTRL 5 search files name(.h *.m)                                // xcode searchCTRL 6 type search show all current methodsCTRL WindowKey left/right  navigate backward/backwardCTRL 1 show recent itemsCTRL C delete current lineALT Left/Right  move word by wordCommand ALT .(dot) change focusCommand ALT Left/Right code fold/unfoldedCommand ALT 0 close utilities windowCommand left/right move cursor to begin/end of line(no highligh)Command , show setting(xcode)Command 3 global searchCommand w close window (not just xcode)Command [] shift hightlight code left/rightCommand double click jump to definitionCommand D mark lineCommand L goto lineCommand E, Command F use selection to findCommand E + Command F search word under cursorCommand Shift O Quick Search fileCommand Shift Y close debug windowCommand Shift 2 Open OrganizerCommand Shift f  global findCommand Shift Left/Right  selection whole lineCommand Shift right/left highlight lineCommand CTRL UP/Down .h and .m fileCommand CTRL J jump to definitionCommand CTRL Left jump backCommand CTRL F full/maximize screen modeCommand CTRL Y debug/continueCommand `  window fit screenCommand /  comment out code
iabbr <buffer> Intellij_shortcut_key // intellij shortcut keyCtrl Shift      // comment block codeShift Tab       // move block of code(code selection) to leftTab/Shift+Tab   // indent/unintented selected lineAlt Right/Left  // cycle through tabsAlt Shift F7    // goto usageAlt Shit f      // global searchAlt g           // find nameAlt UP/Down     // move to previous/next methodCtrl F12        // show all current methodsCtrl W          // select code block. e.g {..}Ctrl-Shift F12  // max/unmaxed windowCtrl UP/Down    // move window up/downCtrl [/]        // Move to code block star/endCtrl Shift #    // book mark line, bookmark lineCtrl #          // goto bookmark #, goto book mark #Ctrl Alt B      // goto implementationCtrl E          // go back to implementationCtrl Alt F7     // shows usagesCtrl w          // select whole wordCtrl Left/Right // move cursor previous/nextCtrl Shift R    // find and replaceCtrl Shift j    // join lineCommand F4      // close tabALT + Command L // reformat code
iabbr <buffer> bookmark https://www.indeed.ca/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=160&vjk=d01cd4f8ed2049c4http://crl.ethz.ch/teaching/shape-modeling-18/   geometry processinghttps://plfa.github.io/Connectives/   Agda bookhttps://www.seas.upenn.edu/~cis194/spring13/lectures.html    Best Haskell Lecturehttp://learnyouahaskell.com/functors-applicative-functors-and-monoidshttps://alexey.radul.name/ideas/2013/introduction-to-automatic-differentiation/    automatic differentiation in Haskellhttp://5outh.blogspot.com/2013/05/symbolic-calculus-in-haskell.html  nice haskell implementation differentiationhttp://www.danielbrice.net/research/https://github.com/friedbrice/AutoDiff/blob/master/README.mdhttps://justindomke.wordpress.com/2009/02/17/automatic-differentiation-the-most-criminally-underused-tool-in-the-potential-machine-learning-toolbox/https://github.com/friedbrice/AutoDiffhttps://ca.indeed.com/jobs?q=software%20developer&l=Vancouver%2C%20BC&start=190&vjk=0996c65a4307dfcbhttps://ca.indeed.com/jobs?q=haskell&l=Vancouver%2C%20BC&vjk=b89c80555ddff9dbhttps://ca.indeed.com/jobs?q=haskell&l=Vancouver%2C%20BC&vjk=da1811c5255cb09bhttps://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=60&vjk=cf6a1197f8820967https://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=150&vjk=87fa4dcd66e9f427https://jobs.lever.co/skyboxlabs/ea330293-4a2b-479c-8fcc-4a8b1202a377https://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=190&vjk=2c52530e2bb82943https://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=190&vjk=4e210f52e43a8075http://www.cs.nott.ac.uk/~pszgmh/pearl.pdfhttps://www.vex.net/~trebla/haskell/index.xhtmlhttps://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&fromage=last&start=10&vjk=513ff4b9ceead549https://stackoverflow.com/questions/41630650/are-join-and-fmap-join-equals-in-haskell-from-category-theory-point-of-viewhttps://kseo.github.io/posts/2016-12-30-write-you-an-interpreter.htmlhttp://www.stephendiehl.com/llvm/       write your a compile in Haskellhttps://www.indeed.ca/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=180&vjk=72bb8ab7962118b6http://colah.github.io/https://github.com/Gabriel439/slides/blob/master/regex/regex.mdhttps://lettier.github.io/posts/2017-02-25-matrix-inverse-purescript.htmlhttps://ca.indeed.com/jobs?q=mathematics&l=Vancouver%2C%20BC&start=20&vjk=99779df35972b7c2
iabbr <buffer> cpp_vector // constructing vectors#include <iostream>#include <vector>int main () {// constructors used in the same order as described above:std::vector<int> first;                                // empty vector of intsstd::vector<int> second (4,100);                       // four ints with value 100std::vector<int> third (second.begin(),second.end());  // iterating through secondstd::vector<int> fourth (third);                       // a copy of third// the iterator constructor can also be used to construct from arrays:int myints[] = {16,2,77,29};std::vector<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );//std::cout << "The contents of fifth are:";for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)std::cout << ' ' << *it;std::cout << '\n';return 0;}
iabbr <buffer> cpp_vector_iterator std::vector<string> results;for ( auto itr = _addresses.begin(), end = _addresses.end(); itr != end; ++itr) {// call the function passed into findMatchingAddresses and see if it matchesif ( func( *itr ) ) {results.push_back( *itr );}}
iabbr <buffer> cmake https://stackoverflow.com/questions/41829852/how-to-create-xcode-project-from-already-existing-git-cmake-project// generate Xcode project from cmakecmake -G Xcode CMakeList.txt// do it inside your projectcmake -H. -Bbuildcmake --build build -- -j3
iabbr <buffer> encry_key *GPG* *gpg* *gpg_key* *gen_key*gpg -d -o password.txt password.txt.gpg // decrypt gpg file to password.txtgpg -c dir.tar.gzgpg -c -o dir.tar.gz dir.tar.gz.gpg     // encrypt your tar filegpg --gen-key                           // generate new keygpg --list-keys                         // list keygpg --delete-keys C3B95227              // delete key uid=C3B95227tar -czf dir.tar.gz dir                 // tar filetar -xzf dir.tar.gz dir                 // untar file
iabbr <buffer> eng_at_in_on Prepositions of Time - at, in, onWe use:at for a PRECISE TIMEin for MONTHS, YEARS, CENTURIES and LONG PERIODSon for DAYS and DATESat PRECISE TIMEin MONTHS, YEARS, CENTURIES and LONG PERIODSon DAYS and DATES//at 3 o'clock	in May	on Sundayat 10.30am	in summer	on Tuesdaysat noon	in the summer	on 6 Marchat dinnertime	in 1990	on 25 Dec. 2010at bedtime	in the 1990s	on Christmas Dayat sunrise	in the next century	on Independence Dayat sunset	in the Ice Age	on my birthdayat the moment	in the past/future	on New Year's Eve
iabbr <buffer> eng_base Base - base caseBases - plural baseBasic - adjestiveBasics - noun (plural)Bases - noun (plural base and basis)
iabbr <buffer> eng_interest interested - are you interested in the job.interesting - it will be very interesting to see what they can come up with.
iabbr <buffer> filee //[ file=kk.html title=""
iabbr <buffer> forr for(int i=0; i<len; i++){}
iabbr <buffer> forr2 for(int i=0; i<len; i++){for(int j=0; j<len; j++){}}
iabbr <buffer> git_command https://ariya.io/2013/09/fast-forward-git-merge
iabbr <buffer> git_gg -------------------------------------------------------------------[create new remote branch]git checkout -b branch1                -> create local branch1git push --set-upstream origin branch1 -> [local branch1] tracking [remote branch1] from [origin]-------------------------------------------------------------------git branch -d my_branch             -> delete [local] my_branchgit push origin --delete my_branch  -> delete [remote] my_branch-------------------------------------------------------------------git status                       -> show statusgit branch -a                    -> list all branchgit commit --amend               -> change your messagegit checkout [mainline] file     -> checkout file from mainlinegit checkout -b new_feature      -> new feature branchgit checkout hash_cmt -- f.x     -> reverse f.x with commit_hashgit add dir/*                    -> add dir to git[dir must contain file]git merge --no-ff feature-branch -> merge No Fast Forwardgit stash                        -> stash your changesgit stash apply                  -> apply top stashgit stash list                   -> list your stashesgit stash apply stash@{2}        -> apply stash@{2}git push -f origin master        -> force to push[replace remote with local]git diff origin master           -> diff changes [file name] and [content]git diff-tree --no-commit-id --name-only -r [your_commit_hash] -> diff [your_commit] with [remote]git diff --name-only             -> diff file names only---------------------------------------------------git clean -n                     -> remove untracked file/dir but NOT DELETE the filegit clean -f                     -> remove untracked file/dir AND DELETE the filegit rm --cached f.x              -> remove tracked file but NOT DELETE itgit push origin --delete branch1        -> Delete remote branch---------------------------------------------------git reflog file                                    -> get log info for filegit diff file                                      -> diff file origingit diff HEAD~1 HEAD -- myfile                     -> diff previous commit and last commitgit diff-tree --no-commit-id --name-only -r HEAD~1 -> list files changed in commitgit diff mainline -- fileName                      -> compare local and remote filegit checkout mainline -- fileName                  -> override local file with remote filegit diff origin/mainline -- fileName               -> diff local and remote file-------------------------------------------------------------------git commit --amend                                 -> add cr url to your commit message-------------------------------------------------------------------Enter -> More-------------------------------------------------------------------Switch to your feature branchgit rebase -i mainline/masterUse pick(p) or squash(s) with nice messagesave and exitAdd final commit message or your CR urlSave and exitSwitch to mainline and brazil-buildgit merge feature to mainlinegit diff commit1 commit2git push to mainline
iabbr <buffer> git_fast_forward https://ariya.io/2013/09/fast-forward-git-merge
iabbr <buffer> git_ignore_file **/*.class# Package Files #*.war*.ear.idea/dictionaries/cat.xml.idea/workspace.xml.idea/vcs.xml.idea/misc.xml
iabbr <buffer> gitignore # Ignore all*--------------------------------------------------------------------------------# Unignore all with extensions!*.*--------------------------------------------------------------------------------# Unignore all dirs!*/--------------------------------------------------------------------------------### Above combination will ignore all files without extension ###--------------------------------------------------------------------------------# Ignore files with extension `.class` & `.sm`*.class*.sm--------------------------------------------------------------------------------# Ignore `bin` dirbin/# or*/bin/*--------------------------------------------------------------------------------# Unignore all `.jar` in `bin` dir!*/bin/*.jar--------------------------------------------------------------------------------# Ignore all `library.jar` in `bin` dir*/bin/library.jar--------------------------------------------------------------------------------# Ignore a file with extensionrelative/path/to/dir/filename.extension--------------------------------------------------------------------------------# Ignore a file without extensionrelative/path/to/dir/anotherfile
iabbr <buffer> google_book_path Reading multiple thread in linkedlist, queue and hashmap./Users/cat/GoogleDrive/Books/thread_concurrent_linkedlist.pdf
iabbr <buffer> gpg_tar_encrypted_decrypted_file gpg -d -o password.txt password.txt.gpg // decrypt gpg file to password.txttar -czf dir.tar.gz dir                 // tar filegpg -c dir.tar.gzgpg -c -o dir.tar.gz dir.tar.gz.gpg     // encrypt your tar filegpg --gen-key                           // generate new keygpg --list-keys                         // list keygpg --delete-keys C3B95227              // delete key uid=C3B95227tar -czf dir.tar.gz dir                 // tar file, tar folder, tar directory, tar directoriestar -xzf dir.tar.gz dir                 // untar file
iabbr <buffer> haskell_package ghc-pkg listghc-pkg find-module  the module belongs to which packages
iabbr <buffer> haskell_regex /Users/cat/myfile/bitbucket/haskell/SearchReplace.hsimport Data.Arrayimport Text.Regex.TDFA-- regex example, search and replace, search replacereplace::String -> (Int, Int) -> String -> Stringreplace s (m, off) r = first ++ r ++ restwherefirst = take m srest = drop (m + off) smain = dolet s = "mydog dog dog cat dog"let r = makeRegex "\\<dog\\>" ::Regexlet ar = matchAllText r spp $ arpp $ map(\x -> snd $ x ! 0 ) arpp $ replace s (2, 3) "KKK"pp $ map(\x -> let tu = x ! 0 in replace s (snd tu) ("<" ++ (fst tu) ++ ">")) ar
iabbr <buffer> html_code <pre class="mycode"><span class="bo">from:sender</span>getLine::IO StringputStrLn::String-> IO()randomRIO::(Random a) => (a, a) -> IO a</pre>
iabbr <buffer> html_title <div class="mytitle">My Title</div><div class="mytext">The App shows how to use simple animation on iPhone.<br>1. Load images to array<br></div><br><div class="cen"><img src="http://localhost/zsurface/image/kkk.png" width="40%" height="40%" /><br><a href="https://github.com/bsdshell/xcode/tree/master/OneRotateBlockApp">Source Code</a></div>
iabbr <buffer> img_3d_draw img,/Users/cat/try/draw13.png
iabbr <buffer> img_draw img,/Users/cat/try/draw1.pngimg,/Users/cat/try/draw10.pngimg,/Users/cat/try/draw11.pngimg,/Users/cat/try/draw12.pngimg,/Users/cat/try/draw13.pngimg,/Users/cat/try/draw14.pngimg,/Users/cat/try/draw15.pngimg,/Users/cat/try/draw16.pngimg,/Users/cat/try/draw17.pngimg,/Users/cat/try/draw18.pngimg,/Users/cat/try/draw19.pngimg,/Users/cat/try/draw2.pngimg,/Users/cat/try/draw20.pngimg,/Users/cat/try/draw21.pngimg,/Users/cat/try/draw22.pngimg,/Users/cat/try/draw23.pngimg,/Users/cat/try/draw3.pngimg,/Users/cat/try/draw4.pngimg,/Users/cat/try/draw5.pngimg,/Users/cat/try/draw6.pngimg,/Users/cat/try/draw7.pngimg,/Users/cat/try/draw8.pngimg,/Users/cat/try/draw9.pngimg,/Users/cat/try/exitpath.pngimg,/Users/cat/try/fold_left_right.pngimg,/Users/cat/try/gitfastforward.pngimg,/Users/cat/try/gitrebase.pngimg,/Users/cat/try/glortho.pngimg,/Users/cat/try/isbstpic.pngimg,/Users/cat/try/javapath.png
iabbr <buffer> iphone_info `[--------------------------------------------------------------------iPhone 4swidth=[320.000000]height=[480.000000]scale =[2.000000]nativeWidth=[640.000000]nativeHeight=[960.000000]scale =[2.000000]--------------------------------------------------------------------iPhone 5 and upwidth=[320.000000]height=[568.000000]scale =[2.000000]nativeWidth=[640.000000]nativeHeight=[1136.000000]scale =[2.000000]--------------------------------------------------------------------iPad 2Switch Backwidth=[768.000000]height=[1024.000000]scale =[1.000000]nativeWidth=[768.000000]nativeHeight=[1024.000000]scale =[1.000000]iPad--------------------------------iPad Airwidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------ipad Air 2Switch Backwidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------iPad Prowidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]iPad Retina--------------------------------width=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------`]
iabbr <buffer> jarr_Integer Integer[] arr = {1, 2, 3, 4};int width = arr.length;
iabbr <buffer> jarr_arr2d_1x3_1_to_n int[][] arr2d = {{ 1,   2,   3},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_arr2d_4x3_1_to_n int[][] arr2d = {{ 1,   2,   3},{ 4,   5,   6},{ 7,   8,  9},{ 10,  11,  12},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_arr2d_4x4_0_or_1_int int[][] arr2d = {{ 0,   0,   0,  1},{ 1,   1,   0,  1},{ 0,   1,   0,  1},{ 0,   1,   1,  0},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;int h = 0, w = 0;
iabbr <buffer> jarr_arr2d_4x4_0_or_1_long long[][] arr2d = {{ 0,   0,   0,  1},{ 1,   1,   0,  1},{ 0,   1,   0,  1},{ 0,   1,   1,  0},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;int h = 0, w = 0;
iabbr <buffer> jarr_arr2d_4x4_1_to_n_int int[][] arr2d = {{ 1,   2,   3,  4},{ 5,   6,   7,  8},{ 9,   10,  11, 12},{ 13,  14,  15, 16},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_arr2d_4x4_1_to_n_long long[][] arr2d = {{ 1,   2,   3,  4},{ 5,   6,   7,  8},{ 9,   10,  11, 12},{ 13,  14,  15, 16},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_arr2d_6x6_0_1_adjacent_matrix_int int[][] arr2d = {{0, 1, 1, 1, 0, 0},{0, 0, 1, 0, 0, 1},{0, 0, 0, 1, 0, 0},{0, 0, 0, 0, 1, 1},{0, 0, 0, 0, 0, 1},{0, 0, 0, 0, 0, 0},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_arr2d_6x6_0_1_adjacent_matrix_long int[][] arr2d = {{0, 1, 1, 1, 0, 0},{0, 0, 1, 0, 0, 1},{0, 0, 0, 1, 0, 0},{0, 0, 0, 0, 1, 1},{0, 0, 0, 0, 0, 1},{0, 0, 0, 0, 0, 0},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;int[][] arr = {{ 0,   0,   1,  0},{ 0,   0,   1,  1},{ 1,   1,   0,  0},{ 0,   1,   0,  0},};
iabbr <buffer> jarr_array1d_int int[] arr = {1, 2, 3, 4};int width = arr.length;
iabbr <buffer> jarr_array1d_long long[] arr = {1, 2, 3, 4};int width = arr.length;
iabbr <buffer> jarr_array2d int[][] arr = {{1, 2, 3, 4},{1, 2, 3, 4},{1, 2, 3, 4},{1, 2, 3, 4},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_array2d_one_4x4 int[][] arr2d = {{1, 1, 1, 1},{1, 1, 1, 1},{1, 1, 1, 1},{1, 1, 1, 1},{1, 1, 1, 1},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_array2d_one_8x8 int[][] arr2d = {{1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 1},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_array2d_zero_4x4 int[][] arr2d = {{0, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_array2d_zero_8x8 int[][] arr2d = {{0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0},};Aron.printArray2D(arr2d);int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_char char[] arr = {'c', 'a', 't', 's'};int width = arr.length;
iabbr <buffer> jarr_char_2d char[][] arr2d = {{'n', 'a', 'c'},{'a', 'b', 'n'},{'k', 'a', 'f'},};int height = arr2d.length;int width = arr2d[0].length;
iabbr <buffer> jarr_int_1d int[] arr = {1, 2, 3, 4};int width = arr.length;
iabbr <buffer> jarr_to_set_Integer Integer[] arr = {1, 2, 3, 4};Set<Integer> set = new HashSet<Integer>(Arrays.asList(arr));for(Integer s: set) Print.p(s);
iabbr <buffer> jarr_to_set_String String[] arr1 = {"Tesla", "autopilot", "fatal incident"};Set<String> sset = new HashSet<>(Arrays.asList(arr1));for(String s: sset) Print.p(s);
iabbr <buffer> java_code /Users/cat/myfile/github/java/TextAreaLineHeightExample/src/Main.java/Users/cat/myfile/github/java/TextAreaSimple/src/Main.java/Users/cat/myfile/github/java/TextAreaFontAlignment/src/Main.javaimg, /Users/cat/myfile/github/image/textarealine.pngimg, /Users/cat/myfile/github/image/textflow_multiple_line.pngimg, /Users/cat/myfile/github/image/textareafont.png
iabbr <buffer> java_doc /**<pre>{@literal"dog"=> False}{@codeString = "cat";}{@link #concat(List<T> list1, List<T> list2)}</pre><img src="{@docRoot}/../image/multiply_long_integer.png" width=300px height=300px>\( f(x) = x^2 + x + 1 \)\[\begin{bmatrix}a & b \\c & d\end{bmatrix}\]*/
iabbr <buffer> jdoc /**<pre>{@literal}{@code}</pre>*/
iabbr <buffer> java_stop_watch StopWatch sw = new StopWatch();sw.start();sw.printTime();
iabbr <buffer> jbst_Binary_Tree BST b1 = new BST();b1.insert(10);b1.insert(5);b1.insert(15);b1.insert(1);b1.insert(7);int level = 0;boolean isLeaf = true;Aron.prettyPrint(b1.root, level, isLeaf);Aron.binImage(b1.root);
iabbr <buffer> jclass_dummy_class class MyClass{public MyClass(){}public void print(){}}
iabbr <buffer> jclassfile_import_classfile import classfile.*;
iabbr <buffer> jda_java_data_structure List<String> list = new ArrayList<String>();List<String> list = new LinkedList<String>();List<String> list = new Stack<String>();List<String> list = new Vector<String>();Queue<String> queue = new ArrayList<String>();Queue<String> queue = new PriorityQueue<String>();Queue<Node> q = new LinkedList(oldQueue);
iabbr <buffer> brew brew location/usr/local/Cellar/fish/2.3.1
iabbr <buffer> fish_shell brew install fishinstall fish shell 2.3.1create symbol linkln -s /usr/local/bin/fish /bin/fishadd /bin/fish to /etc/shells.
iabbr <buffer> java_regex String pattern = "\\.java$";Pattern r = Pattern.compile(pattern);String dir = "/Users/cat/myfile/bitbucket/java";List<String> ldir = Aron.getCurrentFiles(dir);for(String path : ldir){Matcher mat = r.matcher(path);if(mat.find()){Print.p(path);}}
iabbr <buffer> jdate_date //import java.util.Date;Print.pbl(new Date());
iabbr <buffer> jfile_path_base_name String fName = "/Users/cat/myfile/github/java/text/ht/file.txt";Print.pbl(FilenameUtils.getPath(fName));     // [Users/cat/myfile/github/java/text/ht/]Print.pbl(FilenameUtils.getBaseName(fName)); // [file]Print.pbl(FilenameUtils.getName(fName));     // [file.txt]
iabbr <buffer> jgen_generic_type public static <T> void printList(List<T> list) {}
iabbr <buffer> jgetresource ClassLoader cl = ClassLoader.getSystemClassLoader();URL[] urls = ((URLClassLoader) cl).getURLs();for (URL url: urls) {System.out.println(url.getFile());}String classLoaderPath = getClass().getClassLoader().getResource("").getPath();String resourcePath = getClass().getResource("").getPath();Print.pbl("classLoaderPath=" + classLoaderPath);Print.pbl("resourcePath   =" + resourcePath);// /Users/cat/myfile/github/java/GradleResourceExampleimg, /Users/cat/myfile/github/image/getresource.png
iabbr <buffer> jgradle_gradle img, /Users/cat/myfile/github/image/gradle_resources.png
iabbr <buffer> jite_list_to_iterator List<String> list = Arrays.asList("cat1", "dog1", "cow1");List<String> list = ArrayList<String>();Iterator<String> ite = list.iterator();Iterator ite = list.iterator();while(ite.hasNext()){pb(ite.next());}
iabbr <buffer> jite_Iterator List<String> list = ArrayList<String>();Iterator<String> ite = list.iterator();Iterator ite = list.iterator();while(ite.hasNext()){pb(ite.next());}
iabbr <buffer> jlist_2d_Integer List<ArrayList<Integer>> list2d = new ArrayList<ArrayList<Integer>>();
iabbr <buffer> jlist_2d_String List<ArrayList<String>> list2d = new ArrayList<ArrayList<String>>();
iabbr <buffer> jlist_String_simple List<String> ls = new ArrayList<>();
iabbr <buffer> jlist_Integer_simple List<Integer> list = new ArrayList<Integer>();
iabbr <buffer> jlist_Integer_init // immutable listList<Integer> list = Arrays.asList(1, 2, 3);// mutable listList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3));
iabbr <buffer> jlist_String_init // immutable listList<String> list1 = Arrays.asList("cat1", "dog1", "cow1");List<String> list2 = Arrays.asList("cat2", "dog2", "cow2");// mutable listList<String> list3 = new ArrayList<>(Arrays.asList("do the follows", "different from", "exchange for"));List<String> list4 = new ArrayList<>(Arrays.asList("withdraw", "negotiation", "withdrawal-none"));
iabbr <buffer> jlist_copy_list_with_lambda List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3));List<Integer> ll = list.stream().collect(Collectors.toList());Aron.printList(ll);
iabbr <buffer> jlist_list_to_array List<String> list = Arrays.asList("cat", "dog", "cow");String[] arr = list.toArray(new String[list.size()]);String[] array = str.split("\\s+");List<String> list = Arrays.asList(array);img,/Users/cat/try/draw10.png ,/Users/cat/try/draw11.png ,/Users/cat/try/draw12.png ,/Users/cat/try/draw13.png ,/Users/cat/try/draw14.png
iabbr <buffer> jlist_merge_two_lists List<String> list1 = new ArrayList<>(Arrays.asList("cat", "dog"));List<String> list2 = new ArrayList<>(Arrays.asList("Nothing"));List<String> result = Stream.concat(list1.stream(), list2.stream()).distinct().collect(Collectors.toList());
iabbr <buffer> jlist_to_iterator List<Integer> list = Arrays.asList(1, 2, 3);Iterator ite = list.iterator();while(ite.hasNext()){Print.p(ite.next());}//List<String> list = Arrays.asList("cat", "cow", "crab", "camel", "chowchow");Iterator ite = list.iterator();while(ite.hasNext()){Print.p(ite.next());}
iabbr <buffer> jlogger_logger // import java.util.logging.Level;// import java.util.logging.FileHandler;// import java.util.logging.Formatter;// import java.util.logging.Level;// import java.util.logging.Logger;// import java.util.logging.SimpleFormatter;//private final static Logger LOGGER = Logger.getLogger(LoggerExample.class.getName());static private FileHandler fileTxt;static private SimpleFormatter formatterTxt;//fileTxt = new FileHandler("./text/Logging.txt");formatterTxt = new SimpleFormatter();LOGGER.setLevel(Level.INFO);fileTxt.setFormatter(formatterTxt);LOGGER.addHandler(fileTxt);//LOGGER.info("Logging an INFO-level message {0}");
iabbr <buffer> jlogger_simple private final static Logger LOGGER = Aron.logInit(Main.class.getName(), "/Users/cat/myfile/github/java/text/Logging3.txt");LOGGER.info("LOGGER.info logging");
iabbr <buffer> jmain_main_method import java.util.*;import java.io.*;import classfile.*;public class Main{public static void main(String[] args) {}static void test0(){Aron.beg();Aron.end();}}//import java.util.*;import java.io.*;import classfile.*;public class Solution{public static void main(String[] args) {}static void test0(){Aron.beg();Aron.end();}}
iabbr <buffer> jmap_String_to_ListOfString Map<String, List<String>> map = new HashMap<String, List<String>>();for(Map.Entry<String, List<String>> entry : map.entrySet()){System.out.println("[" + entry.getKey());for(String s : entry.getValue()){System.out.println("[" + s + "]");}}
iabbr <buffer> jmap_String_to_SetString Map<String, Set<String>> map = new HashMap<>();Set<String> set1 = new HashSet<>(Arrays.asList("cat", "dog"));Set<String> set2 = new HashSet<>(Arrays.asList("cat", "dog", "cow"));map.put("key1", set1);map.put("key2", set2);for(Map.Entry<String, Set<String>> entry : map.entrySet()){Print.pbl("[" + entry.getKey() + "]->[" + entry.getValue() + "]");Print.line();}
iabbr <buffer> jmap_iterator_Integer Map<Integer, Integer> map = new HashMap<Integer, Integer>();for(Map.Entry<Integer, Integer> entry : map.entrySet()){System.out.println("[" + entry.getKey() + " , " + entry.getValue() + "]");}
iabbr <buffer> jmap_iterator_String Map<String, String> map = new HashMap<String, String>();for(Map.Entry<String, String> entry : map.entrySet()){System.out.println("[" + entry.getKey() + " , " + entry.getValue() + "]");}
iabbr <buffer> jmap_listList_String Map<String, List<List<String>>> map = new HashMap<>();for(Map.Entry<String, List<List<String>>> entry : map.entrySet()){System.out.println("[" + entry.getKey());for(List<String> list : entry.getValue()){for(String s : list)System.out.println("[" + s + "]");}}
iabbr <buffer> jmap_list_Integer_MapEntry Map<String, List<Integer>> map = new HashMap<>();List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2));List<Integer> list2 = new ArrayList<>(Arrays.asList(3, 4));map.put("k1", list1);map.put("k2", list2);for(Map.Entry<String, List<Integer>> entry : map.entrySet()){Print.pbl(entry.getKey());for(Integer s: entry.getValue()){Print.pb(s);}Print.line();}
iabbr <buffer> jmap_list_String_MapEntry Map<String, List<String>> map = new HashMap<>();List<String> list1 = new ArrayList<>(Arrays.asList("v1", "v2"));List<String> list2 = new ArrayList<>(Arrays.asList("vv1", "vv2"));map.put("k1", list1);map.put("k2", list2);for(Map.Entry<String, List<String>> entry : map.entrySet()){Print.pbl(entry.getKey());for(String s: entry.getValue()){Print.pb(s);}Print.line();}
iabbr <buffer> jmap_print_map Map<Integer, Integer> map = new HashMap<Integer, Integer>();for(Map.Entry<Integer, Integer> entry : map.entrySet()){System.out.println("[" + entry.getKey() + " , " + entry.getValue() + "]");}
iabbr <buffer> jmap_simple_String_Integer Map<Integer, Integer> map = new HashMap<Integer, Integer>();Map<Integer, String> map = new HashMap<String, String>();Map<String, String> map = new HashMap<String, String>();Map<String, Integer> map = new HashMap<String, String>();Map<String, List<String>> map = new HashMap<String, List<String>>();Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();Map<String, Set<String>> map = new HashMap<String, Set<String>>();//Map<Integer, Integer> map = new HashMap<>();Map<Integer, String> map = new HashMap<>();Map<String, String> map = new HashMap<>();Map<String, Integer> map = new HashMap<>();Map<String, List<String>> map = new HashMap<>();Map<String, List<Integer>> map = new HashMap<>();Map<String, Set<String>> map = new HashMap<>();
iabbr <buffer> jmap_String_String Map<String, String> map = new HashMap<>();map.put("key1", "url1");map.put("key2", "url2");for(Map.Entry<String, String> entry : map.entrySet()){Print.p("[" + entry.getKey() + "]->[" + entry.getValue() + "]");}
iabbr <buffer> jme public static List<String> get(){}
iabbr <buffer> jqueue_Integer Queue<Integer> queue = new ArrayList<Integer>();
iabbr <buffer> jqueue_String Queue<String> queue = new ArrayList<String>();
iabbr <buffer> jran_random Random ran = new Random();// 0 - 9int num = ran.nextInt(10);Print.p(num);
iabbr <buffer> jregex // example 1String pattern = "([a-z]+).([a-zA-Z-]+)(/)";String[] strArr = {"www.google.com/search?q=goog/nice","www.google.com/search?q=goog","http://www.google.msn.ca/search?q=goog","http://www.google.msn.ca/a/b/c/d"};Pattern r = Pattern.compile(pattern);for(int i=0; i<strArr.length; i++) {Matcher mat = r.matcher(strArr[i]);if(mat.find()) {Print.p("found=" + mat.group(0));}}// example 2Pattern r = Pattern.compile("captheorem", Pattern.CASE_INSENSITIVE);Matcher mat = r.matcher("CAPTHEOREM");if(mat.find()) {Print.p("found=" + mat.group(0));}// example 3Pattern pattern = Pattern.compile("[0-9]{3}[-]?[0-9]{3}[-]?[0-9]{4}");Matcher matcher = pattern.matcher("334-4467777 yes... 334666-4456 415-333-9674 whatever 4264491569");while(matcher.find()) {System.out.println(matcher.group());}// match numberPattern pattern = Pattern.compile("\\d+");Matcher matcher = pattern.matcher("334-4467777 yes... 334666-4456 415-333-9674 whatever 4264491569");while(matcher.find()) {System.out.println(matcher.group());}// match numberPattern pattern = Pattern.compile("[a-zA-Z0-9_]+");Matcher matcher = pattern.matcher("This is case_INSENSITIVE PREPOSITION");while(matcher.find()) {Print.pbl(matcher.group());}// match wordPattern pattern = Pattern.compile("[A-Z]+");Matcher matcher = pattern.matcher("This is case insensitive PREPOSITION");while(matcher.find()) {Print.pbl(matcher.group());}// excluding a,n, and spacePattern pattern = Pattern.compile("[^an ]+");Matcher matcher = pattern.matcher("This is banana");while(matcher.find()) {Print.pbl(matcher.group());}// match lineString str = "  This is case insensitive_PREPOSITION 123 ";Pattern pattern = Pattern.compile("^\\s*[\\w ]+\\s*");Matcher matcher = pattern.matcher(str);while(matcher.find()) {Print.pbl(matcher.group());}
iabbr <buffer> jset_set_to_list Set<String> set1 = new HashSet<String>(Arrays.asList("cat", "dog"));List<String> list = new ArrayList<>(set1);Set<String> set2 = new HashSet<>(list);Set<String> set3 = new HashSet<String>(Arrays.asList(1, 2));Set<Integer> set4 = new HashSet<Integer>(Arrays.asList("cat", "dog"));Aron.printSet(set1);Aron.printSet(set2);Aron.printSet(set3);Aron.printSet(set4);Aron.printList(list);-- java list to set or HashSet, java setList<String> list = new ArrayList<>(Arrays.asList("dog", "cat"));Set<String> set = new HashSet<>(list);-- set to listSet<String> set = new HashSet<>();set.add("dog");List<String> list = new ArrayList<String>(set);
iabbr <buffer> jstack_Integer Stack<Integer> stack = new Stack<Integer>();
iabbr <buffer> jstack_String Stack<String> stack = new Stack<String>();
iabbr <buffer> jthread_AtomicInteger // import java.util.concurrent.atomic.AtomicInteger;AtomicInteger atom = new AtomicInteger(0);atom.incrementAndGet();atom.get()
iabbr <buffer> jthread_simple_thread class MyThread implements Runnable{public int num;public MyThread(int num){this.num = num;}public void run(){for(int i=0; i<10; i++){this.num += 1;Aron.pbl(num);}}}
iabbr <buffer> jthread_thread_id Thread.sleep(2000);  // sleep two secondsThread.currentThread().getId();Print.pbl("Thread_id   =" + Thread.currentThread().getId());Print.pbl("Thread_name =" + Thread.currentThread().getName());//import java.util.Date;Print.pbl("Current time=" + new Date());Thread t = new Thread(new MyClass(queue));t.start();
iabbr <buffer> jthrow_throw_new_IllegalArgumentException throw new IllegalArgumentException("str must be not null.");
iabbr <buffer> jtim_date //import java.util.Date;Print.pbl(new Date());
iabbr <buffer> jtime_Duration // import java.time.LocalDateTime;// import java.time.Duration;LocalDateTime from = LocalDateTime.now();LocalDateTime to = LocalDateTime.now();Duration duration = Duration.between(from, to);Print.pbl(duration.toString());
iabbr <buffer> jtime_LocalDateTime //import java.time.LocalDateTime;LocalDateTime time = LocalDateTime.parse("2007-12-03T10:15:30", DateTimeFormatter.ISO_LOCAL_DATE_TIME);int year = time.getDayOfYear();Month month = time.getMonth();int  day = time.getDayOfMonth();int minute = time.getMinute();int second = time.getSecond();// create different datetime formatDateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy-HH:mm:ss");LocalDateTime time1 = LocalDateTime.parse("30/09/1970-18:34:13", format);
iabbr <buffer> jtime_stop_watch StopWatch sw = new StopWatch();sw.start();sw.printTime();//long ti, tf;ti = System.currentTimeMillis();tf = System.currentTimeMillis();Print.pbl("Finish. Total time:" + (tf - ti));
iabbr <buffer> jtoken_has_more_token StringTokenizer stoken = new StringTokenizer(exp, "+-/*", true);List<String> list = new ArrayList<String>();while(stoken.hasMoreTokens()){String token = stoken.nextToken();list.add(token);System.out.println(token);}
iabbr <buffer> jtry_try_catch try{}catch(IOException e){e.printStackTrace();}
iabbr <buffer> jvector_Integer List<Integer> list = new Vector<Integer>();
iabbr <buffer> jvector_String List<String> list = new Vector<String>();
iabbr <buffer> connected_island count arr2d h w height widthif arr2d[h][w] == 1arr2d[h][w] = 2int n1, n2, n3, n4if h + 1 < heightn1 = count(arr2d, h+1, w, height, width)if h - 1 >= 0n2 = count(arr2d, h-1, w, height, width)if w + 1 < widthn3 = count(arr2d, h, w+1, height, width)if w - 1 >= 0n4 = count(arr2d, h, w-1, height, width)return n1 + n2 + n3 + n4 + 1;return 0
iabbr <buffer> random_array /**[1, 2, 3]len - 0r = 1[1, 3, 2]len - 1r = 0len - 2[3, 1, 2]r = 0[3, 1, 2]*/public static void shuffleArray(int[] arr){int len = arr.length;Random ran = new Random();for(int i=0; i<len - i; i++){int r = ran.nextInt(len - i);int tmp = arr[len - 1 - i];arr[len - 1 - i] = arr[r];arr[r] = tmp;}}
iabbr <buffer> pdf_file img, /Users/cat/myfile/github/math/theorem.pdfvoicemailvoicemailemai
iabbr <buffer> pdf_view /Users/cat/myfile/github/java/PDFBoxViewer
iabbr <buffer> screeninfo -(void)checkDevice{CGSize size = [[UIScreen mainScreen] bounds].size;CGFloat width = size.width;CGFloat height= size.height;CGFloat scale = [UIScreen mainScreen].scale;_screenWidth = width;_screenHeight = height;CGSize nativeSize = [[UIScreen mainScreen] nativeBounds].size;NSLog(@"width=[%f]", size.width);NSLog(@"height=[%f]", size.height);NSLog(@"scale =[%f]", scale);NSLog(@"nativeWidth=[%f]", nativeSize.width);NSLog(@"nativeHeight=[%f]", nativeSize.height);NSLog(@"scale =[%f]", scale);if([[UIDevice currentDevice] userInterfaceIdiom ] == UIUserInterfaceIdiomPhone){NSLog(@"iPhone");}else if([[UIDevice currentDevice] userInterfaceIdiom ] == UIUserInterfaceIdiomPad){NSLog(@"iPad");}else{NSLog(@"ERROR: Unsupported Device");}}--------------------------------------------------------------------iPhone 4swidth=[320.000000]height=[480.000000]scale =[2.000000]nativeWidth=[640.000000]nativeHeight=[960.000000]scale =[2.000000]--------------------------------------------------------------------iPhone 5 and upwidth=[320.000000]height=[568.000000]scale =[2.000000]nativeWidth=[640.000000]nativeHeight=[1136.000000]scale =[2.000000]--------------------------------------------------------------------iPad 2Switch Backwidth=[768.000000]height=[1024.000000]scale =[1.000000]nativeWidth=[768.000000]nativeHeight=[1024.000000]scale =[1.000000]iPad--------------------------------iPad Airwidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------ipad Air 2Switch Backwidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------
iabbr <buffer> regex_pattern POSIX class    similar to    meaning[:upper:]    [A-Z]    uppercase letters[:lower:]    [a-z]    lowercase letters[:alpha:]    [[:upper:][:lower:]]    upper- and lowercase letters[:alnum:]    [[:alpha:][:digit:]]    digits, upper- and lowercase letters[:digit:]    [0-9]    digits[:xdigit:]    [0-9A-Fa-f]    hexadecimal digits[:blank:]    [ \t]    space and TAB characters only[:space:]    [ \t\n\r\f\v]    blank (whitespace) characters[:cntrl:]        control characters[:graph:]    [^ \t\n\r\f\v]    printed characters[:print:]    [^\t\n\r\f\v]    printed characters and space
iabbr <buffer> shell_gcd if [[ "$1" == "d" ]]; thengpg -d -o file.txt file.gpgelif [[ "$1" == "e" ]]; thengpg -c  file.txtfi
iabbr <buffer> shell_num_argu if [[ "$#" -eq 1 ]]; then$b/script/pathx.sh $1elif [[ "$#" -eq 2 ]]; then$b/script/pathx.sh $1 $2fi
iabbr <buffer> shell_grep grep --color -Hnirs class\s\+\w\+\s\+\(extends\s\+[A-Za-z_\]\+\)*\s\+\(implements\s\+\([A-Za-z_\]\+\)\)\?\(\s\+\)*\(,\s\+[A-Za-z_\]\+\)*grep --include="*.java" "\"[a-zA-Z]\+\""  ./grep -l pattern *   [print file name only]grep --include="*.[mh]"  --color -Hnris CATextLayergrep 'wai\|filewatcher'  // grep orgrep -E --color=always -Hnris 'build|String' *.java | more -Rgrep --color=always -Hnris 'build\|String' *.java   | more -Rgrep -E '[[:alnum:]-]+.hs$'       # all haskell file name from a full pathcat ~/.bash_history | grep --color "$1" | sed -E 's/^[0-9]+\s*|\s*$//g' > $tmpFilevim $(grep -l --include="*.java" pattern .)vim $(grep -l --include="*.java" pattern .) #vim open all files from grepvimgrep /MyString/ /home/mycode/**/*.m   // Search MyString pattern recursively[**/*.m] two arsterisks
iabbr <buffer> shell_sort sort -k 2  // sort second columnsort -r -k 2  // sort second column in reverse
iabbr <buffer> shell_regex if [[ "abc_" =~ ^[0-9a-zA-Z-]+$ ]]; then echo "ok"; else echo "bad"; fi  # shell regex, sh regex
iabbr <buffer> iterm2_keybinding autocomplete    # word + Cmd + ;  autocomplete in Terminate
iabbr <buffer> stdin_stdout :cmd 1 > /etc/null   # redirect stdout to /etc/null:cmd 2 > /etc/null   # redirect stderr to /etc/null:cmd 2 > &1          # redirect stderr to stdout:cmd &> file        # redirect stderr and stdout to file
iabbr <buffer> java_static_test static void test0(){Aron.beg();Aron.end();}static void test1(){Aron.beg();Aron.end();}static void test2(){Aron.beg();Aron.end();}
iabbr <buffer> java_class class Person{String firstName;String lastName;int age;public Person(String firstName, String lastName, int age){this.firstName = firstName;this.lastName = lastName;this.age = age;}public String toString(){return "firstName=" + lastName + " lastName=" + lastName + " age=" + age;}}List<Person> ls = of();ls.add(new Person("Michell", 1));ls.add(new Person("David",   0));ls.add(new Person("Michael", 2));
iabbr <buffer> test_only this is cool wileif mywhile while, mykk dogmykk
iabbr <buffer> tmux_key prefix c      => new paneprefix x      => kill paneprefix n      => next windowprefix ?      => help keyprefix h      => switch pane--prefix C => new windowprefix ? => help windowprefix % => split pane--prefix o => change paneprefix x => kill pane--prefix Left/Right  => resize paneprefix   h   select-pane   -Lprefix   i   display-messageprefix   j   select-pane   -Dprefix   k   select-pane   -Uprefix   l   select-pane   -Rprefix   m   select-pane   -m--tmux list-sessions => list all sessionstmux attach-session -t 2 => attach session 2--Run following in MacOS terminal:--------------------------------------inside tmux and escape sequence issueref: http://invisible-island.net/xterm/ctlseqs/ctlseqs.html#h2-Control-Bytes_-Characters_-and-Sequencesref: https://unix.stackexchange.com/questions/409068/tmux-and-control-sequence-character-issue--cabbr mhalf printf '\ePtmux;\e\e[8;100;200t\e\\'cabbr mmax printf '\ePtmux;\e\e[9;1t\e\\'--------------------------------------brew updatebrew install reattach-to-user-namespacebrew upgrade reattach-to-user-namespaceecho "set -g default-command \"reattach-to-user-namespace -l ${SHELL}\"" >> ~/.tmux.conf--Fix the bug, tmux bug, tmux open bug,tmux run UI, get following error:The window server could not be contacted.  open must be run with a user logged in at the console, either as that user or as root
iabbr <buffer> tmux_command :swap-window -s 1 -t 3   #  -s source -t target, swap window id between 1 and 3`:                       # run command in tmux
iabbr <buffer> vim_function :echo matchstrpos("testing", "ing")    => ["ing", 4, 7]:echo matchstrpos('test$ing', '$ing')  => ['$ing', 4, 7]:echo matchstrpos('test\ing', '\ing')  => ['ing', 4, 7]:echo matchstrpos('test\ing', '\\ing') => ['\ing', 4, 7]:echo matchstrpos('test^ing', '^ing')  => ['\ing', 4, 7]:echo matchstrpos('test^ing', '^ing')  => ['', 4, 7]:echo matchstrpos('test^ing', '\^ing') => ['', 4, 7]:echo matchstrpos('testing$', 'ing$')  => ['', 4, 7]:echo matchstrpos('testing$', 'ing\$') => ['ing$', 4, 7]let dumm = matchstrpos('$\mathbb', escape('\math', '$\'))
iabbr <buffer> vim_list_function :fun       => list all function:fun name  => list function name:fun /name => list function contains name
iabbr <buffer> vim_mode Q => visual to exit, the most important mode
iabbr <buffer> vim_tips --------------------------------------------------------------------------------multiple highlightmat[ch] TODO /pattern/2mat[ch] TODO /pattern/3mat[ch] TODO /pattern/--------------------------------------------------------------------------------:record q and letter a, replay @a // vim recording:1/pat                          // search from the first line, cool tip:1?pat                          // search backward, from last line, cool tip:zz                             // move the cursor to middle of screen:zt                             // move the cursor to Top of screen:zb                             // move the cursor to Botton screen:echo v:this_session            // show current vim session file// All in insert modeCTRL-D		command-line completionCTRL-N		command-line completionCTRL-P		command-line completionCTRL-A		command-line completionCTRL-L		command-line completionCTRL-W      delete world before the cursor // insert modeCTRL-U      delete remove all characters between the cursor and the beginning of the line--------------------------------------------------------------------------------s/\%V\_^/\/\//gc  [:h \_^]   // \_^ can be used anywhere in the pattern comment all selected lines, selection match visual mode%s/\[\(\w\+\)\s\+objectAtIndex:\([^]]\+\)\]/\1\[\2\]/gc // replace [array objectAtIndex:index] with array[index] in ObjectiveC//Copy text from vim to clipboard [:h clipboard]"* is special Register that stores selection"*y Copy text from vim to clipboard"*p Paste text from clipboard to vim--------------------------------------------------------------------------------:3dd // delete below 3 line from the current line--------------------------------------------------------------------------------:let x = 0 | g/$/s//\='[' . x . ']'/ | let x = x + 1  // append [x] to the end of line:CTRL-O     //Jump Older cursor position in jump list [:h jumps]:CTRL-I     //Jump newer cursor position in jump list backward:gf         // open the file under the currsor:find will search the directories/files in path option [:h :find]:vim $(find ./ -name \*.java -print)                // open all java file from find:set path? //will show current path option [:h path]:set path +=/home/mycode //You can add different directories to path option in Vim" reset to default:set path& [:h set]:set complete=k/home/myfile/*,k~/home/dir" auto completion search your own file" [:h complete]" k{dict} scan the file {dict}. Servan flags can be given--------------------------------------------------------------------------------In Vim Script, there are many options to handle file names, paths:h cmdline:bufdo e!                                                                                       // Save reload all buffers :syntax on  to enable syntax highlight:bufdo %s/pattern/replacement/ge | update                                                       // vim replace all buffers:zf                // Create fold under cursor:zd                // Delete fold under cursor:5gt               // goto tab 5, [in insert mode]:tabmove 4         // move the current tab to position 4 [help :tabmove]:tabc 11           // close tab 11:set modeline      // display file name in each tabs page:set ls=2:set modeline      // display full file name in statusline [:help :statusline]:set statusline=%F:help debug-mode                             // debug vim script                                                        :echo getline(".")                           // get current line string:echo match("mystring", "st")                // return 2:Align \*\zs\s\+ =                           // *'  ' and '=' separator:\%V                                         // visual selection block [:help \%V]:'<,>'sort /\ze\%V/                          // sort all selected lines in visual mode:'< and >'                                   // first cursor position/last cursor position in registers in visual mode/selection:g/^$/d                                      // delete empty line:let @a=substitue(@a, 'pattern', 'sub', 'g') // substitute 'pattern' with 'sub' in register @a:q: or q/                                    // vim command history:hi Search guifg=Brown guibg=Gray:hi Search cterm=Brown ctermbg=Gray          // change search backgroun/foreground [:h hl-Search]:sort n                                      // sort lines by the first digital in the line, [n] is first decimal number, [x] is first hexadecimal number [:help :sort]:sort! n                                     // reverse sorting:syn match MyKeyWord /MyName/                // Highlight defined keyword [:help syn-match]:highlight MyKeyWord guifg=Green:'<,'>s/^\s*\zs\w/#\0/gc                     // comment all selected lines in python:set ma                                      // set file modifiable on  [:h modifiable]:ctrl-]                                      // class definition:ctrl-t                                      // back to previous window:s/pattern/\=@a.submatch(0).@b/gc            // substitute with two registers:redir @a                                    // redirect ex command to register:ls:redir END:redir @* | ls | redir END                   // redirect all the name of file to reg @* in buffer:tabmove 4                                   // move the current tab to position 4 [help :tabmove]:/pattern\c #ignorecase:hi clear SpellBad # Reset SpellBad bad spell highlight to default:noh                                         // remove hightlight nohlsearch:g/-\s*(\w\+)\w+/z#.1                        // print all xcode methods with line number #:/\(if\)\ze\(then\)                          // if follows then "[if]then":/\(if\)\zs\(then\)                          // then starts with if "if[then]":g*         / search without word boundry:let i=1 | g/dog/s//\='cat' . i/gc | let i=i+1                   //  g/pattern/[cmd] [h :g] [h sub-replace-expression] searchkey: increase:let i=2 | .,$g/dog\d\+/s//\='cat' . i/gc | let i=i+1            //  vim increase number:for i in range(1,10) | if i % 2 == 0 | put = i | endif | endfor //  put even numbers from cursor position vertically:echo synIDattr(synIDtrans(hlID("Normal")), "bg") [:h hlID]      //  get background color:set hidden                                                      //  hidden an unsaved file, ow, file has to be saved with !:.,$s/some\(stuff\)/& . \1/gc                                    //  copy all i think % = \0 here:normal mode:%s/{\zs\(\_.}\@!\)*//gc                                         //  negative operator block of code { hello world }:\@!                                                             //  negative operator, subtract operator, exclusive operator:echo g:colors_name                                              //  get current color scheme:/usr/share/vim/vim73/colors                                     //  vim scheme, vim directory vim folder:autocmd BufEnter *.*  setlocal completefunc=CompleteAbbre       //  file type evil, autocmd will not detect file without extension. e.g. "file"// http://vim.1045645.n5.nabble.com/autocmd-pattern-exclusion-td5712330.html:autocmd BufEnter *\(.txt)\@<!  TTT exec 'echo expand("%:p")'     //  excluding file type// start PERL mode, vim play modegQ
iabbr <buffer> vim_visual vmap pp :s/\%V.*\%V/<strong>\0<\/strong>/ 
iabbr <buffer> vim_command :set spell " enable spelling:set nospell " disable spellingzg      " add word to spellfile, spell file, spell-file, spelling file, spellingfileC-X C-U " e.g. jlist user defined completionC-X C-D " definitionC-X C-K " dictionaryC-X C-X " suggestionC-X C-L " line completionC-X C-N " local keyword completion:fp     " copy current full path:ip     " change to init pathF7 copyF8 past
iabbr <buffer> vim_quickfix --------------------------------------------------------------------------------:vim /subRegex/ **/*.hs  vim search, vim regex:copen      // Open your quickfix:cl         // list your quickfix:cn         // list your quickfix--------------------------------------------------------------------------------
iabbr <buffer> vim_complete :echo &complete -- check path to autocomplete file
iabbr <buffer> vim_regex foo foobar   barfood:s/\v(bar)@<=foo//gc   => foo foobar bard  , remove foo precede with bar:s/\v(bar)@<!foo//gc   => bar barfood      , remove foo NOT precede with bar:s/\vfoo(bar)@=//gc    => foo bar barfood  , remove foo following bar:s/\vfoo(bar)@!//gc    => foobar bard      , remove foo NOT following bar
iabbr <buffer> vim_map :map   :noremap  :unmap     Normal, Visual, Select, Operator-pending:nmap  :nnoremap :nunmap    Normal:vmap  :vnoremap :vunmap    Visual and Select:smap  :snoremap :sunmap    Select:xmap  :xnoremap :xunmap    Visual:omap  :onoremap :ounmap    Operator-pending:map!  :noremap! :unmap!    Insert and Command-line:imap  :inoremap :iunmap    Insert:lmap  :lnoremap :lunmap    Insert, Command-line, Lang-Arg:cmap  :cnoremap :cunmap    Command-lineCOMMANDS				      MODES ~Normal  Visual+Select  Operator-pending ~:map   :noremap   :unmap   :mapclear	 yes	    yes		   yes:nmap  :nnoremap  :nunmap  :nmapclear	 yes	     -		    -:vmap  :vnoremap  :vunmap  :vmapclear	  -	    yes		    -:omap  :onoremap  :ounmap  :omapclear	  -	     -		   yes
iabbr <buffer> string_to_binary /**<pre>{@literalConvert Numbet to Binary String}{@codenumToBinary(4) => "100"}</pre>*/public static String numToBinary(int n){String s = "";if(n == 0)s = "0";while(n > 0){int mod = n % 2;int q = n / 2;s = mod + s;n = q;}return s;}
iabbr <buffer> permutation_code Given a string, print all permutation of their characters, interview questions,/**abcp(abc)a + p(bc)b + p(c)c + p("")c + p(b)b + p("")b + p(ac)a + p(c)c + p("")c + p(a)a + p("")c + p(ab)a + p(b)b + p("")b + p(a)a + p("")*/public static void permutation(String prefix, String s){if(s.length() == 0){Print.pl("perm=" + prefix);}else{for(int i=0; i<s.length(); i++){String ss = "" + s.charAt(i);permutation(prefix + ss, remove(i, s));}}}public static String remove(int n, String s){StringBuffer sb = new StringBuffer(s);sb.deleteCharAt(n);return sb.toString();}
iabbr <buffer> j_language start j or jterm if you like Qt editorJ has its package manager and you can install package in one click--c =. 3 4 NB. c is local variablec =: 3 4 NB. c is global variable--!3 NB. factorial--{. list NB. => Haskell, head list}. list NB. => Haskell, drop 1 list--list=: i.5 NB. create list = 0 1 2 3 4 5-- Haskell equivalentlist = [0..5]--list=: i:5 NB. create list = -5 -4 -3 -2 -1 0 1 2 3 4 5-- Haskell equivalentlist = [-5..5]--l =: i. 3 4  NB. 3x4 matrix from 0 to 3*4-1-- Haskell equivalentl = chunksOf 3 [0.. (3*4)-1]--find the sum of maximum rows in a matrixm =: i. 3 3+/ (>./"1 m) NB => 15-- Haskell equivalentlet m = chunksOf 3 [0..(3*3-1)]sum $ map(\r -> foldr max 0 r)  -- => 15--add two listsc =. 1 2 3d =. 3 4 5c + d4 6 8-- Haskell equivalentzipWith(\c' d' -> c' + d') c d--mean =. +/ % # NB. The mean of a list numbersmean 1 2 3 4   NB. => 2.4let ls = [1, 2, 3, 4] in sum ls / len ls  NB. In Haskell--p. 1 0 1 NB. polynomial p = 1x^0 + 0x^1 + x^2 = 0 => roots: 0j1, 0j_1NB. Haskell does't has build-in function to solve polynomial.--? 3 3 $ 10 NB. generate 3x3 random matrix from [0, 10)-- In Haskellrl 0 = return []; rl n = do r <- randomRIO(0, 10); rs <- rl(n-1); return (r:rs)rm <- mapM(rl) replicate 3 3--(}. list) - (}: list)   NB. Subtraction of adjacent items of listzipWith(\x y -> x - y) (tail list) (init list) -- Haskell--s1 =. i. 3 NB. concat two listss2 =. i. 2s1,s2--0 1         0 1 NB. concate 2d list0 3 ,8 9 => 0 38 9[1] ++ [2] BN. Haskell--NB. command listjconsole -js a=.24 b=.34 "echo a*b" "exit''"j -js a=.24 b=.34 "echo a*b" "exit''"--readFile =. 1!:1writeFile =. 1!:2fn =. < '/tmp/ff.x''1 2 3' writeFile fn
iabbr <buffer> sql_database -- mysql how to create new user+--------------------+create user 'user1'@'localhost' identified by 'password';GRANT select, update, delete ON *.* TO 'user1'@'localhost';--GRANT ALL ON *.* to 'user1'@'localhost';+--------------------+mysql> source /tmp/user.file+--------------------+-- login user1mysql -u user1---------------------show databases;         # show databasesuse testdb;             # use the testdbshow tables;            # show tablesdesc mytable;           # show scheme of mytable
iabbr <buffer> sqlite_database ----------------------------------------------------sqlite3  /Users/cat/myfile/bitbucket/testfile/test.db  => load test.db into sqlite3sqlite3  /Users/cat/myfile/bitbucket/testfile/userinput.db  => user cmd databse (s emacs)sqlite3 test.db "CREATE TABLE test (id INTEGER PRIMARY KEY, str text);INSERT INTO test (str) VALUES ('test string');"----------------------------------------------------# start sqlite on my MacOS.table            => show tables.schema           => show schema.schema  mytable  => show mytable schema, like description in MySql.dump             => dump datbase to file.help             => help command----------------------------------------------------# sqlite3 regex% => wildcard, zero or more_ => single character----------------------------------------------------select * from mytable name LIKE '%david%'  => name of the table field contains 'david'+--------------------+DROP TABLE contacts;CREATE TABLE contacts(contact_id INTEGER PRIMARY KEY,first_name TEXT NOT NULL,last_name TEXT NOT NULL,email TEXT NOT NULL);
iabbr <buffer> tensor_flow --------------------------------------------------------------------------------Sat Mar  2 14:11:57 2019Install Tensor Flow in Python,Yep, it is pain of the ass1. I have old version of Python 2.7.02. Yep, you need to install Python 3.0, /usr/local/bin/python33. You need to set environment variables to point to Python lib4. You need to install other lib such as--------------------------------------------------------------------------------
iabbr <buffer> java_csv Mon Mar  4 18:38:09 2019read csv file with Opencsv jar file, opencsv-4.0.jar, csv file, write to csv file,--/Users/cat/myfile/bitbucket/java/OpenCSVExample/src/Main.java
iabbr <buffer> java_system_property Key Meaning"file.separator"         Character that separates components of a file path. This is "/" on UNIX and "\" on Windows."java.class.path"        Path used to find directories and JAR archives containing class files. Elements of the class path are separated by a platform-specific character specified in the path.separator property."java.home"              Installation directory for Java Runtime Environment (JRE)"java.vendor"            JRE vendor name"java.vendor.url"        JRE vendor URL"java.version"           JRE version number"line.separator"         Sequence used by operating system to separate lines in text files"os.arch"                Operating system architecture"os.name"                Operating system name"os.version"             Operating system version"path.separator"         Path separator character used in java.class.path"user.dir"               User working directory"user.home"              User home directory"user.name"              User account name
iabbr <buffer> java_path // None of these methods requires that the file corresponding// to the Path exists.// Microsoft Windows syntaxPath path = Paths.get("C:\\home\\joe\\foo");//// Solaris syntaxPath path = Paths.get("/home/joe/foo");//System.out.format("toString: %s%n", path.toString());System.out.format("getFileName: %s%n", path.getFileName());System.out.format("getName(0): %s%n", path.getName(0));System.out.format("getNameCount: %d%n", path.getNameCount());System.out.format("subpath(0,2): %s%n", path.subpath(0,2));System.out.format("getParent: %s%n", path.getParent());System.out.format("getRoot: %s%n", path.getRoot());
iabbr <buffer> haskell_profile Sat Mar  2 21:42:02 2019runhaskell with profile, haskell profile, haskell profiling--./Main +RTS -p
iabbr <buffer> haskell_install There are four versions of Haskell in my MacOS7.10.2, 7.10.3, 8.4.3, 8.6.3/usr/local/bin/activate-hs -l  -- list all versionsuse 8.4.3 currently, there are compile error with ghc 8.6.3 for AronLib.hs
iabbr <buffer> redis_setup Wed Apr 10 14:31:04 2019--Reddis is memcache key-value datastoreReddis run as server and you can make the connection.do operations such as:set key valueget key--------------------------------------------------------------------------------In Haskell, how easy to use Reddis?1. It is easy to make the connection:)2. It is easy to set single key-value in the datastore3. It Reddis API is hard to understand if you dont read any examples.4. You need to use module Aeson to serialize/deserialize JSON5. It is painful to convert Stirng to ByteString, Text, Strict and Lazy:OThere are 16 functions that you need to use:O--In Java, I did not try the API yet, but I think it would be easier to use than Haskell.You can use Reddis on Spring with following Bean instance
iabbr <buffer> phone_num If you require assistance and would prefer to contact us by phone,please call the Morgan Stanley Customer Service Center at Toll Free Number (Within the U.S) +1 866 722 7310 8:00 a.m. - 8:00 p.m. EST Non-U.S.Participants +1 801 617 7435 8:00 a.m. - 8:00 p.m. EST or Toll Free Number (Within the U.S) +1 866 722 7310 8:00 a.m. - 8:00 p.m. EST Non-U.S. Participants +1 801 617 7435 8:00 a.m. - 8:00 p.m. EST
iabbr <buffer> cabal cabal list --simple
iabbr <buffer> java_comparable Test File: /Users/cat/myfile/bitbucket/java/CompareDemo.java
iabbr <buffer> irc_channel /msg nickserv sendpass elliptic0   // send newpass to your email/msg NickServ IDENTIFY elliptic0 0000 // identity your self
iabbr <buffer> graphivz dot -Tsvg pretty.gv > /tmp/pretty.svgdot -Tsvg pretty.gv > /tmp/p.svg && chrome.sh /tmp/p.svgchrome.sh /tmp/pretty.svg
iabbr <buffer> xzdsza `cpp_doxygen /Users/cat/myfile/bitbucket/publicfile/doxygen.css
cabbr <buffer> wai :tabnew $HOME/myfile/bitbucket/haskell_webapp/wai.hs
let &cpo=s:cpo_save
unlet s:cpo_save
setlocal keymap=
setlocal noarabic
setlocal autoindent
setlocal backupcopy=
setlocal balloonexpr=
setlocal nobinary
setlocal nobreakindent
setlocal breakindentopt=
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),0],:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
setlocal colorcolumn=
setlocal comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-
setlocal commentstring=/*%s*/
setlocal complete=k$HOME/myfile/bitbucket/javalib/*.java,k$HOME/myfile/bitbucket/jsource/java/util/*.java,k$HOME/myfile/bitbucket/jsource/java/lang/*.java,k$HOME/myfile/bitbucket/jsource/java/math/*.java
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=CompleteJavaLib
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal cursorlineopt=both
setlocal define=
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=
setlocal expandtab
if &filetype != 'java'
setlocal filetype=java
endif
setlocal fixendofline
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=0
setlocal foldmarker={{{,}}}
set foldmethod=marker
setlocal foldmethod=marker
setlocal foldminlines=1
setlocal foldnestmax=20
set foldtext=MyFoldText()
setlocal foldtext=MyFoldText()
setlocal formatexpr=
setlocal formatoptions=tcq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal formatprg=
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=-1
setlocal include=
setlocal includeexpr=
setlocal indentexpr=
setlocal indentkeys=0{,0},0),0],:,0#,!^F,o,O,e
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255,<,>
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal lispwords=
setlocal nolist
setlocal makeencoding=
setlocal makeprg=
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=bin,octal,hex
setlocal nonumber
setlocal numberwidth=4
setlocal omnifunc=
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal scrolloff=-1
setlocal shiftwidth=4
setlocal noshortname
setlocal showbreak=
setlocal sidescrolloff=-1
setlocal signcolumn=auto
setlocal smartindent
setlocal softtabstop=0
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=~/myfile/bitbucket/vim/myword.utf-8.add
setlocal spelllang=en
setlocal statusline=
setlocal suffixesadd=
setlocal noswapfile
setlocal synmaxcol=3000
if &syntax != 'java'
setlocal syntax=java
endif
setlocal tabstop=4
setlocal tagcase=
setlocal tagfunc=
setlocal tags=
setlocal termwinkey=
setlocal termwinscroll=10000
setlocal termwinsize=
setlocal textwidth=1200
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal varsofttabstop=
setlocal vartabstop=
setlocal wincolor=
setlocal nowinfixheight
setlocal nowinfixwidth
setlocal wrap
setlocal wrapmargin=0
let s:l = 21 - ((20 * winheight(0) + 27) / 55)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
21
normal! 05|
tabnext 1
badd +0 ~/myfile/bitbucket/java/EvaluateTree.java
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOS
set winminheight=1 winminwidth=1
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
