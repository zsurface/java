import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.function.BiFunction;
 

/*
    Tue Nov 20 09:45:31 2018 
-------------------------------------------------------------------------------- 
    Double linkedlist for generic type in Java
    append()
    addFront()
    remove()
        head node
        tail node
        middle node
    toList()
    print()
-------------------------------------------------------------------------------- 
*/ 
class MyNode<T>{
    MyNode<T> left;
    MyNode<T> right;
    T data;
    public MyNode(T data){
        this.data = data;
    }
}

class LinkedList<T>{
    public MyNode<T> head;    
    public MyNode<T> tail;
    public LinkedList(){
    }
    public void append(MyNode node){
        if(tail == null){
            head = tail = node;
        }else{
            tail.right = node;
            node.left = tail;
            tail = node;
        }    
    }
    public void addFront(MyNode node){
        if(head == null){
            head = tail = node;
        }else{
            MyNode tmp = head;
            head = node; 
            head.right = tmp;
            tmp.left = head;
        }
    }
    public void remove(MyNode r){
        if(head != null){
            // remove first node
            if(head == r){
                MyNode tmp = head.right;
                head = head.right;
                if(head == null) // only one node
                    tail = head;
                // more than one node
            }else{
                if(tail == r){ // last node, two or more nodes
                    tail = tail.left;
                    tail.right = null;
                }else{
                    // There are nodes between r 
                    MyNode left = r.left;
                    MyNode right = r.right;
                    left.right = right;
                    right.left = left;
                }
            }
        }
    }
    public void print(){
        MyNode tmp = head;
        while(tmp != null){
            Print.p(tmp.data);
            tmp = tmp.right; 
        }
    }        
    public ArrayList<T> toList(){
        ArrayList<T> list = new ArrayList<T>(); 
        MyNode<T> tmp = head;
        while(tmp != null){
            list.add(tmp.data);
            tmp = tmp.right; 
        }
        return list;
    }
}


public class try_link{
    public static void main(String[] args) {
        test0();
        test1();
        test11();
        test00();
        test01();
        test02();
        test03();
        test04();
        test05();
    }

    public static void test05(){
        List floats = new ArrayList();    
        floats.add(Float.valueOf(0.2f));
        fillNum(floats);
    }
    public static void fillNum(List list){
        Number n = Integer.MAX_VALUE;
        list.add(n);
        list.add(3);
    }

    public static void test04(){
        LinkedList<Integer> ll = new LinkedList();
        ArrayList<Integer> ls = Aron.geneListInteger(0, 3);
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        MyNode n0 = new MyNode(0);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        ll.addFront(n0);
        ll.print();
        Test.t(ls, ll.toList());
        Print.fl("test04"); 
        ll.print();
    }
    public static void test03(){
        Print.fl(); 
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        Print.fl("test03"); 
        ll.remove(n3);
        ll.print();
    }
    public static void test02(){
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        Print.fl("test02"); 
        ll.remove(n2);
        ll.print();
    }
    public static void test01(){
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        Print.fl(); 
        ll.remove(n1);
        Print.fl(); 
        ll.print();
    }
    public static void test00(){
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
    }
    public static void test0(){
        Aron.beg();
        {
            MyNode<Integer> r = new MyNode<Integer>(1);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            insert(r, 3, f);
            insert(r, 2, f);
            insert(r, 5, f);
            inorder(r);

            Print.p(isBST(r, f));
        }
        {
            MyNode<Integer> r = new MyNode<Integer>(8);
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            insert(r, 3, f);
            insert(r, 9, f);
            insert(r, 2, f);
            inorder(r);

            Print.p(isBST(r, f));
        }

        Aron.end();
    }
    public static <T> List<T> filter(Function<T, Boolean> f, List<T> list){
        List<T> ls = new ArrayList<T>();
        for(T t : list){
            if(f.apply(t)){
                ls.add(t);
            }
        }
        return ls;
    }
    public static void test11(){
        Function<Integer, Boolean> f = x -> x % 2 == 0;     
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 7)); 
        List<Integer> l = filter(f, list);
        Print.p(l);
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    public static <T> void inorder(MyNode<T> r){
        if(r != null){
            inorder(r.left);
            Print.p(r.data);
            inorder(r.right);
        }
    }
    public static <T> void insert(MyNode<T> r, T t, BiFunction<T, T, Boolean> f){
        if(r == null){
            r = new MyNode<T>(t); 
        }else{
            if(f.apply(t, r.data)){
                if(r.left == null)
                    r.left = new MyNode<T>(t);
                else
                    insert(r.left, t, f);
            }else{
                if(r.right == null)
                    r.right = new MyNode<T>(t);
                else
                    insert(r.right, t, f);
            }
        }
    }
    public static <T> MyNode leftSub(MyNode<T> r){
        if (r.right != null){
           return leftSub(r.right); 
        }else{
            return r;
        }
    }
    public static <T> MyNode rightSub(MyNode<T> r){
        if(r.left != null){
            return rightSub(r.left);
        }else{
            return r;
        }
    }
    /*
        1. null is BST
        2. left subtree is BST
        3. right subtree is BST
        4. max(left substree) < parent.data
        5. min(right subtree) > parent.data
    */
    public static <T> Boolean isBST(MyNode<T> r, BiFunction<T, T, Boolean> f){
        if (r != null){
            if (!isBST(r.left, f))
                return false;
            
            if (r.left != null){
                MyNode<T> lmax = leftSub(r.left);
                if(!f.apply(lmax.data, r.data))
                    return false; 
            }
            if (r.right != null){
                MyNode<T> rmin = rightSub(r.right);
                if(!f.apply(r.data, rmin.data))
                    return false; 
            }

            if (!isBST(r.right, f)) 
                return false;
        }
        return true;
    }

} 

