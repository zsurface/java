import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import classfile.Tuple;
import classfile.Triple;
import classfile.Rational;
import classfile.PNode;
import classfile.Point2;
import classfile.Polynomial;
import classfile.Node;
import classfile.BST;
import classfile.StopWatch;

var s2 = list(0, 1, 0, 1, 0);
ls.subList(0, 2);
ls.subList(1, 2);
ls.subList(1, 3);
ls.stream().reduce(1, (x, y) -> x + y);
ls.subList(1, 4+1);
ls.subList(1, 3+1);
map (x -> x == '\'', ls);
map (x -> x == '\'' ? 1 : 0, ls);
for(int i = 0; i < len(ll); i++){
    int c = 0;
    if(ll.get(i) == 1){
       c++;
    }
    if(0 < c && c <= 2){
       ll.set(i, 1);
    }
}

for(int i = 0; i < len(ll); i++){
    
    if(ll.get(i) == 1){
       c++;
    }
    if(0 < c && c <= 2){
       ll.set(i, 1);
    }
}

for(int i = 0; i < len(ll); i++){
    
    if(ll.get(i) == 1){
       c++;
    }
    if(0 < c && c < 2){
       ll.set(i, 1);
    }
}
int c = 0;

for(int i = 0; i < len(ll); i++){
    
    if(ll.get(i) == 1){
       c++;
    }
    if(0 < c && c < 2){
       ll.set(i, 1);
    }else if(c == 2){
       c = 0;
    }
}
var ls = list(1, 2);
var lt = new ArrayList<>(ls);
dropWhile(x -> x == 0, list(0, 0, 1, 0, 1));
rev(list(0, 1));
var ll = map (x -> x == '\'' ? 1 : 0, ls);
s = list(0, 1, 0, 1, 0, 1, 0, 0);
var s1 = rev(s);
s2 = dropWhile(x -> x == 0, s1);
s2 = init(dropWhile(x -> x == 0, s1));
s2 = tail(dropWhile(x -> x == 0, s1));
s2 = rev(tail(dropWhile(x -> x == 0, s1)));
dropWhile(x -> x == 0, rev(s));
var n = len (dropWhile(x -> x == 0, rev(s)));
len(s) - 6;
dropEnd(3, s);
concat(dropEnd(3, s), repeat(3, 0));
span(x -> x == 'e', "abec");
span(x -> x != 'e', "abec");
splitWhileStr(x -> x != 'e', "abec");
splitIf(x -> x == "a", list("b", "a", "c"));
splitIf(x -> x == "a", list("b", "a", "c", "k"));
splitIf(x -> x != "a", list("b", "a", "c", "k"));
countWhile(x -> x == 'a', list('b', 'a', 'a'));
countWhile(x -> x != 'a', list('b', 'a', 'a'));
for(int i = 0; i < len(ll); i++){
    
    if(ll.get(i) == 1){
       c++;
    }
    if(0 < c && c <= 2){
       ll.set(i, 1);
    }
}
for(int i = 0; i < len(ll); i++){
    
    if(ll.get(i) == 1){
       c++;
    }
    if(0 < c && c < 2){
       ll.set(i, 1);
    }
}
for(int i = 0; i < len(ll); i++){
    
    if(ll.get(i) == 1){
       c++;
    }
    if(0 < c && c < 2){
       ll.set(i, 1);
    }else if(c == 2){
       c = 0;
    }
}
var abc = "abc";
sum (list(1, 2, 3));
fun44("a");
void fun33(Character c, String s, String accStr){
    if(c != '\"'){
        accStr += charToStr(c);
    }
}
s.push(1);
s.pop();
fun44("ab \"ee\"");
fun44("ab\"ee\"");
fun44("aa\"ee\"kk");
void fun44(String s){
    Stack<Character> stack = new Stack<>();
    Stack<Character> st = new Stack<>();
    for(int i = 0; i < len(s); i++){
        if(len(stack) == 0 && s.charAt(i) == '\"'){
            stack.push(s.charAt(i));
        }else if(len(stack) == 1 && s.charAt(i) == '\"'){
            String str = "";
            while(!st.isEmpty()){
                Character c = st.pop();
                str += charToStr(c);
            }
            pl("str=" + str);
            st.clear();
        }else if(len(stack) == 1){
           st.push(s.charAt(i)); 
        }
    }

}
s.push(3);
s.push(4);
s.clear();

/*
public class InxStr{
    int inx;
    String str;
    public InxStr(int inx, String str){
        this.inx = inx;
        this.str = str;
    }

    @Override
    public String toString(){
        String ret = "inx=" + inx + " str=" + str;
        return ret;
    }
}
*/

/**

    (0) -> (1) [a-z] ->  (2)
        "            " 
*/
InxStr fun(int inx, String s){
    String ret = null;
    InxStr two = null;
    int state = 0;
    String str = "";
    boolean done = false;
    int i = inx;
    while(i < len(s) && !done){
        if(state == 0 && s.charAt(i) == '\"'){
            state = 1;
        }else if(state == 1 && s.charAt(i) != '\"'){
            str += charToStr(s.charAt(i));
        }else if(state == 1 && s.charAt(i) == '\"'){
            state = 2;
            done = true;
        }
        i++;
    }
    if(state == 2){
        ret = str;
        two = new InxStr(i, str);
    }
    return two;
}


/**

    (0) -> (1) [a-z] ->  (2)
        "            " 
*/
/*
InxStr string(int inx, String s){
    InxStr two = null;
    int state = 0;
    String str = "";
    boolean done = false;
    int i = inx;
    while(i < len(s) && !done){
        if(state == 0 && s.charAt(i) == '"'){
            state = 1;
            str += charToStr(s.charAt(i));
        }else if(state == 1 && s.charAt(i) != '"'){
            str += charToStr(s.charAt(i));
        }else if(state == 1 && s.charAt(i) == '"'){
            state = 2;
            str += charToStr(s.charAt(i));
            done = true;
        }
        i++;
    }
    if(state == 2){
        two = new InxStr(i, str);
    }
    return two;
}
InxStr string2(int inx, String s){
    InxStr two = null;
    int state = 0;
    String str = "";
    boolean done = false;
    int i = inx;
    while(i < len(s) && !done){
        if(state == 0 && s.charAt(i) == '\''){
            state = 1;
            str += charToStr(s.charAt(i));
        }else if(state == 1 && s.charAt(i) != '\''){
            str += charToStr(s.charAt(i));
        }else if(state == 1 && s.charAt(i) == '\''){
            state = 2;
            str += charToStr(s.charAt(i));
            done = true;
        }
        i++;
    }
    if(state == 2){
        two = new InxStr(i, str);
    }
    return two;
}
*/

/**
    ab12 ✓  
    12ab x

    (0) ->       (1)     ->        (2)
       [a-z]  [a-z0-9]  ^[a-z0-9]  
*/
/*
InxStr alphabetDigit(int inx, String s){
    InxStr two = null;
    int state = 0;
    String str = "";
    boolean done = false;
    int i = inx;
    while(i <= len(s) && !done){
        if(i == len(s)){
            state = 2;
            done = true;
        }else{
            if(state == 0 && isLetter(s.charAt(i))){
                state = 1;
                str += s.charAt(i);
            }else if(state == 1 && (isLetter(s.charAt(i)) || isDigit(s.charAt(i))) ){
                str += charToStr(s.charAt(i));
            }else if(state == 1 && !( isLetter(s.charAt(i)) || isDigit(s.charAt(i)))){
                state = 2;
                done = true;
            }
        }
        i++;
    }
    if(state == 2){
        two = new InxStr(i, str);
    }
    return two;
}

boolean isPositiveDigit(char c){
    return '1' <= c && c <= '9';
}
*/

/**
    0123 Yes
    01   Yes
*/
InxStr digitStr(int inx, String s){

}

/**
    (0) -> (3)  ->      (2)
         0      ' '
                endOFLINE

    (0) -> (1)   ->     (2)
        ↑         ↑ 
       [1-9]    ^[0-9]

    0 yes
    123 Yes
    0123 No 
*/
/*
InxStr integer(int inx, String s){
    InxStr two = null;
    int state = 0;
    String str = "";
    boolean done = false;
    int i = inx;
    while(i <= len(s) && !done){
        if(state == 1 && i == len(s)){
            state = 2;
            done = true;
        }else if(state == 3 && i == len(s)){
            state = 2;
            done = true;
        }else{
            char c = s.charAt(i);
            if(state == 0 && c == '0'){
                str += charToStr(c); 
                state = 3;
            }else if(state == 3 && c == ' '){
                state = 2;
                done = true;
            }else if(state == 3 && c != ' '){
                // Error number format
                state = 0;
                done = true;
            }else if(state == 0 && isPositiveDigit(c)){
                state = 1;
                str += charToStr(c); 
            }else if(state == 1 && isDigit(c)){
                state = 1;
                str += charToStr(c); 
            }else if(state == 1 && !isDigit(c)){
                state = 2;
                done = true;
            }
        }
        i++;
    }
    if(state == 2){
        two = new InxStr(i, str);
    }
    return two;
}
*/

/**
    0.01 Yes
    0.13 Yes
    3.14 Yes
    0.001 Yes
    123.0 Yes
    0.0   No
    .123  No
    0.    No
*/
/*
InxStr float(int inx, String s){
    InxStr two = null;
    return two;
}
*/

/*
String s = "123  ab22 'ee'\"bef\"zz";
// String s = "'ee'zz";
int i = 0;
while(i < len(s)){
    char c = s.charAt(i);
    if(c == '"'){
        InxStr s1 = string(i, s);
        pl("s1=" + s1);
        if(s1 != null){
            i = s1.inx;
        }
    }else if(c == '\''){
        InxStr s2 = string2(i, s);
        pl("s2=" + s2);
        if(s2 != null){
            i = s2.inx;
        }
    }else if(isLetter(c)){
        InxStr s3 = alphabetDigit(i, s);
        pl("s3=" + s3);
        if(s3 != null){
            i = s3.inx;
        }
    }else{
        i++;
    }
}
*/

InxStr s = string(0, "\"abc\"kk");

InxStr s1 = integer(0, "01");
pl(">> s1=" + s1);

InxStr s1 = integer(0, "1");
pl(">> s1=" + s1);

InxStr s1 = integer(0, "123");
pl(">> s1=" + s1);

InxStr s1 = integer(0, "123ab");
pl(">> s1=" + s1);

InxStr s1 = integer(0, "1023ab");
pl(">> s1=" + s1);
InxStr s1 = integer(0, "0");
pl(">> s1=" + s1);

InxStr s1 = integer(0, "0 ");
pl(">> s1=" + s1);
