import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_excelcolumn{
    public static void main(String[] args) {
        test0();
        test1();
    }
    /**
       0 -> A   0 % 3 = 0
       1 -> B   1 % 3 = 1
       2 -> C   2 % 3 = 2
       3 -> 10  3 % 3 = 10

          A   3^1
          B 
          C
         AA   3^2
         AB 
         AC
         BA 
         BB 
         BC
         CA 
         CB 
         CC
        AAA  3^3
        AAB
        AAC
        ABA
        ABB
        ABC
        ACA
        ACB
        ACC
        BAA 
        ...
        CCC

        n % 3 = 0, 1, 2, 0, 1, 2
        n / 3         0  0  1  1  1  1

        (n - 1) /  3        0  0  0  1
        ((n - 1) % 3) + 1 = 1, 2, 3, 1, 2, 3

        1 % 3 = 1     (1 - 1) % 3  = 0    (1 - 1) % 3 + 1 = 1         (1-1)/3 = 0
        2 % 3 = 2     (2 - 1) % 3  = 1    (2 - 1) % 3 + 1 = 2         (2-1)/3 = 0
        3 % 3 = 0     (3 - 1) % 3  = 2    (3 - 1) % 3 + 1 = 3         (3-1)/3 = 0
        4 % 3 = 1     (4 - 1) % 3  = 0    (4 - 1) % 3 + 1 = 1         (4-1)/3 = 1
        5 % 3 = 2     (5 - 1) % 3  = 1    (5 - 1) % 3 + 1 = 2         (5-1)/3 = 1
        6 % 3 = 3     (6 - 1) % 3  = 2    (6 - 1) % 3 + 1 = 3         (6-1)/3 = 1

        0 -> 1 -> A
        1 -> 2 -> B
        2 -> 3 -> C

        1 -> A
        2 -> B
        3 -> C

        4 -> AA
        5 -> AB
        6 -> AC

     */
    public static String excelColumn(int n){
        int base = 3;
        String s = "";
        if(n == 1){
            s = "Z";
        }
        else{
            while(n > 0){
                int q = (n - 1) / base;

                // n - 1 => shift from 1 to 0 since n starts from 1
                int r = ((n - 1) % base) + 1;  // 1 -> A 
                s = intToAlphabetUpper(r - 1) + s;
                n = q;
            }
        }
        return s;
    }
    public static void test0(){
        beg();
        {
            int n = 1;
            String s = excelColumn(n);
            t(s, "A");
        }
        {
            int n = 2;
            String s = excelColumn(n);
            t(s, "B");
        }
        {
            int n = 3;
            String s = excelColumn(n);
            t(s, "C");
        }
        {
            int n = 4;
            String s = excelColumn(n);
            t(s, "AA");
        }
        {
            int n = 6;
            String s = excelColumn(n);
            t(s, "AC");
        }
        {
            int n = 7;
            String s = excelColumn(n);
            t(s, "BA");
        }

        {
            List<String> ls = new ArrayList<>(); 
            for(int i=0; i<28; i++){
                String s = excelColumn(i + 1);
                ls.add(s);
                pl(s);
            } 

            Collections.sort(ls); 
            fl("sort ls");
            pl(ls);
        }

        

        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

