import java.io.*;
import java.lang.String;
import java.util.*;
import static classfile.Aron.*;  
import static classfile.Print.*;  

class Person{
    String name;
    Integer age;
    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
    }
}



public class fold{
    public static void main(String[] args) {
        List<Integer> ls = new ArrayList<>(); 
        ls.add(1);
        ls.add(2);
        ls.add(3);
        Integer n = average(ls);
        pp(n);

        test1();
        test2();
        test3();
        test4();
        test5();
        test6();

        String str = "god";
        String s1 = sort(str);
        p(s1);

        test0();
        test00();
        test000();
        test0000();
    }
    public static void test0000(){
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("ab", "abc");
        map.put("abe", "abce");
        for(Map.Entry<String, String> entry : map.entrySet()){
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        String str = "01234";
        for(int i=0; i<str.length(); i++){
            p(str.substring(0, i+1));
        }

        List<String> list1 = new ArrayList<>();
        list1.add("dog");
        list1.add("cat");
        String[] arr = list1.toArray(new String[0]);
        p(arr);

    }
    public static void test000(){
        Map<String, String> map = new HashMap<>();
        map.put("dog", "dog ->");
        map.put("cat", "cat ->");

        for(Map.Entry<String, String> entry : map.entrySet()){
            System.out.print(entry.getKey());
            System.out.print(entry.getValue());
        }
    }
    public static void test00(){
        String str = "Continua";
        String str1 = sortStr(str);
        p(str1);
    }
    public static void test0(){
        // str => char[] => sort => new String()
        String str = "Continue";
        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        String str1 = new String(chars);
        p(str1);

    }
    public static void test1(){
        List<Integer> ls = Arrays.asList(1, 2, 2, 2, 3);
        List<Integer> nls = unique(ls);
        pp(nls);
    }

    public static void test2(){
        List<Integer> ls = Arrays.asList(1, 2, 2, 3, 2);
        Map<Integer, List<Integer>> map = repeatingCount(ls);
        pp(map);
    }

    public static void test3(){
        List<Integer> ls = Arrays.asList(1, 2, 3, 4);
        List<Integer> lss = Arrays.asList(reverse(ls));
        pp(lss);
    }

    public static void test4(){
        Integer[] arr = new Integer[10];
        arr[0] = 1;
        p(arr);
    }

    public static void test5(){
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.add(1);
        queue.add(0);
        queue.add(2);
        while(queue.size() > 0){
            Integer n = queue.remove();
            p(n);
        }
    }

    public static void test6(){
        PriorityQueue<Person> queue = new PriorityQueue<>((x, y) -> x.age - y.age);
        queue.add(new Person("David", 10));
        queue.add(new Person("Michael", 3));
        queue.add(new Person("Paut", 4));
        while(queue.size() > 0){
            Person per = queue.remove();
            p(per.name);
            p(per.age);
        }
    }


    public static int average(List<Integer> ls){
        Integer s = 0;
        if(ls != null){
            for(Integer n : ls){
                s += n;
            }
            return s/ls.size();
        }
        return 0;
    }

    /**
     * 1, 2, 2, 3
     *  1 -> [1]
     *  2 -> [2]
     *  2 -> [2, 2]
     *  3 -> [3]
     */
    public static Map<Integer, List<Integer>> repeatingCount(List<Integer> ls){
        Map<Integer, List<Integer>> map = new HashMap<>();
        if(ls != null){
            for(Integer n : ls){
                List<Integer> kls = map.get(n);
                if(kls == null){
                    kls = new ArrayList<>();
                }
                kls.add(n);
                map.put(n, kls);
            }
        }
        return map;
    }

    /**
     * find all unique integers from a list
     *
     */
    public static List<Integer> unique(List<Integer> ls){
        Set<Integer> set = new HashSet<>();    
        for(Integer n : ls){
            set.add(n);
        }
        // set => list
        List<Integer> nls = new ArrayList<>(set); 
        // list => set
        // Set<Integer> nset = new HashSet<>(ls);
        return nls;
    }
    public static Integer[] reverse(List<Integer> ls){
        Integer[] arr = new Integer[ls.size()];     
        int k = 0;
        for(Integer n : ls){
            arr[k] = ls.get(k);
            k++;
        }
        
        Integer len = arr.length;
        for(int i=0; i<len/2; i++){
            Integer tmp = arr[i];
            arr[i] = arr[len - 1 - i];
            arr[len - 1 - i] = tmp;
        }
        return arr;
    }

    public static String sort(String s){
        List<String> chls = new ArrayList<>(); 
        for(int i=0; i<s.length(); i++){
            String str = s.charAt(i) + "";
            chls.add(str);
        }
        Collections.sort(chls);
        String sum = "";
        for(String str : chls){
            sum += str;
        }
        return sum;
    }

    // ["dog", "god", "cat", "cow"]
    public static Map<String, List<String>> anagram(List<String> list){
        Map<String, List<String>> map = new HashMap<>();
        if(list != null){ 
            for(String key : list){
                String sortedStr = sort(key); 
                List<String> value = map.get(sortedStr);
                List<String> ls = new ArrayList<>();
                if(value == null){
                    // no key, add key, value
                    ls.add(key);
                    map.put(sortedStr, ls);
                }else{
                    // found key, add sortedStr to ls
                    value.add(sortedStr);    
                }
            }
        }
        return map;
    }

}
