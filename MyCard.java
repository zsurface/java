import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Suit{
	Integer index;
	String suit;
	public Suit(Integer index, String suit){
		this.index = index;
		this.suit = suit;
	}
	@Override
	public String toString(){
		String s = " index => " + index.toString();
		s += " suit =>" + suit;
		return s;
	}
}

interface ICard{
}

interface IDeck{
	public void shuffle();
	public List<Card> getAll();
}

class Card implements ICard{
	public Integer num;    // 1 => 13
	public Suit suit;
	public Card(Integer num, Suit suit){
		this.num = num;
		this.suit = suit;
	}
	public int getNum(){
		return num;
	}
	public Suit getType(){
		return suit;
	}
	
	@Override
	public String toString(){
		String s = "" + num.toString();
		s += " " + suit.toString();
		return s;
	}
}


class Deck implements IDeck{
	List<Card> ls = new ArrayList<>();
	
	public Deck(){
		for(int i = 1; i <= 13; i++){
			ls.add(new Card(i, new Suit(0, "Diamonds")));
		}
		for(int i = 1; i <= 13; i++){
			ls.add(new Card(i, new Suit(1, "Clubs")));
		}
		for(int i = 1; i <= 13; i++){
			ls.add(new Card(i, new Suit(2, "Hearts")));
		}
		for(int i = 1; i <= 13; i++){
			ls.add(new Card(i, new Suit(3, "Spades")));
		}
	}
	@Override
	public List<Card> getAll(){
	    Collections.sort(ls, (x, y) -> x.num - y.num);
		Collections.sort(ls, (x, y) -> x.suit.index - y.suit.index);
		return ls;
	}

	/**
	              x
	        1 2 3 4
			    x
			4 2 3 1
			  x
			4 3 2
			4 3 2 1
			
	 */
	@Override
	public void shuffle(){
		Integer len = ls.size();
		pl("len=" + len.toString());
		Random r = new Random();
		Integer inxLen = len - 1;
		
		for(int i = 0; i < inxLen - 1; i++){
			pl("inxLen - i => " + (inxLen - i));
			Integer inx = r.nextInt(inxLen - i);
			pl("inx => " + inx);

			Card tmp = ls.get(inx);
			ls.set(inx, ls.get(inxLen - i));
			ls.set(inxLen - i, tmp);
		}
    }

}

public class MyCard{
    public static void main(String[] args) {
        test0();
		test1();
    }
    public static void test0(){
		beg();
	    {
			Deck deck = new Deck();
			for(Card card : deck.ls){
				pl(card.toString());
			}
		}
		end();
    }
	public static void test1(){
		beg();
		{
			Deck deck = new Deck();
			for(Card card : deck.ls){
				pl(card.toString());
			}
			
            fl("Card shuffle");
			deck.shuffle();
			for(Card card : deck.ls){
				pl(card.toString());
			}
			
		}
		end();
    }
} 

