import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_function{
    public static void main(String[] args) {
        test0();
        
        // immutable list
        List<Integer> list = Arrays.asList(1, 2, 3);
        Function<Integer, Boolean> f = x -> x % 2 == 0;
        List<Integer> ls = filter(f, list);

        BiFunction<Integer, Integer, Boolean> ff = (x, y) -> x < y;

        Print.p(ls);

        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
    public static List<Integer> filter(Function<Integer, Boolean> f, List<Integer> list){
        List<Integer> ls = new ArrayList<>();
        for(Integer n : list){
            if(f.apply(n)){
                ls.add(n);
            }
        }
        return ls;
    }
} 

