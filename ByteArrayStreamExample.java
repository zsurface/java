import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.Aron;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

public class ByteArrayStreamExample{
    public static void main(String[] args) {
        test0();
        test1();
        fl("fileTime");
        String fname = "/tmp/f.x";
        long milli_sec = fileModTimeMillis(fname);
        fl("fileModTimeMillis");
        pp("milli_sec=" + milli_sec);
        long sec = fileModTimeSec(fname);
        fl("fileModTimeSec");
        pp("sec=" + sec);
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        geneRandomFiles("/tmp", "file", "txt");
        Aron.end();
    }

    private static void printFileTime(FileTime fileTime) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
        System.out.println(dateFormat.format(fileTime.toMillis()));
    }
    /**
      Returns the string representation of this FileTime. The string is returned in the ISO 8601 format:
      YYYY-MM-DDThh:mm:ss[.s+]Z
      where "[.s+]" represents a dot followed by one of more digits for the decimal fraction of a second. It is only present when the decimal fraction of a second is not zero. For example, 
      FileTime.fromMillis(1234567890000L).toString() yields "2009-02-13T23:31:30Z", and FileTime.fromMillis(1234567890123L).toString() yields "2009-02-13T23:31:30.123Z". 
    */
    public static String toTimeFromMillis(long epochMillis){
        return FileTime.fromMillis(epochMillis).toString();
    }

    public static String epochToString(long epochMillis){
        return FileTime.fromMillis(epochMillis).toString();
    }

    public static long fileModTimeSec(String fname){
        return fileModTimeMillis(fname)/1000;
    }

    public static long fileModTimeMillis(String fname){
        long sec = 0;
        Path path = Paths.get(fname);
        FileTime fileTime;
        try {
            fileTime = Files.getLastModifiedTime(path);
            sec = fileTime.toMillis();
        } catch (IOException e) {
            System.err.println("Cannot get the last modified time - " + e);
        }
        return sec;
    }
    public static void test2(){
    }
    public static void test1(){
        Aron.beg();
        StopWatch sw = new StopWatch();

//        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
//        int count = 30000000;
//        List<String> list = Aron.geneRandomStrList(5, 10, count);
        String fw = "/tmp/d.x";
        //Aron.writeFile(fw, list);

        String fw1 = "/tmp/d1.x";
//        sw.start();
//        copy(fw, fw1);
//        sw.print("copy");

        {
            Path path = Paths.get("/", "foo", "bar", "baz.txt");
            Print.p(path.toString());
        }
        {
            String str = Aron.randomStrInt(10);
            Print.p("str=" + str);
        }

        Aron.end();
    }
    
    /**
    <pre>
    {@literal
        copy files, use Files.copy(Paths.get(source), Paths.get(destination)), relace_existing file
    }
    {@code
    String = "cat";
    }
    </pre>
    */ 
    public static void copy(String source, String destination){
        try{
        Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
        }catch(IOException io){
            io.printStackTrace();
        }
    }
    
    /**
    <pre>
    {@literal
        generate many files in a dir
        Only work for MacOS since the path "/tmp/file_0.txt" 
    }
    {@code
    }
    </pre>
    */ 
    public static void geneRandomFiles(String dir, String prefix, String extension){
        int minLen = 4, maxLen = 5;
        int count = 4;
        List<String> list = Aron.geneRandomStrList(minLen, maxLen, count);    
        int c = 0;
        for(String name: list ){
            String suffix = "_" + Aron.intToStr(c) + "." + extension;
            String fname = dir + "/" + (prefix + suffix);
            Aron.writeFile(fname);
            c++;
        }
    }
} 

