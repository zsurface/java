import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.function.BiFunction;
 

/*
    Tue Nov 20 09:45:31 2018 
-------------------------------------------------------------------------------- 
    Double linkedlist for generic type in Java
    append()
    addFront()
    remove()
        head node
        tail node
        middle node
    toList()
    print()
-------------------------------------------------------------------------------- 
*/ 
class MyNode<T>{
    MyNode<T> left;
    MyNode<T> right;
    T data;
    public MyNode(T data){
        this.data = data;
    }
}

class LinkedList<T>{
    public MyNode<T> head;    
    public MyNode<T> tail;
    public LinkedList(){
    }
    public void append(MyNode node){
        if(tail == null){
            head = tail = node;
        }else{
            tail.right = node;
            node.left = tail;
            tail = node;
        }    
    }
    public void addFront(MyNode node){
        if(head == null){
            head = tail = node;
        }else{
            MyNode tmp = head;
            head = node; 
            head.right = tmp;
            tmp.left = head;
        }
    }
    public void remove(MyNode r){
        if(head != null){
            // remove first node
            if(head == r){
                MyNode tmp = head.right;
                head = head.right;
                if(head == null) // only one node
                    tail = head;
                // more than one node
            }else{
                if(tail == r){ // last node, two or more nodes
                    tail = tail.left;
                    tail.right = null;
                }else{
                    // There are nodes between r 
                    MyNode left = r.left;
                    MyNode right = r.right;
                    left.right = right;
                    right.left = left;
                }
            }
        }
    }
    public void print(){
        MyNode tmp = head;
        while(tmp != null){
            Print.p(tmp.data);
            tmp = tmp.right; 
        }
    }        
    public ArrayList<T> toList(){
        ArrayList<T> list = new ArrayList<T>(); 
        MyNode<T> tmp = head;
        while(tmp != null){
            list.add(tmp.data);
            tmp = tmp.right; 
        }
        return list;
    }
}

public class DoubleLinkedListGeneric{
    public static void main(String[] args) {
        test1();
        test00();
        test01();
        test02();
        test03();
        test04();
    }

    public static void test04(){
        Aron.beg();
        LinkedList<Integer> ll = new LinkedList();
        ArrayList<Integer> ls = Aron.geneListInteger(0, 3);
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        MyNode n0 = new MyNode(0);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        ll.addFront(n0);
        ll.print();
        Test.t(ls, ll.toList());
        Print.fl("test04"); 
        ll.print();
        Aron.end();
    }
    public static void test03(){
        Aron.beg();
        Print.fl(); 
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        Print.fl("test03"); 
        ll.remove(n3);
        ll.print();
        Aron.end();
    }
    public static void test02(){
        Aron.beg();
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        Print.fl("test02"); 
        ll.remove(n2);
        ll.print();
        Aron.end();
    }
    public static void test01(){
        Aron.beg();
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        Print.fl(); 
        ll.remove(n1);
        Print.fl(); 
        ll.print();
        Aron.end();
    }
    public static void test00(){
        Aron.beg();
        LinkedList<Integer> ll = new LinkedList();
        MyNode n1 = new MyNode(1);
        MyNode n2 = new MyNode(2);
        MyNode n3 = new MyNode(3);
        ll.append(n1);
        ll.append(n2);
        ll.append(n3);
        ll.print();
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
} 

