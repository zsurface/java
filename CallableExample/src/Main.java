import java.util.concurrent.*;
import java.util.*;
import java.util.stream.Collectors;

import static classfile.Aron.*;
import static classfile.Print.*;
import classfile.StopWatch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.collections4.ListUtils;
/*
   1. Implement Callable interface
   2. Override call() whatever is inside
   3. Add callables object to a list
   4. Invoke all the callable objects with invokeAll(List<Future<>>)
*/

class ReadFileCalled implements Callable<Long>{
    Long count = 0L;
    String dirName;
    private Log log = LogFactory.getLog(ReadFileCalled.class);
    public ReadFileCalled(Long count, String dirName){
        this.count = count;
        this.dirName = dirName;
    }
    public Long call(){
        // 1. Read file
        // 2. Process the file
        // 3. Output strings to list
        // return the list of string
        String newline = System.getProperty("line.separator").toString();
        List<String> listFileName = new ArrayList<String>();
        listFile(dirName, listFileName);

        List<String> listStr = listFileName.stream().map(s -> s + newline).collect(Collectors.toList());
        String fname = "/tmp/aaa_a" + longToStr(count) + ".x";
        p(fname);
        writeFileAppend(fname, listStr);

        p("count=" + count + " name=" + fname);
        return count;
    }
}

/**
    similar to Runnable interface
    class MyRunnable implements Runnable{
        public MyRunnable(String name){
        }
        public void run(){
        }
    }

    Thread t = new Thread(new MyRunnable("name"));
    t.run();
*/
class WordCountFile implements Callable<Map<String, Integer>>{
    String fname;
    private Log log = LogFactory.getLog(WordCountFile.class);
    public WordCountFile(String fname){
        this.fname = fname;
    }
    public Map<String, Integer> call(){
        return wordCount(fname);
    }
}


class MyCallable implements Callable<Long>{
    Long id = 0L;
    public MyCallable(Long val){
        this.id = val;
    }
    /*
    It is similar to Run() in Runnable interface => start() method
    Thread t = new Thread(new Runnable())
    t.start()
    Override call()
    */
    public Long call(){
        int a=4, b = 0;
        try{
            p("id=" + id);
            Thread.sleep(1000);
        }catch(InterruptedException e){
        }
        return id;
    }
}

class ProcessFile{
    final String dir;
    List<String> lsFile = of();
    List<List<String>> lists = of();
    ExecutorService service = Executors.newFixedThreadPool(10);
    public ProcessFile(String dir){
        this.dir = dir;
        listFile(dir, lsFile);
        lists = ListUtils.partition(lsFile, 20);
    }
    public void process() throws ExecutionException, InterruptedException {
        for(List<String> lst : lists) {
            // Consumer: x -> void
            // BiConsumer: (x, y) -> void
            // Function f = x -> x + 1
            // BiFunciton f = (x, y) -> x + y;
            for(String s : lst){
                Future<Map<String, Integer>> f2 = service.submit(() -> wordCount(s));
                if(f2.isDone()){
                    p(f2.get());
                }
            }
        }
        service.shutdown();
    }
}

public class Main{
    public static void main(String args[]){
        // final String path = "/Users/cat/myfile/bitbucket/java/";
        final String path = "/tmp";
        StopWatch sw = new StopWatch();
        sw.start();

        System.out.println("Create callable Demo");
        List<String> lsFile = of(); 
        
        listFile(path, lsFile);
        p(lsFile);

        // Partition List<String> into List<List<String>>
        List<List<String>> lists = ListUtils.partition(lsFile, 20);
        ExecutorService service = Executors.newFixedThreadPool(10);
        p(lists);

        sleep(100);
        Long count = 0L;
        for(List<String> lst : lists) {

            List<WordCountFile> futureList = of();
            for (int i = 0; i < lst.size(); i++) {
                WordCountFile wcf = new WordCountFile(lst.get(i));
                futureList.add(wcf);
            }

            List<Map<String, Integer>> fl = of();
            for(String s : lst){
                service.submit(new Callable<Map<String, Integer>>() {
                    @Override
                    public Map<String, Integer> call() throws Exception {
                        return wordCount(s);
                    }
                });
            }

            List<Map<String, Integer>> myfutureList = of();
            for(String s : lst){
                service.submit(() -> wordCount(s));
            }

            p("Start");
            try{
                List<Future<Map<String, Integer>>> futures = service.invokeAll(futureList);
                for(Future<Map<String, Integer>> future : futures){
                    try{
                        // All the tasks are done
                        if(future.isDone()){
                            p("future.isDone = " + future.isDone());
                            p(future.get());
                        }
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        service.shutdown();

        sw.printTime();
    }
}
