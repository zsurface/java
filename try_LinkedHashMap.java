import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// KEY: LinkedHashMap example
public class try_LinkedHashMap{
    public static void main(String[] args) {
        test0();
        test1();
    }
    
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            List<String> ls = list("a", "b", "a", "c", "b");
            LinkedHashMap map = listStrToMapStrInt(ls);
            p(map);
        }
        {
            Map<String, String> linkedHashMap = new LinkedHashMap<>();
            linkedHashMap.put("k1", "v1"); 
            linkedHashMap.put("k2", "v2"); 
            for(Map.Entry<String, String> entry : linkedHashMap.entrySet()){
                pb("key=" + entry.getKey() + " value=" + entry.getValue());
            }
        }
        fl();
        {
            Map<String, String> hashMap = new HashMap();

            hashMap.put("k1", "v1"); 
            hashMap.put("k2", "v2"); 

            for(Map.Entry<String, String> entry : hashMap.entrySet()){
                pb("key=" + entry.getKey() + " value=" + entry.getValue());
            }
        }

        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

