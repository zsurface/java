import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.AronDev.*;
import static classfile.Token.*;

import java.util.stream.*;
import java.util.stream.Collectors;


public class findString{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg();
        {
            fl("findStringX 1");
            String s = "a";
            List<NameType> ls = tokenize(s);
            findStringX(ls);
        }
        {
            fl("findStringX 2");
            String s = "abc 123";
            List<NameType> ls = tokenize(s);
            findStringX(ls);
        }
        {
            fl("findStringX 2");
            String s = "abc '123 () xxx '- 'def' '445";
            List<NameType> ls = tokenize(s);
            findStringX(ls);
        }
        {
            fl("fillBetween 0");
            List<Integer> lt = list();
            List<Integer> ls = fillBetween(lt);
            var expectedls = list();
            pl(ls);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 01");
            var lt = list(0, 0);
            var ls = fillBetween(lt);
            var expectedls = list(0, 0);
            pl(ls);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 02");
            var lt = list(0);
            var ls = fillBetween(lt);
            var expectedls = list(0);
            pl(ls);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 03");
            var lt = list(1);
            var ls = fillBetween(lt);
            var expectedls = list(0);
            pl(ls);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 04");
            var lt = list(1, 1);
            var ls = fillBetween(lt);
            var expectedls = list(1, 1);
            pl(ls);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 1");
            var lt = list(0, 1, 0, 1, 0);
            var ls = fillBetween(lt);
            var expectedls = list(0, 1, 1, 1, 0);
            pl(ls);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 12");
            var lt = list(0, 1, 0, 1, 1);
            var ls = fillBetween(lt);
            var expectedls = list(0, 1, 1, 1, 0);
            pl(ls);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 2");
            var lt = list(0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1);
            var ls = fillBetween(lt);
            var expectedls = list(0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0);
            t(ls, expectedls);
        }
        {
            fl("fillBetween 3");
            var lt = list(0, 1, 0);
            var ls = fillBetween(lt);
            var expectedls = list(0, 0, 0);
            t(ls, expectedls);
        }
        {
            fl("flipLast1 1");
            var lt = list(0, 1, 0);
            var ls = flipLast1(lt);
            var expectedls = list(0, 0, 0);
            t(ls, expectedls);
        }
        {
            fl("flipLast1 2");
            var lt = list(0, 1, 1);
            var ls = flipLast1(lt);
            var expectedls = list(0, 1, 1);
            t(ls, expectedls);
        }
        {
            fl("flipLast1 3");
            var lt = list(1, 0);
            var ls = flipLast1(lt);
            var expectedls = list(0, 0);
            t(ls, expectedls);
        }
        {
            fl("flipLast1 4");
            var lt = list(0, 1, 1, 0, 1);
            var ls = flipLast1(lt);
            var expectedls = list(0, 1, 1, 0, 0);
            t(ls, expectedls);
        }
        end();
    }
    
    /**
        If there is ODD # of 1, flip the last 1 to 0, otherwise do nothing

        0 1 0 → 0 0 0
          ↑ 
          + flip 1 → 0

        0 1 1 0 → 0 1 1 0
             
            + Do nothing ∵ even # of 1  

        0 1 0 0 1 0 
                ↑ 
                + flip 1 → 0 
    */
    /*
    public static List<Integer> flipLast1(List<Integer> ls){
        List<Integer> ret = new ArrayList<>(ls);
        if(sum(ls) % 2 == 1){
            var s1 = dropWhile(x -> x != 1, rev(ls));
            var diff = len(ls) - len(s1) + 1;
            ret = concat(dropEnd(diff, ls), repeat(diff, 0));
        }
        return ret;
    }
    */
    
    /**
        KEY: fill 1s between 1 and 1
        1. If there is ODD # of 1, flip the last 1 to 0, otherwise do nothing
        2. Fill all 1 between 1 and 1

        0 0 0 1 0 1 0 0 1 0 0 1 0
              ↑   ↑     ↑     ↑          
              1 1 1     1 1 1 1

        0 0 0 1 0 1 0 0 1 0 0 1 1 
              ↑   ↑     ↑     ↑          
              1 1 1     1 1 1 1 0
    */
    /* 
    public static List<Integer> fillBetween(List<Integer> ls){
        List<Integer> rls = new ArrayList<>(ls);
        List<Integer> sls = flipLast1(rls);
        int c = 0;
        for(int i = 0; i < len(sls); i++){
            if(sls.get(i) == 1){
                c++;
            }
            if(0 < c && c < 2){
                sls.set(i, 1);
            }else if(c == 2){
                c = 0;
            }
        }
        return sls;
    }
    */

    public static void findStringX(List<NameType> ls){
       for(NameType nt : ls){
            pl(nt.toString());
       }

        int count = 0;
        int bx = -1;
        int ex = -1;
        List<NameType> nls = new ArrayList<>();
        boolean added = false;

        var bits = map(x -> x.type == Token.SingleQuote ? 1 : 0, ls);
        pl(bits);
        int i = 0;
        while(i < len(ls)){
            NameType nt = ls.get(i);

            List<NameType> lstr = new ArrayList<>();
            // First Single Quote
            if(nt.s.equals("'")){
                lstr.add(nt);
                // nls.add(nt);
                int j = i + 1;
                boolean done = false;
                while(j < len(ls) && !done){
                    NameType nx = ls.get(j);
                    if(!nx.s.equals("'")){
                        lstr.add(nx);
                    }else{
                        lstr.add(nx);
                        // Second Single Quote 
                        pl("lstr =>");
                        pl(lstr);
                        done = true;
                    }
                    j++;
                }
                // Found a string
                if(done){
                    var m = map (x -> x.s, lstr);
                    var s1 = m.stream().reduce("", (x, y) -> x + y);
                    nls.add(new NameType(s1, Unknown));
                    pl("nls=>");
                    pl(nls);
                    pl("Found a string =>");
                    pl(s1);
                }
                i = j;
            }else{
                nls.add(nt);
            }
            i++;
        }
    }
} 

