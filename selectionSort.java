import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class selectionSort{
    public static void main(String[] args) {
        test0();
        //test1();
    }
    public static void test0(){
        Aron.beg();
        {
            List<Integer> ls = Arrays.asList();
            List<Integer> sortedList = selectionSort(ls);
            List<Integer> exp = Arrays.asList();
            Test.t(sortedList, exp); 
        }
        {
            List<Integer> ls = Arrays.asList(1);
            List<Integer> sortedList = selectionSort(ls);
            List<Integer> exp = Arrays.asList(1);
            Test.t(sortedList, exp); 
        }
        {
            List<Integer> ls = Arrays.asList(2, 1);
            List<Integer> sortedList = selectionSort(ls);
            List<Integer> exp = Arrays.asList(1, 2);
            Test.t(sortedList, exp); 
        }
        {
            List<Integer> ls = Arrays.asList(1,3,2);
            List<Integer> sortedList = selectionSort(ls);
            List<Integer> exp = Arrays.asList(1, 2, 3);
            Test.t(sortedList, exp); 
        }

        Aron.end();
    }

    /**
    <pre>
    {@literal
        s1 = s1 + s2
    }
    {@code
    }
    </pre>
    */ 
    public static void concatList2(List<String> s1, List<String> s2){
        if(s1 != null && s2 != null){
            for(String s : s2){
                s1.add(s);
            }
        }
    }
    public static void test1(){
        Aron.beg();
//        {
//            // 2^10 = 2^4 * 2^4 * 2^2 = 16 * 16 * 4 = 256*4 = (250 + 6)*4 = 1000 + 24 = 1024
//            // 2^20 = 1024*1024 = (1000 + 24) = 1000000 + 24*24 + 48*1000
//            // 2^30 = 1024*1024*1024 = (1000 + 24)^3 = 1000 000 000 
//            // 4 byte + 40x1000000
//            int min = 1;
//            int max = 4;
//            int count = 1000000;
//            List<String> list1 = Aron.geneRandomStrList(min, max, count);
//            List<String> list2 = Aron.geneRandomStrList(min, max, count);
//            StopWatch sw = new StopWatch();
//
//            sw.start();
//            concatList2(list1, list2);
//            sw.printMillionSeconds();
//        }
        {
            int min = 1;
            int max = 4;
            int count = 100;
            List<String> list1 = Aron.geneRandomStrList(min, max, count);
            List<String> list2 = Aron.geneRandomStrList(min, max, count);
            StopWatch sw = new StopWatch();

            sw.start();
            List<String> ls = Aron.concatList(list1, list2);
            sw.printMillionSeconds();
        }
        Aron.end();
    }
    
    
} 

