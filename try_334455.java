import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import static classfile.Aron.*; 
import static classfile.Print.*; 
import static classfile.Test.*; 
import classfile.Node; 
import classfile.Tuple; 


interface ITest<T>{
    public T fun(T t1, T t2);
}

class MyTest implements ITest<Integer>{
    public MyTest(){}
    public Integer fun(Integer t1, Integer t2){
        return t1 + t2;
    }
}

class MyTest2 implements ITest<String>{
    public MyTest2(){}
    public String fun(String t1, String t2){
        return t1;
    }
}


/**

   a & b & jjjj
   aa & bb & kk   a  & b & jjjj
   aa & bb & kk
   
 */
public class try_334455{
    public static void main(String[] args) {
        test0();
    }
    public static void test0(){
        beg(); 
        {
            ITest t = new MyTest();
            pp(t.fun(3, 2));
        }
        end();

    }
} 

