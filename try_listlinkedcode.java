import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Node<T>{
    Node prev;
    Node next;
    T data;
    public Node(T data){
        this.data = data;
    }
}

// KEY: double linkedlist append() and remove()
class DoubleLinkedList<T>{
    Node head;
    Node tail;
    public DoubleLinkedList(){
    }
    public void append(T data){
        if (head == null){
            head = tail = new Node(data);
        }else{
            // add a node to the end
            Node prev = tail;
            tail.next = new Node(data);
            tail = tail.next;
            tail.prev = prev;
        }
    }
    public List<T> toList(){
        List<T> ls = new ArrayList<>(); 
        Node curr = head;
        while(curr != null){
            ls.add((T)curr.data);
            curr = curr.next;
        }
        return ls;
    }
    public void print(){
        Node curr = head;
        while(curr != null){
            p(curr.data);      
            curr = curr.next;
        }
    }


                  


    /**
      * Add node to a sorted list
      * 1. list is null            [x]
      * 2. node in head pos       [x] <-> [] <-> []
      * 3. node in tail pos       [] <-> [] <-> [x]
      * 4. node between two nodes   [] <-> [x] <-> [] 
      *
      */
    public void remove(Node node){
    }
    public void remove(Node node){
        if(head != null && node != null){
            Node curr = head;
            while(curr != null){
                if(curr.data == node.data){
                    break;
                }
                curr = curr.next;
            }
            if(curr != null){
                // node is head node. 
                if(curr == head){
                    // only one node.
                    if(curr.next == null){
                        head = tail = null;
                    }else{
                        // more than one node.
                        head = curr.next; 
                        curr.next = null;
                        head.prev = null;
                    }
                }else{
                    if(curr.next == null){
                        // node is last node.
                        Node last = tail;  
                        tail = tail.prev;
                        tail.next = null;
                        last.prev = null;
                    }else{
                        // middle node
                        Node prev = curr.prev;
                        Node next = curr.next;
                        prev.next = next; 
                        next.prev = prev;
                        curr.next = null;
                        curr.prev = null;
                    }
                }
            }else{
                // node is not found. 
            }
             
        }
    }
}


public class try_listlinkedcode{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
        test8();
    }
    public static void test0(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);

        List<Integer> list = Arrays.asList(1);
        Test.t(ddl.toList(), list);

        end();
    }

    public static void test1(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.append(2);

        List<Integer> list = Arrays.asList(1, 2);
        Test.t(ddl.toList(), list);

        end();
    }

    public static void test2(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.append(2);
        ddl.remove(new Node(1));

        List<Integer> list = Arrays.asList(2);
        Test.t(ddl.toList(), list);

        end();
    }
    public static void test3(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.append(2);
        ddl.remove(new Node(2));

        List<Integer> list = Arrays.asList(1);
        Test.t(ddl.toList(), list);

        end();
    }
    public static void test4(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.remove(new Node(1));

        List<Integer> list = Arrays.asList();
        Test.t(ddl.toList(), list);

        end();
    }
    public static void test5(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.append(2);
        ddl.append(3);
        ddl.remove(new Node(2));

        List<Integer> list = Arrays.asList(1, 3);
        Test.t(ddl.toList(), list);

        end();
    }
    public static void test6(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.append(2);
        ddl.append(3);
        ddl.append(4);
        ddl.append(5);
        ddl.remove(new Node(5));

        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        Test.t(ddl.toList(), list);

        end();
    }
    public static void test7(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.append(2);
        ddl.append(3);
        ddl.append(4);
        ddl.append(5);
        ddl.remove(new Node(1));

        List<Integer> list = Arrays.asList(2, 3, 4, 5);
        Test.t(ddl.toList(), list);

        end();
    }
    public static void test8(){
        beg();

        DoubleLinkedList<Integer> ddl = new DoubleLinkedList<>();
        ddl.append(1);
        ddl.append(2);
        ddl.append(3);
        ddl.append(4);
        ddl.append(5);
        ddl.remove(new Node(3));

        List<Integer> list = Arrays.asList(1, 2, 4, 5);
        Test.t(ddl.toList(), list);

        end();
    }
} 

