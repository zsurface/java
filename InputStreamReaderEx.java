import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import classfile.FileLineNum;
import java.util.stream.*;
import java.util.stream.Collectors;

import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;

public class InputStreamReaderEx{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static String getPath(String fname){
        String bitbucket = getEnv("b");
        String fullName = concatPath(list(bitbucket, "testfile", "java", fname));
        return fullName; 
    }

    public static void test0(){
        beg();
        String fname = getPath("test.txt"); 
        List<ArrayList<Integer>> list2 = readFileCharList(fname);
        for(ArrayList<Integer> list: list2){
            pl(list); 
        }
        end();
    }
    public static void test1(){
        beg();
        String fromFile = getPath("test.txt");
        String toFile = getPath("out.txt");
        copyFileStream(fromFile, toFile);

        end();
    }
    
    /**
    <pre>
    {@literal
        FileInputStream and FileOutputStream
        Input File: fromFile
        Output File: toFile

        Stream is good for raw or binary files
    }
    {@code
    }
    </pre>
    */ 
    public static void copyFileStream(String fromFile, String toFile){
        if(fromFile != null && toFile != null){
            try{
                FileInputStream inStream = new FileInputStream(fromFile);
                FileOutputStream outStream = new FileOutputStream(toFile);
                int byteChar = -1;
                while((byteChar = inStream.read()) != -1){
                    outStream.write(byteChar); 
                }
                if(outStream != null)
                    outStream.close();
                if(inStream != null)
                    inStream.close();

            }catch(IOException io){
                io.printStackTrace();
            }
        }
    }
} 

