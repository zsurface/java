import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


public class try_buildtree1{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        StopWatch sw = new StopWatch();
        sw.start();

        Node root = new Node(9);
        root.left = new Node(4);
        root.right = new Node(12);

        try{
            String fname = "/tmp/f1.x";
            Integer k = 0;
            BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
            serialize(root, k, bw);
            bw.close();

            Map<Integer, Integer> map = createMap(fname);
            int k1 = 0;
            Node node = buildTree(map, k1);
            fl("node");
            inorder(node);

        }catch(IOException e){
            e.printStackTrace();
        }


        sw.printTime();
        end();
    }
    /**
               0
          2*0+1             2*0 + 2   
      2*1 + 1  2*1 + 2    2*2 + 1   2*2 + 2 
    */
    public static void serialize(Node root, int k, BufferedWriter bw){
        if(root != null){
            try{
                bw.write(k + " " + root.data + "\n");
                serialize(root.left, 2*k+1, bw);
                serialize(root.right,2*k+2, bw);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
    public static Node buildTree(Map<Integer, Integer> map, Integer k){
        Integer v = map.get(k);            
        if(v != null){
            Node node = new Node(v);    
            node.left = buildTree(map, 2*k+1);
            node.right = buildTree(map, 2*k+2);
            return node;
        }
        return null;
    }
    public static Map<Integer, Integer> createMap(String fname){
        Map<Integer, Integer> map = new HashMap<>();
        if(fname != null){
            try{
                BufferedReader bf = new BufferedReader(new FileReader(fname));
                String s = null;
                while((s = bf.readLine()) != null){
                    String[] arr = s.split("\\s+");
                    if(arr.length == 2){
                        map.put(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
                    }
                    pp(arr);
                }
                bf.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return map;
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();


        sw.printTime();
        end();
    }
} 

