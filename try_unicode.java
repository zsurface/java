import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;

public class try_unicode{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void printCharacterDetails(String character){
        System.out.println("Unicode Value for "+character+"="+Integer.toHexString(character.codePointAt(0)));
        byte[] bytes = character.getBytes();
        System.out.println("The UTF-8 Character="+character+"  | Default: Number of Bytes="+bytes.length);
        String stringUTF16 = new String(bytes, StandardCharsets.UTF_16);
        System.out.println("The corresponding UTF-16 Character="+stringUTF16+"  | UTF-16: Number of Bytes="+stringUTF16.getBytes().length);
        System.out.println("----------------------------------------------------------------------------------------");
    }
    public static void test0(){
        beg();
        {
            // String s = "\t" + new String(Character.toChars(0x1F601));
            String s = "\t" + new String(Character.toChars(0x1D465));
            printCharacterDetails(s);
        }
        {
                try{
                    Charset utf8Charset = Charset.forName("UTF-16");
                    Charset defaultCharset = Charset.defaultCharset();
                    System.out.println(defaultCharset);
                    // charset is windows-1252

                    // String unicodeMessage = "\u4e16\u754c\u4f60\u597d\uff01\u1D565";
                    String unicodeMessage = "1D565";

                    System.out.println(unicodeMessage);
                    // string is printed correctly using System.out (世界你好！)


                    byte[] sourceBytes = unicodeMessage.getBytes("UTF-16");
                    String data = new String(sourceBytes , defaultCharset.name());

                    PrintStream out = new PrintStream(System.out, true, utf8Charset.name());
                    out.println(data);
                }catch(UnsupportedEncodingException e){
                }
        }
//        {
//            PrintWriter printWriter = new PrintWriter(System.out,true);
//            char aa = '\u1D465';
//            printWriter.println("aa = " + aa);
//        }


        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();




        sw.printTime();
        end();
    }
} 

