import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.util.Iterator;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import static classfile.Aron.*;
import static classfile.Print.*;

/*
gx https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple/1.1
<!-- https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple -->
<dependency>
    <groupId>com.googlecode.json-simple</groupId>
    <artifactId>json-simple</artifactId>
    <version>1.1</version>
</dependency>
*/


/*
 * Mon 11 Apr 19:51:38 2022 
 * Update: Update the path files to testfile
 *
 */
// *j-json-example-array-object*   *json-example*
public class JsonArrayExample{
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        test0();
        Ut.l();
        test1();
        Ut.l();
        test2();
        Ut.l();
        test3();
        Ut.l();
        test4();
    }
    public static String testPath(String file){
            String bitbucket = getEnv("b");
            String fPath = bitbucket + "/" + "testfile" + "/" + "java" + "/" + file;
            return fPath;
    }
    static void test0(){
        beg();

        JSONParser parser = new JSONParser();
        try {
            String fPath = testPath("file1.txt");
            Object obj = parser.parse(new FileReader(fPath)); 
 
            JSONObject jsonObject = (JSONObject) obj;
 
            String name = (String) jsonObject.get("Name");
            String author = (String) jsonObject.get("Author");
            JSONArray companyList = (JSONArray) jsonObject.get("Company List");
 
            pl("Name: " + name);
            pl("Author: " + author);
            pl("\nCompany List:");
            Iterator<String> iterator = companyList.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        end();
    }
    static void test1(){
        beg();

        JSONParser parser = new JSONParser();
        try {

            String fPath = testPath("jsonobj.txt");
            Object obj = parser.parse(new FileReader(fPath)); 
 
            JSONObject jsonObject = (JSONObject) obj;
 
            String name = (String) jsonObject.get("name");
            pbl("name=" + name);
            JSONObject jsonObj = (JSONObject)jsonObject.get("obj");
            String age = (String)jsonObj.get("age");
            pbl(age);
        } catch (Exception e) {
            e.printStackTrace();
        }
        end();
    }

    static void test2(){
        beg();

        JSONParser parser = new JSONParser();
        try {

            String fPath = testPath("jsonArray.txt");
            Object obj = parser.parse(new FileReader(fPath)); 
 
            JSONObject jsonObject = (JSONObject) obj;
 
            String name = (String) jsonObject.get("name");
            pbl("name=" + name);
            JSONObject jsonObj = (JSONObject)jsonObject.get("product");
            String age = (String)jsonObj.get("age");
            pbl(age);
            JSONArray jsonArray = (JSONArray)jsonObject.get("array"); 

            Iterator<JSONObject> iterator = jsonArray.iterator();
            while (iterator.hasNext()) {
                pbl(iterator.next().get("id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        end();
    } 

    static void test3(){
        beg();
        JSONParser parser = new JSONParser();
        try {
            String fPath = testPath("jsonObjObj.txt");
            Object obj = parser.parse(new FileReader(fPath)); 
 
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray jsonArray = (JSONArray)jsonObject.get("product"); 
            Iterator<JSONObject> iterator = jsonArray.iterator();
            while (iterator.hasNext()) {
                pbl(iterator.next().get("id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        end();
    } 

    static void test4(){
        beg();
        listAllCategories();
        end();
    } 

    public static void listAllCategories(){
        JSONParser parser = new JSONParser();
        try {
            String fPath = testPath("json_product.txt");
            Object obj = parser.parse(new FileReader(fPath)); 
 
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray jsonArray = (JSONArray)jsonObject.get("products"); 
            Iterator<JSONObject> iterator = jsonArray.iterator();
            while (iterator.hasNext()) {
                JSONObject currJson = iterator.next(); 
                pbl(currJson.get("id"));
                pbl(currJson.get("name"));
                pbl(currJson.get("category"));
                Ut.l();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

