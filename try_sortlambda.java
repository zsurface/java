import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

class Person {
    Integer age;
    public Person(Integer age){
        this.age = age;
    }
}

public class try_sortlambda{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        {
            Integer[] arr = {1, 2, 3, 4};
            int width = arr.length; 
            List<Integer> list = Arrays.asList(arr);
        }

        List<Person> list = Arrays.asList (new Person(1), new Person(2), new Person(0));    
        Collections.sort(list, (Person a, Person b) -> a.age - b.age);
        for(Person pe : list){
            p(pe.age);
        }

        sw.printTime();
        end();
    }
} 

