let SessionLoad = 1
if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
inoremap <F4> =ToggleCompletefunc()
inoremap <Right> :tabn
inoremap <Left> :tabp
map! <D-v> *
nnoremap <silent>  :nohlsearch
vmap ** :s/\%V\S.*\S\%>v/\*\0\*/ 
map ,m :call ChangeJavaMaven()
map ,<F9> :R $HOME/myfile/bitbucket/script/jav.sh % 
map ,rwp <Plug>RestoreWinPosn
map ,swp <Plug>SaveWinPosn
map ,n :b #
noremap ,i :call ToggleIgnoreCase()
noremap ,k :call ToggleCompletefunc()
noremap ,s :call ToggleSpell()
nmap <silent> ,c :call CopyJavaMethod()
nmap <silent> ,d :!open dict://<cword>
map _in :call IncreaseColor() 
vnoremap a$ F$of$
onoremap <silent> a$ :normal! F$vf$
vmap gx <Plug>NetrwBrowseXVis
nmap gx <Plug>NetrwBrowseX
nnoremap gO :!open <cfile>
vnoremap i$ T$ot$
onoremap <silent> i$ :normal! T$vt$
vmap xx :s/\%V\_^\%V/\/\//g 
vmap xu :s/\%V\_^\s*\zs\/\/\%V//g 
map <F9> :w! |  :call CompileJava()  
vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(expand((exists("g:netrw_gx")? g:netrw_gx : '<cfile>')),netrw#CheckIfRemote())
nmap <silent> <Plug>RestoreWinPosn :call RestoreWinPosn()
nmap <silent> <Plug>SaveWinPosn :call SaveWinPosn()
map <F12> :Bf o
map <F8> :w!
map <F5> :tabnew $HOME/myfile/bitbucket/snippets/snippet.vimrc| :tabnew $HOME/myfile/bitbucket/snippets/snippet.m 
map <F4> :vertical res -8
map <F3> :vertical res +8
map <PageDown> :tabc
map <PageUp> :tabnew
nnoremap <Right> :tabn
nnoremap <Left> :tabp
vmap <BS> "-d
vmap <D-x> "*d
vmap <D-c> "*y
vmap <D-v> "-d"*P
nmap <D-v> "*P
imap  :call AddBegin()
inoremap ,s =ToggleSpell()
inoremap ,i =ToggleIgnoreCase()
inoremap ,k =ToggleCompletefunc()
cmap BB s//\[\0\]/  
cmap SV vim // **/*.m
cmap SS .,$s///gc
cmap White /\S\zs\s\+$
iabbr <expr> jprr_system_out_println 'System.out.println(xxx)' . "\" . "^" . ":.,.s/xxx/i/gc" . ""
iabbr <expr> forr_one_for_loop 'for(int xxx=0; xxx<10; xxx++){}' . "\" . "1k" . "^". ":.,.s/xxx/i/gc" . ""
iabbr <expr> for2_two_for_loop 'for(int xxx=0; xxx < 9; xxx++){for(int xxx=0; xxx < 9; xxx++){}}' . "\" . "3k" . "^" . ":.,.s/xxx/i/gc" . ""
iabbr <expr> jim 'import java.io.*;import java.lang.String;import java.util.*;' . "\" . "^"
iabbr <expr> jl 'List<String> list = new ArrayList<String>();' . "\" . "^" . ":.,.s/String/Integer/gc" . ""
iabbr <expr> jm_HashMap 'Map<String, Integer> map = new HashMap<String, Integer>();' . "\" . "^"
iabbr <expr> jll_ListOfList 'ArrayList<ArrayList<String>> list2d = new ArrayList<ArrayList<String>>();' . "\" . "^"
iabbr <expr> r- "// ".'80i-' . ""
iabbr <expr> r= "// ".'80i=' . ""  
cabbr TTT :exec 'echo expand("%:p")' 
cabbr vnote :Bf o
iabbr ~~~ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
iabbr +++ ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
iabbr === ================================================================== 
iabbr --- ------------------------------------------------------------------
iabbr <expr> ddd strftime('%c')
iabbr skk // searchkey:
cabbr se :call SetEnvVar() 
cabbr pl :call ListTabPage() 
cabbr bufm :call ToggleBufferManager() 
cabbr Ma :call MaximizeToggle() 
cabbr Ela :tabnew /Library/WebServer/Documents/zsurface/html/indexLatexMatrix.html
cabbr Info :tabnew ~/.viminfo
cabbr Ta call TagsSymlink() 
cabbr Tb :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/bash & 
cabbr Tf :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/fish & 
cabbr Tz :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/zsh & 
cabbr Ky :let @*=expand("%:p")
cabbr Job :tabnew /Users/cat/GoogleDrive/job/recruiter_email.txt 
cabbr No :tabnew $HOME/myfile/bitbucket/note/noteindex.txt 
cabbr FF :call JavaComment()  
cabbr Ep :tabnew /Users/cat/myfile/vimprivate/private.vimrc
cabbr Ec :tabe /Library/WebServer/Documents/zsurface/html/indexCommandLineTricks.html  
cabbr Eng :tabe /Library/WebServer/Documents/zsurface/html/indexEnglishNote.html  
cabbr Evimt :tabe /Library/WebServer/Documents/zsurface/html/indexVimTricks.html
cabbr Enote :tabe /Library/WebServer/Documents/zsurface/html/indexDailyNote.html
cabbr Esty :tabe /Library/WebServer/Documents/zsurface/style.css
cabbr Sty :%!astyle --style=java 
cabbr Int :!open /Users/cat/myfile/github/text/intellij_shortcut.pdf    
cabbr Cmmc :! /Users/cat/myfile/script/CodeManager.sh
cabbr Cmm :! /Users/cat/myfile/github/javabin/CodeManager/run.sh 
cabbr Res :!open /Users/cat/GoogleDrive/NewResume/aronsitu00resume.pdf     
cabbr Tiny :!/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome  tiny3.com  -incongnito 
cabbr Wo :tabe /Users/cat/myfile/github/vim/myword.utf-8.add    " My words file
cabbr jf :tabe /tmp/f.x
cabbr mylib :tabe /Users/cat/myfile/github/cpp/MyLib
cabbr mm :marks
cabbr qn :tabe $HOME/myfile/github/quicknote/quicknote.txt " quick node
cabbr esess :tabe $HOME/myfile/vimsession/sess.vim 
cabbr mk :mksession! $HOME/myfile/vimsession/sess.vim
cabbr ep :tabnew /etc/profile 
cabbr mmax printf '\ePtmux;\e\e[9;1t\e\\'
cabbr mhalf printf '\ePtmux;\e\e[8;100;200t\e\\'
cabbr full printf '\e[10;1t' " full-screen mode 
cabbr umax printf '\e[10;0t' " undo full-screen mode 
cabbr right printf '\e[3;1410;0t' " move to top left corner
cabbr left printf '\e[3;10;10t' " move to top left corner
cabbr half printf '\e[8;100;200t \e[3;1410;0t' " resize to 200x100/move to right
cabbr maxh printf '\e[9;3t' " maximize horizontally
cabbr maxv printf '\e[9;2t' " maximize vertically
cabbr max printf '\e[9;1t' " maximize
cabbr min printf '\e[2t' " minimize window
cabbr cpp :call SetEnvVar()
cabbr sll :call StatusLineManager()
cabbr ev :tabe $HOME/myfile/bitbucket/vim/vim.vimrc
cabbr sv :source $HOME/myfile/bitbucket/vim/vim.vimrc 
cabbr big :tabe  $HOME/cat/myfile/github/java/big.java
cabbr kk .g/\S*\%#\S*/y | let @*=@" 
cabbr ip :call  ChangePathInit()
cabbr fp :call  CopyCurrentFilePath()
cabbr upp :call  system('scp /Users/longshu/myfile/backup/up.x dev-dsk-longshu-2c-84ff96da.us-west-2.amazon.com:/home/longshu/try')
cabbr up :tabnew /Users/longshu/myfile/backup/up.x
cabbr aa %s/\(,\)\(\w\)/\1\r\2/g
cabbr mmm %s/--- a/\/home\/longshu\/myfile\/workspace\/ContraCogsFunding\/src\/ContraCogsFundingBookingLibrary/g
cabbr eb :tabe ~/.bashrc
let &cpo=s:cpo_save
unlet s:cpo_save
set ambiwidth=double
set autochdir
set autoindent
set autoread
set backspace=2
set complete=.,w,b,u,t,i,k$HOME/myfile/github/java/*,k$HOME/myfile/github/JavaLib/*,k$HOME/myfile/github/Jsource/*
set completefunc=CompleteAbbre
set dictionary=~/myfile/bitbucket/vim/words.txt
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set helplang=en
set hlsearch
set ignorecase
set laststatus=2
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set omnifunc=csscomplete#CompleteCSS
set path=.,/usr/include,,,**
set ruler
set shell=/bin/zsh
set shiftwidth=4
set showcmd
set smartindent
set spellfile=~/myfile/bitbucket/vim/myword.utf-8.add
set statusline=%t\ [%1.5n]\ %l:%c\ %r\ %m
set noswapfile
set tabstop=4
set tags=~/myfile/backup/JavaJDK/jdk/.tag,~/myfile/workspace/ContraCogsFunding/src/ContraCogsFundingBookingLibrary/.tag,~/myfile/workspace/ContraCogsFunding/src/ContraCogsFundingDomain/.tag
set notimeout
set timeoutlen=100
set ttimeout
set wildmenu
set window=63
set nowritebackup
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/myfile/bitbucket/java
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +69 ~/myfile/bitbucket/java/GuassianElimination.java
badd +2006 ~/myfile/bitbucket/javalib/Aron.java
badd +334 ~/myfile/bitbucket/java/Test/AronTest.java
badd +1 ~/myfile/bitbucket/javalib
badd +1 ~/myfile/bitbucket/java/Guassian2.java
badd +1 ~/myfile/bitbucket/java/GuassianEli
argglobal
silent! argdel *
$argadd GuassianElimination.java
edit ~/myfile/bitbucket/java/GuassianElimination.java
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winminheight=1 winheight=1 winminwidth=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 116 + 136) / 272)
exe 'vert 2resize ' . ((&columns * 155 + 136) / 272)
argglobal
vmap <buffer> str :s/\%V.*\%V/"\0"/ 
setlocal keymap=
setlocal noarabic
setlocal autoindent
setlocal backupcopy=
setlocal balloonexpr=
setlocal nobinary
setlocal nobreakindent
setlocal breakindentopt=
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
setlocal colorcolumn=
setlocal comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-
setlocal commentstring=/*%s*/
setlocal complete=.,w,b,u,t,i,k$HOME/myfile/github/java/*,k$HOME/myfile/github/JavaLib/*,k$HOME/myfile/github/Jsource/*
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=CompleteAbbre
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal define=
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=
setlocal expandtab
if &filetype != 'java'
setlocal filetype=java
endif
setlocal fixendofline
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=0
setlocal foldmarker={{{,}}}
setlocal foldmethod=manual
setlocal foldminlines=1
setlocal foldnestmax=20
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=tcq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal formatprg=
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=-1
setlocal include=
setlocal includeexpr=
setlocal indentexpr=
setlocal indentkeys=0{,0},:,0#,!^F,o,O,e
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal lispwords=
setlocal nolist
setlocal makeencoding=
setlocal makeprg=
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=bin,octal,hex
setlocal nonumber
setlocal numberwidth=4
setlocal omnifunc=csscomplete#CompleteCSS
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal shiftwidth=4
setlocal noshortname
setlocal signcolumn=auto
setlocal smartindent
setlocal softtabstop=0
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=~/myfile/bitbucket/vim/myword.utf-8.add
setlocal spelllang=en
setlocal statusline=
setlocal suffixesadd=
setlocal noswapfile
setlocal synmaxcol=3000
if &syntax != 'java'
setlocal syntax=java
endif
setlocal tabstop=4
setlocal tagcase=
setlocal tags=
setlocal termkey=
setlocal termsize=
setlocal textwidth=0
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal nowinfixheight
setlocal nowinfixwidth
setlocal wrap
setlocal wrapmargin=0
silent! normal! zE
let s:l = 227 - ((34 * winheight(0) + 34) / 69)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
227
normal! 016|
lcd ~/myfile/bitbucket/java
wincmd w
argglobal
if bufexists('~/myfile/bitbucket/javalib/Aron.java') | buffer ~/myfile/bitbucket/javalib/Aron.java | else | edit ~/myfile/bitbucket/javalib/Aron.java | endif
vmap <buffer> str :s/\%V.*\%V/"\0"/ 
setlocal keymap=
setlocal noarabic
setlocal autoindent
setlocal backupcopy=
setlocal balloonexpr=
setlocal nobinary
setlocal nobreakindent
setlocal breakindentopt=
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
setlocal colorcolumn=
setlocal comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-
setlocal commentstring=/*%s*/
setlocal complete=.,w,b,u,t,i,k$HOME/myfile/github/java/*,k$HOME/myfile/github/JavaLib/*,k$HOME/myfile/github/Jsource/*
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=CompleteAbbre
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal define=
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=
setlocal expandtab
if &filetype != 'java'
setlocal filetype=java
endif
setlocal fixendofline
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=0
setlocal foldmarker={{{,}}}
setlocal foldmethod=manual
setlocal foldminlines=1
setlocal foldnestmax=20
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=tcq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal formatprg=
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=-1
setlocal include=
setlocal includeexpr=
setlocal indentexpr=
setlocal indentkeys=0{,0},:,0#,!^F,o,O,e
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal lispwords=
setlocal nolist
setlocal makeencoding=
setlocal makeprg=
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=bin,octal,hex
setlocal nonumber
setlocal numberwidth=4
setlocal omnifunc=csscomplete#CompleteCSS
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal shiftwidth=4
setlocal noshortname
setlocal signcolumn=auto
setlocal smartindent
setlocal softtabstop=0
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=~/myfile/bitbucket/vim/myword.utf-8.add
setlocal spelllang=en
setlocal statusline=
setlocal suffixesadd=
setlocal noswapfile
setlocal synmaxcol=3000
if &syntax != 'java'
setlocal syntax=java
endif
setlocal tabstop=4
setlocal tagcase=
setlocal tags=
setlocal termkey=
setlocal termsize=
setlocal textwidth=0
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal nowinfixheight
setlocal nowinfixwidth
setlocal wrap
setlocal wrapmargin=0
silent! normal! zE
let s:l = 1233 - ((46 * winheight(0) + 34) / 69)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1233
normal! 028|
lcd ~/myfile/bitbucket/javalib
wincmd w
exe 'vert 1resize ' . ((&columns * 116 + 136) / 272)
exe 'vert 2resize ' . ((&columns * 155 + 136) / 272)
tabnext 1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
set winminheight=1 winminwidth=1
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
