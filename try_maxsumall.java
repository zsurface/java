import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class try_maxsumall{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        Integer[] arr = {2, -3, 4, 9, -2};
        Integer m = maxSum(arr);
        p(arr);
        p("max=" + m);


        sw.printTime();
        end();
    }

    /**
        k = 3
        1, 2 => beg = 0, i = 1
        s = 0
        s = 1
        s = 3

        k = 3
        1, 4, 1, 2
        s = 0
        s = 1
        s = 5
        s = 4 beg = 1
        s = 1 beg = 2
        s = 2 
        s = 4 beg = 3

        k = 3
        4 1 2

        s = 0, beg = 0, i = 0
        s = 4, 0, 1, beg =1   i = 1  
        s = 3, i = 2 => (beg = 1, i = 2)

        k = 2
        1, 3, 2 
        s = 0, beg = 0, i=0
        s = 1,          i=1
        s = 4,          i=2
            s = 1, beg = 1

    */
    public static Pair<Integer, Intege> maxContinuousSum(Integer[] arr, Integer k){
        Pair<Integer, Integer> p = new Pair<>();
        if(arr != null){
            int max = 0;
            int s = 0;
            int beg = 0;
            int end = 0;
            for(int i=0; i<arr.length; i++){
                if(s == k) {
                    p = new Pair<>(beg, i);
                }
                else if(s > k){
                    s -= arr[beg];
                    beg++;
                }
                s += arr[i];
            }
        }
        return p;
    }
    public static int maxSum(Integer[] arr){
            int len = arr.length;
            List<Integer> list = Arrays.asList(arr); 
            int max = 0;
            int s = 0;
            // 1 => 1
            // 1 -3 => 1
            // 1 -3 2 4
            // m=1, m=0, m=2, m=6 => 6
            for(Integer n : list){
                s += n;            
                if(s < 0)
                    s = 0;
                if(max < s)
                    max = s;
            }
        return max;
    }
} 

