import java.util.*;
import classfile.Aron;
import classfile.Print;

public class Main{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        List<Person> list1 = new ArrayList<Person>();
        list1.add(new Person("David", 20));
        list1.add(new Person("Michael", 30));
        list1.add(new Person("Michelle", 30));
        list1.add(new Person("Mexico", 10));

        List<Person> list2 = new ArrayList<Person>();
        list2.add(new Person("David", 20));
        list2.add(new Person("Michael", 30));
        list2.add(new Person("Michelle", 30));
        list2.add(new Person("Mexico", 10));

        for(Person p : list1){
            p.print();
        }

        Print.ln();
        for(Person p : list2){
            p.print();
        }

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        Aron.end();
    }
}
