import java.util.Collections;
import java.util.List;

/**
 * Created by cat on 3/7/19.
 */
public final class SortPerson {
    public List<Person> sort(List<Person> list){
        Collections.sort(list);
        return list;
    }
    public List<Person> sortMyPerson(List<Person> list){
        MyPerson myPerson = new MyPerson();
        Collections.sort(list, myPerson);
        return list;
    }
}
