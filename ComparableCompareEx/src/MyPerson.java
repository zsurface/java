import java.util.Comparator;

class MyPerson implements Comparator<Person> {
    public int compare(Person first, Person second){
        return first.age - second.age;
    }
}