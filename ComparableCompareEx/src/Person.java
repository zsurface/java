class Person implements Comparable<Person>{
    String name;
    Integer age;
    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
    }

    public int compareTo(Person other ){
        return age - other.age;
    }
    public void print(){
        System.out.println("name=" + name + " age=" + age);
    }
}