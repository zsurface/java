import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.security.MessageDigest;

class Node {
    public String key;
    public String value;
    public Node left;
    public Node right;

    public Node(String key, String value) {
        this.key = key;
        this.value = value;
    }
}

class BinaryTree {
    Node root;

    public BinaryTree() {
    }
    public String get(String k) {
        Node curr = root;
        while(curr != null) {
            if(hash(k).compareTo(curr.key) < 0) {
                curr = curr.left;
            } else if(hash(k).compareTo(curr.key) > 0) {
                curr = curr.right;
            } else {
                return curr.value;
            }
        }
        return null;
    }
    public Boolean contains(String k) {
        Node curr = root;
        while(curr != null) {
            if(hash(k).compareTo(curr.key) < 0) {
                curr = curr.left;
            } else if(hash(k).compareTo(curr.key) > 0) {
                curr = curr.right;
            } else {
                return true;
            }
        }
        return false;
    }

    public static String hash(String str) {
        String hash = null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(str.getBytes());
            hash = new String(messageDigest.digest());
        } catch(Exception e) {
            e.printStackTrace();
        }
        return hash;
    }

    // use key to insert node to binary tree
    // TODO use Integer instead of String
    public void insert(String k, String v) {
        if(root == null) {
            root = new Node(hash(k), v);
        } else {
            Node curr = root;
            while(curr != null) {
                // left subtree
                if(hash(k).compareTo(curr.key) <= 0) {
                    if(curr.left == null) {
                        curr.left = new Node(hash(k), v);
                        break;
                    } else
                        curr = curr.left;
                } else {
                    if(curr.right == null) {
                        curr.right = new Node(hash(k), v);
                        break;
                    } else
                        curr = curr.right;
                }
            }
        }
    }
}


public class BinaryTreeHashMap {
    public static void main(String[] args) {
        // test0();
        test1();
    }
    public static void inorder(Node root) {
        if(root != null) {
            inorder(root.left);
            p("k=" + root.key + " v=" + root.value);
            inorder(root.right);
        }
    }

    public static void test0() {
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        BinaryTree bin = new BinaryTree();
        bin.insert("k6", "v6");
        bin.insert("k2", "v2");
        bin.insert("k8", "v8");
        inorder(bin.root);
        fl("contains");
        p("contains k2=" + (bin.contains("k2") == true));
        p("contains k3=" + (bin.contains("k3") == false));
        p("get(k2)=" + bin.get("k2").equals("v2"));
        p("get(k5)=" + (bin.get("k5") == null));

        sw.printTime();
        end();
    }
    public static void test1() {
        beg();
        
        fl("insert");
        StopWatch sw1 = new StopWatch();
        sw1.start();

        List<String> list = geneRandomStrList(10000000);
        
        Map<String, String> map = new HashMap<>();
        for(String s : list){
            map.put(s, s);
        }

//        BinaryTree bin = new BinaryTree();
//        for(String s : list){
//            bin.insert(s, s);
//        }
        sw1.printTime();

//        fl("contains");
//        StopWatch sw = new StopWatch();
//        sw.start();
//
//        p("abacde=" + bin.contains("abacde"));
//
//        sw.printTime();


        end();
    }
}

