import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_permutation{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        permutation("", "abc");
        Print.p("decimal=" + decimal("1"));
        Print.p("decimal=" + decimal("123"));
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Print.p("binary=" + numToBinary(0));
        Print.p("binary=" + numToBinary(1));
        Print.p("binary=" + numToBinary(4));
        Aron.end();
    }
    /**
    abc 
      p(abc)
        a + p(bc)
            b + p(c)
                c + p("")
            c + p(b)
                b + p("")
        b + p(ac)
            a + p(c) 
                c + p("")
            c + p(a)
                a + p("")
        c + p(ab)
            a + p(b)
                b + p("")
            b + p(a)
                a + p("")
    */
    public static void permutation(String prefix, String s){
        if(s.length() == 0){
            Print.pl("perm=" + prefix);
        }else{
            for(int i=0; i<s.length(); i++){
                String ss = "" + s.charAt(i);
                permutation(prefix + ss, remove(i, s));
            }
        }
    }
    public static String remove(int n, String s){
        StringBuffer sb = new StringBuffer(s);
        sb.deleteCharAt(n);
        return sb.toString();
    }
    /**
        string to decimal, string to number, string to integer
        "123" = 3*pow(10, 0) + 2*pow(10, 1) + 1 * pow(10, 2) 
        decimal("123") => 123
    */
    public static int decimal(String s){
        int num = 0;
        if( s != null){
            int len = s.length();
            for(int i=len - 1; i>=0; i--){
                char c = s.charAt(i);
                int n = (int)c - '0';
                num += n * (int)Math.pow(10, (len - 1) - i);
            }
        }
        return num;
    }
    
    /**
    <pre>
    {@literal
        Convert Numbet to Binary String
    }
    {@code
        numToBinary(4) => "100"
    }
    </pre>
    */ 
    public static String numToBinary(int n){
        String s = "";
        if(n == 0)
           s = "0"; 
        while(n > 0){
            int mod = n % 2;
            int q = n / 2;
            s = mod + s;
            n = q;
        }
        return s;
    }
} 

