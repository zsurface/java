import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

class Person{
    String name;
    Integer age;
}
/**
    Implement your own Iterator
    There are three methods that need to be overridden 

    Boolean hasNext()
    T next()
    void remove()
*/

class MyIterator implements Iterator<Person>{
    List<Person> list;
    public MyIterator(List<Person> list){
        this.list = new ArrayList<>();
        for(Person p : list)
            this.list.add(p);
    }
    public boolean hasNext(){
        return list.size() > 0;
    }
    public Person next(){
        if(hasNext())
            return list.remove(list.size() - 1); 
        return null;
    }
    public void remove(){
        list.remove(list.size() - 1); 
    }
}

public class try_scanner{
    public static void main(String[] args) {
//        test0();
//        test1();
        test2();
//        standardInput();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        
        List<Integer> list1 = of(1, 2, 3);
        List<Integer> list2 = of(1, 2, 3);

        List<ArrayList<Integer>> ls2 = zipWith2d((x, y) -> x + y, list1, list2);
        p(ls2);


        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

        List<Integer> list1 = of(1, 2, 3);
        List<Integer> list2 = of(1, 2, 3);
        List<Integer> list3 = of(1, 2, 3);

        List<ArrayList<ArrayList<Integer>>> ls3 = zipWith3d((x, y, z) -> x + y + z, list1, list2, list3);
        for(ArrayList<ArrayList<Integer>> s2 : ls3){
            p(s2);
        }


        sw.printTime();
        end();
    }
    public static void test2(){
        beg();
        int n = 8000;
        
//        {
//            StopWatch sw = new StopWatch();
//            sw.start();
//
//            Integer[][] arr2d = geneMatrixRandomInteger(n, n, n);
//            List<ArrayList<Integer>> ls2 = arrayToList2(arr2d);
//
//            sw.printTime();
//        }
        {
            StopWatch sw = new StopWatch();
            sw.start();

            Integer[][] arr2d = geneMatrixRandomInteger(n, n, n);
            List<List<Integer>> ls3 = arrayToList3(arr2d);

            sw.printTime();
        }

        end();
    }

    /**
    <pre>
    {@literal
        KEY: system.in, standard input
        Scanner implements Iterator,
        interface Interator<T>
            hasNext()
            next()
            remove()
    }
    {@code
    }
    </pre>
    */ 
    public static void standardInput(){
        Scanner scanner = new Scanner(System.in);
        String s = "";
        while(true){
            if(scanner.hasNext()){
                s = scanner.next();
                if(!s.equals("x")){
                    p("s=" + s);
                }else{
                    break;
                }
            }
        }
    }
    public static <T> List<List<T>> arrayToList3(T[][] data){
        return Arrays.stream(data).map(Arrays::asList).collect(Collectors.toList());
    }
} 

