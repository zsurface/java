import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
   Lease Recent Used
   1. Bouble Linked List with maximum size
   2. Implement append, and removeFront operation
   3. If list is full, then remove the least node from the head.
 */

class LRU{
       class Node{
	Node prev;
	Node next;
	String data;
	public Node(String data){
	    this.data = data;
	}	  
    }
		       

    int max;
    int count;
    Node head;
    Node tail;
    Map<String, Node> map = new HashMap<>();
    
    public LRU(int max){
        this.max = max;
    }
    public void insertKV(String key, String value){
	Node node = new Node(value);
	if(count < max){
	    map.put(key, node);
	    append(node);
	    count++;
	}else{
	    Node n = map.get(key);
	    if(n != null){
		// found key
		remove(n);
		map.put(key, node);
		append(node);
	    }else{
		// no key is found
		removeHead();
		map.put(key, node);
		append(node);
	    }
	}
    }
    public void remove(Node node){
	// first node
	if(node.prev == null){
	    removeHead();
	}else if(node.next == null){
	    // last node
	    Node prev = tail.prev;
	    prev.next = null;
	    tail = prev;
	}else{
	    // "middle" node
	    Node prev = node.prev;
	    Node next = node.next;
	    prev.next = next;
	    next.prev = prev;
	    node = null;
	}
    }
    // 1. one node
    // 2 . two or more nodes
    public void removeHead(){
	if(head != null){
	    Node tmp = head;
	    head = tmp.next;
	    // two or more nodes
	    if(head != null)
		head.prev = null;
	    else
		tail = null;
	}
    }
    public void append(Node node){
	if(tail == null){
	    head = tail = node;
	}else{
	    Node tmp = tail;
	    tmp.next = node;
	    node.prev = tmp;
	    tail = node;
	}
    }
    public void print(){
	Node curr = head;
	while(curr != null){
	    System.out.println(curr.data);
	    curr = curr.next;
	}
    }
}



public class try_LRU1{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();


        sw.printTime();
        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();

	fl("LRU(2) v3");
	{
	    LRU lru = new LRU(2);
	    lru.insertKV("k1", "v1");
	    lru.insertKV("k2", "v2");
	    lru.insertKV("k3", "v3");
	    lru.print();
	}
	fl("LRU(3) v1 v2 v3");
	{
	    LRU lru = new LRU(3);
	    lru.insertKV("k1", "v1");
	    lru.insertKV("k2", "v2");
	    lru.insertKV("k3", "v3");
	    lru.print();
	}
	fl("LRU(1) v3");
	{
	    LRU lru = new LRU(1);
	    lru.insertKV("k1", "v1");
	    lru.insertKV("k2", "v2");
	    lru.insertKV("k3", "v3");
	    lru.print();
	}

	fl("LRU(1)  v4");
	{
	    LRU lru = new LRU(1);
	    lru.insertKV("k1", "v1");
	    lru.insertKV("k2", "v2");
	    lru.insertKV("k2", "v4");
	    lru.print();
	}
        fl("LRU(1)  v2 v4 v33");                   
        {                                   	
            LRU lru = new LRU(3);           	
            lru.insertKV("k1", "v1");       	
            lru.insertKV("k2", "v2");       
            lru.insertKV("k3", "v3");
	    lru.insertKV("k4", "v4");
	    lru.insertKV("k3", "v33");
            lru.print();                   
        }
	
        sw.printTime();
        end();
    }
} 

