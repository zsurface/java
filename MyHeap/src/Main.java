
import classfile.Heap;
import classfile.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import static classfile.Aron.*;
import static classfile.Test.*;
import static classfile.Print.*;
import static classfile.Heap.*;


/**
 * KEY: heap sort, heapify
 */
public class Main {
    public static void main(String[] args) {
        pp("Hello World");
        test0();
        test1();
        test2();
    }
    public static void test2(){
        beg();
        {
            List<Node> list = new ArrayList<>();
            list.add(new Node(4));
            List<Node> ret = heapSort(list, (x, y) -> x.data < y.data);
            t(ret.get(0).data, 4);
        }
        {
            List<Node> list = new ArrayList<>();
            list.add(new Node(4));
            list.add(new Node(1));
            list.add(new Node(2));
            List<Node> ret = heapSort(list, (x, y) -> x.data < y.data);
            t(ret.get(0).data, 1);
            t(ret.get(1).data, 2);
            t(ret.get(2).data, 4);
        }
        {
            List<Node> list = new ArrayList<>();
            list.add(new Node(4));
            list.add(new Node(1));
            list.add(new Node(2));
            list.add(new Node(0));
            List<Node> ret = heapSort(list, (x, y) -> x.data < y.data);
            t(ret.get(0).data, 0);
            t(ret.get(1).data, 1);
            t(ret.get(2).data, 2);
            t(ret.get(3).data, 4);
        }
        end();
    }


    static void test0(){
        beg();
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(2);
            heap.insert(n1);
            t(heap.list.get(1).data, 2);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(4);
            Node n3 = new Node(8);
            // Node n4 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            // heap.insert(n4);
            t(heap.list.get(1).data, 8);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(4);
            Node n3 = new Node(8);
            // Node n4 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            // heap.insert(n4);
            t(heap.list.get(1).data, 8);
            t(heap.list.get(2).data, 2);
            t(heap.list.get(3).data, 4);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            t(heap.list.get(1).data, 2);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(3);
            Node n3 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            t(heap.list.get(1).data, 3);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(1);
            heap.insert(n1);
            Node n = heap.removeTop();
            t(n.data, 1);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(4);
            Node n2 = new Node(2);
            Node n3 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            Node n = heap.removeTop();
            t(n.data, 4);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data > y.data);
            Node n1 = new Node(4);
            Node n2 = new Node(1);
            Node n3 = new Node(2);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            Node nn = heap.removeTop();
            t(nn.data, 4);
            Node nn1 = heap.removeTop();
            t(nn1.data, 2);
            Node nn2 = heap.removeTop();
            t(nn2.data, 1);
            Node nn3 = heap.removeTop();
            t(nn3, null);
        }
        end();
    }
    static void test1(){
        beg();
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(2);
            heap.insert(n1);
            t(heap.list.get(1).data, 2);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(4);
            Node n3 = new Node(8);
            // Node n4 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            // heap.insert(n4);
            t(heap.list.get(1).data, 2);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(4);
            Node n3 = new Node(8);
            // Node n4 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            // heap.insert(n4);
            t(heap.list.get(2).data, 4);
            t(heap.list.get(3).data, 8);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            t(heap.list.get(1).data, 1);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(3);
            Node n3 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            t(heap.list.get(1).data, 1);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(2);
            Node n2 = new Node(3);
            Node n3 = new Node(1);
            Node n4 = new Node(9);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            heap.insert(n4);
            t(heap.list.get(1).data, 1);
            t(heap.list.get(2).data, 3);
            t(heap.list.get(3).data, 2);
            t(heap.list.get(4).data, 9);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(1);
            heap.insert(n1);
            Node n = heap.removeTop();
            t(n.data, 1);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(4);
            Node n2 = new Node(2);
            Node n3 = new Node(1);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            Node n = heap.removeTop();
            t(n.data, 1);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(4);
            Node n2 = new Node(1);
            Node n3 = new Node(2);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            Node n = heap.removeTop();
            t(n.data, 1);
        }
        {
            Heap<Node> heap = new Heap<Node>((x, y) -> x.data < y.data);
            Node n1 = new Node(4);
            Node n2 = new Node(1);
            Node n3 = new Node(2);
            heap.insert(n1);
            heap.insert(n2);
            heap.insert(n3);
            Node nn = heap.removeTop();
            t(nn.data, 1);
            Node nn1 = heap.removeTop();
            t(nn1.data, 2);
            Node nn2 = heap.removeTop();
            t(nn2.data, 4);
            Node nn3 = heap.removeTop();
            t(nn3, null);
        }


        end();
    }
}
