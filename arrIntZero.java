import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class arrIntZero{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        {
            int[] arr = {0};
            int width = arr.length; 
            Test.t(Aron.isZero(arr), true);
        }

        {
            int[] arr = {1, 2, 3, 4};
            int width = arr.length; 
            Test.t(Aron.isZero(arr), false);
        }

        {
            int[] arr = {-3};
            int width = arr.length; 
            Test.t(Aron.isZero(arr), false);
        }
        {
            int[] arr = {0, -3};
            int width = arr.length; 
            Test.t(Aron.isZero(arr), false);
        }

        Aron.end();
    }
    
} 

