import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;


public class AddLongIntegerNum{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        int[] arr1 = {9, 9};
        int[] arr2 = {9};
        int[] arr = addLongInt(arr1, arr2);
        Print.p(arr);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        {
            int[] arr1 = {0};
            int[] arr2 = {9};
            int[] arr = addLongInt(arr1, arr2);
            Print.p(arr);
        }
        {
            int[] arr1 = {9};
            int[] arr2 = {9};
            int[] arr = addLongInt(arr1, arr2);
            Print.p(arr);
        }

        Aron.end();
    }

    public static int[] padArr(int len, int[] arr){
        // x x x 
        // _ y y 
        int la = arr.length; // la = 2
        int[] a = new int[len]; // len = 3
        int diff = len - la;
        for(int i=0; i<len; i++){
            a[i] = i < diff ? 0 : arr[i - diff]; // i=0, i=1=> 0, i=2 => 1[
        }
        return a;
    }
    public static int[] addLongInt(int[] arr1, int[] arr2){

        if( arr1 != null && arr2 != null){
            int len1 = arr1.length;    
            int len2 = arr2.length;
            int len = Math.max(len1, len2);
            int[] re = new int[len + 1];
            if(len1 < len2){
                int[] a1 = padArr(len2, arr1);
                int c = 0;
                for(int i=0; i<len2; i++){
                    int r = len2 - 1 - i;
                    int s = c + (a1[r] + arr2[r]);
                    re[len + 1 - 1 - i] = s % 10;
                    c = s / 10;
                }
                re[0] = c;
            }else{
                int[] a2 = padArr(len1, arr2);
                int c = 0;
                for(int i=0; i<len1; i++){
                    int r = len1 - 1 - i;
                    int s = c + (arr1[r] + a2[r]);
                    re[len + 1 - 1 - i] = s % 10;
                    c = s / 10;
                }
                re[0] = c;
            }
            return re;
        }
        return null;
    }
} 

