import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;

import classfile.Tuple;
import classfile.Triple;
import classfile.FileKeyPattern;
import java.util.stream.*;
import java.util.stream.Collectors;


class Student2{
    String name;
    Integer age;
    public Student2(String name, Integer age){
        this.name = name;
        this.age = age;
    }
}

class MyNode{
    String name;
    MyNode next;
    public MyNode(String name){
        this.name = name;
    }
    public MyNode(){
    }
}

class Customer{
    public String name;
    public Integer age;
    public Customer(String name, Integer age){
        this.name = name;
        this.age = age;
    }
    @Override
        public String toString(){
            return "name=" + name + " " + "age=" + age.toString();
        }
}

class MyLinkedList{
    MyNode first;
    public MyLinkedList(){
    }
    public void add(MyNode node){
        if(first == null){
            first = node;
        }else{
            first.next = node;
        }
    }
    public Integer size(){
        MyNode curr = first;
        Integer count = 0;
        while(curr != null){
            count++;
            curr = curr.next;
        }
        return count;
    }
    /*
       v
       [1] -> [2]
       v
       [1] -> [2]

     */
    public void removeFirst(){
        if(first != null){
            first = first.next;
        }
    }

    public void removeLast(){
        MyNode curr = first;
        MyNode prev = null;
        if(curr != null){
            // [1] -> [2]
            // curr   curr.next
            if(curr.next != null){
                while(curr.next != null){
                    prev = curr;
                    curr = curr.next;
                }
                prev.next = null;
            }else{
                first = null;
            }
        }
    }

    /*
       Remove a given node from the linkedlist
     */
    public void removeNode(MyNode node){
        MyNode curr = first;
        if(first != null){
            if(first.name.compareTo(node.name) == 0){
                removeFirst(); // first name
            }else{
                MyNode lnode = lastNode();
                if(lnode.name.compareTo(node.name) == 0){
                    removeLast(); // last node
                }else{
                    // "middle" node
                    // [1] -> [2] -> [3]
                    // prev   curr   curr.next
                    // [1]    ->     [3]
                    MyNode prev = null;
                    while(curr.next != null){
                        if(node.name.compareTo(curr.name) == 0){
                            prev.next = curr.next;
                            break;
                        }
                        prev = curr;
                        curr = curr.next;
                    }
                }
            }
        }
    }

    public MyNode lastNode(){
        MyNode curr = first;
        MyNode prev = null;
        if(curr != null){
            while(curr != null){
                prev = curr;
                curr = curr.next;
            }
        }
        return prev;
    }
    /*
       [1] -> [2] -> [3] ->x
     */
    public void print(){
        MyNode curr = first;
        while(curr != null){
            pl(curr.name);
            curr = curr.next;
        }
    }
}

// Represent column vector2
class Vector2{
    Double x;
    Double y;
    Point2<Double> p1;
    Point2<Double> p2;
    // p1 -> p2
    public Vector2(Point2<Double> p1, Point2<Double> p2){
        x = p2.x - p1.x;
        y = p2.y - p1.y;
    }
}

// class Node{
// Node left;
// Node right;
// Integer data;
// }

class Person{
    String name;
    Integer age;
    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
    }
    @Override
        public String toString(){
            return "name=" + name + " age=" + age.toString();
        }
}



public class try1{
    public static void main(String[] args) {
        // test0();
        // genePoints();
        // test1();
        // test2();

        // test4();
        // test5();
        // test7();
        // test8();
        test6();
    }

    /**

      v1 = [a1, 
      b1]
      v2 = [a2
      b2]

     */
    public static Double determinant2(Vector2 v1, Vector2 v2){
        return v1.x * v2.y - v1.y * v2.x;
    }

    // Vector2: p1 -> p2
    public static Vector2 segmentToVector2(Segment<Double> seg){
        return new Vector2(seg.p1, seg.p2);
    }
    public static Boolean lineIntersected(Segment s1, Segment s2){
        Boolean ret = true;

        return ret;
    }

    public static Boolean doesSegmentIntersected(Segment s1, Segment s2){
        Boolean ret = true;

        return ret;
    }

    public static Segment<Integer> tupleToSegment(Tuple<Point2, Point2> tup){
        return new Segment(tup.x, tup.y);
    }

    public static Tuple<Point2, Point2> segmentToTuple(Segment seg){
        return tuple(seg.p1, seg.p2);
    }

    public static void η(int k){
        pp(k);
    }

    /*
       public static void rotateArray(int[][] arr){
       if(arr != null){
       int len = arr.length;
       for(int k = 0; k<len/2; k++){
       for(int j=k; j<len-1-k; j++){
       int tmp = arr[k][j];
       arr[k][j] = arr[len-1-j][k];
       arr[len-1-j][k] = arr[len-1-k][len-1-j];
       arr[len-1-k][len-1-j] = arr[j][len-1-k];
       arr[j][len-1-k] = tmp;
       }
       }
       }
       }
     */



    // public static List<Tuple<Point2, Point2>> unique(List<Tuple<Point2, Point2>> ls){
    // var s1 = groupBy((a, b) -> a.x.equals(b.x), ls);
    // var s2 = groupBy((a, b) -> a.y.equals(b.y), ls);
    // return s2;
    // }
    // p0 -> {p1, p2 ...}
    // p1 -> {p0, p2 ...}
    public static void genePoints(){
        List<Double> p1 = map(x -> ((double)x)/10, geneRandomIntFromTo(0, 5, 5));
        List<Double> p2 = map(x -> ((double)x)/10, geneRandomIntFromTo(0, 5, 5));
        // var lss = outer((x, y) -> triple(x, y, 0.0), p1, p2);
        var ptls = zipWith_new((x, y) -> new Point2(x, y, 0.0), p1, p2);


        // var fls = flatMap(lss);
        var seglss = outer((x, y) -> tuple(x, y), ptls, ptls);
        pp(ptls);
        fl2(4);
        pp(seglss);
        var ffls = flatMap(seglss);
        var trils = filter(e -> !e.x.equals(e.y), map(t -> triple(t.x, t.y, norm(t.x, t.y)), ffls));
        fl2(4);
        pp(ffls);
        var distls = filter(x -> x > 0, map(t -> norm(t.x, t.y), ffls));
        fl("distls");
        pp(distls);
        var sortls = sort(trils, (x, y) -> x.z.compareTo(y.z));
        fl("distls");
        pp(sortls);

        // fl("sortls");
        // 	pp(sortls);


        // var flss = map(t -> ll(t.x.toStrSPC(), t.y.toStrSPC()), ffls);
        // var fstr = map(x -> x + "\n", flatMap(flss));
        // fl2(4);
        // pp(fstr);

        // List<String> ls = new ArrayList<>();
        // for(int i = 0; i < len(p1); i++){
        // String str = drop(1, concat(" ", ll(toStr(p1.get(i)), toStr(p2.get(i)), "0.0", "\n")));
        // ls.add(str);             
        // }
        // var ss = cat(ll("point:\n"), ls, ll("endpoint:\n"));
        // var xfile = getEnv("b") + "/tmp/draw.x";
        // writeFileList(xfile, ss);

        // var xfile2 = getEnv("b") + "/tmp/draw.x";
        // var wfss = cat(ll("segment:\n"), fstr, ll("endsegment:"));
        // writeFileList(xfile2, wfss);
    }


    public static Boolean matchCPPFunction(String s){
        Boolean ret = isMatchedGroup("(^[a-zA-Z0-9_<>]+\\s+[a-zA-Z0-9_]+\\([^)}{]*\\))", s);
        return ret;
    }
    public static void test0(){
        String bitbucket = getEnv("b");
        List<String> ls = readFile(bitbucket + "/cpplib/AronLib.h");
        // List<String> ls = readFile("/tmp/x.x");

        List<String> s1 = init(ls);
        fl("s1");
        pp(s1);
        List<String> s2 = tail(ls);
        fl("s2");
        pp(s2);

        List<Tuple<String, String>> zs = zip(s1, s2);
        fl("zs");

        /**
          a b c d

          a b c
          b c d
          (a, b)
          (b, c)
          (c, d)
         */
        for(Tuple<String, String> tup : zs){
            Boolean b1 = isMatchedGroup("(^template<[^>]*>)", tup.x);
            Boolean b2 = isMatchedGroup("(^[a-zA-Z0-9_<>]+\\s+[a-zA-Z0-9_]+\\([^)}{]*\\))", tup.y);
            if(b1 && b2){
                fl("template and function");
                pp(tup);
            }else if(b1){
                fl("template only");
                pp(tup.x);
            }else if(b2){
                fl("function only");
                pp("FUN: " + tup.y);
            }
        }

        // isMatched("^[a-zA-Z0-9_<>]+\\s*[a-zA-Z0-9_]+\\([^)]*\\)", "vector<int> funKK()\\s*{")

        pp(zs);
    }

    /**
      2 4 1 0 5 3

      3
      2 
      4
      1
      0 
      5
      3 

      1. one element 
      [2] => return i == j
      [2, 3]
      i  j

      [2][3  ]
      i j 

      [2, 3] => 1

     */
    public static Integer partition(Integer[] arr, Integer lo, Integer hi){
        Integer[] tmpArr = new Integer[hi - lo + 1];
        int i = 0;
        int j = hi - lo;
        int k = lo;
        int pivot = arr[j];
        while(i != j){
            if(arr[k] <= pivot){
                tmpArr[i] = arr[k];
                i++;
            }else{
                tmpArr[j] = arr[k];
                j--;
            }
            k++;
        }
        tmpArr[i] = pivot;

        for(int x = 0; x < (hi - lo + 1); x++){
            arr[x + lo] = tmpArr[x];
        }

        return i + lo;
    }

    public static void quickSort3(Integer[] arr, Integer lo, Integer hi){
        if(lo < hi){
            Integer pivoInx = partition(arr, lo, hi);
            quickSort3(arr, lo, pivoInx - 1);
            quickSort3(arr, pivoInx + 1, hi);
        }
    }

    /**
      8
      6          10
      2      7     9      12


     */
    public static boolean isBST(Node root){
        if(root != null){
            if(!isBST(root.left))
                return false;

            if(root.left != null && maxLeft(root.left).data >= root.data)
                return false;
            if(root.right != null && minRight(root.right).data <= root.data)
                return false;

            if(!isBST(root.right))
                return false;
        }
        return true;
    }

    public static Node maxLeft(Node subLeft){
        while(subLeft.right != null){
            subLeft = subLeft.right;
        }
        return subLeft;
    }

    public static Node minRight(Node subRight){
        while(subRight.left != null){
            subRight = subRight.right;
        }
        return subRight;
    }


    /**
      3 => 0

      3  
      2

     */
    public static Integer levelBin(Node root){
        if(root != null){
            Integer l = levelBin(root.left);
            Integer r = levelBin(root.right);
            return Math.max(l, r) + 1;
        }
        return 0;
    }

    public static Integer hBin(Node root){
        return levelBin(root) - 1;
    }


    public static Integer strToDecimal(String s){
        if(s != null){
            Integer sum = 0;
            int linx = len(s) - 1;
            for(int i = linx; i >= 0; i--){
                sum += (s.charAt(i) - '0') * (int)Math.pow(10, linx - i);
            }
            return sum;
        }else{
            throw new IllegalArgumentException("Invalid argument");
        }
    }
    public static Integer[] reverse(Integer[] arr){
        if(arr != null){
            int linx = arr.length - 1;
            for(int i = 0; i < arr.length/2; i++){
                Integer tmp = arr[i];
                arr[i] = arr[linx - i];
                arr[linx - i] = tmp;
            }
        }
        return arr;
    }


    /**
      (1)(0)
      (1)9  7
      9 8  6
      ---------
      10 8  3

     */
    public static Integer[] addArr(Integer[] arr, Integer[] brr){
        int la = arr.length;
        int lb = brr.length;
        int max = Math.max(la, lb) + 1;
        Integer[] aa = new Integer[max];
        Integer[] bb = new Integer[max];
        Integer[] ret = new Integer[max];

        for(int i = 0; i < max; i++){
            int diffa = max - arr.length;
            if(i < diffa){
                aa[i] = 0;
            }else{
                aa[i] = arr[i - diffa];
            }

            int diffb = max - brr.length;
            if(i < diffb){
                bb[i] = 0;
            }else{
                bb[i] = brr[i - diffb];
            }
        }

        int c = 0;
        for(int i = max - 1; i >= 0; i--){
            int n = aa[i] + bb[i] + c;
            int r = n % 10;
            c = n / 10;
            ret[i] = r;
        }

        return ret;
    }

    public static List<Integer> addList(List<Integer> s1, List<Integer> s2){
        List<Integer> ret = new ArrayList<>();
        Integer len1 = s1.size();
        Integer len2 = s2.size();
        List<Integer> ss1 = new ArrayList<>();
        List<Integer> ss2 = new ArrayList<>();


        Integer diff = Math.abs(len1 - len2) + 1;
        List<Integer> rep = IntStream.range(0, diff).mapToObj(x -> 0).collect(Collectors.toList());
        if(len1 > len2){
            ss2.addAll(rep);
            ss2.addAll(s2);
            ss1.add(0);
            ss1.addAll(s1);
        }else{
            ss1.addAll(rep);
            ss1.addAll(s1);
            ss2.add(0);
            ss2.addAll(s2);
        }
        Integer carrying = 0;

        for(int i = ss1.size() - 1; i >= 0 ; i--){
            Integer n1 = ss1.get(i);
            Integer n2 = ss2.get(i);
            Integer sum = n1 + n2 + carrying;
            carrying = sum / 10;
            Integer r = sum % 10;
            ret.add(r);
        }

        Collections.reverse(ret);
        return ret;
    }

    public static List<Integer> add(List<Integer> ls, Integer n){
        List<Integer> ret = new ArrayList<>();
        ret.addAll(ls);
        ret.add(n);
        return ret;
    }
    public static List<Integer> rep(Integer n){
        if(n > 0){
            return add(rep(n - 1), 0);
        }
        return new ArrayList<>();
    }
    public static List<Integer> repeat(Integer n){
        List<Integer> ls = new ArrayList<>();
        for(int i = 0; i < n; i++){
            ls.add(0);
        }
        return ls;
    }

    public static void addToPriorityQueue(List<Person> ls){
        PriorityQueue<Person> q = new PriorityQueue<Person>((x, y) -> -(x.age - y.age));
        for(Person p : ls){
            q.add(p);
        }

        int k = 3;

        while(k > 0){
            Person person = q.remove();
            System.out.println("name=" + person.name + "age=" + toStr(person.age));
            k--;
        }
    }

    /*
       0 1 1 1 0
       1 0 1 0 0
       0 1 0 0 0
       0 1 1 1 1

     */
    public static Integer mymaxConnected(int[][] arr, int height, int width, int h, int w){
        if(arr[h][w] == 1){
            arr[h][w] = 0;
            int c0 = 0, c1 = 0, c2 = 0, c3 = 0;
            if(h + 1 < height)
                c0 = mymaxConnected(arr, height, width, h + 1, w);
            if(h - 1 >= 0)
                c1 = mymaxConnected(arr, height, width, h - 1, w);
            if(w + 1 < width)
                c2 = mymaxConnected(arr, height, width, h, w + 1);
            if(w - 1 >= 0)
                c3 = mymaxConnected(arr, height, width, h, w - 1);

            arr[h][w] = 1;

            return c0 + c1 + c2 + c3 + 1;
        }
        return 0;
    }

    public static Integer maxNum(int[][] arr, int height, int width, int h, int w){
        int max = 0;
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                int n = mymaxConnected(arr, height, width, i, j);
                if(max < n)
                    max = n;
            }
        }
        return max;
    }


    public static void test1(){
        beg();
        {
            {
                Integer n = strToDecimal("0");
                t(n, 0);
            }
            {
                Integer n = strToDecimal("1");
                t(n, 1);
            }
            {
                Integer n = strToDecimal("11");
                t(n, 11);
            }
            {
                Integer n = strToDecimal("100");
                t(n, 100);
            }
        }
        {
            fl("--");
            {
                Node root = geneBinNoImage();
                Integer h = hBin(root);
                t(h, 2);
            }
            {
                Integer[] arr = {1, 2, 3};
                Integer[] rarr = reverse(arr);
                Integer[] exparr = {3, 2, 1};
                t(rarr, exparr);
            }
        }
        {
            /**
              9 9
              9 9
              ------
              10 8
             */
            Integer[] arr = {9, 9};
            Integer[] brr = {9, 9};
            Integer[] ret = addArr(arr, brr);
            Integer[] expret = {1, 0, 8};
            t(ret, expret);
            printArr(ret);
            printArr(expret);
            fl2(4);

        }
        {
            /**
              9 9
              9 9
              ------
              19 8
             */
            Integer[] arr = {0};
            Integer[] brr = {9, 9};
            Integer[] ret = addArr(arr, brr);
            Integer[] expret = {0, 9, 9};
            t(ret, expret);
            printArr(ret);
            printArr(expret);
            fl2(4);

        }
        {
            /**
              9 9
              9 9
              ------
              19 8
             */
            Integer[] arr = {9, 9};
            Integer[] brr = {0};
            Integer[] ret = addArr(arr, brr);
            Integer[] expret = {0, 9, 9};
            t(ret, expret);
            printArr(ret);
            printArr(expret);
            fl2(4);

        }
        {
            /**
              9 9
              9 9
              ------
              19 8
             */
            Integer[] arr = {9, 9};
            Integer[] brr = {9};
            Integer[] ret = addArr(arr, brr);
            Integer[] expret = {1, 0, 8};
            t(ret, expret);
            printArr(ret);
            printArr(expret);
            fl2(4);

        }
        {
            fl2(4);
            {
                List<Person> ls = new ArrayList<>();
                ls.add(new Person("David", 10));
                ls.add(new Person("Jame", 1));
                ls.add(new Person("Jonny", 30));
                ls.add(new Person("Kenny", 2));

                addToPriorityQueue(ls);
            }
        }
        end();
    }

    public static Integer sumList(Integer[] arr){
        Integer s = 0;
        for(int i = 0; i < arr.length; i++){
            s += arr[i];
        }
        return s;
    }

    public static void test2(){
        beg();
        {
            Integer[] arr = {1, 2, 3};
            Integer s = sumList(arr);
            t(s, 6);
        }
        end();
    }
    public static void test3(){
        beg();
        {
            String dir	= "h";     // haskell directory
            String fileType = "\\.hs";
            String key	= "KEY:";
            String path     = getEnv("h");
            List<FileKeyPattern> ls = getFileKeyPattern(path, fileType, key);
            for(FileKeyPattern k : ls){
                pl(k.toString());
            }
        }
        end();
    }
    public static void test4(){
        beg();
        {
            String s = "a12bc01kk001ee1";
            Integer n = countNumberFromAString(s);
            t(n, 2);
        }
        {
            String s = "001ee1";
            Integer n = countNumberFromAString(s);
            t(n, 1);
        }
        {
            String s = "ee001ee1e";
            Integer n = countNumberFromAString(s);
            t(n, 1);
        }
        {
            String s = "0001";
            Integer n = countNumberFromAString(s);
            t(n, 1);
        }
        {
            String s = "00a0";
            Integer n = countNumberFromAString(s);
            t(n, 1);
        }
        end();
    }


    public static List<FileKeyPattern> getFileKeyPattern(String path, String strRex, String key){
        List<FileKeyPattern> lsKey = list();
        List<String> ls = readDirRegex(path, strRex);
        for(String p : ls){
            List<String> buf = readFileNoTrim(p);
            FileKeyPattern fp = new FileKeyPattern();
            fp.fileName = p;
            for(String b : buf){
                if(containStr(b, key)){
                    fp.keyList.add(b);
                }
            }
            if(len(fp.keyList) > 0){
                lsKey.add(fp);
            }
        }
        return lsKey;
    }
    public static Integer countNumberFromAString(String s){
        Set<Integer> set = new HashSet<>();
        if(s != null){
            String[] arr = s.split("[a-z]+");
            for(String str : arr){
                pl(str);
                if(len(str.trim()) > 0){
                    set.add(Integer.parseInt(str));
                }
            }
        }
        return set.size();
    }
    public static void test5(){
        beg();
        {
            {
                Integer[] A = { 1, 2, 3};
                int k = 2;

                /* If the array contains repeated elements, sort it to handle
                   duplicate combinations */

                // Arrays.sort(A);

                // process elements from left to right
                /*
                   List<Integer> ls = new ArrayList<>();
                   combination(A, 0, A.length, k, ls);
                 */
            }
            {
                List<Integer> ls = list(1, 2, 4);
                minimalHeaviestSetA(ls);
            }
        }
        {
            {
                String s = "ab";
                String prefix = "";
                List<String> ls = list();
                Set<String> set = new HashSet<>();
                permutation(s, prefix, ls, set);
                fl2(4);
            }
            {
                String s = "abc";
                String prefix = "";
                List<String> ls = list();
                Set<String> set = new HashSet<>();
                permutation(s, prefix, ls, set);
                fl2(4);
                pl(ls);
            }
            {
                String s = "abcd";
                String prefix = "";
                List<String> ls = list();
                Set<String> set = new HashSet<>();
                permutation(s, prefix, ls, set);
                fl2(4);
                pl(ls);
            }
            {
                {
                    fl("insertionSort");
                    List<Integer> ls = list(3);
                    insertionSort(ls);
                    pl(ls);
                }
                {

                    fl("insertionSort");
                    List<Integer> ls = list(3, 1);
                    insertionSort(ls);
                    pl(ls);

                }
                {
                    fl("insertionSort");
                    List<Integer> ls = list(3, 1, 2);
                    insertionSort(ls);
                    pl(ls);

                }
                {
                    fl("insertionSort2");
                    List<Integer> ls = list(4, 3, 2, 1);
                    insertionSort2(ls);
                    t(ls, list(1, 2, 3, 4));
                }
            }
            {
                {
                    fl("Set to List");
                    Set set = new HashSet<Integer>();
                    set.add(1);
                    set.add(2);
                    set.add(3);
                    List<Integer> ls = new ArrayList<>(set);
                    pl(ls);
                }
                {
                    fl("list to set");
                    List<Integer> ls = list(1, 2, 3, 4);
                    Set set = new HashSet<>(ls);
                    pl(set);
                }
                {
                    fl("Collections");
                    List<Integer> ls = new ArrayList<Integer>();
                    ls.add(4);
                    ls.add(3);
                    ls.add(2);
                    ls.add(1);
                    Collections.sort(ls);
                    pl(ls);
                }
                {
                    fl("mytranspose");
                    Integer[][] m1 = { {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                    };
                    printArr2dInteger(m1);
                    fl("after mytranpose");
                    Integer[][] m = mytranspose(m1);
                    printArr2dInteger(m);
                }
                {

                    /*
                       [1, 2] [2
                       4]  = 2 + 8 = [0][0] = 10
                       [1, 2] [3
                       5] = 3 + 10 = [0][1] = 13
                       [3, 4] [2
                       4] = 6 + 16 = [1][0] = 22
                       [3, 4] [3
                       5] = 9 + 20 = [1][1] = 29
                     */
                    fl("multiMatrix");
                    Integer[][] m1 = { {1, 2},
                        {3, 4}
                    };
                    Integer[][] m2 = { {2, 3},
                        {4, 5}
                    };

                    /*
                       m2ᵀ = {{2, 4}
                       {3, 5}}
                     */

                    Integer[][] m = multiMatrix(m1, m2);
                    printArray2d(m);
                }
                {

                }
            }
            {
                fl("repeating");
                List<Integer> ls = repeating(3);
                pl(ls);
                List<Integer> lss = IntStream.range(0, 3).mapToObj(x -> 0).collect(Collectors.toList());
                for(Integer n: lss){
                    pl("n=" + n);
                }
            }
            {
                List<Integer> ls = new ArrayList<>();
                ls.add(1);
                ls.add(2);
                ls.add(3);
                List<Integer> lss = repeat3(3);
                pl(lss);
            }
            {
                fl("stream() filter");
                List<Integer> ls = list(1, 2, 3);
                List<Integer> lss = ls.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
                pl(lss);
            }
            {
                List<Integer> ls = list(1, 2, 3);
                List<Integer> lss = ls.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
                pl(lss);
            }
            {
                {
                    fl("coinChange");
                    List<Integer> ls = list(3, 2, 1);
                    List<Integer> sumls = new ArrayList<>();
                    Integer currSum = 0;
                    Integer total = 1;
                    coinChange(ls, total, currSum, sumls);
                }
                {
                    fl("coinChange");
                    List<Integer> ls = list(3, 2, 1);
                    List<Integer> sumls = new ArrayList<>();
                    Integer currSum = 0;
                    Integer total = 3;
                    coinChange(ls, total, currSum, sumls);
                }
                {
                    fl("coinChange");
                    List<Integer> ls = list(3, 2, 1);
                    List<Integer> sumls = new ArrayList<>();
                    Integer currSum = 0;
                    Integer total = 4;
                    coinChange(ls, total, currSum, sumls);
                }
            }
            {
                {
                    fl("coinChangeMiniCoin");
                    List<Integer> ls = list(3, 2, 1);
                    List<List<Integer>> lss = new ArrayList<>();
                    List<Integer> sumls = new ArrayList<>();
                    Integer currSum = 0;
                    Integer total = 3;
                    coinChangeMiniCoin(ls, total, currSum, sumls, lss);
                    pp(lss);

                }
            }
            {
                fl("htmlTable");
                List<List<String>> lss = list(list("dog", "cat"), list("pig", "fox"));
                String ss = htmlTable(lss, "");
                pp(ss);
            }
        }
        {

        }

        end();
    }

    public static List<Integer> repeat3(Integer n){
        List<Integer> ls = new ArrayList<>();
        for(int i = 0; i < n; i++){
            ls.add(0);
        }
        return ls;
    }

    public static List<Integer> repeat2(Integer n){
        return IntStream.range(0, n).mapToObj(x -> x).collect(Collectors.toList());
    }
    public static List<Integer> repeating(Integer n){
        List<Integer> ret = IntStream.range(0, n).mapToObj(x -> 0).collect(Collectors.toList());
        return ret;
    }

    /*
       1 2 3 4
       1 2 3 4
       1 2 3 4
       1 2 3 4

     */
    public static Integer[][] mytranspose(Integer[][] m){
        Integer[][] ret = new Integer[m.length][m[0].length];
        for(int i = 0; i < m.length; i++){
            for(int j = i+1; j < m[0].length; j++){
                Integer tmp = m[i][j];
                m[i][j] = m[j][i];
                m[j][i] = tmp;
            }
        }

        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m[0].length; j++){
                ret[i][j] = m[i][j];
            }
        }
        return ret;
    }

    public static List<Integer> addList(List<Integer> ls, Integer n){
        List<Integer> ret = new ArrayList<>();
        for(Integer e : ls){
            ret.add(e);
        }
        ret.add(n);
        return ret;
    }

    public static void combination(Integer[] A, int i, int n, int k, List<Integer> ls) {
        if (k > n) {
            return;
        }
        if (k == 0) {
            pp(ls);
            fl2(4);
            return;
        }

        for (int j = i; j < n; j++){
            combination(A, j + 1, n, k - 1, addList(ls, A[j]));
        }
    }

    /*
       a b c
       b c   a c    a b
       c   b  c  a   b    a

       abc
       ab
     */
    public static void permutation(String s, String prefix, List<String> ls, Set<String> set){
        if(s.length() == 0){
            pl("prefix=" + prefix);
        } else if(s.length() == 1){
            String st = sortStr(prefix);
            if(!set.contains(st)){
                set.add(st);
                ls.add(prefix);
            }
        }else{
            for(Integer i : range(0, len(s))){
                String ss = removeIndex(i, s);
                permutation(ss, prefix + (s.charAt(i) + ""), ls, set);
            }
        }
    }

    /*
KEY: inesrtion sort, 

4 3 2 1
↑
3 4       ← swap
3 4 2 1
↑
3 2 4     ← swap
↑
2 3       ← swap
2 3 4 1
↑
1 4   ← swap
2 3
↑
1 3     ← swap
↑
1 2       ← swap
1 2 3 4

     */
    public static void insertionSort(List<Integer> ls){
        for(int i = 1; i < len(ls); i++){
            for(int j = i; j >= 1; j--){
                if(ls.get(j - 1) > ls.get(j)){
                    Integer tmp = ls.get(j-1);
                    ls.set(j-1, ls.get(j));
                    ls.set(j, tmp);
                }
            }
        }
    }

    /*
       4 3 2 1
       x
       3 4 2 1
       x
       2 4
       2 3 4 1
       x
       1 4
       1 3
       1 2

     */
    public static void insertionSort2(List<Integer> ls){
        for(int i = 1; i < ls.size(); i++){
            for(int j = 0; j < i; j++){
                if(ls.get(i - 1) > ls.get(i)){
                    Integer tmp = ls.get(i-1);
                    ls.set(i - 1, ls.get(i));
                    ls.set(i, tmp);
                }
            }
        }
    }



    public static List<Integer> minimalHeaviestSetA(List<Integer> ls) {
        List<Integer> ret = new ArrayList<>();
        return ret;
    }


    /*
       s = "kaaaabbbcc" => "a4b3"
       a4b3 => aaaabbb

       s = "abc" => a1b1c1

       abbcccd
       x
       x
     */
    public static String compress(String s){
        String accumulator = "";
        if(s.length() > 0){
            char c = s.charAt(0);
            Integer count = 1;
            char c0 = '0';
            char c1 = '0';
            for(int i = 1; i < s.length(); i++){
                c0 = s.charAt(i-1);
                c1 = s.charAt(i);
                if(c0 != c1){
                    accumulator += (c0 + "") + (count + "");
                    count = 1;
                }else{
                    count++;
                }
            }
            accumulator += (c1 + "") + (count + "");
        }
        return accumulator;
    }

    /*
       ab123cc12 => 123
       "1"
       "2"
       "3"
       => "123"
     */
    public static List<Integer> extractNum(String s){
        String accumulator = "";
        List<Integer> ls = new ArrayList<>();
        for(int i = 1; i < s.length(); i++){
            char c = s.charAt(i);
            if(Character.isDigit(c)){
                accumulator += (c + "");
            }else{
                if(accumulator.length() > 0){
                    ls.add(Integer.parseInt(accumulator));
                    accumulator = "";
                }
            }

        }
        if(accumulator.length() > 0){
            ls.add(Integer.parseInt(accumulator));
        }
        return ls;
    }

    /*
       Apple => Michael, Cherry
       Banana => Avocada, Abaca
       Cherry => Orange, ZheFruit
     */
    public static void myGraph(){
        Map<MyNode, List<MyNode>> graph = new HashMap<>();
        List<MyNode> s1 = list(new MyNode("Michael"), new MyNode("Cherry"));
        List<MyNode> s2 = list(new MyNode("Avocada"), new MyNode("Abaca"));
        MyNode n1 = new MyNode("Apple");
        MyNode n2 = new MyNode("Banana");
        graph.put(n1, s1);
        graph.put(n2, s2);
    }
    /*
a:1  2   6
[2] 2   2  
3   [3] 3  
4   4   [4]


     */
    public static Integer[] multiplyAll(Integer[] arr){
        Integer[] a = new Integer[arr.length];
        Integer[] b = new Integer[arr.length];
        a[0] = 1;
        b[arr.length - 1] = 1;

        int maxInx = arr.length - 1;
        for(int i = 1; i < arr.length; i++){
            a[i] = a[i-1]*arr[i-1]; // index
            b[maxInx - i] = b[maxInx - (i - 1)]*arr[maxInx - (i - 1)]; // reverse index
        }
        for(int i = 0; i < arr.length; i++){
            a[i] = a[i] * b[i];
        }
        return a;
    }

    /*
       a1b2c3 => abbc3
       a12b1 => a..ab
     */
    public static String uncompress(String s){
        return "";
    }
    /*
       s1 = "123"
       s2 = " 99"
       ----------
       2  c = 1
       2   c = 1
       2    c = 0

       123 -> 00123
     */
    public static Integer sumStr2(String s1, String s2){
        List<Integer> ss1 = strnumToArr(s1);
        List<Integer> ss2 = strnumToArr(s2);
        List<Integer> padls1 = new ArrayList<>();
        List<Integer> padls2 = new ArrayList<>();
        List<Integer> numls = new ArrayList<>();
        Integer len1 = ss1.size();
        Integer len2 = ss2.size();
        if(len1 > len2){
            Integer diff = len1 - len2;
            for(int i = 0; i < diff + 1; i++){
                padls2.add(0);
            }
            padls2.addAll(ss2);

            padls1.add(0);
            padls1.addAll(ss1);
        }else{
            Integer diff = len2 - len1;
            for(int i = 0; i < diff + 1; i++){
                padls1.add(0);
            }
            padls1.addAll(ss1);

            padls2.add(0);
            padls2.addAll(ss2);
        }

        Integer carrying = 0;
        pl(padls1);
        pl(padls2);
        for(int i = padls1.size() - 1; i >= 0; i--){
            Integer sum = padls1.get(i) + padls2.get(i) + carrying;
            carrying = sum / 10;
            Integer r = sum % 10;
            pl("r=" + r);
            numls.add(r);
        }
        Collections.reverse(numls);
        fl2(4);
        pp(numls);
        Integer sum = 0;
        Integer maxInx = numls.size() - 1;
        for(int i = 0; i < numls.size(); i++){
            Integer coefficient = numls.get(maxInx - i);
            Integer p = (int)pow(10, i);
            sum += coefficient*p;
        }


        return sum;
    }


    /*
       a -> 1
       ab -> 2
     */
    public static Integer repeatCount(List<String> ls){
        Map<String, Integer> map = new HashMap<>();

        for(String s : ls){
            Integer v = map.get(s);
            if(v == null){
                map.put(s, 1);
            }else{
                map.put(s, v + 1);
            }
        }
        Integer max = 0;
        for(Map.Entry<String, Integer> entry : map.entrySet()){
            pl("key=" + entry.getKey() + " value=" + entry.getValue());
            if(max < entry.getValue()){
                max = entry.getValue();
            }
        }
        return max;
    }
    public static Integer sumStr(String s1, String s2){
        return s2n(s1) + s2n(s2);
    }
    public static Integer s2n(String s){
        Integer sum = 0;
        for(int i = s.length() - 1; i >= 0; i--){
            int c = s.charAt(i) - '0';
            int p = s.length() - 1 - i;
            int po = (Integer)pow(10, p);
            sum += c*po;
        }
        return sum;
    }
    //  ab c 12ab44 3303 00ab43 abc
    public static List<Integer> allNumber(String str){
        List<Integer> ret = new ArrayList<>();
        String[] arr = str.split("\\s+");
        for(String s : arr){
            // pl("s=" + s);
            String[] numArr = s.split("[a-zA-Z]+");
            for(String n : numArr){
                pl("n=" + n);
                if(n.trim().length() > 0){
                    pl("num = " + n);
                    ret.add(Integer.parseInt(n));
                }
            }
        }
        return ret;
    }
    //  1 2 3 =  10^0 * 3 + 10^1 * 2 + 10^2 * 1
    public static Integer strToNum(String s){
        Integer ret = 0;

        int maxInx = s.length() - 1;
        for(int i = maxInx; i >= 0; i--){
            pl(s.charAt(i));
            int c = s.charAt(i) - '0';
            Integer p = (Integer)pow(10, maxInx - i);
            ret += p*c;
        }
        return ret;
    }

    public static List<Integer> strnumToArr(String s){
        List<Integer> ls = new ArrayList<>();
        for(int i = 0; i < s.length(); i++){
            int c = s.charAt(i) - '0';
            ls.add(c);
        }
        return ls;
    }





    /*
       x x x
       2 4 9
       1 2 7
       x x x x

       1 2 2 4 7 9

     */
    public static List<Integer> merge(ArrayList<Integer> s1, ArrayList<Integer> s2){
        int i = 0;
        int j = 0;
        ArrayList<Integer> ret = new ArrayList<>();
        while(i < s1.size() || j < s2.size()){

            if(i >= s1.size()){
                ret.add(s2.get(j));
                j++;
            }else if(j >= s2.size()){
                ret.add(s1.get(i));
                i++;
            }else{
                if(s1.get(i) < s2.get(j)){
                    ret.add(s1.get(i));
                    i++;
                }else{
                    ret.add(s2.get(j));
                    j++;
                }
            }
        }
        return ret;
    }

    /*
       k = 1,

       1 2

     */
    public static Boolean binSearch(Integer[] arr, Integer k, Integer lo, Integer hi){
        if(lo < hi){
            Integer mid = (lo + hi)/2;
            if(k < arr[mid]){
                return binSearch(arr, k, lo, mid - 1);
            }else if(k > arr[mid]){
                return binSearch(arr, k, mid + 1, hi);
            }else{
                return true;
            }
        }else{
            return arr[lo] == k;
        }
    }

    /*
       2 3 1

       2 3 1        2 3 1       2 3 1

       2 3 1  2 3 1  2 3 1

     */
    public static void coinChange(List<Integer> ls, Integer total, Integer currSum, List<Integer> sumls){
        for(int i = 0; i < ls.size(); i++){
            if(currSum + ls.get(i) == total){
                pl(append(sumls, ls.get(i)));
                fl2(4);
            }else if(currSum < total){
                coinChange(ls, total, currSum + ls.get(i), append(sumls, ls.get(i)));
            }
        }
    }

    public static void coinChangeMiniCoin(List<Integer> ls, Integer total, Integer currSum, List<Integer> sumls, List<List<Integer>> lss){
        for(int i = 0; i < ls.size(); i++){
            if(currSum + ls.get(i) == total){
                List<Integer> tmpls = append(sumls, ls.get(i));
                lss.add(tmpls);
            }else if(currSum + ls.get(i) < total){
                coinChangeMiniCoin(ls, total, currSum + ls.get(i), append(sumls, ls.get(i)), lss);
            }
        }
    }

    /*
       <pre>
KEY:
{@literal
}
{@code
String style = "background:green;";
List<List<String>> lss = list(list("dog", "cat"), list("pig", "fox"));

htmlTable(lss, style)
}
</pre>
     */
public static String htmlTable(List<List<String>> lss, String style){
    String tableS = "<table>";
    String tablecS = "</table>";
    String td = "<td style='" + style + "'>";
    String tdc = "</td>";
    String tr = "<tr>";
    String trc = "</tr>";

    List<String> lss1 = map(ls -> tr + concat("", map(x -> td + x + tdc, ls)) + trc, lss);
    pp(lss1);
    fl2(4);
    return tableS + concat("", lss1) + tablecS;
}

public static List<Integer> append(List<Integer> ls, Integer n){
    List<Integer> ret = new ArrayList<>(ls);
    ret.add(n);
    return ret;
}
public static List<Integer> rmInx(List<Integer> ls, Integer inx){
    List<Integer> ret = new ArrayList<>();
    for(int i = 0; i < ls.size(); i++){
        if(i != inx)
            ret.add(ls.get(i));
    }
    return ret;
}

/*
   1 2 3
   1          (3, 1)
   2        
   3
   1 2        (3, 2)
   2 3
   1 3
   1 2 3      (3, 3)


   1
   1   1
   1     2     1
   1     3     3     1
   1     4     6     4      1
   (n, k) = (n-1, k) + (n-1, k-1)
   (3, 2) = (2, 2) + (2, 1)

   (4, 3) = (3, 3) + (3, 2)
   (4, 2) = (3, 1) + (3, 2) = 3 + 3 = 6

 */
public static void nchoosek(List<Integer> ls, List<Integer> prefix, Integer k, List<List<Integer>> answer, Set<List<Integer>> set){
    if(prefix.size() == k){
        pp(prefix);

        List<Integer> ss = new ArrayList<Integer>(prefix);
        Collections.sort(ss);
        if(!set.contains(ss)){
            answer.add(prefix);
        }else{
            set.add(ss);
        }
        fl2(4);
    }else{
        for(int i = 0; i < ls.size(); i++){
            Integer rNum = ls.get(i);
            nchoosek(rmInx(ls, i), append(prefix, rNum), k, answer, set);
        }
    }
}

/*      x x x x
        1 2 3 4
        x x    x
        2 3 4  3 4  4
        x	
        3 4   4     x

        4   


 */
public static void nchoosek2(List<Integer> ls, List<Integer> prefix, Integer k){
    if(prefix.size() == k){
        pl(prefix);
        fl2(4);
    }else{
        for(int i = 0; i < ls.size(); i++){
            nchoosek2(drop(i+1, ls), append(prefix, ls.get(i)), k);
        }
    }
}

public static void test7(){
    beg();
    {
        {
            List<Integer> ls = list(1, 2, 3);
            List<Integer> prefix = new ArrayList<>();
            List<List<Integer>> answer = new ArrayList<>();
            Set<List<Integer>> set = new HashSet<>();
            Integer k = 1;
            nchoosek(ls, prefix, k, answer, set);
            pl(answer);
        }
        {
            fl("n choose 1");
            List<Integer> ls = list(1, 2, 3);
            List<Integer> prefix = new ArrayList<>();
            List<List<Integer>> answer = new ArrayList<>();
            Set<List<Integer>> set = new HashSet<>();
            Integer k = 1;
            nchoosek(ls, prefix, k, answer, set);
            pl(answer);
        }
        {
            fl("n choose 2");
            List<Integer> ls = list(1, 2, 3);
            List<Integer> prefix = new ArrayList<>();
            List<List<Integer>> answer = new ArrayList<>();
            Set<List<Integer>> set = new HashSet<>();
            Integer k = 2;
            nchoosek(ls, prefix, k, answer, set);
            pl(answer);
        }
        {
            fl("nchoosek2");
            List<Integer> ls = list(1, 2, 3);
            List<Integer> prefix = list();
            Integer k = 2;
            nchoosek2(ls, prefix, k);
        }
    }
    {
        {
            fl("hexStrToInt");
            String s = "0";
            Integer n = hexStrToInt(s);
            pl(n == 0);
        }
        {
            fl("hexStrToInt");
            String s = "1";
            Integer n = hexStrToInt(s);
            pl(n == 1);
        }
        {
            fl("hexStrToInt");
            String s = "A";
            Integer n = hexStrToInt(s);
            pl(n == 10);
        }
        {
            fl("hexStrToInt");
            String s = "F";
            Integer n = hexStrToInt(s);
            pl(n == 15);
        }
        {
            //  15*16 + 15 = 15 * 15 + 15 =  150 + 75 + 15 + 15 = 225 + 30 = 255
            fl("hexStrToInt");
            String s = "FF";  // 2^4 2^4 2^4 2^4 = FFFF
            Integer n = hexStrToInt(s);
            pl(n == 255);
        }
        {
            Integer n = hexStrToInt("2206");
            pl("n=" + n);
        }
    }
    end();
}

public static void test8(){
    beg();
    {
        {
            fl("intToList");
            Integer n = 0;
            List<Integer> ls = intToList(n);
            pl(ls);
        }
        {
            fl("intToList");
            Integer n = 123;
            List<Integer> ls = intToList(n);
            pl(ls);
        }

    }
    {
        {
            fl("extractStr");
            String s = "<<ab>>{<123b>}{44}";
            List<String> ls = extractStr(s);
            pl(ls);
        }
        {
            fl("extractStr");
            String s = "<<ab]>{<123b>}{44}";
            List<String> ls = extractStr(s);
            pl(ls);
        }
        {
            fl("extractStr");
            String s = "<{]>{<>}{}";
            List<String> ls = extractStr(s);
            pl(ls);
        }
    }
    {
        {

            fl("rotate90");
            Integer[][] arr = {
                { 0,   0,   1,  0},
                { 0,   0,   1,  1},
                { 1,   1,   0,  0},
                { 0,   1,   0,  0},
            };
            Integer[][] ret = rotate90(arr);
            printArray2d(arr);
            fl();
            printArray2d(ret);
        }
    }
    {
        {
            fl();
            List<Integer> ls = list(1, 2, 3);
            List<Integer> lss = swapList(ls);
            pl(lss);
        }
        {
            fl();
            List<Integer> ls = list();
            List<Integer> lss = swapList(ls);
            pl(lss);
        }
        {
            fl();
            List<Integer> ls = list(1);
            List<Integer> lss = swapList(ls);
            pl(lss);
        }
        {
            fl();
            List<Integer> ls = list(1, 2);
            List<Integer> lss = swapList(ls);
            pl(lss);
        }
    }
    {
        {
            fl("findMissingInteger");
            Integer[] arr = {-2, 3, 4, 7};
            Integer n = findMissingInteger(arr);
            pl("n=" + n + " " + (n == 1));
        }
        {
            fl("findMissingInteger");
            Integer[] arr = {1, 2, 3};
            Integer n = findMissingInteger(arr);
            pl("n=" + n + " " + (n == 4));
        }
        {
            fl("findMissingInteger");
            Integer[] arr = {3, 4, -1, 1};
            Integer n = findMissingInteger(arr);
            pl("n=" + n + " " + (n == 2));
        }
    }
    {
        Node root = new Node(10);
        root.left = new Node(4);
        root.right = new Node(14);
        Node n1 = new Node(5);
        addNode(root, n1);
        // printBinTree(root);

    }
    {
        fl("shuffle");
        {
            Integer[] arr = {};
            Integer[] array = shuffle(arr);
            printArr(array);
        }
        {
            Integer[] arr = {1};
            Integer[] array = shuffle(arr);
            printArr(array);
        }
        {
            Integer[] arr = {1, 2};
            Integer[] array = shuffle(arr);
            printArr(array);
        }
        {
            Integer[] arr = {1, 2, 3};
            Integer[] array = shuffle(arr);
            printArr(array);
        }
    }
    {
        {
            Node root = new Node(2004);
            addAllFile(root, "/tmp/");
            // printBinTree(root);
        }
    }
    {
        fl("differentElement");
        Integer[] arr = {5, 3, 1};
        Integer min = differentElement(arr);
        pl(min == 2);
    }
    {
        {
            fl("stack()");
            Stack<Integer> stack = new Stack<>();
            stack.add(1);
            stack.add(3);
            while(!stack.isEmpty()){
                Integer n = stack.pop();
                pl(n);
            }
        }
        {
            Queue<String> queue = new LinkedList<>();
            queue.add("David");
            queue.add("Michael");
            queue.add("ijk lmn opq, rst uvw xyz  ");
            while(!queue.isEmpty()){
                String s = queue.remove();
                pl(s);
            }
        }
        {
            PriorityQueue<String> queue = new PriorityQueue<>((x, y) -> y.compareTo(x));
            queue.add("a");
            queue.add("b");
            queue.add("o");
            queue.add("p");
            queue.add("q");
            while(!queue.isEmpty()){
                String s = queue.remove();
                pl(s);
            }
        }
        {
            fl("Customer 1");
            PriorityQueue<Customer> queue = new PriorityQueue<>((x, y) -> x.age - y.age);
            queue.add(new Customer("David", 10));
            queue.add(new Customer("Michael", 20));
            queue.add(new Customer("Tomy", 20));
            queue.add(new Customer("Tony", 4));
            while(!queue.isEmpty()){
                Customer c = queue.remove();
                pl(c.toString());
            }

        }
        {
            PriorityQueue<Customer> queue = new PriorityQueue<>((x, y) -> x.name.compareTo(y.name));

            fl("Customer 2");
            queue.add(new Customer("a", 4));
            queue.add(new Customer("c", 3));
            queue.add(new Customer("x", 40));
            queue.add(new Customer("y", 30));
            queue.add(new Customer("b", 3));
            while(!queue.isEmpty()){
                Customer c = queue.remove();
                pl(c.toString());
            }
        }
        {
            List<Integer> ls = list(1, 2, 4, 9, 10, 10, 3, 4);
            Map<Integer, Integer> map = new HashMap<>();
            for(Integer n : ls){
                Integer v = map.get(n);
                if(v != null){
                    map.put(n, v + 1);
                }else{
                    map.put(n, 1);
                }
            }
            pl(map);
        }
        {
            fl("Find the first missing non negative Integer");
            // 0 1 2 4 8 9
            // 0 1 2 3 4 5
            List<Integer> ls = list(0, 1, 9, 2, 4, 8);
            List<Integer> lss = ls.stream().map(x -> x + 1).collect(Collectors.toList());
            List<Integer> lst = ls.stream().filter(x -> x >= 0).collect(Collectors.toList());
            pp(lst);

            List<String> lt = list("David", "Michael", "Cherry", "Banana", "Apple");
            List<Integer> ltt = lt.stream().map(x -> len(x)).collect(Collectors.toList());
            pp(ltt);
            Collections.sort(lt, (x, y) -> x.compareTo(y));
            pp(lt);
            Collections.sort(lt, (x, y) -> y.compareTo(x));
            fl();
            pp(lt);
            List<Integer> lrr = IntStream.range(0, len(ls)).boxed().collect(Collectors.toList());
            fl("lrr");
            pp(lrr);


            fl("missing number");
            Collections.sort(ls, (x, y) -> x - y);
            for(int i = 0; i < ls.size(); i++){
                if(ls.get(i) == lrr.get(i)){

                }else{
                    pp(lrr.get(i));
                    break;
                }
            }
        }
        {
            {
                fl("missing number with set 1");
                List<Integer> ls = list(0, 9, 2, 1, 4, 3);
                Set<Integer> set = new HashSet<>(ls);
                pl(set);

                int missNum = -1;
                for(int i = 0; i < set.size(); i++){
                    if(set.contains(i)){
                    }else{
                        missNum = i;
                    }
                }

                if(missNum == -1){
                    pl("missNum=" + set.size() + 1);
                }else{
                    pl("missNum=" + missNum);
                }

            }
            {
                fl("missing number with set 2");
                List<Integer> ls = list(0, 1);
                Set<Integer> set = new HashSet<>(ls);
                pl(set);

                int missNum = -1;
                for(int i = 0; i < set.size(); i++){
                    if(set.contains(i)){
                    }else{
                        missNum = i;
                        break;
                    }
                }

                if(missNum == -1){
                    pl("missNum=[" + set.size() + "]");
                }else{
                    pl("missNum=[" + missNum + "]");
                }
            }
        }
        {
            {
                fl("partition 1");
                List<Integer> ls = list(4, 9, 1, 3, 6, 5);
                Integer[] arr = new Integer[ls.size()];
                int lo = 0;
                int hi = ls.size() - 1;
                int pivot = ls.get(hi);

                int k = lo;
                while(lo != hi){
                    if(ls.get(k) <= pivot){
                        arr[lo] = ls.get(k);
                        lo++;
                    }else{
                        arr[hi] = ls.get(k);
                        hi--;
                    }
                    k++;
                }
                arr[lo] = pivot;
                printArr(arr);
                var lss = arrayToList(arr);
                pl(lss);
            }
            {


            }
        }

    }
    {
        {
            fl("partition2 1");
            Integer[] arr = {1};
            Integer lo = 0, hi = 0;
            Integer pivotIndex = partition2(arr, lo, hi);
            t(pivotIndex, 0);
            printArr(arr);
        }
        {
            fl("partition2 2");
            Integer[] arr = {2, 1};
            Integer lo = 0, hi = arr.length - 1;
            Integer pivotIndex = partition2(arr, lo, hi);
            t(pivotIndex, 0);
            Integer[] expectedArray = {1, 2};
            t(expectedArray, arr);
            printArr(arr);
        }
        {
            fl("partition2 3");
            Integer[] arr = {2, 1, 3};
            Integer lo = 0, hi = arr.length - 1;
            Integer pivotIndex = partition2(arr, lo, hi);
            t(pivotIndex, 2);
            Integer[] expectedArray = {2, 1, 3};
            t(expectedArray, arr);
            printArr(arr);
        }
    }
    {
        {
            int[] arr = {1};
            printArrInt(arr);
        }
    }
    {
        {
            fl("pretty print");
            String[][] table = {{"Joe", "Bloggs", "18"},
                {"Steve", "Jobs", "20"},
                {"George", "Cloggs", "21"}};


            printArrStr2d(table);
            /**
              int max = 0;
              for(String[] arr : table){
              for(String s : arr){
              max = Math.max(max, len(s));
              }
              }

              for(int i=0; i<3; i++){
              for(int j=0; j<3; j++){
              System.out.print(String.format("%" + (max + 4) + "s", table[i][j]));
              }
              System.out.println("");
              }
             */
        }
        {
            {
                String[] arr = {"Joe", "Michael", "David", "Tony", "Tomy"};
                printArrStr(arr);
            }
        }
    }
    {
        {
            fl("First missing Number 1");
            Integer[] arr = {1, 3, 2, 5};
            Integer missingNum = firstMissingNum(arr);
            t(missingNum, 4);
        }
        {
            fl("First missing Number 2");
            Integer[] arr = {1, 3};
            Integer missingNum = firstMissingNum(arr);
            t(missingNum, 2);
        }
        {
            fl("First missing Number 3");
            Integer[] arr = {1, 5, 2, 3};
            Integer missingNum = firstMissingNum(arr);
            t(missingNum, 4);
        }
    }
    {
        {
            fl("groupList 1");
            List<Integer> ls = list(0, 0, 1, 0, 0, 0);
            List<List<Integer>> lss = groupList(ls);
            t(list(list(0,0), list(1), list(0, 0, 0)), lss);
        }
        {
            fl("groupList 2");
            List<Integer> ls = list(0);
            List<List<Integer>> lss = groupList(ls);
            t(list(list(0)), lss);
        }
        {
            fl("groupList 3");
            List<Integer> ls = list(0, 0, 1, 1, 1);
            List<List<Integer>> lss = groupList(ls);
            t(list(list(0, 0), list(1, 1, 1)), lss);
        }
        {
            fl("groupList 4");
            List<Integer> ls = list();
            List<List<Integer>> lss = groupList(ls);
            t(list(), lss);
        }
    }
    {
        {
            fl("test 8 1");
            List<Integer> expectedList = list(2, 4, 5, 9, 10);
            List<Integer> ls = list(2, 4, 9);
            List<Integer> lt = list(5, 10);

            int len1 = ls.size();
            int len2 = lt.size();
            int i = 0;
            int j = 0;
            List<Integer> ret = new ArrayList<>();
            while(i < len1 || j < len2){
                if(i >= len1){
                    ret.add(lt.get(j));
                    j++;
                }else if(j >= len2){
                    ret.add(ls.get(i));
                    i++;
                }else{
                    if(ls.get(i) > lt.get(j)){
                        ret.add(lt.get(j));
                        j++;
                    }else{
                        ret.add(ls.get(i));
                        i++;
                    }
                }
            }

            t(ret, expectedList);
        }
    }
    {
        {
            fl("group number");

            List<List<Integer>> expectedList = list(list(2), list(9, 9), list(2), list(1, 1) );
            // 2 9 9 2 1 1 => [2] [9,9] [2] [1, 1]

            // List<Integer> lnum = IntStream.range(1, 10).mapToObj(x -> x + 1).collect(Collectors.toList());
            List<Integer> ls = list(2, 9, 9, 2, 1, 1);
            List<List<Integer>> ret = new ArrayList<>();
            List<Integer> tmplist = new ArrayList<>();

            if(ls.size() > 0){
                tmplist.add(ls.get(0));
                for(int i = 1; i < ls.size(); i++){
                    if(ls.get(i-1) == ls.get(i)){
                        tmplist.add(ls.get(i));
                    }else{
                        ret.add(tmplist);
                        tmplist = new ArrayList<>();
                        tmplist.add(ls.get(i));
                    }
                }
            }
            if(tmplist.size() > 0)
                ret.add(tmplist);

            t(ret, expectedList);
            fl("expectedList");
            pp(expectedList);
            fl("ret");
            pp(ret);

        }

    }
    {
        {
            List<Integer> ls = IntStream.range(1, 10).mapToObj(x -> x + 1).collect(Collectors.toList());
            Integer[][] arr = {
                {1, 2},
                {3, 4}
            };
            pp(ls);
        }
        {
            fl("Matrix Multiplication 1");
            Integer[][] m1 = {
                {1, 2},
                {3, 4}
            };

            Integer[][] m2 = {
                {2, 3},
                {4, 5}
            };
            Integer[][] arr = multiMatrix(m1, m2);
            printArray2d(arr);


        }
        {
            fl("Matrix Multiplication 2");
            Integer[][] m1 = {
                {1}
            };

            Integer[][] m2 = {
                {2}
            };
            Integer[][] arr = multiMatrix(m1, m2);
            printArray2d(arr);
        }
        {
            {
                fl("Three consecutive repeating number");
                List<Integer> ls = list(1, 2, 2, 2, 1, 1, 0);
                boolean ret = threeConsecutive(ls);
                t(ret, true);
            }
        }
    }
    {
        {
            fl("mergeSortedList2");
            List<Integer> s1 = list(1, 4, 9);
            List<Integer> s2 = list(2, 8, 10);
            List<Integer> expectedList = list(1, 2, 4, 8, 9, 10);
            List<Integer> ls = mergeSortedList2(s1, s2);
            t(ls, expectedList);
        }
    }
    {
        fl("countMaxRepatingNum");
        List<Integer> ls = list(1, 1, 2, 3, 2, 1, 1);
        Integer max = countMaxRepatingNum(ls);
        t(max, 4);
    }
    {
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> ls = list(1, 2, 3);
        for(Integer n : ls){
            map.put(n, n);
        }

        for(Map.Entry<Integer,Integer> entry : map.entrySet()){
            pp(entry.getKey());
            pp(entry.getValue());
        }

        if(map.containsKey(2)){
            pp("map contains key ? " + map.containsKey(2));
        }
    }
    {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        for(Integer n : set){
            pp("n=" + n);
        }
        if(set.contains(2)){
            pp("set containsKey 2" + set.contains(2));
        }
    }
    {
        List<Integer> ls = IntStream.range(0, 10).mapToObj(x -> x + 1).collect(Collectors.toList());
        pp(ls);
    }
    {
        List<Integer> ls = IntStream.range(0, 10).mapToObj(x -> x + 1).collect(Collectors.toList());
    }
    {
        List<Integer> ls = IntStream.range(0, 10).mapToObj(x -> x + 1).collect(Collectors.toList());
    }
    {

        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 2);
        map.put(2, 3);
        for(Map.Entry<Integer, Integer> entry : map.entrySet()){
            pp(entry.getKey());
            pp(entry.getValue());
        }
    }
    {
        {
            fl("makeList");
            MyNode node = new MyNode();
            List<String> ls = list("January", "February", "March", "April", "May", "June", "July", "Auguest", "September", "October", "November", "December");  //  
            MyLinkedList linkedList = new MyLinkedList();
            linkedList.first = makeList(node, ls);
            linkedList.print();
        }
        {
            PriorityQueue<Student2> queue = new PriorityQueue<>((x, y) -> x.name.compareTo(y.name));
            queue.add(new Student2("David", 3));
            queue.add(new Student2("April", 4));
            queue.add(new Student2("February", 1));
            while(!queue.isEmpty()){
                Student2 s = queue.remove();
                pp(s.name);
                pp(s.age);
            }
        }
    }
    {
        {
            fl("Find n consecutive repeating number from a given list 1");
            List<Integer> ls = list(1, 2, 2, 2, 3, 3);
            List<List<Integer>> lss = new ArrayList<>();
            List<Integer> lt = new ArrayList<>(ls);
            while(len(lt) > 3){
                var s = take(3, lt);
                lss.add(s);
                lt = drop(1, lt);
            }
            if(len(lt) > 0)
                lss.add(lt);

            pp(lss);
        }
        {
            fl("Find n consecutive repeating number from a given list 2");
            List<Integer> ls = list(1, 2, 2);
            List<List<Integer>> lss = new ArrayList<>();
            List<Integer> lt = new ArrayList<>(ls);
            while(len(lt) > 3){
                var s = take(3, lt);
                lss.add(s);
                lt = drop(1, lt);
            }
            if(len(lt) > 0)
                lss.add(lt);

            pp(lss);
        }
    }
    {

    }
    end();
}

public static Integer maxConnection3(Integer[][] arr, Integer width, Integer height, Integer w, Integer h){
    return null;
}

public static MyNode makeList(MyNode node, List<String> ls){
    MyNode root = new MyNode();
    MyNode curr = root;
    for(String s : ls){
        if(curr.name == null){
            curr.name = s;
        }else{
            curr.next = new MyNode(s);
            curr = curr.next;
        }
    }
    return root;
}
public static Integer countMaxRepatingNum(List<Integer> ls){
    Integer ret = 0;
    Integer max = 0;
    Map<Integer, Integer> map = new HashMap<>();
    for(Integer n : ls){
        Integer value = map.get(n);
        if(value != null){
            map.put(n, value + 1);
        }else{
            map.put(n, 1);
        }
    }


    for(Map.Entry<Integer, Integer> entry : map.entrySet()){
        if(entry.getValue() > max){
            max = entry.getValue();
        }
    }
    return max;
}

public static List<Integer> mergeSortedList2(List<Integer> s1, List<Integer> s2){
    List<Integer> ls = new ArrayList<>();
    int len1 = s1.size();
    int len2 = s2.size();
    int i = 0, j = 0;
    while(i < len1 || j < len2){
        if(i >= len1){
            ls.add(s2.get(j));
            j++;
        }else if( j >= len2){
            ls.add(s1.get(i));
            i++;
        }else{
            if(s1.get(i) < s2.get(j)){
                ls.add(s1.get(i));
                i++;
            }else{
                ls.add(s2.get(j));
                j++;
            }
        }
    }
    return ls;
}
public static boolean threeConsecutive(List<Integer> ls){
    boolean ret = false;
    if(ls.size() > 2){
        for(int i = 2; i < ls.size(); i++){
            if(ls.get(i-1) == ls.get(i-2) && ls.get(i-2) == ls.get(i)){
                ret = true;
                break;
            }
        }
    }
    return ret;
}
public static Integer[] column(Integer[][] arr, int k){
    Integer[] ret = new Integer[arr.length];
    for(int i = 0; i < arr.length; i++){
        ret[i] = arr[i][k];
    }
    return ret;
}




public static Integer firstMissingNum(Integer[] arr){
    Integer len = arr.length;

    for(int i = 0; i < len; i++){
        Integer n = arr[i];
        if(0 <= (n-1) && (n-1) < len) // in the range [0 - (len - 1)]
            arr[n-1] = n;
        else
            arr[i] = -n;
    }

    Integer firstNum = -1;
    for(int i = 0; i < len; i++){
        if(arr[i] < 0)
            firstNum = i+1;
    }
    return firstNum;
}


public static Integer partition2(Integer[] arr, Integer lo, Integer hi){
    Integer bigInx = lo;
    Integer pivotInx = lo;
    Integer pivot = arr[hi];
    for(int i = lo; i <= hi; i++){
        if(arr[i] <= pivot){
            swap(arr, bigInx, i);
            printArr(arr);
            pivotInx = bigInx;
            bigInx++;
        }
    }
    return pivotInx;
}

public static String intToBin(Integer n){

    String ret = "";
    if(n == 0){
        ret = "0";
    }else{
        while(n > 0){
            Integer r = n % 2;
            n = n / 2;
            ret = intToStr(r) + ret;
        }
    }
    return ret;
}

public static Integer strToInt(String s){
    List<Integer> ls = IntStream.range(0, s.length()).mapToObj(x -> x).collect(Collectors.toList());
    Integer sum = 0;
    for(int i = 0; i < s.length(); i++){
        Integer p = s.length() - 1 - i;
        Integer c = s.charAt(i) - '0';
        sum += c * (int)Math.pow(10, p);
    }
    return sum;
}

/*
KEY: hex string to int, hex to int
"2206"
2*16^3  2*16^2  0* 16^1 +  6*16^0
 */
public static Integer hexStrToInt(String s){
    Function<Character, Integer> toHex = x -> {
        Character c = toLower(x);
        if ('0' <= c && c <= '9'){
            return c - '0';
        }else{
            return 10 + (c - 'a');
        }
    };


    Integer sum = 0;
    for(int i = s.length() - 1; i >= 0; i--){
        sum += toHex.apply(s.charAt(i)) * (int)Math.pow(16, i);
    }
    return sum;
}

/*

   a b c d   e f g h   i j

   k   l m n  o p q   r s t

   u v w x y z

 */
public static String digitToHex(Integer n){
    String ret = "";
    if(0 <= n && n <= 9){
        ret = intToStr(n);
    }else{
        if(n == 10){
            ret = "a";
        }else if(n == 11){
            ret = "b";
        }else if(n == 12){
            ret = "c";
        }else if(n == 13){
            ret = "d";
        }else if(n == 14){
            ret = "e";
        }else if(n == 15){
            ret = "f";
        }
    }
    return ret;
}
public static String intToHexStr(Integer n){
    String ret = "";
    Integer base = 16;
    if(n == 0){
        ret = "0";
    }else{
        Integer curr = n;
        while( curr > 0){
            Integer r = curr % base;
            curr = curr / base;
            ret = digitToHex(r) + ret;
        }
    }
    return ret;
}

/*
   "*|**|*|*"
   x  x
   c = 2, | => add 2
   c = 1  | => add 1
   c = 1       No op

 */
public static List<Integer> countItem(String s, List<Integer> startIndices, List<Integer> endIndices){
    Integer count = 0;
    Stack<Character> stack = new Stack<>();
    Integer current = 0;
    List<Integer> ret = new ArrayList<>();
    for(int k = 0; k < startIndices.size(); k++){

        Integer startInx = startIndices.get(k) - 1;
        Integer endInx = endIndices.get(k) - 1;
        if(s.length() >=  endInx - startInx && s.length() >= endIndices.get(k)){
            for(int i = startInx; i <= endInx; i++){
                if(s.charAt(i) == '|'){
                    if(stack.size() == 1){
                        count += current;
                    }else{
                        stack.add('|');
                    }
                    current = 0;
                }else
                    current++;
            }
        }
        ret.add(count);
        current = 0;
        count = 0;
        if(stack.size() > 0)
            stack.clear();
    }
    return ret;
}

/*
   extra 
   extract -> ex - tract 
   exactly -> ex - actly
   extract
   Extract string from a given string which is enclosed with brackets such as {}, <> and []
   {ab}<{123}>{44}{} => ab 123 44 ""
   <ab><334><>     => ab 334
   [ab][44][a12][] => ab 44 a12
 */
public static List<String> extractStr(String s){
    Stack<Character> stack = new Stack<>();
    List<String> ls = new ArrayList<>();
    String str = "";
    for(int i = 0; i < s.length(); i++){
        char c = s.charAt(i);
        if(c == '{' || c == '<' || c == '['){
            stack.add(c);
        }else{
            if(c == '}' || c == '>' || c == ']'){
                Character cc = stack.peek();
                if(cc == '{' && c == '}'){
                    stack.pop();
                }else if(cc == '<' && c == '>'){
                    stack.pop();
                }else if(cc == '[' && c == ']'){
                    stack.pop();
                }else{
                    pl("Invalid format");
                }

                if(trim(str).length() > 0){
                    ls.add(str);
                    str = "";
                }
            }else{
                str += charToStr(c);
            }
        }
    }
    if(stack.size() == 0){
        pl("valid format");
    }else{
        pl("Invalid format");
    }
    return ls;
}



/*
   1 2
   3 4
   => rotate counterclockwise(CCW) 90 degree
   3 1
   4 2
   swap
 */
public static Integer[][] rotate90(Integer[][] arr){
    Integer[][] array = copyArrayInteger2d(arr);

    Integer heightIndex = arr.length - 1;
    for(int i = 0; i < arr.length; i++){
        for(int j = 0; j < arr[0].length/2; j++){
            // swap column, swap elements vertically, swap elements horizontally
            Integer tmp = arr[j][i];
            arr[j][i] = arr[heightIndex - j][i];
            arr[heightIndex - j][i] = tmp;
        }
    }

    // swap elements main-diagonally, minor diagonally
    for(int i = 0; i < arr.length; i++){
        for(int j = i; j < arr[0].length; j++){
            Integer tmp = arr[i][j];
            arr[i][j] = arr[j][i];
            arr[j][i] = tmp;
        }
    }
    return array;

}

/*
   Given unsorted integer array, find the smallest positive missing integer.

https://leetcode.com/problems/first-missing-positive/
Leetcode question

Input nums = [1, 2, 0]
Output 3

Input nums = [3, 4, -1, 1]
Output 2

0 1 2

-1 1 3 4

-1 1 3
1  3 4
2  2 1

1 2 3 5 6 7 9
1 1 1 1 1 1 1
2 3 4 6 7 8 10

-3 1 2 4
1  1 1 1
-2 2 3 5

-3 2 4
1 1 1
-2 3 5

1 2 3 5 6 7
2 3 5 6 7 9
1 1 2 1 1 2

-2 4 5 7 9

-2 4 5 7
4 5 7 9
6 1 2 2

3 4 -1 1

m1 = -1
m2 = 1

1 2 3 4 6 9
m1 = 1
m2 = 2


 */
public static Integer findMissingInteger(Integer[] arr){
    Integer min = 0;
    Arrays.sort(arr);
    Integer[] a1 = Arrays.copyOf(arr, arr.length);
    Integer[] a2 = Arrays.copyOf(arr, arr.length);
    Integer[] diff = new Integer[arr.length];
    Boolean found = false;
    for(int i = 1; i < a1.length && !found; i++){
        diff[i] = a2[i] - a1[i-1];
        if(diff[i] > 1){
            int k = 1;
            while(k < diff[i] && !found){
                if(a1[i-1] + k > 0){
                    min = a1[i-1] + k;
                    found = true;
                }
                k++;
            }
        }
    }
    if(!found){
        Integer m = a1[arr.length - 1];
        while(m < 0)
            m++;

        min = m + 1;
    }

    return min;
}

/*

https://leetcode.com/problems/wildcard-matching/
String matching
Given a string and pattern
? => match single character
 * => match zero or more characters

 s = abc  
 p = ?bc
 => true

 s = abbc
 p = a*c
 => true

 s = abcd
 p = *ca
 => false

 s = abcd
 p = *ab??
 => true

 s = abcd
 p = ?*
 => true

 s = ab
 p = a**b
 => true

 */
public static Boolean isMatched(String s, String p){
    Boolean ret = false;
    return ret;
}

/*
   Given a binary Tree, find the maximum path from the root node
 */
public static Integer maxPath(Node root){
    if(root != null){
        Integer left = maxPath(root.left);
        Integer right = maxPath(root.right);
        return Math.max(left, right) + 1;
    }else{
        return 0;
    }
}

public static void addNode(Node root, Node node){
    Node curr = root;
    if(curr == null){
        root = node;
    }else{
        while(curr != null){
            if(node.data < curr.data){
                if(curr.left == null){
                    curr.left = node;
                    break;
                }
                else
                    curr = curr.left;

            }else{
                if(curr.right == null){
                    curr.right = node;
                    break;
                }
                else
                    curr = curr.right;
            }
        }
    }

}

public static void addAllFile(Node root, String fname){
    List<String> dirs = recurveDir(fname);
    for(String name : dirs){
        Integer hashCode = name.hashCode();
        addNode(root, new Node(hashCode));
    }

}

/*
   1 3
   2 4

   5 3 1 -4 -6 -100, -9999
   6 4 2 -3 -5
   -2
   -1
   0
   1

   split the list into two lists:
   partition a list into multiple sublist
   partition a list into two sublists
   divide a list into two lists
   one list contains positive integer or zero
   other list contains negative integer

   -4 -6 -100 -999 5 3 1
 */
public static Integer differentElement(Integer[] arr){
    Set<Integer> set = new HashSet<>();
    Set<Integer> differentSet = new HashSet<>();
    Integer min = Integer.MAX_VALUE;
    for(int i = 0; i < arr.length; i++){
        set.add(arr[i]);
    }
    for(int i = 0; i < arr.length; i++){
        if(!set.contains(arr[i] + 1)){
            if(arr[i] + 1 < min){
                min = arr[i] + 1;
                pl("min=" + min);
            }
        }
    }
    return min;
}

public static String reverseStr(String s){
    int len = s.length();
    StringBuffer sb = new StringBuffer(s);
    for(int i = 0; i < len/2; i++){
        int rInx = len - 1 - i;
        char c = sb.charAt(i);
        sb.setCharAt(i, sb.charAt(rInx));
        sb.setCharAt(rInx, c);
    }
    return sb.toString();
}


public static List<List<Integer>> groupList(List<Integer> arr){
    List<Integer> ls = new ArrayList<>();
    List<List<Integer>> lss = new ArrayList<>();
    if(arr.size() > 0){
        ls.add(arr.get(0));
        for(int i = 1; i < arr.size(); i++){
            if(arr.get(i-1) == arr.get(i)){
                ls.add(arr.get(i));
            }else{
                lss.add(ls);
                ls = new ArrayList<>();
                ls.add(arr.get(i));
            }
        }
    }

    if(len(ls) > 0){
        lss.add(ls);
    }

    return lss;
}
		
public static boolean binSearch(int[] arr, int lo, int hi, int key){
	if(lo < hi){
		int m = (lo + hi)/2;
		if(arr[m] < key){
			binSearch(arr, m + 1, hi, key);
		}else if (arr[m] > key) {
			binSearch(arr, lo, m - 1, key);
		}else{
			return true;
		}
	}else{
		return arr[lo] == key;
	}
	return false;
}

	/**
	   Merge two sorted lists
	 */
	public static List<Integer> mergeList3(List<Integer> s1, List<Integer> s2){
		List<Integer> ret = new ArrayList<>();
		int i = 0;
		int j = 0;
		while(i < len(s1) || j < len(s2)){
			if(i >= len(s1)){
				ret.add(s2.get(j));
				j++;
			}else if(j >= len(s2)){
				ret.add(s1.get(i));
				i++;
			}else{
				if(s1.get(i) < s2.get(j)){
					ret.add(s1.get(i));
					i++;
				}else{
					ret.add(s2.get(j));
					j++;
				}
			}
		}
		return ret;
	}
	
	public static Integer partList3(List<Integer> ls, Integer lo, Integer hi){
		if(lo < hi){
			Integer pivot = ls.get(hi);
			Integer big = lo;
			for(int i = lo; i <= hi; i++){
				if(ls.get(i) <= pivot){
					swap(ls, i, big);
					if(big < hi)
						big++;
				}
			}
			return big;
		}
		return lo;
	}
    

    public static int[] sumArrX(int[] a1, int[] a2){
       int mx = max(len(a1), len(a2)); 
       int[] b1 = concatArr(repeatArrint(mx - len(a1), 0), a1);
       int[] b2 = concatArr(repeatArrint(mx - len(a2), 0), a2);
       int[] su = repeatArrint(mx + 1, 0); 
       
       int c = 0;
       for(int i = mx - 1; i >= 0; i--){
         int s = b1[i]  + b2[i] + c;
         su[i + 1] = s % 10;
         c = s / 10;
       }
       su[0] = c;

       return su;
    }

	/**
	   Given an Integer array, partition the array to even part and old part
	   [1, 2, 3, 4]
	   =>
	   [2, 4, 1, 3]
	   
	   Use partiton pivot algorithm to solve it
	 */
	public static Integer[] partitionEvenOdd(Integer[] arr, int lo, int hi){
		Integer[] ret = Arrays.copyOf(arr, len(arr));
		int oInx = 0;
		for(int i = 0; i < len(arr); i++){
			if (arr[i] % 2 == 0){
				swap(ret, oInx, i);
				oInx++;
			}
		}
		return ret;
	}

    /**
      Given a sorted array, search whether the key is in the array
    */
    public static boolean binSearchX(int[] arr, int key, int lo, int hi){
        if(lo <= hi){
            int m = (lo + hi)/2;
            if(arr[m] < key){
              return binSearchX(arr, key, m+1, hi);
            }else if(arr[m] > key){
              return binSearchX(arr, key, lo, m-1);
            }else{
                return arr[m] == key;
            }
        }
        return false;
    }
	
    public static void test6(){
        beg();
        {
            {
                fl2(4);
                String s = "";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(1);
                endIndices.add(6);
                endIndices.add(5);
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(0, 0));
            }
            {
                fl2(4);
                String s = "|*|";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(2);
                endIndices.add(6);
                endIndices.add(5);
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(0, 0));
            }
            {
                fl2(4);
                String s = "*||*";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(2);
                endIndices.add(4);
                endIndices.add(4);
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(0, 0));
            }
            {
                fl2(4);
                String s = "*|||";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(2);
                endIndices.add(4);
                endIndices.add(4);
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(0, 0));
            }
            {
                fl2(4);
                String s = "*|||*|";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(1);
                endIndices.add(4);
                endIndices.add(s.length());
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(0, 1));
            }
            {
                fl2(4);
                String s = "*||**|*|";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(1);
                endIndices.add(5);
                endIndices.add(s.length());
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(0, 3));
            }
            {
                fl2(4);
                String s = "|*||**|*|";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(1);
                endIndices.add(5);
                endIndices.add(s.length());
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(1, 4));
            }
            {
                fl2(4);
                String s = "|*||**|*|";
                List<Integer> startIndices = new ArrayList<>();
                List<Integer> endIndices = new ArrayList<>();
                startIndices.add(1);
                startIndices.add(1);
                endIndices.add(5);
                endIndices.add(s.length());
                List<Integer> ls = countItem(s, startIndices, endIndices);

                t(ls, list(1, 4));
            }

        }
        {
            fl("Reverse String 0");        
            String s = "abc";
            String rs = reverseStr(s);
            pp(rs);
			fl();
        }
		{
			List<String> ls = new ArrayList<>();
			String s = "moroccoArgintinaSpanJapanKoran";
			String curr = "";
			pl(s);
			for(int i = 0; i < len(s); i++){
				char c = s.charAt(i);
				if(isUpper(c)){
					if(len(curr) > 0){
						ls.add(curr);
					}
					ls.add(c + "");
					curr = "";
				}else{
					if(len(ls) == 0){
						ls.add("");
					}
					curr += c + "";
				}
			}
			if(len(curr) != 0){
				ls.add(curr);
				curr = "";
			}
			pp(ls);
		}
		{
			fl("binSearch 1");
			int[] arr = {1, 3, 9, 6};
			int lo = 0;
			int hi = len(arr) - 1;
			int key = 3;
			boolean b = binSearch(arr, lo, hi, key);
			pp(b == true);
		}
		{
			fl("binSearch 2");
			int[] arr = {1, 3, 9, 6};
			int lo = 0;
			int hi = len(arr) - 1;
			int key = 2;
			boolean b = binSearch(arr, lo, hi, key);
			pp(b == false);
		}
		{
			fl("binSearch 3");
			int[] arr = {1};
			int lo = 0;
			int hi = len(arr) - 1;
			int key = 2;
			boolean b = binSearch(arr, lo, hi, key);
			pp(b == false);
		}
		{
			fl("mergeList3");
			List<Integer> ls = list(1, 3, 3, 9, 10);
			List<Integer> lt = list(0, 3, 2, 4, 11);
			List<Integer> ret = mergeList3(ls, lt);
			pp(ret);
			
		}
		{
			fl("partList3");
			List<Integer> ls = list(1,2,3);
			Integer lo = 0;
			Integer hi = ls.size() - 1;
			Integer pivotInx = partList3(ls, lo, hi);
			pp(ls);
			pp("pivotInx=" + pivotInx);
		}
		{
			fl("partitionEvenOdd 0");
			Integer[] arr = {1, 2};
			Integer[] expectedArr = {2, 1};
			int lo = 0;
			int hi = len(arr) - 1;
			Integer[] ls = partitionEvenOdd(arr, lo, hi);
			t(expectedArr, ls);
			printArray(ls);
		}
		{
			fl("partitionEvenOdd 1");
			Integer[] arr = {1};
			Integer[] expectedArr = {1};
			int lo = 0;
			int hi = len(arr) - 1;
			Integer[] ls = partitionEvenOdd(arr, lo, hi);
			t(expectedArr, ls);
			printArray(ls);
		}
		{
			fl("partitionEvenOdd 2");
			Integer[] arr = {1, 1, 2, 2, 2};
			Integer[] expectedArr = {2, 2, 2, 1, 1};
			int lo = 0;
			int hi = len(arr) - 1;
			Integer[] ls = partitionEvenOdd(arr, lo, hi);
			t(expectedArr, ls);
			printArray(ls);
		}
		{
			fl("partitionEvenOdd 2");
			Integer[] arr = {2, 2, 2, 1, 1};
			Integer[] expectedArr = {2, 2, 2, 1, 1};
			int lo = 0;
			int hi = len(arr) - 1;
			Integer[] ls = partitionEvenOdd(arr, lo, hi);
			t(expectedArr, ls);
			printArray(ls);
		}
		{
			fl("partitionEvenOdd 2");
			Integer[] arr = {1, 2, 3, 4};
			Integer[] expectedArr = {2, 4, 3, 1};
			int lo = 0;
			int hi = len(arr) - 1;
			Integer[] ls = partitionEvenOdd(arr, lo, hi);
			t(expectedArr, ls);
			printArray(ls);
		}
        {
            {
                int[] arr = {1, 4, 4, 8, 9};
                int key = 8;
                int lo = 0;
                int hi = len(arr) - 1;
                boolean b = binSearchX(arr, key, lo, hi);
                t(b, true);
            }
            {
                int[] arr = {1, 4, 4, 8, 9};
                int key = 10;
                int lo = 0;
                int hi = len(arr) - 1;
                boolean b = binSearchX(arr, key, lo, hi);
                t(b, false);
            }
            {
                int[] arr = {1};
                int key = 10;
                int lo = 0;
                int hi = len(arr) - 1;
                boolean b = binSearchX(arr, key, lo, hi);
                t(b, false);
            }
            {
                int[] arr = {1};
                int key = 1;
                int lo = 0;
                int hi = len(arr) - 1;
                boolean b = binSearchX(arr, key, lo, hi);
                t(b, true);
            }
            {
                int[] arr = {1, 2};
                int key = 1;
                int lo = 0;
                int hi = len(arr) - 1;
                boolean b = binSearchX(arr, key, lo, hi);
                t(b, true);
            }
            {
                int[] arr = {1, 2};
                int key = 3;
                int lo = 0;
                int hi = len(arr) - 1;
                boolean b = binSearchX(arr, key, lo, hi);
                t(b, false);
            }
            {
                fl("sumArrX 1");
                int[] a1 = {1, 2};
                int[] a2 = {9}; 
                int[] sum = sumArrX(a1, a2);
                printArrint(sum);
            }
            {
                fl("sumArrX 1");
                int[] a1 = {9};
                int[] a2 = {9}; 
                int[] sum = sumArrX(a1, a2);
                printArrint(sum);
            }
        }
		{
			{
				fl("List<Integer> Sort 1000000 random integers from 1 to 1000");
				List<Integer> ls = geneRandomIntegerFromToList(1, 1000, 1000000);
				int lo = 0;
				int hi = len(ls) - 1;
				StopWatch sw = new StopWatch();
				quickSort(ls, lo, hi);
				sw.print();
			}
			{
				fl("Array Sort 1000000 random integers from 1 to 1000");
				Integer[] ls = geneRandomIntegerFromToArr(1, 1000, 1000000);
				int lo = 0;
				int hi = len(ls) - 1;
				StopWatch sw = new StopWatch();
				quickSort(ls, lo, hi);
				sw.print();
			}
			
		}
        end();
    }
}  
