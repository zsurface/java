import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;

public class test_prefix{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String str = "abc";
        List<String> list = Aron.prefix(str);
        Print.printList(list);
        Print.p(list.size());
        Test.t(list.size() == 3);


        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        {
            String str = "";
            List<String> list = Aron.prefix(str);
            Print.printList(list);
            Print.p(list.size());
            Test.t(list.size() == 0);
        }
        {
            String str = null;
            List<String> list = Aron.prefix(str);
            Print.printList(list);
            Print.p(list.size());
            Test.t(list.size() == 0);
        }
        {
            String str = "a";
            List<String> list = Aron.prefix(str);
            Print.printList(list);
            Print.p(list.size());
            Test.t(list.size() == 1);
        }

        Aron.end();
    }
} 

