import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

// java algebraic data type
interface Term{
    public abstract <R> R accept(Visitor<R> visitor);
    public interface Visitor<R> {
        R constant(int value);
        R add(Term t1, Term t2);
        R isZero(Term condition, Term then, Term otherwise);
    }
    public static Visitor<Term> term = new Visitor<Term>(){
        public Term constant(int value){
            return new Term(){
                public <R> R accept(Visitor<R> v){
                    return v.constant(value);
                }
            };
        }
        public Term add(Term t1, Term t2){
            return new Term(){
                public <R> R accept(Visitor<R> v){
                    return v.add(t1, t2);
                }
            };
        }
        public Term isZero(Term condition, Term then, Term otherwise){
            return new Term(){
               public <R> R accept(Visitor<R> v){
                   return v.isZero(condition, then, otherwise);
               }
            };
        }
    };
}


public class AlgebraicDataType{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
      end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

