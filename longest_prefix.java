import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class FileType{
    Boolean isHtml(String s){
        Pattern pat = Pattern.compile(".\\.html$", Pattern.CASE_INSENSITIVE);
        return pat.matcher(s).find();
    }
    Boolean isJava(String s){
        Pattern pat = Pattern.compile(".\\.java$", Pattern.CASE_INSENSITIVE);
        return pat.matcher(s).find();
    }
    Boolean isHaskell(String s){
        Pattern pat = Pattern.compile(".\\.hs$", Pattern.CASE_INSENSITIVE);
        return pat.matcher(s).find();
    }
    Boolean isJPG(String s){
        Pattern pat1 = Pattern.compile(".\\.jpg$", Pattern.CASE_INSENSITIVE);
        Pattern pat2 = Pattern.compile(".\\.jpeg$", Pattern.CASE_INSENSITIVE);
        return pat1.matcher(s).find() || pat2.matcher(s).find();
    }
}

public class longest_prefix{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";

        List<String> ls1 = Arrays.asList("dogsk", "dogskcat", "banada");
        String[] arr = new String[ls1.size()];
        arr = ls1.toArray(arr);
        Print.p(arr);

        List<String> ls2 = Arrays.asList("dogsgod", "toronto", "bankk");
        String s = Aron.longPrefix(ls1, ls2);

        Print.p("s=" + s);

        List<String> list = Aron.suffixStr("abc");
        List<String> list1 = Aron.prefixStr("abc");
        Print.p(list);
        Print.fl();
        Print.p(list1);

        Aron.end();
    }

    public static void test1(){
        Aron.beg();
        {
            String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
            List<Integer> ls = Arrays.asList(1, 3, 3);
            Integer n = ls.stream().reduce(0, (x, y) -> x + y);
            List<Integer> lss = ls.stream().filter( x -> x < 3).collect(Collectors.toList());

            ls.forEach(x -> System.out.println("x=" + x));

            Pattern pattern = Pattern.compile("\\s+"); 

            Print.p(n);
        }
        {
            List<String> ls = Arrays.asList("dog", "cat", "pig", "Michael", "file.html", "kk.html_33", ".html", "html");
            Pattern pattern = Pattern.compile(".\\.html$");
            List<String> fls = ls.stream().filter( s -> pattern.matcher(s).find()).collect(Collectors.toList());
            Print.p(fls);
        }

        Aron.end();
    }
} 

