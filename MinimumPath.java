import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class MinimumPath{
    public static void main(String[] args) {
        test0();
        test1();
        test2();
        test3();
        test4();
        test5();
    }
    public static void test0(){
        Aron.beg();
        int[][] arr2d = {
            { 0,   1,   0,  1},
            { 0,   0,   1,  1},
            { 0,   0,   0,  1},
            { 0,   0,   0,  0},
        };
        Aron.printArray2D(arr2d);
        int height = arr2d.length;
        int width = arr2d[0].length;
        
        int k = 2; 
        int h = 0;
        int w = 1;
        printPath(arr2d, h, w, k);
        
        Aron.end();
    }
    public static void test1(){
        Aron.beg();

        int[][] arr2d = {
            { 1,   2,   9,  1},
            { 0,   0,   3,  4},
            { 0,   0,   0,  1},
            { 0,   0,   0,  0},
        };
        Aron.printArray2D(arr2d);
        int height = arr2d.length;
        int width = arr2d[0].length;
        
        int k = 2; 
        int h = 0;
        int w = 1;
        int m = findMaxPath(arr2d, h, w, k);
        Print.pl("m=" + m);

        Aron.end();
    }
    public static void test2(){
        List<Integer> list = Arrays.asList(1, 2, 3);
        int s = 5;
        int n = minCoins(list, s);
        Test.t(n == 2); 
    }
    public static void test3(){
        List<Integer> list = Arrays.asList(1, 2, 3);
        int s = 2;
        int n = minCoins(list, s);
        Test.t(n == 1); 
    }
    public static void test4(){
        List<Integer> list = Arrays.asList(1, 2, 3);
        int s = 3;
        int n = minCoins(list, s);
        Test.t(n == 1); 
    }
    public static void test5(){
        List<Integer> list = Arrays.asList(1, 2, 3);
        int s = 6;
        int n = minCoins(list, s);
        Test.t(n == 2); 
    }
    /*
        Precondition: given node: [h][w] > 0 and k > 0 
        If k = 0, then no edge, do nothing
        If k > 0 and arr[h][w] > 0
    */
    public static int findMaxPath(int[][] arr, int h, int w, int k){
        if(arr != null && arr[h][w] > 0){
            if(k - 1 == 0)
                return arr[h][w];
            else
                return maxPath(arr, w, k - 1);            
            
        }
        return 0;
    }

    // The method handles k = 0
    public static void printPath(int[][] arr2d, int h, int w, int k){
        if(arr2d[h][w] > 0){
            if(k == 0)
                Print.pl("[" + h + "][" + w + "]=" + arr2d[h][w]);
            else {
                allPaths(arr2d, w, k - 1);
            }
        }
    }

    // find all the paths that have length k
    // if k == 0, then print the root node, if node has loop 
    // if node has loop, it is trick => you can loop forever 
    // (0) - 1 > (1) => [1][2] = 1
    public static void allPaths(int[][] arr, int h, int k){
        if(arr != null){
            int height = arr.length;
            int width  = arr[0].length;
            for(int w = 0; w < width; w++){
                if(h != w && arr[h][w] > 0){
                    if(k == 0){
                        Print.pl("[" + h + "][" + w + "]=" + arr[h][w]);
                    }else{
                        allPaths(arr, w, k - 1); 
                    }
                }
            }
        }
    }
    
    /*
            [ m
                [
                  3
                ]
                [
                  2
                ]
              3
            ]
    */

    public static int maxPath(int[][] arr, int h, int k){
        int m = 0;
        if( arr != null){
            int height = arr.length;
            int width = arr[0].length;
            for(int w = 0; w < width; w++){
                if(h != w && arr[h][w] > 0){
                    if(k == 0){
                        return arr[h][w];
                    }else{
                        m = Math.max(m, maxPath(arr, w, k - 1));
                    }
                }
            }
        }
        return m;
    }

    /* 
        (4) -> (4 - 2) -> (2 - 2)
        {2} s = 4
        [ s=4
                [ s=2
                        [ s = 0
                          

                      0 <-]
              1 <-]
     2 <-]
    */
    public static int minCoins(List<Integer> list, int s){
        int min = Integer.MAX_VALUE;
        if( list != null){
            if (s < 0){
                return Integer.MAX_VALUE;
            }
            else if( s == 0){
                return 0;
            }else{
                for(Integer n : list){
                    min = Math.min(min, minCoins(list, s - n));
                }
            }
        }
        return min + 1;
    }
} 

