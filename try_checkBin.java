import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;


/**
     
     
     


 */

public class try_checkBin{
    public static void main(String[] args) {
        test1();
	test_MaxStack();
	test3();
	test4();
	test6();
	test7();
	test_diffOneChar();
	test_diffOneChar2();
    }
    
    public static void test1(){
        beg();
	{
	    int[][] arr = { {1, 2, 3},
			    {3, 4, 5},
			    {6, 7, 8}};

	    p(arr);
	    fl();
	    rotateArray(arr);
	    p(arr);
	}
        end();
    }
    
    public static void test_MaxStack(){
        beg();
	{
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(1);
		mstack.insert(2);
		Optional opt = mstack.getMax();
		t(3, opt.get());
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		Optional opt = mstack.getMax();
		t(3, opt.get());
	    }
	    {
		MaxStack mstack = new MaxStack();
		Optional opt = mstack.getMax();
		t(opt.isPresent(), false);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		Optional opt = mstack.getMax();
		t(opt.isPresent(), true);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(4);
		Optional opt = mstack.getMax();
		t(opt.get(), 4);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(4);
		mstack.insert(1);
		mstack.pop();
		Optional opt = mstack.getMax();
		t(opt.get(), 4);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(4);
		mstack.pop();
		Optional opt = mstack.getMax();
		t(opt.get(), 3);
	    }
	}
        end();
    }
    public static void test3(){
        beg();
	{
	    {
		Node node = geneBinNoImage(list(4, 3), false);
		Node n1 = new Node(4);
		Node n2 = new Node(3);
		Node n = leaseCommonAncestor(node, n1, n2);
		t(n.data, 4);
	    }
	    {
		Node node = geneBinNoImage(list(4, 3, 1), false);
		Node n1 = new Node(3);
		Node n2 = new Node(1);
		Node n = leaseCommonAncestor(node, n1, n2);
		t(n.data, 3);
	    }
	    {
		Node node = geneBinNoImage(list(4, 3, 1, 0), false);
		Node n1 = new Node(3);
		Node n2 = new Node(0);
		Node n = leaseCommonAncestor(node, n1, n2);
		t(n.data, 3);
	    }
	    {

		/**
		                  4 
                               3          10
                            1                    
                         0                        20
                                             12
		 */
		Node node = geneBinNoImage(list(4, 3, 1, 0, 10, 20, 12), false);
		Node n1 = new Node(12);
		Node n2 = new Node(20);
		Node n = leaseCommonAncestor(node, n1, n2);
		t(n.data, 20);
	    }
	}
	end();
    }
    public static void test4(){
	beg();
	{
	    var ls = list(1, 2, 3, 4, 5, 6, 7);
	    List<Integer> minList = list();
	    Integer s = 10;
	    coinChange(ls, s, list(), minList);
	    p(minList);
	}
	end();
    }

    public static void test_diffOneChar(){
	beg();
	{
	    {
		String s1 = "abc";
		String s2 = "ab";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "a";
		String s2 = "ab";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "";
		String s2 = "a";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "a";
		String s2 = "ab";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abcd";
		String s2 = "abcde";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "ab";
		String s2 = "b";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abc";
		String s2 = "ac";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abcd";
		String s2 = "abd";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abc";
		String s2 = "bca";
		Boolean b = diffOneChar(s1, s2);
		t(b, false);
	    }
	    {
		String s1 = "";
		String s2 = "b";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "a";
		String s2 = "";
		Boolean b = diffOneChar(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "ab";
		String s2 = "eb";
		Boolean b = diffOneChar(s1, s2);
		t(b, false);
	    }
	    {
		String s1 = "abc";
		String s2 = "aec";
		Boolean b = diffOneChar(s1, s2);
		t(b, false);
	    }
	    {
		String s1 = "abc";
		String s2 = "abe";
		Boolean b = diffOneChar(s1, s2);
		t(b, false);
	    }
	    {
		String s1 = "abc";
		String s2 = "ebc";
		Boolean b = diffOneChar(s1, s2);
		t(b, false);
	    }
	    {
		String s1 = "bcee";
		String s2 = "abce";
		Boolean b = diffOneChar(s1, s2);
		t(b, false);
	    }
	}
	end();
    }
    public static void test_diffOneChar2(){
	beg();
	{
	    {
		String s1 = "a";
		String s2 = "ab";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "ab";
		String s2 = "a";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "ab";
		String s2 = "b";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "";
		String s2 = "a";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "a";
		String s2 = "";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "ab";
		String s2 = "abc";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abc";
		String s2 = "ab";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abc";
		String s2 = "ab";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "ab";
		String s2 = "aeb";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "aeb";
		String s2 = "ab";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abcd";
		String s2 = "abd";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abcd";
		String s2 = "abc";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abcd";
		String s2 = "acd";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "aaa";
		String s2 = "aa";
		Boolean b = diffOneChar2(s1, s2);
		t(b, true);
	    }
	    {
		String s1 = "abc";
		String s2 = "cb";
		Boolean b = diffOneChar2(s1, s2);
		t(b, false);
	    }
	    {
		String s1 = "abc";
		String s2 = "cba";
		Boolean b = diffOneChar2(s1, s2);
		t(b, false);
	    }
	    {
		String s1 = "abc";
		String s2 = "ca";
		Boolean b = diffOneChar2(s1, s2);
		t(b, false);
	    }
	    
	}
	end();
    }
    public static Node node(Integer n ){
	return new Node(n);
    }
    public static void test6(){
	beg();
	{
	    {
		Node root = null;
		Boolean b = isBST(root);
		t(b, true);
	    }
	    {
		Node root = new Node(9);
		Boolean b = isBST(root);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4, 12), false);
		Boolean b = isBST(root);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4), false);
		Boolean b = isBST(root);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4, 1, 10, 20, 8), false);
		Boolean b = isBST(root);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(2, 6, 3, 40, 26, 9, 4, 1, 10, 20, 8), false);
		Boolean b = isBST(root);
		t(b, true);
	    }
	    {
		Node root = node(9);
		root.right = node(2);
		Boolean b = isBST(root);
		t(b, false);
	    }
	    {
		Node root = node(9);
		root.left = node(20);
		Boolean b = isBST(root);
		t(b, false);
	    }
	    {
		Node root = node(9);
		root.left = node(20);
		root.left.left = node(1);
		Boolean b = isBST(root);
		t(b, false);
	    }
	}
    }
    public static void test7(){
	beg();
	{
	    {
		/**
		             9
                        1         20
		 */
		Node root = geneBinNoImage(list(9, 1, 20), false);
		Node n = maxLeftSubtree(root.left);
		t(n.data, 1);
	    }
	    {
		/**
		            9
                         4
                      1
		 */
		Node root = geneBinNoImage(list(9, 4, 1), false);
		Node n = maxLeftSubtree(root.left);
		t(n.data, 4);
	    }
	    {
		/**
		                   9
                               4
                           1       
                               3
		 */          
		Node root = geneBinNoImage(list(9, 4, 1, 3), false);
		Node n = maxLeftSubtree(root.left);
		t(n.data, 4);
	    }
	    {
		/**
		       9
                          20
		 */
		Node root = geneBinNoImage(list(9, 20), false);
		Node n = minRightSubtree(root.right);
		t(n.data, 20);
	    }
	    {
		/**
		               9   
			           20  
				       30
                               
                                       
		 */
		Node root = geneBinNoImage(list(9, 20, 30), false);
		Node n = minRightSubtree(root.right);
		t(n.data, 20);
	    }
	    {
		/**
		               9   
			  4        20  
			     6	       30
                               
                                       
		 */
		Node root = geneBinNoImage(list(9, 20, 30, 4, 6), false);
		Node n = minRightSubtree(root.right);
		t(n.data, 20);
	    }
	    {
		/**
		                   9   
			  [4]           20  
			     6	   [11]      30
                               
                                       
		 */
		Node root = geneBinNoImage(list(9, 20, 30, 4, 6, 11), false);
		Node n = minRightSubtree(root.right);
		t(n.data, 11);
	    }
	}
	end();
    }
    public static void test_isBSTRecurve(){
	beg();
	{
	    {
		Node root = null;
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = new Node(9);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4, 12), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4, 1, 10, 20, 8), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(2, 6, 3, 40, 26, 9, 4, 1, 10, 20, 8), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = node(9);
		Node[] arr = new Node[1];
		root.right = node(2);
		Boolean b = isBSTRecurve(root, arr);
		t(b, false);
	    }
	    {
		/**
		          9
                      20
		 */
		Node root = node(9);
		Node[] arr = new Node[1];
		root.left = node(20);
		Boolean b = isBSTRecurve(root, arr);
		t(b, false);
	    }
	    {
		/**
		             9
			 20      30
		     
		 */
		
		Node root = node(9);
		root.left = node(20);
		Node[] arr = new Node[1];
		root.right = node(30);
		Boolean b = isBSTRecurve(root, arr);
		t(b, false);
		
	    }
	    {
		
		Node root = new Node(9);
		Node[] arr = new Node[1];
		root.left = new Node(20);
		root.left.left = new Node(1);
		Boolean b = isBSTRecurve(root, arr);
		printTree(root);
		t(b, false);
	    }
	}
    }
    public static void rotateArray(int[][] arr){
	if(arr != null){
	    int heigh = arr.length;
	    int width = arr[0].length;
	    for(int h = 0; h < heigh; h++){
		for( int w = 0; w < width/2; w++){
		    int tmp = arr[w][h];
		    arr[w][h] = arr[width - 1 - w][h];
		    arr[width - 1 - w][h] = tmp;
		}
	    }
	    /**
	            1 2 3 4
                      4 5 6
                        7 8
                          9
             */
	    for(int h = 0; h < heigh; h++){
		for( int w = h; w < width; w++){
		    int tmp = arr[h][w];
		    arr[h][w] = arr[w][h];
		    arr[w][h] = tmp;
		}
	    }
	    
	}
    }

    // i j k l
    // TODO:
    /**
                    9
               5         12
           2      6    10    14


                   9
                5
	      2
	   
     */
    public static Node leaseCommonAncestor(Node root, Node n1, Node n2){
	if(root != null){
	    if(root.data == n1.data || root.data == n2.data)
		return root;
	    
	    Node l = leaseCommonAncestor(root.left, n1, n2);
	    Node r = leaseCommonAncestor(root.right, n1, n2);
	    if(l != null && r != null)
		return root;
	    else if( l != null)
		return l;
	    else
		return r;
	}
	return null;
    }
    
    public static void multiLongInteger(int[][] arr1, int[][] arr2){
    }

    /**
         9 8
         7 9
         5 8
         (1, 7)  => (1, 7) => 7
         (1, 6)  => (1, 8) => 8
	 (1, 3)  => (1, 4) => 4


	 
              (1, 7)
              (0, 0)  => (1, 7) => 7
	                 (1, 6) => (1, 7) => 7
                                   (1, 3) => (1, 4) => 4
                                             (0, 0) => (0, 1) => 1
              
              
         
         9 8
           8  
          (1, 6)  => 6
          (0, 9)  => (1, 0) => 0
                  => 1 0 6

           
     */
    public static int[] sumLongInteger(int[] arr1, int[] arr2){
	int[] sum = new int[Math.max(len(arr1), len(arr2))];
	var s1 = arrayToList(arr1);
	var s2 = arrayToList(arr2);
	BiFunction<Integer, Integer, Tuple> f = (x, y) -> divMod(x + y, 10);
	var ls = zipWith(f, s1, s2);
	
	return sum;
    }

    public static List<Integer> coinChange(List<Integer> ls, Integer s, List<Integer> currList, List<Integer> minList){
	List<Integer> minCoins = new ArrayList<>();
	if(sum(currList) == s){
	    if(len(minList) == 0){
		for(Integer n : currList)
		    minList.add(n);
	    }else if(len(currList) < len(minList)){
		empty(minList);
		for(Integer n : currList)
		    minList.add(n);
	    }
	    // p(minList);
	}else{
	    for(int i = 0; i < len(ls); i++){
		coinChange(removeIndex(i, ls), s, append(currList, ls.get(i)), minList); 
	    }
	}
	return minCoins;
    }
    /**
       TODO:
       Use definition:
       1. null is BST
       2. Left subtree is BST
       3. Right subtree is BST
       4. max(left subtree) < parent < min(right subtree)
     */
    public static Boolean isBST(Node root){
	if(root != null){
	    if(!isBST(root.left))
		return false;
	    
	    if(root.left != null && maxLeftSubtree(root.left).data > root.data)
		return false;
	    
	    if(root.right != null && minRightSubtree(root.right).data < root.data)
		return false;
	    
	    if(!isBST(root.right))
		return false;
	}
	return true;
    }

    public static Node maxLeftSubtree(Node r){
	Node curr = r;
	while(curr != null && curr.right != null)
		curr = curr.right;
	
	return curr;
    }

    public static Node minRightSubtree(Node r){
	Node curr = r;
	while(curr != null && curr.left != null)
	    curr = curr.left;
	
	return curr;
    }
    /**
       Similar to coin change problem
     */
    public static List<String> longPathWords(String word, List<String> ls){
	List<String> ret = new ArrayList<>();
	return ret;
    }
    
    public static Boolean diffOneChar(String s1, String s2){
	Boolean ret = false;
	if(len(s1) + 1 == len(s2)){
	    int k = 0;
	    for(int i=0; i < len(s2) && k + 1 != len(s2); i++){
		if(s1.charAt(k) == s2.charAt(i))
		    k++;
	    }
	    ret = k + 1 == len(s2);
	}else if(len(s1) == len(s2) + 1){
	    int k = 0;
	    for(int i=0; i < len(s1) && k + 1 != len(s1); i++){
		if(s2.charAt(k) == s1.charAt(i))
		    k++;
	    }
	    ret = k + 1 == len(s1);
	}
	return ret;
    }
    
    public static Boolean diffOneChar2(String s1, String s2){
	int len1 = len(s1);
	int len2 = len(s2);
	int i = 0, j = 0;
	int c = 0;
	if(abs(len1 - len2) == 1){
	    while(i < len1 && j < len2){
		if(s1.charAt(i) == s2.charAt(j)){
		    i++;
		    j++;
		}else{
		    if(len1 > len2){
			i++;
			c++;
		    }
		    else{
			j++;
			c++;
		    }
		    
		}
	    }
	    return (c == 1 && (len1 - i == 1 || len2 - j == 1)) || (len1 - i == 0 && len2 - j == 1) && (len1 - i == 1 && len2 - j == 0);
	}else{
	    return false;
	}	
    }
} 

