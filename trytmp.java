import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import static classfile.AnsiColor.*;
import java.util.stream.*;
import java.util.stream.Collectors;

enum Token{
        BracketLeftSq,
        BracketRightSq,
        BracketLeft,
        BracketRight,
        BracketLeftAngle,
        BracketRightAngle,
        Equal,
        Period,
        Comma,
        Colon,
        Semicolon,
        DoubleQuote,
        Letter,
        Digit,
        Space 
}

class Bin{
    Node root;
}

class Node9{
    Node9 parent;
    Node9 left;
    Node9 right;
    int data;
    public Node9(int data, Node9 parent){
        this.data = data;
        this.parent = parent;
    }
}

class XY{
	int x;
	int y;
	public XY(int x, int y){
		this.x = x;
		this.y = y;
	}
	@Override
	public String toString(){
		return "(" + x + " " + y + ")";
	}
}

class TNode{
    Map<Character, TNode> map;
    boolean isWord;
    public TNode(boolean isWord){
        map = new HashMap<>();
        this.isWord = isWord;
    }
}


record intArr(int inxOfNonZero, int[] arr){};

class Point{
    Integer x;
    Integer y;
    public Point(Integer x, Integer y){
        this.x = x;
        this.y = y;
    }
}

class Seg{
    Point beg;
    Point end;
    public Seg(Point beg, Point end){
        this.beg = beg;
        this.end = end;
    }
}


class Edge{
    String beg;
    String end;
    public Edge(String beg, String end){
        this.beg = beg;
        this.end = end;
    }
}

class ANode{
    Map<Character, ANode> map = new HashMap<>();
    boolean isEnd;
    public ANode(boolean isEnd){
        this.isEnd = isEnd;
    }
}

class NodeX{
    Integer data;
    NodeX next;
    NodeX prev;
    public NodeX(Integer data){
        this.data = data;
    }
}

class Linked{
    NodeX first;
    NodeX last;
    public Linked(){
    }
    public int size(){
        NodeX curr = first;
        int k = 0;
        while(curr != null){
            k++;
            curr = curr.next;
        }
        return k;
    }
    public void insert(NodeX node){
        if(first == null){
            first = last = node;
        }else{
            last.next = node;
            node.prev = last;
            last = node;
        }
    }
    public void print(){
        NodeX curr = first;
        while(curr != null){
            pl(curr.data);
            curr = curr.next;
        }
    }

    public void removeHead(){
        if(first != null){
            NodeX next = first.next;
            first.next = null;
            if(next != null){
                next.prev = null;
                first = next;
            }else{
                first = last = null;
            }
        }
    }
    public void removeTail(){
        if(last != null){
            NodeX prev = last.prev;
            last.prev = null;
            if(prev != null){
                prev.next = null;
                last = prev;
            }else{
                first = last = null;
            }
        }
    }
    public void delete(Integer n){
        NodeX curr = first;
        while(curr != null){

            if(curr.data == n){
                // head
                if(curr.prev == null) {
                    removeHead();
                    break;
                }else if(curr.next == null){
                    // tail
                    removeTail();
                    break;
                }else{
                    // "middle" node
                    NodeX prev = curr.prev;
                    NodeX next = curr.next;
                    prev.next = next;
                    next.prev = prev;
                    curr.next = null;
                    curr.prev = null;
                    break;
                }
            }

            curr = curr.next;
        }
    }
}

class AddSearchStr{
    ANode root = new ANode(true);
    public AddSearchStr(){
    }

    public void addWord(String word){
        ANode curr = root;
        for(int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
               ANode next = curr.map.get(c); 
            if(next != null){
               curr = next; 
            }else{
                ANode node = new ANode( i == word.length() - 1 ? true : false);
                curr.map.put(c, node);
                curr = node;
            }
        }
        if(curr != null){
            curr.isEnd = true;
        }
    }


    /**
        
             . 

             a
          b     c

    */
    public boolean search(String word){
        boolean ret = false;
        ANode curr = root;
        for(int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            if(c == '.'){
                    
            }else{
                ANode next = curr.map.get(c); 
                if(next != null){
                    curr = next;
                }else{
                    return false; 
                }
            }
        }
        ret = curr.isEnd;
        return ret;
    }
};

public class trytmp{
    public static void main(String[] args) {
        test0();
        test1();
		test2();
    }
    public static void test0(){
        beg();
        end();
    }


    public static boolean searchWord(ANode curr, String s, int i){
        if(i < s.length()){
            char c = s.charAt(i);    
            ANode next = curr.map.get(c); 
            if(next != null){
                return searchWord(next, s, i + 1);
            }else{
                return false;
            }
        }else{
            return curr.isEnd;
        }
    }

    public static boolean searchWord2(ANode curr, String s, int i){
        if(i < s.length()){
            char c = s.charAt(i);
            if(c != '.'){
                ANode next = curr.map.get(c);
                if( next != null){
                    return searchWord2(next, s, i + 1);        
                }else{
                    return false;
                }
            }else{
                if(curr.map.size() > 0){
                    for(Map.Entry<Character, ANode> entry : curr.map.entrySet()){
                        return searchWord2(entry.getValue(), s, i+1);
                    }
                }
                return false;
            }
        }else{
            return curr.isEnd;
        }
    }
    
	   
	public static int partitionX(int[] arr, int lo, int hi){
		int len = arr.length;
		int[] tmpArr = new int[len];
		int leftInx = 0;
		int rightInx = len - 1;
		if(len > 1 ){
			int pivot = arr[len - 1];
			int k = 0;
			while(leftInx != rightInx){
				if(arr[k] <= pivot){
					tmpArr[leftInx] = arr[k];
					leftInx++;
				}else{
					tmpArr[rightInx] = arr[k];
					rightInx--;
				}
				k++;
			}
			tmpArr[leftInx] = pivot;
		}

		for(int i = 0; i < len; i++){
			arr[lo + i] = tmpArr[i];
		}
		return leftInx;
	}

	
	/**
    <pre>
    {@literal
	   === KEY:
    }
    {@code
    }
    </pre>
    */
	public static intArr moveNonZeroToTheRightX(int[] arr){
		int[] retArr = copyArrayint(arr);
		fl("");
		printArrInt(retArr);
		fl("");
		int bigIndex = 0;
		int pivot = 0;
		for(int i = 0; i < retArr.length; i++){
			if(retArr[i] <= pivot){
				swap(retArr, bigIndex, i);
				pl("s (" + bigIndex + " " + i + ")");
				pl(" retArr=" + retArr[i] + " " + pivot);
			}else{
				bigIndex++;
			}
		}
		return new intArr(bigIndex, retArr);
	}

    /**
    <pre>
    {@literal

    }
    {@code
        fl("moveNonZeroToTheRight 7");
        int[] arr = {0, 1, 0, 1};
        int len = arr.length;
        int n = moveNonZeroToTheRight(arr);
        int[] expectedArr = {0, 0, 1, 1};
        t(expectedArr, arr);
    }
    </pre>
    */ 
    public static int moveNonZeroToTheRight(int[] arr){
        int ni = 0;
        int zi = 0;
        int len = arr.length;
        while(ni < len && zi < len){
            if(arr[ni] == 0 && arr[zi] == 0){
                ni++;
                zi++; 
            }else{
                if(arr[ni] != 0 && arr[zi] == 0){
                    swap(arr, ni, zi);
                        ni++;
                        zi++;
                }else{
                    if(arr[ni] == 0){
                        ni++;
                    }
                    if(arr[zi] != 0){
                        zi++;
                    }
                }
            }
        }
        return 0;
    }

	public static int moveNonZeroToTheRightFB(int[] arr){
		int bigIndex = 0;
		int pivot = 0;
        int len = arr.length;
		for(int i = 0; i < len; i++){
			if(arr[i] <= pivot){
				swap(arr, bigIndex, i);
                // if(i < len - 1)
				   bigIndex++;
			}
		}
		return len - bigIndex;
	}


	/**
    <pre>
    {@literal
	   === KEY: partition an array
    }
    {@code

        // move all zeros to the right side of array
        // [1, 0, 3, 0] => [1, 3, 0, 0]

        BUG: failed
        TODO: Fixed it
        fl("partitionfb 2");
        int[] arr = {1, 0};
        int len = arr.length;
        int n = partitionfb(arr);
        int[] expectedArr = {0, 1};
        // printArrint(arr);
        t(expectedArr, arr);

        URL: http://localhost/html/indexFacebookInterviewQuestion.html 
    }
    </pre>
    */
    public static int partitionfb(int[] arr){
        int zeroIndex = 0;
        int len = arr.length;
        for(int i=0; i<len; i++){
            if(arr[i] > 0){
                if(i != zeroIndex)
                    swap(arr, i, zeroIndex);
                // make sure the zeroIndex is not out of bounds
                if(zeroIndex < len-1)
                    zeroIndex++;
            }
        }
        return 0;
    }


	/**
    <pre>
    {@literal
	   === KEY: partition an array
    }
    {@code
        fl("partition4 2");
        int[] arr = {1, 4, 3};
        int len = arr.length;
        int lo = 0;
        int hi = len - 1;
        int pivotInx = partition4(arr, lo, hi);
        pl("pivotInx=" + pivotInx);
        printArrint(arr);
    }
    </pre>
    */
    public static int partition4(int[] arr, int lo, int hi){
        int len = arr.length;
        int[] retArr = new int[len];
        int leftInx = 0;
        int rightInx = len - 1;
        int pivot = arr[hi];
        int k = lo;
        while(leftInx != rightInx){
            retArr[arr[k] <= pivot ? leftInx++ : rightInx--] = arr[k++];
        }

        retArr[leftInx] = pivot;

        // copy back to arr from retArr
        for(int i = 0; i < len; i++){
            arr[lo + i] = retArr[i];
        }
        // pivot index == leftInx == rightInx
        return leftInx;
    }

	/**
    <pre>
    {@literal
	   === KEY: Move all non zero to the left side 

              x
          2 0 0
              x

            x 
          2 0 0
            x  

          x   
          2 0 0
          x    

          x   
      0 3 2 0 0
        x     
          x   
      0 3 2 0 0
      x       
        x 
      2 3 0 0 0
    x
    }
    {@code
    }
    </pre>
    */
    public static int partitionFB2(Integer[] arr){
        int len = arr.length;
        int countZero = 0;
        int nonzero = len-1;
        for(int i=len-1; i >= 0; i--){
            if(arr[i] == 0){
                swap(arr, i, nonzero);
                countZero++;
                nonzero--;
            }
        }
        return len - countZero; 
    }



    public static int[][] setMatrix(int[][] arr){
        int height = arr.length;
        int width = arr[0].length; 

        List<Tuple<Integer, Integer>> ls = new ArrayList<>();
        Set<Integer> setRow = new HashSet<>();
        Set<Integer> setCol = new HashSet<>();

        for(int i = 0; i < height; i++){
            for(int j = -1; j < width; j++){
                if(arr[i][j] == 0){
                    setCol.add(j);
                    setRow.add(i);
                }
            }
        }
        for(Integer r : setRow){
            for(int i = 0; i < width; i++){
                arr[r][i] = 0;
            }
        }

        for(Integer c : setCol){
            for(int i = 0; i < height; i++){
                arr[i][c] = 0;
            }
        }
        return null;

    }

    public static boolean wordBreak(Set<String> dict, String word){
        boolean ret = false;
        int len = word.length();

        for(int i = 0; i < len; i++){
            String prefix = word.substring(0, i + 1);
            String suffix = word.substring(i + 1, len);
            pl("prefix=" + prefix);
            pl("suffix=" + suffix);
            if(dict.contains(prefix)){
                if(suffix.length() > 0){
                    return wordBreak(dict, suffix);
                }else{
                    return true;
                }
                
            }
        }
        return ret;
    }

    public static List<String> allsubstrings(String s){
        List<String> ls = new ArrayList<>(); 
        int len = s.length();
        for(int j = 0; j < len; j++){
            for(int i = 0; i < len; i++){
                if(i + 1 >= j){
                    String sub = s.substring(j, i + 1);
                    if(sub.length() > 0){
                        ls.add(sub);
                    }
                }
            }
        }
        return ls;
    }

    public static List<String> prefixSuffix2(String s){
        List<String> ls = new ArrayList<>(); 
        int len = s.length();
        for(int i = 0; i < len; i++){
            String prefix = s.substring(0, i+1);
            String suffix = s.substring(i+1, len);
            if(prefix.length() > 0){
                ls.add(prefix);
            }

            if(suffix.length() > 0){
                ls.add(suffix);
            }
        }
        return ls;

    }

    public static boolean pacificOcean(int[][] arr, int curr, int h, int w){
        int height = arr.length;
        int width = arr[0].length;
        boolean left = false;
        boolean up = false;
        boolean right = false;
        boolean down = false;

        // move left 
        if(w == 0){
            left = true; 
        }else{
            if(w - 1 >= 0){
                if(curr >= arr[h][w-1]){
                    left = pacificOcean(arr, curr, h, w - 1);
                }else{
                    left = false;
                }
            }
        }

        // move up
        if(h == 0){
            up = true;
        }else{
            if(h - 1 >= 0){
                if(curr >= arr[h - 1][w]){
                    up = pacificOcean(arr, curr, h - 1, w);
                }else{
                    up = false;
                }
            }
        }
        

        // move right
        if(w == width - 1){
            right = true;  // right edge
        }else{

            pl("h=" + h);
            pl("w=" + w);
            pl("width=" + width);
            pl("height=" + height);
             
            // if(w + 1 <= width - 1){
                // if(curr >= arr[h][w + 1]){
                    // right = pacificOcean(arr, curr, h, w + 1);
                // }else{
                    // right = false;
                // }
            // }
        }

        return (left || up) && right;
    }

    public static Map<Character, Integer> strCharCount(String s){
        int len = s.length();

        Map<Character, Integer> map = new HashMap<>();
        for(int i = 0; i < len; i++){
            char c = s.charAt(i);
            Integer k = map.get(c);    
            if(k != null){
                map.put(c, k + 1);
            }else{
                map.put(c, 1);
            }
        }
        return map;
    }

    public static int maxRepeatingChar(String s, int k){
            List<String> ls = allsubstrings(s); 
            int maxRepeating = 0;

            for(String e : ls){
                Map<Character, Integer> map = frequencyCount(e);

                int max = 0;
                for(Map.Entry<Character, Integer> entry : map.entrySet()){
                    pl(entry.getKey());
                    pl(entry.getValue());

                    if(entry.getValue() > max){
                        max = entry.getValue();
                    }
                }
                int maxRep = len(e);
                int diff = maxRep - max;

                if(k == diff){
                    if(maxRep > maxRepeating){
                        maxRepeating = maxRep;
                    }
                }

                pl(map);
            }
            return maxRepeating;
    }

    public static List<Integer> toBinary(Integer n){
        List<Integer> ls = new ArrayList<>();
        if(n == 0){
            ls.add(n);
        }else{
            while(n > 0){
                Integer r = n % 2;
                Integer q = n / 2;
                ls.add(r);
                n = q;
            }
        }
        return ls;
    }

    /**
     *
     *     d  o  g
     *        x
     *
     *        o g
     *        o d
     *
     *
     *         x
     *         x
     *     x x A x x
     *         x
     *         x
     *
     *   d -> y
     *   d o -> y
     *   d o g -> y
     */
    public static void wordSearchRight(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(w + 1 < width){
                wordSearchRight(arr, h, w + 1, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }
    public static void wordSearchLeft(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = (c + "") + s;

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(w - 1 >= 0){
                wordSearchLeft(arr, h, w - 1, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }
    public static void wordSearchUp(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(h - 1 >= 0){
                wordSearchUp(arr, h - 1, w, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }
    public static void wordSearchDown(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(h + 1 < height){
                wordSearchDown(arr, h + 1, w, ss, set, dict);
            }
            arr[h][w] = c;
        }
    }

    public static void wordSearch(char[][] arr,  int h, int w, String s, Set<String> set, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        
        char c = arr[h][w];
        if(c != '0'){
            arr[h][w] = '0';
            String ss = s + (c + "");

            if(dict.contains(ss)){
               set.add(ss); 
            }

            if(h + 1 < height){
                wordSearch(arr, h + 1, w, ss, set, dict);
            }

            if(h - 1 >= 0){
                wordSearch(arr, h - 1, w, ss, set, dict);
            }

            if(w - 1 >= 0){
                wordSearch(arr, h, w - 1, ss, set, dict);
            }

            if(w + 1 < width){
                wordSearch(arr, h, w + 1, ss, set, dict);
            }

            arr[h][w] = c;
        }
    }

    public static Set<String> wordSearchAll(char[][] arr, Set<String> dict){
        int height = arr.length;
        int width = arr[0].length;
        String ss = "";
        Set<String> set = new HashSet<>();

        dict.add("k");
        dict.add("kecccb");
        dict.add("kecccbabc");
        dict.add("a");
        dict.add("ab");
        dict.add("abk");
        dict.add("abke");
        dict.add("abcccecbk");

        dict.add("abb");
        dict.add("akc");


        // char[][] arr = {
            // {'a', 'b', 'c'},
            // {'b', 'k', 'c'},
            // {'c', 'e', 'c'}
        // };

        for(int h = 0; h < height; h++){
            for(int w = 0; w < width; w++){
                wordSearch(arr, h, w, ss, set, dict);
            }
        }
        pl(set);
        return set;
    }

    public static void inorder(Node r){
        if(r != null){
            inorder(r.left);
            pl("data=" + r.data);
            inorder(r.right);
        }
    }

    public static void preorder(Node r){
        if(r != null){
            pl("data=" + r.data);
            preorder(r.left);
            preorder(r.right);
        }
    }
    public static void postorder(Node r){
        if(r != null){
            postorder(r.left);
            postorder(r.right);
            pl("data=" + r.data);
        }
    }

    public static int[] shiftArrLeft(int[] arr, int n){
        int[] retArr = Arrays.copyOf(arr, arr.length);
        int len = retArr.length;
        int k = 0;
        while(k < n){
            for(int i = 0; i < len - 1; i++){
                swap(retArr, i, (i + 1) % len);    
            }
            k++;
        }
        return retArr;
    }

    public static int[] shiftArrRight(int[] arr, int n){
        int[] retArr = Arrays.copyOf(arr, arr.length);
        int len = retArr.length;
        int k = 0;
        while(k < n){
            for(int i = len - 1; i > 0; i--){
                swap(retArr, i, (i - 1) % len);    
            }
            k++;
        }
        return retArr;
    }

    public static Integer[] shiftArrLeftInteger(Integer[] arr, int n){
        Integer[] retArr = Arrays.copyOf(arr, arr.length);
        int len = retArr.length;
        int k = 0;
        while(k < n){
            for(int i = 0; i < len - 1; i++){
                swap(retArr, i, (i + 1) % len);    
            }
            k++;
        }
        return retArr;
    }

    public static void printAllPath(Node r){
           List<Integer> ls = new ArrayList<>(); 
           allPath(r, ls);
    }

    public static void allPath(Node r, List<Integer> ls){
        if(r != null){
            if(r.left == null && r.right == null){
                printList(append(ls, r.data));
            }else{
                allPath(r.left,  append(ls, r.data));
                allPath(r.right, append(ls, r.data));
            }
            
        }
    }

    /**
     *                                (2 3 5)
     *                             2     3       5 
     *                         (3 5)       (2 5)        (2 3)
     *                      3     5      2     5       2       3
     *                   5         3   5        2   3            2 
     *
     */
    public static void coinChange(List<Integer> ls, Integer sum, List<Integer> sumls, Set<List<Integer>> set){
        if(sum == 0){
            Collections.sort(sumls);
            set.add(sumls);
            // printList(sumls);
        }else if (sum > 0) {
            for(int i = 0; i < ls.size(); i++){
                List<Integer> lt = append(sumls, ls.get(i));
                coinChange(removeIndex(i, ls), sum - ls.get(i), lt, set);
            }
        }
    }

    public static void uniqueRandomNum(){
        Random r = new Random();
        int len = 10;
        int[] arr = new int[len];
        for(int i = 0; i < arr.length; i++){
            arr[i] = i;
        }

        for(int i = 0; i < arr.length; i++){
            int n = r.nextInt(len) % (len - i);
            pl("len - i =" + (len - i));
            pl("len - i =" + (len - i));
            pl("n=" + n);
            pl("len - i - 1 = " + (len - i - 1));
            swap(arr, (len - i - 1), n);    
        }

        printArrint(arr);
    }

    public static List<Integer> mergeList(List<Integer> s1, List<Integer> s2){
       List<Integer> ls = new ArrayList<>(); 
       int i = 0;
       int j = 0;

       while(i < s1.size() || j < s2.size()){
          if(i >= s1.size()){
            ls.add(s2.get(j));
            j++;
          }else if(j >= s2.size()){
            ls.add(s1.get(i));
            i++;
          }else if(s1.get(i) < s2.get(j)){
              ls.add(s1.get(i));
              i++;
          }else{
              ls.add(s2.get(j));
              j++;
          }
       }
       return ls;
    }

    public static int[] mergeListArr(int[] arr1, int[] arr2){
        int len1 = arr1.length;
        int len2 = arr2.length;
        int[] arr = new int[len1 + len2];
        int i = 0, j = 0;
        int k = 0;
        while(i < len1 || j < len2){
            if(i >= len1){
                arr[k] = arr2[j];
                j++; 
            }else if(j >= len2){
                arr[k] = arr1[i];
                i++;
            }else {
                if(arr1[i] < arr2[j]){
                    arr[k] = arr1[i];
                    i++;
                }else{
                    arr[k] = arr2[j];
                    j++;
                }
            }
            k++;
        }
        return arr;
    }

    public static int[] mergeListArr2(int[] a, int[] b){
        int lena = a.length;
        int lenb = b.length;
        int[] arr = new int[lena + lenb];
        int ax = 0;
        int bx = 0;
        int k = 0;
        while(ax < lena || bx < lenb){
            if(ax >= lena){
                arr[k] = b[bx];
                bx++;
            }else if(bx >= lenb){
                arr[k] = a[ax];
                ax++;
            }else {
                if(a[ax] < b[bx]){
                    arr[k] = a[ax];
                    ax++;
                }else {
                    arr[k] = b[bx];
                    bx++;
                }
            }
            k++;
        }
        return arr;
    }

    public static int[] mergeListArr3(int[] a, int[] b){
        int lena = a.length;
        int lenb = b.length;
        int[] arr = new int[lena + lenb];
        int i = 0, j = 0, k = 0;
        while(i < lena || j < lenb){
            if(i >= lena){
                arr[k] = b[j++];
            }else if(j >= lenb){
                arr[k] = a[i++];
            }else{
                arr[k] = a[i] < b[j] ? a[i++] : b[j++]; 
            }
            k++;
        }
        return arr;
    }

    public static void mergeListArr5(int[] a, int lo, int mid, int hi){
        int[] arr = new int[hi - lo + 1];
        int i = lo, j = mid + 1, k = 0;
        while(i <= mid || j <= hi){
            if(i > mid){
                arr[k] = a[j++];
            }else if(j > hi){
                arr[k] = a[i++];
            }else {
                if(a[i] < a[j]){
                    arr[k] = a[i++];
                }else{
                    arr[k] = a[j++];
                }
            }
            k++;
        }

        for(int x = 0; x < arr.length; x++){
            a[lo + x] = arr[x];
        }
    }

    public static void mergeSort(int[] arr, int lo, int hi){
        if(lo < hi){
            int mid = (lo + hi)/2;
            mergeSort(arr, lo, mid);
            mergeSort(arr, mid + 1, hi);
            mergeListArr5(arr, lo, mid, hi);
        }
    }
    

    public static int part(int[] arr, int lo, int hi){
        int pivot = arr[hi];
        int len = hi - lo + 1;
        int[] tmpArr = new int[len];
        int i = 0, j = len - 1, k = lo;

        while(i != j){
            tmpArr[ arr[k] <= pivot ? i++ : j--] = arr[k];
            k++;
        }
        tmpArr[i] = pivot;

        for(int x = 0; x < tmpArr.length; x++){
            arr[lo + x] = tmpArr[x];
        }

        return (lo + i);
    }

    public static void quickSort(int[] arr, int lo, int hi){
        if(lo < hi){
            int pivotInx = part(arr, lo, hi);
            pl("lo=" + lo);
            pl("hi=" + hi);
            pl("pivotInx=" + pivotInx);

            quickSort(arr, lo, pivotInx - 1);
            quickSort(arr, pivotInx + 1, hi);
        }
    }

    public static int getPivot(int[] arr, int lo, int hi){
        int bigInx = lo;
        int pivotInx = lo;
        int pivot = arr[hi];
        for(int i = lo; i <= hi; i++){
            if(arr[i] <= pivot){
                swap(arr, bigInx, i);
                if(i < hi){
                    bigInx++;
                }
            }
        }
        return bigInx;
    }

    public static int[] sumList(int[] a, int[] b){
        int la = a.length;
        int lb = b.length;
        int len = Math.max(la, lb) + 1; 
        int[] arr = new int[len];

        int[] a1 = concatArr(repeatArrint(len - la, 0), a);
        int[] b1 = concatArr(repeatArrint(len - lb, 0), b);

        int c = 0;
        for(int i = len - 1; i >= 0; i--){
            int s = a1[i] + b1[i] + c;
            arr[i] = s % 10;
            c = s / 10;
        }

        return arr;
    }

    public static int[] multiArr(int n, int[] arr){
        int[] ret = new int[arr.length];
        for(int i = 0; i < arr.length; i++){
            arr[i] = n*arr[i];
        }
        return arr;
    }
    public static int[] multiplyList(int[] a, int[] b){
        int la = a.length;
        int lb = b.length;
        int len = la + lb;

        int[][] arr = new int[la][len];
        for(int i = 0; i < la; i++){
            int[] bb = multiArr(a[i], b); 
            int[] pad = repeatArrint(i, 0);
        }


        return null;
    }

    public static void test1(){
        beg();
		{
			/*
			{
				fl("moveNonZeroToTheRightX 1");
				int[] arr = {-1, 0, 3};
				intArr intarr = moveNonZeroToTheRightX(arr);
				pl("inxOfNonzero=" + intarr.inxOfNonZero());
				printArrInt(intarr.arr());
			}
			*/
			{
				fl("moveNonZeroToTheRightX 3");
				int[] arr = {-1, -2, 0, 0, 3};
				intArr intarr = moveNonZeroToTheRightX(arr);
				pl("inxOfNonZero=" + intarr.inxOfNonZero());
				printArrInt(intarr.arr());
			}
			
			{
				fl("moveNonZeroToTheRightX 2");
				int[] arr = {-1, -2, 0, 3};
				intArr intarr = moveNonZeroToTheRightX(arr);
				pl("inxOfNonZero=" + intarr.inxOfNonZero());
				printArrInt(intarr.arr());
			}
			{
				fl("");
				int[] arr = {1, 2, 0, 3, 0, 2, 5, 1, 1, 4, 3, 1};
				int lo = 0;
				int hi = arr.length - 1;
				int pivotInx = partitionX(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArrint(arr);
			}
			{
				fl("partitionWithCountSwap 1");
				int[] arr = {1, 2, 0, 3, 0, 2, 5, 1, 1, 4, 3, 1};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
                int[] expectedArr = {1, 0, 0, 1, 1, 1, 5, 3, 2, 4, 3, 2};
				Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
				pl("pivotInx=" + t.x);
				pl("numOfswap=" + t.y);
                t(t.x, 6);
                t(t.y, 6);
                t(expectedArr, arr);
				printArrint(arr);
				
			}
			{
				fl("partitionWithCountSwap 2");
				int[] arr = {2, 1};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
                int[] expectedArr = {1, 2};
				Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
				pl("pivotInx=" + t.x);
				pl("numOfswap=" + t.y);
                t(t.x, 1);
                t(t.y, 1);
                t(expectedArr, arr);
				
			}
			{
				fl("partitionWithCountSwap 3");
				int[] arr = {2, 1, 1};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
                int[] expectedArr = {1, 1, 2};
				Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
				pl("pivotInx=" + t.x);
				pl("numOfswap=" + t.y);
				printArrint(arr);
                t(t.x, 2);
                t(t.y, 2);
                t(expectedArr, arr);
				
			}
			{
				fl("partitionWithCountSwap 4");
				int[] arr = {1};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
				Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
				pl("pivotInx=" + t.x);
				pl("numOfswap=" + t.y);
                t(t.x, 0);
                t(t.y, 1);
			}
		}
        {
            {
				fl("partition4 0");
				int[] arr = {1};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
				int pivotInx = partition4(arr, lo, hi);
                int[] expectedArr = {1};
				pl("pivotInx=" + pivotInx);
				printArrint(arr);
                t(pivotInx, 0);
                t(expectedArr, arr);
            }
            {
				fl("partition4 1");
				int[] arr = {1, 4};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
				int pivotInx = partition4(arr, lo, hi);
				printArrint(arr);
                int[] expectedArr = {1, 4};
                t(pivotInx, 1);
                t(expectedArr, arr);
            }
            {
				fl("partition4 2");
				int[] arr = {1, 4, 3};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
				int pivotInx = partition4(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArrint(arr);

                int[] expectedArr = {1, 3, 4};
                t(pivotInx, 1);
                t(expectedArr, arr);
            }
            {
				fl("partition4 2");
				int[] arr = {1, 4, 3, 2};
				int len = arr.length;
				int lo = 0;
				int hi = len - 1;
				int pivotInx = partition4(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArrint(arr);
            }
        }
        {
            {
				fl("moveNonZeroToTheRight 1");
				int[] arr = {1};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
                int[] expectedArr = {1};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 2");
				int[] arr = {1, 1};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
                int[] expectedArr = {1, 1};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 3");
				int[] arr = {0};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
                int[] expectedArr = {0};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 4");
				int[] arr = {0, 0};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
                int[] expectedArr = {0, 0};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 5");
				int[] arr = {1, 0};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
                int[] expectedArr = {0, 1};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 6");
				int[] arr = {0, 1, 0};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
                int[] expectedArr = {0, 0, 1};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 7");
				int[] arr = {0, 1, 0, 1};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
				int[] expectedArr = {0, 0, 1, 1};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 8");
				int[] arr = {0, 0, 1,  1, 0, 1};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
				int[] expectedArr = {0, 0, 0, 1, 1, 1};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 9");
				int[] arr = {0, 0, 1,  1, 0, 1, 0};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
				int[] expectedArr = {0, 0, 0, 0, 1, 1, 1};
                t(expectedArr, arr);
            }
            {
				fl("moveNonZeroToTheRight 10");
				int[] arr = {1, 1, 0, 0, 0, 1, 1, 1};
				int len = arr.length;
                int n = moveNonZeroToTheRight(arr);
				int[] expectedArr = {0, 0, 0, 1, 1, 1, 1, 1};
                t(expectedArr, arr);
            }

        }
        {
            {
				fl("moveNonZeroToTheRightFB 1");
				int[] arr = {1, 1, 0, 0, 0, 1, 1, 1};
				int len = arr.length;
                int n = moveNonZeroToTheRightFB(arr);
				int[] expectedArr = {0, 0, 0, 1, 1, 1, 1, 1};
                printArrint(arr);
                t(expectedArr, arr);
                t(n, 5);
            }
            {
				fl("moveNonZeroToTheRightFB 2");
				int[] arr = {1, 1, 0, 1, 1, 1};
				int len = arr.length;
                int n = moveNonZeroToTheRightFB(arr);
				int[] expectedArr = {0, 1, 1, 1, 1, 1};
                printArrint(arr);
                t(expectedArr, arr);
                t(n, 5);
            }
            {
				fl("moveNonZeroToTheRightFB 3");
				int[] arr = {0, 1, 0, 1, 0, 1};
				int len = arr.length;
                int n = moveNonZeroToTheRightFB(arr);
				int[] expectedArr = {0, 0, 0, 1, 1, 1};
                printArrint(arr);
                t(expectedArr, arr);
                t(n, 3);
            }
            {
				fl("moveNonZeroToTheRightFB 4");
				int[] arr = {0, 0, 1, 0, 0};
				int len = arr.length;
                int n = moveNonZeroToTheRightFB(arr);
				int[] expectedArr = {0, 0, 0, 0, 1};
                printArrint(arr);
                t(expectedArr, arr);
                t(n, 1);
            }
            {
				fl("moveNonZeroToTheRightFB 5");
				int[] arr = {0, 1, 0, 2, 0, 0, 3};
				int len = arr.length;
                int n = moveNonZeroToTheRightFB(arr);
				int[] expectedArr = {0, 0, 0, 0, 1, 2, 3};
                printArrint(arr);
                t(expectedArr, arr);
                t(n, 3);
            }
            {
				fl("moveNonZeroToTheRightFB 5");
				int[] arr = {0, 0};
				int len = arr.length;
                int n = moveNonZeroToTheRightFB(arr);
				int[] expectedArr = {0, 0};
                printArrint(arr);
                t(expectedArr, arr);
                t(n, 0);
            }
        }
        {
            {
				fl("partitionfb 1");
				int[] arr = {0};
				int len = arr.length;
                int n = partitionfb(arr);
				int[] expectedArr = {0};
                printArrint(arr);
                t(expectedArr, arr);
                t(n, 0);
            }
            {
				fl("partitionfb 2");
				int[] arr = {1, 0};
				int len = arr.length;
                int n = partitionfb(arr);
				int[] expectedArr = {1, 0};
                // printArrint(arr);
                t(expectedArr, arr);
                // t(n, 0);
            }
            {
				fl("partitionfb 3");
				int[] arr = {1, 0, 1};
				int len = arr.length;
                int n = partitionfb(arr);
				int[] expectedArr = {1, 1, 0};
                // printArrint(arr);
                t(expectedArr, arr);
                // t(n, 0);
            }
            {
				fl("partitionfb 4");
				int[] arr = {1, 0, 1, 0};
				int len = arr.length;
                int n = partitionfb(arr);
				int[] expectedArr = {1, 1, 0, 0};
                // printArrint(arr);
                t(expectedArr, arr);
                // t(n, 0);
            }
        }
        {
            {
				fl("partitionFB2 1");
                Integer[] arr = {2, 0, 3, 0};
                int n = partitionFB2(arr);
                t(n, 2);
                printArray(arr);
            }
            {
				fl("partitionFB2 2");
                Integer[] arr = {2, 0, 3};
                int n = partitionFB2(arr);
                t(n, 2);
                printArray(arr);
            }
            {
				fl("partitionFB2 3");
                Integer[] arr = {0, 0, 2, 0, 0, 3, 2, 0, 3};
                int n = partitionFB2(arr);
                t(n, 4);
                printArray(arr);
            }
            {
				fl("partitionFB2 4");
                Integer[] arr = {0, 0, 0, 0, 0};
                int n = partitionFB2(arr);
                t(n, 0);
                printArray(arr);
            } 
            {
				fl("partitionFB2 5");
                Integer[] arr = {32, 3};
                int n = partitionFB2(arr);
                t(n, 2);
                printArray(arr);
            } 

        }
        {

            {
                fl("wordBreak 1");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                boolean ret = wordBreak(dict, "dogcat");
                t(ret, true);
            }

            {
                fl("wordBreak 2");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("ccow");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, false);
            }

            {
                fl("wordBreak 3");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("dogatcow");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, false);
            }
            {
                fl("wordBreak 4");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("cow");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, true);
            }
            {
                fl("wordBreak 5");
                Set<String> dict = new HashSet<>();
                dict.add("dog");
                dict.add("cat");
                dict.add("cows");
                boolean ret = wordBreak(dict, "dogcatcow");
                t(ret, false);
            }
            {
                fl("wordBreak 7");
                Set<String> dict = new HashSet<>();
                dict.add("a");
                boolean ret = wordBreak(dict, "a");
                t(ret, true);
            }
            {
                fl("wordBreak 8");
                Set<String> dict = new HashSet<>();
                dict.add("a");
                dict.add("b");
                boolean ret = wordBreak(dict, "ab");
                t(ret, true);
            }

            {
                fl("wordBreak 9");
                Set<String> dict = new HashSet<>();
                dict.add("apple");
                dict.add("pen");
                boolean ret = wordBreak(dict, "applepenapple");
                t(ret, true);
            }
        }
        {
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("");
                boolean b = sc.search("");
                t(b, true);
            }
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("a");
                boolean b = sc.search("a");
                t(b, true);
            }
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("ab");
                boolean b = sc.search("ab");
                t(b, true);
            }
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("abc");
                boolean b = sc.search("abc");
                t(b, true);
            }
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("abc");
                sc.addWord("abcef");
                boolean b = sc.search("abc");
                t(b, true);
            }
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("abc");
                sc.addWord("abcef");
                boolean b = sc.search("abce");
                t(b, false);
            }
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("abc");
                sc.addWord("abcef");
                boolean b = sc.search("abcef");
                t(b, true);
            }
            {
                fl("AddSearchStr");
                AddSearchStr sc = new AddSearchStr();
                sc.addWord("abc");
                sc.addWord("abcef");
                sc.addWord("abce");
                boolean b = sc.search("abce");
                t(b, true);
            }
        }
        {
            {
				fl("pacificOcean 1");
                int[][] arr = {
                    {0},
                };

                int h = 0, w = 0;
                int curr = arr[h][w]; 
                boolean b = pacificOcean(arr, curr, h, w);
                t(b, true);
            }
            {
				fl("pacificOcean 2");
                int[][] arr = {
                    {1},
                };

                int h = 0, w = 0;
                int curr = arr[h][w]; 
                boolean b = pacificOcean(arr, curr, h, w);
                t(b, true);
            }

            {
				fl("pacificOcean 3");
                int[][] arr = {
                    {1, 3},
                };

                int h = 0, w = 1;
                int curr = arr[h][w]; 
                boolean b = pacificOcean(arr, curr, h, w);
                t(b, true);
            }

            {
				fl("pacificOcean 4");
                int[][] arr = {
                    {1, 3, 2},
                };

                int h = 0, w = 2;
                int curr = arr[h][w]; 
                boolean b = pacificOcean(arr, curr, h, w);
                t(b, true);
            }

            // {
				// fl("pacificOcean 5");
                // int[][] arr = {
                    // {1, 3, 2, 4},
                // };

                // int h = 0, w = 3;
                // int curr = arr[h][w]; 
                // boolean b = pacificOcean(arr, curr, h, w);
                // t(b, true);
            // }
        }
        {
            {
                fl("all substrings 1");
                String s = "dogcatcowfoxCostaRicaJamaica";
                List<String> ls = allsubstrings(s); 
                Collections.sort(ls, (x, y) -> y.compareTo(x));
                pl(ls);
                pl("len=" + len(ls));
            }
            {
                fl("prefix and suffix 2");
                String s = "dogcatcowfoxCostaRicaJamaica";
                List<String> ls = prefixSuffix2(s); 
                Collections.sort(ls, (x, y) -> y.compareTo(x));
                pl(ls);
                pl("len=" + len(ls));
            }
        }
        {
            {
                fl("strCharCount");
                String s = "banana";
                Map<Character, Integer> map = strCharCount(s);
                pl(map);
            }
        }
        {
            {
                fl("max repeating max length 1");
                String s = "banana";
                int k = 2;
                int max = maxRepeatingChar(s, k);
                pl("max=" + max);
            }
            {
                fl("max repeating max length 2");
                String s = "banannnn";
                int k = 3;
                int max = maxRepeatingChar(s, k);
                pl("max=" + max);
            }
        }
        {
            {
                fl("toBinary 1");
                Integer n = 0;
                List<Integer> ls = toBinary(n);
                pl(ls);
                t(ls, list(0));
            }
            {
                fl("toBinary 2");
                Integer n = 1;
                List<Integer> ls = toBinary(n);
                pl(ls);
                t(ls, list(1));
            }
            {
                fl("toBinary 3");
                Integer n = 2;
                List<Integer> ls = toBinary(n);
                pl(ls);
                t(ls, list(0, 1));
            }
            {
                fl("toBinary 4");
                Integer n = 3;
                List<Integer> ls = toBinary(n);
                pl(ls);
                t(ls, list(1, 1));
            }
            {
                fl("toBinary 5");
                Integer n = 5;
                List<Integer> ls = toBinary(n);
                pl(ls);
                t(ls, list(1, 0, 1));
            }
        }
        {
            {
                fl("wordSearchRight 1");
                char[][] arr = {{'a', 'b', 'c', 'd'}};
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("ab");
                dict.add("bc");
                dict.add("bcd");
                String ss = "";
                int h = 0, w = 1;
                wordSearchRight(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("bc", "bcd"));
            }
            {
                fl("wordSearchLeft 1");
                char[][] arr = {{'z', 'a', 'b', 'c'}};
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("ab");
                dict.add("bc");
                dict.add("zab");
                String ss = "";
                int h = 0, w = 2; 
                wordSearchLeft(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("ab", "zab"));
            }
            {
                fl("wordSearchDown 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("b");
                dict.add("bk");
                dict.add("kb");
                dict.add("bke");
                dict.add("a");
                dict.add("ab");
                dict.add("abc");
                String ss = "";
                int h = 0, w = 0;
                wordSearchDown(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("bk", "bke"));
            }
            {
                fl("wordSearchUp 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'a', 'k', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("bk");
                dict.add("kb");
                String ss = "";
                int h = 1, w = 1;
                wordSearchUp(arr, h, w, ss, set, dict);
                pl(set);
            }
        }
        {
            {
                fl("wordSearch 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k"));
            }
            {
                fl("wordSearch 2");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                dict.add("ke");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k", "ke"));
            }
            {
                fl("wordSearch 3");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                dict.add("ke");
                dict.add("kb");
                dict.add("kec");
                dict.add("kecc");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k", "ke", "kb", "kec", "kecc"));
            }
            {
                fl("wordSearch 4");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };
                Set<String> set = new HashSet<>();
                Set<String> dict = new HashSet<>();
                dict.add("k");
                dict.add("kecccb");
                dict.add("kecccbabc");
                String ss = "";
                int h = 1, w = 1;
                wordSearch(arr, h, w, ss, set, dict);
                pl(set);
                t(set, set("k", "kecccb", "kecccbabc"));
            }
            {
                fl("wordSearchAll 1");
                char[][] arr = {
                    {'a', 'b', 'c'},
                    {'b', 'k', 'c'},
                    {'c', 'e', 'c'}
                };

                Set<String> dict = new HashSet<>();
                dict.add("k");
                dict.add("kecccb");
                dict.add("kecccbabc");
                dict.add("a");
                dict.add("ab");
                dict.add("abk");
                dict.add("abke");
                dict.add("abcccecbk");

                dict.add("abb");
                dict.add("akc");

                Set<String> set = wordSearchAll(arr, dict);
                t(set, set("k", "kecccb", "kecccbabc", "a", "ab", "abk", "abke", "abcccecbk"));
            }

            {
                fl("wordSearchAll 2");
                char[][] arr = {
                    {'a', 'b', 'c', 'd', 'e'},
                    {'f', 'g', 'h', 'i', 'j'},
                    {'k', 'l', 'm', 'n', 'o'},
                    {'p', 'q', 'r', 's', 't'},
                    {'u', 'v', 'w', 'x', 'y'}
                };
                
                var p = getEnv("b") + "/vim/words.txt";
                var ls = readFile(p);
                Set<String> dict = listToSet(ls);

                Set<String> set = wordSearchAll(arr, dict);
                pl("set.size()=" + set.size());
                for(String s : set){
                    if(s.length() > 1){
                        pl(s);
                    }
                }
            }
        }
        {
            {
                fl("inorder");
                Node r = geneBinNoImage(); 
                // printBinTree(r);
                inorder(r);
            }
            {
                fl("preorder");
                Node r = geneBinNoImage(); 
                preorder(r);
            }
            {
                fl("postorder");
                Node r = geneBinNoImage(); 
                postorder(r);
            }
            {
                fl("printAllPath");
                Node r = geneBinNoImage(); 
                // printBinTree(r);
                printAllPath(r);
            }
        }
        {
            fl("coin change");
            List<Integer> coins = list(2, 4, 7, 5, 3, 9, 8);
            List<Integer> sumls = list();
            Integer sum = 20;
            Set<List<Integer>> set = new HashSet<>();
            coinChange(coins, sum, sumls, set);

            // Integer.MIN_VALUE
            int min = Integer.MAX_VALUE;
            for(List<Integer> ss : set){
                min = Math.min(min, ss.size());
            }

            fl("coin change set");
            pp(set);
            pl("min=" + min);
        }
        {
            fl("uniqueRandomNum");
            uniqueRandomNum();
        }
        {
            fl("mergeList");
            List<Integer> s1 = list(1, 4, 9); 
            List<Integer> s2 = list(2, 3, 11, 14); 
            List<Integer> ls = mergeList(s1, s2);
            printList(ls);
        }
        {
            fl("mergeListArr 1");
            int[] arr1 = {1};
            int[] arr2 = {2};
            int[] arr = mergeListArr(arr1, arr2);
            printArrint(arr);
        }
        {
            fl("mergeListArr2 1");
            int[] arr1 = {1, 4, 9};
            int[] arr2 = {2, 7, 8, 11};
            int[] arr = mergeListArr2(arr1, arr2);
            printArrint(arr);
        }
        {
            fl("mergeListArr3 1");
            int[] arr1 = {1, 4, 9};
            int[] arr2 = {2, 7, 8, 11};
            int[] arr = mergeListArr3(arr1, arr2);
            printArrint(arr);
        }
        {
            fl("mergeListArr5 1");
            int[] arr1 = {1};
            int lo = 0, mid = 0, hi = 0; 
            mergeListArr5(arr1, lo, mid, hi);
            printArrint(arr1);
        }
        {
            fl("mergeListArr5 2");
            int[] arr1 = {4, 2};
            int lo = 0, mid = 0, hi = 1;
            mergeListArr5(arr1, lo, mid, hi);
            printArrint(arr1);
        }
        {
            fl("mergeListArr5 3");
            int[] arr1 = {2, 4};
            int lo = 0, mid = 0, hi = 1;
            mergeListArr5(arr1, lo, mid, hi);
            printArrint(arr1);
        }
        {
            fl("mergeListArr5 4");
            int[] arr1 = {1, 4, 2,  9};
            int lo = 0, mid = 1, hi = 3;
            mergeListArr5(arr1, lo, mid, hi);
            printArrint(arr1);
        }
        {
            fl("mergeListArr5 5");
            int[] arr1 = {8, 9, 6, 7};
            int lo = 0, mid = 1, hi = 3;
            mergeListArr5(arr1, lo, mid, hi);
            printArrint(arr1);
        }
        {
            fl("mergeListArr5 6");
            int[] arr1 = {8, 9, 6};
            int lo = 0, mid = 1, hi = 2;
            mergeListArr5(arr1, lo, mid, hi);
            printArrint(arr1);
        }
        {
            fl("mergeList 1");
            int[] arr = {1};
            int lo = 0, hi = 0;
            mergeSort(arr, lo, hi);
            printArrint(arr);
            
        }
        {
            fl("mergeList 2");
            int[] arr = {2, 1};
            int lo = 0, hi = 1;
            mergeSort(arr, lo, hi);
            printArrint(arr);
            
        }
        {
            fl("mergeList 3");
            int[] arr = {1, 2};
            int lo = 0, hi = 1;
            mergeSort(arr, lo, hi);
            printArrint(arr);
            
        }
        {
            fl("mergeList 4");
            int[] arr = {1, 3, 2};
            int lo = 0, hi = 2;
            mergeSort(arr, lo, hi);
            printArrint(arr);
            
        }
        {
            fl("mergeList 5");
            int[] arr = geneArr1ToNInt(0, 4); 
            int lo = 0, hi = 3;
            mergeSort(arr, lo, hi);
            printArrint(arr);
            
        }
        {
            fl("mergeList 6");
            int[] arr = geneArr1ToNInt(0, 10); 
            int lo = 0, hi = 9;
            mergeSort(arr, lo, hi);
            printArrint(arr);
            
        }
        {

            fl("part 1");
            int[] arr = {1};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = part(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);

        }
        {

            fl("part 2");
            int[] arr = {2, 1};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = part(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);

        }
        {

            fl("part 3");
            int[] arr = {2, 1, 3, 4, 2};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = part(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);

        }
        {

            fl("part 3");
            int[] arr = {2, 1, 3, 4, 2, 9, 1, 3};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = part(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);

        }
        {

            fl("part 4");
            int[] arr = {2, 1, 3};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = part(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);

        }
        {
            {
                fl("quickSort 1");
                int[] arr = {2};
                int lo = 0, hi = arr.length - 1;
                quickSort(arr, lo, hi);
                printArrint(arr);
            }
            {
                fl("quickSort 2");
                int[] arr = {2, 1};
                int lo = 0, hi = arr.length - 1;
                quickSort(arr, lo, hi);
                printArrint(arr);
            }
            {
                fl("quickSort 3");
                int[] arr = {2, 1, 3};
                int lo = 0, hi = arr.length - 1;
                quickSort(arr, lo, hi);
                printArrint(arr);
            }
            {
                fl("quickSort 4");
                int[] arr = {2, 1, 3, 4, 9, 2, 1};
                int lo = 0, hi = arr.length - 1;
                quickSort(arr, lo, hi);
                printArrint(arr);
            }

        }
        {
            fl("getPivot 1");
            int[] arr = {2, 1, 3, 4, 9, 2, 1};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = getPivot(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);
        }
        {
            fl("getPivot 2");
            int[] arr = {2, 1, 3};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = getPivot(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);
        }
        {
            fl("getPivot 3");
            int[] arr = {2, 1, 3, 0};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = getPivot(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);
        }
        {
            fl("getPivot 4");
            int[] arr = {2, 1, 3, 0, 2};
            int lo = 0, hi = arr.length - 1;
            int pivotInx = getPivot(arr, lo, hi);
            pl("pivotInx=" + pivotInx);
            printArrint(arr);
        }
        {
            {
                fl("Linked");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                t(ll.first.data, 1);
                t(ll.last.data, 3);
                ll.print();
            }
            {
                fl("removeHead 1");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.removeHead();
                ll.print();
                fl("removeHead 2");
                ll.removeHead();
                ll.print();
                fl("removeHead 3");
                ll.removeHead();
                ll.print();
                ll.insert(new NodeX(9));
                fl("insert 9");
                ll.print();
                fl("insert 19");
                ll.insert(new NodeX(19));
                ll.print();
            }
        }
        {
                fl("Linked");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                fl("removeTail 1");
                ll.removeTail();
                t(ll.size(), 2);
                ll.print();

                fl("removeTail 2");
                ll.removeTail();
                t(ll.size(), 1);
                ll.print();

                fl("removeTail 3");
                ll.removeTail();
                ll.print();
                t(ll.size(), 0);
        }
        {
            {
                fl("delete 1");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.delete(1);
                t(ll.size(), 2);
                ll.print();
            }
            {
                fl("delete 2");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.delete(1);
                ll.delete(2);
                t(ll.size(), 1);
                ll.print();
            }
            {
                fl("delete 3");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.delete(1);
                ll.delete(2);
                ll.delete(3);
                t(ll.size(), 0);
                ll.print();
            }
            {
                fl("delete 4");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.delete(2);
                t(ll.size(), 2);
                ll.print();
            }
            {
                fl("delete 5");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.insert(new NodeX(4));
                ll.delete(2);
                ll.delete(3);
                t(ll.size(), 2);
                ll.print();
            }
            {
                fl("delete 6");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.insert(new NodeX(4));
                ll.delete(2);
                ll.delete(3);;
                ll.delete(1);;
                t(ll.size(), 1);
                ll.print();
            }
            {
                fl("delete 7");
                Linked ll = new Linked(); 
                ll.insert(new NodeX(1));
                ll.insert(new NodeX(2));
                ll.insert(new NodeX(3));
                ll.insert(new NodeX(4));
                ll.delete(2);
                ll.delete(3);;
                ll.delete(1);;
                ll.delete(4);;
                t(ll.size(), 0);
                t(ll.first, null);
                t(ll.last, null);
                ll.print();
            }
        }
        {
            {
                fl("searchWord 0");
                AddSearchStr adds = new AddSearchStr();
                adds.addWord("");
                int i = 0;
                boolean b = searchWord(adds.root, "", i);
                t(b, true);
                 
            }
            {
                fl("searchWord 1");
                AddSearchStr adds = new AddSearchStr();
                adds.addWord("a");
                int i = 0;
                boolean b = searchWord(adds.root, "a", i);
                t(b, true);
            }
            {
                fl("searchWord 2");
                AddSearchStr adds = new AddSearchStr();
                adds.addWord("a");
                adds.addWord("b");
                adds.addWord("ab");
                adds.addWord("abc");
                int i = 0;
                boolean b = searchWord(adds.root, "ab", i);
                t(b, true);
            }
            {
                fl("searchWord 3");
                AddSearchStr adds = new AddSearchStr();
                adds.addWord("a");
                adds.addWord("b");
                adds.addWord("ab");
                adds.addWord("abc");
                int i = 0;
                boolean b = searchWord(adds.root, "ab", i);
                t(b, true);
            }
            {
                fl("searchWord 4");
                AddSearchStr adds = new AddSearchStr();
                adds.addWord("a");
                adds.addWord("b");
                adds.addWord("ab");
                adds.addWord("abc");
                int i = 0;
                boolean b = searchWord(adds.root, "abc", i);
                t(b, true);
            }
            {
                {
                    fl("searchWord 5");
                    AddSearchStr adds = new AddSearchStr();
                    adds.addWord("a");
                    adds.addWord("b");
                    adds.addWord("ab");
                    adds.addWord("abc");
                    int i = 0;
                    boolean b = searchWord2(adds.root, "abc", i);
                    t(b, true);
                }
                {
                    fl("searchWord 6");
                    AddSearchStr adds = new AddSearchStr();
                    adds.addWord("a");
                    adds.addWord("b");
                    adds.addWord("ab");
                    adds.addWord("abc");
                    int i = 0;
                    boolean b = searchWord2(adds.root, "ab.", i);
                    t(b, true);
                }
                {
                    fl("searchWord 7");
                    AddSearchStr adds = new AddSearchStr();
                    adds.addWord("a");
                    adds.addWord("b");
                    adds.addWord("ab");
                    adds.addWord("abc");
                    int i = 0;
                    boolean b = searchWord2(adds.root, "a.c", i);
                    t(b, true);
                }
                {
                    fl("searchWord 8");
                    AddSearchStr adds = new AddSearchStr();
                    adds.addWord("a");
                    adds.addWord("b");
                    adds.addWord("ab");
                    adds.addWord("abc");
                    int i = 0;
                    boolean b = searchWord2(adds.root, "...", i);
                    t(b, true);
                }
                {
                    fl("searchWord 9");
                    AddSearchStr adds = new AddSearchStr();
                    adds.addWord("a");
                    adds.addWord("b");
                    adds.addWord("ab");
                    adds.addWord("abc");
                    int i = 0;
                    boolean b = searchWord2(adds.root, ".bc", i);
                    t(b, true);
                }
                {
                    fl("searchWord 10");
                    AddSearchStr adds = new AddSearchStr();
                    adds.addWord("a");
                    adds.addWord("b");
                    adds.addWord("ab");
                    adds.addWord("abc");
                    int i = 0;
                    boolean b = searchWord2(adds.root, ".", i);
                    t(b, true);
                }
            }
            {
                {
                    fl("sumList 1");
                    int[] a = {1};
                    int[] b = {0};
                    int[] sum = sumList(a, b);
                    int[] expectedSum = {0, 1};
                    t(sum, expectedSum);
                }
                {
                    fl("sumList 2");
                    int[] a = {9};
                    int[] b = {9};
                    int[] sum = sumList(a, b);
                    int[] expectedSum = {1, 8};
                    t(sum, expectedSum);
                }
                {
                    fl("sumList 2");
                    int[] a = {1, 1};
                    int[] b = {0, 1};
                    int[] sum = sumList(a, b);
                    int[] expectedSum = {0, 1, 2};
                    t(sum, expectedSum);
                }
                {
                    fl("sumList 3");
                    int[] a = {9, 9};
                    int[] b = {0, 9};
                    int[] sum = sumList(a, b);
                    int[] expectedSum = {1, 0, 8};
                    t(sum, expectedSum);
                    printArrint(sum);
                }
                {
                    fl("sumList 4");
                    int[] a = {9, 9};
                    int[] b = {9, 9};
                    int[] sum = sumList(a, b);
                    int[] expectedSum = {1, 9, 8};
                    t(sum, expectedSum);
                }
            }
            {
                {
                    fl("");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(repeatArrint(4, 0), arr);
                    int[] b1 = shiftArrLeft(b, 0);
                    printArrint(b1);
                }
                {
                    fl("");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(repeatArrint(4, 0), arr);
                    int[] b1 = shiftArrLeft(b, 1);
                    printArrint(b1);
                }
                {
                    fl("");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(repeatArrint(4, 0), arr);
                    int[] b1 = shiftArrLeft(b, 2);
                    printArrint(b1);
                }
                {
                    fl("shiftArrRight 1");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(arr, repeatArrint(4, 0));
                    int[] b1 = shiftArrRight(b, 0);
                    printArrint(b1);
                }
                {
                    fl("shiftArrRight 2");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(arr, repeatArrint(4, 0));
                    int[] b1 = shiftArrRight(b, 1);
                    printArrint(b1);
                }
                {
                    fl("shiftArrRight 3");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(arr, repeatArrint(4, 0));
                    int[] b1 = shiftArrRight(b, 2);
                    printArrint(b1);
                }
                {
                    fl("shiftArrRight 4");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(arr, repeatArrint(4, 0));
                    int[] b1 = shiftArrRight(b, 4);
                    printArrint(b1);
                }
                {
                    fl("shiftArrRight 5");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(arr, repeatArrint(4, 0));
                    int[] b1 = shiftArrRight(b, 5);
                    printArrint(b1);
                }
                {
                    fl("shiftArrRight 6");
                    int[] arr = {1, 2, 3};
                    int[] b = concatArr(arr, repeatArrint(4, 0));
                    int[] b1 = shiftArrRight(b, 6);
                    printArrint(b1);
                }

            }
            {
                {
                    fl("shiftArrLeft 2d 1");
                    int[][] zero = arrZero2d(4, 4); 
                    int[][] arr2d = {
                        { 1,   2,   3},
                        { 4,   5,   6},
                        { 7,   8,  9},
                        { 10,  11,  12},
                    };
                    int[][] a2 = concatMatrix(zero, arr2d);

                    int height = a2.length;
                    int width = a2[0].length;
                    int[][] arr = new int[height][width];
                    for(int h = 0; h < height; h++){
                        arr[h] = multiArrShiftLeft(a2[h], h, 10);
                    }

                    printArr2d(arr);

                }
            }
            {
                {
                    fl("mul 1");
                    List<Integer> ls = list(9, 4, 5);
                    List<Integer> ret = list();
                    int n = 2;
                    int c = 0;

                    for(Integer e : reverse(ls)){
                        int s = e * n + c; 
                        c = s / 10;
                        int r = s % 10;
                        ret.add(r);
                    }
                    ret.add(c);
                    pl(reverse(ret));
                    
                }
                {
                    fl("muList");
                    List<Integer> ls = list(9, 4, 5);
                    int n = 2;
                    List<Integer> ret = muList(ls, n);
                    pl(ret);
                }
                {
                    fl("lss 1");
                    int n = 4;
                    List<List<Integer>> ret = list();
                    List<List<Integer>> lss = list(list(1, 2), list(4, 5));
                    for(List<Integer> ls : lss){
                        var s = muList(ls, n);
                        ret.add(s);
                    }

                    for(List<Integer> e : ret){
                        var tt = concatList(repeat(4 - len(e), 0), e);
                        printList(tt);
                    }
                }
            }
            {
                {
                    fl("binSearch 1");
                    int[] arr = {1};
                    int lo = 0, hi = arr.length - 1, k = 0;
                    boolean b = binSearch(arr, lo, hi, k);
                    t(b, false);
                }
                {
                    fl("binSearch 2");
                    int[] arr = {1};
                    int lo = 0, hi = arr.length - 1, k = 1;
                    boolean b = binSearch(arr, lo, hi, k);
                    t(b, true);
                }
                {
                    fl("binSearch 3");
                    int[] arr = {1, 2};
                    int lo = 0, hi = arr.length - 1, k = 1;
                    boolean b = binSearch(arr, lo, hi, k);
                    t(b, true);
                }
                {
                    fl("binSearch 4");
                    int[] arr = {1, 2, 3};
                    int lo = 0, hi = arr.length - 1, k = 2;
                    boolean b = binSearch(arr, lo, hi, k);
                    t(b, true);
                }
                {
                    fl("binSearch 4");
                    int[] arr = {1, 2, 3, 4};
                    int lo = 0, hi = arr.length - 1, k = 4;
                    boolean b = binSearch(arr, lo, hi, k);
                    t(b, true);
                }
                {
                    fl("binSearch 5");
                    int[] arr = {1, 2, 3, 4};
                    int lo = 0, hi = arr.length - 1, k = 1;
                    boolean b = binSearch(arr, lo, hi, k);
                    t(b, true);
                }
            }
            {
                {
                    fl("multi 1");
                    List<Integer> s1 =  list(1);
                    List<Integer> s2 =  list(2);
                    multi(s1, s2);
                }
                {
                    fl("multi 2");
                    List<Integer> s1 =  list(9);
                    List<Integer> s2 =  list(9);
                    multi(s1, s2);
                }
                {
                    fl("multi 2");
                    List<Integer> s1 =  list(9, 1);
                    List<Integer> s2 =  list(9, 2);
                    multi(s1, s2);
                }
                {
                    fl("multi 3");
                    List<Integer> s1 =  list(9, 1, 4);
                    List<Integer> s2 =  list(9, 2, 3);
                    multi(s1, s2);
                }
                {
                    fl("multi 4");
                    List<Integer> s1 =  list(9, 1, 4);
                    List<Integer> s2 =  list(9, 2, 3);
                    var ls = multi(s1, s2);
                    printList(ls);
                }
            }
            {
                {
                    fl("shiftLeft4 1");
                    List<Integer> ls = list(1, 2, 3, 0, 0);
                    List<Integer> ret = shiftLeft4(2, ls);
                    printList(ret);
                }
            }
            {
                {
                    fl("all substrings 1");
                    String s = "abc";
                    int len = s.length();
                    for(int i = 0; i < len; i++){
                        for(int j = 0; j < len; j++){
                            if(j + 1 + i <= len){
                                String sub = s.substring(j, j + 1 + i);
                                pl("sub=" + sub);
                            }
                        }
                    }
                }
                {
                    fl("all substrings 2");
                    String s = "abc";
                    int len = s.length();
                    for(int i = 0; i < len; i++){
                        for(int j = 0; j < len; j++){
                            if(i < j + 1){
                                String sub = s.substring(i, j + 1);
                                pl("sub=" + sub);
                            }
                        }
                    }
                }
               
            }
            {
                {
                    fl("test 2");
                    String s = "abc";
                    List<String> lp = new ArrayList<>();
                    List<String> ls = new ArrayList<>();
                    int len = len(s);
                    for(int i = 1; i <= len; i++){
                        String prefix = s.substring(0, i);
                        String suffix = s.substring(i, len); 
                    }
                }
            }
            {
                fl("leet code 1");
                String s1 = "super";
                String s2 = "car";
                String s3 = "hero";
            }
        }
        {
            {
                fl("permStr 1");
                List<String> ls = list("Jamaica", "Panama", "CostaRica", "centralAmerica");
                List<String> prefix = list();
                permStr(prefix, ls);
            }
            {
                fl("combine strings 2");
                // List<String> ls = list("Jamaica", "Panama", "JamaicaPanama");
                List<String> ls = list("car","super","supercar","hero","superhero","superherocar","spring","flower","springflower","winter","cool");

                List<String> alls = list();
                for(String s : ls){
                    List<String> sub = allSubstring(s);
                    alls = concat(alls, sub);
                }

                Set<String> set1 = new HashSet<>();
                Set<String> set2 = new HashSet<>();
                Map<String, String> map = new HashMap<>();
                for(int i = 0; i < len(ls); i++){
                    for(int j = 0; j < len(ls); j++){
                        if(i != j && len(ls.get(i)) < len(ls.get(j))){
                            // pl(ls.get(i) + " -> " + ls.get(j));
                            List<String> sub = allSubstring(ls.get(j));
                            Set<String> set = new HashSet<>(sub);
                            if(set.contains(ls.get(i))){
                                pl(ls.get(i) + " => " + ls.get(j));
                                set1.add(ls.get(i));
                                set2.add(ls.get(j));
                                map.put(ls.get(i), ls.get(j));
                            }
                        }
                    }
                }
                List<String> ss = list();
                for(String t : set1){
                    if(set2.contains(t)){
                        ss.add(t);
                    }
                }

                for(String s : ss){
                    map.remove(s);
                }
                fl("map 1");
                printMap(map);

                Map<String, List<String>> bmap = new HashMap<>();
                for(Map.Entry<String, String> entry : map.entrySet()){
                    pl(entry.getKey() + " -> " + entry.getValue());
                    List<String> v = bmap.get(entry.getValue());
                    if(v == null ){
                        v = new ArrayList<>();
                    }
                    v.add(entry.getKey());
                    bmap.put(entry.getValue(), v);
                }
                fl("bmap 1");
                printMap(bmap);
            }
        }

		end();
    }
	public static void test2(){
		beg();
		{
			{
				Integer[] a1 = {1};
				Integer[] a2 = {2};
				Integer[] arr = mergeSortedArr(a1, a2);
				Integer[] expectedArr = {1, 2};
				t(arr, expectedArr);
			}
			{
				Integer[] a1 = {1, 9, 11};
				Integer[] a2 = {2, 3, 9, 11, 12};
				Integer[] arr = mergeSortedArr(a1, a2);
				Integer[] expectedArr = {1, 2, 3, 9, 9, 11, 11, 12};
				t(arr, expectedArr);
			}
		}
		{
			{
				Integer[] a1 = {1};
				Integer[] a2 = {2};
				Integer[] arr = merge(a1, a2);
				Integer[] expectedArr = {1, 2};
				t(arr, expectedArr);
			}
			{
				Integer[] a1 = {1, 9, 11};
				Integer[] a2 = {2, 3, 9, 11, 12};
				Integer[] arr = merge(a1, a2);
				Integer[] expectedArr = {1, 2, 3, 9, 9, 11, 11, 12};
				t(arr, expectedArr);
			}
		}
		{

			{
				fl("maxSumArray 1");
				Integer[] arr = {-3, 4, -5, 1, 6};
				int max = maxSumArray(arr);
				pp("max=" + max);
				t(max, 7);
			}
			{
				fl("maxSumArray 2");
				Integer[] arr = {-3, 4, -5, -1, 6};
				int max = maxSumArray(arr);
				pp("max=" + max);
				t(max, 6);
			}
			{
				fl("maxSumArray 3");
				Integer[] arr = {-3, 4, -3, 1, 6};
				int max = maxSumArray(arr);
				pp("max=" + max);
				t(max, 8);
			}
		}
		{
			{
				fl("continuousSum 1");
				Integer[] arr = {-3, 4, -5, 1, 6};
				int max = continuousSum(arr);
				pp("max=" + max);
				t(max, 7);
			}
			{
				fl("continuousSum 2");
				Integer[] arr = {-3, 4, -5, -1, 6};
				int max = continuousSum(arr);
				pp("max=" + max);
				t(max, 6);
			}
			{
				fl("continuousSum 3");
				Integer[] arr = {-3, 4, -3, 1, 6};
				int max = continuousSum(arr);
				pp("max=" + max);
				t(max, 8);
			}

		}
		{
			{
				fl("partitionArr 1");
				Integer[] arr = {1};
				int lo = 0;
				int hi = arr.length - 1;
				int pivotInx = partitionArr(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArr(arr);
			}
			{
				fl("partitionArr 2");
				Integer[] arr = {2, 1};
				int lo = 0;
				int hi = arr.length - 1;
				int pivotInx = partitionArr(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArr(arr);
			}
			{
				fl("partitionArr 3");
				Integer[] arr = {1, 3, 2};
				int lo = 0;
				int hi = arr.length - 1;
				int pivotInx = partitionArr(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArr(arr);
			}
			{
				fl("partitionArr 4");
				Integer[] arr = {1, 3, 2, -1};
				int lo = 0;
				int hi = arr.length - 1;
				int pivotInx = partitionArr(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArr(arr);
			}
			{
				fl("partitionArr 5");
				Integer[] arr = {1, 3, 2, -1, 1};
				int lo = 0;
				int hi = arr.length - 1;
				int pivotInx = partitionArr(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArr(arr);
			}
			{
				fl("partitionArr 6");
				Integer[] arr = {1, 3, 2, -1, 1, 2};
				int lo = 0;
				int hi = arr.length - 1;
				int pivotInx = partitionArr(arr, lo, hi);
				pl("pivotInx=" + pivotInx);
				printArr(arr);
			}
		}
		{
			{
				fl("quickSort 1");
				Integer[] arr = {1};
				Integer[] expectedArr = {1};
				int lo = 0;
				int hi = arr.length - 1;
				quickSort(arr, lo, hi);
				printArr(arr);
				t(arr, expectedArr);
			}
			{
				fl("quickSort 2");
				Integer[] arr = {2, 1};
				Integer[] expectedArr = {1, 2};
				int lo = 0;
				int hi = arr.length - 1;
				quickSort(arr, lo, hi);
				printArr(arr);
				t(arr, expectedArr);
			}
			{
				fl("quickSort 3");
				Integer[] arr = {1, 2};
				Integer[] expectedArr = {1, 2};
				int lo = 0;
				int hi = arr.length - 1;
				quickSort(arr, lo, hi);
				printArr(arr);
				t(arr, expectedArr);
			}
			{
				fl("quickSort 4");
				Integer[] arr = {1, 3, 2};
				Integer[] expectedArr = {1, 2, 3};
				int lo = 0;
				int hi = arr.length - 1;
				quickSort(arr, lo, hi);
				printArr(arr);
				t(arr, expectedArr);
			}
			{
				fl("quickSort 5");
				Integer[] arr = {1, 3, 2, 9, -1};
				Integer[] expectedArr = {-1, 1, 2, 3, 9};
				int lo = 0;
				int hi = arr.length - 1;
				quickSort(arr, lo, hi);
				printArr(arr);
				t(arr, expectedArr);
			}

		}
		{
			{
				fl("multiplynew 1");
				int[] a = {9};
				int[] b = {9};
				int[] arr = multiplynew(a, b);
				printArrint(arr);
			}
			{
				fl("multiplynew 2");
				int[] a = {9, 9};
				int[] b = {9};
				int[] arr = multiplynew(a, b);
				printArrint(arr);
			}
		}
		{
			{
				fl("multiArr 1");
				int[] arr = {1, 2, 3};
				int[] ret = multiArr(arr, 4);
				int[] expectedArr = {0, 4, 9, 2};
				t(ret, expectedArr);
			}
			{
				fl("multiArr 2");
				int[] arr = {9};
				int[] ret = multiArr(arr, 9);
				int[] expectedArr = {8, 1};
				t(ret, expectedArr);
			}
			{
				fl("multiArr 3");
				int[] arr = {0};
				int[] ret = multiArr(arr, 0);
				int[] expectedArr = {0, 0};
				t(ret, expectedArr);
			}
			{
				fl("multiArr 4");
				int[] arr = {0};
				int[] ret = multiArr(arr, 1);
				int[] expectedArr = {0, 0};
				t(ret, expectedArr);
			}
			{
				fl("multiArr 5");
				int[] arr = {9, 9};
				int[] ret = multiArr(arr, 9);
				int[] expectedArr = {8, 9, 1};
				t(ret, expectedArr);
			}
		}
		{
			{
				fl("multiLongInteger 1");
				int[] a = {1, 2, 3};
				int[] b = {2, 3};
				int[] arr = multiLongInteger(a, b);
				printArrint(arr);
			}
		}
		{
			{
				fl("maxTwo 1");
				int[] arr = {1, 0, 9, 0, 2};
				int[] ret = maxTwo(arr);
				printArrint(ret);
			}
		}
		{
			{
				fl("sum recursion 1");
				int[] arr = {};
				int len = arr.length;
				int s = sum(arr, len);
				t(s, 0);
			}
			{
				fl("sum recursion 2");
				int[] arr = {1};
				int len = arr.length;
				int s = sum(arr, len);
				t(s, 1);
			}
			{
				fl("sum recursion 2");
				int[] arr = {1, 2, 3};
				int len = arr.length;
				int s = sum(arr, len);
				t(s, 6);
			}
		}
		{
			{
				/**
				Node node = geneBin();
				int n = maxPath(node);
				pp("n=" + n);
				*/
			}
		}
		{
			{
				List<Integer> ls = list(1, 2, 3);
				List<List<Integer>> ss = tails(ls);
				pp(ss);
			}
		}
		{
            
			fl("combination 1");
			List<Integer> prefix = list();
			List<Integer> ls = list(1, 2, 3);
			int n = 2;
			combination(prefix, ls, n);
		}
        {
            {
                
                /**
                    Given two vertexes a b, find the shortest path from  a ⟹  b

                    a ⟹   b 
                         b ⟹  c

                    a ⟹   c
                */
                fl("geneGraph 1");
                List<Edge> ls = list(new Edge("a", "b"), new Edge("b", "c"), new Edge("a","c"));
                Map<String, Set<String>> map = geneGraph(ls);
                pp(map);
            }
            {
                fl("geneGraph 1");
                List<Edge> ls = list(new Edge("a", "b"), new Edge("b", "c"), new Edge("a","c"));
                Map<String, Set<String>> map = geneGraph(ls);
                pp(map);
                
                fl("print graph 1");
                String s = "a";
                String e = "c"; 
                pp(s + "=>");
                allPath(map, s, e);
            }
            {
                Node node = geneBinNoImage();
                List<Node> ls = list();

                fl("binary all paths 1");
                binAllPath(node, ls);
            }

            {
                fl("printAllPath 2");
                List<Edge> ls = list(
                  new Edge("10", "5"), 
                  new Edge("5", "1"), 
                  new Edge("5","9"),
                  new Edge("10","15"),
                  new Edge("15","9")
                  );
                Map<String, Set<String>> map = geneGraph(ls);
                String s = "10";
                String e = "9";
                List<String> rs = list();
                printAllPath(map, s, e, append(rs, s));
            }
            {

                /*

                                10 ->  5 -> 1
                                 |     |
                                 v     v
                                 15 -> 9

                            10 -> 11 -> 13 -> 9
                            10 -> 14 -> 16 -> 17 -> 9 
                */
                fl("printAllPathShortest 1");
                List<Edge> ls = list(
                  new Edge("10", "5"), 
                  new Edge("5", "1"), 
                  new Edge("5","9"),
                  new Edge("10","15"),
                  new Edge("15","9"),
                  new Edge("10","11"),
                  new Edge("11","13"),
                  new Edge("13","9"),

                  new Edge("10","14"),
                  new Edge("14","16"),
                  new Edge("16","17"),
                  new Edge("17","9")
                  );
                Map<String, Set<String>> map = geneGraph(ls);

                fl("print map");
                printMap(map);

                String s = "10";
                String e = "9";
                List<String> rs = list();
                int n = printAllPathShortest(map, s, e, append(rs, s));
                pl("n=>" + n);
            }
        }
        {
           { 
                fl("binLongestPath 1");
                Node node = geneBinNoImage();
                int n = binLongestPath(node);
                pl("n=" + n);
           }
        }
        {
            {
                fl("insertNode 1");
                Node h = new Node(1);
                Node n1 = new Node(2);
                Node head = insertNode(h, n1);
                printNode(head);
            }
            {
                fl("insertNode 2");
                Node h = null;
                Node n1 = new Node(1);
                Node head = insertNode(h, n1);
                printNode(head);
            }
            {
                fl("insertFront 1");
                Node h = null;
                Node n1 = new Node(1);
                Node head = insertFront(h, n1);
                printNode(head);
            }
            {
                fl("insertFront 2");
                Node h = new Node(1);
                Node n1 = new Node(2);
                Node head = insertFront(h, n1);
                printNode(head);
            }
            {
                fl("insertFront 3");
                Node h = new Node(1);
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node head = insertFront(h, n1);
                head = insertFront(head, n2);
                printNode(head);
            }
            {
                fl("insertLast 1");
                Node h = null; 
                Node n1 = new Node(1);
                Node head = insertLast(h, n1);
                printNode(head);
            }
            {
                fl("insertLast 2");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                Node head = insertLast(h, n1);
                printNode(head);
            }
            {
                fl("insertLast 2");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node head = insertLast(h, n1);
                head = insertLast(head, n2);
                printNode(head);
            }
            {
                fl("removeFront 1");
                Node h = null; 
                Node head = removeFront(h);
                printNode(head);
            }
            {
                fl("removeFront 2");
                Node h = new Node(1); 
                Node head = removeFront(h);
                printNode(head);
            }
            {
                fl("removeFront 3");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                insertNode(h, n1);
                Node head = removeFront(h);
                printNode(head);
            }
            {
                fl("removeFront 4");
                Node h = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node head = insertNode(h, n1);
                head = insertNode(head, n2);
                Node newH = removeFront(head);
                printNode(newH);
            }
            {
                fl("removeLast 1");
                Node head = null; 
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 2");
                Node head = new Node(1); 
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 3");
                Node head = new Node(1); 
                Node n1 = new Node(2);
                insertNode(head, n1);
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 4");
                Node head = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                head = insertNode(head, n1);
                head = insertNode(head, n2);
                head = removeLast(head);
                printNode(head);
            }
            {
                fl("removeLast 5");
                Node head = new Node(1); 
                Node n1 = new Node(2);
                Node n2 = new Node(3);
                Node n3 = new Node(4);
                head = insertNode(head, n1);
                head = insertNode(head, n2);
                head = insertNode(head, n3);
                head = removeLast(head);
                printNode(head);
            }
        }
        {
            {
                fl("insertStr contains 1");
                String s = "";
                TNode root = new TNode(true);
                int inx = 0;
                insertStr(root, s, inx);

                boolean b = contains(root, s, inx);
                t(b, true);
            }
            {
                fl("insertStr contains 2");
                String s = "a";
                TNode root = new TNode(true);
                int inx = 0;
                insertStr(root, s, inx);
                boolean b = contains(root, s, inx);
                t(b, true);
            }
            {
                fl("insertStr contains 3");
                String s = "ab";
                TNode root = new TNode(true);
                int inx = 0;
                insertStr(root, s, inx);
                boolean b = contains(root, s, inx);
                t(b, true);
            }
			{
				fl("connectIsland 1");
				int hInx = 0, wInx = 0;
				int[][] arr = {
					{ 0,   0,   0,  1},
					{ 1,   1,   0,  1},
					{ 0,   1,   0,  1},
					{ 0,   1,   1,  0},
				};
				int n = connectIsland(arr, hInx, wInx);
				int max = 0;
				for(int i = 0; i < arr.length; i++){
					for(int j = 0; j < arr[0].length; j++){
						int m = connectIsland(arr, i, j);
						if(m > max){
							max = m;
						}
					}
				}
				pp("max=" + max);
				
			}

			{
				fl("connectIsland 2");
				int hInx = 0, wInx = 0;
				int[][] arr = {
					{ 1,   1,   1},
					{ 1,   1,   1},
					{ 1,   1,   1},
				};

				int max = 0;
				for(int i = 0; i < arr.length; i++){
					for(int j = 0; j < arr[0].length; j++){
						List<XY> ls = new ArrayList<>();
						int m = connectIslandWithPosition(arr, i, j, ls);
						if(m > max){
							max = m;
						}
						printList(ls);
					}
				}
				pp("max=" + max);
			}
			{
				fl("connectIsland 3");
				int hInx = 0, wInx = 0;
				int[][] arr = {
					{ 0,   0,   0,  1},
					{ 1,   1,   0,  1},
					{ 0,   1,   0,  1},
					{ 0,   1,   1,  0},
				};

				int max = 0;
				for(int i = 0; i < arr.length; i++){
					for(int j = 0; j < arr[0].length; j++){
						List<XY> ls = new ArrayList<>();
						int m = connectIslandWithPosition(arr, i, j, ls);
						if(m > max){
							max = m;
						}
						printList(ls);
					}
				}
				pp("max=" + max);

				
			}
        }
        {
            fl("Multiply all integers except the current one in an array 1");
            int[] arr = {2, 3, 4};
            maxArray(arr);
        }
        {
            fl("Multiply all integers except the current one in an array 2");
            int[] arr = {2, 3};
            maxArray(arr);
        }
        {
            fl("insert 1");
            Bin bin = new Bin();        
            Node root = insert(bin.root, 1);
            t(root.data, 1);
        }
        {
            fl("insert 2");
            Bin bin = new Bin();        
            Node root = insert(bin.root, 6);
            root = insert(root, 10);
            root = insert(root, 8);
            t(root.data, 6);
        }
        {
            fl("insert 3");
            Bin bin = new Bin();        
            Node root = insert(bin.root, 6);
            root = insert(root, 10);
            root = insert(root, 8);

            Node maxNode = maxBST(root);
            t(maxNode.data, 10);
        }
        {
            fl("insert 4");
            Bin bin = new Bin();        
            Node root = insert(bin.root, 6);
            root = insert(root, 10);
            root = insert(root, 4);

            Node minNode = minBST(root);
            t(minNode.data, 4);
        }
        {
            fl("deleteMinNode 1");
            Bin bin = new Bin();        
            Node root = insert(bin.root, 6);
            root = insert(root, 10);
            Node node = insert(root, 4);
            Node n1 = deleteMinNode(node);

            Node minNode1 = minBST(n1);
            t(minNode1.data, 6);

        }
        {
            fl("deleteMinNode 2");
            Bin bin = new Bin();        
            Node root = insert(bin.root, 4);
            root = insert(root, 6);
            Node node = insert(root, 10);
            Node n1 = deleteMinNode(node);

            Node minNode1 = minBST(n1);
            t(minNode1.data, 6);

        }
        {
            fl("deleteMaxNode 11");
            Node root = new Node(9);
            deleteMinNode(root);
        }
        {
            fl("deleteMaxNode 12");
            Node root = geneBinNoImage(); 
            Node node = minNode(root);
            pp("node => " + node.data);
        }
        {
            fl("insert 6");
            Bin bin = new Bin();        
            Node root = insert(bin.root, 6);
            root = insert(root, 10);
            root = insert(root, 4);

            // printBinTree(root);

            deleteMaxNode(root);

            Node maxNode = maxBST(root);
            t(maxNode.data, 6);

            // printBinTree(root);
        }
        {
            {
                fl("matchHeader 1");
                String s = "abc:*:123abc, efg";
                matchHeader(s);
            }
        }
		end();
	}

    public static String matchHeader(String s){
        String regex = "([ a-zA-Z0-9_-]+:[^:]+:[ a-zA-Z0-9_,-]+)";
        Pattern r = Pattern.compile(regex);
        Matcher mat = r.matcher(s);
        if(mat.find()){
            pl("mat.group=" + mat.group());
        }
        return "";
    }

    public static void binAllPath(Node root, List<Node> ls){
        if(root != null){
            if(root.left == null && root.right == null){
                List<Node> s = append(ls, root);
                printList(s); 
            }
            binAllPath(root.right, append(ls, root));
            binAllPath(root.left, append(ls, root));
        }
    }

    /**
        Find the longest path from the root node
    */
    public static int binLongestPath(Node root){
        if(root != null){
            int l = binLongestPath(root.left);
            int r = binLongestPath(root.right);
            return Math.max(l, r) + 1;
        }
        return 0;
    }

    public static List<Point> findAllChildren(List<Seg> segs, Point point){
        List<Point> ls = new ArrayList<>();
        for(Seg s : segs){
            if(s.beg.x == point.x && s.beg.y == point.y){
                ls.add(s.end);
            }else if (s.end.x == point.x && s.end.y == point.y){
                ls.add(s.beg);
            }
        }
        return ls;
    }

    public static void printAllPath(Map<String, Set<String>> map, String a, String b, List<String> ls){
        Set<String> set = map.get(a); 
        if(set != null){
            for(String e : set){
                if( e != b){
                    printAllPath(map, e, b, append(ls, e));
                }else{
                    pl("");
                    List<String> rs = append(ls, e);
                    printList(rs);
                }
            }
        }
    }

    public static int printAllPathShortest(Map<String, Set<String>> map, String a, String b, List<String> ls){
        Set<String> set = map.get(a); 
        int max = Integer.MIN_VALUE;
        if(set != null){
            for(String e : set){
                if( e != b){
                    // Overflow in Integer here
                    int n = printAllPathShortest(map, e, b, append(ls, e)) + 1;
                    if(n > max){
                        max = n;
                    }
                }else{
                    pl("ls xx");
                    List<String> rs = append(ls, e);
                    printList(rs);
                    return 0;
                }
            }
        }
        return max; 
    }


    public static void allPath(Map<String, Set<String>> map, String a, String b){
        Set<String> set = map.get(a); 
        if( set != null){
            for(String e : set){
                if (e != b){
                    pp(e);
                    pl("");
                    allPath(map, e, b);
                }else{
                    pp(b);
                    pl("");
                }
            }
        }
    }

    public static List<String> rmInx(int n, List<String> ls){
        List<String> ret = new ArrayList<>();
        for(int i = 0;  i < ls.size(); i++){
            if(i != n){
                ret.add(ls.get(i));
            }
        }
        return ret;
    }
    public static void permStr(List<String> prefix, List<String> ls){
        if(ls.size() == 0){
            printList(prefix);
        }else{
            for(int i = 0; i < len(ls); i++){
                String s1 = ls.get(i);
                permStr(append(prefix, s1), rmInx(i, ls));
            }
        }
    }
    
    public static boolean binSearch(int[] arr, int lo, int hi, int k){
        if(lo <= hi){
            int mid = (lo + hi)/2;
            if(k < arr[mid]){
                return binSearch(arr, lo, mid - 1, k);
            }else if(k > arr[mid]){
                return binSearch(arr, mid + 1, hi, k);
            }else{
                return true;
            }
        }
        return false;
    }

    public static List<Integer> muList(List<Integer> ls, int n){
            List<Integer> ret = list();
            int c = 0;
            for(Integer e : reverse(ls)){
                int s = e * n + c; 
                c = s / 10;
                int r = s % 10;
                ret.add(r);
            }
            ret.add(c);
            return reverse(ret);
    }

    public static List<Integer> concat4(List<Integer> s1, List<Integer> s2, List<Integer> s3){
        List<Integer> ret = new ArrayList<>();
        for(Integer n : s1){
            ret.add(n);
        }
        for(Integer n : s2){
            ret.add(n);
        }
        for(Integer n : s3){
            ret.add(n);
        }
        return ret;
    }
    
    public static List<Integer> shiftLeft4(int n, List<Integer> ls){
       List<Integer> ret = new ArrayList<>();
       int len = ls.size();
       List<Integer> s1 = ls.subList(0, n); 
       List<Integer> s2 = ls.subList(n, len);
       for(Integer e : s2){
           ret.add(e);
       }
       for(Integer e : s1){
           ret.add(e);
       }
       return ret;
    }

	/**
	   Multiply two list of Integers
	 */
    public static List<Integer> multi(List<Integer> s1, List<Integer> s2){
        List<List<Integer>> lss = new ArrayList<>();
        for(Integer n : s2){
            List<Integer> ls = muList(s1, n);
            lss.add(ls);
        }

        List<List<Integer>> ss = new ArrayList<>();
        int len = lss.size();
        for(int i = 0; i < len; i++){
            List<Integer> s = cat(repeat(len - i, 0),  cat(lss.get(i), repeat(i, 0)));
            ss.add(s);
        }
        printList2d(lss);
        fl("");
        printList2d(ss);

        List<Integer> ret = new ArrayList<>();

        int c = 0;
        for(int k = ss.get(0).size() - 1; k >= 0; k--){
            int sum = 0;
            var col = getCol(k, ss);
            for(Integer n : col){
                sum += n;
            }
            sum += c;

            int r = sum % 10;
            int q = sum / 10;
            c = q;
            ret = cons(r, ret); 
        }
        return ret;
    }
	
	// Monday, 20 June 2022 12:45 PDT
	public static void preOrder(Node node){
		if(node != null){
			System.out.println(node.data);
			preOrder(node.left);
			preOrder(node.right);
		}
	}
	public static void inOrder(Node node){
		if(node != null){
			inOrder(node.left);
			System.out.println(node.data);
			inOrder(node.right);
		}
	}
	public static void postOrder(Node node){
		if(node != null){
			postOrder(node.left);
			postOrder(node.right);
			System.out.println(node.data);
		}
	}
	
	public static Integer[] mergeSortedArr(Integer[] a1, Integer[] a2){
		int len1 = a1.length;
		int len2 = a2.length;
		Integer[] arr = new Integer[len1 + len2];
		int i = 0, j = 0, k = 0;
		while(i < len1 || j < len2){
			if(i >= len1){  
				arr[k] = a2[j];  // i is out of bound
				j++;
			}else if(j >= len2){
				arr[k] = a1[i];  // j is out of bound
				i++;
			}else{
				if(a1[i] < a2[j]){
					arr[k] = a1[i];
					i++;
				}else{
					arr[k] = a2[j];
					j++;
				}
			}
			k++;
		}
		
		return arr;
	}
	
	// merge two sorted array
    public static Integer[] merge(Integer[] arr1, Integer[] arr2){
        Integer[] array = null;
        if( arr1 != null && arr2 != null){
            int i=0, j = 0, k = 0;
            int len1 = arr1.length;
            int len2 = arr2.length;
            array = new Integer[len1+len2];
            while(k < len1 + len2) {
                if( i >= len1) {
                    array[k] = arr2[j];  // i is out of bound
                    j++;
                }
                else if( j >= len2) {
                    array[k] = arr1[i];  // j is out of bound
                    i++;
                }
                else {
                    if(arr1[i] < arr2[j]) {
                        array[k] = arr1[i];
                        i++;
                    }
                    else {
                        array[k] = arr2[j];
                        j++;
                    }
                }
                k++;
            }
        }
        return array;
    }
	/**
	   Given a integer array, find the maximum continuous sum

	   1 3 -4 5 -1 6

	   1 => 1
	   1 3 => 4
	   1 3 -4 => 0
	   1 3 -4 5 => 5
	   1 3 -4 5 -1 => 4
	   1 3 -4 5 -1 6 => 10
	   
	   1 3 -6 5 -1 6
	   1 => 1          m = 1, max = 1
	   1 3 => 4        m = 4, max = 4
	   1 3 -6 => -2    m = 0  max = 4
	   1 3 -6 5 => 5   m = 5, max = 5
	   1 3 -6 5 -1 =>  m = 4, max = 5
	   1 3 -6 5 -1 6   m = 10 max = 10
	     
	 */
	public static Integer maxSumArray(Integer[] arr){
		int m = 0, max = 0;
		for(int i = 0; i < arr.length; i++){
			m += arr[i];
			if (m < 0){
				m = 0;
			}
			
			if(max < m){
				max = m;
			}
		}
		return max;
	}

	public static Integer continuousSum(Integer[] arr){
		Integer max = 0;
		if(arr != null){
			int len = arr.length;
			int cur = 0;
			for(int i=0; i< len; i++){
				if(cur < 0){
					cur = 0;
				}

				cur = cur + arr[i];
				// compare: [curr value, curr accumulator, max] 
				max = Math.max(max, arr[i]);
				max = Math.max(max, cur);
			}
		}
		return max;
	}
	
	public static int partitionArr(Integer[] arr, int lo, int hi){
		int len = arr.length;
		int bigInx = lo;
	    Integer pivot = arr[hi];		
		for(int i = lo; i <= hi; i++){
			if(arr[i] <= pivot){
				int tmp = arr[i];
				arr[i] = arr[bigInx];
				arr[bigInx] = tmp;
				if(i < hi)
					bigInx++;
			}
		}
		return bigInx;
	}
	
	public static void quickSort(Integer[] arr, int lo, int hi){
		if(lo < hi){
			int pivotInx = partitionArr(arr, lo, hi);
			quickSort(arr, lo, pivotInx - 1);
			quickSort(arr, pivotInx + 1, hi);
		}
	}
	
	public static int[] multiplynew(int[] arr1, int[] arr2){
        int len1 = arr1.length;
        int len2 = arr2.length;
        int len = len1 + len2;
        int[][] arr = new int[len1][len1+len2];

        for(int i=len2-1; i >= 0; i--){
            int carry = 0;
            int j=0;
            for(j=len1-1; j >= 0; j--){
                int shiftLeft = i + 1;
                arr[i][j + shiftLeft] = (arr1[j]*arr2[i] + carry) %10;
                carry = (arr1[j]*arr2[i]+carry) / 10;
            }
            arr[i][(i+1)+j] = carry;
        }

        int[] total = new int[len];
        int carry = 0;
        for(int j=len-1; j >= 0; j--){
            int s=0;
            for(int i=0; i < len2; i++){
                s += arr[i][j];
            }
            total[j] = (s + carry) % 10;
            carry = (s + carry)/10;
        }
        return total;
    }

	public static int[] multiArr(int[] arr, int n){
		int len = arr.length;
		int c = 0;
		int[] ret = new int[len + 1];
		int lret = len + 1;
		for(int i = 0; i < len; i++){
			int s = arr[len - 1 - i] * n + c;
			ret[lret - 1 - i] = s % 10;
			c = s / 10;
		}
		ret[0] = c;
		return ret;
	}

	public static int[] sumColumn(int[][] arr){
		int[] ret = new int[arr[0].length];
		int c = 0;
		for(int i = 0; i < arr[0].length; i++){
			int rIx = arr[0].length - 1 - i;
			
			int s = 0;
			for(int j = 0; j < arr.length; j++){
				s += arr[j][rIx];
			}
			ret[rIx] = (s + c) % 10;
			c = (s + c) / 10;
		}
		ret[0] = c;
		return ret;
	}

	/**
                1 2 3
				  2 3


	   
	             1 2 3          <- a
				       3
					   2
					   ↑
					   b

	 */
	public static int[] multiLongInteger(int[] a, int[] b){
		int len1 = a.length;
		int len2 = b.length;
		int[][] arr = new int[len2][len1 + len2];
		int lena = len1 + len2;
		for(int i = 0; i < len2; i++){
			int[] c = multiArr(a, b[b.length - 1 - i]);
			for(int j = 0; j < c.length; j++){
				arr[i][lena - 1 - j - i] = c[c.length - 1 - j];
			}
		}
		printArr2d(arr);
		
		return sumColumn(arr);
	}
	
	/**
	     3 1 4 9 0
		 x
		   - x x _
	 */
	public static int[] maxTwo(int[] arr){
		int[] ret = new int[2];
		int m2 = Math.max(arr[0], arr[1]);
		int m1 = Math.min(arr[0], arr[1]);
		for(int i = 2; i < arr.length; i++){
			if(arr[i] >= m2){
				m2 = arr[i];
			}else if(arr[i] >= m1){
					m1 = arr[i];
			}
	    }
		ret[0] = m2;
		ret[1] = m1;
		return ret;
	}
	
	public static int sum(int[] arr, int len){
		return len == 0 ? 0 : arr[len - 1] + sum(arr, len - 1);
	}

	/**
	   DFS
	 */
   public static int maxPath(Node node){
	   if(node != null){
		   int left = maxPath(node.left);
		   int right = maxPath(node.right);
		   return Math.max(left, right) + node.data;
	   }
	   return 0;
   }

   public static <T> List<List<T>> tails(List<T> ls){
	   List<T> ret = new ArrayList<>();
	   int n = ls.size();
	   List<Integer> inxls = IntStream.range(0, n).mapToObj(x -> x).collect(Collectors.toList());
	   List<List<T>> ss = inxls.stream().map(x -> drop(x, ls)).collect(Collectors.toList());
	   return ss;
   }

    /**
       KEY: combination of list
       1 2 3
       [1, 2], [1, 3] [2, 3]
    */
   public static void combination(List<Integer> prefix, List<Integer> ls, Integer n){
	   if(n == 0){
		   printList(prefix);
	   }else{
		   for(int i = 0; i < ls.size(); i++){
			   Integer a = ls.get(i);
			   var rs = drop(i+1, ls);
			   // combination(cons(a, prefix), rs, n-1);   // [2,1] [3,1] [3,2]
			   combination(append(prefix, a), rs, n-1);    // [1,2] [1,3] [2,3]
		   }
	   }
   }

    public static Map<String, Set<String>> geneGraph(List<Edge> ls){
        Map<String, Set<String>> map = new HashMap<>();
        for(Edge e : ls){
            var set = map.get(e.beg);
            if( set == null){
                set = new HashSet<>();
            }
            set.add(e.end);
            map.put(e.beg, set);
        }
        return map;
    }


    public static void printNode(Node head){
        Node curr = head;
        while(curr != null){
            pl(curr.toString());
            curr = curr.next;
        }
    }
    public static Node insertNode(Node head, Node node){
       if(head == null){
           head = node;
       }else{
            Node curr = head;
            Node prev = null;
            while(curr.next != null){
               curr = curr.next;
            }
            curr.next = node;
       }
       return head;
    }

    public static Node insertFront(Node head, Node node){
        if(head == null){
            head = node;
        }else{
            node.next = head;
            head = node;
        }
        return head;
    }

    public static Node insertLast(Node head, Node node){
        if(head == null){
            head = node;
        }else{
            Node curr = head;
            while(curr.next != null){
                curr = curr.next;
            }
            curr.next = node;
        }
        return head;
    }

    public static Node removeFront(Node head){
        if(head == null){
            // Nothing to remove
        }else{
            Node h2 = head.next;
            head = h2;
        }
        return head;
    }
    public static Node removeLast(Node head){
        if(head == null){
            // Nothing to remove
        }else{
            Node curr = head;
            Node prev = null;
            while(curr.next != null){
                prev = curr;
                curr = curr.next;
            }
            if(prev == null){  // Only one node
                head = null;
            }else{
               prev.next = null; 
            }
        }
        return head;
    }

    public static boolean contains(TNode root, String s, int inx){
        if(inx == s.length()){
            return root.isWord;
        }else{
            char c = s.charAt(inx);
            TNode node = root.map.get(c);
            if(node != null){
                return contains(node, s, inx + 1);
            }
            return false;
        }
    }
    public static void insertStr(TNode root, String s, int inx){
        if(inx < s.length()){
            char c = s.charAt(inx);
            TNode node = root.map.get(c);
            if(node == null){
                node = new TNode(false);
                root.map.put(c, node);
            }
            insertStr(node, s, inx + 1);
        }else{
            root.isWord = true;
        }
    }

	/**
	   int[][] arr2d = {
		   { 0,   0,   0,  1},
		   { 1,   1,   0,  1},
		   { 0,   1,   0,  1},
		   { 0,   1,   1,  0},
	   };

	   connected island problem
	 */
	public static int connectIsland(int[][] arr, int hInx, int wInx){
		int hh = arr.length;
		int ww = arr[0].length;
		int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
		if(arr[hInx][wInx] == 1){
			int tmp = arr[hInx][wInx];
			arr[hInx][wInx] = 0;
			if(hInx + 1 < hh){
				n1 = connectIsland(arr, hInx + 1, wInx);
			}
			if(hInx - 1 >= 0){
				n2 = connectIsland(arr, hInx - 1, wInx);
			}
			if(wInx + 1 < ww){
				n3 = connectIsland(arr, hInx, wInx + 1);
			}
			if(wInx - 1 >= 0){
				n4 = connectIsland(arr, hInx, wInx - 1);
			}
			arr[hInx][wInx] = tmp;
			return n1 + n2 + n3 + n4 + 1;
		}
		return 0;
	}

    /**
	   int[][] arr2d = {
		   { 0,   0,   0,  1},
		   { 1,   1,   0,  1},
		   { 0,   1,   0,  1},
		   { 0,   1,   1,  0},
	   };

	   connected island problem
	   []
	   []
	   []
	   [(0 3) (1 3) (2 3)]
	   [(1 0) (1 1) (2 1) (3 1) (3 2)]
	   [(1 1) (2 1) (3 1) (3 2) (1 0)]
	   []
	   [(1 3) (2 3) (0 3)]
	   []
	   [(2 1) (3 1) (3 2) (1 1) (1 0)]
	   []
	   [(2 3) (1 3) (0 3)]
	   []
	   [(3 1) (2 1) (1 1) (1 0) (3 2)]
	   [(3 2) (3 1) (2 1) (1 1) (1 0)]
	   []
	*/
	public static int connectIslandWithPosition(int[][] arr, int hInx, int wInx, List<XY> ls){
		int hh = arr.length;
		int ww = arr[0].length;
		int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
		if(arr[hInx][wInx] == 1){
			int tmp = arr[hInx][wInx];
			arr[hInx][wInx] = 0;
			/**
			if(ls.size() > 0){
				XY xy = last(ls);
				// All paths is either a -> b -> c or c -> b -> a
				// See word search problem
				// Adjacent position
				if(xy.x == hInx && Math.abs(xy.y - wInx) == 1 || xy.y == wInx && Math.abs(xy.x - hInx) == 1){
					ls.add(new XY(hInx, wInx));
				}
			}else{
				ls.add(new XY(hInx, wInx));
			}
			*/
			// ls.add(new XY(hInx, wInx));
			var lt = append(ls, new XY(hInx, wInx));
			if(lt.size() > 2){
				printList(lt);
			}
			
			if(hInx + 1 < hh){
				n1 = connectIslandWithPosition(arr, hInx + 1, wInx, lt);
			}
			if(hInx - 1 >= 0){
				n2 = connectIslandWithPosition(arr, hInx - 1, wInx, lt);
			}
			if(wInx + 1 < ww){
				n3 = connectIslandWithPosition(arr, hInx, wInx + 1, lt);
			}
			if(wInx - 1 >= 0){
				n4 = connectIslandWithPosition(arr, hInx, wInx - 1, lt);
			}
			arr[hInx][wInx] = tmp;
			return n1 + n2 + n3 + n4 + 1;
		}
		return 0;
	}


    /*
       1 [3 4 5]
       [2] 1 [4 5]
       [2 3] 1 [5]
       [2 3 4] 1 

       1   60
       2   20
       6   5
       24  1
             

                
       2   3  4  5
       |   |  |  |
       1x  3  4  5
    */
    public static int[] maxArray(int[] arr){
        int len = arr.length;
        int[] a1 = new int[len];
        int[] a2 = new int[len];
        a1[0] = 1;
        a2[len-1] = 1;
       for(int i = 1; i < len; i++){
         a1[i] = arr[i - 1] * a1[i-1];
       }

       for(int i = 1; i < len; i++){
          int ix = len - 1 - i;
          a2[ix] = arr[ix + 1] * a2[ix + 1];
       }
       for(int i = 0; i < len; i++){
        a1[i] = a1[i] * a2[i];
       }
       printArrint(a1);

       return a1; 
    }

    public static Node insert(Node root, int data){
        if(root == null){
            root = new Node(data, null);
        }else{
            insertNode(root, data);
        }
        return root;
    }

    public static void insertNode(Node root, int data){
        if(root == null){
        }else{
            if(data < root.data){
                if(root.left == null){
                   root.left = new Node(data, root); 
                }else{
                   insertNode(root.left, data); 
                }
            }else if(data > root.data){
                if(root.right == null){
                    root.right = new Node(data, root);
                }else{
                    insertNode(root.right, data);
                }
            }
        }
    }
    public static Node maxBST(Node root){
        if(root.right != null){
            return maxBST(root.right);
        }
        return root;
    }
    public static Node minBST(Node root){
        if(root.left != null){
            return minBST(root.left);
        }
        return root;
    }
    // Delete min node in the subtree
    public static Node deleteMinNode(Node root){
        Node curr = root;
        if(curr.left != null){
            Node parent = null;
            while(curr.left != null){
                parent = curr;
                curr = curr.left;
            }
            parent.left = null;
        }else{
            return curr.right;
        }
        return root;
    }

    public static Node minNode(Node root){
        if(root.left == null){
            return root;
        }else{
            Node curr = root;
            Node prev = null;
            while(curr.left != null){
                curr = curr.left;
            }
            return curr;
        }
    }
    public static Node secondMinNode(Node root){
        if(root.left == null){
            return root;
        }else{
            Node curr = root;
            Node prev = null;
            while(curr.left != null){
                curr = curr.left;
            }
            return curr;
        }
    }

    // Delete max node in the subtree
    public static void deleteMaxNode(Node root){
        Node prev = null;
        if(root.right != null){
            prev = root;
            maxBST(root.right);
        }
        root.right = null;
    }
}
