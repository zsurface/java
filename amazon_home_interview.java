import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

/**
           9
    4            14
 1     6    12       17

search:  >= 
9 

13
    find(curr, int n, parent){
        if(curr != null){
            if(n < curr.data)               // 13
                return find(curr.left, n, curr);
            else if(n == curr.data){        // 14
                int ret = n;
                return ret;
            }else if(n > curr.data){         // 15
                return find(curr.right, n, curr);        
            }
        }else{
            return parent != null : parent.data : -1;
        }
    }

 KEY: amazon interview, delete node, delete minimum node

*/

class Person{
    String name;
    int age;
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }
    @Override 
    public String toString(){
        return "name=" + name + " " + "age=" + age;
    }
}

class KNode<T>{
    String key;
    T data;
    KNode left;
    KNode right;
    public KNode(String key, T data){
        this.key = key;
        this.data = data;
    }
}

/**
    n1 <-> n2 <-> n3
    linkedList.addLast()
    linkedList.removeFirst()

    s1 -> n1
*/

class LRU{
    int count = 0;
    int max;
    LinkedList<KNode> list = new LinkedList<>();
    Map<String, KNode> map = new HashMap<>();
    public LRU(int max){
        this.max = max;
    }
    public void put(String key, Person data){
        KNode n = map.get(key);
        if(n == null){
            if(count < max){
                KNode n1 = new KNode(key, data);
                map.put(key, n1);
                list.addLast(n1);
                count++;
            }else{
                KNode n2 = new KNode(key, data);
                KNode firstKNode = list.get(0);
                list.removeFirst(); 
                list.addLast(n2);
                map.remove(firstKNode.key);
                map.put(key, n2);
            }
        }else{
           list.remove(n);
           KNode n3 = new KNode(key, data);
           list.addLast(n3);
           map.put(key, n3);
        }
    }
    public KNode get(String key){
        KNode n = map.get(key);
        if(n != null){
            list.remove(n);
            list.add(n);
        }
        return n;
    }
    public void print(){
        for(Map.Entry<String, KNode> item : map.entrySet()){
            System.out.println(item.getKey() + " " + item.getValue().data.toString());
        }
    }
}

class Pair{
    List<Integer> left = new ArrayList<>();
    List<Integer> right = new ArrayList<>();
    public Pair(List<Integer> left, List<Integer> right){
        this.left = left;
        this.right = right;
    }
}

/**
    Given a list of Node which contains timeStamp and customerId

    1, ab
    2, cd
    3, ab
    4, cd
*/
public class amazon_home_interview{
    public static void main(String[] args) {
        test_deleteMinNode();

        test0();
        {
            LRU lru = new LRU(1);
            Person p1 = new Person("David", 1);
            Person p2 = new Person("Jonny", 2);
            lru.put("a", p1);
            lru.put("b", p2);
            lru.print();
        }
        fl();
        {
            LRU lru = new LRU(2);
            Person p1 = new Person("David", 1);
            Person p2 = new Person("Jonny", 2);
            lru.put("a", p1);
            lru.put("b", p2);
            lru.print();
        }
        fl();
        {
            LRU lru = new LRU(2);
            Person p1 = new Person("David", 1);
            Person p2 = new Person("Jonny", 2);
            lru.put("a", p1);
            lru.put("a", p2);
            lru.print();
        }
        fl();
        {
            LRU lru = new LRU(2);
            Person p1 = new Person("David", 1);
            Person p2 = new Person("Jonny", 2);
            Person p3 = new Person("Bob", 3);
            Person p4 = new Person("Alice", 4);
            lru.put("a", p1);
            lru.put("a", p2);
            lru.put("b", p3);
            lru.put("c", p4);
            lru.print();
            fl();
            KNode n = lru.get("c");
            System.out.print(n.data.toString()); 
        }

        test1();
	test2();
    }
    public static void test0(){
        beg();

        end();
    }
    public static void insertBin(Node root, Integer n){
        Node curr = root;
        if(root == null){
            root = curr = new Node(n);
        }else{
            if(n <= curr.data){
                if(curr.left == null)
                    curr.left = new Node(n);
                else
                    insertBin(curr.left, n);
            }else{
                if(curr.right == null)
                    curr.right = new Node(n);
                else 
                    insertBin(curr.right, n);
            }
        }
    }
    /**
       Assume there is left or right subtree in root
       => root.left or root.right is not null, CAN NOT both be null.
                           10
               4                         14
         2            6             12            18

        
    */
    public static boolean deleteMinNode(Node root, Node parent){
        if(root == null)
            return false;
        else{
	    // no left subtree
	    if(root.left == null){
		if(root.right != null){
		    
		    Node minNode = minBinTree(root.right);
		    root.data = minNode.data;
		    deleteMinNode(root.right, minNode.data);
		}
		else
		    if(parent != null)
			parent.left = null;
		    else  // one node only
			root = null;
	    }else{
		deleteMinNode(root.left, root);
	    }
        }
        return true;
    }

    
    public static boolean deleteMinNode(Node root){
	Node parent = null;
	return deleteMinNode(root, parent);
    }
    public static void test_deleteMinNode(){
        beg();
        {
	    fl("deleteMinNode case 1");
            BST b1 = new BST();
            b1.insert(10);
            b1.insert(5);
            b1.insert(15);
            b1.insert(1);
            b1.insert(7);

            inorder(b1.root); 
            deleteMinNode(b1.root);
            fl();
            List<Integer> ls = inorderToList(b1.root);
	    t(ls, list(5, 7, 10, 15));
	    
            // Aron.binImage(b1.root);
        }
        {
	    fl("deleteMinNode case 2");
            BST b1 = new BST();
            b1.insert(10);
            b1.insert(5);
            b1.insert(5);
            b1.insert(15);
            b1.insert(1);
            b1.insert(7);

            inorder(b1.root); 
            deleteMinNode(b1.root);
            fl();
            List<Integer> ls = inorderToList(b1.root);
	    t(ls, list(5, 5, 7, 10, 15));
            // Aron.binImage(b1.root);
        }
	{
	    
	    fl("deleteMinNode case 3");
            BST b1 = new BST();
            b1.insert(10);
            deleteMinNode(b1.root);
            fl();
            List<Integer> ls = inorderToList(b1.root);
	    t(ls, list(10));
            // Aron.binImage(b1.root);
        }
	{
	    fl("deleteMinNode case 4");
            BST b1 = new BST();
            b1.insert(10);
	    b1.insert(1);
            inorder(b1.root); 
            deleteMinNode(b1.root);
            fl();
            List<Integer> ls = inorderToList(b1.root);
	    t(ls, list(10));
            // Aron.binImage(b1.root);
        }
	{
	    fl("deleteMinNode cast 5");
            BST b1 = new BST();
            b1.insert(2);
	    b1.insert(4);
            inorder(b1.root); 
            deleteMinNode(b1.root);
            fl();
            List<Integer> ls = inorderToList(b1.root);
	    t(ls, list(4));
            // Aron.binImage(b1.root);
        }
        end();
    }

    /** 
    delete n from an BST
    delete the minimum value that is equal or greater to n
    */
    public static void deleteNode(Node root, Integer n){
	class NodeDir{
	    Node parent;
	    Boolean isLeft = true;
	}
        Node curr = root;
        Node parent = null;
	NodeDir nodeDir = new NodeDir();
	
        while(curr != null){
	    // p("curr.data=" + curr.data);
            if(n == curr.data){
		// equal node
		// pp("curr.right=" + curr.right);
                if(curr.right != null) { // There is right subtree
		    // get the minimum, most left subtree
                    Node minNode = minBinTree(curr.right);
		    // pp("minNode.data=" + minNode.data);
		    curr.data = minNode.data;
		    if(curr.right.left == null && curr.right.right == null)
			curr.right = null;
		    else
			deleteMinNode(curr.right);
		    
		    break;
                }else if(curr.left != null) { //  There is left subtree
                    Node maxNode = maxBinTree(curr.left);
		    curr.data = maxNode.data;
		    // only one node in the left subtree
		    if(curr.left.left == null && curr.left.right == null)
			curr.left = null;
		    else
			deleteMinNode(curr.left);
                }else{
		    if(nodeDir.parent != null){
			if(nodeDir.isLeft){
			    nodeDir.parent.left = null;
			}else{
			    nodeDir.parent.right = null;
			}
			break;
		    }else{
			// you CAN not delete your self.
		    }
		}
            }else if(n > curr.data){    // greater than, recurve in the right subtree
                nodeDir.parent = curr;
		nodeDir.isLeft = false;
                curr = curr.right;
            }else{
		nodeDir.parent = curr;
		nodeDir.isLeft = true;
		curr = curr.left;
            }
        }

    }
    
    /**
           4
      2        9
   1    3    7    10 

    preorder
    4 2 1 3 9 7 10
    4
    4 [2 1 3] [9 7 10]

    inorder
    1 2 3 4 7 9 10
    1 2 3   7 9 10

    left subtree
    [2 1 3]  [1 2 3]    
    right subtree
    [9 7 10] [7 9 10]

    
    4

    // s = a b c d
    //         2
    // take 2 s  
    // sublist(0, 2)
    // sublist(2, list.size());

    */
    public Node buildTree(List<Integer> preList, List<Integer> inList){
        if(preList.size() > 0){
            Integer n = preList.get(0);
            preList.remove(0);
            Pair in = splitList(inList, n);
            int len = in.left.size();
            List<Integer> leftPre = preList.subList(0, len);    // [0, 1, 2) [2, 3, 4) // take len preList
            List<Integer> rightPre = preList.subList(len, preList.size());  // drop len preList
            Node left = buildTree(leftPre, in.left); 
            Node right = buildTree(rightPre, in.right);
            Node node = new Node(n);
            node.left = left;
            node.right = right;
            return node;
        }
        return null;
    }
    public static Pair splitList(List<Integer> list, int num){
        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();
        for(Integer n : list){
            if(n < num)
                left.add(n);
            else if(n > num)
                right.add(n);
        }
        return new Pair(left, right);
    }

    public static void test1(){
        beg();
	{
	    fl("case 1");
	    Node node = geneBinNoImage(list(9, 6, 2, 7));
	    deleteNode(node, 6);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(2, 7, 9));
	}
	{
	    fl("case 2");
	    Node node = geneBinNoImage(list(9, 6, 2, 7));
	    deleteNode(node, 7);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(2, 6, 9));
	}
	{
	    fl("case 3");
	    Node node = geneBinNoImage(list(9, 6));
	    deleteNode(node, 6);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(9));
	}
	{
	    fl("case 4");
	    Node node = geneBinNoImage(list(9, 10));
	    deleteNode(node, 10);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(9));
	}
	{
	    fl("case 5");
	    Node node = geneBinNoImage(list(1, 2, 3));
	    deleteNode(node, 3);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(1, 2));
	}
	{
	    fl("case 6");
	    Node node = geneBinNoImage(list(3, 2, 1));
	    deleteNode(node, 1);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(2, 3));
	}
	
        end();
    }
    public static void test2(){
        beg();
	{
	    fl("case 11");
	    Node node = geneBinNoImage(list(9, 6, 7, 1, 2, 4, 10));
	    deleteNode(node, 2);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(1, 4, 6, 7, 9, 10));
	}
	{
	    fl("case 12");
	    Node node = geneBinNoImage(list(9, 6, 7, 1, 2, 4, 10));
	    deleteNode(node, 2);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(1, 4, 6, 7, 9, 10));
	}
	{
	    fl("case 13");
	    Node node = geneBinNoImage(list(9, 6, 7, 1, 2, 4, 10));
	    deleteNode(node, 6);
	    Ut.l();
	    fl("deleteNode");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(1, 2, 4, 7, 9, 10));
	}
	{
	    fl("case 14");
	    Node node = geneBinNoImage(list(9, 6, 7, 1, 2, 4, 10));
	    deleteNode(node, 1);
	    Ut.l();
	    fl("deleteNode 1");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(2, 4, 6, 7, 9, 10));
	}
	{
	    fl("case 15");
	    Node node = geneBinNoImage(list(9, 6, 7, 1, 2, 4, 10));
	    deleteNode(node, 10);
	    fl("deleteNode 10");
	    printTree(node);
	    List<Integer> ls = inorderToList(node);
	    t(ls, list(1, 2, 4, 6, 7, 9));
	    
	    fl("deleteNode 4 kkk");
	    deleteNode(node, 4);
	    printTree(node);
	    List<Integer> ls1 = inorderToList(node);
	    t(ls1, list(1, 2, 6, 7, 9));
	    
	    fl("deleteNode 2");
	    deleteNode(node, 2);
	    printTree(node);
	    List<Integer> ls2 = inorderToList(node);
	    t(ls2, list(1, 6, 7, 9));
	    
	    fl("deleteNode 7");
	    deleteNode(node, 7);
	    printTree(node);
	    List<Integer> ls3 = inorderToList(node);
	    t(ls3, list(1, 6, 9));
	}
    }
} 

