import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class randomStr{
    public static void main(String[] args) {
        test0();
        //test1();
    }
    public static void test0(){
        Aron.beg();
//        List<Integer> ls = Arrays.asList(1,3, 2, 9, 4);
//        List<Integer> actMin = removeMin(ls);
//        List<Integer> actMax= removeMax(ls);
//
//        List<Integer> exp1 = Arrays.asList(3, 2, 9, 4);
//        List<Integer> exp2 = Arrays.asList(1,3, 2,4);
//        Test.t(actMin, exp1);
//        Test.t(actMax, exp2);
        
        List<Integer> ls = Arrays.asList(1,3,2);
        List<Integer> sortedList = selectionSort(ls);
        Print.p(sortedList);

        Aron.end();
    }

    public static <T extends Comparable<T>> List<T> selectionSort(List<T> list){
        List<T> ls = new ArrayList<>(list);
        List<T> retList = new ArrayList<>(list);
        while(ls.size() > 0){
            Pair<T, List<T>> p = removeMin(ls);
            retList.add(p.getKey());
            ls = new ArrayList<>(p.getValue());
        }
        return retList;
    }
    public static <T extends Comparable<T>> Pair<T, List<T>> removeMin(List<T> list){
        if(list != null){
            List<T> ls = new ArrayList<>(list);
            int len = list.size();
            if(len > 0){
                T min = list.get(0);
                int mInx = 0;
                for(int i=1; i<len; i++){
                    // min - list.get(i) > 0 => min > list.get(i)
                    if(min.compareTo(ls.get(i)) > 0){
                        min = ls.get(i);
                        mInx = i;
                    }
                }
                ls.remove(mInx);
                return new Pair<T, List<T>>(min, ls);
            }
        }
        throw new IllegalArgumentException("List can not be null or empty.");
    }
    public static <T extends Comparable<T>> Pair<T, List<T>> removeMax(List<T> list){
        List<T> ls = new ArrayList<>(list);
        if(list != null){
            int len = list.size();
            if(len > 0){
                T max = list.get(0);
                int maxIx = 0;
                for(int i=1; i<len; i++){
                    // max - list.get(i) < 0 => min > list.get(i)
                    if(max.compareTo(ls.get(i)) < 0){
                        max = ls.get(i);
                        maxIx = i;
                    }
                }
                ls.remove(maxIx);
                return new Pair<T, List<T>>(max, ls);
            }
        }
        throw new IllegalArgumentException("List can not be null or empty");
    }
    
    /**
    <pre>
    {@literal
        s1 = s1 + s2
    }
    {@code
    String = "cat";
    }
    </pre>
    */ 
    public static void concatList2(List<String> s1, List<String> s2){
        if(s1 != null && s2 != null){
            for(String s : s2){
                s1.add(s);
            }
        }
    }
    public static void test1(){
        Aron.beg();
//        {
//            // 2^10 = 2^4 * 2^4 * 2^2 = 16 * 16 * 4 = 256*4 = (250 + 6)*4 = 1000 + 24 = 1024
//            // 2^20 = 1024*1024 = (1000 + 24) = 1000000 + 24*24 + 48*1000
//            // 2^30 = 1024*1024*1024 = (1000 + 24)^3 = 1000 000 000 
//            // 4 byte + 40x1000000
//            int min = 1;
//            int max = 4;
//            int count = 1000000;
//            List<String> list1 = Aron.geneRandomStrList(min, max, count);
//            List<String> list2 = Aron.geneRandomStrList(min, max, count);
//            StopWatch sw = new StopWatch();
//
//            sw.start();
//            concatList2(list1, list2);
//            sw.printMillionSeconds();
//        }
        {
            int min = 1;
            int max = 4;
            int count = 100;
            List<String> list1 = Aron.geneRandomStrList(min, max, count);
            List<String> list2 = Aron.geneRandomStrList(min, max, count);
            StopWatch sw = new StopWatch();

            sw.start();
            List<String> ls = Aron.concatList(list1, list2);
            sw.printMillionSeconds();
        }
        Aron.end();
    }
    
    
} 

