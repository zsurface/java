import static classfile.Print.*;
import static classfile.Aron.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import classfile.Ut;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

import java.io.*;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URL;

// KEY: Jsoup, jsoup
public class JsoupTry1{
    public static void main(String[] args) {
        // test0();
		// test1();
		test2();
    }
    public static void test0(){
        beg();

		try{
			String url = "https://xfido.com";
			Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
			Elements links = doc.select("a[href]");
		
			for(Element e : links){
				pl("text=" + e.text());
				pl("toString=" + e.toString());
				pl("href=" + e.attr("abs:href"));
			}
		}catch (IOException e){
			e.printStackTrace();
		}
		
        end();
    }
	
	public static void test1(){
        beg();
		
		try{
			File input = new File("/tmp/x.html");
			// Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
			Document doc = Jsoup.parse(input, "UTF-8", "http://x.com");
			Elements elm = doc.select("div.con").select("a[href]");
			// Elements elm = doc.select("div.con");
			
			/*
			Elements links = doc.select("a[href]");
			for(Element e : links){
				pl("text=" + e.text());
				pl("toString=" + e.toString());
				pl("href=" + e.attr("abs:href"));
			}
			*/
			
			for(Element e : elm){
				pl("text=" + e.text());
				pl("toString=" + e.toString());
				pl("abs:href=" + e.attr("abs:href"));
				pl("href=" + e.attr("href"));
				String href = e.attr("href");
				String category = dropExt(takeName(href));
				pl("category=" + category + " -> " + e.text()); 
			}

		}catch (IOException e){
			e.printStackTrace();
		}
		
        end();
    }
	
	public static Map<String, String> filterMap(Map<String, String> map){
		Map<String, String> retMap = new HashMap<>();
		for(Map.Entry<String, String> entry : map.entrySet()){
			if(containStr(entry.getKey(), "vanpk") == false){
				retMap.put(entry.getKey(), entry.getValue());
			}
		}
		return retMap;
	}
	
	public static void test2(){
        beg();
		{
			fl("getCategoryMap");
			Map<String, String> map = getCategoryMap();
			printMap(map);
			pl("map size=" + map.size());
		}
		{
			fl("filterMap");
			Map<String, String> map = getCategoryMap();
			Map<String, String> fmap = filterMap(map);
			printMap(fmap);
			pl("map  size="  + map.size());
			pl("fmap size=" + fmap.size());
		}
		end();
	}
	   
		
	public static Map<String, String> getCategoryMap(){
		Map<String, String> map = new HashMap<>();
		try{
			String bitbucket = getEnv("b");
			String indexHtml = bitbucket + "/" + "testfile/vansky" + "/" + "index.html";
			File input = new File(indexHtml);
			// Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
			Document doc = Jsoup.parse(input, "UTF-8", "http://x.com");
			Elements elm = doc.select("div.con").select("a[href]");
		   
			for(Element e : elm){
				/*
				pl("text=" + e.text());
				pl("toString=" + e.toString());
				pl("abs:href=" + e.attr("abs:href"));
				pl("href=" + e.attr("href"));
			    */
				String href = e.attr("href");
				String category = dropExt(takeName(href));
				pl("category=" + category + " -> " + e.text());
				map.put(category, e.text());
			}

		}catch (IOException e){
			e.printStackTrace();
		}
		return map;
    }
} 

