import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class tryLongestValidParentheses{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        {
            {
                String s = "";
                int ret = longestValidParentheses(s);
                t(ret, 0);
            }
            {
                String s = "(";
                int ret = longestValidParentheses(s);
                t(ret, 0);
            }
            {
                String s = ")";
                int ret = longestValidParentheses(s);
                t(ret, 0);
            }
            {
                String s = ")(";
                int ret = longestValidParentheses(s);
                t(ret, 0);
            }
            {
                String s = "()";
                int ret = longestValidParentheses(s);
                t(ret, 2);
            }
            {
                String s = "(()";
                int ret = longestValidParentheses(s);
                t(ret, 2);
            }
            {
                String s = "())";
                int ret = longestValidParentheses(s);
                t(ret, 2);
            }
            {
                String s = "()()";
                int ret = longestValidParentheses(s);
                t(ret, 4);
            }
            {
                String s = "(())";
                int ret = longestValidParentheses(s);
                t(ret, 4);
            }
            {
                String s = "((())";
                int ret = longestValidParentheses(s);
                t(ret, 4);
            }
            {
                String s = "((()";
                int ret = longestValidParentheses(s);
                t(ret, 2);
            }
            {
                String s = "((()(";
                int ret = longestValidParentheses(s);
                t(ret, 2);
            }
        }
        end();
    }
    public static void test1(){
        beg();
        end();
    }

    /**
        ( )
        x
          x
        (


        ) )
        x x

        ( ) )
        x
          x
        (

        ( ( )
        x
          x
             x
        (
           ( 

         ) ) (
         x
           x
              (
    */
    public static int longestValidParentheses(String s){
        int ret = 0;
        Stack<Character> stack = new Stack<>();

        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(c == '('){
                stack.push(c);
            }else{
                if(stack.size() > 0){
                    Character ch = stack.pop();
                    if(ch == '(' && c == ')'){
                        ret += 2;
                    }
                }
            }
        }
        return ret;
    }
    
} 

