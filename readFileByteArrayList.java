import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.nio.charset.StandardCharsets;

public class readFileByteArrayList{
    public static void main(String[] args) {
        test2();
    }
    public static void removeCharFromFile(String fname, char ch){
            List<byte[]> listBuf = Aron.readFileByteList(fname, 100);
            List<byte[]> list = new ArrayList<>(); 
            for(byte[] b : listBuf){
                byte[] tmpByte = new byte[b.length];
                int k=0;
                for(int i=0; i<b.length; i++){
                    if(b[i] != ch){
                        tmpByte[k] = b[i];
                        k++;
                    }
                }
                byte[] nbf = Arrays.copyOf(tmpByte, k);
                list.add(nbf);
            }
            Aron.writeFileByteArray(fname, list);
    }
    public static void test2() {
        Aron.beg();

        {
//            String fname = "/tmp/gg1.x";
////            List<String> list = Aron.geneRandomStrList(100);
////            Aron.writeFile(fname, list);
//            List<byte[]> listBuf = Aron.readFileByteList(fname, 100);
//            Print.pListByte(listBuf);
//            List<byte[]> exp = new ArrayList<>();
//            byte[] b1 = {97, 98, 99, 10};
//            byte[] b2 = {97, 98, 99, 100, 10};
//            byte[] b3 = {10};
//            exp.add(b1);
//            exp.add(b2);
//            exp.add(b3);
//            
//            for(int i=0; i<listBuf.size(); i++){
//                Test.t(listBuf.get(i), exp.get(i));
//            }
        }
        {
            String fname = "/tmp/gg1.x";
            char ch = ',';
            removeCharFromFile(fname, ch);
        }

        Aron.end();
    }
}

