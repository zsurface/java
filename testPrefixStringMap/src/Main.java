import classfile.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Print.p();
        test0();
    }

    static void test0(){
        Aron.beg();

        List<String> list1 = new ArrayList<>(Arrays.asList("cat", "dog", "pig")); 
        List<List<String>> list2d = Aron.prefixWordsFromList(list1); 
        for(List<String> slist : list2d){
            Print.p(Aron.listToString(slist));
        }
        Aron.printList2d(list2d);

        Aron.end();
    }
}
