import java.io.*;
import java.lang.String;
import java.util.*;
class ReverseStr
{
	public static void main(String args[]){
		String s = "hi there";
		String rs = RevStr(s);
		System.out.println(rs);
	}
	public static String RevStr(String str){
		int len = str.length();
		StringBuffer sb = new StringBuffer(str);
		for(int i = 0; i < len/2; i++){
			char c = sb.charAt(i);
			sb.setCharAt(i, sb.charAt(len - 1 - i));
			sb.setCharAt(len - 1 - i, c);
		}
		return sb.toString();
	}
}
