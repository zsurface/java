import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Tuple.*;
import java.util.stream.*;
import java.util.stream.Collectors;


public class LongestRepeatingCharRep{
    public static void main(String[] args) {
        test1();
    }

    public static int maxRepeatingChar(String s, int k){
            List<String> ls = allSubstring(s); 
            int maxRepeating = 0;

            for(String e : ls){
                Map<Character, Integer> map = frequencyCount(e);

                int max = 0;
                for(Map.Entry<Character, Integer> entry : map.entrySet()){
                    if(entry.getValue() > max){
                        max = entry.getValue();
                    }
                }
                int maxRep = len(e);
                int diff = maxRep - max;

                if(k == diff){
                    if(maxRep > maxRepeating){
                        maxRepeating = maxRep;
                    }
                }
            }
            return maxRepeating;
    }

    public static void test1(){
        beg();
        {
            {
                fl("maxRepeatingChar 1");
                String s = "banana";
                int k = 1;
                int max = maxRepeatingChar(s, k);
                pl("max=" + max);
                t(max, 3);
            }
            {
                fl("maxRepeatingChar 2");
                String s = "banana";
                int k = 2;
                int max = maxRepeatingChar(s, k);
                pl("max=" + max);
                t(max, 5);  // anana
            }
            {
                fl("maxRepeatingChar 3");
                String s = "banannnn";
                int k = 3;
                int max = maxRepeatingChar(s, k);
                pl("max=" + max);
                t(max, 8); 
            }
        }
		end();
    }

}
