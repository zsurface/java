import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class try_dimension{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        beg();
        {
            Integer ls = 3; 
            int bo = dim(ls);
            t(bo, 0);
        }
        {
            List<Character> ls = new ArrayList<>();
            int bo = dim(ls);
            t(bo, 0);
        }
        {
            ArrayList<Character> ls = new ArrayList<>();
            int bo = dim(ls);
            t(bo, 0);
        }
        {
            ArrayList<Integer> ls = new ArrayList<>();
            ls.add(1);
            int bo = dim(ls);
            t(bo, 1);
        }
        {
            List<ArrayList<Integer>> lss = new ArrayList<>();
            ArrayList<Integer> ls = new ArrayList<>();
            ls.add(1);
            lss.add(ls);
            int bo = dim(lss);
            t(bo, 2);
        }

        end();
    }
    public static void test1(){
        beg();
        String fname = "/Users/cat/myfile/bitbucket/testfile/test.txt";
        StopWatch sw = new StopWatch();
        sw.start();



        sw.printTime();
        end();
    }
} 

