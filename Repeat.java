import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

public class Repeat{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();

        String s = repeat(18, "-");
        Print.p(s);

        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        Aron.end();
    }
    public static String repeat(int n, String s){
        return IntStream.range(0, n).mapToObj(i -> s).collect(Collectors.joining("")); 
    }
} 

