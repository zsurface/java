import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class Animal{
    public void speak(){
    }
    public String getType(){
        return "generic animal";
    }
};

class Cat extends Animal{
    @Override
    public void speak(){
    }
    @Override
    public String getType(){
        return "cat animal";
    }
};

// subtype and generic type in Java
// List<Object> lo = new ArrayList<Object>(); 
// List<String> ls = new ArrayList<String>();
// lo = ls; // error


// create your own annotation

@interface MyDefault{
    boolean value() default true;
}


public class AnnotationExample{
    public static void main(String[] args) {
        test0();
        test1();
    }
    public static void test0(){
        Aron.beg();
        
        // immutable list
        List<Integer> list = Arrays.asList(1, 2, 3);
        
        Set<String> set1 = new HashSet<String>(Arrays.asList("cat", "dog"));

        Print.pp(arr2d);


        Aron.end();
    }
    public static void test1(){
        Aron.beg();
        List<Object> lo = new ArrayList<Object>();
        List<String> ls = new ArrayList<String>();
        lo.add("dog");
        Aron.end();
    }
} 

